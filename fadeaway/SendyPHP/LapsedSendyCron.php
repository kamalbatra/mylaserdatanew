<?php
//~ error_reporting(E_ALL);
//~ ini_set('display_errors', 1);
//~ error_reporting(0);
define('SALT','324325923495kdfgiert734t');

$template_name 	= "LAPSED CLIENTS";

$list_name_test = "Lapsed Newsletter Clients New";
$list_name_live = "Lapsed Newsletter Clients";
$list_name = "Lapsed Newsletter Clients";

include('/var/www/html/sendy/includes/helpers/short.php');
require('SendyPHP.php');
$sendy 			= new \SendyPHP\SendyPHP($list_name);
$conn_fadeaway 	= mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway");
$currentDate 	= date("Y-m-d 00:00:00");
$businessids = '(1,6,12,11)';
//~ $businessids = '(11)';

/*
 * $businessids contains bussiness IDs from table "tbl_business" where 
 * Business id 	==> Business name
 * 		1  		==> Fadeaway Laser
 * 		6  		==> Vamoose Tattoo Removal
 * 		11 		==> testbussinessaccount
 * 		12 		==> Fade Away Laser - Canada
 * 
 * */
$cols = "tbl_business.BusinessID, tbl_business.BusinessName, AES_DECRYPT(Email, '".SALT."') AS Email, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, tbl_treatment_log.ClientID, tbl_business.sendy_brand_id AS app_id";
$condition = "tbl_clients.ClientID = tbl_treatment_log.ClientID INNER JOIN tbl_business ON tbl_business.BusinessID = tbl_clients.BusinessID AND tbl_business.BusinessID IN ".$businessids." WHERE tbl_clients.TimeStamp < '".$currentDate."' AND unSubscribe = 0 GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=90 and DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) <=150 ORDER BY tbl_business.sendy_brand_id";

$fadeaway_query = "SELECT ".$cols." FROM tbl_treatment_log INNER JOIN tbl_clients ON ".$condition;

$lapsed_data 	= mysqli_query($conn_fadeaway,$fadeaway_query);
$old_app_id 	= 0;

while($row = mysqli_fetch_assoc($lapsed_data)){

	if($old_app_id != $row['app_id']){
		
		if( $row['app_id'] == 6 ){
			$list_name = $list_name_test;
		} else {
			$list_name = $list_name_live;
		}
		$listdata 	= $sendy->get_cron_list_id($list_name,$row['app_id'], $old_app_id);
		$sendy->setListId($listdata['list_id']);
		// $old_app_id = $row['app_id'];
	}
	
	$subscribe 			= array();
	$subscribe['name'] 	= ucfirst($row['FirstName']).' '.ucfirst($row['LastName']);
	$subscribe['email'] = $row['Email'];
	
	//Starting Changing status of "lapsed_mail_status" cols to 1
	$SQL = "UPDATE tbl_clients set lapsed_mail_status =1 WHERE ClientID ={$row['ClientID']}";
	mysqli_query($conn_fadeaway,$SQL);
	//End Changing status of "lapsed_mail_status"

	$sendy->subscribe($subscribe);
}

$conn_sendy = mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway-sendy");
$query = "SELECT template.userID, template.app, apps.from_name, apps.from_email, apps.reply_to, template.template_name, template.html_text FROM template JOIN apps ON template.app = apps.id WHERE template.template_name = '$template_name'";
$result 	= mysqli_query($conn_sendy,$query) or die($query);

while($fetch_data = mysqli_fetch_assoc($result)){

	if( $fetch_data['app'] == 6 ){
		$list_name = $list_name_test;
	} else {
		$list_name = $list_name_live;
	}
	
	//~ if($fetch_data['app'] == 6){
		$listdata = $sendy->get_cron_list_id($list_name,$fetch_data['app'], 0);
		
		$sendy->setListId($listdata['list_id']);
		$active_count = $sendy->subcount();
		
		if(isset($active_count['message']) && $active_count['message'] != 'List does not exist'){
		
			$campaign_data 					= array();
			$campaign_data['from_name'] 	= $fetch_data['from_name'];
			$campaign_data['from_email'] 	= $fetch_data['from_email'];
			$campaign_data['reply_to'] 		= $fetch_data['reply_to'];
			$campaign_data['subject'] 		= $fetch_data['template_name'];
			$campaign_data['html_text'] 	= $fetch_data['html_text'];
			$campaign_data['list_ids'] 		= $listdata['list_id'];
			$campaign_data['send_campaign'] = 1;
		
			
			$res = $sendy->createAndSendCampaign($campaign_data);
			if($res == "Campaign created and now sending"){
				echo $res;
			}
		
		}	
	//~ }

}

?>
