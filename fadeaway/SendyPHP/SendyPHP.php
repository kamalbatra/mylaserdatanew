<?php

namespace SendyPHP;

/**
 * Sendy Class
 */
class SendyPHP
{
    protected $installation_url;
    protected $api_key;
    protected $list_id;

    public function __construct($listid)
    {
        $list_id = $listid;
        $installation_url = 'http://dexteroustechnologies.co.in/mylaserdata/sendy';
        $api_key = 'pnGODgyOLItRplL0rKne';
        if (empty($list_id)) {
            throw new \Exception("Required config parameter [list_id] is not set or empty", 1);
        }
        
        if (empty($installation_url)) {
            throw new \Exception("Required config parameter [installation_url] is not set or empty", 1);
        }
        
        if (empty($api_key)) {
            throw new \Exception("Required config parameter [api_key] is not set or empty", 1);
        }

        $this->list_id = $list_id;
        $this->installation_url = $installation_url;
        $this->api_key = $api_key;
    }

    public function setListId($list_id)
    {
        if (empty($list_id)) {
            throw new \Exception("Required config parameter [list_id] is not set", 1);
        }

        $this->list_id = $list_id;
    }

    public function getListId()
    {
        return $this->list_id;
    }

    public function subscribe(array $values)
    {
     
        $type = 'subscribe';

        //Send the subscribe
        $result = strval($this->buildAndSend($type, $values));

        //Handle results
        switch ($result) {
            case '1':
                return array(
                    'status' => true,
                    'message' => 'Subscribed'
                    );
                break;

            case 'Already subscribed.':
                return array(
                    'status' => true,
                    'message' => 'Already subscribed.'
                    );
                break;

            default:
                return array(
                    'status' => false,
                    'message' => $result
                    );
                break;
        }
    }

    public function unsubscribe($email)
    {
        $type = 'unsubscribe';

        //Send the unsubscribe
        $result = strval($this->buildAndSend($type, array('email' => $email)));

        //Handle results
        switch ($result) {
            case '1':
                return array(
                    'status' => true,
                    'message' => 'Unsubscribed'
                    );
                break;
            
            default:
                return array(
                    'status' => false,
                    'message' => $result
                    );
                break;
        }
    }

    public function substatus($email)
    {
        $type = 'api/subscribers/subscription-status.php';

        //Send the request for status
        $result = $this->buildAndSend($type, array(
            'email' => $email,
            'api_key' => $this->api_key,
            'list_id' => $this->list_id
        ));

        //Handle the results
        switch ($result) {
            case 'Subscribed':
            case 'Unsubscribed':
            case 'Unconfirmed':
            case 'Bounced':
            case 'Soft bounced':
            case 'Complained':
                return array(
                    'status' => true,
                    'message' => $result
                    );
                break;

            default:
                return array(
                    'status' => false,
                    'message' => $result
                    );
                break;
        }
    }

    public function subcount($list = "")
    {
        $type = 'api/subscribers/active-subscriber-count.php';

        //if a list is passed in use it, otherwise use $this->list_id
        if (empty($list)) {
            $list = $this->list_id;
        }

        //handle exceptions
        if (empty($list)) {
            throw new \Exception("method [subcount] requires parameter [list] or [$this->list_id] to be set.", 1);
        }


        //Send request for subcount
        $result = $this->buildAndSend($type, array(
            'api_key' => $this->api_key,
            'list_id' => $list
        ));

        //Handle the results
        if (is_numeric($result)) {
            return array(
                'status' => true,
                'message' => $result
            );
        }

        //Error
        return array(
            'status' => false,
            'message' => $result
        );
    }
	
	public function createAndSendCampaign($postdata){
		$type = 'api/campaigns/create.php';
		$postdata['api_key'] = $this->api_key;
		$result = $this->buildAndSend($type, $postdata);
		return $result;
	}

	public function removeSubscriber($listID,$line,$userID){
        
        $mysqli = mysqli_connect("localhost", "dexterou_mydata", "myrda)&&&@#%$}", "dexterou_fadeaway-sendy");
		//check if email exists to be deleted
		$q = 'SELECT email, unsubscribed FROM subscribers WHERE list = '.$listID.' AND email = "'.$line.'" AND userID = '.$userID;
		$r = mysqli_query($mysqli, $q);
		$result = mysqli_fetch_assoc($r);
		if (mysqli_num_rows($r) > 0){
			//email exists, delete subscriber
			if($result['unsubscribed'] == 0){
				$query = 'DELETE FROM subscribers WHERE email = "'.$line.'" AND list = '.$listID.' AND userID = '.$userID;
				mysqli_query($mysqli, $query);
			}
		}
		return isset($result['unsubscribed']) ? $result['unsubscribed'] : 0;
	}
	
    private function buildAndSend($type, array $values) {
        //error checking
        if (empty($type)) {
            throw new \Exception("Required config parameter [type] is not set or empty", 1);
        }

        if (empty($values)) {
            throw new \Exception("Required config parameter [values] is not set or empty", 1);
        }

        //Global options for return
        $return_options = array(
            'list' => $this->list_id,
            'boolean' => 'true'
        );

        //Merge the passed in values with the options for return
        $content = array_merge($values, $return_options);
        

        //build a query using the $content
        $postdata = http_build_query($content);

        $ch = curl_init($this->installation_url .'/'. $type);

        // Settings to disable SSL verification for testing (leave commented for production use)
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
    
    public function get_list_id($list_name, $call_count = 0){
        $mysqli = mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway-sendy");
        $rootPath='/var/www/html';
        if($call_count == 0)
            include($rootPath.'/sendy/includes/helpers/short.php');
        $query_list_id = mysqli_query($mysqli,"SELECT id FROM lists WHERE app = $_SESSION[SendyBrandID] AND name = '$list_name'");
        $result_list_id   = mysqli_fetch_assoc($query_list_id);
        $id               = $result_list_id['id'];
        $listid           = short($id);
        mysqli_close($mysqli);
        $listdata['list_id'] = $listid;
        $listdata['original_id'] = $id;
        return $listdata;
    }   
	
	public function get_list_id_new($list_name, $call_count = 0){
        $mysqli = mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway-sendy");
        $rootPath='/var/www/html';
        if($call_count == 0)
            include('/var/www/html/sendy/includes/helpers/short.php');
        $query_list_id = mysqli_query($mysqli,"SELECT id FROM lists WHERE app = $brand_id AND name = '$list_name'");
        $result_list_id   = mysqli_fetch_assoc($query_list_id);
        $id               = $result_list_id['id'];
        $listid           = short($id);
        mysqli_close($mysqli);
        $listdata['list_id'] = $listid;
        $listdata['original_id'] = $id;
        return $listdata;
    } 
	
	public function get_cron_list_id($list_name, $brand_id, $old_app_id){
        
        $mysqli       = mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway-sendy");
        $rootPath     = '/var/www/html';
        
        if($old_app_id == 0)
       // include($_SERVER['DOCUMENT_ROOT'].'/sendy/includes/helpers/short.php');
        $query_list_id    = mysqli_query($mysqli,"SELECT id FROM lists WHERE app = $brand_id AND name = '$list_name'");
        $result_list_id   = mysqli_fetch_assoc($query_list_id);
        $id               = $result_list_id['id'];
        $listid           = short($id);
        mysqli_close($mysqli);
        $listdata['list_id'] = $listid;
        $listdata['original_id'] = $id;
        return $listdata;
    }

    public function get_cron_list_id1($list_name, $brand_id, $old_app_id){
        
        $mysqli       = mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway-sendy");
        $rootPath     = '/var/www/html';
        
        $query_list_id    = mysqli_query($mysqli,"SELECT id FROM lists WHERE app = $brand_id AND name = '$list_name'");
        $result_list_id   = mysqli_fetch_assoc($query_list_id);
        $id               = $result_list_id['id'];
        $listid           = short($id);
        mysqli_close($mysqli);
        $listdata['list_id'] = $listid;
        $listdata['original_id'] = $id;
        return $listdata;
    }

}
