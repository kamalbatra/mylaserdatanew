<?php
error_reporting(-1);

define('SALT','324325923495kdfgiert734t');
$template_name 	= "Duluth New Location";
$list_name 		= "Duluth New Location Revised";

// $template_name 	= "Duluth New Location";
// $list_name 		= "Lapsed Newsletter Clients New";

include('/var/www/html/sendy/includes/helpers/short.php');
require('SendyPHP.php');
$sendy 			= new \SendyPHP\SendyPHP($list_name);

$conn_fadeaway 	= mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway");

$cols 			= "AES_DECRYPT(Email, '".SALT."') AS Email, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, tbl_treatment_log.ClientID, tbl_business.sendy_brand_id AS app_id";

/*$condition 		= "tbl_clients.ClientID = tbl_treatment_log.ClientID INNER JOIN tbl_business ON tbl_business.BusinessID = tbl_clients.BusinessID AND tbl_business.BusinessID =11 WHERE tbl_treatment_log.location = 1 ORDER BY tbl_business.sendy_brand_id  limit 1";*/

$condition 		= "tbl_clients.ClientID = tbl_treatment_log.ClientID INNER JOIN tbl_business ON tbl_business.BusinessID = tbl_clients.BusinessID AND tbl_business.BusinessID =1 ORDER BY tbl_business.sendy_brand_id  limit 1";

$fadeaway_query = "SELECT ".$cols." FROM tbl_treatment_log INNER JOIN tbl_clients ON ".$condition;


$lapsed_data 	= mysqli_query($conn_fadeaway,$fadeaway_query);
$old_app_id 	= 0;

while($row = mysqli_fetch_assoc($lapsed_data)){
	
	if($old_app_id != $row['app_id']){
		$listdata 	= $sendy->get_cron_list_id1($list_name,$row['app_id'], $old_app_id);		
		
		$sendy->setListId($listdata['list_id']);
		$old_app_id = $row['app_id'];		
	}

	$subscribe 					= array();
	$subscribe['name'] 			= ucfirst($row['FirstName']).' '.ucfirst($row['LastName']);
	$sendy->subscribe($subscribe);
}

$conn_sendy = mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway-sendy");

$query 		= "SELECT template.userID, template.app, apps.from_name, apps.from_email, apps.reply_to, template.template_name, template.html_text 
			FROM template JOIN apps ON template.app = apps.id 
			WHERE template.template_name = '$template_name' ORDER BY template.app limit 1";

$result 	= mysqli_query($conn_sendy,$query) or die($query);

while($fetch_data = mysqli_fetch_assoc($result)){
	
	// print_r($fetch_data);
	
	$listdata = $sendy->get_cron_list_id1($list_name,$fetch_data['app'], 1);
	
	$sendy->setListId($listdata['list_id']);
	$active_count = $sendy->subcount();
	
	if(isset($active_count['message']) && $active_count['message']){
		$campaign_data 					= array();
		$campaign_data['from_name'] 	= $fetch_data['from_name'];
		$campaign_data['from_email'] 	= $fetch_data['from_email'];
		$campaign_data['reply_to'] 		= $fetch_data['reply_to'];
		$campaign_data['subject'] 		= $fetch_data['template_name'];
		$campaign_data['html_text'] 	= $fetch_data['html_text'];
		$campaign_data['list_ids'] 		= $listdata['list_id'];
		$campaign_data['send_campaign'] = 1;
		
		echo $sendy->createAndSendCampaign($campaign_data);
	}
}