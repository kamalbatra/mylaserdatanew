<?php
class CreateListTemplate{
	protected $mysqli;
	protected $app_id;
	function __construct($app_id) {
		$this->mysqli = mysqli_connect("localhost", "root", "f6t4VKBjpDhKUw2w", "fadeaway-sendy");
		$this->app_id = $app_id;
	}
	
	public function create_list(){
		$query ="SELECT userID, name, unsubscribed_url, thankyou_message, goodbye_message, confirmation_email FROM lists WHERE app = 1";
		$result = mysqli_query($this->mysqli,$query) or die($query);
		while($fetch_data = mysqli_fetch_assoc($result)){
			$fetch_data['app'] = $this->app_id;
			$fetch_data['thankyou_message'] = mysqli_real_escape_string($this->mysqli, $fetch_data['thankyou_message']);
			$fetch_data['goodbye_message'] = mysqli_real_escape_string($this->mysqli, $fetch_data['goodbye_message']);
			$fetch_data['confirmation_email'] = mysqli_real_escape_string($this->mysqli, $fetch_data['confirmation_email']);
			$insert_id = $this->insert_data('lists', $fetch_data);
			if($fetch_data['name'] == "Hair Removal Clients"){
				$this->create_autoresponder($insert_id, "Newly Treated Hair", "Hair");
			}
			if($fetch_data['name'] == "Tattoo Removal Clients"){
				$this->create_autoresponder($insert_id, "Newly Treated Tattoo", "Tattoo");
			}
		}
	}
	
	public function create_template(){
		$query ="SELECT userID, template_name, html_text FROM template WHERE app = 1";
		$result = mysqli_query($this->mysqli,$query);
		while($fetch_data = mysqli_fetch_assoc($result)){
			$fetch_data['app'] = $this->app_id;
			$fetch_data['html_text'] = mysqli_real_escape_string($this->mysqli, $fetch_data['html_text']);
			$this->insert_data('template', $fetch_data);
		}
	}
	
	private function create_autoresponder($list_id, $name, $like_title){
		$fetch_data['name'] = $name;
		$fetch_data['type'] = 1;
		$fetch_data['list'] = $list_id;
		$ares_id = $this->insert_data('ares', $fetch_data);
		$this->create_ares_email($ares_id, $like_title);
	}
	
	private function create_ares_email($ares_id, $like_title){
		$query_apps = "SELECT from_name, from_email, reply_to FROM apps WHERE id = $this->app_id";
		$result_apps = mysqli_query($this->mysqli,$query_apps);
		$fetch_apps_data = mysqli_fetch_assoc($result_apps);

		$query_email = "SELECT title, plain_text, html_text, query_string, time_condition FROM ares_emails WHERE title LIKE '%$like_title%'";
		$result_email = mysqli_query($this->mysqli,$query_email);
		$fetch_email_data = mysqli_fetch_assoc($result_email);
		$fetch_email_data['html_text'] = mysqli_real_escape_string($this->mysqli, $fetch_email_data['html_text']);
		$fetch_data = array_merge($fetch_apps_data,$fetch_email_data);
		$fetch_data['ares_id'] = $ares_id;
		$fetch_data['created'] = time();
		$fetch_data['wysiwyg'] = 1;
		$this->insert_data('ares_emails', $fetch_data);
	}
	
	private function insert_data($table, $data){
		$keys = array_keys($data);
		$value = array_values($data);
		$fields	= implode(",",$keys);
		$values	= "('".implode("','",$value)."')";			
		$SQL = "INSERT INTO ".$table." (".$fields.") values ".$values;
		$MYSQL = mysqli_query($this->mysqli,$SQL);
		return $this->mysqli->insert_id;
	}
	
	public function checkalreadyexist(){
		$query_list ="SELECT * FROM lists WHERE app = $this->app_id";
		$result_list = mysqli_query($this->mysqli,$query_list);
		$query_temp ="SELECT * WHERE app = $this->app_id";
		$result_temp = mysqli_query($this->mysqli,$query_temp);
		if(mysqli_num_rows($result_list) || mysqli_num_rows($result_temp)){
			return true;
		}else
			return false;
		
	}
}
?>
