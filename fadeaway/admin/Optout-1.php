<?php
	include("admin_includes/header.php");
	include("../includes/dbFunctions.php");
	if(isset($_GET["sub"]) && $_GET["sub"]=="yes") {
		$optout = new dbFunctions();
		$table = "tbl_business";
		$data["BusinessID"] = $_SESSION["BusinessID"];
		$data["Optout"] = "Yes";
		$optout->update_spot($table,$data);
	}
?>	
<div class="form-container" id="sub-msg-div">
	<h2 class="sub-msg"><font color="green">Your membership successfully withdrew from Fadeaway.</font></h2>
</div><!--End @form-container-->
</div><!--End @container-->	
<?php
	include('admin_includes/footer.php');
?>
