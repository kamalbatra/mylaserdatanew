<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include('../includes/dbFunctions.php');
	$mangeCat = new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"]) ) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
?>	
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<style>
		.right-margin-6 { margin-right: 6%; }
		.menu-checkbox{ float: left; width: 25%; }
		.addNewReport { float: right; }
		.formdonly {display:none;}
	</style>
	<script>
		jQuery(document).ready(function() {
			jQuery("#insertcategory").validate({
				errorClass: 'errorblocks',
				errorElement: 'div',
				rules: {
					categoryName: {
						required: true,
					},
					status: {
						required: true,
					}
				},	
				messages: {
					categoryName: {
						required: "Please enter category name",
					},
					status: {
						required: "Please select a status.",
					}
				},
				submitHandler: function(form) {
					$('.loadingOuter').show();
					var str = $("#insertcategory" ).serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result) {
							if(result == 0){
								$("#insertResult").show();
								$("#insertResult").html("<span style='color:green;'>Category inserted successfully.</span>");
								setTimeout(function() {
									location.href = 'categories'
								}, 1000);
							}
						}
					}); 
				}
			});					
		});
	</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="loadingOuter"><img src="../images/loader.svg"></div>
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Add Category</h1>
					</div>
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="categories" class="submit-btn"><button class="addnewbtn">All Categories</button></a>
						</div>
					</div>
					<div class="formcontentblock-ld">						
					<form action="" name="insertcategory" id="insertcategory" method="post">
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Category Name</label>
													<input class="text-input-field" type="text" name="categoryName" id="categoryName"/>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Status:</label>
													<select name="status" id="status" class="select-option">
														<option value="">Select a status</option>
														<option value="1">Active</option>
														<option value="0">De-Active</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="businessID" value="<?php echo $_SESSION[BusinessID]; ?>"/>
									<input type="hidden" name="dateAdded" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
									<input type="hidden" name="formname" value="addcategory"/>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="submit"  id="submitForm" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
											<div id="insertResult" class="u_mess" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
					</div>
					</div>				
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
