<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	if($_SESSION['loginuser'] != "sitesuperadmin"){
		?>
		<script>
			$(function(){
				window.location.replace("dashboard?msg=noper");
			});
		</script>
		<?php
		die;
	}
	$mnglocation = new dbFunctions();
	if( !in_array(7,$_SESSION["menuPermissions"]))	{ 
?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php 
	}
	$titleName = "Add New Plan";
	$submitButton = "Submit";
	if(isset($_POST['title']) && $_POST['title']!= NULL) {	
		$insert_tbl_name = "tbl_master_plans";
		if(isset($_GET['id'])) {
			$updatedData['id'] = $_GET['id'];
			$updatedData['title'] = $_POST['title'];
			$updatedData['days'] = $_POST['days'];
			$updatedData['amount'] = $_POST['amount'];
			$mnglocation->update_spot($insert_tbl_name,$updatedData);
			$msg = "update";
		}else{
			$_POST['created_date'] = date("Y-m-d H:i:s");
			$mnglocation->insert_data($insert_tbl_name,$_POST);
			$msg = "add";
		}
	?>
		<script>
			$(function() {
				window.location.replace("manage-plans?msg=<?php echo $msg; ?>");
			});
		</script>
	<?php
	}
	if(isset($_GET['id']) && $_GET['id'] != '')	{
		$titleName = "Edit Plan";
		$submitButton = "Update";
		$condition = "WHERE id = $_GET[id]";
		$EditPlanData = $mnglocation->selectTableSingleRow("tbl_master_plans", $condition);
	}
?>
	<script type="text/javascript">
	jQuery(document).ready(function() { 	
		jQuery("#AddNewPlan").validate({
			rules: {
				title: "required",              
				days: "required",              
				amount: "required",              
				title: {
					required: true,
				},
				days: {
					required: true,
					number: true,
				},
				amount: {
					required: true,
					number: true,
				},
			},                
			messages: {
				title: "Please enter title.",
				days: {
					required: "Please enter no. of days.",
					number: "Please enter a valid number."
				},
				amount: {
					required: "Please enter amount.",
					number: "Please enter a valid amount.",
				}
			}
		});
	});
	</script>
	<div class="form-container">
		<div class="heading-container">		
			<h1 class="heading empHead"><?php echo $titleName; ?></h1>
			<div class="addNew">
				<a class="empLinks" href="manage-plans" class="submit-btn">Plan Details </a>
			</div>
		</div>
		<div class="user-entry-block">			
			<form action="" name="ManageLocationFrom" id="AddNewPlan" method="post">	
				<div class="row">
					<div class="span6">
						<label id="" class="user-name">Title:</label>
						<input class="text-input-field" type="text" name="title" id="title" value="<?php echo isset($EditPlanData['title']) ? $EditPlanData['title'] : ''; ?>" <?php echo (isset($_GET['id']) && $_GET['id'] == 1) ? "readonly" : ''; ?> />
					</div>					
				</div><!--End @row-->
				<div class="row">
					<div class="span6">
						<label id="" class="user-name">No. of days:</label>
						<input class="text-input-field" type="text" name="days" id="days" value="<?php echo isset($EditPlanData['days']) ? $EditPlanData['days'] : ''; ?>"/>
					</div>					
				</div><!--End @row-->
			   <div class="row">
					<div class="span6">
						<label id="" class="user-name">Amount:</label>
						<input class="text-input-field" type="text" name="amount" id="amount" value="<?php echo isset($EditPlanData['title']) ? $EditPlanData['amount'] : ''; ?>" <?php echo (isset($_GET['id']) && $_GET['id'] == 1) ? "readonly" : ''; ?>/>
					</div>					
				</div><!--End @row-->
				<div class="row">
					<div class="span12">
					  <input type="submit" id="submitManageForm" value="<?php echo $submitButton; ?>" class="submit-btn">
					  <?php if(isset($return) && $return!= NULL){ echo "<span style='color:green;'>Information inserted successfully.</span>";}?>
					  <br/>
					</div>			
				</div><!--End @row-->
			</form>
		</div><!--End @user-entry-block-->
	</div><!-- End  @form-container--->
</div><!--End @container-->
<?php 
	include('admin_includes/footer.php');	
?>
