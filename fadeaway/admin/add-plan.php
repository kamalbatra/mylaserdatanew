<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	if($_SESSION['loginuser'] != "sitesuperadmin"){
		?>
		<script>
			$(function(){
				window.location.replace("dashboard?msg=noper");
			});
		</script>
		<?php
		die;
	}
	$mnglocation = new dbFunctions();
	if( !in_array(7,$_SESSION["menuPermissions"]))	{ 
?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php 
	}
	$titleName = "Add New Plan";
	$submitButton = "Submit";
	if(isset($_POST['title']) && $_POST['title']!= NULL) {	
		$insert_tbl_name = "tbl_master_plans";
		if(isset($_GET['id'])) {
			$updatedData['id'] = $_GET['id'];
			$updatedData['title'] = $_POST['title'];
			$updatedData['days'] = $_POST['days'];
			$updatedData['amount'] = $_POST['amount'];
			$mnglocation->update_spot($insert_tbl_name,$updatedData);
			$msg = "update";
		}else{
			$_POST['created_date'] = date("Y-m-d H:i:s");
			$mnglocation->insert_data($insert_tbl_name,$_POST);
			$msg = "add";
		}
	?>
		<script>
			$(function() {
				window.location.replace("manage-plans?msg=<?php echo $msg; ?>");
			});
		</script>
	<?php
	}
	if(isset($_GET['id']) && $_GET['id'] != '')	{
		$titleName = "Edit Plan";
		$submitButton = "Update";
		$condition = "WHERE id = $_GET[id]";
		$EditPlanData = $mnglocation->selectTableSingleRow("tbl_master_plans", $condition);
	}
?>
	<script type="text/javascript">
	jQuery(document).ready(function() { 	
		jQuery("#AddNewPlan").validate({
			errorClass: 'errorblocks',
			errorElement: 'div',
			rules: {
				title: "required",              
				days: "required",              
				amount: "required",              
				title: {
					required: true,
				},
				days: {
					required: true,
					number: true,
				},
				amount: {
					required: true,
					number: true,
				},
			},                
			messages: {
				title: "Please enter title.",
				days: {
					required: "Please enter no. of days.",
					number: "Please enter a valid number."
				},
				amount: {
					required: "Please enter amount.",
					number: "Please enter a valid amount.",
				}
			}
		});
	});
	</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0"><?php echo $titleName; ?></h1>
						<a class="empLinks" href="manage-plans" class="submit-btn">Plan Details </a>
					</div>
					<form action="" name="ManageLocationFrom" id="AddNewPlan" method="post">
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="" class="user-name">Title:</label>
													<input class="text-input-field" type="text" name="title" id="title" value="<?php echo isset($EditPlanData['title']) ? $EditPlanData['title'] : ''; ?>" <?php echo (isset($_GET['id']) && $_GET['id'] == 1) ? "readonly" : ''; ?> />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="" class="user-name">No. of days:</label>
													<input class="text-input-field" type="text" name="days" id="days" value="<?php echo isset($EditPlanData['days']) ? $EditPlanData['days'] : ''; ?>"/>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="" class="user-name">Amount:</label>
													<input class="text-input-field" type="text" name="amount" id="amount" value="<?php echo isset($EditPlanData['title']) ? $EditPlanData['amount'] : ''; ?>" <?php echo (isset($_GET['id']) && $_GET['id'] == 1) ? "readonly" : ''; ?>/>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="submit"  id="submitManageForm" value="<?php echo $submitButton; ?>" class="submit-btn nextbtn" style="float:left;"><?php echo $submitButton; ?></button>
					  						<?php if(isset($return) && $return!= NULL){ echo "<span style='color:green;'>Information inserted successfully.</span>";}?>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>