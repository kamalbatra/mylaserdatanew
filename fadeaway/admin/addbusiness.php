<?php
include("../includes/dbFunctions.php");
$business = new dbFunctions();
$btable="tbl_business";

$subtable = "tbl_subscription_history";
$plantable = "tbl_master_plans";	
$subscribe = new dbFunctions();

/******************** Generate Strong Password ********************/
function crypto_rand_secure($min, $max)
{
	$range = $max - $min;
	if ($range < 1) return $min; // not so random...
	$log = ceil(log($range, 2));
	$bytes = (int) ($log / 8) + 1; // length in bytes
	$bits = (int) $log + 1; // length in bits
	$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
	do {
		$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
		$rnd = $rnd & $filter; // discard irrelevant bits
	} while ($rnd >= $range);
	return $min + $rnd;
}

function getToken($length)
{
	$token = "";
	$codeAlphabet  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
	$codeAlphabet .= "!@#$%^&*";
	$codeAlphabet .= "0123456789";
	$max = strlen($codeAlphabet) - 1;
	for ($i=0; $i < $length; $i++) {
		$token .= $codeAlphabet[crypto_rand_secure(0, $max)];
	}
	return $token;
}
/****************** End Generate Strong Password ******************/

$logofile = "";
$valid_formats = array("jpg", "png", "gif", "jpeg","PNG","JPG","JPEG","GIF");
if(isset($_POST["updatedId"]))
	$businesscreds["BusinessID"]=$_POST["updatedId"];
if($_FILES["logofile"]["name"] != ""){
	$filename=$_FILES["logofile"]["name"];
	$ext=end((explode(".",$filename)));
	$logofile="businesslogo".time().".".$ext;
	if(!in_array($ext,$valid_formats)){
		header("location:newbusiness.php?msg=failed");
		die;
	}
	$businesscreds["Logo"]=$logofile;
}
if(isset($_POST["service"])) {
	$service = implode(",",$_POST["service"]);
}

$businesscreds["BusinessName"]=$_POST["bname"];
$businesscreds["serviceId"]=$service;
$businesscreds["Contact"]=$_POST["contact"];
$businesscreds["LiabilityRelease"]=$_POST["LiabilityRelease"];
$businesscreds["Address"]=$_POST["Address"];
$businesscreds["City"]=$_POST["City"];
$businesscreds["State"]=$_POST["State"];
$businesscreds["Country"]=$_POST["Country"];
$businesscreds["sendy_brand_id"]=$_POST["sendy_brand_id"];

if(isset($_POST["updatedId"])){
	/*************** Update Employee Details ***************/
	$empUpdateDetail['Emp_ID'] = $_POST['employee_updated_id'];
	$empUpdateDetail['First_Name'] = $_POST['First_Name'];
	$empUpdateDetail['Last_Name'] = $_POST['Last_Name'];
	$empUpdateDetail['Emp_email'] = $_POST['email'];
	$business->update_spot("tbl_employees",$empUpdateDetail);
	
	/*************** Update Business Details ***************/
	$business->update_spot($btable,$businesscreds);
	$path="Business-Uploads/".$_POST["updatedId"];
	$msg = "update";
	
	/**************UPDATE SUBSCRIPTION HISTORY**************/
	
	$subcols = "PlanID";
	$subcondition = "where BusinessID=".$_POST["updatedId"]." order by ID desc";
	$subdetails = $subscribe->selectTableSingleRow($subtable,$subcondition,$subcols);
	$_POST["select-plan"] = ($_POST["select-plan"] == 1) ? $_POST["trialplan"] : $_POST["select-plan"];
	if($subdetails[0] != $_POST["select-plan"]){
		$subcreds["BusinessID"] = $_POST["updatedId"];
		$subcreds["RenewalDate"] = date("Y-m-d H:i:s");
		$subcreds["PlanID"] = $_POST["select-plan"];
		$plancondition = "where id=".$_POST["select-plan"];
		$plandetails = $subscribe->selectTableSingleRow($plantable,$plancondition,$cols="*");
		//$subcreds["PaymentAmount"] = $plandetails["amount"];
		$subcreds["PaymentAmount"] = 0;
		$expiredate = strtotime("+".$plandetails["days"]." days", strtotime($subcreds["RenewalDate"]));
		$expiredate = date("Y-m-d H:i:s", $expiredate);
		$subcreds["ExpireDate"] = $expiredate;
		$subcreds["PaymentStatus"] = "Success";
		$subscribe->insert_data($subtable,$subcreds);
	}
} else {
	$businesscreds["created_date"]=date("Y-m-d H:i:s");
	$business->insert_data($btable,$businesscreds);
	$id=mysqli_insert_id($business->con);
	
	/******************** Create List And Template On Sendy********************/
//	include("../SendyPHP/CreateListTemplate.php");
	//$sendy = new CreateListTemplate($businesscreds["sendy_brand_id"]);
	//$response = $sendy->checkalreadyexist();
	/*if(!$response){
		$sendy->create_list();
		$sendy->create_template();
	}*/
	/****************** End Create List And Template On Sendy******************/
	
	$path="Business-Uploads/".$id;
	mkdir($path);
	chmod($path, 0777);
	$thumbpath="Business-Uploads/".$id."/thumbnail";
	mkdir($thumbpath);
	chmod($thumbpath, 0777);
	$resizepath="Business-Uploads/".$id."/resize";
	mkdir($resizepath);
	chmod($resizepath, 0777);
	$emptable="tbl_employees";
	
	/************** INSERT EMPLOYEE DETAIL **************/
	
	$empcreds["BusinessID"]=$id;
	$empcreds["Username"]=$_POST["username"];
	$empcreds["First_Name"]=$_POST["First_Name"];
	$empcreds["Last_Name"]=$_POST["Last_Name"];
	//$random=rand(100000, 999999);
	$random=getToken(10);
	$empcreds["Password"]=md5($random);
	$empcreds["Emp_email"]=$_POST["email"];
	$empcreds["Admin"]="TRUE";
	$empcreds["Technician"]="TRUE";
	$empcreds["Medicaldirector"]="Yes";
	$business->insert_data($emptable,$empcreds);
	
	/************* INSERT SUBSCRIPTION HISTORY **********/
	$_POST["select-plan"] = ($_POST["select-plan"] == 1) ? $_POST["trialplan"] : $_POST["select-plan"];
	$plancondition = "where id=".$_POST["select-plan"];
	$subcreds["BusinessID"] = $id;
	$subcreds["RenewalDate"] = date("Y-m-d H:i:s");
	$subcreds["PlanID"] = $_POST["select-plan"];
	$plandetails = $subscribe->selectTableSingleRow($plantable,$plancondition,$cols="*");
	//$subcreds["PaymentAmount"] = $plandetails["amount"];
	$subcreds["PaymentAmount"] = 0;
	$expiredate = strtotime("+".$plandetails["days"]." days", strtotime($subcreds["RenewalDate"]));
    $expiredate = date("Y-m-d H:i:s", $expiredate);
	$subcreds["ExpireDate"] = $expiredate;
	$subcreds["PaymentStatus"] = "Success";
	$subscribe->insert_data($subtable,$subcreds);
	if($_POST["select-plan"] == 1){
		include("../sendEmails.php");
		$useremail = new sendEmails();
		$useremail->sendBusinessDetails($_POST["First_Name"],$_POST["username"],$random,$_POST["email"]);
	} else if($_POST["select-plan"] == 2){
		include("../sendEmails.php");
		$useremail = new sendEmails();
		$useremail->sendBusinessDetailsWithLink($_POST["First_Name"],$_POST["username"],$random,$_POST["email"]);
	}
	/********************** End **********************/
	$msg = "add";
}
if($logofile != ""){
	include("admin_includes/image_resize.php");
	$max_width = 950;
	$max_height = 222;
	list($orig_width, $orig_height) = getimagesize($_FILES['logofile']['tmp_name']);
	$width = $orig_width;
	$height = $orig_height;
	
	if ($height > $max_height) {
		$width = ($max_height / $height) * $width;
		$height = $max_height;
	}

	# wider
	if ($width > $max_width) {
		$height = ($max_width / $width) * $height;
		$width = $max_width;
	}
	resize($width, $height, $path."/".$logofile);
	//move_uploaded_file($_FILES["logofile"]["tmp_name"],$path."/".$logofile);
}

header("location:managebusiness?msg=$msg");
?>
