			<!-- Footer -->
			<footer class="sticky-footer bg-black">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright Northeast Laser Tattoo Removal, LLC 2019</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->
		</div>
		<!-- End of Content Wrapper -->
	</div>
	<!-- End of Page Wrapper -->
	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="login.html">Logout</a>
				</div>
			</div>
		</div>
	</div>
	<?php
		$sql ="SELECT * FROM activity_logs WHERE businessID = ".$_SESSION['BusinessID']." ORDER BY created_on DESC LIMIT 10";
		$var = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);
		$result = mysqli_query($var, $sql);
		$activityLogs = array();
		while($data = mysqli_fetch_assoc($result)) {
			$activityLogs[] = $data;
		}
		//~ $activityLogs = mysqli_fetch_all($result, MYSQLI_ASSOC);
	?>
	<div class="latest-activities">
	   <div class="header">
			<span class="hamburger"></span>
			<h4>Latest Activity</h4>
			<i class="fas fa-times btn-close"></i>
		</div>
		<div class="list">
			<ul>
				<?php
					if(!empty($activityLogs)) {
						foreach ($activityLogs as $key => $value) { 
							$rowClass = ($key%2 == 0)?'blue':'green';// orange, yellow
							?>
							<li class="<?php echo $rowClass; ?>">
								<span class="date"><?php echo date('F d, Y', strtotime($value['created_on'])); ?></span>
								<p><?php echo $value['list_name'].' email sent to <a style="cursor:auto;" href="javascript:void(0);">'.ucwords($value['fname']).' '.ucwords($value['lname']).'</a>.'; ?></p>
							</li>
				<?php	}
					}
				?>
			</ul>
		</div>
	</div>	
	
	<!-- Core plugin JavaScript-->
	<script src="<?php echo $domain; ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="<?php echo $domain; ?>/js-new/sb-admin-2.min.js"></script>
	<!-- Page level plugins -->
	<!-- script src="<?php echo $domain; ?>/js-new/Chart.min.js"></script -->
	<!-- Page level custom scripts -->
	<!-- script src="<?php echo $domain; ?>/js-new/demo/chart-bar-demo.js"></script -->
	<script>
		$('.btn-close').click(function(){
			$('.latest-activities').removeClass('active');
		});
		$('.btn-latest-activities').click(function(){
			$('.latest-activities').addClass('active');
		});
	</script>
</body>
</html>

