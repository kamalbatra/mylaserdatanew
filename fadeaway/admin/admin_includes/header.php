<?php
	error_reporting(0);
	$session_lifetime = 3600 * 24 * 2; // 2 days
	session_set_cookie_params ($session_lifetime);
	//session_start();
	session_start();
	if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3600)) {
		// last request was more than 30 minutes ago
		//session_unset();     // unset $_SESSION variable for the run-time 
		//session_destroy();   // destroy session data in storage
	}
	$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
	$currentpage = $_SERVER['REQUEST_URI'];
	if(!isset($_SESSION['id']))
		header('location:index.php?msg=session_expired');
	if(isset($_SESSION["period"]) && $_SESSION["period"] == "false" && ($page_name != "subscription"))
		header('location:subscription');
	//error handler function
	function customError($errno, $errstr) {
		$fileLocation = getenv("DOCUMENT_ROOT") . "/fadeaway/error.log";
		$v = "Error: [$errno] $errstr, "." Date: ".date("Y-m-d H:i:s");
		@file_put_contents($fileLocation, print_r($v."\n", true), FILE_APPEND);
	}
	//set error handler
	set_error_handler("customError");
	define("SITE_URL","http://dexteroustechnologies.co.in/mylaserdata/fadeaway/");
	$domain=$_SERVER['DOMAIN'];
	if(strtolower($_SESSION['Medicaldirector'])=='yes') {
		$_SESSION['Usertype']="Admin";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
		<title>Dashboard</title>
		<!--link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'/-->
		<link href="<?php echo $domain; ?>/css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" language="javascript" src="<?php echo $domain; ?>/js/html5.js"></script>
		<!--script type="text/javascript"  language="javascript" src="<?php echo $domain; ?>/js/PIE.htc"></script-->
		<script language="JavaScript" type="text/javascript" src="<?php echo $domain; ?>/js/suggest.js"></script>
		<script src="<?php echo $domain; ?>/js/jquery.min.js"></script>
		<!--script type="text/javascript" src="<?php echo $domain; ?>/js/jquery-1.9.1.js" ></script-->
		<script type="text/javascript" src="<?php echo $domain; ?>/js/jquerytest.js" ></script>
		<script type="text/javascript" src="<?php echo $domain; ?>/js/jquery.validate.min.js" ></script>
		<script type="text/javascript" src="<?php echo $domain; ?>/js/pwdwidget.js" ></script>
		<script type="text/javascript" src="<?php echo $domain; ?>/js/custom.js" ></script>
		<script type="text/javascript" src="<?php echo $domain; ?>/js/idel.js" ></script>
		<!--script type="text/javascript" src="<?php echo $domain; ?>/js/maskinput.js" ></script-->
		<!--script src="http://jquery.bassistance.de/validate/additional-methods.js"></script-->
		<script src="<?php echo $domain; ?>/js/suggest.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/theme4.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/overlaypopup.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/pwdwidget.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/datepicker.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/jquery.calendars.picker.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/slicknav.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/popUp.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/custom.css" />
		<link rel="stylesheet" href="<?php echo $domain; ?>/admin/datepicker/jquery-ui.css" /> 
		<script src="<?php echo $domain; ?>/admin/datepicker/jquery-ui.js"></script>
		<script src="<?php echo $domain; ?>/admin/datepicker/script.js"></script> 
		<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script-->
		<script src="<?php echo $domain; ?>/js/jquery.slicknav.js"></script>
		<script type="text/javascript">
			var refreshSn = function () {
				var time = 600000; // 10 mins
				setTimeout(function(){
					$.ajax({
					   url: 'refresh_session.php',
					   cache: false,
					   complete: function () {refreshSn();}
					});
				},time);
			};
			refreshSn();
			$(document).idle({
				onIdle: function(){
					alert("Your session has been expired.");
					window.location.reload();
				},
				idle: 99999999
			});
			$(document).ready(function(){
				$('#menu').slicknav();
			});
		</script>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
					phone_number = phone_number.replace(/\s+/g, "");
					return this.optional(element) || phone_number.length > 9 &&
					phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
				}, "Please specify a valid phone number");	
				jQuery("#add_page_form").validate({
					rules: {
						FirstName: "required",
						LastName:"required",
						DOB: {
							required: true,
							//date:true,
						},
						//Age:"required",
						Occupation:"required",
						Address1:"required",
						City:"required",
						State:"required",
						Zip:"required",
						PhoneNumber:"required",
						ReferralSource:"required",
						Email:"required",				
						Email: {
							required: true,
							email: true,
						},
						Age: {
							required: true,
							number:true
						},
						Zip: {
							required: true,
							//number:true
						},
						PhoneNumber: {
							required: true,
							phoneUS: true
						},
					},
					messages: {
						FirstName: "Please enter First Name.",
						LastName:"Please enter Last Name.",
						DOB: {
							required: "Enter your date of birth !",
							accept:"Select from box!",
						},
						Occupation:"Please enter Occupation.",
						Address1:"Please enter Address1.",
						City:"Please enter City.",
						State:"Please enter State.",
						PhoneNumber:"Please enter Phone Number.",
						ReferralSource:"Please select an option.",
						Email: {
							required: "Please enter Email." ,
							email: "Please enter valid email address.",
						},
						/*	Age: {
						required: "Please enter Age.",
						number: "Please enter a valid Age."
						},*/
						Zip: {
							required: "Please enter Zip Code.",
							//number: "Please enter a valid zip code."
						},
						PhoneNumber: {
							required: "Please enter Phone Number.",
							number: "Please enter a valid Phone Number.",
							minlength: "Please enter ten digit Phone Number.",
							maxlength: "Please enter ten digit Phone Number."
						},
					}
				});
			});
		</script>
		
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("#insertFrom").validate({
					rules: {
						First_Name: "required",
						Admin: "required",
						Medicaldirector: "required",
						Location:"required",
						admin_co:"required",
						Last_Name:"required",
						Username:"required",
						Office_Addr:"required",
						Password:"required",
						Emp_email:"required",                
						Emp_email: {
							required: true,
							email: true,
						},	
					},	
					messages: {
						First_Name: "Please enter First Name.",
						Admin: "Please select an option.",
						Medicaldirector: "Please select an option.",
						Location: "Please select an option.",
						admin_co: "Please select an option.",
						Last_Name:"Please enter Last Name.",
						Username:"Please enter User Name.",
						Office_Addr:"Please enter Address.",
						Emp_email: {
							required: "Please enter Email." ,
							email: "Please enter valid email address.",
						},
						Password: {
							required: "Please enter Password.",
							minlength: "Please enter at least 5 digit Pssword.",
							
						},
					}
				});
				$('#submitForm').click(function() {
					if( $("#insertFrom").valid()){
						insertEmp_form();
					}else{
						$("#insertResult").hide();		
					}			
				});		
				$('#submitFormLegal').click(function() {
					if( $("#submitFormLegal").valid()){
						insertlegal_form();
					}else{
						$("#insertResult").hide();		
					}			
				});		   
			});
		</script>
		
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("#insertFromEdit").validate({
					rules: {
						First_Name: "required",
						Admin: "required",
						Medicaldirector: "required",
						Location:"required",
						admin_co:"required",
						Last_Name:"required",
						Username:"required",
						Office_Addr:"required",
						Password:"required",
						Emp_email:"required",                						
						Emp_email: {
							required: true,
							email: true,
						},							
					},	
					messages: {
						First_Name: "Please enter First Name.",
						Admin: "Please select an option.",
						Medicaldirector: "Please select an option.",
						Location: "Please select an option.",
						admin_co: "Please select an option.",
						Last_Name:"Please enter Last Name.",
						Username:"Please enter User Name.",
						Office_Addr:"Please enter Address.",
						Emp_email: {
							required: "Please enter email." ,
							email: "Please enter valid email address.",
						},
						Password: {
							required: "Please enter Password.",
							minlength: "Please enter at least 5 digit Pssword.",						
						},					
					}
				});
				
				$('#submitFormEdit').click(function() {
					if( $("#insertFromEdit").valid()){
						//alert("ok");
						//insertEmp_formEdit();				
						var str = $("#insertFromEdit").serialize();
						$.ajax({
							type: "POST",
							url: "ajax_insertformEdit.php",
							data: str,
							cache: false,
							success: function(result){
								if(result ==1)	{
									$("#emailExistEdit").show();
									$("#emailExistEdit").html("This Email Id Already Exist!");
									$("#insertResultEdit").hide();
								}
								else {
									$("#emailExistEdit").hide();
									$("#insertResultEdit").show();							
									$("#insertResultEdit").html(result);
									setTimeout(function() {location.reload()	}, 5000);
								}
							}
						});
					}
					else {
						$("#insertResultEdit").hide();		
					}
				}); 
				
				$('#updProtocol').on( 'change', function() {
					$('.fileupProto').hide();
					myfile= $( this ).val();
					var ext = myfile.split('.').pop();
					if(ext!="pdf"){
						$('.fileupProto').html('Please upload only pdf file.');
						$('.fileupProto').show();
					}
				});
				
				$('#submitFormPro').click(function() {
					myfile= $( "#updProtocol" ).val();
					var ext = myfile.split('.').pop();
					if(ext=="pdf"){
						$( "#manageProtocol" ).submit();
					} else {
						$('.fileupProto').show();
						return false;
					}
				});	
				
			});
		</script>
		<script>
			<!--***** Manage  Employee form from manage-employee.php */
			function insertEmp_form()	{
				var str = $("#insertFrom" ).serialize();
				$.ajax({
					type: "POST",
					url: "ajax_insertform.php",
					data: str,
					cache: false,
					success: function(result){
						if(result ==1){
							$("#emailExist").html("This email id already exist!").show();
							$("#usernameExist").hide();
						}
						if(result ==2){
							$("#usernameExist").html("User name already exist!").show();
							$("#emailExist").hide();
						}
						if(result ==12){
							$("#usernameExist").html("User name already exist!").show();
							$("#emailExist").html("This email id already exist!").show();
						}
						if(result ==0){
							$("#insertResult").show();
							$("#insertResult").html("<span style='color:green;'>Information inserted successfully.</span>");
							setTimeout(function() {location.reload()	}, 5000);
						}
					}
				});					
			}
			function insertlegal_form()	{
				var notice = tinymce.get('notice').getContent();
				var risks = tinymce.get('risks').getContent();
				var legal = tinymce.get('legal').getContent();
				if(notice == '' || risks == '' || legal == '') {
					$("#insertResult").show();
					$("#insertResult").html("<span style='color:green;'>All fields are required.</span>");
					setTimeout(function() { $("#insertResult").hide();	}, 2000);
				} else {
					$('#notice').html(notice);
					$('#risks').html(risks);
					$('#legal').html(legal);
					var str = $("#insertLegalForm" ).serialize();
					$.ajax({
						type: "POST",
						url: "ajax_insertLegalform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result ==0){
								$("#insertResult").show();
								$("#insertResult").html("<span style='color:green;'>Legal Disclaimer updated successfully.</span>");
								setTimeout(function() { window.location.href = 'listdisclaimer';	}, 2000);
							}
						}
					});
				}
			}

			function aa(id) {
				//alert(id);
				var dataString = 'id='+ id;
				$.ajax ({
					type: "POST",
					url: "edit_details.php",
					data: dataString,
					cache: false,
					success: function(html) {
						$("#mydiv2").show();
						$("#mydiv2").html(html);
					}
				});
				return true; 
			}
		
			function get_hair_price1(){
				var Size = $("select[name='Size']").find('option:selected').text();
				var Area = $("select[name='Area']").find('option:selected').text();
				var location = $("input[name='Location']").val();
				if(Size != '' && Size != '-Select Size-' && Area != '' && Area != '-Select Area-'){
					$("#hairsizeandarea").val(Size+' '+Area);
					$.ajax({
						url: 'ajax_pricing.php', 
						data:{Size, Area, 'Service' : '2', location},
						type:"POST",
						success:function(res){
							$("#hairpricesdiv").html(res);
						}
					});
				}
			}

			function get_selected_hair_price(){
				var Size = $("#SelectedSize").val();
				var Area = $("#SelectedArea").val();
				var location = $("input[name='Location']").val();
				if(Size != '' && Size != '-Select Size-' && Area != '' && Area != '-Select Area-'){
					$("#hairsizeandarea").val(Size+' '+Area);
					$.ajax({
						url: 'ajax_pricing.php', 
						data:{Size, Area, 'Service' : '2', location},
						type:"POST",
						success:function(res){
							$("#hairpricesdiv").html(res);
						}
					});
				}
			}
				
			$(document).ready(function() {
				$('#dbTxt').change(function() {
					$('.client-info').show();	
				});  
				// Show hide panel	   
				$("body").on('click','#showPanel',function() {
					$("#detailpanel").slideDown("slow");
					$("#showPanel").hide();
					$("#hidePanel").show();
				});
				
				$("body").on('click','#hidePanel',function() {
					$("#detailpanel").slideUp("slow");
					$("#hidePanel").hide();
					$("#showPanel").show();
				});
				 
				$('#reviewClient').click(function() {
					$("#mydiv2").show();	
					$('#mydiv2').load('process.php', {
						'clientname': $('input[name="dbTxt"]').val(),'clientnameid': $('input[id="clientid"]').val()
					});                    
					return false;
				});
				
				$('#treatClient').click(function()	{
					var clid = $('input[id="clientid"]').val();
					if(clid !="") {
						$("#mydiv2").show();
						$('#mydiv2').load('process.php', { 
							'clientnameid': $('input[id="clientid"]').val(),'firstname': $('input[id="clientfname"]').val(),'lastname': $('input[id="clientlname"]').val(),'serviceId': $(this).attr("rel")
						});   
					}              
					return false;
				});
				
				$('#treatClientHair').click(function()	{
					var clid = $('input[id="clientid"]').val();
					if(clid !="") {
						$("#mydiv2").show();
						$('#mydiv2').load('processhair.php', {
							'clientnameid': $('input[id="clientid"]').val(),'firstname': $('input[id="clientfname"]').val(),'lastname': $('input[id="clientlname"]').val(),'serviceId': $(this).attr("rel")
						});   
					}                 
					return false;
				});
				
				//GET newTatto under treat this client
				// check if tatoo name exist
				$("#TattoNumber").live('keyup',function() {
					var str = 'TattoNumber='+$("#TattoNumber").val()+'&ClientID='+$("#ClientID").val();
					$.ajax({
						type: "POST",
						url: "ajax_tattoExist.php",
						data: str,
						cache: false,
						success: function(result) {
							$("#alreadyExist").val(result);
							if(result==1) {
								$("#TattoNumberError").html("Tatto name already exist!").show();
							}else {
								$("#TattoNumberError1").html("").hide();
								$("#TattoNumberError").html("").hide();
							}
						}
					});
				});
				
				$("select[name='Size']").live('change',function() {
					$("#TattoNumber").val($("select[name='Size']").find('option:selected').text()+'-'+$("select[name='Area']").find('option:selected').text());
					get_hair_price1();
				});
				
				$("select[name='Area']").live('change',function() { 
					$("#TattoNumber").val($("select[name='Size']").find('option:selected').text()+'-'+$("select[name='Area']").find('option:selected').text());
					get_hair_price1();
				});
				
				//end  check if tatto name exist
				$("#existTatto").live('change',function() {
					if($('#existTatto').val() == "select"){
						$("#loadingTatto").show();
						var d = $("#ClientID").val();					
						var d ='clientnameid='+$("#ClientID").val()+'&firstname='+$("#firstname").val()+'&lastname='+$("#lastname").val()+'&serviceId='+$("#serviceId").val();$.ajax({
							type: "POST",
							url: "process.php",
							data: d,
							cache: false,
							success: function(html){	
								$('#mydiv2').html(html)	; 
							}
						});  
					}else{
						$("#loadingTatto").show();
						$("#TattoNumber").val("");
						var data = 'TattoNumber='+$('#existTatto').val()+'&ClientID='+$("#ClientID").val();
						$.ajax({
							type: "POST",
							url: "ajax_tattoSession.php",
							data: data,
							cache: false,
							success: function(data1) {
								var html = data1.split(",");															
								var dataString = 'SessionNumber='+html[0]+'&clientnameid='+$("#ClientID").val()+'&TattoNumber='+$('#existTatto').val()+'&firstname='+$("#firstname").val()+'&lastname='+$("#lastname").val()+'&Size='+html[1]+'&serviceId='+$("#serviceId").val();	
								$.ajax({
									type: "POST",
									url: "process.php",
									data: dataString,
									cache: false,
									success: function(html) {										
										$('#mydiv2').html(html)	;  
										$("#loadingTatto").hide(); 
									}
								});
							}
						});
					}
				}); 
				//@ end GET newTatto under treat this client
				$("#existTattoHair").live('change',function() {
					if($('#existTattoHair').val() == "select"){
						$("#loadingTatto").show();
						var d = $("#ClientID").val();					
						var d ='clientnameid='+$("#ClientID").val()+'&firstname='+$("#firstname").val()+'&lastname='+$("#lastname").val()+'&serviceId='+$("#serviceId").val();
						$.ajax({
							type: "POST",
							url: "processhair.php",
							data: d,
							cache: false,
							success: function(html){	
								$('#mydiv2').html(html)	; 
								// $("#loadingTatto").hide();  
							}
						});  
					} else {
						$("#loadingTatto").show();
						$("#TattoNumber").val("");
						var data = 'TattoNumber='+$('#existTattoHair').val()+'&ClientID='+$("#ClientID").val();
						$.ajax({
							type: "POST",
							url: "ajax_tattoSession.php",
							data: data,
							cache: false,
							success: function(data1) {
								var html = data1.split(",");															
								var dataString = 'SessionNumber='+html[0]+'&clientnameid='+$("#ClientID").val()+'&TattoNumber='+$('#existTattoHair').val()+'&firstname='+$("#firstname").val()+'&lastname='+$("#lastname").val()+'&Size='+html[1]+'&Area='+html[2]+'&HairType='+html[3]+'&HairColour='+html[4]+'&TattooInfo='+html[5]+'&serviceId='+$("#serviceId").val();	
								$.ajax({
									type: "POST",
									url: "processhair.php",
									data: dataString,
									cache: false,
									success: function(html) {										
										$('#mydiv2').html(html)	;  
										$("#loadingTatto").hide(); 
									}
								});
							}
						});
					}
				}); 
				//@ end GET new Hair under treat this client
			});
				
			function edit_form(id) {
				var str = $( "#edit_details" ).serialize();
				$.ajax({
					type: "POST",
					url: "edit_ajax.php",
					data: str,
					cache: false,
					success: function(result)	{
						$("#u_mess").html(result);
					}
				});
			}
				
			function treat_client() {
				var requiredNumeric = $("#DeviceName option:selected").attr("id")				
				var TattoNumber = $("#TattoNumber" ).val();
				var existTatto = $("#existTatto" ).val();
				//alert(existTatto);				
				var check="";			
				if(TattoNumber =="" && existTatto=="select") {
					check = 1;
					$("#TattoNumberError").html("Please enter tatto name!").show();
					$("#TattoNumber").focus();				
				} else {
					check =0;
					$("#TattoNumberError").html("").hide();
				}
				if($("#alreadyExist").val() == 1  || $("#alreadyExist").val()==""){
					$("#TattoNumberError1").html("Tatto name already exist!").show();
				}
				if($("#alreadyExist").val() == 0) {
					$("#TattoNumberError1").html("").show();
				}
				//alert(requiredNumeric);
				var $textboxes = $('input[name="Fluence[]"]')			
				var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;			
				var arr = $('.flclass').map(function(i, e) {
					if(requiredNumeric =="Yes"){
						if(!numberRegex.test(e.value)){
							//return e.value; 
							//alert("ee:-"+e.id);				 
							$("#"+e.id).css("border","1px solid red");	
							$("#"+e.id).focus();
							$("#"+e.id).addClass('placeholderInt');
							$("#"+e.id).attr("placeholder", "Enter numeric value only");			  
							return e.id; 
						} else {
							$("#"+e.id).css("border","1px solid #ccc");	
						}
					} else {
						$("#"+e.id).val("Unable to record");
						//return e.id;
					} 
				}).toArray();
				var previewimage = $(".preview").val();
				if(previewimage != '')	{
					$("#photoimgError").html("Please upload a photo!").show();
					$("#photoimg").focus();
					return false;
				} else if(previewimage == '') {
					$("#photoimgError").html("Please upload a photo!").hide();
				}
				var strForm = $("#treatmentForm").serialize();			
				//alert(arr.length);
				//alert(check);	
				var alreadyExist =  $("#alreadyExist").val();
				if(arr.length ==0 && check ==0 && alreadyExist ==0){
					if(typeof existTatto === 'undefined' && TattoNumber =="") {
						$("#TattoNumberError").html("Please enter tatto name!").show();
						$("#TattoNumber").focus();
					} else {
						$("#TattoNumberError").html("").hide();
						var  err = "";
						if($("#Size").val() =="") {
							err= 1;
							$("#Size").css("border","1px solid red");	
							$("#Size").focus();						
							$("#Size").attr("placeholder", "Enter numeric value only");	
						}
						if($("#Price").val() =="") {
							err=1;
							$("#Price").css("border","1px solid red");	
							$("#Price").focus();						
							$("#Price").attr("placeholder", "Enter numeric value only");	
						}
						if(err==0) {
							$("#Size").css("border","1px solid #ccc");	
							$("#Price").css("border","1px solid #ccc");	
							$.ajax({
								type: "POST",
								url: "treat_client.php",
								data: strForm,
								cache: false,
								success: function(result){
									$("#u_mess").html(result);
								}
							});
						}
					}
				}// if check error 
			}
			/******************************* Functionality For Hair Treatment *******************************/
			function hair_treat_client() {
				var requiredNumeric = $("#DeviceName option:selected").attr("id")				
				var TattoNumber = $("#TattoNumber" ).val();
				var existTatto = $("#existTattoHair" ).val();
				var check="";			
				if(TattoNumber =="" && existTatto=="select") {
					check = 1;
					$("#TattoNumberError").html("Please enter tatto name!").show();
					$("#TattoNumber").focus();				
				} else {
					check =0;
					$("#TattoNumberError").html("").hide();
				}
				if($("#alreadyExist").val() == 1  || $("#alreadyExist").val()==""){			
					$("#TattoNumberError1").html("Tatto name already exist!").show();
				}
				if($("#alreadyExist").val() == 0){
					$("#TattoNumberError1").html("").show();
				}
				$("input,select").css({'border':'1px solid #ccc'});
				var $textboxes = $('input[name="Fluence[]"]')			
				var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;			
				var arr = $('.flclass').map(function(i, e) {
					if(requiredNumeric =="Yes"){
						if(!numberRegex.test(e.value)){
							$("#"+e.id).css("border","1px solid red");	
							$("#"+e.id).focus();
							$("#"+e.id).addClass('placeholderInt');
							$("#"+e.id).attr("placeholder", "Enter numeric value only");			  
							return e.id; 
						} else {
							$("#"+e.id).css("border","1px solid #ccc");	
						}
					} else {
						$("#"+e.id).val("Unable to record");
						//return e.id;
					}
				}).toArray();
				if($("#Size").val() =="0") {
					err= 1;
					$("#Size").css("border","1px solid red");	
					$("#Size").focus();						
				}
				if($("#Area").val() =="0") {
					err= 1;
					$("#Area").css("border","1px solid red");	
					$("#Area").focus();						
				}
				var strForm = $("#treatmentForm").serialize();			
				var alreadyExist =  $("#alreadyExist").val();
				if(arr.length ==0 && check ==0 && alreadyExist ==0) {
					if(typeof existTatto === 'undefined' && TattoNumber =="") {
						$("#TattoNumberError").html("Please enter tatto name!").show();
						$("#TattoNumber").focus();
					} else {
						$("#TattoNumberError").html("").hide();
						var  err = "";
						if($("#Size").val() =="0") {
							err= 1;
							$("#Size").css("border","1px solid red");	
							$("#Size").focus();						
						}
						if($("#Area").val() =="0") {
							err= 1;
							$("#Area").css("border","1px solid red");	
							$("#Area").focus();						
						}
						if($("#Price").val() =="") {
							err=1;
							$("#Price").css("border","1px solid red");	
							$("#Price").focus();						
							$("#Price").attr("placeholder", "Enter numeric value only");	
						}
						if(err==0) {
							$("#Size").css("border","1px solid #ccc");	
							$("#Price").css("border","1px solid #ccc");	
							$.ajax({
								type: "POST",
								url: "hair_treat_client.php",
								data: strForm,
								cache: false,
								success: function(result){
									$("#u_mess").html(result);
								}
							});
						}
					}
				}
			}
			/***************************** End Functionality For Hair Treatment *****************************/
			// Add additional note by docor technician
			function edit_formConsentAdd(id) {
				//var TechnicianReview =$("#TechnicianReview").val();
				if($("#TechNotes").val() !="") {
					$("#TechNotesError").hide();
					if ($('#TechnicianReview').is(":checked")) {
						var str = $( "#edit_ConsetDetails" ).serialize();
						$.ajax({
							type: "POST",
							url: "edit_consetAdd.php",
							data: str,
							cache: false,
							success: function(result) {
								$("#conset_massage").html(result);							
								setTimeout(function() {
									$('#mydiv2').load('process.php', {
										'clientname': $('input[name="dbTxt"]').val(),'clientnameid': $('input[id="clientid"]').val()
									});                    
									return false;
								}, 3000);
							}
						});
					} else {	
						$("#TechnicianReviewError").html("Please select review process box!");
					}
				} else {
					$("#TechNotesError").show();
					$("#TechNotesError").html("This is required field!");
				}		
			}	
			// Add additional note by docor
			function edit_docformConsentAdd(id){
				var txtVal= $("#MedicalDirectorNotes").val();		
				if(txtVal !=""){
					$("#errMsg").hide();
					var str = $( "#edit_DocDetails" ).serialize();
					$.ajax({
						type: "POST",
						url: "edit_DocDetails.php",
						data: str,
						cache: false,
						success: function(result){
							$("#conset_Docmassage").html(result);
							setTimeout(function() {
								$('#mydiv2').load('process.php', { 'clientname': $('input[name="dbTxt"]').val(),'clientnameid': $('input[id="clientid"]').val()
								});                    
								return false;
							}, 3000);
						}
					});		
				} else {
					$("#errMsg").show();
					$("#errMsg").html("This is required field!");
				}	
			}
		</script>
		<script type="text/javascript">
			function isNumberKey(evt)	{
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			}
		</script>
		<!-- --- ADD text and remove text box from fullinfo.php -->
		<script type="text/javascript">
			jQuery(document).ready( function () {
				var counter1 = 2;
				$("#append").click( function() {
					$("#textBoxDiv").show();
					if(counter1>10){
						// alert("Only 10 textboxes allow");
						return false;	
					}
					$("#addTxtbox").append('<div class="boxx"><a href="#" class="remove_this btn btn-danger">.</a> <input type="text" class="text-input-field medicationautocmp" name="CurrentMeds'+counter1 +'" id="CurrentMeds'+counter1 +'" autocomplete="off"> <input type="hidden" class="text-input-field" id="CurrentMedsId'+counter1 +'"/></div>');
					function split(val) {
						return val.split( /,\s*/ );
					}
					function extractLast(term) {
						return split( term ).pop();
					}
					$( "#CurrentMeds"+counter1 )
						.bind( "keydown", function( event ) {
							if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete( "instance" ).menu.active ) {
								event.preventDefault();
							}
						})
						.autocomplete({
							source: function( request, response ) {
								$.getJSON( "search_meds.php", {
									term: extractLast( request.term )
								}, response );
							},
							search: function() {
							  // custom minLength
								var term = extractLast( this.value );
								if ( term.length < 2 ) {
									return false;
								}
							},
							focus: function() {
							  // prevent value inserted on focus
							  return false;
							},
							select: function( event, ui ) {
								this.value = ui.item.value;
								return false;
							}
						});
						counter1++;
						return false;
				});
				
				$('.remove_this').live('click', function() {
					if(counter1==1){
						return false;
					}
					counter1--;
					jQuery(this).parent().remove();
					return false;
				});
				
				$('.protocolTab').hover(function () {
					$('.submenu').show();
				});
				
				$('.protocolTab').mouseleave(function(){
					$('.submenu').hide();
				});
				
				if($('.aclass').parent().hasClass('slicknav_item')) {
					$('.aclass').parent().removeClass('slicknav_item').addClass('protocolTabula');
				}
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				var str1 = 'review=header';
				$.ajax({
					type: "POST",
					url: "panddingconsent.php",
					data: str1,
					cache: false,
					success: function(result)	{					
						$("#totalPandingReview").html(result);					
					}
				});
			});
		</script>
	<!-- Active Class  menu  -->
		<script>
			$(function() {
				var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
				$(".navigation ul li a").each(function(){
					if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
					$(this).addClass("active");
				})
			});
		</script>
		<!--open popup for pending review on dashbord.php file-->
		<!--related file overlaypop.css-->
		<?php //if($_SESSION['pendingReview']>0){?>
		<?php
		if($_SESSION['protocol_status']==1) {
			$protocol_name='href="protocols"';
		} else {
			$protocol_name='';
		}
		if($_SESSION['logintype']!="client") {
		?>
			<script>
				$(document).ready(function(){
					// show popup when you click on the link
					//$('.show-popup').click(function(event){
					//event.preventDefault(); // disable normal link function so that it doesn't refresh the page
					$('.overlay-bg').show(); //display your popup
					setTimeout(function() {
						var str1 = 'review=dashbord';
						$.ajax({
							type: "POST",
							url: "panddingconsent.php",
							data: str1,
							cache: false,
							success: function(result)	{					
								$("#totalPandingReviewDashbord").html(result);					
							}
						});
						return false;
					}, 1000);
					// });
					// hide popup when user clicks on close button
					$('.close-btn').live('click',function() {
						$('.overlay-bg').hide(); // hide the overlay
					});
					
					$('.close-link').click(function() {
						$('.overlay-bg').hide(); // hide the overlay
						$(location).attr('href', 'consentforms.php');
					});
					
					// hides the popup if user clicks anywhere outside the container
					$('.overlay-bg').click(function() {
						$('.overlay-bg').hide();
					});
					
					// prevents the overlay from closing if user clicks inside the popup overlay
					$('.overlay-content').click(function(){
						return false;
					});
				});
			</script>
		<?php 
		}
		?>
		<style>
			a.backtodash {	color:#000;	}
			a.backtodash:hover {	text-decoration: underline;	color:#11719f;	}
			.no-script {margin-bottom: -7px !important;padding-top: 20px;text-align: center;font-size:14px}
			.protocolTab span { display: none !important; }
		</style>
		<!--@end open popup for pending review -->
	</head>
	<body>
		<?php //echo $_SESSION['Medicaldirector'];?>	
		<?php //echo "Emp-location :-".$EmpLocation = $_SESSION['Location'];?>
		<div class="header-top-bar"></div>
		<noscript><div class="no-script"><b>Warning:</b> Javascript must be enabled to use this application correctly. Here are the <a href="http://www.enable-javascript.com" target="_blank"> instructions how to enable JavaScript in your web browser</a></div></noscript>
		<div class="container">
			<div class="admin-login">  
				<h6>
				<?php
					if($_SESSION['Superadmin']!="1" && $_SESSION["period"]=="true" && $_SESSION['logintype']!="client")	{  ?> 
						<span id="totalPandingReview" style=""> </span>
					<?php
					}
					if($_SESSION['Superadmin']=="0" && $_SESSION['Usertype']=="Admin") { ?>
						<a href="profile?id=<?php echo $_SESSION['BusinessID']; ?>" title="View Profile">
							<span class="user-icon">Welcome 
							<?php /* if($_SESSION['Superadmin']=="1"){ ?>
									SuperAdmin 
								<?php } else if($_SESSION['Usertype']=="Admin"){ ?>
									Admin 
								<?php } else { ?>
									Employee 
								<?php }*/ 
							echo $_SESSION['First_Name']." ".$_SESSION['Last_Name'];
							?>
							</span>
						</a>
					<?php
					} else { ?>
						<span class="user-icon">Welcome 
							<?php /*if($_SESSION['Superadmin']=="1"){ ?>
								SuperAdmin 
							<?php } else if($_SESSION['Usertype']=="Admin"){ ?>
								Admin 
							<?php } else { ?>
								Employee 
							<?php }*/ 
							echo $_SESSION['First_Name']." ".$_SESSION['Last_Name'];?>
						</span> 
					<?php 
					} ?>| <a href="<?php echo SITE_URL; ?>includes/login.php?logout=true"><span class="signout">Signout</span></a>
				</h6>
			</div>
			<div class="logo">
				<?php 
				if($_SESSION["period"] == "true"){ ?>
					<a href="dashboard">
				<?php 
				}
					if(isset($_SESSION['businessdata']['Logo']) && $_SESSION['businessdata']['Logo'] != "" && file_exists("Business-Uploads/$_SESSION[BusinessID]/".$_SESSION['businessdata']['Logo'])) { 
				?>
						<img alt="logo" src="<?php echo $domain; ?>/Business-Uploads/<?php echo $_SESSION['BusinessID'].'/'.$_SESSION['businessdata']['Logo']; ?>" />
				<?php
					} else {
				?>
						<img alt="logo" src="<?php echo $domain; ?>/images/logo.jpg" />
				<?php 
					}	
				if($_SESSION["period"] != "false"){ ?>
					</a>
				 <?php 
				} ?>
			</div>
			<?php 
			$clss_add="";
			if( in_array(14,$_SESSION["menuPermissions"])){
				$clss_add="protocolClass";
			}
			if($_SESSION['Superadmin'] == 0 && isset($_SESSION['loginuser']) && $_SESSION['loginuser'] == "sitesuperadmin") { ?>
				<div style="clear:both;float:right;margin-bottom:7px" class="row">
					<div style="float:right">&nbsp;|&nbsp;<a href="viewbusiness" class="backtodash">Back to dashboard</a></div>
					<div style="float:right;">Logged in as <?php echo $_SESSION['Loggedinas']; ?></div>
				</div>
			<?php 
			} 
			if($currentpage!='/nelasertattooremoval/admin/dashboard') {
				// if(isset($_COOKIE["dashbord"])) {  ?>
				<div id="mobmenu"></div>
				<?php 
				if(isset($_SESSION["Superadmin"]) && $_SESSION["Superadmin"]==1){ ?>
					<div class="navigation">
						<ul id="menu">
							<li><span></span> <a href="newbusiness" title="Add New Business">Add New Business</a></li>
							<li><span></span> <a href="managebusiness" title="All Businesses">All Businesses</a></li>
							<li><span></span> <a href="emailtemplates" title="Manage Email Templates">Manage Email Templates</a> </li>
							<li><span></span> <a href="manage-plans" title="Manage Plans">Manage Plans</a> </li>
							<li><span></span> <a href="all_transactions" title="Transactions History">Transactions History</a></li>
							<li><span></span> <a href="superadmin-report" title="Reports">Reports</a> </li>
							<li><span></span> <a href="clientlocationdetail" title="Reports">Export Clients</a></li>					
						</ul>
					</div>
				<?php 
				} else if($_SESSION["period"]=="true") { ?>
					<div class="navigation <?php echo $clss_add; ?>">
						<ul id="menu">
							<?php 
							if( in_array(1,$_SESSION["menuPermissions"])){  ?>
								<li><span></span><a href="user-entry" title="New Client">New Client</a></li>
							<?php 
							} 
							if($_SESSION['logintype']!="client"){
								if( in_array(2,$_SESSION["menuPermissions"])){  ?>
									<li><span></span> <a href="treatment" title="Client Lookup">Client Lookup</a></li> 
								<?php 
								}
								if( in_array(3,$_SESSION["menuPermissions"])){ ?>
									<li><span></span> <a href="pricing" title="Pricing Calculator">Pricing Calculator</a></li>
								<?php 
								}
								if( in_array(4,$_SESSION["menuPermissions"])){  ?>
									<li><span></span> <a href="consentforms" title="Consent Form Review">Consent Form Review </a></li>
								<?php 
								}
								if( in_array(5,$_SESSION["menuPermissions"])){  ?>
									<li><span></span> <a href="clientdetails" title="Manage Clients">Manage Clients</a> </li>
								<?php 
								}
								if( in_array(6,$_SESSION["menuPermissions"])){  ?>
									<li><span></span> <a href="empdetails" title="Manage Employees">Manage Employees</a> </li>	
								<?php 			
								}
								if( in_array(7,$_SESSION["menuPermissions"])){
									// if($_SESSION['loginuser'] == "sitesuperadmin") { 
									if($_SESSION['loginuser'] == "sitesuperadmin" || $_SESSION["Usertype"] == 'Admin') { ?>		
										<li><span></span><a href="manage_location_details" title="Manage Locations">Manage Locations </a></li>
									<?php 
									}
								}
								if( in_array(8,$_SESSION["menuPermissions"])){ ?>
									<li><span></span> <a href="devicedetails" title="Manage Devices">Manage Devices</a> </li>
								<?php 
								}
								if( in_array(9,$_SESSION["menuPermissions"])){ ?>
									<li><span></span><a title="Manage Pricing" href="<?php if(in_array("1",$_SESSION["services"])) echo 'pricingdetails'; else if(in_array("2",$_SESSION["services"])) echo 'pricingdetailshair';?>">Manage Pricing</a> </li>
								<?php 
								} 
								if( in_array(10,$_SESSION["menuPermissions"])){  ?>
									<li><span></span><a title="Transactions History" href="trans_history">Transactions History</a></li>
								<?php 
								}
								if( in_array(11,$_SESSION["menuPermissions"])){ ?>
									<li><span></span><a href="your_sub" title="Subscription Status">Subscription Status</a></li>
								<?php 
								}
								if( in_array(12,$_SESSION["menuPermissions"])){ 
									if($_SESSION['loginuser'] != "sitesuperadmin") { ?>
										<li><span></span><a href="lapsed-client-status" title="Client Status Update">Client Status Update</a></li>
									<?php 
									}
								}
								if( in_array(13,$_SESSION["menuPermissions"])){  ?>
									<li><span></span><a href="clientreport" title="Reports">Reports</a> </li>
								<?php 
								} ?>
								<li class="<?php if( in_array(14,$_SESSION["menuPermissions"])){ echo "protocolTab"; } ?>"><span></span> <a title="Protocols" class="aclass" target="_blank" <?php echo $protocol_name; ?>>Protocols</a>
								<?php 
								if( in_array(14,$_SESSION["menuPermissions"])){ ?>
									<ul class="submenu" style="display:none;">
										<li><span></span><a href="manage_protocol" title="Manage Protocols">Manage Protocols</a></li>
									</ul>
								<?php 
								} if( in_array(15,$_SESSION["menuPermissions"])){  ?>
									<li><span></span><a href="listdisclaimer">Manage Disclaimers</a></li>
								<?php
								}
								if($_SESSION['Admin'] == 'TRUE'){
								?>
									<li><span></span><a href="categories">Manage Categories</a></li>
									<li><span></span><a href="services">Manage Services</a></li>
									<li><span></span><a href="areas">Manage Areas</a></li>
									<li><span></span><a href="iframes">Generate Widget</a></li>
								<?php
								}
								?>
								<li><span></span> <a href="calander">Appointment Calendar</a></li>
							</li>
							<?php 		
							} ?> 
						</ul>
					</div>
				<?php
				}
				
			}
			if( $_SESSION['Superadmin'] == 0 && isset($_SESSION['loginuser']) && $_SESSION['loginuser'] == "sitesuperadmin") {

			} else {
				include('../includes/config.php');	
				$con = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);
				$query = "SELECT * FROM tbl_subscription_history WHERE BusinessID =".$_SESSION['BusinessID'];
				$res = mysqli_query($con,$query) or die($query);
				if(mysqli_num_rows($res) > 0)	{
					$row = mysqli_fetch_object($res);
					$expiredate = $row->ExpireDate;
					$now = time(); 
					$your_date = strtotime("$expiredate");
					$datediff = $your_date - $now;
					$daydiff =  round($datediff / (60 * 60 * 24));
					if( $daydiff >0 && $daydiff <= 15) { ?>	
						<div class="SubscriptionBlock">
							<p>Your Subscription to MyLaserData <b> Expires in <?php echo $daydiff; ?> Days </b> </p><p> Please Contact MyLaserData to Renew <a href="mailto:info@mylaserdata.com">info@mylaserdata.com</a>.</p>
						</div>
					<?php
					}
				}
			}
			?>
