<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion1" id="accordionSidebar">
	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
		<img src="<?php echo $domain; ?>/img/logo-fadeaway.png" alt="FadeAway">
	</a>
	<?php 
	if(isset($_SESSION["Superadmin"]) && $_SESSION["Superadmin"]==1){ 
	?>
		<!-- Nav Item - superadmin Dashboard -->
		<li class="nav-item">
			<a class="nav-link" href="dashboard" title="Dashboard">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="newbusiness" title="Add New Business">
			<img src="../img/addnewbusiness.png">
			<span>Add New Business</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="managebusiness" title="All Businesses">
			<img src="../img/business.png">
			<span>All Businesses</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="emailtemplates" title="Manage Email Templates">
			<img src="../img/emails.png">
			<span>Manage Email Templates</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="manage-plans" title="Manage Plans">
			<img src="../img/manageplans.png">
			<span>Manage Plans</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="all_transactions" title="Transactions History">
			<i class="fas fa-history"></i>
			<span>Transactions History</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="superadmin-report">
			<img src="../img/report.svg">
			<span>Reports</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="clientlocationdetail">
			<img src="../img/exportclients.png">
			<span>Export Clients</span></a>
		</li>
	<?php 
	} else if($_SESSION["period"]=="true") { ?>
		<!-- Nav Item - Dashboard -->
		<li class="nav-item">
			<a class="nav-link" href="dashboard" title="Dashboard">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a>
		</li>
		<?php 
		if( in_array(1,$_SESSION["menuPermissions"])){  ?>
			<li class="nav-item">
				<a class="nav-link" href="user-entry" title="New Client">
				<i class="fas fa-user-tie"></i>
				<span>New Client</span></a>
			</li>
		<?php
		} 
		if($_SESSION['logintype']!="client"){
			if( in_array(2,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="treatment" title="Client Lookup">
					<img src="../img/clientlookup.svg">
					<span>Client Lookup</span></a>
				</li>
			<?php 
			}
			if( in_array(3,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="pricing" title="Pricing Calculator">
					<i class="fas fa-calculator"></i>
					<span>Pricing Calculator</span></a>
				</li>
			<?php 
			}
			if( in_array(4,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="consentforms" title="Consent Form Review">
					<i class="fas fa-file-invoice"></i>
					<span>Consent Form Review</span></a>
				</li>
			<?php 
			}
			if( in_array(5,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="clientdetails" title="Manage Clients">
					<i class="fas fa-users-cog"></i>
					<span>Manage Clients</span></a>
				</li>
			<?php 
			}
			if( in_array(6,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="empdetails" title="Manage Employees">
					<i class="fas fa-user-cog"></i>
					<span>Manage Employees</span></a>
				</li>
			<?php 
			}
			if( in_array(7,$_SESSION["menuPermissions"])){
				// if($_SESSION['loginuser'] == "sitesuperadmin") { 
				if($_SESSION['loginuser'] == "sitesuperadmin" || $_SESSION["Usertype"] == 'Admin') {
				?>
					<li class="nav-item">
						<a class="nav-link" href="manage_location_details" title="Manage Locations">
						<i class="fas fa-map-marker-alt"></i>
						<span>Manage Locations</span></a>
					</li>
				<?php 		
				}
			}
			if( in_array(8,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="devicedetails" title="Manage Devices">
					<i class="fas fa-mobile-alt"></i>
					<span>Manage Devices</span></a>
				</li>
			<?php 
			}
			if( in_array(9,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="<?php if(in_array("1",$_SESSION["services"])) echo 'pricingdetails'; else if(in_array("2",$_SESSION["services"])) echo 'pricingdetailshair';?>" title="Manage Pricing">
					<i class="fas fa-dollar-sign"></i>
					<span>Manage Pricing</span></a>
				</li>
			<?php 
			} 
			if( in_array(10,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="trans_history" title="Transactions History">
					<i class="fas fa-history"></i>
					<span>Transactions History</span></a>
				</li>
			<?php
			}
			if( in_array(11,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="your_sub" title="Subscription Status">
					<i class="fas fa-battery-three-quarters"></i>
					<span>Subscription Status</span></a>
				</li>
			<?php 
			}
			if( in_array(12,$_SESSION["menuPermissions"])){ 
				if($_SESSION['loginuser'] != "sitesuperadmin") { ?>
					<li class="nav-item">
						<a class="nav-link" href="lapsed-client-status" title="Client Status Update">
						<i class="fas fa-mug-hot"></i>
						<span>Client Status Update</span></a>
					</li>
				<?php 
				} 
			}
			if( in_array(13,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="clientreport" title="Reports">
					<img src="../img/report.svg">
					<span>Reports</span></a>
				</li>
			<?php 
			} ?>
			<li class="nav-item <?php // if( in_array(14,$_SESSION["menuPermissions"])){ echo "protocolTab"; } ?>">
				<a class="nav-link aclass" target="_blank" <?php echo $protocol_name; ?> title="Protocols">
				<i class="fas fa-chart-area"></i>
				<span>Protocols</span></a>
			</li>
			<?php
			if( in_array(14,$_SESSION["menuPermissions"])){  ?>
				<ul class="submenu" style="display:none;">
					<li class="nav-item">
						<a class="nav-link" href="manage_protocol" title="Manage Protocols">
						<img src="../img/disclaimer.png">
						<span>Manage Protocols</span></a>
					</li>
				</ul>
			<?php
			}
			?>
			</li>
			<?php
			if( in_array(15,$_SESSION["menuPermissions"])){  ?>
				<li class="nav-item">
					<a class="nav-link" href="listdisclaimer" title="Manage Disclaimers">
					<img src="../img/disclaimer.png">
					<span>Manage Disclaimers</span></a>
				</li>
			<?php 
			} 
			?>
			<li class="nav-item">
				<a class="nav-link" href="calander" title="Appointment Calendar">
				<img src="../img/calander.png">
				<span>Appointment Calendar</span></a>
			</li>
			<?php
			if( $_SESSION['Admin'] == 'TRUE' ){
			?>
				<li class="nav-item">
					<a class="nav-link" href="categories" title="Manage Categories">
					<img src="../img/categories.svg">
					<span>Manage Categories</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="services" title="Manage Services">
					<img src="../img/service.png">
					<span>Manage Services</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="areas" title="Manage Areas">
					<img src="../img/areas.png">
					<span>Manage Areas</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="iframes" title="Generate widget">
					<img src="../img/widget.png">
					<span>Generate widget</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="statistics" title="Generate widget">
					<img src="../img/statics.png">
					<span>Clinic Statistics</span></a>
				</li>
			<?php
			}
		}
	}
	?>
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline btn-sidebar-toggle">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
</ul>
