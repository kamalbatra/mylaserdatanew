<nav class="navbar navbar-expand navbar-light topbar mb-4 static-top">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>
    <!-- Topbar Search -->
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
        <div class="input-group">
            <div class="input-group-append">
                <i class="fas fa-search fa-sm"></i>
            </div>
            <input type="text" class="searchclient form-control border-0 bg-transparent small" id="searchid" placeholder="Search client by first name, last name..." aria-label="Search" aria-describedby="basic-addon2">
			<input type="hidden" name="location" id="location" class="text-input-field" id="pricing_amt" value="<?=$_SESSION['Location']?>"/>
			
			<div id="resultClientSerch" class="resultClientSerch" style="font-family:Verdana,Geneva,Tahoma,sans-serif"></div>
        </div>
    </form>
    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>
        <!-- Nav Item - Alerts -->
        <!-- li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <span class="badge badge-counter">5</span>
            </a>
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
					Alerts Center
				</h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-file-alt text-white"></i>
                        </div>
                    </div>
                    <div>
                        <div class="small text-gray-500">December 12, 2019</div>
                        <span class="font-weight-bold">A new monthly report is ready to download!</span>
                    </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                        <div class="icon-circle bg-success">
                            <i class="fas fa-donate text-white"></i>
                        </div>
                    </div>
                    <div>
                        <div class="small text-gray-500">December 7, 2019</div>
                        $290.29 has been deposited into your account!
                    </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                        <div class="icon-circle bg-warning">
                            <i class="fas fa-exclamation-triangle text-white"></i>
                        </div>
                    </div>
                    <div>
                        <div class="small text-gray-500">December 2, 2019</div>
                        Spending Alert: We've noticed unusually high spending for your account.
                    </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
            </div>
        </li -->
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
				<!-- div class="admin-login">  
				<h6>
				<?php
					if($_SESSION['Superadmin']!="1" && $_SESSION["period"]=="true" && $_SESSION['logintype']!="client")	{ 
				?> 
						     
				<?php
					}
					if($_SESSION['Superadmin']=="0" && $_SESSION['Usertype']=="Admin") {
				?>
						<a href="profile?id=<?php echo $_SESSION['BusinessID']; ?>" title="View Profile">
							<span class="user-icon">Welcome 
							<?php echo $_SESSION['First_Name']." ".$_SESSION['Last_Name'];?>
							</span>
						</a>
				<?php
					} else { 
				?>
						<span class="user-icon">Welcome 
							<?php echo $_SESSION['First_Name']." ".$_SESSION['Last_Name'];?>
						</span> 
						<?php 
					} 
					?>| <a href="<?php echo SITE_URL; ?>includes/login.php?logout=true"><span class="signout">Signout</span></a>
				</h6>
			</div -->
			<?php
			if($_SESSION['Superadmin']=="0" && $_SESSION['Usertype']=="Admin") {
				$name = $_SESSION['First_Name']." ".$_SESSION['Last_Name'];
				$profilelink = "profile?id=".$_SESSION['BusinessID'];
			} else {
				$name = $_SESSION['First_Name']." ".$_SESSION['Last_Name'];
				$profilelink = "";
			} 
			?>


            <a class="nav-link dropdown-toggle" href="<?php echo $profilelink; ?>" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-right">Welcome<br><em><?php echo $name; ?></em></span>
                <img class="img-profile rounded-circle" src="<?php echo $domain; ?>/img/img-profile-picture.jpg" alt="">
                <i class="fas fa-chevron-down ml-2"></i>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo $profilelink; ?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i> Profile
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i> Settings
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i> Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo SITE_URL; ?>includes/login.php?logout=true">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> Logout
                </a>
            </div>
        </li>

    </ul>

</nav>
