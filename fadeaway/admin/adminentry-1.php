<?php
	//session_start();
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$adminForms	= new dbFunctions();
	if( !in_array(8,$_SESSION["menuPermissions"])) {
	?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php
	}
	/*** fetch All device Name**/
	$tableDevice = "tbl_devicename";
	$condition = "ORDER BY deviceId  DESC ";
	$cols="*";
	$DeviceData	= $adminForms->selectTableRows($tableDevice,$condition);
	//print_r($DeviceData);
	/*** End fetch All device Name**/
?>
<!---- Validation for empty field--->
<script type="text/javascript">
	jQuery(document).ready(function() { 	
		jQuery("#addDeviceName").validate({
			rules: {
                DeviceName: "required",              
                'service[]':"required",             
				DeviceName: {
					required: true,
				}
			},
			messages: {
				DeviceName: "This field is required.",
				'service[]':"Please select atleast one service."
			},
			  errorPlacement: function(error, element) {
				if (element.attr("name") == "service[]" ) {
				  $(".servicenameerr").html('Please select atleast one service.');
				} else {
				  error.insertAfter(element);
				}
			  }
		});
		$("#deviceSubBtn").click(function() {
			if( $("#addDeviceName").valid()) {
				//alert("ss");
				//var strForm = $( "#manageDeviceEdit" ).serialize();
				var $textboxes = $('input[name="Wavelength[]"]')
				var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
				var arr = $('.deviceName').map(function(i, e) {
					if(!numberRegex.test(e.value)) {
						 //return e.value; 
						 //alert("ee:-"+e.id);				 
						 $("#"+e.id).css("border","1px solid red");	
						 $("#"+e.id).val("");
						 $("#"+e.id).focus();
						 $("#"+e.id).addClass('placeholderInt');
						 $("#"+e.id).attr("placeholder", "Enter numeric value only");			  
						 return e.id; 
					} else {
						$("#"+e.id).css("border","1px solid #ccc");	
					}
				}).toArray();
				//alert(arr.length);
				if(arr.length ==0) {
					adddeviceName();
				}
			} else {
			}
		});
	});
	function adddeviceName(id) {	
		var str = $( "#addDeviceName" ).serialize();
		$.ajax({
			type: "POST",
			url: "ajax_addDeviceName.php",
			data: str,
			cache: false,
			success: function(result) {
				//alert(result);
				if(result==1) {
					$("#deviceExist").html("Device name already exist! ").show();
					$("#deviceResult").hide();
				} else {
					$("#deviceExist").hide();	
					$("#deviceResult").html(result).show();
				}
			}
		});
	}
</script>
<!----End Validation for empty field--->
<!-- --- ADD text and remove text box for Wave length -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendWavelength").click( function() {
			$("#textBoxwave").show();
			if(counter1>20) {
				// alert("Only 10 textboxes allow");
				return false;	
			}
			$("#addwave").append('<div class="boxx"><a href="#" class="remove_wave btn btn-danger">.</a> <input type="text" class="deviceName text-input-field" name="Wavelength[]" id="wavelenght'+counter1+'"></div>');
			//alert(counter1);
			counter1++;
			return false;
		});	
		$('.remove_wave').live('click', function() {
			if(counter1==1) {
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});
	});
</script>
<!-- --- ADD text and remove text box for SpotSize -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendSpotSize").click( function() {
			$("#textBoxSpot").show();
			if(counter1>20) {
				// alert("Only 10 textboxes allow");
				return false;	
			}
			$("#addSpot").append('<div class="boxx"><a href="#" class="remove_spot btn btn-danger">.</a> <input type="text" class="deviceName text-input-field" name="spotsize[]" id="spotsize'+counter1+'"></div>');
			//alert(counter1);
			counter1++;
			return false;
		});
		$('.remove_spot').live('click', function() {
			if(counter1==1) {
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});	
	});
</script>
<style>
	.span6.service {
		  font-family: Verdana,Geneva,Tahoma,sans-serif;
		  font-size: 13px;
		  font-weight: 500;
		  margin-top: 16px;
		  color: #666666;
	}
	.servicenameerr {
    color: red;
    float: right;
    font-size: 13px;
    line-height: 20px;
    width: 93%;
}
</style>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Manage Device </h1>
		<div class="addNew"><a class="empLinks" href="devicedetails" class="submit-btn">Device List </a></div>
	</div>
	<div class="user-entry-block">
		<div class="row">	
			<!--div class="span3 adhead"><a href="adminentry.php">Add Details</a></div>
			<div class="span3 adhead"><a href="devicedetails.php"> Edit Details</a></div-->
			<!--div class="span3 adhead"><a href=""> Detail </a></div>
			<div class="span3 adhead"> .</div-->
		</div>
		<div id="horizontalTab">
			<!--ul class="resp-tabs-list">
				<li>Device name</li>
				<li>Wave length</li>
				<li>Spot size</li>
				<!--li>Fluence</li-->	
			<!--/ul-->
			<!---Device Name:---->
			<div class="resp-tabs-container">
				<div>
					<div class="row treatment treatment-post">
						<form action="" method="post" name="addDeviceName" id="addDeviceName">							      
						 <div class="row treatment">
							  <div class="span3" style="text-align:right;padding-top:16px;"><label class="user-name">Select Service:</label></div>
							  <div class="span6 service">
								<?php 
									$ServiceTable = "tbl_master_services";
									$SerCond = "where status=1 and id in(".implode(",",$_SESSION["services"]).")";
									$cols="*";
									$ServiceData = $adminForms->selectTableRows($ServiceTable,$SerCond);
									//~ echo "<pre>";
									//~ print_r($_SESSION["services"]);
									//~ print_r($ServiceData);	die;
									foreach($ServiceData as $key=>$Service ) {
								?>  
									<input type="checkbox" name="service[]" value="<?php echo $Service['id'];?>"/><span class="servicename"><?php echo $Service["name"]; ?></span> 
								<?php } ?>
								<span class="servicenameerr"></span>
							  </div>	  
						  </div>	
						  <div class="row treatment">
							  <div class="span3" style="text-align:right;padding-top:16px;"><label class="user-name">Device:</label></div>
							  <div class="span6"><input type="text" class="text-input-field" value="" name="DeviceName" value="" /> 
							  </div>	  
						  </div>						      
						  <div class="row treatment">
							  <div class="span3" style="text-align:right;padding-top:16px;"><label class="user-name">Numeric validation:</label></div>
							  <div class="span6">
								  <select class="select-option" name="Required" id="Required">
									  <option value="Yes">Yes</option>
									  <option value="No">No</option>
								 </select>											 
								 <span id="deviceExist" class="mngdevicesName error"> </span>   
							  </div>									  
						  </div><!-- @row end-->
						 <div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;margin-bottom:6px;"><label class="user-name">Wavelength(EX:1064):</label></div>
							<div class="span6"><input type="text" class="deviceName text-input-field" value="" name="Wavelength[]" id="wavelength" />	 </div> 
						 </div><!-- rowend-->
						 <div class="row treatment"  id="textBoxwave" style="display:none;">
						   <div class="span3" style="text-align:right;padding-top:16px;"></div>
						   <div id="addwave" class="span6 last" style="">	 </div>
						</div>
						 <!-- add more btn--->
						  <div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;"></div>
							<div id="addwavbt" class="span6 last" style="">
							<button class="btn btn-info" type="button" id="appendWavelength" name="append">Add More</button>
							 </div>
						 </div><!-- rowend-->
						  <!-- add more btn--->
						  <!--add spot size-->
						  <div class="row treatment">
						 <div class="span3" style="text-align:right;padding-top:16px;"><label class="user-name">Spot Size(EX:2):</label></div>
						 <div class="span6">
							 <input type="text" class="deviceName text-input-field" id="spotsize" value="" name="spotsize[]" />
						 </div> 
						  <div class="span3 mmlable">
							  <label class="user-name">(mm)</label>
						  </div> 
						</div><!-- rowend-->							  
						 <div class="row treatment"  id="textBoxSpot" style="display:none;">
							<div class="span3" style="text-align:right;padding-top:16px;"></div>
						   <div id="addSpot" class="span6 last" style="">	 </div>
						 </div>								
						<!-- add more btn for spot size--->
						<div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;"></div>
							<div id="addspotSize" class="span6 last" style="">
							<button class="btn btn-info" type="button" id="appendSpotSize" name="append">Add More</button>
							 </div>
						 </div><!-- rowend-->
						  <!-- add more btn for spot size--->							      
						<div class="row treatment">
						   <div class="span3"></div>
						<div class="span6 submtbutAdd"><input type="button" id="deviceSubBtn" value="Submit" class="submit-btn" />
						</div>
						<span id="deviceResult" class="mngdevicesName"> </span>
						</div>							  
						</form>
					</div><!--End @row-block-->
				</div>
			</div><!--resp-tabs-container-->					
		</div><!---Device Name:---->					
	</div><!--End @user-entry-block-->
</div><!--End @form-container-->
</div><!--End @container-->
<?php
	include('admin_includes/footer.php');
?>
