<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$adminForms	= new dbFunctions();
	if( !in_array(8,$_SESSION["menuPermissions"])) {
	?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php
	}
	/*** fetch All device Name**/
	$tableDevice = "tbl_devicename";
	$condition = "ORDER BY deviceId  DESC ";
	$cols="*";
	$DeviceData	= $adminForms->selectTableRows($tableDevice,$condition);
	//print_r($DeviceData);
	/*** End fetch All device Name**/
?>
<!---- Validation for empty field--->
<script type="text/javascript">
	jQuery(document).ready(function() { 	
		jQuery("#addDeviceName").validate({
			ignore: [],
			errorClass: 'errorblocks',
			errorElement: 'div',
			rules: {
                DeviceName: "required",              
                'service[]':"required",             
				DeviceName: {
					required: true,
				}
			},
			messages: {
				DeviceName: "This field is required.",
				'service[]':"Please select atleast one service."
			},
		});
		$("#deviceSubBtn").click(function() {
			if( $("#addDeviceName").valid()) {
				//alert("ss");
				//var strForm = $( "#manageDeviceEdit" ).serialize();
				var $textboxes = $('input[name="Wavelength[]"]')
				var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
				var arr = $('.deviceName').map(function(i, e) {
					if(!numberRegex.test(e.value)) {
						 //return e.value; 
						 //alert("ee:-"+e.id);				 
						 $("#"+e.id).css("border","1px solid red");	
						 $("#"+e.id).val("");
						 $("#"+e.id).focus();
						 $("#"+e.id).addClass('placeholderInt');
						 $("#"+e.id).attr("placeholder", "Enter numeric value only");			  
						 return e.id; 
					} else {
						$("#"+e.id).css("border","1px solid #ccc");	
					}
				}).toArray();
				//alert(arr.length);
				if(arr.length ==0) {
					adddeviceName();
				}
			} else {
			}
		});
	});
	function adddeviceName(id) {	
		var str = $( "#addDeviceName" ).serialize();
		$.ajax({
			type: "POST",
			url: "ajax_addDeviceName.php",
			data: str,
			cache: false,
			success: function(result) {
				//alert(result);
				if(result==1) {
					$("#deviceExist").html("Device name already exist! ").show();
					$("#deviceResult").hide();
				} else {
					$("#deviceExist").hide();	
					$("#deviceResult").html(result).show();
				}
			}
		});
	}
</script>
<!----End Validation for empty field--->
<!-- --- ADD text and remove text box for Wave length -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendWavelength").click( function() {
			$("#textBoxwave").show();
			if(counter1>20) {
				// alert("Only 10 textboxes allow");
				return false;	
			}
			
			$("#addwave").append('<div class="half boxx"><a href="#" class="remove_wave btn btn-danger">x</a><div class="form-col-ld"><div class="inputblock-ld"><input type="text" class="deviceName text-input-field" name="Wavelength[]" id="wavelenght'+counter1+'"></div></div></div>');	
			//alert(counter1);
			counter1++;
			return false;
		});	
		$('.remove_wave').live('click', function() {
			if(counter1==1) {
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});
	});
</script>
<!-- --- ADD text and remove text box for SpotSize -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendSpotSize").click( function() {
			$("#textBoxSpot").show();
			if(counter1>20) {
				// alert("Only 10 textboxes allow");
				return false;	
			}
			$("#addSpot").append('<div class="half boxx"><a href="#" class="remove_spot btn btn-danger">x</a><div class="form-col-ld"><div class="inputblock-ld"><small>(mm)</small><input type="text" class="deviceName text-input-field" name="spotsize[]" id="spotsize'+counter1+'"></div></div></div>');
			//alert(counter1);
			counter1++;
			return false;
		});
		$('.remove_spot').live('click', function() {
			if(counter1==1) {
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});	
	});
</script>
<style>
	.span6.service {
		  font-family: Verdana,Geneva,Tahoma,sans-serif;
		  font-size: 13px;
		  font-weight: 500;
		  margin-top: 16px;
		  color: #666666;
	}
	.servicenameerr {
    color: red;
    float: right;
    font-size: 13px;
    line-height: 20px;
    width: 93%;
}
</style>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Device</h1>
				</div>	
				
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="devicedetails" class="submit-btn"><button class="addnewbtn">Device List </button></a>
						</div>
					</div>
					<div class="formcontentblock-ld">
						<form action="" method="post" name="addDeviceName" id="addDeviceName">							      
							
								<div class="form-row-ld">
									<div class="full">
										<div class="form-col-ld">
											<div class="inputblock-ld radiolabel">
												<label>Select Service</label>
												<div class="radioblocksBtns">
												<?php
													$ServiceTable = "tbl_master_services";
													$SerCond = "where status=1 and id in(".implode(",",$_SESSION["services"]).")";
													$cols="*";
													$ServiceData = $adminForms->selectTableRows($ServiceTable,$SerCond);
													foreach($ServiceData as $key=>$Service ) {
												?>  
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" name="service[]" value="<?php echo $Service['id'];?>" id="<?php echo $Service['id'];?>"/><label for="<?php echo $Service['id'];?>"><?php echo $Service["name"]; ?></label>
														</div>
												<?php 
													} 
												?>
													<span class="servicenameerr"></span>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Device</label>
												<input type="text" class="text-input-field" value="" name="DeviceName" value="" />
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Numeric Validation</label>
												<select class="select-option" name="Required" id="Required">
												<option value="Yes">Yes</option>
												<option value="No">No</option>
												</select>											 
												<span id="deviceExist" class="mngdevicesName error"> </span>   
											</div>
										</div>
									</div>
								</div>								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label class="user-name">Wavelength(EX:1064):</label>
												<input type="text" class="deviceName text-input-field" value="" name="Wavelength[]" id="wavelength" />
											</div>
										</div>
									</div>
									<div id="addwave" class="span6 last" style="">	 </div> 								
								</div>

<div class="form-row-ld">
		<div class="form-col-ld">
			<div id="addwavbt" class="span3" style="width: 12%;"> 
				<div id="appendWavelength" class="addMoreBtn addMoreblk">
					Add More
				</div>
			</div>
		</div>
</div>
																										  
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Spot Size(EX:2):</label>
												<small>(mm)</small>
												<input type="text" class="deviceName text-input-field" id="spotsize" value="" name="spotsize[]" />
											</div>
										</div>
									</div>
									<div id="addSpot" class="span6 last" style=""></div>
								</div>
								
							
<div class="form-row-ld">
		<div class="form-col-ld">
			<div id="addspotSize" class="span3" style="width: 12%;"> 
				<div id="appendSpotSize" class="addMoreBtn addMoreblk">
					Add More
				</div>
			</div>
		</div>
</div>	
								<div class="form-row-ld">
									<div class="backNextbtn">
										<button type="button" id="deviceSubBtn" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
										<span id="deviceResult" class="mngdevicesName"> </span>
									</div>
								</div>								

						</form>

					</div>
				</div>
				
				
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
