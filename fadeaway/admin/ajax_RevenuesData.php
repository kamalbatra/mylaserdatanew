<?php
	//error_reporting(0);
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$revenuesReport	= new dbFunctions();
	//print_r($_POST);
	if(strtotime($_POST['inputDate']) <= strtotime($_POST['inputEnddate']) ) {
		$startDate =  $_POST['inputDate'];	 
		$newStartDate = date("Y-m-d H:i:s", strtotime($startDate));
		$endtDate  =  $_POST['inputEnddate'];
		$newEndtDate = date("Y-m-d H:i:s", strtotime($endtDate));
		$table = "tbl_treatment_log as t";
		$table2 = "tbl_clients as c";
		$cols = "c.ClientID ,AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName,AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName,t.Size,t.Price,t.COUNT(*) as number,t.AVG(Price) as avgrage";
        $condition = "c.ClientID=t.ClientID WHERE c.BusinessID=".$_SESSION["BusinessID"]." AND t.Date_of_treatment between '".$newStartDate."' and '".$newEndtDate."' and t.Emp_First_Name = '".$_POST['technicianName']."' and t.Size !=''  GROUP BY Size ";
        //$RevenuesReportData= $revenuesReport->selectTableRows($table,$condition,$cols);     
        $RevenuesReportData= $revenuesReport->selectTableJoin($table,$table2,$join="",$condition,$cols);
		// print_r($RevenuesReportData);
		if($RevenuesReportData !=NULL) {
			include ("libchart/classes/libchart.php");
			$chart = new PieChart();         
			$dataSet = new XYDataSet();
			foreach($RevenuesReportData as $revenuesData) {
				$dataSet->addPoint(new Point("Size of tattoo (".$revenuesData['Size']."),  Average price (".$revenuesData['avgrage'].")", $revenuesData['avgrage']));		  
			}
			$chart->setDataSet($dataSet);
			$chart->setTitle("Tattoo size and average price according to technician.");
			$chart->render("generated/demo3.png");
			echo '<img alt="Pie chart"  src="generated/demo3.png" style="border: 1px solid gray;"/>';
			//SELECT ClientID ,Client_First_Name, Client_Last_Name,Date_of_treatment,SessionNumber,Size,Price, COUNT(*) as number ,AVG(Price) as avgrage FROM tbl_treatment_log WHERE   Date_of_treatment between '2011-04-15 00:00:00' and '2014-04-21 00:00:00'  and `Emp_First_Name` = 'pratibha'  GROUP BY Size
		} else {
			echo "<span style='float:left;padding:10px 0 0;text-align:center;width:100%;'>No record found.</span>";
		}
    } else {
		echo "<span class='error' style='float:left;padding:10px 0 0;text-align:center;width:100%;'>Please select correct date!</span>";
	}
	/*
	$result = mysql_query("SELECT ReferralSource,COUNT(*) as totalnum  FROM tbl_clients Where ReferralSource !=''  GROUP BY ReferralSource");
	$rows = array();
	while($r = mysql_fetch_array($result)) {
		//echo "<pre>";
		//print_r($r);
		$row[0] = $r[0];
		$row[1] = $r[1];
		array_push($rows,$row);
	}
	//print_r($rows);
	print json_encode($rows, JSON_NUMERIC_CHECK);
	*/
?> 
