<?php
	include("../includes/dbFunctions.php");
	$Size = new dbFunctions();
	$Location = $_POST["Location"];
	$Service = $_POST["service"];
	
	$SizeTable = "tbl_master_sizes";
	$SCond = "where status=1 AND serviceId=2";
	$SCols = "id,size";
	$SizeData = $Size->selectTableRows($SizeTable,$SCond,$SCols);
	$domain=$_SERVER['DOMAIN'];
?>
<script>
	$('#size').change(function(){
		var SizeID = this.value;
		if( SizeID!= 0) {
			var str = 'SizeID='+SizeID;
			$.ajax ({
				type: "POST",
				url: "ajax_areadata.php",
				data: str,
				success: function(msg) {
					$("#area_div").html(msg).show();
				}
			});
		} else {
			$("#area_div").html('').show();
		}
	});
</script>
<div class="popupblockOuter">
	<div class="popoutertb">
		<div class="popoutercell">
			<div class="popoutercontent popoutereditcal">
				<form method="" name="addpricingform" id="addpricingform">
					<div class="popcontentmain">	
					<?php if( $Service == "Tattoo" ) { ?>
							<h2>
								Add Price For Tattoo Renewal
								<img src="<?php echo $domain; ?>/img/cross.png" class="crossiconblk">
							</h2>
							<div class="sessionOuterBlock">
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Size:</label>
												<input type="text" class="text-input-field pricingclass" name="size" id="size"/>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Recommended Price:</label>
												<input type="text" class="text-input-field pricingclass" name="Rec_Price" id="Rec_Price"/>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Three Session Discounted Price:</label>
												<input type="text" class="text-input-field pricingclass" name="discount5" id="discount5"/>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Six Session Discounted Price:</label>
												<input type="text" class="text-input-field pricingclass" name="discount10" id="discount10"/>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Cost Savings for Three Sessions:</label>
												<input type="text" class="text-input-field pricingclass" name="savings5" id="savings5"/>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Cost Savings for Six Sessions:</label>
												<input type="text" class="text-input-field pricingclass" name="savings10" id="savings10"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php 
						} else if( $Service == "Hair" ) { ?>
							<h2>
								Add Price For Hair Renewal
								<img src="<?php echo $domain; ?>/img/cross.png" class="crossiconblk">
							</h2>
							<div class="sessionOuterBlock">
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Select Size:</label>
												<select class="text-input-field" name="size" id="size">
													<option value="0">-Select Size-</option>
													<?php
														if(!empty($SizeData)) {
															foreach( $SizeData as $Sizes ) { ?>
																<option value="<?php echo $Sizes['id'] ?>"><?php echo $Sizes["size"]; ?></option>
															<?php
															}
														}
													?>
												</select>
											</div>
										</div>
									</div>
									<div id="area_div" class="span6 last"></div>
								</div>
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Recommended Price:</label>
												<input type="text" class="text-input-field pricingclass" name="Rec_Price" id="Rec_Price"/>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Three Session Discounted Price:</label>
												<input type="text" class="text-input-field pricingclass" name="discount5" id="discount5"/>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Six Session Discounted Price:</label>
												<input type="text" class="text-input-field pricingclass" name="discount10" id="discount10"/>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Cost Savings for Three Sessions:</label>
												<input type="text" class="text-input-field pricingclass" name="savings5" id="savings5"/>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Cost Savings for Six Sessions:</label>
												<input type="text" class="text-input-field pricingclass" name="savings10" id="savings10"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php 
						} ?>
						
						<div class="form-row-ld">
							<input type="hidden" name="Location" value="<?php echo $Location; ?>" />
							<input type="hidden" name="Service" value="<?php echo $Service; ?>" />
							<div class="backNextbtn">
								<button type="button" id="addBtn" value="Add" class="submit-btn nextbtn" style="float:left;">Add</button>
								<div id="successUpdateLocation" style="color:green;"></div>
							</div>
						</div>
					</div>
					<div>
						<div id="pricingMsg" class="commonerror"></div>
						<span id="pricingMsgUpdate"></span>
						<span id="pricingMsgUpdateLoding" style="display:none;"><img src="<?php echo $domain; ?>/images/loading.gif"></span>
					</div>
				
				</form>
			</div>
		</div>
	</div>
</div>
