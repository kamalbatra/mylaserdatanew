<?php
	//error_reporting(0);
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$clientSerch = new dbFunctions();
	if(isset($_POST) && $_POST != NULL ) {
		$search = strtolower($_POST['search']);
		$table1= "tbl_treatment_log";
		$table2= "tbl_clients";
		$currentDate = date("Y-m-d 00:00:00");
		$join="INNER";
		$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location";
		if($_POST['searchfor'] == 'lost'){
			$condition = "tbl_clients.ClientID=tbl_treatment_log.ClientID WHERE (CONCAT(AES_DECRYPT(tbl_clients.FirstName, '".SALT."'),' ',AES_DECRYPT(tbl_clients.LastName, '".SALT."')) like '".$search ."%' or AES_DECRYPT(tbl_clients.LastName, '".SALT."') like('" .$search . "%') or AES_DECRYPT(tbl_clients.PhoneNumber, '".SALT."') like('" .$search . "%')) AND tbl_clients.TimeStamp < '".$currentDate."' AND tbl_clients.BusinessID = $_SESSION[BusinessID] GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=180 ORDER BY tbl_clients.FirstName ";
			
			$clientdata	= $clientSerch->selectTableJoin($table1,$table2,$join,$condition,$cols);
			if($clientdata !=NULL) {
			?>	
				<div class="lapsedclass">
				<?php
				foreach($clientdata as $values1) { ?>
					<div id="click_<?php echo  $values1['ClientID']; ?>" class="show" align="left">
						<a href="lost-client-status.php?ClientID=<?php echo  base64_encode($values1['ClientID']); ?>"> <span class="name"><?php echo ucfirst($values1['FirstName']); ?></span>&nbsp;<?php echo ucfirst($values1['LastName']);?></a>
					</div>				
				 <?php   
				}
				?>
				</div>
			<?php	
			}
		}else{
			$condition = "tbl_clients.ClientID=tbl_treatment_log.ClientID WHERE (CONCAT(AES_DECRYPT(tbl_clients.FirstName, '".SALT."'),' ',AES_DECRYPT(tbl_clients.LastName, '".SALT."')) like '".$search ."%' or AES_DECRYPT(tbl_clients.LastName, '".SALT."') like('" .$search . "%') or AES_DECRYPT(tbl_clients.PhoneNumber, '".SALT."') like('" .$search . "%')) AND tbl_clients.TimeStamp < '".$currentDate."' AND tbl_clients.BusinessID = $_SESSION[BusinessID] GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=90 and DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) <=150 ORDER BY tbl_clients.FirstName ";
			
			$clientdata	= $clientSerch->selectTableJoin($table1,$table2,$join,$condition,$cols);
			if($clientdata !=NULL) {
			?>	
				<div class="lapsedclass">
				<?php	
				foreach($clientdata as $values1) { ?>
					<div id="click_<?php echo  $values1['ClientID']; ?>" class="show" align="left">
						<a href="lapsed-client-status.php?ClientID=<?php echo  base64_encode($values1['ClientID']); ?>"> <span class="name"><?php echo ucfirst($values1['FirstName']); ?></span>&nbsp;<?php echo ucfirst($values1['LastName']);?></a>
					</div>				
				 <?php   
				}
				?>
				</div>
			<?php	
			}
		}
	}
?>
