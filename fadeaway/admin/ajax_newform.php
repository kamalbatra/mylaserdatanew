<?php
	//error_reporting(0);
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$fullinfo = new dbFunctions();
	if(isset($_POST) && $_POST != NULL ) {
	
		if( $_POST['formname'] == 'addcategory' ){
			$table = "tbl_categories";
			$data['businessID'] = $_POST['businessID'];
			$data['categoryName'] = $_POST['categoryName'];
			$data['dateAdded'] = $_POST['dateAdded'];
			$data['status'] = $_POST['status'];
			$fullinfo->insert_data($table,$data);
			$id=mysqli_insert_id($fullinfo->con);
			if($id) {
				echo  "0";
			}
		}
		
		if( $_POST['formname'] == 'editcategory' ){
			$table = "tbl_categories";
			$data['id'] = $_POST['id'];
			$data['businessID'] = $_POST['businessID'];
			$data['categoryName'] = $_POST['categoryName'];
			$data['dateAdded'] = $_POST['dateAdded'];
			$data['status'] = $_POST['status'];
			$updatecat = $fullinfo->update_user($table,$data);
			if($updatecat) {
				echo  "1";
			} else {
				echo  "0";
			}
		}
		
		if( $_POST['formname'] == 'addservice' ){
			$table = "tbl_services";
			$data['businessID'] = $_POST['businessID'];
			$data['categoryID'] = $_POST['categoryID'];
			$data['locationID'] = $_POST['locationID'];
			$data['serviceName'] = $_POST['serviceName'];
			$data['bufferTime'] = $_POST['bufferTime'];
			$data['status'] = $_POST['status'];
			$data['dateAdded'] = $_POST['dateAdded'];
			if( isset($_POST['agreecheckbox']) ) {
				$data['isArea']	= 1;
			} else {
				$data['isArea']	= 0;
			}
			$fullinfo->insert_data($table,$data);
			$id=mysqli_insert_id($fullinfo->con);
			if($id) {
				echo  "0";
			}
		}
		
		if( $_POST['formname'] == 'editservice' ){
			$table = "tbl_services";
			$data['seviceID'] = $_POST['seviceID'];
			$data['businessID'] = $_POST['businessID'];
			$data['categoryID'] = $_POST['categoryID'];
			$data['locationID'] = $_POST['locationID'];
			$data['serviceName'] = $_POST['serviceName'];
			$data['bufferTime'] = $_POST['bufferTime'];
			$data['status'] = $_POST['status'];
			$data['dateAdded'] = $_POST['dateAdded'];
			if( isset($_POST['agreecheckbox']) ) {
				$data['isArea']	= 1;
			} else {
				$data['isArea']	= 0;
			}
			$updateservice = $fullinfo->update_user($table,$data);
			if($updateservice) {
				echo  "1";
			} else {
				echo  "0";
			}
		}
		
		if( $_POST['formname'] == 'addarea' ){
		
			if( isset( $_POST['durationTime'] ) && $_POST['durationTime'] != '' ){
				$durationTime = $_POST['durationTime'];
			} else {
				$durationTime = '';
			}
			if( isset( $_POST['categoryID'] ) && $_POST['categoryID'] != '' ){
				$categoryID = $_POST['categoryID'];
			} else {
				$categoryID = '';
			}
			$table = "tbl_areas";
			$data['businessID'] = $_POST['businessID'];
			$data['seviceID'] = $_POST['seviceID'];
			$data['categoryID'] = $categoryID;
			$data['areaName'] = $_POST['areaName'];
			$data['price'] = $_POST['price'];
			$data['durationTime'] = $durationTime;
			$data['status'] = $_POST['status'];
			$data['dateAdded'] = $_POST['dateAdded'];
			$fullinfo->insert_data($table,$data);
			$id=mysqli_insert_id($fullinfo->con);
			if($id) {
				echo  "0";
			}
		}
		
		if( $_POST['formname'] == 'editarea' ){
			if( isset( $_POST['durationTime'] ) && $_POST['durationTime'] != '' ){
				$durationTime = $_POST['durationTime'];
			} else {
				$durationTime = '';
			}
			if( isset( $_POST['categoryID'] ) && $_POST['categoryID'] != '' ){
				$categoryID = $_POST['categoryID'];
			} else {
				$categoryID = '';
			}
			$table = "tbl_areas";
			$data['areaID'] = $_POST['areaID'];
			$data['businessID'] = $_POST['businessID'];
			$data['seviceID'] = $_POST['seviceID'];
			$data['categoryID'] = $categoryID;
			$data['areaName'] = $_POST['areaName'];
			$data['price'] = $_POST['price'];
			$data['durationTime'] = $durationTime;
			$data['status'] = $_POST['status'];
			$data['dateAdded'] = $_POST['dateAdded'];
			$updatearea = $fullinfo->update_user($table,$data);
			if($updatearea) {
				echo  "1";
			} else {
				echo  "0";
			}
		}
		
		if( $_POST['formname'] == 'fetchcategory' ){
			$table1 = "tbl_services";
			$table2 = "tbl_categories";
			$condition = $table1.".categoryID = ".$table2.".id WHERE ".$table1.".seviceID = ".$_POST['id']." GROUP BY ".$table2.".categoryName ORDER by ".$table2.".categoryName ASC";
			$cols = $table2.".id,".$table2.".categoryName";
			$categoryData = $fullinfo->selectTableJoinNew($table1,$table2, 'left', $condition,$cols);	
			echo json_encode($categoryData[0]);
		}
		
		if( $_POST['formname'] == 'scheduleadd' ){
			$data = array();
			$dayarray = array();
			for( $i=0;$i<=6;$i++ )	{
				$day = $_POST['day1'][$i].'-'.trim($_POST['hourfrom'][$i]).'-'.trim($_POST['hourto'][$i]);
				array_push($dayarray,$day);
			}
			$data['Emp_ID'] = $_POST['empid'];
			$data['dailySchdeule'] = serialize($dayarray);
			$tableEmp = "tbl_employees";
			$cols = " Emp_ID ";
			$condition = "where Emp_ID='".$_POST['empid']."' ";
			$checkExistence = $fullinfo->selectTableSingleRow($tableEmp,$condition,$cols);
			if($checkExistence != NULL) {
				$fullinfo->update_user($tableEmp,$data);
				echo "1";
			} else {
				echo "0";
			}
		}
		
		if( $_POST['formname'] == 'leaveadd' ){
			$table = "tbl_employees_leave";				
			if( $_POST['status'] == 0){
				if(isset($_POST['noofdays']) && $_POST['noofdays'] != ''){
					$strnum = strpos($_POST['startDate'],"T");
					if($strnum == TRUE ){
						$date = explode('T',$_POST['startDate']);
						$date1 = date('Y-m-d', strtotime($date[0]. ' + '.$_POST['noofdays'].' days')).'T'.$date[1];
					} else if($strnum == FALSE ){
						$date1 = date('Y-m-d', strtotime($_POST['startDate']. ' + '.$_POST['noofdays'].' days')).'T'.'00:00:00';
					}
				} else {
					$date1	= $_POST['startDate'];
				}  
			} else if( $_POST['status'] == 1){
				if(isset($_POST['noofhours']) && $_POST['noofhours'] != ''){
					$strnum = strpos($_POST['startDate'],"T");
					if($strnum == TRUE ){
						$date = explode('T',$_POST['startDate']);
						$date1 = $date[0].'T'.date('H:i:s', strtotime($date[1]. ' + '.$_POST['noofhours'].' hours'));
					} else {
						$date1 = $_POST['startDate'].'T'.date('H:i:s', strtotime('00:00:00'. ' + '.$_POST['noofhours'].' hours'));
					}
				} else {
					$date1	= $_POST['startDate'];
				}  
			}
			$data['employeeID'] = $_POST['employeeID'];
			$data['businessID'] = $_POST['businessID'];
			$data['userID'] = $_POST['userID'];
			$data['status'] = $_POST['status'];
			$data['reason'] = $_POST['reason'];
			$data['startDate'] = $_POST['startDate'];
			$data['endDate'] = $date1;
			$fullinfo->insert_data($table,$data);
			$id=mysqli_insert_id($fullinfo->con);
			if($id) {
				echo  "1";
			}
		}
			
		if( $_POST['formname'] == 'leaveedit' ){
			if( $_POST['editeventstatus'] == 0){
				if(isset($_POST['editnoofdays']) && $_POST['editnoofdays'] != ''){
					$strnum = strpos($_POST['startDate'],"T");
					if($strnum == TRUE ){
						$date = explode('T',$_POST['startDate']);
						$date1 = date('Y-m-d', strtotime($date[0]. ' + '.$_POST['editnoofdays'].' days')).'T'.$date[1];
					} else if($strnum == FALSE ){
						$date1 = date('Y-m-d', strtotime($_POST['startDate']. ' + '.$_POST['editnoofdays'].' days')).'T'.'00:00:00';
					}
				} else {
					$date1	= $_POST['startDate'];
				}  
			} else if( $_POST['editeventstatus'] == 1){
				if(isset($_POST['editnoofhours']) && $_POST['editnoofhours'] != ''){
					$strnum = strpos($_POST['startDate'],"T");
					if($strnum == TRUE ){
						$date = explode('T',$_POST['startDate']);
						$date1 = $date[0].'T'.date('H:i:s', strtotime($date[1]. ' + '.$_POST['editnoofhours'].' hours'));
					} else {
						$date1 = $_POST['startDate'].'T'.date('H:i:s', strtotime('00:00:00'. ' + '.$_POST['editnoofhours'].' hours'));
					}
				} else {
					$date1	= $_POST['startDate'];
				}  
			}
			$table = "tbl_employees_leave";
			$data['id'] = $_POST['ID'];
			$data['employeeID'] = $_POST['employeeID'];
			$data['businessID'] = $_POST['businessID'];
			$data['userID'] = $_POST['userID'];
			$data['status'] = $_POST['editeventstatus'];
			$data['reason'] = $_POST['reason'];
			$data['startDate'] = $_POST['startDate'];
			$data['endDate'] = $date1;
			$updatearea = $fullinfo->update_user($table,$data);
			if($updatearea) {
				echo  "1";
			} else {
				echo  "0";
			}
		}
		
		if( $_POST['formname'] == 'leavedelete' ){
			$id = $_POST['id'];
			$table = "tbl_employees_leave";
			$condition = 'WHERE id ='.$id;
			$deleteleave = $fullinfo->deleteRowNew($table,$condition);
			if($deleteleave) {
				echo  "1";
			} else {
				echo  "0";
			}
			die;
		}
		
		if( $_POST['formname'] == 'genrateiframe' ){
			$table = "tbl_iframe";
			$condition = " where businessID=".$_POST['businessID']." AND loactionID=".$_POST['location'];
			$cols = "iframeText";
			$editinfo = $fullinfo->selectTableSingleRowNew($table,$condition,$cols);
			if( isset($editinfo) && $editinfo != '' ){
				echo "0";
			} else {
				$domain = $_SERVER['DOMAIN'];
				$domain = $domain.'/frame?bussinessid='.base64_encode($_POST["businessID"]).'&location='.base64_encode($_POST['location']).'&frontend=true';			
				$iframe = '<iframe src="'.$domain.'" align="center" width="100%" height="100%" name="myIframe" id="myIframe"></iframe>';
				$data['businessID'] = $_POST['businessID'];
				$data['loactionID'] = $_POST['location'];
				$data['iframeText'] = $iframe;
				$data['dateAdded'] = date('Y-m-d H:i:s');
				$fullinfo->insert_data($table,$data);
				$id=mysqli_insert_id($fullinfo->con);
				if($id) {
					echo  "1";
				}				
			}
		}
				
		if( $_POST['formname'] == 'fetcharea' ){
			$table = "tbl_areas";
			$condition = " WHERE seviceID=".$_POST['id']." AND status= 1";
			$cols = "areaID,areaName,price";
			$areainfo = $fullinfo->selectTableRowsNew($table,$condition,$cols);
			$resultarray = array();
			if(!empty($areainfo)){
				$areaall = '';
				foreach($areainfo as $area){
					$areaall.= '<option value="'.$area['areaID'].'">'.ucfirst($area['areaName']).' - $'.$area['price'].'</option>';
				}
				$result['status'] = 1;
				$result['message'] = $areaall;
			} else {
				$result['status'] = 0;
				$result['message'] = '<option value="">No area found, Select another service.</option>';
			}
			echo json_encode($result);
			die;
		}
				
		if( $_POST['formname'] == 'framestep' ){
			
			if( $_POST['step'] == 1 ){
				unset($_SESSION['frame']);
				$_SESSION['frame']['bussinessid'] = $_POST['bussinessid'];
				$_SESSION['frame']['locationid'] = $_POST['locationid'];
				$newstring = '';
				for($j = 0; $j < count($_POST['service']); $j++){
					$_SESSION['frame']['service'][$j] = $_POST['service'][$j];
					$_SESSION['frame']['area'][$j] = $_POST['area'][$j];
					$_SESSION['frame']['staff'][$j] = $_POST['staff'][$j];

					$service[$j] = $fullinfo->selectTableSingleRowNew('tbl_services','WHERE seviceID ='.$_POST['service'][$j],'serviceName');
					$serviceName[$j] = ucfirst($service[$j]['serviceName']);
					if( !empty($_POST['area'][$j]) && $_POST['area'][$j] != 0 ){
						$area[$j] = $fullinfo->selectTableSingleRowNew('tbl_areas','WHERE areaID ='.$_POST['area'][$j],'areaName,price');
						$areaName[$j] = ucfirst($area[$j]['areaName']).'- $'.$area[$j]['price'];					
					} else {
						$areaName[$j] = "Free consultion";
					}

					if( $_POST['staff'][$j] == 0 ){
						$employeeName[$j] = 'Any';
					} else {
						$employee[$j] = $fullinfo->selectTableSingleRowNew('tbl_employees','WHERE Emp_ID ='.$_POST['staff'][$j],'First_Name,Last_Name');
						$employeeName[$j] = ucfirst($employee[$j]['First_Name'].' '.$employee[$j]['Last_Name']);					
					}
						
					$newstring .= '<div class="la-seltype"><span>'.$serviceName[$j].', '.$areaName[$j].', '.$employeeName[$j].'</span><a><img class="cross" src="iframe-img/cross.svg"></a></div>';
				}
				
				$_SESSION['frame']['selection'] = $newstring;

				if(isset($_SESSION['frame'])){
					$result['status'] = 1;
					$result['message'] = $_SESSION['frame']['selection'];
				} else {
					$result['status'] = 0;
				}
				echo json_encode($result);	
			} else if( $_POST['step'] == 2 ){
				if( $_POST['submit'] == 'date') {
					
					if(count($_SESSION['frame']['service']) > 0 ) {
						
						$time = $_POST['date'].' '.$_POST['time'];
						$message = $newtime = $serviceid = '';
						for($j = 0; $j < count($_SESSION['frame']['service']); $j++){
							if($newtime == ''){
								$newtime = $_POST['time'];
							}else {
								$newtime = $newtime;
							}
							$time = $_POST['date'].' '.$newtime;	
							$table = "tbl_employees_leave";
							$condition = " WHERE employeeID=".$_SESSION['frame']['staff'][$j]." AND '".$time."' BETWEEN startDate AND endDate";
							$cols = "id";
							$empinfo = $fullinfo->selectTableRowsNew($table,$condition,$cols);				
							$result = array();
							if(empty($empinfo)){
								if( $j > 0 ){
									if( $serviceid != $_SESSION['frame']['service'][$j] ){
										$table1 = "tbl_services";
										$condition1 = " WHERE seviceID=".$_SESSION['frame']['service'][$j];
										$cols1 = "bufferTime";
										$serviceinfo = $fullinfo->selectTableRowsNew($table1,$condition1,$cols1);
										$buffertime = $serviceinfo[0]['bufferTime'];
										$buffertime = strtotime("+".$buffertime."minutes", strtotime($newtime));
										$newtime = date('H:i', $buffertime);
										$time = $_POST['date'].' '.$newtime ;	
									} else {
									
									}								
								}
								$serviceid = $_SESSION['frame']['service'][$j];
								if( $_SESSION['frame']['area'][$j] == 0 ){
									$message .= '<li><a>'.$newtime.'-'.$newtime.'</a></li>';
									$_SESSION['frame']['time-slot'][$j] = $newtime.'-'.$newtime;
									$newtime = $newtime;
									$endDate = $_POST['date'].' '.$newtime;
									$_SESSION['frame']['startDate'][$j] = $time;
									$_SESSION['frame']['endDate'][$j] = $endDate;
								} else {
									$table = "tbl_areas";
									$condition = " WHERE areaID=".$_SESSION['frame']['area'][$j];
									$cols = "durationTime";
									$timeinfo = $fullinfo->selectTableRowsNew($table,$condition,$cols);
									$endTime = strtotime("+".$timeinfo[0]['durationTime']."minutes", strtotime($newtime));
									$durationTime[$j] = date('H:i', $endTime);
									$message .= '<li><a>'.$newtime.'-'.$durationTime[$j].'</a></li>';
									$_SESSION['frame']['time-slot'][$j] = $newtime.'-'.$durationTime[$j];
									$newtime = $durationTime[$j];
									$endDate = $_POST['date'].' '.$newtime;
									$_SESSION['frame']['startDate'][$j] = $time;
									$_SESSION['frame']['endDate'][$j] = $endDate;
								}
								$result['status'] = 1;
							} else{
								$message = "Technician not available. Please select another time slot.";
								$result['status'] = 0;
							}
						}
						$result['message'] = $message;
						echo json_encode($result);
					}
				} else if( $_POST['submit'] == 'form') {
					//~ print_r($_SESSION);
					echo 1;
				}
			} else if( $_POST['step'] == 3 ) {
				$_SESSION['frame']['patientName'] = $_POST['pname'];
				$_SESSION['frame']['patientEmail'] = $_POST['pemail'];
				$_SESSION['frame']['patientPhone'] = $_POST['pphone'];
				$_SESSION['frame']['patientDescription'] = $_POST['pmessage'];
				if(isset($_SESSION['frame'])){
					echo 1;
				} else {
					echo 0;
				}
			} else if( $_POST['step'] == 4 ) {
				
				if( $_POST['agreecheckbox'] == 'on'){
					$table = 'tbl_appointment';

					$data['businessID'] = $_SESSION['frame']['bussinessid'];
					$data['employeeID'] = serialize($_SESSION['frame']['staff']);
					$data['locationID'] = $_SESSION['frame']['locationid'];
					$data['serviceID'] = serialize($_SESSION['frame']['service']);
					$data['areaID'] = serialize($_SESSION['frame']['area']);
					$data['patientName'] = $_SESSION['frame']['patientName'];
					$data['patientEmail'] = $_SESSION['frame']['patientEmail'];
					$data['patientPhone'] = $_SESSION['frame']['patientPhone'];
					$data['patientDescription'] = $_SESSION['frame']['patientDescription'];
					$data['startDate'] = serialize($_SESSION['frame']['startDate']);
					$data['endDate'] = serialize($_SESSION['frame']['endDate']);
					$fullinfo->insert_data($table,$data);
					$id=mysqli_insert_id($fullinfo->con);					
					
					/*
					for($a = 0; $a < count($_SESSION['frame']['service']); $a++){
						$data['businessID'] = $_SESSION['frame']['bussinessid'];
						$data['employeeID'] = $_SESSION['frame']['staff'][$a];
						$data['locationID'] = $_SESSION['frame']['locationid'];
						$data['serviceID'] = $_SESSION['frame']['service'][$a];
						$data['areaID'] = $_SESSION['frame']['area'][$a];
						$data['patientName'] = $_SESSION['frame']['patientName'];
						$data['patientEmail'] = $_SESSION['frame']['patientEmail'];
						$data['patientPhone'] = $_SESSION['frame']['patientPhone'];
						$data['patientDescription'] = $_SESSION['frame']['patientDescription'];
						$data['startDate'] = $_SESSION['frame']['startDate'][$a];
						$data['endDate'] = $_SESSION['frame']['endDate'][$a];
						$fullinfo->insert_data($table,$data);
						$id=mysqli_insert_id($fullinfo->con);					
					
					}
					*/
					if(isset($id)) {
						echo  "1";
						unset($_SESSION['frame']);
					} else {
						echo "0";
					}
				} else {
					echo 'Please agree with the terms and conditions.';
				}
			}

		}
			
	}
?>
