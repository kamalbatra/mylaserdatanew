<?php
	include("../includes/dbFunctions.php");
	$AddPrice = new dbFunctions();
	$Table = "tbl_pricing_table";
	$STable = "tbl_master_sizes";
	$ATable = "tbl_master_areas";
	$Data["Rec_Price"] = $_POST["Rec_Price"];
	$Data["discount5"] = $_POST["discount5"];
	$Data["discount10"] = $_POST["discount10"];
	$Data["savings5"] = $_POST["savings5"];
	$Data["savings10"] = $_POST["savings10"];
	$Data["Location"] = $_POST["Location"];
	$Data["BusinessID"] = $_SESSION["BusinessID"];
	if(!isset($_POST["Area"])) {
		$Data["Size"] = $_POST["size"];
		$Result = $AddPrice->insert_data($Table,$Data);
		if( $Result != 1 ) 
			echo "Error Occured.";
		else 
			echo "Yes"; 
	}
	else {
		$Size = $_POST["size"];
		$SCond = "where id=".$Size;
		$SCols = "size";
		$SName = $AddPrice->selectTableSingleRow($STable,$SCond,$SCols);
		$Data["Size"] = $SName["size"];
		
		$Area = $_POST["Area"];
		$ACond = "where id=".$Area;
		$ACols = "area";
		$AName = $AddPrice->selectTableSingleRow($ATable,$ACond,$ACols);
		$Data["Area"] = $AName["area"];
		$Result = $AddPrice->insert_data($Table,$Data);
		if( $Result != 1 ) 
			echo "Error Occured.";
		else 
			echo "Yes"; 
	}
?>
