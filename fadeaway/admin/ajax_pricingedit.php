<?php
//error_reporting(0);
include("../includes/config.php");
include("../includes/dbFunctions.php");
$pricingDetails	= new dbFunctions();
$domain=$_SERVER['DOMAIN'];
?>

<?php

if(isset($_POST['Size'])){
	$table	   = "tbl_pricing_table";
	$condition = "WHERE pid = '".$_POST['pid']."'  and Size='".$_POST['Size'] ."' ";
	$sizedata  = $pricingDetails->selectTableSingleRow($table,$condition);
		
}
?>
<div class="popupblockOuter">
	<div class="popoutertb">
		<div class="popoutercell">
			<div class="popoutercontent popoutereditcal">
				<form method="" name="pricingform" id="pricingform">
					<div class="popcontentmain">	
						<input type="hidden" class="text-input-field" name="pid" id="pid"  value="<?php echo $_POST['pid']; ?>"/>
						<input type="hidden" class="text-input-field" name="Size"  id="Size"  value="<?php echo $_POST['Size']; ?>"/>
					    <input type="hidden" class="text-input-field"  id="page"  value="<?php echo $_POST['page']; ?>"/>
						<h2>
							Edit Price For Tattoo Renewal
							<img src="<?php echo $domain; ?>/img/cross.png" class="crossiconblk">
						</h2> 
						<div class="sessionOuterBlock">
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Recommended Price:</label>
											<input type="text" class="text-input-field pricingclass" name="Rec_Price" id="Rec_Price"  value="<?php echo $sizedata['Rec_Price'] ?>"/>
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Three Session Discounted Price:</label>
											<input type="text" class="text-input-field pricingclass" name="discount5" id="discount5" value="<?php echo $sizedata['discount5'] ?>"/>
										</div>
									</div>
								</div>
							</div>

							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Six Session Discounted Price:</label>
											<input type="text" class="text-input-field pricingclass" name="discount10" id="discount10" value="<?php echo $sizedata['discount10'] ?>"/>
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Cost Savings for Three Sessions:</label>
											<input type="text" class="text-input-field pricingclass" name="savings5" id="savings5"  value="<?php echo $sizedata['savings5'] ?>"/>
										</div>
									</div>
								</div>
							</div>

							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Cost Savings for Six Sessions:</label>
											<input type="text" class="text-input-field pricingclass" name="savings10" id="savings10"  value="<?php echo $sizedata['savings10'] ?>"/>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="form-row-ld">
							<div class="backNextbtn">
								<button type="button" id="updateBtn" value="Add" class="submit-btn nextbtn" style="float:left;">UPDATE</button>
							</div>
						</div>
					</div>
					<div>
						<span id="pricingMsg" class="commonerror"></span>
						<span id="pricingMsgUpdate" class="successUpdate"></span>
						<!-- span id="pricingMsgUpdateLoding" style="display:none;"><img src="<?php echo $domain; ?>/images/loading.gif"></span -->
					</div>
				</form>


			</div>
		</div>
	</div>
</div>
