<?php
	session_start();
	include('SimpleImage.php');
	//$path = "uploads/";
	$path = "Business-Uploads/".$_SESSION['BusinessID']."/";
	$thumbpath = "Business-Uploads/".$_SESSION['BusinessID']."/thumbnail/";
	function getExtension($str) {
		ini_set( 'upload_max_size' , '64M' );
		$i = strrpos($str,".");
		if (!$i) { return ""; } 
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
	$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
		$name = $_FILES['photoimg']['name'];
		$size = $_FILES['photoimg']['size'];			
		if(strlen($name)) {
			$ext = getExtension($name);
			if(in_array($ext,$valid_formats)) {
				if($size>0)	{
					$actual_image_name 	= time().".".$ext;
					$Del_image_name 	= time();
					$tmp 				= $_FILES['photoimg']['tmp_name'];
					$numberImg 			= 1;
					if(move_uploaded_file($tmp, $path.$actual_image_name)) {									
						//mysql_query("UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");									
						//echo "<div class='imgupload'><img src='uploads/".$actual_image_name."'  class='preview' width='50px'height='50px'></div>";
						//echo "<div class='imgupload'><img src='uploads/thumbs/thumb".$actual_image_name."'  class='preview'>";									
						echo "<div class='imgupload' id=".$Del_image_name.">
							<a id=".$Del_image_name." href='#' class='remove_img'><i class='fa fa-times'></i></a>
							<img src='".$thumbpath.$actual_image_name."'  class='preview'>";								
						$updir = $thumbpath;									
						//$id= rand(1,100);
						makeThumbnails($updir, $actual_image_name);
						$image = new SimpleImage();
						$image->load($path.$actual_image_name);
						$image->resize(100,100);
						$image->save($thumbpath.$actual_image_name) ;									 
						list($width, $height) = getimagesize($path.$actual_image_name);
						if($width > 900 ) {
							$width="900";
						} else {
							$width=$width;
						}
						if($height >800) {
							$height ="800";
						} else {
							$height =$height;
						}
						if($width !="") {
							$image->load($path.$actual_image_name);
							$image->resize($width,$height);
							$image->save($path."resize/".$actual_image_name) ;
						}										
					?>
						<script language="javascript">
							changeIt();			
							function changeIt() {								
								my_div.innerHTML = my_div.innerHTML +"<input type='hidden' name='Tattoo_Photo_thumbnail[]' id='img_<?php echo $Del_image_name;?>' value='<?php echo $actual_image_name; ?>'>";
							}						
						</script>	
					<?php
						$numberImg++;		
					} 
					else
						echo "Fail upload folder with read access.";
				}
				else
					echo "Image file size max 2.3 MB";					
			}
			else
				echo "Invalid file format..";	
		}	
	}			
	function makeThumbnails($updir, $img, $id=null) {
		$thumbnail_width =80;
		$thumbnail_height =80;
		$thumb_beforeword = "thumb";
		if( file_exists ($updir.$img) ) {
			$arr_image_details = getimagesize("$updir"."$img"); // pass id to thumb name
		} else {
			$arr_image_details = 0;
		}
		if( $arr_image_details != 0 ) {		
			$original_width 	= $arr_image_details[0];
			$original_height 	= $arr_image_details[1];
		} else {
			$original_width 	= 0;
			$original_height 	= 0;
		}
		if ($original_width > $original_height) {
			$new_width 	= $thumbnail_width;
			$new_height = intval($original_height * $new_width / $original_width);
		} else if($original_height != 0) {
			$new_height = $thumbnail_height;
			$new_width 	= intval($original_width * $new_height / $original_height);
		} else {
			$new_height = 0;
			$new_width 	= 0;
		}
		$dest_x = intval(($thumbnail_width - $new_width) / 2);
		$dest_y = intval(($thumbnail_height - $new_height) / 2);
		if ($arr_image_details[2] == 1) {
			$imgt = "ImageGIF";
			$imgcreatefrom = "ImageCreateFromGIF";
		}
		if ($arr_image_details[2] == 2) {
			$imgt = "ImageJPEG";
			$imgcreatefrom = "ImageCreateFromJPEG";
		}
		if ($arr_image_details[2] == 3) {
			$imgt = "ImagePNG";
			$imgcreatefrom = "ImageCreateFromPNG";
		}
		if (isset($imgt)) {
			$old_image = $imgcreatefrom("$updir"  . "$img");
			$new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
			imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
			//$imgt($new_image, "$updir"."/thumbs/" . "$thumb_beforeword" . "$img");
			$imgt($new_image, "$updir".$thumb_beforeword.  "$img");
		}
	}
?>
