<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	if($_SESSION['loginuser'] != "sitesuperadmin"){
		?>
		<script>
			$(function(){
				window.location.replace("dashboard?msg=noper");
			});
		</script>
		<?php
		die;
	}
	$history = new dbFunctions();
	$subtable = "tbl_subscription_history";
	$joincondition = "tbl_subscription_history.BusinessID = tbl_business.BusinessID where PaymentStatus != 'Success' order by ID desc";
	$adjacents = 3;
	$reload="all_transactions";
	$cols = "tbl_business.BusinessName, tbl_subscription_history.RenewalDate, tbl_subscription_history.ExpireDate, tbl_subscription_history.PaymentAmount, tbl_subscription_history.TransactionID";
	$total_pages = $history->joinTotalNumRows("tbl_subscription_history","tbl_business", "LEFT", $joincondition,$cols);
	if(isset($_GET['page'])) {
		$page=$_GET['page'];
	} else {
		$page="";
	}
	$limit = 5;                                  //how many items to show per page
	if($page)
		$start = ($page - 1) * $limit;          //first item to display on this page
	else
		$start = 0; 
	
	$joincondition = "tbl_subscription_history.BusinessID = tbl_business.BusinessID where PaymentStatus != 'Success' order by ID desc limit ".$start.",".$limit;
	$subscription = $history->selectTableJoin("tbl_subscription_history","tbl_business", "LEFT", $joincondition,$cols);

?>
<style type="text/css">
	.srtHead { 
		width: 14%; 
	}
	.srno {
		width: 8%;
	}
	.span3 {
		text-align: center;
	}
	.trans {
		width: 20%;
	}
</style>

<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Transactions History</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="card-body">
					<?php
						if( !empty($subscription) ) {
							$i = 0;
							$srno=$start+1;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>Business</th>
										<th>Transaction ID</th>
										<th>Membership Fees</th>
										<th>Duration</th>
										<th>Renewal Date</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $subscription as $subdata ) {
											if($i%2==0) {
												$bgdata = "bgnone";	
											} else {
												$bgdata = "bgdata";
											}
									?>
												<tr class="<?php echo $bgdata;?>">
													<td class="span3 srtHeadEditEmp srtcontent"><label id="" class="user-name"><?php echo $srno; ?> </label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $subdata["BusinessName"]; ?></label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $subdata["TransactionID"]; ?></label></td>
													<td class="span6 srtHeadloc srtcontent"><label id="" class="user-name amt"><?php echo "$".$subdata["PaymentAmount"]; ?></label></td>
													<td class="span6 cMain "><label id="" class="user-name"><?php echo date("M j, Y", strtotime($subdata["RenewalDate"]))." - ".date("M j, Y", strtotime($subdata["ExpireDate"])); ?></label></td>
													<td class="span6 cMain "><label id="" class="user-name"><?php echo date("M j, Y", strtotime($subdata["ExpireDate"])); ?></label></td>
												</tr><!--End @row-block-->
												<?php
												$i++; $srno++;
										} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $history->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
