<!DOCTYPE html>
<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$clintInfo = new dbFunctions();
	$tableConsent1="tbl_treatment_log";
	$cols="*";
	$condition="where ClientID =".base64_decode($_GET['ClientID'])."  AND  Tattoo_Photo_thumbnail !='' ";
	$checkExistence = $clintInfo->selectTableRows($tableConsent1,$condition,$cols);
	//print_r($checkExistence);
?>	
<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/css_gallery/demo.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/css_gallery/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/css_gallery/elastislide.css" />			
<noscript>
	<style>
		.es-carousel ul{
			display:block;
		}
	</style>
</noscript>
<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
	<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
		{{/if}}
		<div class="rg-image"></div>
		<div class="rg-loading"></div>
		<div class="rg-caption-wrapper">
			<div class="rg-caption" style="display:none;">
				<p></p>
			</div>
		</div>
	</div>
</script>	
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Treatments log images</h1>
		<div class="addNew"><a class="empLinks" href="clientdetails" class="submit-btn">Client list </a></div>
	</div>	
	<div class="user-entry-block">
	<?php //print_r ($checkExistence); 
	if($checkExistence !=NULL){?>	
		<div id="rg-gallery" class="rg-gallery">
			<div class="rg-thumbs">
			<!-- Elastislide Carousel Thumbnail Viewer -->
				<div class="es-carousel-wrapper">
					<div class="es-nav">
						<span class="es-nav-prev">Previous</span>
						<span class="es-nav-next">Next</span>
					</div>
					<div class="es-carousel">
						<ul>					
						<?php
							$lk = 0;
							foreach($checkExistence as $data)	{ 
								//echo $lk;
								if($data['Tattoo_Photo_thumbnail'] !="")	{
									$imgSrc = explode(",",$data['Tattoo_Photo_thumbnail']);
									for($k =0; $k < count($imgSrc);$k++){					
										//$filename = "uploads/thumbnail/". $imgSrc[$k];
										$filename = "Business-Uploads/".$_SESSION['BusinessID']."/thumbnail/". $imgSrc[$k];
											if(file_exists($filename)) { 
												$date = new DateTime($data['Date_of_treatment']);
												$date->setTimezone(new DateTimeZone('CST')); // +04
												$treatment=$date->format('Y-m-d H:i:s'); // 2012-07-15 05:00:00 
												date_default_timezone_set(timezone_name_from_abbr("UTC"));
												
												?>								 
												<li><a href="#"><img src="<?php echo $filename; ?>" data-large='<?php echo "Business-Uploads/$_SESSION[BusinessID]/resize/$imgSrc[$k]"; ?>' alt="image01" data-description="Session : <?php echo $data['SessionNumber']; ?>&nbsp; Date : <?php echo $treatment//$data['Date_of_treatment']; ?>" /></a></li>	
											<?php 
											} else {
											?>				
												<li><a href="#"><img src="uploads/thumbnail/noimg.jpg" data-large="uploads/resize/noimg.jpg" alt="image01" data-description="Session :<?php echo $checkExistence[$lk]['SessionNumber'];?>&nbsp; Date :<?php echo $checkExistence[$lk]['Date_of_treatment'];?>" /></a></li>	
											<?php	
											}//close for loop 
									} 
								} 
								$lk++; 
							} //foreach
						?>
						</ul>
					</div>
				</div>
				<!-- End Elastislide Carousel Thumbnail Viewer -->
			</div><!-- rg-thumbs -->
		</div><!-- rg-gallery -->
	<?php 
	} else { 
	?>
		<div> No Prior record found! </div>	  
 <?php 
	} ?> 
	<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/jquery.elastislide.js"></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/gallery.js"></script>
	</div><!--user-entry-block-->
</div><!--form-container-->
</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
