<?php
	header('Content-Type: application/json');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	$location_con = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY LocationName";
	$locations = $empInfo->selectTableRows('tbl_manage_location', $location_con);
	$oldlocation = '';
	$arra = array();
	$uniqueids = array();
	$locationids = array();
	foreach($locations AS $location){
		if($oldlocation != $location['LocationName']){
			$i = 0;
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
			$oldlocation = $location['LocationName'];
			array_push($locationids,$location['ManageLocationId']);
		}else{
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
			array_push($locationids,$location['ManageLocationId']);
		}	
		$i++;
	}
	$locationids = implode(',',$locationids);
	$year = date('Y');
	
	$result = mysqli_query($empInfo->con,"SELECT MONTH(Date_of_treatment) AS m, SUM(Price) as price FROM `tbl_treatment_log` WHERE YEAR(Date_of_treatment) = '".$year."' AND `Location` IN (".$locationids.") GROUP BY m ORDER BY m ASC");
	$row = array();
	$rows = array();
	$j=1;
	while($r = mysqli_fetch_assoc($result)) {
		if($r['m'] == 1){
			$month = 'Jan';
		} else if($r['m'] == 2){
			$month = 'Feb';
		} else if($r['m'] == 3){
			$month = 'Mar';
		}
		 else if($r['m'] == 4){
			$month = 'April';
		}
		 else if($r['m'] == 5){
			$month = 'May';
		}
		$rows[$j]['m'] = $month;
		$rows[$j]['price'] = (int)$r['price'];
		$j++;
	}
	echo json_encode($rows);
?>
<?php
//setting header to json
//~ header('Content-Type: application/json');
//~ echo '[{"m":"2"},{"price":26654},{"m":"3"},{"price":24944},{"m":"4"},{"price":2455},{"m":"1"},{"price":27007}]';
?>
