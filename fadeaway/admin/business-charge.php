<?php $domain=$_SERVER['DOMAIN']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
	<title>Dashboard</title>
	<link href="<?php echo $domain; ?>/css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/theme4.css" />
	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://www.mylaserdata.com/heartland-php-master/examples/end-to-end/assets/secure.submit-1.0.2.js"></script>
	<script type="text/javascript" src="https://hps.github.io/token/2.1/securesubmit.js"></script>
</head>
<?php session_start(); ?>
<body>
	<div class="header-top-bar"></div>
		<div class="container">
		<div class="admin-login">  
			<h6>
			<?php
				if($_SESSION['Superadmin']!="1" && $_SESSION["period"]=="true")	{ 
			?> 
				<span id="totalPandingReview" style=""> </span>	     
			<?php
				}
			?>	     
			<?php
				if($_SESSION['Superadmin']=="0" && $_SESSION['Usertype']=="Admin") {
			?>
					<a href="profile.php?id=<?php echo $_SESSION['BusinessID']; ?>" title="View Profile">
						<span class="user-icon">Welcome 
						<?php echo $_SESSION['First_Name']." ".$_SESSION['Last_Name']; ?>
						</span>
					</a>
				<?php
					} else{ 
				?>
					<span class="user-icon">Welcome 
							<?php echo $_SESSION['First_Name']." ".$_SESSION['Last_Name']; ?>
						</span> 
						<?php } ?>| <a href="<?php echo $domain; ?>/includes/login.php?logout=true"><span class="signout">Signout</span></a>
			</h6>
		</div>
		<div class="logo">
			<?php if($_SESSION["period"] == "true"){ ?>
				<a href="dashboard">
			 <?php } ?>
			<?php
				if(isset($_SESSION['businessdata']['Logo']) && $_SESSION['businessdata']['Logo'] != "" && file_exists("Business-Uploads/$_SESSION[BusinessID]/".$_SESSION['businessdata']['Logo'])) { 
				?>
					<img alt="logo" src="Business-Uploads/<?php echo $_SESSION['BusinessID'].'/'.$_SESSION['businessdata']['Logo']; ?>" />
				<?php
					}else{
				?>
					<img alt="logo" src="<?php echo $domain; ?>/images/logo.jpg" />
				<?php }	?>
			<?php if($_SESSION["period"] != "false"){ ?>
				</a>
			 <?php } ?>
		</div>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Card Information </h1>
		<div class="addNew">
			<a class="empLinks" href="devicedetails.php" class="submit-btn">Select Plan </a>
		</div>
	</div>
	<div class="user-entry-block">
		<div id="horizontalTab">
			<div class="resp-tabs-container">
				<div class="row treatment">
					<form class="payment_form form-horizontal" id="payment_form" action="https://www.mylaserdata.com/heartland-php-master/examples/recurring-signup-ach/charge.php" method="POST">
						<input type="hidden" name="FirstName" value="<?php echo $_SESSION['First_Name']; ?>" />
						<input type="hidden" name="LastName" value="<?php echo $_SESSION['Last_Name']; ?>" />
						<input type="hidden" name="Email" value="<?php echo $_SESSION['Emp_email']; ?>" />
						<input type="hidden" name="payment_amount" value="17" />
						<div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;">
								<label class="user-name">Card Number:</label>
							</div>
							<div class="span6">
								<input type="hidden" id="card_number" />
								<input type="text" class="text-input-field" name="card_number" id="cardNumber" />
								<label class="error card-error-message"></label>
							</div>	  
						</div>	
						<div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;">
								<label class="user-name">CVC:</label>
							</div>
							<div class="span6">
								<input type="text" id="card_cvc" name="card_cvc" class="text-input-field" />
							</div>
						</div>
						<div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;">
								<label class="user-name">Expiration Date:</label>
							</div>
							<div class="span3" style="margin:6px 6px 3px 37px; width:158px">
								<select class="select-option" id="exp_month" name="exp_month">
									<option>01</option>
									<option>02</option>
									<option>03</option>
									<option>04</option>
									<option>05</option>
									<option>06</option>
									<option>07</option>
									<option>08</option>
									<option>09</option>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>
							</div>
							<div class="span3" style="margin:6px 6px 3px 10px; width:158px">
								<select class="select-option" id="exp_year" name="exp_year">
									<?php for($y=date("Y"); $y<=(date("Y")+20); $y++) { ?>
									<option><?php echo $y; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="row treatment">
							<div class="span3"></div>
							<div class="span6">
								<input type="submit" value="Submit Payment" id="PaymentButton" class="submit-btn" />
							</div>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	(function (document, Heartland) {
		Heartland.Card.attachNumberEvents('#cardNumber');
		Heartland.Card.attachCvvEvents('#card_cvc');
	}(document, Heartland));
	
	jQuery(function(){
		jQuery("input[name='card_number']").change(function(){
			jQuery("#card_number").val((jQuery("input[name='card_number']").val()).replace(/ /g,''));
		});
		
		/*jQuery("#payment_form").validate({
			rules: {
				card_number: "required",
				card_cvc: "required",
			},
			messages: {
				card_number: "Please enter your card number.",
				card_cvc: "Please enter your card cvc."
			},
		});*/
	});
	jQuery("#payment_form").SecureSubmit({
		public_key: "pkapi_cert_kr6L8m3cUA5bxm834X",
		error: function (response) {
			jQuery(".card-error-message").text(response.message).show();
		}
	});
</script>
</body>
</html>
