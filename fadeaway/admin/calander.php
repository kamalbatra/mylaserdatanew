<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include('admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();

	$table1 = "tbl_appointment";
	if( $_SESSION['Admin'] == 'TRUE') {
		$condition1 = "where businessID=".$_SESSION['BusinessID'];	
	} else if( $_SESSION['Admin'] == 'FALSE') { 
		$condition1 = "where employeeID LIKE '%".$_SESSION['id']."%'";
	}	
	$cols1 = "*";
	$employeeleavedata = $empInfo->selectTableRowsNew($table1,$condition1,$cols1);

	$table1 = "tbl_manage_location";
	if( $_SESSION['Admin'] == 'TRUE') {
		$condition1 = "where businessID=".$_SESSION['BusinessID']." GROUP BY LocationName";
	} else if( $_SESSION['Admin'] == 'FALSE') { 
		$condition1 = "where ManageLocationId=".$_SESSION['Location'];	
	}	
	$cols1 = "ManageLocationId,LocationName";
	$employeedata = $empInfo->selectTableRowsNew($table1,$condition1,$cols1);

	$calanderdata = array();
	if( !empty($employeeleavedata) ){
		
		for( $i=0;$i< count($employeeleavedata); $i++){
			$clinicdata = array();
			$clinicdatastring = '';	

			$calanderdata[$i]['id'] = $employeeleavedata[$i]['appointmentID'];
			$calanderdata[$i]['locationID'] = $employeeleavedata[$i]['locationID'];
			
			$locationname = $empInfo->selectTableSingleRowNew('tbl_manage_location','WHERE ManageLocationId ='.$employeeleavedata[$i]['locationID'],'LocationName');
			$calanderdata[$i]['locationName'] = ucfirst($locationname['LocationName']);
			
			$employeeresult = unserialize($employeeleavedata[$i]['employeeID']);
			for($j=0; $j<count($employeeresult); $j++){
				if( $employeeresult[$j] == 0) {
					$clinicdata[$j]['employeedata'] = 'Any';
				} else {
					$employeename = $empInfo->selectTableSingleRowNew('tbl_employees','WHERE Emp_ID ='.$employeeresult[$j],'First_Name,Last_Name');
					$clinicdata[$j]['employeedata'] = ucfirst($employeename['First_Name']).' '.ucfirst($employeename['Last_Name']);
				}
			}
			
			$serviceresult = unserialize($employeeleavedata[$i]['serviceID']);
			for($j=0; $j<count($serviceresult); $j++){
				$servicename = $empInfo->selectTableSingleRowNew('tbl_services','WHERE seviceID ='.$serviceresult[$j],'serviceName');
				$clinicdata[$j]['servicedata'] = ucfirst($servicename['serviceName']);
			}
			
			$arearesult = unserialize($employeeleavedata[$i]['areaID']);
			for($j=0; $j<count($arearesult); $j++){
				$areaname = $empInfo->selectTableSingleRowNew('tbl_areas','WHERE areaID ='.$arearesult[$j],'areaName');
				$clinicdata[$j]['areadata'] = ucfirst($areaname['areaName']);
			}
			
			$startDateresult = unserialize($employeeleavedata[$i]['startDate']);
			for($j=0; $j<count($startDateresult); $j++){
				$appointmentstartdate = $startDateresult[0];
				$clinicdata[$j]['appointmentstarttime'] = date("h:i A",strtotime($startDateresult[$j]));
			}
			
			$endDateresult = unserialize($employeeleavedata[$i]['endDate']);
			for($j=0; $j< count($endDateresult); $j++){
				$appointmentenddate = $endDateresult[count($endDateresult) - 1];
				$clinicdata[$j]['appointmentendtime'] = date("h:i A",strtotime($endDateresult[$j]));
			}
		
			$clinicdatastring ='<div class="clinicdatacointainer">';
			foreach( $clinicdata as $clinicdataindi){
				$clinicdatastring .= '<div class="datacontainer"><div class="appointmenttime"><span class="schedule-heading">Appointment Time</span><span class="schedule-value">'.$clinicdataindi['appointmentstarttime'].' - '.$clinicdataindi['appointmentendtime'].'</span></div><div class="employeename"><span class="schedule-heading">Employee Name</span><span class="schedule-value">'.$clinicdataindi['employeedata'].'</span></div><div class="servicename"><span class="schedule-heading">Service Name</span><span class="schedule-value">'.$clinicdataindi['servicedata'].'</span></div><div class="areaname"><span class="schedule-heading">Area to be treated</span><span class="schedule-value">'.$clinicdataindi['areadata'].'</span></div></div>';
			}
			
			$clinicdatastring .='</div>';
			$calanderdata[$i]['clinicdata'] = $clinicdatastring; 
			
			$calanderdata[$i]['serviceID'] = $employeeleavedata[$i]['serviceID'];
			$calanderdata[$i]['areaID'] = $employeeleavedata[$i]['areaID'];

			$calanderdata[$i]['patientName'] = $employeeleavedata[$i]['patientName'];
			$calanderdata[$i]['patientEmail'] = $employeeleavedata[$i]['patientEmail'];
			$calanderdata[$i]['patientPhone'] = $employeeleavedata[$i]['patientPhone'];
			$calanderdata[$i]['patientDescription'] = nl2br($employeeleavedata[$i]['patientDescription']);
						
			$calanderdata[$i]['startDate'] = $employeeleavedata[$i]['startDate'];
			$calanderdata[$i]['endDate'] = $employeeleavedata[$i]['endDate'];

			$calanderdata[$i]['startDate1'] = date("M d,Y",strtotime($appointmentstartdate)).' '.date("h:i A",strtotime($appointmentstartdate)).' - '.date("M d,Y",strtotime($appointmentenddate)).' '.date("h:i A",strtotime($appointmentenddate));
			
			$startDate = unserialize($employeeleavedata[$i]['startDate']);
			$endDate = unserialize($employeeleavedata[$i]['endDate']);
			/*************** Mandatory **********************/
			$calanderdata[$i]['title'] = $calanderdata[$i]['patientName'];
			$calanderdata[$i]['start'] = $appointmentstartdate;
			$calanderdata[$i]['end'] = $appointmentenddate;
			/*************** Mandatory *********************/
		}
	} 
	$calanderdata = JSON_encode($calanderdata);
?>	
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/core/main.css' rel='stylesheet' />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/timegrid/main.css' rel='stylesheet' />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/list/main.css' rel='stylesheet' />
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/core/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/interaction/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/daygrid/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/timegrid/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/list/main.js'></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var calendarEl = document.getElementById('calendar');
			var timer;
			var calendar = new FullCalendar.Calendar(calendarEl, {
				plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
				defaultView: 'dayGridMonth',
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
				},
				defaultDate:  '<?php echo date("Y-m-d"); ?>',
				navLinks: true,
				editable: true,
				allDay:true,
				timeFormat:'h(:mm)tt',
				events: <?php echo $calanderdata; ?>,
				eventClick: function(info) {
					var eventid = info.event.id;
					var patientname = info.event.extendedProps.patientName;
					var patientemail = info.event.extendedProps.patientEmail;
					var patientphone = info.event.extendedProps.patientPhone;
					var eventdesc = info.event.extendedProps.patientDescription;
					var clinicdata = info.event.extendedProps.clinicdata;
					var eventDate = info.event.extendedProps.startDate1;
					
					$('#deleteevent').attr('idattr',eventid);
					$('#editModal').show();
					
					$('#view-schedule-content').append('<div class="schedule-row-outer"><div class="schedule-row"><span class="schedule-heading">Time of treatment</span><span class="schedule-value">'+eventDate+'</span></div><div class="schedule-row"><span class="schedule-heading">Patient'+"'s"+' Name</span><span class="schedule-value">'+patientname+'</span></div><div class="schedule-row"><span class="schedule-heading">Patient'+"'s"+' Email Address</span><span class="schedule-value">'+patientemail+'</span></div><div class="schedule-row"><span class="schedule-heading">Patient'+"'s"+' Phone</span><span class="schedule-value">'+patientphone+'</span></div><div class="schedule-row"><span class="schedule-heading">Reason for treatment</span><span class="schedule-value">'+eventdesc+'</span></div><div class="schedule-row">'+clinicdata+'</div></div>');
				},
			   eventRender: function eventRender( info ) {
				   //~ var locationid = $('#location_selector').val();
				   return ['all',info.event.extendedProps.locationID].indexOf($('#location_selector').val()) >= 0
				},
				/*	
				eventMouseEnter: function(info) {
					timer = setTimeout(function() {
						$("#showModal").html("<div class='primary_heading'><span style='font-weight:bold; font-size:16px;'>Event Description</span><span style='font-weight:bold; font-size:16px;margin-left:10px'>"+info.event.extendedProps.description+"</span></div>");
						$("#showModal").fadeIn();
					}, 1000); 
				},
				eventMouseLeave : function(info) {
					setTimeout(function() { 
						clearTimeout(timer);
						$("#showModal").fadeOut();
					},7000);
				},
				
				dateClick: function(info) {
					var date1 = info.dateStr;
					date1 = date1.split('+');
					$('#startDate').val(date1[0]);
					$('#myModal').show();
				},
				*/
			});
			calendar.render();
			
			$('#location_selector').on('change', function() {	
				calendar.render();
				//~ $('#calendar').fullCalendar('removeEvents');
				//~ $('#calendar').fullCalendar( 'refetchEvents' );
			});
		});
		
		$(document).ready(function(){
			$('#close1').click(function(){
				$('#view-schedule-content').html('');
				$('#editModal').hide();
			});		
			
			$('#deleteevent').click(function(){
				var str = 'id='+$(this).attr('idattr')+'&formname=leavedelete';
				$.ajax({
					type: "POST",
					url: "ajax_newform.php",
					data: str,
					cache: false,
					success: function(result){
						if(result == 1){
							setTimeout(function() {
								location.reload();
							}, 1000);
						}
					}
				});				
			});		
		
		});

		jQuery(document).ready(function(){
			/* ********* Add form ********* */
			jQuery("#leaveaddform").validate({
				ignore: [],
				rules: {                 
					status:{
						required: true,
					},                                
					reason:{
						required: true,
					},                                
				},                
				messages: {                    
					status:{
						required: "Please select a status.",
					},                                
					reason:{
						required: "Please enter a reason.",
					},                                
				},
				submitHandler: function(form){
					var str = $("#leaveaddform").serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								setTimeout(function() {
									location.reload();
								}, 2000);
							}
						}
					}); 
				}
			});
			$("#status").on('change', function() {
				var statusval = $(this).val();
				if ( statusval == 0 ) {
					$('.numbers').hide();
					$('#numdays').show();
				} else if ( statusval == 1 ) {
					$('.numbers').hide();
					$('#numhours').show();
				}
			});
			/* ********* Add form  ********* */
			/* ********* Edit form ********* */
			jQuery("#leaveeditform").validate({
				ignore: [],
				rules: {                 
					editeventstatus:{
						required: true,
					},                                
					reason:{
						required: true,
					},                                
				},                
				messages: {                    
					editeventstatus:{
						required: "Please select a status.",
					},                                
					reason:{
						required: "Please enter a reason.",
					},                                
				},
				submitHandler: function(form){
					var str = $("#leaveeditform").serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								setTimeout(function() {
									location.reload();
								}, 2000);
							}
						}
					}); 
				}
			});	   
			$("#editeventstatus").on('change', function() {
				var statusval = $(this).val();
				if ( statusval == 0 ) {
					$('.editnumbers').hide();
					$('#editnumdays').show();
				} else if ( statusval == 1 ) {
					$('.editnumbers').hide();
					$('#editnumhours').show();
				}
			});
			/* ********* Edit form ********* */
			
		});
	</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Appointment Calender</h1>
				</div>	
				<div class="card shadow mb-4 table-main-con marketing-report">
					<div class="manageschedule">
						<div class="sliderBlockOuter">
							<div class="calander-container">
								<div class="fc-event-inner"></div>
								<?php
								if( $_SESSION['Admin'] == 'TRUE') {
							?>
									<select id="location_selector">
									<option value="all">All locations</option>
								<?php
									foreach($employeedata as $employee){
								?>
										<option value="<?php echo $employee['ManageLocationId'] ?>"><?php echo $employee['LocationName']; ?></option>	
								<?php
									}
								?>
									</select>
								<?php
								} else if( $_SESSION['Admin'] == 'FALSE') { 
								?>
									<select id="location_selector">
										<option value="<?php echo $employeedata[0]['ManageLocationId'] ?>"><?php echo $employeedata[0]['LocationName']; ?></option>	
									</select>
								<?php
								}	
								?>
								<div id='calendar'></div>
							</div>
			
			
						</div>
					</div>

				</div>




<div  id="editModal" class="popupblockOuter modal editleavemodel modal-custom">
		<div class="popoutertb">
			<div class="popoutercell">
				<div class="popoutercontent popoutereditcal">
					<div class="popcontentmain">
						<h2>
							<p>Appointment Detail</p>
							<img id="close1" src="<?php echo $domain; ?>/img/cross.png" class="crossiconblk">
						</h2>
						<div class="sessionOuterBlock">
					
					<div>
						<div id='view-schedule-content' class='view-schedule-content'></div>
					</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

			
			<div id="showModal" class="modal editleavemodel">TEst tes </div>				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
