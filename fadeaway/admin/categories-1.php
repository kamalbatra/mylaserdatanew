<?php
	include('admin_includes/header.php');
	include('../includes/dbFunctions.php');
	$catDetails	= new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"]) ) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	/*** fetch all Employee location**/
	$tbl_category	= "tbl_categories";
	$condition = "WHERE BusinessID = $_SESSION[BusinessID] ORDER BY id DESC";
	$cols = "*";
	$catData = $catDetails->selectTableRowsNew($tbl_category,$condition);
	/*** fetch All device Name**/
	?>
		<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function() {
			
			$('a.activeUser').click(function() {
				var statusMsg = $(this).attr('id');
				if ( confirm("Are you sure you want to "+statusMsg+" this category?") ) {
					$('.loadingOuter').show();
					var id = $(this).parent().parent().attr('id');
					var status = $(this).parent().parent().attr('status');
					var data = 'id='+id+'&status='+status;
					var parent = $(this).parent().parent();			
					$.ajax({
						type: "POST",
						url: "ajax_newchangestatus.php",
						data: data,
						cache: false,				
						success: function(data) {
							if(data == 1) {
								data = 'Status updated succesfully.';
								setTimeout(function(){ location.reload() }, 1000);							
							}
						}
					});		
				}
			});
			
		});
	</script>
	<style>
		.srtHeadEditEmp { width:10%; }
		.tablecategories .span4.srtHead.srtHeadBorder { width:23%; }
		.tablecategories .span4.srtHead.srtcontent { width:23%;padding: 10px 0px 2px 12px; }
		.addNewReport { float: right; }
	</style>
	<div class="form-container">
		<div class="loadingOuter"><img src="../images/loader.svg"></div>		
		<div class="heading-container">
			<h1 class="heading empHeadReport">Categories</h1>
			<div class="addNewReport addReportemp"><a class="empLinks" href="add-category">Add New</a></div>
		</div>
		<div class="user-entry-block">
			<div class="tablebushead">	
				<div class="tablebusinner tablecategories tableservices-outer">	
					<div class="row sortingHead">	
						<div class="span4 srtHead srtHeadBorder">Category Name</div>
						<div class="span4 srtHead srtHeadBorder">Status</div>
						<div class="span4 srtHead srtHeadBorder">Date Added</div>
						<div class="span4 srtHeadEditEmp srtHeadBorder" style="text-align:center;">Action</div>
						<div class="span4 srtHeadEditEmp srtHeadBorder" style="border:none;text-align:center;">Action</div>
					</div>        
					<?php 
					if( !empty($catData) ) {
						$i = 0;
						foreach( $catData as $dataemp ){
							if(	$i%2 == 0 ){ 
								$bgdata = "bgnone";	
							} else {
								$bgdata = "bgdata";
							}	
							if(($_SESSION['id'] == $dataemp['Emp_ID']) && (strtoupper($_SESSION['Medicaldirector'])!="YES")){
								$currentLogin= "currentLogin";
							} else { 
								$currentLogin= "";
							}
							?>	
							<div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>" id="<?php echo $dataemp['id']?>" status="<?php echo $dataemp['status'];?>" >
								<div class="span4 srtHead srtcontent">
									<label id="" class="user-name"><?php echo $dataemp['categoryName']?> </label>						
								</div>					
								<div class="span4 srtHead srtcontent">	
									<label id="" class="user-name">
									<?php 
										if( $dataemp['status'] == 0 ){
											$status = 'De-Active';
										} else if ( $dataemp['status'] == 1 ) {
											$status = 'Active';									
										}
										echo $status;
									?>
									</label>
								</div>					
								<div class="span4 srtHead srtcontent">	
									<label id="" class="user-name" style="text-transform:none;">
										<?php echo date("F j, Y", strtotime($dataemp['dateAdded'])); ?>
									</label>
								</div>
								<?php 
								if( ( isset($_SESSION['loginuser']) && $_SESSION['loginuser']=="sitesuperadmin" ) || $_SESSION['Admin']=="TRUE" || ( strtoupper($_SESSION['Medicaldirector'])=="YES" ) || $_SESSION['id']==$dataemp['Emp_ID'] ) { ?>						
									<div class="span4 srtHeadEditEmp srtcontent text-align">
										<label id="" class="user-name"><a href="edit-category?id=<?php echo $dataemp['id']?>&action=catdetails"><img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit Category"/></a></label>							
									</div>
									<?php 
									if( $dataemp['status'] == 1 ) { ?>
										<div class="span4 srtHeadEditEmp srtcontent text-align" style="cursor:pointer;">
											<a class="activeUser" id="deactive" ><img src="<?php echo $domain; ?>/images/activeUser.png" title="Deactive" width="20px" height="20"/></a>
										</div>
									<?php 		
									}  else { ?>
										<div class="span4 srtHeadEditEmp srtcontent text-align" style="cursor:pointer;">
											<a class="activeUser" id="active" ><img src="<?php echo $domain; ?>/images/no.png" title="Active" width="20px" height="20"/> </a> 
										</div>
									<?php 
									} 
								} ?>
							</div><!--End @row-->				  
						<?php 
						$i++;  
						} 
					} else {
					?>	
						<div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>">
							<div class="srtcontent">
								No record found.
							</div>
						</div>
					<?php
					} 
					?>			  
					<div id="statuResult"></div>      
				</div><!--End @user-entry-block-->
			</div><!-- End  @form-container--->
		</div><!--End @container-->
	</div><!--End @container-->
	</div><!--End @container-->
	<?php include('admin_includes/footer.php');	?>
