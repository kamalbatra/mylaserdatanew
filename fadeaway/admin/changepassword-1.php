<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	//print_r($_SESSION);
	//$empId =$_SESSION['id'];
	/*
	if(!isset($_GET['Empid']) && $_GET['Empid']== NULL){
		$empId = $_GET['Empid'];
		$table = "tbl_employees";
		$condition = " where Emp_ID=".$empId." ";
		$cols="*";
		$editinfo = $empInfo->selectTableSingleRow($table,$condition,$cols);
		//print_r($editinfo);
	*/
	$table = "tbl_employees";
	$condition = " where Emp_ID=".base64_decode($_GET['empid'])." ";
	$cols = "*";
	$editinfo = $empInfo->selectTableSingleRow($table,$condition,$cols);
?>	
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#changePwdEmp").validate({
			rules: {                 
                Password:"required",
                Emp_email:"required",                                
				Emp_email: {
					required: true,
					email: true,
				},					
			},                
			messages: {                    
				Emp_email: {
					required: "Please enter Email." ,
					email: "Please enter valid email address.",
				},
				Password: {
					required: "Please enter Password.",
					minlength: "Please enter at least 5 digit Pssword.",						
				},					
			}
		});	   
		$('#changepwdBtn').click(function() {
			if( $("#changePwdEmp").valid()){
				//$("#pwdResult").show();	
				changepwd_form();			
			} else {			
				$("#pwdResult").hide();		
			}        
		});	   
	});
	function changepwd_form(){
		var str = $("#changePwdEmp" ).serialize();
		$.ajax({
			type: "POST",
			url: "ajax_changepwd.php",
			data: str,
			cache: false,
			success: function(result){
				if(result ==1){
					$("#emailNotExist").html("This email not exist!").show();
					$("#pwdResult").hide();
				} else {
					$("#pwdResult").show();
					$("#emailNotExist").hide();
					$("#pwdResult").html(result);
					//setTimeout(function() {location.reload()	}, 2000);
				}
			}
		});	
	}
</script>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Change password</h1>
		<div class="addNew">
			<a class="empLinks" href="empdetails" class="submit-btn">Employee list </a>
		</div>
	</div>	
	<div class="user-entry-block">
	<!--  dummy form to submit data  -->
		<form action="" name="changePwdEmp" id="changePwdEmp" method="post">
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">Email:</label>
					<input class="text-input-field" type="email" name="Emp_email" id="Emp_email" value="<?php echo $editinfo['Emp_email']?>" readonly="readonly"/>
					<span id="emailNotExist" class="error"></span>
				</div>			
			</div><!--End @row-->	
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">Password:</label>
					<div class='pwdwidgetdiv--' id='thepwddiv'></div>
					<script  type="text/javascript" >
						var pwdwidget = new PasswordWidget('thepwddiv','Password');
						pwdwidget.MakePWDWidget();
				    </script>
				    <noscript>
						<div><input class="text-input-field" type="password" name="Password" id="Password"/></div>
					</noscript>
				</div>
			</div><!--End @row-->
			<!--div class='para'>
				<label for='regpwd'>Password:</label> <br />
				<div class='pwdwidgetdiv' id='thepwddiv'></div>
				<script  type="text/javascript" >
				var pwdwidget = new PasswordWidget('thepwddiv','regpwd');
				pwdwidget.MakePWDWidget();
				</script>
				<noscript>
				<div><input type='password' id='regpwd' name='regpwd' /></div>		
				</noscript>
			</div-->
			<div class="row">
				<div class="span12">				
     				<input type="button"  id="changepwdBtn" value="submit" class="submit-btn"><br/>
					<div id="pwdResult" style="display:none;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
				</div>
			</div><!--End @row-->
		</form>
	</div>
</div>
</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
