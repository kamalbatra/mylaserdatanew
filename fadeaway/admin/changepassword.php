<?php
	//~ error_reporting(0);
	include('admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
		
	$empInfo = new dbFunctions();
	$table = "tbl_employees";
	$condition = " where Emp_ID=".base64_decode($_GET['empid'])." ";
	$cols = "*";
	$editinfo = $empInfo->selectTableSingleRow($table,$condition,$cols);
	?>	
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#changePwdEmp").validate({
				errorClass: 'errorblocks',
				errorElement: 'div',
				rules: {                 
					Password:"required",
					Emp_email:"required",                                
					Emp_email: {
						required: true,
						email: true,
					},					
				},                
				messages: {                    
					Emp_email: {
						required: "Please enter Email." ,
						email: "Please enter valid email address.",
					},
					Password: {
						required: "Please enter Password.",
						minlength: "Please enter at least 5 digit Pssword.",						
					},					
				}
			});	   
			$('#changepwdBtn').click(function() {
				if( $("#changePwdEmp").valid()){
					//$("#pwdResult").show();	
					changepwd_form();			
				} else {			
					$("#pwdResult").hide();		
				}        
			});	   
		});
		function changepwd_form(){
			var str = $("#changePwdEmp" ).serialize();
			$.ajax({
				type: "POST",
				url: "ajax_changepwd.php",
				data: str,
				cache: false,
				success: function(result){
					if(result ==1){
						$("#emailNotExist").html("This email not exist!").show();
						$("#pwdResult").hide();
					} else {
						$("#pwdResult").show();
						$("#emailNotExist").hide();
						$("#pwdResult").html(result);
						//setTimeout(function() {location.reload()	}, 2000);
					}
				}
			});	
		}
	</script>	
	
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Change Password</h1>
				</div>	
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="empdetails" class="submit-btn"><button class="addnewbtn">Employee list </button></a>
						</div>
					</div>
					<form action="" name="changePwdEmp" id="changePwdEmp" method="post">
					
						<div class="formcontentblock-ld">

							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label>Email:</label>
											<input class="text-input-field" type="email" name="Emp_email" id="Emp_email" value="<?php echo $editinfo['Emp_email']?>" readonly="readonly"/>
											<span id="emailNotExist" class="error"></span>
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Password:</label>
											<div class='pwdwidgetdiv--' id='thepwddiv'></div>
											<script  type="text/javascript" >
												var pwdwidget = new PasswordWidget('thepwddiv','Password');
												pwdwidget.MakePWDWidget();
											</script>
											<noscript>
												<div><input class="text-input-field" type="password" name="Password" id="Password"/></div>
											</noscript>
											
										</div>
									</div>
								</div>
							</div>


							<div class="form-row-ld">
								<div class="backNextbtn">
									<button type="button" id="changepwdBtn" class="nextbtn submit-btn">SUBMIT</button>
									
								</div>
								<div id="pwdResult" class="u_mess" style="display:none;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
							</div>

						</div>

					</form>
                    

				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
