<?php
//session_start();
include("admin_includes/header.php");
include("../includes/dbFunctions.php");
error_reporting(0);
$client	= new dbFunctions();
if(isset($_GET['clientid']) && $_GET['clientid']!=""){
	$clientid = $_GET['clientid'];
} else {
	$url ="consentforms";
	echo "<script>window.location.href='$url'</script>";
}
$table = "tbl_consent_form";
$tableSkin= "tbl_FitzPatrick_skin_types";
$condition = " where ClientID =".$clientid." ";
$clientdata = $client->selectTableSingleRow($table,$condition);
$cols="*";
$sizedata = $client->selectTableData($table,$condition,$cols);
$clientcols = "AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, AES_DECRYPT(DOB, '".SALT."') AS DOB";
$clientdob = $client->selectTableSingleRow("tbl_clients", $condition, $clientcols);
$sizedata->dob = $clientdob['DOB'];
$sizedata->ClientSignature = ucfirst($clientdob['FirstName']).' '.ucfirst($clientdob['LastName']);
$sizedata->ClientSignaturePrompt = '/'.ucfirst($clientdob['FirstName']).' '.ucfirst($clientdob['LastName']).'/'
?>
<script type="text/javascript">	
	$(document).ready(function() {
		$('#techReviewBtn').click(function(){
			var txtVal= $("#TechNotes").val();
			var checkBoxVal= $("#TechnicianReview:checked").val();
			if(txtVal !="" && checkBoxVal =="Y"){
				$("#errMsgTech").hide();
				$("#errMsgTechCheck").hide();
				var str = $("#edit_ConsetDetails" ).serialize();
				$.ajax({
					type: "POST",
					url: "edit_consetAdd.php",
					data: str,
					cache: false,
					success: function(result){
						$("#conset_massage").html(result);
						setTimeout(function() {
							$(location).attr('href', 'consentforms.php'); 
							return false;
						}, 3000);
					}//sucess
				});
			}
			else{
				if(txtVal ==""){
					$("#errMsgTech").html("This is required field.").show();
					$("#TechnicianReview").focus();
				}
				else{
					$("#errMsgTech").hide();
				}
				if(checkBoxVal !="Y"){
					$("#errMsgTechCheck").html("This is required field.").show();
				}
				else{
					$("#errMsgTechCheck").hide();
				}
				/*$("#errMsgTech").show();
				$("#errMsgTech").html("This is required field.");
				$("#TechnicianReview").focus();
				$("#errMsgTechCheck").show();
				$("#errMsgTechCheck").html("This is required field.");*/
			}
		});
		// Add additional note by docor
		$('#techDocReviewBtn').click(function(){
			var txtVal= $("#MedicalDirectorNotes").val();
			if(txtVal !=""){
				$("#errMsg").hide();					
				var str = $("#edit_DocDetails" ).serialize();
				$.ajax({
					type: "POST",
					url: "edit_DocDetails.php",
					data: str,
					cache: false,
					success: function(result){
						$("#conset_Docmassage").html(result);					
						setTimeout(function() {
							$(location).attr('href', 'consentforms.php');    				                   
							return false;
						}, 3000);
					}
				});						
			}
			else{
				$("#errMsg").show();
				$("#errMsg").html("This is required field.");
			}		
	   });	
  });	
</script>
<script>
function printContent(div_id) {
	var DocumentContainer = document.getElementById(div_id);
	var html = '<html><head>'+
               '<link href="<?php echo $domain; ?>/css/theme4.css" rel="stylesheet" type="text/css" /><link href="<?php echo $domain; ?>/css/style.css" rel="stylesheet" type="text/css" />'+
               '</head><body style="background:#ffffff;"><h1 class="heading empHeadReport" stye="width:100%;">Consent form review</h1>	<br>	<br><hr>'+
               DocumentContainer.innerHTML+
               '</body></html>';
    var WindowObject = window.open("", "PrintWindow",
    "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
    WindowObject.document.writeln(html);
    WindowObject.document.close();
    WindowObject.focus();
    WindowObject.print();
    WindowObject.close();
    //document.getElementById('print_link').style.display='block';
}
</script>
<div class="form-container">
	<div class="heading-container">		
		<h1 class="heading empHeadReport">Consent form review</h1>	 
		<div class="addNewReport" style="width:20%;float:right;"><a class="empLinks" href="consentforms" class="submit-btn">Consent forms list</a></div>
		<?php 
		if($clientdata['TechnicianReview'] !="" && $clientdata['MedicalDirectorNotes'] !=""){ 
		?>
		<!--div class="addNewReport" style="width:16%;">  <a href='javascript:printContent("ad_div")' id='print_link' style="background:#11719F;color:#ffffff;padding:6px 19px 5px 20px;"> </a> </div-->
		<?php 
		} 
		?>
	</div>
	<div class="user-entry-block" id="ad_div">	 
	<?php 
		if(!empty($sizedata)){
			$i = 0;
			foreach($sizedata as $key=>$data)
			{
				if($data !=""){	
					if($key=="TattooAge"){
						if( $data == "-1" ){
							$data = "<1 year";
						} else if( $data == 1 ){
							$data = $data.' Year';
						}else if ( $data == "20+" ){
							$data = ">20 years";
						} else {
							$data = $data.' Years';
						}
					}
					if($key=="TattoColor"){
						$tablecolor = "tbl_tatto_colors";
						$colorcols = "name";
						$colorcondition = " where id IN (".$data.") LIMIT 0,10";
						$tattocolors = $client->selectclientrecord($tablecolor,$colorcols,$colorcondition);
						$colorarray = array();
						foreach($tattocolors as $tattocolor ){
							array_push($colorarray, ucfirst($tattocolor['name']));
						}
						$data = implode(', ',$colorarray);						
					}
					if($key=="BodyPart"){
						$tablebody = "tbl_tattoremove_part";
						$bodycols = "part_name";
						$bodycondition = " where id IN (".$data.") LIMIT 0,10";
						$bodyparts = $client->selectclientrecord($tablebody,$bodycols,$bodycondition);
						$bodypararray = array();
						foreach($bodyparts as $bodypart ){
							array_push($bodypararray, ucfirst($bodypart['part_name']));
						}
						$data = implode(', ',$bodypararray);						
					}
				  	  					  
					if($key=="TechnicianReview" || $key=="EmployeeID" || $key=="TechnicianFirstName" || $key=="TechnicianLastName" || $key=="Location" ||  $key=="Duluth"  ||  $key=="Minneapolis"  ||  $key=="St_Paul" ){$style="display:none;";}else{$style="";}	 
					if($data=="Yes" || $data=="yes"){$style1="color:red;";}else{$style1="";} 	  
					if($i%2==0){
						$bgdata = "bgdata";
					}
					else{	
						$bgdata = "bgnone";
					}
					?>
					<div class="row treatment <?php echo $bgdata;?>" style="<?php echo $style; ?>">
						<div class="span6" style="width:26%;"><label id="Label1" class="user-name">	
						<?php //echo $key; ?>
						<?php $with_space = preg_replace('/[A-Z]/'," $0",$key); 
                 $str= ucwords($with_space);
                 $str = explode(" ",$str);                 
                // print_r($str);
                 $strUnde = $str[1];
                $journalName = str_replace("_", " ",$strUnde);
                if(strlen($str[3]) <=1){
					$code = $str[3];					
				}		 				
				else{
					$code .= "&nbsp;".$str[3];
				}
				if($key=="HIV"){
					echo $key;
				}
				if($key=="SmokeCigarette"){
					echo "Do you smoke cigarettes?";
				} else if($key=="TattooAge"){
					echo "Approximately how many years have you had the tattoo you wish to remove?";
				} else if($key=="TattoColor"){
					echo "Which colors are present in your tattoo?";
				} else if($key=="BodyPart"){
					echo "On which body part is the tattoo you wish to remove?";
				} else if($key=="dob"){
					echo "Date of Birth";
				}else{
					echo ($key=="HIV") ? '' : ($str = $journalName."&nbsp;".strtolower($str[2]).strtolower($code));   
				}                      
                ?>				
				</label></div>
				<div class="span6" style="width:1%;"><label id="" class="user-name">:</label></div>
				<div class="span6 last "><label id="Label1" class="user-name" style="<?php echo $style1; ?>"><?php 
				$data = str_replace("NO", "No",$data);
				echo $data;?></label></div>							
			  </div><!--End @row-->		
					<?php if($key=="Location"){					 
					 ?>
					<div class="row treatment bgnone">
						<div class="span6" style="width:26%;"><label id="Label1" class="user-name"><?php echo $key; ?> </label></div>
					<div class="span6" style="width:1%;"><label id="Label1" class="user-name">:</label></div>
					<div class="span6 last "><label id="Label1" class="user-name" style="<?php echo $style1; ?>">
					<?php //echo $locationData['LocationName'];
					    $clentLocation = explode(",",$data);
					         $k= count($clentLocation);
					         for($j=0 ; $j<count($clentLocation);$j++){
							$tbl_manage_location	= "tbl_manage_location";
							$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
							$cols="*";
							$locationData = $client->selectTableSingleRow($tbl_manage_location,$condition1);
							  echo $locationData['LocationName'];
							   if($k!=1){
								  echo ",";
							  }
							  echo "&nbsp;";
							$k--;
						  }//for loop close
					?>
					</label></div>							
					</div><!-- roew end-->
					<?php } ?>
		     <?php
	        $i++;	
		   }
	     }// foreach end 
        }  //if end
	?>
			<div class="row">
				<div class="span12 user-name" style="margin-top:30px;text-transform:none">
					<strong><i>AGREEMENT:</i></strong> By signing this Electronic Signature Acknowledgment Form, I agree that my electronic signature is the legally binding equivalent to my handwritten signature. Whenever I execute an electronic signature, it has the same validity and meaning as my handwritten signature. I will not, at any time in the future, repudiate the meaning of my electronic signature or claim that my electronic signature is not legally binding.				
				</div>
			</div><!--End @row-->
		 <!-- check client  revirew condition-->
							<!--  conset form additinal Technician review-->
			<?php if($clientdata['TechnicianReview'] =="" && $_SESSION['Technician']=="TRUE"){?>	
			<form action="" method="POST" name="" id="edit_ConsetDetails">
				<div class="user-entry-block" style="margin-top:30px;">
					<div class="row">
					   <input type="hidden" name="ConsentID" value="<?php echo $clientdata['ConsentID'];?>" />
					   <div class="span6"> 
					   <label id="Label1" class="user-name">Fitzpatrick Skin Type: </label>
							<select name="SkinType" id="SkinType" class="text-input-field select-option">
								<?php
								$clientdata1 = $client->selectTableRows($tableSkin);
								foreach($clientdata1 as $skin){
								?>
							    <option> <?=$skin['SkinType']; ?> </option>
							<?php }	?>
							</select>
						</div>	
						   <div class="span6 last">
							 <label id="Label1" class="user-name">	Additional information notes for this client.</label>
							 <textarea name="TechNotes" id="TechNotes" class="text-area-field" rows="4" cols="20"></textarea>
							  <span id="errMsgTech" class="error"></span>
						   </div>	  
						</div>  
					<div class="row">	
					   <div class="span6"> 
								   <label id="Label1" class="user-name">Indicate the price quoted.</label> 
								   <input type="text" class="text-input-field search_for_client" name="PriceQuote" id="PriceQuote"/>
						</div>  
						 <div class="span6 last" style="padding-top: 31px;">
							  <input type="checkbox" name="TechnicianReview" id="TechnicianReview" class="largerCheckbox" value="Y">
							  <span id="errMsgTechCheck"  class="error"></span>
							  <label id="Label1" class="user-name">Check to complete the review process.</label>
							  <input type="hidden" name="TimeStampTechnician" id="TimeStampTechnician " value="<?=date('Y-m-d H:i:s'); ?>"/>
							  <input type="hidden" name="TechnicianSignature" id="TechnicianSignature" value="/<?php echo $_SESSION['First_Name'].' '.$_SESSION['Last_Name']; ?>/"/>
						  </div>	  
					</div>
					<div class="row">
						<div class="span6"> 
						 <input type="button" class="submit-btn" value="Submit" style="cursor:pointer;" id="techReviewBtn" onclick=""/>
						 <div class="u_mess" id="conset_massage"></div>
					   </div>
					</div>
			</div>
			</form>
			<?php } ?>
			<!--  @End conset form additinal Technician review-->
			<!---- Consent Additional review For Doctor(Admin)-->			
				<?php //if(($clientdata['TechnicianReview'] =="" && $_SESSION['Admin']=="TRUE" && $_SESSION['Technician']=="TRUE") ||   ($clientdata['TechnicianReview'] !="" && $_SESSION['Admin']=="TRUE") || ($clientdata['MedicalDirectorNotes'] =="" && $_SESSION['Admin']=="TRUE")){?>
				 <?php if($clientdata['MedicalDirectorNotes'] =="" && $_SESSION['Medicaldirector']=="Yes"){					
					   $check = 1;
					   }
					   else{
						     $check = 0;
					   }
					?>	
			<?php if($check ==1){?>		
			<div class="docAddContainer" style="border-top: 1px dotted #ccc;float:left;width:100%;margin-top: 50px;">	
			<form action="" method="POST" name="" id="edit_DocDetails">
				<div class="user-entry-block" style="margin-top:30px;">
					 <input type="hidden" name="ConsentID" value="<?php echo $clientdata['ConsentID'];?>" />
					<div class="row">
						   <div class="span6 last">
							 <label id="Label1" class="user-name">	Additional information notes for this client(By director).</label>
							 <textarea name="MedicalDirectorNotes" id="MedicalDirectorNotes" class="text-area-field" rows="4" cols="20"></textarea>
							 <span id="errMsg" class="error"></span>
							 <input type="hidden" name="MedicalDirectorSignature" id="MedicalDirectorSignature" value="/<?php echo $_SESSION['First_Name'].' '. $_SESSION['Last_Name']?>,MD/"/>
							 <input type="hidden" name="TimeStampDoctor" id="TimeStampDoctor" value="<?=date('Y-m-d H:i:s'); ?>"/>
						   </div>	  
					</div> <!-- @End row Blck-->
					<div class="row">
						<div class="span6"> 
						 <input type="button" class="submit-btn" value="Submit" style="cursor:pointer;"  id="techDocReviewBtn" onclick=""/>
						 <div class="u_mess" id="conset_Docmassage"></div>
					   </div>
					</div><!-- @End row Blck-->
			</div>
			</form>
			</div><!-- @End docAddContainer -->
			<?php } ?>
			<!----End Consent Additional review For Doctor(Admin)-->
		</div><!--user-entry-block"-->	
</div><!--End @form-container-->
</div><!--End @container-->
<?php
	include('admin_includes/footer.php');
	?>
