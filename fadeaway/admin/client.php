<?php
session_start();
$doaminPath = $_SERVER['DOMAINPATH'];
$domain = $_SERVER['DOMAIN'];
include('admin_includes/header-new.php');
include("../includes/dbFunctions.php");
error_reporting(0);
$client	= new dbFunctions();
if(isset($_GET['clientid']) && $_GET['clientid']!=""){
	$clientid = $_GET['clientid'];
} else {
	$url ="consentforms";
	echo "<script>window.location.href='$url'</script>";
}
$table = "tbl_consent_form";
$tableSkin= "tbl_FitzPatrick_skin_types";
$condition = " where ClientID =".$clientid." ";
$clientdata = $client->selectTableSingleRow($table,$condition);
$cols="*";
$sizedata = $client->selectTableData($table,$condition,$cols);
$clientcols = "AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, AES_DECRYPT(DOB, '".SALT."') AS DOB";
$clientdob = $client->selectTableSingleRow("tbl_clients", $condition, $clientcols);
$sizedata->dob = $clientdob['DOB'];
$sizedata->ClientSignature = ucfirst($clientdob['FirstName']).' '.ucfirst($clientdob['LastName']);
$sizedata->ClientSignaturePrompt = '/'.ucfirst($clientdob['FirstName']).' '.ucfirst($clientdob['LastName']).'/'
?>
<script type="text/javascript">	
	$(document).ready(function() {
		$('#techReviewBtn').click(function(){
			var txtVal= $("#TechNotes").val();
			var checkBoxVal= $("#TechnicianReview:checked").val();
			if(txtVal !="" && checkBoxVal =="Y"){
				$("#errMsgTech").hide();
				$("#errMsgTechCheck").hide();
				var str = $("#edit_ConsetDetails" ).serialize();
				$.ajax({
					type: "POST",
					url: "edit_consetAdd.php",
					data: str,
					cache: false,
					success: function(result){
						$("#conset_massage").show().html(result);
						setTimeout(function() {
							$(location).attr('href', 'consentforms.php'); 
							return false;
						}, 3000);
					}//sucess
				});
			}
			else{
				if(txtVal ==""){
					$("#errMsgTech").html("This is required field.").show();
					$("#TechnicianReview").focus();
				}
				else{
					$("#errMsgTech").hide();
				}
				if(checkBoxVal !="Y"){
					$("#errMsgTechCheck").html("This is required field.").show();
				}
				else{
					$("#errMsgTechCheck").hide();
				}
				/*$("#errMsgTech").show();
				$("#errMsgTech").html("This is required field.");
				$("#TechnicianReview").focus();
				$("#errMsgTechCheck").show();
				$("#errMsgTechCheck").html("This is required field.");*/
			}
		});
		// Add additional note by docor
		$('#techDocReviewBtn').click(function(){
			var txtVal= $("#MedicalDirectorNotes").val();
			if(txtVal !=""){
				$("#errMsg").hide();					
				var str = $("#edit_DocDetails" ).serialize();
				$.ajax({
					type: "POST",
					url: "edit_DocDetails.php",
					data: str,
					cache: false,
					success: function(result){
						$("#conset_Docmassage").show().html(result);
						setTimeout(function() {
							$(location).attr('href', 'consentforms.php');    				                   
							return false;
						}, 3000);
					}
				});						
			}
			else{
				$("#errMsg").show();
				$("#errMsg").html("This is required field.");
			}		
	   });	
  });	
</script>
<script>
function printContent(div_id) {
	var DocumentContainer = document.getElementById(div_id);
	var html = '<html><head>'+
               '<link href="<?php echo $domain; ?>/css/theme4.css" rel="stylesheet" type="text/css" /><link href="<?php echo $domain; ?>/css/style.css" rel="stylesheet" type="text/css" />'+
               '</head><body style="background:#ffffff;"><h1 class="heading empHeadReport" stye="width:100%;">Consent form review</h1>	<br>	<br><hr>'+
               DocumentContainer.innerHTML+
               '</body></html>';
    var WindowObject = window.open("", "PrintWindow",
    "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
    WindowObject.document.writeln(html);
    WindowObject.document.close();
    WindowObject.focus();
    WindowObject.print();
    WindowObject.close();
    //document.getElementById('print_link').style.display='block';
}
</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Consent Form Review</h1>
				</div>
				
				<div class="bussiness-searchblock no-searchbox">
					<div class="search-btn">
						<a class="empLinks" href="consentforms" class="submit-btn"><button class="addnewbtn">Consent forms list </button></a>
					</div>
				</div>
				<?php if(!empty($sizedata)) {
					$sizedata = objectToArray($sizedata);
					list($array1, $array2) = array_chunk($sizedata, ceil((count($sizedata))/2), true);
				?>
					<div class="searchReportBlk">
						<div class="searchReportOuterblk">
							<div class="half">
								<div class="card shadow mb-4 searchReportcard">
									<?php
									$i = 0;
									foreach($array1 as $key=>$data) {
										if($data !=""){	
											if($key=="TattooAge"){
												if( $data == "-1" ){
													$data = "<1 year";
												} else if( $data == 1 ){
													$data = $data.' Year';
												}else if ( $data == "20+" ){
													$data = ">20 years";
												} else {
													$data = $data.' Years';
												}
											}
											if($key=="TattoColor"){
												$tablecolor = "tbl_tatto_colors";
												$colorcols = "name";
												$colorcondition = " where id IN (".$data.") LIMIT 0,10";
												$tattocolors = $client->selectclientrecord($tablecolor,$colorcols,$colorcondition);
												$colorarray = array();
												foreach($tattocolors as $tattocolor ){
													array_push($colorarray, ucfirst($tattocolor['name']));
												}
												$data = implode(', ',$colorarray);						
											}
											if($key=="BodyPart"){
												$tablebody = "tbl_tattoremove_part";
												$bodycols = "part_name";
												$bodycondition = " where id IN (".$data.") LIMIT 0,10";
												$bodyparts = $client->selectclientrecord($tablebody,$bodycols,$bodycondition);
												$bodypararray = array();
												foreach($bodyparts as $bodypart ){
													array_push($bodypararray, ucfirst($bodypart['part_name']));
												}
												$data = implode(', ',$bodypararray);						
											}
										  	  					  
											if($key=="TechnicianReview" || $key=="EmployeeID" || $key=="TechnicianFirstName" || $key=="TechnicianLastName" || $key=="Location" ||  $key=="Duluth"  ||  $key=="Minneapolis"  ||  $key=="St_Paul" ){$style="display:none;";}else{$style="";}	 
											if($data=="Yes" || $data=="yes"){$style1="color:red;";}else{$style1="";} 	  
											if($i%2==0) {
												$bgdata = "bgdata";
											} else {	
												$bgdata = "bgnone";
											}
											?>
											<div class="searchreportrow <?php echo $bgdata;?>" style="<?php echo $style; ?>">
												<div class="serachreportTital">
													<label id="Label1" class="user-name">	
													<?php
													$with_space = preg_replace('/[A-Z]/'," $0",$key); 
													$str= ucwords($with_space);
													$str = explode(" ",$str);                 
													// print_r($str);
													$strUnde = $str[1];
													$journalName = str_replace("_", " ",$strUnde);
													if(strlen($str[3]) <=1) {
														$code = $str[3];					
													} else{
														$code .= "&nbsp;".$str[3];
													}
													if($key=="HIV"){
														echo $key;
													}
													if($key=="SmokeCigarette"){
														echo "Do you smoke cigarettes?";
													} else if($key=="TattooAge"){
														echo "Approximately how many years have you had the tattoo you wish to remove?";
													} else if($key=="TattoColor"){
														echo "Which colors are present in your tattoo?";
													} else if($key=="BodyPart"){
														echo "On which body part is the tattoo you wish to remove?";
													} else if($key=="dob"){
														echo "Date of Birth";
													}else{
														echo ($key=="HIV") ? '' : ($str = $journalName."&nbsp;".strtolower($str[2]).strtolower($code));   
													}                      
													?>				
													</label>
													<b>:</b>
												</div>
												<div class="searchReportvalue">
													<label id="Label1" class="user-name" style="<?php echo $style1; ?>">
														<?php 
														$data = str_replace("NO", "No",$data);
														echo $data;
														?>
													</label>
												</div>
											</div>
											<?php if($key=="Location"){ ?>
												<div class="searchreportrow">
													<div class="serachreportTital">
														<label id="Label1" class="user-name"><?php echo $key; ?> </label>
														<b>:</b>
													</div>
													<div class="searchReportvalue">
														<label id="Label1" class="user-name" style="<?php echo $style1; ?>">
														<?php //echo $locationData['LocationName'];
														    $clentLocation = explode(",",$data);
															$k= count($clentLocation);
															for($j=0 ; $j<count($clentLocation);$j++) {
																$tbl_manage_location	= "tbl_manage_location";
																$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
																$cols="*";
																$locationData = $client->selectTableSingleRow($tbl_manage_location,$condition1);
																echo $locationData['LocationName'];
																if($k!=1) {
																	echo ",";
																}
																echo "&nbsp;";
																$k--;
															}//for loop close
														?>
														</label>
													</div>
												</div>
											<?php } ?>
										<?php $i++;	
						   				}
									} ?>
								</div>
							</div>
							<div class="half">
								<div class="card shadow mb-4 searchReportcard">
									<?php
									$i = 0;
									foreach($array2 as $key=>$data) {
										if($data !=""){	
											if($key=="TattooAge"){
												if( $data == "-1" ){
													$data = "<1 year";
												} else if( $data == 1 ){
													$data = $data.' Year';
												}else if ( $data == "20+" ){
													$data = ">20 years";
												} else {
													$data = $data.' Years';
												}
											}
											if($key=="TattoColor"){
												$tablecolor = "tbl_tatto_colors";
												$colorcols = "name";
												$colorcondition = " where id IN (".$data.") LIMIT 0,10";
												$tattocolors = $client->selectclientrecord($tablecolor,$colorcols,$colorcondition);
												$colorarray = array();
												foreach($tattocolors as $tattocolor ){
													array_push($colorarray, ucfirst($tattocolor['name']));
												}
												$data = implode(', ',$colorarray);						
											}
											if($key=="BodyPart"){
												$tablebody = "tbl_tattoremove_part";
												$bodycols = "part_name";
												$bodycondition = " where id IN (".$data.") LIMIT 0,10";
												$bodyparts = $client->selectclientrecord($tablebody,$bodycols,$bodycondition);
												$bodypararray = array();
												foreach($bodyparts as $bodypart ){
													array_push($bodypararray, ucfirst($bodypart['part_name']));
												}
												$data = implode(', ',$bodypararray);						
											}
										  	  					  
											if($key=="TechnicianReview" || $key=="EmployeeID" || $key=="TechnicianFirstName" || $key=="TechnicianLastName" || $key=="Location" ||  $key=="Duluth"  ||  $key=="Minneapolis"  ||  $key=="St_Paul" ){$style="display:none;";}else{$style="";}	 
											if($data=="Yes" || $data=="yes"){$style1="color:red;";}else{$style1="";} 	  
											if($i%2==0) {
												$bgdata = "bgdata";
											} else {	
												$bgdata = "bgnone";
											}
											?>
											<div class="searchreportrow <?php echo $bgdata;?>" style="<?php echo $style; ?>">
												<div class="serachreportTital">
													<label id="Label1" class="user-name">	
													<?php
													$with_space = preg_replace('/[A-Z]/'," $0",$key); 
													$str= ucwords($with_space);
													$str = explode(" ",$str);                 
													// print_r($str);
													$strUnde = $str[1];
													$journalName = str_replace("_", " ",$strUnde);
													if(strlen($str[3]) <=1) {
														$code = $str[3];					
													} else{
														$code .= "&nbsp;".$str[3];
													}
													if($key=="HIV"){
														echo $key;
													}
													if($key=="SmokeCigarette"){
														echo "Do you smoke cigarettes?";
													} else if($key=="TattooAge"){
														echo "Approximately how many years have you had the tattoo you wish to remove?";
													} else if($key=="TattoColor"){
														echo "Which colors are present in your tattoo?";
													} else if($key=="BodyPart"){
														echo "On which body part is the tattoo you wish to remove?";
													} else if($key=="dob"){
														echo "Date of Birth";
													}else{
														echo ($key=="HIV") ? '' : ($str = $journalName."&nbsp;".strtolower($str[2]).strtolower($code));   
													}                      
													?>				
													</label>
													<b>:</b>
												</div>
												<div class="searchReportvalue">
													<label id="Label1" class="user-name" style="<?php echo $style1; ?>">
														<?php 
														$data = str_replace("NO", "No",$data);
														echo $data;
														?>
													</label>
												</div>
											</div>
											<?php if($key=="Location"){ ?>
												<div class="searchreportrow">
													<div class="serachreportTital">
														<label id="Label1" class="user-name"><?php echo $key; ?> </label>
														<b>:</b>
													</div>
													<div class="searchReportvalue">
														<label id="Label1" class="user-name" style="<?php echo $style1; ?>">
														<?php //echo $locationData['LocationName'];
														    $clentLocation = explode(",",$data);
															$k= count($clentLocation);
															for($j=0 ; $j<count($clentLocation);$j++) {
																$tbl_manage_location	= "tbl_manage_location";
																$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
																$cols="*";
																$locationData = $client->selectTableSingleRow($tbl_manage_location,$condition1);
																echo $locationData['LocationName'];
																if($k!=1) {
																	echo ",";
																}
																echo "&nbsp;";
																$k--;
															}//for loop close
														?>
														</label>
													</div>
												</div>
											<?php } ?>
										<?php $i++;	
						   				}
									} ?>
								</div>
							</div>
						</div>
					</div>
		<?php   } ?>

				<div class="agreementblock">
					<strong><i>AGREEMENT:</i></strong> By signing this Electronic Signature Acknowledgment Form, I agree that my electronic signature is the legally binding equivalent to my handwritten signature. Whenever I execute an electronic signature, it has the same validity and meaning as my handwritten signature. I will not, at any time in the future, repudiate the meaning of my electronic signature or claim that my electronic signature is not legally binding.
				</div>

				<?php if($clientdata['TechnicianReview'] =="" && $_SESSION['Technician']=="TRUE"){ ?>
				<div class="formWhiteBlockMargin">
					<form action="" method="POST" name="" id="edit_ConsetDetails">
						<input type="hidden" name="ConsentID" value="<?php echo $clientdata['ConsentID'];?>" />
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Fitzpatrick Skin Type: </label>
													<select name="SkinType" id="SkinType" class="text-input-field select-option">
														<?php
														$clientdata1 = $client->selectTableRows($tableSkin);
														foreach($clientdata1 as $skin){
														?>
														    <option> <?=$skin['SkinType']; ?> </option>
														<?php }	?>
													</select>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Additional Information Notes For This Client.</label>
													<textarea name="TechNotes" id="TechNotes" class="text-area-field" rows="4" cols="20"></textarea>
													<div id="errMsgTech" class="errorblocks"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Indicate The Price Quoted.</label>
								   					<input type="text" class="text-input-field search_for_client" name="PriceQuote" id="PriceQuote"/>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<div class="radioblocksBtns">
														<div class="radioBtn ckeckboxres">		
															<input type="checkbox" name="TechnicianReview" id="TechnicianReview" class="largerCheckbox" value="Y">
															<div id="errMsgTechCheck"  class="errorblocks"></div>
															<label for="TechnicianReview" class="user-name">Check to complete the review process.</label>
															<input type="hidden" name="TimeStampTechnician" id="TimeStampTechnician " value="<?=date('Y-m-d H:i:s'); ?>"/>
															<input type="hidden" name="TechnicianSignature" id="TechnicianSignature" value="/<?php echo $_SESSION['First_Name'].' '.$_SESSION['Last_Name']; ?>/"/>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" onclick="" id="techReviewBtn" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
						 					<div class="u_mess" id="conset_massage"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				</div>	
				<?php } ?>

				<?php //if(($clientdata['TechnicianReview'] =="" && $_SESSION['Admin']=="TRUE" && $_SESSION['Technician']=="TRUE") ||   ($clientdata['TechnicianReview'] !="" && $_SESSION['Admin']=="TRUE") || ($clientdata['MedicalDirectorNotes'] =="" && $_SESSION['Admin']=="TRUE")){?>
				<?php
				 	if($clientdata['MedicalDirectorNotes'] == "" && $_SESSION['Medicaldirector'] == "Yes") {
					   	$check = 1;
					} else {
						$check = 0;
					}
				?>
				<?php if($check ==1) { ?>
				<div class="formWhiteBlockMargin">	
					<form action="" method="POST" name="" id="edit_DocDetails">
						<input type="hidden" name="ConsentID" value="<?php echo $clientdata['ConsentID'];?>" />
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div>
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Additional Information Notes For This Client(By Director).</label>
													<textarea name="MedicalDirectorNotes" id="MedicalDirectorNotes" class="text-area-field" rows="4" cols="20"></textarea>
													<div id="errMsg" class="errorblocks"></div>
													<input type="hidden" name="MedicalDirectorSignature" id="MedicalDirectorSignature" value="/<?php echo $_SESSION['First_Name'].' '. $_SESSION['Last_Name']?>,MD/"/>
													<input type="hidden" name="TimeStampDoctor" id="TimeStampDoctor" value="<?=date('Y-m-d H:i:s'); ?>"/>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" onclick="" id="techDocReviewBtn" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
						 					<div class="u_mess" id="conset_Docmassage"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				</div>	
				<?php } ?>


				
			</div>
			<!-- /.container-fluid -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php
	function objectToArray($d) {
	    if (is_object($d)) {
	        // Gets the properties of the given object
	        // with get_object_vars function
	        $d = get_object_vars($d);
	    }
		
	    if (is_array($d)) {
	        /*
	        * Return array converted to object
	        * Using __FUNCTION__ (Magic constant)
	        * for recursive call
	        */
	        return array_map(__FUNCTION__, $d);
	    }
	    else {
	        // Return array
	        return $d;
	    }
	}
	?>
	<?php	
	include('admin_includes/footer-new.php');	
	?>
