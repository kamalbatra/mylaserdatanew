<?php
	//session_start();
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	
	$fullinfoEdit = new dbFunctions();
	$table = "tbl_consent_form";
	$condition = " where ClientID =".base64_decode($_GET['ClientID'])." ";
	$cols="*";
	$clientdata = $fullinfoEdit->selectTableSingleRow($table,$condition,$cols);
	$clientcondition = " where ClientID =".base64_decode($_GET['ClientID'])." ";
	$clientcols="tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, AES_DECRYPT(tbl_clients.DOB, '".SALT."') AS DOB, AES_DECRYPT(tbl_clients.Age, '".SALT."') AS Age, tbl_clients.disclaimer_version as legal_version";
	$clientname = $fullinfoEdit->selectTableSingleRow("tbl_clients",$clientcondition,$clientcols);
	
	if($clientname['legal_version']!='')
	{
		$tableLegal="tbl_legal_disclaimer";
		$cols=" * ";
		$condition=" where id=".$clientname['legal_version']." order by id desc";
		$checkLegalExistance = $fullinfoEdit->selectTableData($tableLegal,$condition,$cols);
		$legalVersionId = $checkLegalExistance->id;
		$legalnotice = $checkLegalExistance->notice;
		$legalrisk = $checkLegalExistance->risk;
		$legalliability = $checkLegalExistance->liability_release;
		
	}
	$BusinessName=$_SESSION['businessdata']['BusinessName'];
	
	$table	= "tbl_tattoremove_part";
	$bodypartchoices = $fullinfoEdit->selectTableRows($table);

	$table	= "tbl_tatto_colors";
	$tattocolors = $fullinfoEdit->selectTableRows($table);
?>
<script src="<?php echo $domain; ?>/js/fullinfoValidateEdit.js"></script>
<script type="text/javascript">
	$(function () {
		$('#FirstName').keydown(function (e) {
			if (e.shiftKey || e.ctrlKey || e.altKey) {
				e.preventDefault();
			} else {
				var key = e.keyCode;
				if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
					e.preventDefault();
				}
			}
		});
	});
</script>
<script type="text/javascript">
	$(function () {
		$('#LastName').keydown(function (e) {
			if (e.shiftKey || e.ctrlKey || e.altKey) {
				e.preventDefault();
			} else {
				var key = e.keyCode;
				if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
					e.preventDefault();
				}
			}
		});
	});
</script>
<script>
	$(function () {	
		$('form :input').attr('disabled','disabled');
	});
</script>
<script>
	function printContent(div_id) {
		var DocumentContainer = document.getElementById(div_id);
		var html = '<html><head>'+
				   '<link href="<?php echo $domain; ?>/css/theme4.css" rel="stylesheet" type="text/css" /><link href="<?php echo $domain; ?>/css/style.css" rel="stylesheet" type="text/css" />'+
				   '</head><body style="background:#ffffff;"><h1 class="heading empHeadReport" stye="width:100%;">Consent form </h1><br><br><hr>'+
				   DocumentContainer.innerHTML+
				   '</body></html>';
		var WindowObject = window.open("", "PrintWindow",
			"width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
		WindowObject.document.writeln(html);
		WindowObject.document.close();
		WindowObject.focus();
		WindowObject.print();
		WindowObject.close();
		//document.getElementById('print_link').style.display='block';
	}
</script>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHeadReport">Medical History</h1>
		<?php
		if($_SESSION['Usertype']=="Admin") { ?>
			<div class="addNewReport"><a class="empLinks" href="clientdetails.php" class="submit-btn">Client list </a></div>
		<?php
		} ?> 
		<div class="addNewReport" style="width:16%;">  <a href='javascript:printContent("ad_div")' id='print_link' style="background:#11719F;color:#ffffff;padding:6px 19px 5px 20px;"> Print</a> </div>
	</div>
	<div class="medical-history-block" id="ad_div">
		<?php
		$fname = strtolower($clientdata['FirstName']);
		$lname = strtolower($clientdata['LastName']); ?>			
		<form action="thanks.php" name="form" id ="cforn" method="post" onsubmit="return validateFormEdit('<?php echo $fname;?>','<?php echo $lname;?>')">
			<input type="hidden" value="<?php echo base64_decode($_GET['ClientID']); ?>" name="ClientID"/>
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">First Name:</label>
					<input class="text-input-field" type="text" name="FirstName" id="FirstName" value="<?php echo ucfirst($clientname['FirstName']); ?>"/>
				</div>
				<div class="span6 last">
					<label id="Label1" class="user-name">Last Name:</label>
					<input class="text-input-field" type="text" name="LastName" id="LastName" value="<?php echo ucfirst($clientname['LastName']); ?>"/>
				</div>
				<div class="span6">
					<span id="gender" class="user-name">Gender:</span>
					<input type="radio" name="Gender" id="Gender" value="male" class="radio-fields regular-radio" <?php if($clientdata['Gender']=="male") {echo "checked"; } ?>/>
					<label for="Gender"></label>
					<span class="select-txt">Male</span>
					<input value="female" type="radio" name="Gender" id="Gender" class="radio-fields regular-radio"  <?php if($clientdata['Gender']=="female") {echo "checked"; } ?>/>
					<label for="Gender"></label>
					<span class="select-txt">Female</span> 
					<span id="GenderMsg" class="error ermsg"></span>
				</div>
				<div class="span6">
					<label id="Label1" class="user-name">Date of Birth:</label>
					<input class="text-input-field" type="text" name="FirstName" id="FirstName" value="<?php echo $clientname['DOB']; ?>"/>
				</div>
				<div class="span6 last">
					<label id="Label1" class="user-name">Age:</label>
					<input class="text-input-field" type="text" name="FirstName" id="FirstName" value="<?php echo $clientname['Age']; ?>"/>
				</div>
			</div><!--End @row-->
			<h2>Past Medical History</h2>
			<div class="row">
				<div class="span12">
					<label id="Label1" class="user-name">Please describe the tattoo(s) you wish to have treated:</label>
					<textarea rows="4" cols="30" name="TreatmentRequest" id="TreatmentRequest" class="text-area-field"><?php echo $clientdata['TreatmentRequest']; ?></textarea>
					<span id="treatmentRequestLenMsg" class="error"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span id="Label1" class="user-name">Are you currently under the care of a physician?</span>
					<input value="Yes" type="radio" name="PhysicianCare" id="PhysicianCare" class="radio-fields regular-radio" onClick="enablePhysicanCareReason();" <?php if($clientdata['PhysicianCare']=="Yes") {echo "checked"; } ?> /> 
					<label for="PhysicianCare"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="PhysicianCare" id="PhysicianCare1" class="radio-fields regular-radio" onClick="disablePhysicanCareReason();" <?php if($clientdata['PhysicianCare']=="No") {echo "checked"; } ?>/>
					<label for="PhysicianCare1"></label>
					<span class="select-txt">No</span>  
					<span id="PhysicianCareMsg" class="error"></span>
				</div>
				<div class="span6 last">
					<label id="Label1" class="user-name">If yes, then for what reason?</label>
					<input type="text" name="PhysicanCareReason" id="PhysicanCareReason" class="text-input-field" value="<?php echo $clientdata['PhysicanCareReason']; ?>" />
					<span id="PhysicanCareReasoneMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<h6>Please indicate any of the following medical conditions for which you have been treated in the past:</h6>
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Cancer:</span>
					<input type="radio" name="Cancer" id="Cancer" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['Cancer']=="Yes") {echo "checked"; } ?>/>
					<label for="Cancer"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Cancer" id="Cancer1" class="radio-fields regular-radio"  <?php if($clientdata['Cancer']=="No") {echo "checked"; } ?>/>
					<label for="Cancer1"></label>
					<span class="select-txt">No</span> 
					<span id="CancerMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">High Blood Pressure:</span>
					<input type="radio" name="HighBloodPressure" id="HighBloodPressure" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['HighBloodPressure']=="Yes") {echo "checked"; } ?> /><label for="HighBloodPressure"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HighBloodPressure" id="HighBloodPressure1" class="radio-fields regular-radio"  <?php if($clientdata['HighBloodPressure']=="No") {echo "checked"; } ?>/><label for="HighBloodPressure1"></label>
					<span class="select-txt">No</span>
					<span id="HighBloodPressureMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Arthritis:</span>
					<input type="radio" name="Arthritis" id="Arthritis" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['Arthritis']=="Yes") {echo "checked"; } ?> /><label for="Arthritis"></label> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Arthritis" id="Arthritis1" class="radio-fields regular-radio"  <?php if($clientdata['Arthritis']=="No") {echo "checked"; } ?> /><label for="Arthritis1"></label>
					<span class="select-txt">No</span>  
					<span id="ArthritisMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Cold Sores:</span>
					<input type="radio" name="ColdSores" id="ColdSores" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['ColdSores']=="Yes") {echo "checked"; } ?> /><label for="ColdSores"></label> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="ColdSores" id="ColdSores" class="radio-fields regular-radio" <?php if($clientdata['ColdSores']=="No") {echo "checked"; } ?>/><label for="ColdSores1"></label>
					<span class="select-txt">No</span>
					<span id="ColdSoresMsg" class="error ermsg"></span>
				</div>					
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Keloids: </span>
					<input type="radio" name="Keloids" id="Keloids" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['Keloids']=="Yes") {echo "checked"; } ?>/><label for="Keloids"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Keloids" id="Keloids1" class="radio-fields regular-radio" <?php if($clientdata['Keloids']=="No") {echo "checked"; } ?>/><label for="Keloids1"></label>
					<span class="select-txt">No</span>  
					<span id="KeloidsMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Disease of the Skin:</span>
					<input type="radio" name="SkinDisease" id="SkinDisease" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['SkinDisease']=="Yes") {echo "checked"; } ?>/><label for="SkinDisease1"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="SkinDisease" id="SkinDisease" class="radio-fields regular-radio" <?php if($clientdata['SkinDisease']=="No") {echo "checked"; } ?>/><label for="SkinDisease1"></label>
					<span class="select-txt">No</span>
					<span id="SkinDiseaseMsg" class="error ermsg"></span>
				</div>					
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Diabetes:</span>
					<input type="radio" name="Diabetes" id="Diabetes" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['Diabetes']=="Yes") {echo "checked"; } ?> /><label for="Diabetes"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Diabetes" id="Diabetes1" class="radio-fields regular-radio" <?php if($clientdata['Diabetes']=="No") {echo "checked"; } ?>/><label for="Diabetes1"></label>
					<span class="select-txt">No</span>  
					<span id="DiabetesMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">HIV / AIDS:</span>
					<input type="radio" name="HIV" id="HIV" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['HIV']=="Yes") {echo "checked"; } ?> /> <label for="HIV"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HIV" id="HIV1" class="radio-fields regular-radio" <?php if($clientdata['HIV']=="No") {echo "checked"; } ?>/><label for="HIV1"></label>
					<span class="select-txt">No</span>
					<span id="HIVMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Hormone Imbalance:</span>
					<input  type="radio" name="HormoneImbalance" id="HormoneImbalance" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['HormoneImbalance']=="Yes") {echo "checked"; } ?> />
					<label for="HormoneImbalance"></label>
					 <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HormoneImbalance" id="HormoneImbalance1" class="radio-fields regular-radio" <?php if($clientdata['HormoneImbalance']=="No") {echo "checked"; } ?> />
					<label for="HormoneImbalance1"></label>
					<span class="select-txt">No</span>  
					<span id="HormoneImbalanceMsg" class="error ermsg"></span>
				</div>
				<div class="span6 last">
					<span class="medical-condition-txt">Seizures:</span>
					<input  type="radio" name="Seizures" id="Seizures" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['Seizures']=="Yes") {echo "checked"; } ?> /> <label for="Seizures"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Seizures" id="Seizures1" class="radio-fields regular-radio"   <?php if($clientdata['Seizures']=="No") {echo "checked"; } ?>/><label for="Seizures1"></label>
					<span class="select-txt">No</span>				
					<span id="SeizuresMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Blood Clotting Abnormality:</span>
					<input type="radio" name="BloodClottingAbnormality" id="BloodClottingAbnormality" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['BloodClottingAbnormality']=="Yes") {echo "checked"; } ?> />
					<label for="BloodClottingAbnormality"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="BloodClottingAbnormality" id="BloodClottingAbnormality1" class="radio-fields regular-radio" <?php if($clientdata['BloodClottingAbnormality']=="No") {echo "checked"; } ?>/>
					<label for="BloodClottingAbnormality1"></label>
					<span class="select-txt">No</span>  
					<span id="BloodClottingAbnormalityMsg" class="error ermsg"></span>
				</div>
				<div class="span6 last">
					<span class="medical-condition-txt">Hepatitis:</span>
					<input type="radio" name="Hepatitis" id="Hepatitis" value="Yes" class="radio-fields regular-radio"  <?php if($clientdata['Hepatitis']=="Yes") {echo "checked"; } ?>/> <label for="Hepatitis"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Hepatitis" id="Hepatitis1" class="radio-fields regular-radio"   <?php if($clientdata['Hepatitis']=="No") {echo "checked"; } ?>/><label for="Hepatitis1"></label>
					<span class="select-txt">No</span>				
					<span id="HepatitisMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Thyroid Imbalance:</span>
					<input type="radio" name="ThyroidImbalance" id="ThyroidImbalance" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['ThyroidImbalance']=="Yes") {echo "checked"; }?> /><label for="ThyroidImbalance"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="ThyroidImbalance" id="ThyroidImbalance1" class="radio-fields regular-radio" <?php if($clientdata['ThyroidImbalance']=="No") {echo "checked"; }?>/><label for="ThyroidImbalance1"></label>
					<span class="select-txt">No</span>  
					<span id="ThyroidImbalanceMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Any Active Infection:</span>
					<input type="radio" name="ActiveInfection" id="ActiveInfection" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['ActiveInfection']=="Yes") {echo "checked"; }?> /> <label for="ActiveInfection"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="ActiveInfection" id="ActiveInfection1" class="radio-fields regular-radio" <?php if($clientdata['ActiveInfection']=="No") {echo "checked"; }?>/><label for="ActiveInfection1"></label>
					<span class="select-txt">No</span>
					<span id="ActiveInfectionMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<label id="Label1" class="user-name">Please list any other medical conditions you may suffer:</label>
					<input class="text-input-field" type="text" name="MedicalProblemsOther" id="MedicalProblemsOther" value="<?php echo $clientdata['MedicalProblemsOther'];?>"/>
					<span id="MedicalProblemsOtherMsg" class="error ermsg"></span>
				</div>				
			</div><!--End @row-->
			<div class="divider-dotted">&nbsp;</div>			
			<h2>Allergies</h2>
			<h6>Have you ever had an allergic reaction to any of the following?</h6>
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Food Allergy:</span>
					<input type="radio" name="FoodAllergy" id="FoodAllergy" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['FoodAllergy']=="Yes") {echo "checked"; }?>/><label for="FoodAllergy"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="FoodAllergy" id="FoodAllergy1" class="radio-fields regular-radio"  <?php if($clientdata['FoodAllergy']=="No") {echo "checked"; }?>/><label for="FoodAllergy1"></label>
					<span class="select-txt">No</span>  
					<span id="FoodAllergyMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Hydrocortisone Allergy:</span>
					<input type="radio" name="HydrocortisoneAllergy" id="HydrocortisoneAllergy" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['HydrocortisoneAllergy']=="Yes") {echo "checked"; }?> /><label for="HydrocortisoneAllergy"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HydrocortisoneAllergy" id="HydrocortisoneAllergy1" class="radio-fields regular-radio" <?php if($clientdata['HydrocortisoneAllergy']=="No") {echo "checked"; }?> /><label for="HydrocortisoneAllergy1"></label>
					<span class="select-txt">No</span>
					<span id="HydrocortisoneAllergyMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->						
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Hydroquinone Allergy: </span>
					<input type="radio" name="HydroquinoneAllergy" id="HydroquinoneAllergy" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['HydroquinoneAllergy']=="Yes") {echo "checked"; }?> /><label for="HydroquinoneAllergy"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HydroquinoneAllergy" id="HydroquinoneAllergy1" class="radio-fields regular-radio" <?php if($clientdata['HydroquinoneAllergy']=="No") {echo "checked"; }?> />
					<label for="HydroquinoneAllergy1"></label>
					<span class="select-txt">No</span>  
					<span id="HydroquinoneAllergyMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Aspirin Allergy:</span>
					<input type="radio" name="AspirinAllergy" id="AspirinAllergy" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['AspirinAllergy']=="Yes") {echo "checked"; }?>/><label for="AspirinAllergy"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="AspirinAllergy" id="AspirinAllergy1" class="radio-fields regular-radio" <?php if($clientdata['AspirinAllergy']=="No") {echo "checked"; }?>/><label for="AspirinAllergy1"></label>
					<span class="select-txt">No</span>				
					<span id="AspirinAllergyMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Latex Allergy:</span>
					<input type="radio" name="LatexAllergy" id="LatexAllergy" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['LatexAllergy']=="Yes") {echo "checked"; }?>/> <label for="LatexAllergy"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="LatexAllergy" id="LatexAllergy1" class="radio-fields regular-radio" <?php if($clientdata['LatexAllergy']=="No") {echo "checked"; }?>/><label for="LatexAllergy1"></label>
					<span class="select-txt">No</span>  
					<span id="LatexAllergyMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Lidocaine Allergy:</span>
					<input type="radio" name="LidocaineAllergy" id="LidocaineAllergy" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['LidocaineAllergy']=="Yes") {echo "checked"; }?> /><label for="LidocaineAllergy"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="LidocaineAllergy" id="LidocaineAllergy1" class="radio-fields regular-radio" <?php if($clientdata['LidocaineAllergy']=="No") {echo "checked"; }?>/><label for="LidocaineAllergy1"></label>
					<span class="select-txt">No</span>				
					<span id="LidocaineAllergyMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<label id="Label1" class="user-name">List any other allergies:</label>
					<textarea type="text" name="OtherAllergy" id="OtherAllergy" class="text-area-field"><?php echo $clientdata['LidocaineAllergy'];?></textarea>
					<span id="OtherAllergyMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="divider-dotted">&nbsp;</div>
			<h2>Medications</h2>
			<h6>What oral medications are you presently taking? (Please check all that apply)</h6>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Blood Thinners (Aspirin, Plavix, Coumadin (Warfarin), Ticlid, etc.):</span>
					<input type="radio" name="BloodThinners" id="BloodThinners" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['BloodThinners']=="Yes") {echo "checked"; }?> /><label for="BloodThinners"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="BloodThinners" id="BloodThinners" class="radio-fields regular-radio" <?php if($clientdata['BloodThinners']=="No") {echo "checked"; }?> /><label for="BloodThinners1"></label>
					<span class="select-txt">No</span>				
					<span id="BloodThinnersMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Immunosuppressive Drugs (Therapy for Autoimmune Diseases (Lupus, Rheumatoid Arthritis, etc.) Chemotherapy for Cancer, Steroids, etc.):</span>
					<input type="radio" name="Immunosuppressants" id="Immunosuppressants" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['Immunosuppressants']=="Yes") {echo "checked"; }?>/><label for="Immunosuppressants"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="Immunosuppressants" id="Immunosuppressants1" value="No" class="radio-fields regular-radio" <?php if($clientdata['Immunosuppressants']=="No") {echo "checked"; }?>/><label for="Immunosuppressants1"></label>
					<span class="select-txt">No</span>
					<span id="ImmunosuppressantsMsg" class="error ermsg mederr"></span>		
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Hormone Therapy (Estrogen Replacement Therapy (ERT), Birth Control Pills, etc.):</span>
					<input type="radio" name="HormoneTherapy" id="HormoneTherapy" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['HormoneTherapy']=="Yes") {echo "checked"; }?>/> <label for="HormoneTherapy"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HormoneTherapy" id="HormoneTherapy1" class="radio-fields regular-radio"  <?php if($clientdata['HormoneTherapy']=="No") {echo "checked"; }?>/><label for="HormoneTherapy1"></label>
					<span class="select-txt">No</span>				
					<span id="HormoneTherapyMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="divider-dotted">&nbsp;</div>
			<!----List your current medications-->
			<h2>List your current medications</h2>
			<div class="row">
				<div class="span3">
					<label id="Label1" class="user-name">Medication 1:</label>
				</div>
				<div class="span6 last" style="margin:0px;">
					<input class="text-input-field" type="text" name="CurrentMeds1" id="CurrentMeds1" value="<?php echo $clientdata['CurrentMeds1'];?>"/>
					<span id="CurrentMeds1Msg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->	
			<?php 
			for($j=2;$j<11; $j++ ) { 
				if($clientdata['CurrentMeds'.$j.''] !="") {
					//echo	 $clientdata['CurrentMeds'.$j.''];
			?>
					<div class="row">
						<div class="span3">
							<label id="Label1" class="user-name"></label>
						</div>
						<div class="span6 last" style="margin:0px;">
							<input class="text-input-field" type="text" name="CurrentMeds<?php echo $j; ?>" id="CurrentMeds1" value="<?php echo	 $clientdata['CurrentMeds'.$j.''];?>" />
							<span id="CurrentMeds1Msg" class="error ermsg"></span>
						</div>
					</div><!--End @row-->
				<?php
				} 
			} ?>
			<!--div class="span3" style="width: 12%;padding-left:21px;"> 
				<!--div class="addMoreBtn"><input type='button' value='Add More' class="submit-btn" id='addButton' style="padding: 5px 20px!important;"></div-->
			<!--/div-->	
			<div class="row" id="textBoxDiv" style="display:none;">
				<div class="span3"></div>
				<div id="addTxtbox" class="span6 last" style="margin:0px;">	 </div>
			</div>
			<!--div class="row">	
		        <div class="span3" style=""></div>
				<!--div class="span6 addMoreBtn"><input type='button' value='Add More' class="submit-btn" id='addButton' style="padding: 5px 20px!important;"></div-->	 
				<!--button class="btn btn-info" type="button" id="append" name="append">Add More</button>
			</div-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Are you on any mood altering or anti-depression medication?</span>
					<input type="radio" name="PsychMeds" id="PsychMeds" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['PsychMeds']=="Yes") {echo "checked"; }?> /> <label for="PsychMeds"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="PsychMeds" id="PsychMeds1" class="radio-fields regular-radio" <?php if($clientdata['PsychMeds']=="No") {echo "checked"; }?>/><label for="PsychMeds1"></label>
					<span class="select-txt">No</span>				
					<span id="PsychMedsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Have you ever used Accutane?</span>
					<input  type="radio" name="Accutane" id="Accutane" value="Yes" class="radio-fields regular-radio" onClick="enableTxt();" <?php if($clientdata['Accutane']=="Yes") {echo "checked"; }?> /><label for="Accutane"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Accutane" id="Accutane1" class="radio-fields regular-radio" onClick="disableTxt();" <?php if($clientdata['Accutane']=="No") {echo "checked"; }?>/><label for="Accutane1"></label>
					<span class="select-txt">No</span>				
					<span id="AccutaneMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">If yes, when enter your last use?</label>				
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="AccutaneLastUse" id="AccutaneLastUse" value="<?php echo $clientdata['AccutaneLastUse'];?>"  />
					<span id="AccutaneLastUseMsg" class="error ermsg"></span>					
				</div>				
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Are you presently using topcal Retinoid creams?</span>
					<input type="radio" name="Retinoids" id="Retinoids" value="Yes" class="radio-fields regular-radio" onClick="enableTxtBox();" <?php if($clientdata['Retinoids']=="Yes") { echo "checked"; } ?> /><label for="Retinoids"></label>
					 <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Retinoids" id="Retinoids1" class="radio-fields regular-radio" onClick="disableTxtBox();" <?php if($clientdata['Retinoids']=="No") { echo "checked"; } ?>/><label for="Retinoids1"></label>
					<span class="select-txt">No</span>				
					<span id="RetinoidsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->			
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">If yes, then was the date of last retinoid use?</label>
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="RetinoidsLastUse" id="RetinoidsLastUse"  value="<?php echo $clientdata['RetinoidsLastUse'];?>"/>
					<span id="RetinoidsLastUseMsg" class="error ermsg"></span>
				</div>				
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">What herbal supplements do you use regularly?</label>
				</div>
				<div class="span6 last">
					<input class="text-input-field"  type="text" name="Herbals" id="Herbals" value="<?php echo $clientdata['Herbals'];?>"/>
					<span id="HerbalsMsg" class="error ermsg"></span>
				</div>				
			</div><!--End @row-->
			<div class="divider-dotted">&nbsp;</div>						
			<!----List your current medications-->
			<h2>History</h2>			
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Have you received laser tattoo removal before?</span>
					<input type="radio" name="PriorTattooLaser" id="PriorTattooLaser" value="Yes" class="radio-fields regular-radio" onClick="enablePriorTattooLaserInfo();" <?php if($clientdata['PriorTattooLaser']=="Yes") {echo "checked"; }?>/>
					<label for="PriorTattooLaser"></label>
					<span class="select-txt">Yes</span>
					<input value="No" type="radio" name="PriorTattooLaser" id="PriorTattooLaser1" class="radio-fields regular-radio"  onClick="disablePriorTattooLaserInfo();" <?php if($clientdata['PriorTattooLaser']=="No") {echo "checked"; }?>/><label for="PriorTattooLaser1"></label>
					<span class="select-txt">No</span>				
					<span id="PriorTattooLaserMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span3">
					<label id="Label1" class="user-name">If yes, then please describe:</label>				
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="PriorTattooLaserInfo" id="PriorTattooLaserInfo" value="<?php echo $clientdata['PriorTattooLaserInfo'];?>" />
					<span id="PriorTattooLaserInfoMsg" class="error ermsg"></span>
				</div>				
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Have you had any recent tanning or sun exposure that changed the color of your skin?</span>
					<input type="radio" name="RecentTanning" id="RecentTanning" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['RecentTanning']=="Yes") {echo "checked"; }?>/><label for="RecentTanning"></label>
					<span class="select-txt">Yes</span>
					<input name="RecentTanning" type="radio" id="RecentTanning1"  class="radio-fields regular-radio" value="No" <?php if($clientdata['RecentTanning']=="No") {echo "checked"; }?>/><label for="RecentTanning1"></label>
					<span class="select-txt">No</span>				
					<span id="RecentTanningMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->			
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Have you recently used any self-tanning lotions or treatments?</span>
					<input type="radio" name="RecentSelfTanningLotions" id="RecentSelfTanningLotions" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['RecentSelfTanningLotions']=="Yes") {echo "checked"; }?> /><label for="RecentTanning"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="RecentSelfTanningLotions" id="RecentSelfTanningLotions1" value="No" class="radio-fields regular-radio" <?php if($clientdata['RecentSelfTanningLotions']=="No") {echo "checked"; }?>/><label for="RecentSelfTanningLotions1"></label>
					<span class="select-txt">No</span>				
					<span id="RecentSelfTanningLotionsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Do you form thick or raised scars from cuts or burns?</span>
					<input type="radio" name="BadScars" id="BadScars" value="Yes" class="radio-fields regular-radio" onClick="enableBadScarsInfoInfo();"  <?php if($clientdata['BadScars']=="Yes") {echo "checked"; }?>/><label for="BadScars"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="BadScars" id="BadScars1" value="No" class="radio-fields regular-radio"  onClick="disableBadScarsInfoInfo();" <?php if($clientdata['BadScars']=="NO") {echo "checked"; }?>/><label for="BadScars1"></label>
					<span class="select-txt">No</span>				
					<span id="BadScarsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">If yes, please describe any scarring issues:</label>				
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="BadScarsInfo" id="BadScarsInfo" value="<?php echo $clientdata['BadScarsInfo'];?>"/>
					<span id="BadScarsInfoMsg" class="error ermsg"></span>
				</div>				
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Do you have Hyper-pigmentation (darkening of the skin) or Hypo-pigmentation (lightening of the skin), or marks after a physical trauma?</span>
					<input type="radio" name="Dyspigmentation" id="Dyspigmentation" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['Dyspigmentation']=="Yes") {echo "checked"; }?>/> <label for="Dyspigmentation"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="Dyspigmentation" id="Dyspigmentation1" value="No" class="radio-fields regular-radio" <?php if($clientdata['Dyspigmentation']=="No") {echo "checked"; }?>/><label for="Dyspigmentation1"></label>
					<span class="select-txt">No</span>				
                    <span id="DyspigmentationMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->
			<?php
			if( $clientdata['SmokeCigarette'] != "") {
			?>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Do you smoke cigarettes?</span>
					<input type="radio" name="SmokeCigarette" id="SmokeCigarette" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['SmokeCigarette'] == "Yes") {echo "checked"; }?>/> <label for="SmokeCigarette"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="SmokeCigarette" id="SmokeCigarette1" value="No" class="radio-fields regular-radio" <?php if($clientdata['SmokeCigarette']=="No") {echo "checked"; }?>/><label for="SmokeCigarette1"></label>
					<span class="select-txt">No</span>

					<span id="SmokeCigaretteMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->						
			<?php
			}
			if( $clientdata['TattooAge'] != "" ) { 
			?>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Approximately how many years have you had the tattoo you wish to remove?</span>
					<select class="text-input-field select-option" name="TattooAge" id="TattooAge">
						<option value="">Select an option</option>
						<option value="-1" <?php if( $clientdata['TattooAge'] == "-1" ) { echo 'selected'; }?> > <1 year </option>
						<option value="1" <?php if( $clientdata['TattooAge'] == 1 ) { echo 'selected'; }?> >1 year</option>
						<option value="2" <?php if( $clientdata['TattooAge'] == 2 ) { echo 'selected'; }?> >2 years</option>
						<option value="3" <?php if( $clientdata['TattooAge'] == 3 ) { echo 'selected'; }?> >3 years</option>
						<option value="4" <?php if( $clientdata['TattooAge'] == 4 ) { echo 'selected'; }?> >4 years</option>
						<option value="5" <?php if( $clientdata['TattooAge'] == 5 ) { echo 'selected'; }?> >5 years</option>
						<option value="6" <?php if( $clientdata['TattooAge'] == 6 ) { echo 'selected'; }?> >6 years</option>
						<option value="7" <?php if( $clientdata['TattooAge'] == 7 ) { echo 'selected'; }?> >7 years</option>
						<option value="8" <?php if( $clientdata['TattooAge'] == 8 ) { echo 'selected'; }?> >8 years</option>
						<option value="9" <?php if( $clientdata['TattooAge'] == 9 ) { echo 'selected'; }?> >9 years</option>
						<option value="10" <?php if( $clientdata['TattooAge'] == 10 ) { echo 'selected'; }?> >10 years</option>
						<option value="11" <?php if( $clientdata['TattooAge'] == 11 ) { echo 'selected'; }?> >11 years</option>
						<option value="12" <?php if( $clientdata['TattooAge'] == 12 ) { echo 'selected'; }?> >12 years</option>
						<option value="13" <?php if( $clientdata['TattooAge'] == 13 ) { echo 'selected'; }?> >13 years</option>
						<option value="14" <?php if( $clientdata['TattooAge'] == 14 ) { echo 'selected'; }?> >14 years</option>
						<option value="15" <?php if( $clientdata['TattooAge'] == 15 ) { echo 'selected'; }?> >15 years</option>
						<option value="16" <?php if( $clientdata['TattooAge'] == 16 ) { echo 'selected'; }?> >16 years</option>
						<option value="17" <?php if( $clientdata['TattooAge'] == 17 ) { echo 'selected'; }?> >17 years</option>
						<option value="18" <?php if( $clientdata['TattooAge'] == 18 ) { echo 'selected'; }?> >18 years</option>
						<option value="19" <?php if( $clientdata['TattooAge'] == 19 ) { echo 'selected'; }?> >19 years</option>
						<option value="20" <?php if( $clientdata['TattooAge'] == 20 ) { echo 'selected'; }?> >20 years</option>
						<option value="20+" <?php if( $clientdata['TattooAge'] == "20+" ) { echo 'selected'; }?> > >20 years</option>
					</select>
					<span id="TattooAgeMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->				
			<?php
			}
			if( $clientdata['TattoColor'] != "" ) {
			?>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Which colors are present in your tattoo?</span>
					<div class="inputcheck-right">
						<?php
						$alltattocolor  = explode(',',$clientdata['TattoColor']);
						foreach($tattocolors as $tattocolor){
						?>
						<div class="tatto-color">
							<input type="checkbox" name="TattoColor" id="TattoColor" value="<?php echo $tattocolor['id']; ?>" class="radio-fields" <?php if(in_array($tattocolor['id'],$alltattocolor) ) {echo "checked"; } ?> /> <span class="select-txt"><?php echo $tattocolor['name']; ?></span>
						</div>	
						<?php
						}
						?>
					</div>
					<span id="TattoColorMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->			
			<?php
			} 
			if($clientdata['BodyPart'] != "" ) {
			?>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">On which body part is the tattoo you wish to remove?</span>
					<div class="inputcheck-right">
					<?php
					$allbodypart = explode(',',$clientdata['BodyPart']);
					foreach($bodypartchoices as $bodypartchoice){
					?>
					<div class="tatto-color">
						<input type="checkbox" name="BodyPart" id="BodyPart" value="<?php echo $bodypartchoice['id']; ?>" class="radio-fields" <?php if (in_array($bodypartchoice['id'],$allbodypart)) {echo "checked"; } ?> /> <span class="select-txt"><?php echo $bodypartchoice['part_name']; ?></span>
					</div>	
					<?php
					}
					?>
					</div>
					<span id="BodyPartMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->						
			<?php	
			} 
			?>						
			<div class="divider-dotted">&nbsp;</div>			
			<!----female clients-->
			<h2>Female Clients</h2>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Are you pregnant or trying to become pregnant?</span>
					<input type="radio" name="PregnancyRisk" id="PregnancyRisk" value="Yes" class="radio-fields regular-radio " <?php if($clientdata['PregnancyRisk']=="Yes") {echo "checked"; }?> /> <label for="PregnancyRisk"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="PregnancyRisk" id="PregnancyRisk1" value="No" class="radio-fields regular-radio" <?php if($clientdata['PregnancyRisk']=="No") {echo "checked"; }?>/><label for="PregnancyRisk1"></label>
					<span class="select-txt">No</span>				
					<span id="PregnancyRiskMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Are you breastfeeding?</span>
					<input type="radio" name="BreastFeeding" id="BreastFeeding" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['BreastFeeding']=="Yes") {echo "checked"; }?> /><label for="BreastFeeding"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="BreastFeeding" id="BreastFeeding1" value="No" class="radio-fields regular-radio"  <?php if($clientdata['BreastFeeding']=="No") {echo "checked"; }?>/><label for="BreastFeeding1"></label>
					<span class="select-txt">No</span>				
					<span id="BreastFeedingMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Are you using contraception?</span>
					<input type="radio" name="ContraceptionUse" id="ContraceptionUse" value="Yes" class="radio-fields regular-radio" <?php if($clientdata['ContraceptionUse']=="Yes") {echo "checked"; }?>/> <label for="ContraceptionUse"></label>
					<span class="select-txt">Yes</span>
					<input type="radio" name="ContraceptionUse" id="ContraceptionUse1" value="No" class="radio-fields regular-radio" <?php if($clientdata['ContraceptionUse']=="No") {echo "checked"; }?>/><label for="ContraceptionUse1"></label>
					<span class="select-txt">No</span>				
					<span id="ContraceptionUseMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->			
			<div class="divider-dotted">&nbsp;</div>			
			<!--notice--->
			<h2>Notice</h2>
			<!--p>Northeast Laser Tattoo Removal,LLC and its dba (doing business as) entities include Highbridge Laser and Fade Away Laser.</p-->
			
			<?php if($legalnotice!='') { echo $legalnotice; } else { ?>
			<p>This section is deliberately left blank.</p>
			<?php } ?>
			<div class="divider-dotted">&nbsp;</div>	
			<!--notice--->
			<h2>Risks</h2>
			<?php if($legalrisk!='') { 
		 echo $legalrisk;
		 } else { ?>
			<p><strong><i>Incomplete Removal:</i></strong> There is a chance that the treated tattoo will not remove completely	and some tattoos will have minimal reaction to the laser treatments. We cannot predict whether your tattoo can be completely removed.</p>
			<p class="space"></p>			
			<p> <strong><i>Scarring and Skin Textural Changes:</i></strong> However slight, there is always a risk of scarring. Skin texture changes (roughness, thickening, discoloration) can occur and may be permanent.</p>
			<p class="space"></p>
			<p><strong><i>Hyper-pigmentation:</i></strong>Darkening of your skin can occur after laser treatments. This is more commonly seen in patients with darker skin types, particularly those with Asian heritage, but
			can occur in any skin type. Hyper-pigmentation usually resolves spontaneously but can require additional treatment in some cases. </p>		
			<p class="space"></p>
			<p> <strong><i>Hypo-pigmentation:	</i></strong>  Lightening of your skin after treatment can occur after laser treatments.This can occur with any skin type and although it may resolve spontaneously, it can be permanent.</p>
			<p class="space"></p>
			<p> <strong><i>Infection:</i></strong> Although unusual; bacterial, fungal and viral infections can occur after being treated. Notify us if you have a history of cold sores prior to any treatments involving the face or any active infections for which you are being treated or should be treated prior to laser tattoo removal.</p>
			<p class="space"></p>
			<p><strong><i>Bleeding:</i></strong> Pinpoint bleeding can occur during a laser treatment, although it is rare in most cases. If you are on blood thinners such as Coumadin (Warfarin), Aspirin, Plavix, etc., please notify us for discussion with our physician prior to treatment.</p>
			<p class="space"></p>
			<p><strong><i>Blistering:</i></strong> Blistering is common after Q-switched laser treatments and is discussed in the after-care forms.</p>
			<p class="space"></p>
			<p> <strong><i>Allergic Reactions:</i></strong> Allergic reactions have been reported after Q-switched laser treatments of certain tattoo pigments, most commonly red inks, but can occur with other colors. Breakdown of tattoo inks is thought to trigger this reaction.</p>
			<p class="space"></p>
			<p> <strong><i>Darkening of Flesh Colored Tattoos:</i></strong> Some types of tattoo inks become darker upon treatment with Q-switched lasers. This is particularly common with white pigments. However, we can never know with certainty if an ink in your tattoo has been mixed with
			these pigments. If you experience tattoo ink darkening, this is often correctable, but may require additional treatments.</p>
			<p class="space"></p>
			<p> <strong><i>Eye protection:</i></strong> I understand that exposure of my unprotected eyes to the light created by laser operation could harm my vision. The eye protection provided will be worn at all times when the laser is in use (Eyewear will be provided).</p>
			<p class="space"></p>
			<p> <strong><i>Aftercare:</i></strong> Compliance with the provided aftercare is crucial to the healing of the affected area in the prevention of infection, scarring, timely healing and many other health factors.</p>		
			
	<?php }  ?>		
		<div class="divider-dotted">&nbsp;</div>	
		<h2>Privacy Rule</h2>
		<p><?php echo $BusinessName; ?> will use appropriate safeguards to protect the privacy of personal health information, and will not sell or give such information without patient authorization. </p>
		<div class="divider-dotted">&nbsp;</div>	
		<h2>Photo Release</h2>
		<p>We photograph all tattoos undergoing treatment for documentation. Selected tattoos may be used in marketing materials but will not include your name or other identifying information.</p>
		
		<div class="divider-dotted">&nbsp;</div>				
			<!--notice--->
			<h2>Liability Release</h2>
			<?php if($legalliability!="") {  echo $legalliability;
			/*if(isset($_SESSION['businessdata']) && $_SESSION['businessdata']['LiabilityRelease'] != "") { 
				?>
					<p><?php echo $_SESSION['businessdata']['LiabilityRelease']; ?></p>
				<?php 
					} else {
				?>
					<p>I certify that the information described above is accurate and complete to the best of my knowledge. Any questions I had have been answered to my satisfaction, and I understand the process and been explained all of the risks. By signing this I hereby release Northeast Laser Tattoo Removal and its dba entities Highbridge Laser and Fade Away Laser; Thomas Barrows, MD, Aleksandar Nedich, and any designated individual in relation to Northeast Laser Tattoo Removal from any and all legal or financial responsibilities. By signing this I am also allowing Northeast Laser Tattoo Removal to proceed with laser treatment of my tattoo.</p>
				<?php
					}*/
				} else { ?>
		<p>By signing this document,  I certify that my questions and concerns have adequately been addressed and I hereby release <?php echo $BusinessName; ?> and its owners and operators from any legal or financial liabilities pertaining to my laser treatments.</p>
		
		<?php } ?>
			<br/>			
			<div class="row">
				<div class="span3">
					<label id="Label1" class="user-name">Client Electronic Signature:</label>				
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="ClientSignature" id="ClientSignature" onKeyup="" value="<?php echo ucfirst($clientname['FirstName']).' '.ucfirst($clientname['LastName']); ?>" />
					<input class="text-input-field" type="hidden" name="ClientSignaturePrompt" id="ClientSignaturePrompt" />					
					<span id="ClientSignatureMsg" class="error"></span>					
				</div>				
			</div><!--End @row-->			
			<input type="hidden" name="ModifiedDate" id="ModifiedDate" value="<?=date('Y-m-d H:i:s'); ?>"/>		
			<div class="row">
				<div class="span12 user-name" style="text-transform:none;">
					<strong><i>AGREEMENT:</i></strong> By signing this Electronic Signature Acknowledgment Form, I agree that my electronic signature is the legally binding equivalent to my handwritten signature. Whenever I execute an electronic signature, it has the same validity and meaning as my handwritten signature. I will not, at any time in the future, repudiate the meaning of my electronic signature or claim that my electronic signature is not legally binding.				
				</div>
			</div><!--End @row-->		
			<!--input type="hidden" name="TimeStampClient" id="TimeStampClient" value="<?//=date('Y-m-d H:i:s'); ?>"/>
			<input type="hidden" name="Duluth" id="Duluth" value="<?//=$_SESSION['Emp_Location_Duluth']?>"/>
			<input type="hidden" name="Minneapolis" id="Minneapolis" value="<?//=$_SESSION['Emp_Location_Minneapolis']?>"/>
			<input type="hidden" name="St_Paul" id="St_Paul" value="<?//=$_SESSION['Emp_Location_St_Paul']?>"/>
			<input type="hidden" name="Location" id="Location" value="<?//=$_SESSION['Location']?>"/-->			
			<div class="row">
				<div class="span12">					
				<!--input class="submit-btn" type="submit" value="Submit" /-->
				</div>			
			</div><!--End @row-->
		</form>
		<script>
			if(form.PhysicianCare[1].checked == true){
				disablePhysicanCareReason();
			}
			if( form.Accutane[1].checked == true ){
				disableTxt();
			}
			if( form.Retinoids[1].checked == true ){
				disableTxtBox();
			}
			if( form.PriorTattooLaser[1].checked == true ){
				disablePriorTattooLaserInfo();
			}
			if( form.BadScars[1].checked == true ){
				disableBadScarsInfoInfo();
			}	
		</script>
	</div>
</div>
</div>
<?php
	include('admin_includes/footer.php');
?>
