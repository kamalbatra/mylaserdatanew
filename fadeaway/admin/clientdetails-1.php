<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$clientDetails = new dbFunctions();
if( !in_array(5,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard.php");
	</script>
<?php }
if(isset($_GET['orderby'])&& $_GET['name'] ){
	if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
		$order ="DESC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
		$order ="ASC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_asc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
		$order ="DESC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
		$order ="ASC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_asc.png";
	}
}else{
	$order ="DESC";
	$name = "ClientID";
	$ascDescImg=$domain."/images/s_desc.png";
}
?>
<script type="text/javascript">
$(function(){
	$(".searchclient").keyup(function(){ 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		if(searchid!=''){
			$.ajax({
				type: "POST",
				url: "ajax_clientsearch.php",
				data: dataString,
				cache: false,
				success: function(html){
				   $("#resultClientSerch").html(html).show();
				}
			});
		}return false;    
	});
	jQuery("#result").live("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		//alert(name)
		var decoded = $("<div/>").html($name).text();
		$('#searchid').val(decoded);
		$('#fname').val(decoded);
		//alert(decoded);
	});
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
			jQuery("#result").fadeOut(); 
		}
	});
	$('#searchid').click(function(){
		jQuery("#result").fadeIn();
	});
});
</script>
<?php
/*** fetch all Client with location**/
/*$sql = "SELECT tbl_clients.ClientID,tbl_clients.FirstName,tbl_clients.LastName ,tbl_clients.Location FROM tbl_consent_form  INNER JOIN tbl_clients ON tbl_clients.ClientID=tbl_consent_form.ClientID";
$result = mysql_query($sql);
echo "Nim row-".$countRow = mysql_num_rows($result);*/
$table1= "tbl_consent_form";
$table2= "tbl_clients";
$join="INNER";
//$cols = "tbl_clients.ClientID,tbl_clients.FirstName,tbl_clients.LastName ,tbl_clients.Location";
$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location";
/*** numrows**/
 $adjacents = 3;
 $reload="clientdetails.php";
 if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
	 $conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";	 
 }else{
	$conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] ORDER BY tbl_clients.ClientID DESC";
 }	
  $totalNumRows	= $clientDetails->joinTotalNumRows($table1,$table2,$join,$conditionNumRows);
$total_pages = $totalNumRows;
//$page="";
 if(isset($_GET['page'])){
	 $page=$_GET['page'];
	}
	else{
		$page="";
	}
 $limit = 10;                                  //how many items to show per page
    if($page)
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0; 
/*** numrow**/
if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
  $condition = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";
}else{
  $condition = "tbl_clients.ClientID=tbl_consent_form.ClientID WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] ORDER BY tbl_clients.".$name." ".$order." LIMIT ".$start.", ".$limit." ";
}
$clientdata	= $clientDetails->selectTableJoin($table1,$table2,$join,$condition,$cols);
//echo count($clientdata);
/*** fetch All client with location Name**/
?>
<div class="form-container" id="top1">
	   <div class="heading-container">
		  <h1 class="heading empHead">Manage Client</h1>
		  <!--div class="addNew"><a class="empLinks" href="">  </a></div-->		 
	   </div>
		<div class="user-entry-block">
			 <div class="row">
				 <!--div class="span6"></div-->
				 <div class="span6 last user-block-span user-block-last">
					 <label id="Label1" class="user-name">First name, last name or phone number:</label>					 
				  <input type="text" class="searchclient text-input-field" id="searchid" />
		          <div id="resultClientSerch" style="font-family:Verdana,Geneva,Tahoma,sans-serif"></div>
		         </div> 
		     </div>
<div class="tablebushead">	
	<div class="tablebusinner">	
			<div class="row sortingHead">	
				<div class="span3 srtHead srtHeadBorder"> <a title="<?php echo $order;?>" href="clientdetails.php?orderby=<?php echo $order;?>&name=firstname"> First Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a> </div>
				<div class="span3 srtHead srtHeadBorder">      <a title="<?php echo $order;?>" href="clientdetails.php?orderby=<?php echo $order;?>&name=lastname">Last Name  <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a> </div>
				<div class="span3 srtHeadloc srtHeadBorder"> Office Location(s) </div>
				<div class="span3 srtHeadEdit srtHeadBorder">Location(s)</div>
				<div class="span3 srtHeadEdit srtHeadBorder">Consent form</div>
				<div class="span3 srtHead srtHeadBorder" style="border:none;">Treatment pics</div>
			</div>
        	<?php if(!empty($clientdata)) { $i = 0;
				foreach($clientdata as $cldata){
					if($i%2==0){$bgdata = "bgnone";	}
						else{$bgdata = "bgdata";}								
			?>	
                   <div class="row  <?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>">
					 <div class="span3 srtHead srtcontent">
						<label id="" class="user-name"><?php echo ucfirst($cldata['FirstName']);?> </label>						
					</div>					
					<div class="span3 srtHead srtcontent">	
					  <label id="" class="user-name"><?php echo $cldata['LastName']?></label>
					</div>					
					<div class="span3 srtHeadloc srtcontent">	<label id="" class="user-name">
					<?php 
					      if($cldata['Location'] !=""){							  
					         $clentLocation = explode(",",$cldata['Location']);
					         for($j=0 ; $j<count($clentLocation);$j++){
							$tbl_manage_location	= "tbl_manage_location";
							$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
							$cols="*";
							$locationData	= $clientDetails->selectTableSingleRow($tbl_manage_location,$condition1);
							  echo $locationData['LocationName'];							  
							  if($j !=count($clentLocation)-1){
							  echo ",&nbsp;";
						     }
						  }//for loop close
						}
                     ?>
					  </label>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align">	
					  <label id="" class="user-name"><a href="clientdetailsedit?ClientID=<?php echo base64_encode($cldata['ClientID'])?>&action=clientdetails"><img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit location"/></a></label>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align">	
					  <label id="" class="user-name"><a href="clientconsentform?ClientID=<?php echo base64_encode($cldata['ClientID'])?>&action=clientdetails"><img src="<?php echo $domain; ?>/images/Images-icon.png" title="View consent form"/></a></label>
					</div>					
					<div class="span3 srtHead srtcontent text-align"><!--editclent-->
					  <label id="" class="user-name"><a href="alltreatments?ClientID=<?php echo base64_encode($cldata['ClientID'])?>&action=clientdetails"><img src="<?php echo $domain; ?>/images/Images-icon.png" title="View treatment pics" width="30px"/></a></label>
					</div>
			    </div><!--End @row-->				  
			  <?php $i++;  }?>
			  <?php echo $clientDetails->paginateShow($page,$total_pages,$limit,$adjacents,$reload)?>
			  <div id="statuResult"></div>
			  <?php }else echo "<div class='not-found-data'>No client found.</div>"; ?>
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
</div><!--End @container-->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
