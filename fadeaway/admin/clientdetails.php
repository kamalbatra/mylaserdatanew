<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$clientDetails = new dbFunctions();
	if( !in_array(5,$_SESSION["menuPermissions"])){ ?> 
		<script>
			window.location.replace("dashboard.php");
		</script>
	<?php }
	if(isset($_GET['orderby'])&& $_GET['name'] ){
		if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
			$order ="DESC";
			$name = "FirstName";
			$ascDescImg=$domain."/images/s_desc.png";
		}
		if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
			$order ="ASC";
			$name = "FirstName";
			$ascDescImg=$domain."/images/s_asc.png";
		}
		if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
			$order ="DESC";
			$name = "LastName";
			$ascDescImg=$domain."/images/s_desc.png";
		}
		if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
			$order ="ASC";
			$name = "LastName";
			$ascDescImg=$domain."/images/s_asc.png";
		}
	}else{
		$order ="DESC";
		$name = "ClientID";
		$ascDescImg=$domain."/images/s_desc.png";
	}
?>
	<script type="text/javascript">
		$(function(){
			$(".searchclient1").keyup(function(){ 
				var searchid = $(this).val();
				var dataString = 'search='+ searchid;
				if(searchid!=''){
					$.ajax({
						type: "POST",
						url: "ajax_clientsearch.php",
						data: dataString,
						cache: false,
						success: function(html){
						   $("#resultClientSerch1").html(html).show();
						}
					});
				}return false;    
			});
			jQuery("#result").live("click",function(e){ 
				var $clicked = $(e.target);
				var $name = $clicked.find('.name').html();
				//alert(name)
				var decoded = $("<div/>").html($name).text();
				$('#searchid1').val(decoded);
				$('#fname').val(decoded);
				//alert(decoded);
			});
			jQuery(document).live("click", function(e) { 
				var $clicked = $(e.target);
				if (! $clicked.hasClass("search")){
					jQuery("#result").fadeOut(); 
				}
			});
			$('#searchid1').click(function(){
				jQuery("#result").fadeIn();
			});
		});
	</script>
	<?php
	/*** fetch all Client with location**/
		$table1= "tbl_consent_form";
		$table2= "tbl_clients";
		$join="INNER";
		$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location";
	/*** numrows**/
		$adjacents = 3;
		$reload="clientdetails.php";
		if( isset($_GET['ClientID'])&& $_GET['ClientID']!="" ) {
			$conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";	 
		} else {
			$conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] ORDER BY tbl_clients.ClientID DESC";
		}	
		$totalNumRows	= $clientDetails->joinTotalNumRows($table1,$table2,$join,$conditionNumRows);
		$total_pages = $totalNumRows;
		
		if(isset($_GET['page'])){
			$page=$_GET['page'];
		}
		else{
			$page="";
		}
		$limit = 10;                                  //how many items to show per page
		if($page)
			$start = ($page - 1) * $limit;          //first item to display on this page
		else
			$start = 0; 
		/*** numrow**/
		if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
			$condition = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";
		} else {
			$condition = "tbl_clients.ClientID=tbl_consent_form.ClientID WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] ORDER BY tbl_clients.".$name." ".$order." LIMIT ".$start.", ".$limit." ";
		}
		$clientdata	= $clientDetails->selectTableJoin($table1,$table2,$join,$condition,$cols);
		/*** fetch All client with location Name**/
	?>	
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Client</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock manageemp">
						<div class="busniss-search searchbussiness last">
							<img src="../img/searchblk.png">
							<input type="hidden" name="location" id="location" class="text-input-field" id="pricing_amt" value="<?=$_SESSION['Location']?>"/>				 
							<input type="text" class="searchclient1 text-input-field" id="searchid1" placeholder="First name, last name or phone number ..."/>
							<div id="resultClientSerch1" style="font-family:Verdana,Geneva,Tahoma,sans-serif"></div>
						</div>
						<!-- div class="search-btn">
							<button>Search</button>
							<button class="addnewbtn"><img src="img/plus.png"> Add New</button>
						</div -->
					</div>		
					<div class="card-body">
					<?php
					    if(!empty($clientdata)) { 
							$i = 0;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th><a title="<?php echo $order;?>" href="clientdetails.php?orderby=<?php echo $order;?>&name=firstname"> First Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a></th>
										<th><a title="<?php echo $order;?>" href="clientdetails.php?orderby=<?php echo $order;?>&name=lastname">Last Name  <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a></th>
										<th class="span3 srtHeadloc srtHeadBorder"> Office Location(s)</th>
										<th class="span3 srtHeadEdit srtHeadBorder center-text">Location(s)</th>
										<th class="span3 srtHeadEdit srtHeadBorder center-text">Consent form</th>
										<th class="span3 srtHead srtHeadBorder center-text">Treatment pics</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									foreach($clientdata as $cldata){
										if($i%2==0) {
											$bgdata = "bgnone";	
										} else { 
											$bgdata = "bgdata"; 
										}
									?>
										<tr class="<?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>">
											<td class="span3 srtHeadEditEmp srtcontent"><label class="user-name"><?php echo ucfirst($cldata['FirstName']);?></label></td>
											<td class="span3 srtHeadEditEmp srtcontent"><label class="user-name"><?php echo $cldata['LastName']?></label></td>
											<td class="span3 srtHeadloc srtcontent"><label id="" class="user-name">
											<?php
												if($cldata['Location'] !=""){
													$clentLocation = explode(",",$cldata['Location']);
													for($j=0 ; $j<count($clentLocation);$j++){
														$tbl_manage_location	= "tbl_manage_location";
														$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
														$cols="*";
														$locationData	= $clientDetails->selectTableSingleRow($tbl_manage_location,$condition1);
														echo $locationData['LocationName'];							  
														if($j !=count($clentLocation)-1){
															echo ",&nbsp;";
														}
													}//for loop close
												}
											?></label>
											</td>
											<td class="span3 srtHeadEdit srtcontent text-align center-text">
												<label id="" class="user-name"><a href="clientdetailsedit?ClientID=<?php echo base64_encode($cldata['ClientID'])?>&action=clientdetails"><img src="<?php echo $domain; ?>/img/editimg.png" title="Edit location"/></a></label>
											</td>
											<td class="span3 srtHeadEdit srtcontent text-align center-text">
												<label id="" class="user-name"><a href="clientconsentform?ClientID=<?php echo base64_encode($cldata['ClientID'])?>&action=clientdetails"><img src="<?php echo $domain; ?>/img/eye-img.jpg" title="View consent form"/></a></label>
											</td>
											<td class="span3 srtHead srtcontent text-align center-text" ><!--editclent-->
												<label id="" class="user-name"><a href="alltreatments?ClientID=<?php echo base64_encode($cldata['ClientID'])?>&action=clientdetails"><img src="<?php echo $domain; ?>/img/eye-img.jpg" title="View treatment pics" width="30px"/></a></label>
											</td>
										</tr><!--End @row-block-->
										<?php
										$i++;
									} //foreach end
									if(isset($_POST) && $_POST != NULL  )  {
										if( $consentForms->update_user($tableUpdate,$_POST) ) { 
											echo "<span style='color:green;'>Information updated successfully.</span>"; 
										}
									}
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $clientDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No client found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
