<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$clientDetails = new dbFunctions();
if( !in_array(5,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard.php");
	</script>
<?php }
if(isset($_GET['orderby'])&& $_GET['name'] ){
	if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
		$order ="DESC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
		$order ="ASC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_asc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
		$order ="DESC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
		$order ="ASC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_asc.png";
	}
}else{
	$order ="DESC";
	$name = "ClientID";
	$ascDescImg=$domain."/images/s_desc.png";
}
?>
<script type="text/javascript">
$(function(){
	
	$(".searchclient").keyup(function(){ 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		if(searchid!=''){
			$.ajax({
				type: "POST",
				url: "ajax_clientsearch.php",
				data: dataString,
				cache: false,
				success: function(html){
				   $("#resultClientSerch").html(html).show();
				}
			});
		}return false;    
	});
	jQuery("#result").live("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		//alert(name)
		var decoded = $("<div/>").html($name).text();
		$('#searchid').val(decoded);
		$('#fname').val(decoded);
		//alert(decoded);
	});
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
			jQuery("#result").fadeOut(); 
		}
	});
	$('#searchid').click(function(){
		jQuery("#result").fadeIn();
	});

	
});
	function printDiv(divName) {
	alert('aa');
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		var mywindow = window.open('', 'print');
		var is_chrome = Boolean(mywindow.document);
		
		mywindow.document.write('<html><body >');
		mywindow.document.write('<link rel="stylesheet" href="<?php echo $domain; ?>/css/theme4.css"  type="text/css" />');
		mywindow.document.write('<link rel="stylesheet" href="<?php echo $domain; ?>/css/style.css"  type="text/css" />');
		mywindow.document.write(printContents);
		mywindow.document.write('</body></html>');
		if (is_chrome) {
			setTimeout(function() { // wait until all resources loaded 
				mywindow.document.close(); // necessary for IE >= 10
				mywindow.focus(); // necessary for IE >= 10
				mywindow.print(); // change window to winPrint
				mywindow.close(); // change window to winPrint
			}, 2);
		} else {
			mywindow.document.close(); // necessary for IE >= 10
			mywindow.focus(); // necessary for IE >= 10
			mywindow.print();
			mywindow.close();
		}
	}
</script>
<?php
/*** fetch all Client with location**/
$table1= "tbl_consent_form";
$table2= "tbl_clients";
$join="INNER";
$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location, AES_DECRYPT(tbl_clients.Email, '".SALT."') AS Email, AES_DECRYPT(tbl_clients.PhoneNumber, '".SALT."') AS PhoneNumber";
/*** numrows**/
 $adjacents = 3;
 $reload="clientdetailsdulath.php";
 if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
	 $conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";	 
 }else{
	$conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] AND tbl_clients.Location IN ('1','8','9') ORDER BY tbl_clients.ClientID DESC";
 }	
$totalNumRows	= $clientDetails->joinTotalNumRows($table1,$table2,$join,$conditionNumRows);
$total_pages = $totalNumRows;
//$page="";
 if(isset($_GET['page'])){
	 $page=$_GET['page'];
	}
	else{
		$page="";
	}
 $limit = 10;                                  //how many items to show per page
    if($page)
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0; 
/*** numrow**/
if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
  $condition = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";
}else{
  $condition = "tbl_clients.ClientID=tbl_consent_form.ClientID WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] AND tbl_clients.Location IN ('1','8','9')  ORDER BY tbl_clients.".$name." ".$order." LIMIT ".$start.", ".$limit." ";
}
echo "SELECT ".$cols." FROM ".$table1." ".$join." JOIN  ".$table2." ON ".$condition;
$clientdata	= $clientDetails->selectTableJoin($table1,$table2,$join,$condition,$cols);

/*** fetch All client with location Name**/
?>
			<div class="row">
				<div class="span6 last user-block-span user-block-last">
					<span class="submit-btn"> <a href="csvreport.php?dulath">Csv file</a></span>
					<a class="print-icon" href="javascript:void(0)" title="Print" onclick="printDiv('top1');" ><img src="<?php echo $domain; ?>/images/printicon.png" title="print list"/></a>	 
				</div> 
		    </div>
		    
<div class="form-container" id="top1">
	   <div class="heading-container">
		  <h1 class="heading empHead">Client Duluth Location </h1>
		  <!--div class="addNew"><a class="empLinks" href="">  </a></div-->		 
	   </div>
		<div class="user-entry-block">

<div class="tablebushead">	
	<div class="tablebusinner">	
			<div class="row sortingHead">	
				<div class="span3 srtHeadEdit srtHeadBorder sno">S. No.</div>
				<div class="span3 srtHead srtHeadBorder"> <a title="<?php echo $order;?>" href="clientdetailsdulath.php?orderby=<?php echo $order;?>&name=firstname"> First Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a> </div>
				<div class="span3 srtHead srtHeadBorder">      <a title="<?php echo $order;?>" href="clientdetailsdulath.php?orderby=<?php echo $order;?>&name=lastname">Last Name  <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a> </div>
				<div class="span3 srtHeadEdit srtHeadBorder emailheader">Email</div>
				<div class="span3 srtHeadEdit srtHeadBorder phoneheader">PhoneNumber</div>
			</div>
        	<?php if(!empty($clientdata)) { $i = 0;
				$a = 1;
				foreach($clientdata as $cldata){
					
					if($i%2==0){$bgdata = "bgnone";	}
						else{$bgdata = "bgdata";}								
			?>	
                   <div class="row  <?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>">
					<div class="span3 srtHead srtcontent sno">
						<label id="" class="user-name"><?php echo $a;?> </label>						
					</div>					
					<div class="span3 srtHead srtcontent">
						<label id="" class="user-name"><?php echo ucfirst($cldata['FirstName']);?> </label>						
					</div>					
					<div class="span3 srtHead srtcontent">	
					  <label id="" class="user-name"><?php echo $cldata['LastName']?></label>
					</div>					
					<div class="span3 srtHeadEdit srtcontent">	
					  <label id="" class="user-name"><?php echo $cldata['Email'];?></label>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align">	
					  <label id="" class="user-name"> <?php echo $cldata['PhoneNumber'];?> </label>
					</div>
			    </div><!--End @row-->				  
			  <?php $i++; $a++;  }?>
			  <?php echo $clientDetails->paginateShow($page,$total_pages,$limit,$adjacents,$reload)?>
			  <div id="statuResult"></div>
			  <?php }else echo "<div class='not-found-data'>No client found.</div>"; ?>
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
</div><!--End @container-->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
