<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$clientInfo	= new dbFunctions();
$table	= "tbl_clients";
$condition = " where ClientID =".base64_decode($_GET['ClientID'])." ";
$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location";
$clientdata = $clientInfo->selectTableSingleRow($table,$condition,$cols);
/**** Fetch all  locations******/
$tbl_manage_location = "tbl_manage_location";
$condition1 = "WHERE BusinessID = $_SESSION[BusinessID] GROUP BY LocationName ORDER BY ManageLocationId  ASC ";
$cols="*";
$locationData = $clientInfo->selectTableRows($tbl_manage_location,$condition1);
?>	
<script type="text/javascript">
jQuery(document).ready(function(){
	$('#locationBtn').click(function(){		 
		if($("input[type='checkbox']:checked").length > 0){
			$("#atLastOne").html('Please select at least one location!').hide();
			var str = $("#clientLocationEdit" ).serialize();
				$.ajax({
					type: "POST",
					url: "ajax_EditclientLocation.php",
					data: str,
					cache: false,
					success: function(result){	
						$("#clientLocationResult").html(result).show();
						//$("#clientLocationResult").fadeOut( "slow" );					
						$("#clientLocationResult").fadeOut(5000);
						//setTimeout(function() {location.reload()	}, 2000);  
					}
				});				
		}
		else{
			$("#atLastOne").html('Please select at least one location!').show();
			$("#clientLocationResult").hide();
		}		       
    });   
});
</script>
<style>
.row {
	font-family: Verdana,Geneva,Tahoma,sans-serif;
	font-size: 13px;
	font-weight: 500;
	margin-top: 16px;
	color: #666666;
}
</style>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Manage Client Location</h1>
		<div class="addNew"><a class="empLinks" href="clientdetails.php" class="submit-btn">Client List </a></div>
	</div>	
		<div class="user-entry-block">
<!--  dummy form to submit data  -->
<form action="" name="clientLocationEdit" id="clientLocationEdit" method="post">
		<div class="search-client-block">
      <div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">First Name:</label>
					<input type="hidden" value="<?php echo $clientdata['ClientID']; ?>" name="ClientID">
					<input class="text-input-field" type="text"  id="First_Name" value="<?php echo $clientdata['FirstName'];?>"  readonly="readonly"/>
				</div>				
				<div class="span3">
					<label id="Label1" class="user-name">Last Name:</label>
					<input class="text-input-field" type="text" id="Last_Name" value="<?php echo $clientdata['LastName'];?>" readonly="readonly"/>
				</div>
			</div><!--End @row-->			
			<!--div class="divider-dotted"> </div-->
			<h2 style="font-family:Verdana,Geneva,Tahoma,sans-serif">Office Locations</h2>    
		<div class="row">
				<div class="span12">					
					<?php $clentLocatio= explode(",",$clientdata['Location']);
					 //print_r($clentLocatio);
					/*for($j=0 ; $j<count($clentLocatio); $j++){
							 $check[]=$clentLocatio[$j];
						}
						print_r($check);*/
					 ?>
			     <?php $i=0; foreach($locationData as $ldata){ ?>	
				    <input type="checkbox" value="<?php echo $ldata['ManageLocationId'];?>" id="" name="Location[]"
				    <?php 									 
						 for($j=0 ; $j<  count($clentLocatio); $j++){
							 //echo $check[] = $clentLocatio[$j];
							 if($ldata['ManageLocationId']==$clentLocatio[$j]){
								 $selected = 1;							 
							 echo "checked";
							 }else{ 								 
								}								
						}
						   ?>				     
				        <span class="checkbox-txt location-name-font"><?php echo $ldata['LocationName'];;?></span>
				<?php $i++; } ?>
				<span id="atLastOne" class="error"></span> 
			   </div>	
			</div>	<!--@End  row-->			
             <div class="row">
				<div class="span3">				
     				<input type="button"  id="locationBtn" value="submit" class="submit-btn">				
				</div>
				 <div id="clientLocationResult" class="span6 mngdevicesName" style="display:none;"><img alt="loading...." src="../images/loading.gif"></div>			
			</div><!--End @row-->		
	</div>			
</form>
</div>
</div>
</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
