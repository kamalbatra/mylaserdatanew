<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
		
	$clientInfo	= new dbFunctions();
	$table	= "tbl_clients";
	$condition = " where ClientID =".base64_decode($_GET['ClientID'])." ";
	$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location";
	$clientdata = $clientInfo->selectTableSingleRow($table,$condition,$cols);
	/**** Fetch all  locations******/
	$tbl_manage_location = "tbl_manage_location";
	$condition1 = "WHERE BusinessID = $_SESSION[BusinessID] GROUP BY LocationName ORDER BY ManageLocationId  ASC ";
	$cols="*";
	$locationData = $clientInfo->selectTableRows($tbl_manage_location,$condition1);
	?>	
	<script type="text/javascript">
	jQuery(document).ready(function(){
		$('#locationBtn').click(function(){		 
			if($("input[type='checkbox']:checked").length > 0){
				$("#atLastOne").html('Please select at least one location!').hide();
				var str = $("#clientLocationEdit" ).serialize();
					$.ajax({
						type: "POST",
						url: "ajax_EditclientLocation.php",
						data: str,
						cache: false,
						success: function(result){	
							$("#clientLocationResult").html(result).show();
							$("#clientLocationResult").fadeOut(5000);
						}
					});				
			}
			else{
				$("#atLastOne").html('Please select at least one location!').show();
				$("#clientLocationResult").hide();
			}		       
		});   
	});
	</script>	
	
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Client Location</h1>
				</div>	
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="clientdetails.php" class="submit-btn"><button class="addnewbtn">Client List </button></a>
						</div>
					</div>

					<form action="" name="clientLocationEdit" id="clientLocationEdit" method="post">
					
						<div class="formcontentblock-ld">

							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">First Name:</label>
											<input type="hidden" value="<?php echo $clientdata['ClientID']; ?>" name="ClientID">
											<input class="text-input-field" type="text" id="First_Name" value="<?php echo $clientdata['FirstName'];?>" readonly="readonly" />
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Last Name:</label>
											<input class="text-input-field" type="text" id="Last_Name" value="<?php echo $clientdata['LastName'];?>" readonly="readonly" />
										</div>
									</div>
								</div>
							</div>

							<div class="commonboldHeading">Office Locations</div>

							<div class="form-row-ld">
								<div class="full">
									<div class="form-col-ld">
										<div class="inputblock-ld radiolabel">

											<?php $clentLocatio= explode(",",$clientdata['Location']);	 ?>
												<div class="radioblocksBtns">
												<?php 
													$i=0; 
													foreach($locationData as $ldata){ ?>
														<div class="radioBtn ckeckboxres">
															
															<input type="checkbox" value="<?php echo $ldata['ManageLocationId'];?>" id="<?php echo $ldata['ManageLocationId'];?>" name="Location[]" <?php for($j=0 ; $j< count($clentLocatio); $j++){ if($ldata[ 'ManageLocationId']==$clentLocatio[$j]){ $selected=1 ; echo "checked"; } else { } } ?>>
															<label for="<?php echo $ldata['ManageLocationId'];?>">
																<?php echo $ldata['LocationName'];?>
															</label>
														</div>
														<?php $i++; 					
																	} ?>
												</div>

										</div>
									</div>
								</div>
							</div>

							<div class="form-row-ld">
								<div class="backNextbtn">
									<button type="button" id="locationBtn" class="nextbtn submit-btn">SUBMIT</button>
								</div>
								<div id="clientLocationResult" class="u_mess" style="display:none;"><img alt="loading...." src="../images/loading.gif"></div>			
							</div>
						</div>

					</form>
                    

				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
