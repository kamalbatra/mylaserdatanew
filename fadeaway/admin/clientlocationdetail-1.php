<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$clientDetails = new dbFunctions();
	if( !in_array(5,$_SESSION["menuPermissions"])){ 
?> 
		<script>
			window.location.replace("dashboard.php");
		</script>
<?php 
	}

	
	if(isset($_GET['orderby'])&& $_GET['name'] ){

		if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
			$order ="DESC";
			$name = "FirstName";
			$ascDescImg=$domain."/images/s_desc.png";
		}
		if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
			$order ="ASC";
			$name = "FirstName";
			$ascDescImg=$domain."/images/s_asc.png";
		}
		if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
			$order ="DESC";
			$name = "LastName";
			$ascDescImg=$domain."/images/s_desc.png";
		}
		if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
			$order ="ASC";
			$name = "LastName";
			$ascDescImg=$domain."/images/s_asc.png";
		}

		if( empty($_POST) ){
			// $_SESSION['business'] = '';
			$business = "BusinessID = '".$_SESSION['business']."'";
			// $_SESSION['selectlocation'] = '';
			$location = "AND Location = '".$_SESSION['selectlocation']."'";
		}
		else {
			if( isset($_POST['businessid']) && $_POST['businessid']!= '' ){
				$_SESSION['business'] = $_POST['businessid'];
				$business = "BusinessID = '".$_SESSION['business']."'";
			}
			else{
				if( isset($_SESSION['business']) && $_SESSION['business'] != ''){
					$business = "BusinessID = '".$_SESSION['business']."'";
				} else {
					$business = "BusinessID = '0'";
				}
			}
			if( isset($_POST['selectlocation']) && $_POST['selectlocation']!= '' ){
				$_SESSION['selectlocation'] = $_POST['selectlocation'];
				$location = "AND Location = '".$_SESSION['selectlocation']."'";
			}
			else{
				if( isset($_SESSION['selectlocation']) && $_SESSION['selectlocation'] !='' ){
					$location = "AND Location = '".$_SESSION['selectlocation']."'";
				}else {
					$location = "AND Location = ''";	
				}	
				
			}
		}
	
	} else if(isset($_GET['page'])){

		$order ="ASC";
		$name = "FirstName";
		
		$page=$_GET['page'];
		
		if( empty($_POST) ){
			// $_SESSION['business'] = '';
			$business = "BusinessID = '".$_SESSION['business']."'";
			// $_SESSION['selectlocation'] = '';
			$location = "AND Location = '".$_SESSION['selectlocation']."'";
		}
		else {
			if( isset($_POST['businessid']) && $_POST['businessid']!= '' ){
				$_SESSION['business'] = $_POST['businessid'];
				$business = "BusinessID = '".$_SESSION['business']."'";
			}
			else{
				if( isset($_SESSION['business']) && $_SESSION['business'] != ''){
					$business = "BusinessID = '".$_SESSION['business']."'";
				} else {
					$business = "BusinessID = '0'";
				}
			}
			if( isset($_POST['selectlocation']) && $_POST['selectlocation']!= '' ){
				$_SESSION['selectlocation'] = $_POST['selectlocation'];
				$location = "AND Location = '".$_SESSION['selectlocation']."'";
			}
			else{
				if( isset($_SESSION['selectlocation']) && $_SESSION['selectlocation'] !='' ){
					$location = "AND Location = '".$_SESSION['selectlocation']."'";
				}else {
					$location = "AND Location = ''";	
				}	
				
			}
		}
		
	} else{

		$page="";
		
		$order ="ASC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_desc.png";
		
		if( empty($_POST) ){
			$_SESSION['business'] = '';
			$business = "BusinessID = '".$_SESSION['business']."'";
			$_SESSION['selectlocation'] = '';
			$location = "AND Location = '".$_SESSION['selectlocation']."'";
		}
		else {
			if( isset($_POST['businessid']) && $_POST['businessid']!= '' ){
				$_SESSION['business'] = $_POST['businessid'];
				$business = "BusinessID = '".$_SESSION['business']."'";
			}
			else{
				if( isset($_SESSION['business']) && $_SESSION['business'] != ''){
					$business = "BusinessID = '".$_SESSION['business']."'";
				} else {
					$business = "BusinessID = '0'";
				}
			}
			if( isset($_POST['selectlocation']) && $_POST['selectlocation']!= '' ){
				$_SESSION['selectlocation'] = $_POST['selectlocation'];
				$location = "AND Location = '".$_SESSION['selectlocation']."'";
			}
			else{
				if( isset($_SESSION['selectlocation']) && $_SESSION['selectlocation'] !='' ){
					$location = "AND Location = '".$_SESSION['selectlocation']."'";
				}else {
					$location = "AND Location = ''";	
				}	
				
			}
		}
	
	
	}
	
	
	//echo "POST = "; print_r($_POST);
	// echo "==session = "; print_r($_SESSION['business'].'s==S'.$_SESSION['selectlocation']); echo "ok==";
?>
<script type="text/javascript">
	$(function(){
		
		$("#locationclient").validate({
			rules: {                 
                businessid:"required",
                selectlocation:"required",
			},                
			messages: {                    
				businessid: {
					required: "Please select a business." ,
				},
				selectlocation: {
					required: "Please select a location." ,
				},
			}
		});
		
		$('#businessid').change(function(){
			var str = $('#businessid').val();
			
			$.ajax({
				type: "POST",
				url: "ajax_businesslocation.php",
				data: {businessid:str},
				cache: false,
				success: function(result){
					if(result == '' ){
						$('#locationcon').hide();
						$('#location').html('');
						$('#nolocation').show();
						$('#csvbutton').show();
						
					} else {
						$('#nolocation').hide();
						$('#locationcon').show();
						$('#location').html(result);											
						$('#csvbutton').show();
					}

				}
			});	
		});

/********** Location on load *****************************/		
		var business = '<?php echo $_SESSION['business'] ?>';
		var location = '<?php echo $_SESSION['selectlocation'] ?>';
		if( location == '' ){
			
		} else {
			$.ajax({
				type: "POST",
				url: "ajax_businesslocation.php",
				data: {businessid:business},
				cache: false,
				success: function(result){
					if(result == '' ){
						$('#locationcon').hide();
						$('#location').html('');
						$('#nolocation').show();
						$('#csvbutton').show();
					} else {
						$('#nolocation').hide();
						$('#locationcon').show();
						$('#location').html(result);										
						$('#csvbutton').show();						
					}

				}
			});	
		}
/*********************************************/		

		$(".searchclient").keyup(function(){ 
			var searchid = $(this).val();
			var dataString = 'search='+ searchid;
			if(searchid!=''){
				$.ajax({
					type: "POST",
					url: "ajax_clientsearch.php",
					data: dataString,
					cache: false,
					success: function(html){
					   $("#resultClientSerch").html(html).show();
					}
				});
			}return false;    
		});
		
		jQuery("#result").live("click",function(e){ 
			var $clicked = $(e.target);
			var $name = $clicked.find('.name').html();
			//alert(name)
			var decoded = $("<div/>").html($name).text();
			$('#searchid').val(decoded);
			$('#fname').val(decoded);
			//alert(decoded);
		});
		
		jQuery(document).live("click", function(e) { 
			var $clicked = $(e.target);
			if (! $clicked.hasClass("search")){
				jQuery("#result").fadeOut(); 
			}
		});
		
		$('#searchid').click(function(){
			jQuery("#result").fadeIn();
		});
	});
	
	function printDiv(divName) {
	
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		var mywindow = window.open('', 'print');
		var is_chrome = Boolean(mywindow.document);
		
		mywindow.document.write('<html><body >');
		mywindow.document.write('<link rel="stylesheet" href="<?php echo $domain; ?>/css/theme4.css"  type="text/css" />');
		mywindow.document.write('<link rel="stylesheet" href="<?php echo $domain; ?>/css/style.css"  type="text/css" />');
		mywindow.document.write(printContents);
		mywindow.document.write('</body></html>');
		if (is_chrome) {
			setTimeout(function() { // wait until all resources loaded 
				mywindow.document.close(); // necessary for IE >= 10
				mywindow.focus(); // necessary for IE >= 10
				mywindow.print(); // change window to winPrint
				mywindow.close(); // change window to winPrint
			}, 2);
		} else {
			mywindow.document.close(); // necessary for IE >= 10
			mywindow.focus(); // necessary for IE >= 10
			mywindow.print();
			mywindow.close();
		}
	}
	
</script>

<?php
	$business = $business.' '.$location;
	
	/*** fetch all Client with location**/
	$table1= "tbl_clients";
	$join="INNER";
	$cols = "ClientID, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, Location, AES_DECRYPT(Email, '".SALT."') AS Email, AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber";
	/*** numrows**/
 
	$adjacents = 3;
	$reload="clientlocationdetail-1.php";
	if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
		$conditionNumRows  = "WHERE ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY FirstName ";	 
	}else{
		$conditionNumRows  = "WHERE ".$business." ORDER BY ClientID DESC";
	}	
	$totalNumRows	= $clientDetails->TotalclientRows($table1,$cols,$conditionNumRows);
	$total_pages = $totalNumRows;
	
	
	//~ if(isset($_GET['page'])){
		//~ $page=$_GET['page'];
	//~ }
	//~ else{
		//~ $page="";
	//~ }
	$limit = 10;                                  //how many items to show per page
    if($page)
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0; 
	/*** numrow**/
	
	if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
		$condition = "WHERE ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";
	} else {
		$condition = "WHERE ".$business." ORDER BY ".$name." ".$order." LIMIT ".$start.", ".$limit." ";
	}

	$clientdata	= $clientDetails->selectclientrecord($table1,$cols,$condition);
	
	$var = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);
	$businessquery = "SELECT BusinessID,BusinessName FROM `tbl_business` WHERE status = 1 ORDER BY `BusinessName` ASC ";
	$NO = mysqli_query($var,$businessquery) or die($businessquery);
	$allbusiness  = array();
	while( $row = mysqli_fetch_assoc($NO)) {
		$allbusiness[]=$row;
	}

	/*** fetch All client with location Name**/
?>

	
	<form id="locationclient" class="locationclient" name="locationclient" action="" method="POST">
		
		<div class="row">
			<div class="spanloc">	
				<label for="username" class="user-name">Select Business:</label>
				<select class="select-option" name="businessid" id="businessid">
					<option value="">Please select a busniess</option>
					<?php 
					foreach( $allbusiness as $business){
					?>	
					<option value="<?php echo $business['BusinessID']; ?>" <?php if($business['BusinessID'] == $_SESSION['business'] ){ echo 'selected="selected"'; } ?> ><?php echo ucfirst($business['BusinessName']); ?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="spanloc">	
				<div id="locationcon" class="locationcon" style="display:none;">
					<label for="username" class="user-name">Select Location:</label>
					<div id="location"></div>				
				</div>
				<div id="nolocation" class="nolocation" style="display:none;">
					<div id="noinlocation">No location found.</div>	
				</div>
				
			</div>
			<div class="spanloc">
				<input type="submit" class="submit-btn" id="getclient" value="Search">
			</div>

		</div>
		
	</form>
	
	
		    
	<div class="form-container locationclientcontainer" id="top1">
	
		<div class="heading-container">
			<h1 class="heading empHead">Client Location </h1>
			<div id="csvbutton" class="spanloc">
				<?php 
				if($_SESSION['business'] == ''){
					$busid = 0;
				} else {
					$busid = $_SESSION['business'];
				}
				if($_SESSION['selectlocation'] == ''){
					$locid = 0;	
				} else{
					$locid = $_SESSION['selectlocation'];
				}	
				
				?>
				<span class="submit-btn"> <a href="csvreport.php?bid=<?php echo $busid; ?>&loc=<?php echo $locid; ?>">Csv file</a></span>
			</div> 
		</div>
		<div class="user-entry-block">
			<div class="tablebushead">	
				<div class="tablebusinner">	
					<div class="row sortingHead">	
						<div class="span4 srtHeadEdit srtHeadBorder sno">S. No.</div>
						<div class="span4 srtHead srtHeadBorder"><a title="<?php echo $order;?>" href="clientlocationdetail.php?orderby=<?php echo $order;?>&name=firstname"> First Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a> </div>
						<div class="span4 srtHead srtHeadBorder"><a title="<?php echo $order;?>" href="clientlocationdetail.php?orderby=<?php echo $order;?>&name=lastname">Last Name  <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a> </div>
						<div class="span4 srtHeadEdit srtHeadBorder emailheader">Email</div>
						<div class="span4 srtHeadEdit srtHeadBorder phoneheader">PhoneNumber</div>
					</div>
					<?php 
					if(!empty($clientdata)) { 
						$i = 0;
						$a = 1;
						
						foreach($clientdata as $cldata){
							if($i%2==0){
								$bgdata = "bgnone";	
							} else { 
								$bgdata = "bgdata";
							}								
					?>	
							<div class="row  <?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>">
								<div class="span4 srtHead srtcontent sno">
									<label id="" class="user-name"><?php echo $a;?> </label>						
								</div>					
								<div class="span4 srtHead srtcontent">
									<label id="" class="user-name"><?php echo ucfirst($cldata['FirstName']);?> </label>						
								</div>					
								<div class="span4 srtHead srtcontent">	
								  <label id="" class="user-name"><?php echo $cldata['LastName']?></label>
								</div>					
								<div class="span4 srtHeadEdit srtcontent emailheader">	
								  <label id="" class="user-name"><?php echo $cldata['Email'];?></label>
								</div>
								<div class="span4 srtHeadEdit srtcontent phoneheader">	
								  <label id="" class="user-name"> <?php echo $cldata['PhoneNumber'];?> </label>
								</div>
							</div><!--End @row-->				  
					<?php 
							$i++; 
							$a++;  
						}
						echo $clientDetails->paginateShow($page,$total_pages,$limit,$adjacents,$reload);
					?>
						<div id="statuResult"></div>
					<?php 
					}
					else{
						echo "<div class='not-found-data'>No client found.</div>";	
					}
					?>
				</div><!--End @user-entry-block-->
				
			</div><!-- End  @form-container--->
		</div><!--End @container-->
	</div><!--End @container-->
	
	</div><!--End @container-->
<?php
	include('admin_includes/footer.php');
?>
