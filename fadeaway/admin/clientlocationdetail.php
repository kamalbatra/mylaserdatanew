<?php
	include('admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$clientDetails = new dbFunctions();
	if( !in_array(5,$_SESSION["menuPermissions"])) {
?> 
		<script>
			window.location.replace("dashboard.php");
		</script>
<?php 
	}

	if(isset($_GET['orderby'])&& $_GET['name'] ){
		if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
			$order ="DESC";
			$name = "FirstName";
			$ascDescImg=$domain."/images/s_desc.png";
		}
		if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
			$order ="ASC";
			$name = "FirstName";
			$ascDescImg=$domain."/images/s_asc.png";
		}
		if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
			$order ="DESC";
			$name = "LastName";
			$ascDescImg=$domain."/images/s_desc.png";
		}
		if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
			$order ="ASC";
			$name = "LastName";
			$ascDescImg=$domain."/images/s_asc.png";
		}

		if( empty($_POST) ){
			$business = "BusinessID = '".$_SESSION['business']."'";
			$location = "AND Location = '".$_SESSION['selectlocation']."'";
		}
		else {
			if( isset($_POST['businessid']) && $_POST['businessid']!= '' ){
				$_SESSION['business'] = $_POST['businessid'];
				$business = "BusinessID = '".$_SESSION['business']."'";
			}
			else{
				if( isset($_SESSION['business']) && $_SESSION['business'] != ''){
					$business = "BusinessID = '".$_SESSION['business']."'";
				} else {
					$business = "BusinessID = '0'";
				}
			}
			if( isset($_POST['selectlocation']) && $_POST['selectlocation']!= '' ){
				$_SESSION['selectlocation'] = $_POST['selectlocation'];
				$location = "AND Location = '".$_SESSION['selectlocation']."'";
			}
			else{
				if( isset($_SESSION['selectlocation']) && $_SESSION['selectlocation'] !='' ){
					$location = "AND Location = '".$_SESSION['selectlocation']."'";
				}else {
					$location = "AND Location = ''";	
				}	
				
			}
		}
	
	} else if(isset($_GET['page'])) {

		$order ="ASC";
		$name = "FirstName";
		$page=$_GET['page'];
		if( empty($_POST) ) {
			$business = "BusinessID = '".$_SESSION['business']."'";
			$location = "AND Location = '".$_SESSION['selectlocation']."'";
		}
		else {
			if( isset($_POST['businessid']) && $_POST['businessid']!= '' ) {
				$_SESSION['business'] = $_POST['businessid'];
				$business = "BusinessID = '".$_SESSION['business']."'";
			}
			else {
				if( isset($_SESSION['business']) && $_SESSION['business'] != '') {
					$business = "BusinessID = '".$_SESSION['business']."'";
				} else {
					$business = "BusinessID = '0'";
				}
			}
			if( isset($_POST['selectlocation']) && $_POST['selectlocation']!= '' ) {
				$_SESSION['selectlocation'] = $_POST['selectlocation'];
				$location = "AND Location = '".$_SESSION['selectlocation']."'";
			}
			else {
				if( isset($_SESSION['selectlocation']) && $_SESSION['selectlocation'] !='' ) {
					$location = "AND Location = '".$_SESSION['selectlocation']."'";
				} else {
					$location = "AND Location = ''";	
				}	
			}
		}
		
	} else {
		$page="";
		$order ="ASC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_desc.png";
		
		if( empty($_POST) ) {
			$_SESSION['business'] = '';
			$business = "BusinessID = '".$_SESSION['business']."'";
			$_SESSION['selectlocation'] = '';
			$location = "AND Location = '".$_SESSION['selectlocation']."'";
		}
		else {
			if( isset($_POST['businessid']) && $_POST['businessid']!= '' ) {
				$_SESSION['business'] = $_POST['businessid'];
				$business = "BusinessID = '".$_SESSION['business']."'";
			}
			else {
				if( isset($_SESSION['business']) && $_SESSION['business'] != '') {
					$business = "BusinessID = '".$_SESSION['business']."'";
				} else {
					$business = "BusinessID = '0'";
				}
			}
			if( isset($_POST['selectlocation']) && $_POST['selectlocation']!= '' ) {
				$_SESSION['selectlocation'] = $_POST['selectlocation'];
				$location = "AND Location = '".$_SESSION['selectlocation']."'";
			}
			else {
				if( isset($_SESSION['selectlocation']) && $_SESSION['selectlocation'] !='' ) {
					$location = "AND Location = '".$_SESSION['selectlocation']."'";
				} else {
					$location = "AND Location = ''";	
				}
			}
		}
	}
?>
<script type="text/javascript">
	$(function(){
		
		$("#locationclient").validate({
			errorClass: 'errorblocks',
			errorElement: 'div',
			rules: {                 
                businessid:"required",
                selectlocation:"required",
			},                
			messages: {                    
				businessid: {
					required: "Please select a business." ,
				},
				selectlocation: {
					required: "Please select a location." ,
				},
			}
		});
		
		$('#businessid').change(function(){
			var str = $('#businessid').val();
			
			$.ajax({
				type: "POST",
				url: "ajax_businesslocation.php",
				data: {businessid:str},
				cache: false,
				success: function(result){
					
					if(result == '' ){
						$('#locationcon').hide();
						$('#location1').html('');
						$('#nolocation').show();
						$('#csvbutton').show();
					} else {
						$('#nolocation').hide();
						$('#locationcon').show();
						$('#location1').html(result);											
						$('#csvbutton').show();
					}

				}
			});	
		});

/********** Location on load *****************************/		
		var business = '<?php echo $_SESSION['business'] ?>';
		var location = '<?php echo $_SESSION['selectlocation'] ?>';
		if( location == '' ){
			
		} else {
			$.ajax({
				type: "POST",
				url: "ajax_businesslocation.php",
				data: {businessid:business},
				cache: false,
				success: function(result){
					if(result == '' ){
						$('#locationcon').hide();
						$('#location1').html('');
						$('#nolocation').show();
						$('#csvbutton').show();
					} else {
						$('#nolocation').hide();
						$('#locationcon').show();
						$('#location1').html(result);										
						$('#csvbutton').show();						
					}

				}
			});	
		}
/*********************************************/		

		$(".searchclient").keyup(function(){ 
			var searchid = $(this).val();
			var dataString = 'search='+ searchid;
			if(searchid!=''){
				$.ajax({
					type: "POST",
					url: "ajax_clientsearch.php",
					data: dataString,
					cache: false,
					success: function(html){
					   $("#resultClientSerch").html(html).show();
					}
				});
			}return false;    
		});
		
		jQuery("#result").live("click",function(e){ 
			var $clicked = $(e.target);
			var $name = $clicked.find('.name').html();
			//alert(name)
			var decoded = $("<div/>").html($name).text();
			$('#searchid').val(decoded);
			$('#fname').val(decoded);
			//alert(decoded);
		});
		
		jQuery(document).live("click", function(e) { 
			var $clicked = $(e.target);
			if (! $clicked.hasClass("search")){
				jQuery("#result").fadeOut(); 
			}
		});
		
		$('#searchid').click(function(){
			jQuery("#result").fadeIn();
		});
	});
	
	function printDiv(divName) {
	
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		var mywindow = window.open('', 'print');
		var is_chrome = Boolean(mywindow.document);
		
		mywindow.document.write('<html><body >');
		mywindow.document.write('<link rel="stylesheet" href="<?php echo $domain; ?>/css/theme4.css"  type="text/css" />');
		mywindow.document.write('<link rel="stylesheet" href="<?php echo $domain; ?>/css/style.css"  type="text/css" />');
		mywindow.document.write(printContents);
		mywindow.document.write('</body></html>');
		if (is_chrome) {
			setTimeout(function() { // wait until all resources loaded 
				mywindow.document.close(); // necessary for IE >= 10
				mywindow.focus(); // necessary for IE >= 10
				mywindow.print(); // change window to winPrint
				mywindow.close(); // change window to winPrint
			}, 2);
		} else {
			mywindow.document.close(); // necessary for IE >= 10
			mywindow.focus(); // necessary for IE >= 10
			mywindow.print();
			mywindow.close();
		}
	}
	
</script>
<?php
	$business = $business.' '.$location;
	/*** fetch all Client with location**/
	$table1= "tbl_clients";
	$join="INNER";
	$cols = "ClientID, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, Location, AES_DECRYPT(Email, '".SALT."') AS Email, AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber";
	/*** numrows**/
	$adjacents = 3;
	$reload="clientlocationdetail.php";
	if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
		$conditionNumRows  = "WHERE ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY FirstName ";	 
	}else{
		$conditionNumRows  = "WHERE ".$business." ORDER BY ClientID DESC";
	}	
	$totalNumRows	= $clientDetails->TotalclientRows($table1,$cols,$conditionNumRows);
	$total_pages = $totalNumRows;
	
	$limit = 10;                                  //how many items to show per page
    if($page)
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0; 
	/*** numrow**/
	if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
		$condition = "WHERE ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";
	} else {
		$condition = "WHERE ".$business." ORDER BY ".$name." ".$order." LIMIT ".$start.", ".$limit." ";
	}

	$clientdata	= $clientDetails->selectclientrecord($table1,$cols,$condition);
	
	$var = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);
	$businessquery = "SELECT BusinessID,BusinessName FROM `tbl_business` WHERE status = 1 ORDER BY `BusinessName` ASC ";
	$NO = mysqli_query($var,$businessquery) or die($businessquery);
	$allbusiness  = array();
	while( $row = mysqli_fetch_assoc($NO)) {
		$allbusiness[]=$row;
	}
	/*** fetch All client with location Name**/
?>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Client Location</h1>
				</div>
				<div class="card shadow mb-4 table-main-con marketing-report">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<?php 
							if($_SESSION['business'] == ''){
								$busid = 0;
							} else {
								$busid = $_SESSION['business'];
							}
							if($_SESSION['selectlocation'] == ''){
								$locid = 0;	
							} else {
								$locid = $_SESSION['selectlocation'];
							}
							?>
							<a href="csvreport.php?bid=<?php echo $busid; ?>&loc=<?php echo $locid; ?>"><button class="addnewbtn">Csv file</button></a>
						</div>
					</div>
					<div class="bussiness-searchblock">
						<form id="locationclient" class="locationclient" name="locationclient" action="" method="POST">
							<div class="busniss-search">
								<select class="select-option" name="businessid" id="businessid">
									<option value="">Please select a busniess</option>
									<?php 
									foreach( $allbusiness as $business){
									?>	
									<option value="<?php echo $business['BusinessID']; ?>" <?php if($business['BusinessID'] == $_SESSION['business'] ){ echo 'selected="selected"'; } ?> ><?php echo ucfirst($business['BusinessName']); ?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="busniss-search last">
								<div id="locationcon" class="locationcon" style="display:none;">
									<div id="location1"></div>				
								</div>
								<div id="nolocation" class="nolocation" style="display:none;">
									<div id="noinlocation">No location found.</div>	
								</div>
							</div>
							<div class="search-btn">
								<!-- input type="submit" class="submit-btn clicktosearchreferral" id="getclient" value="Search" -->
								<button type="submit" id="getclient" class="clicktosearchreferral" title="Search">Search</button>
							</div>
						</form>	
					</div>										
					
					<div class="card-body">
						<div class="marketingOuterBlk">
						<?php
						if(!empty($clientdata)) {
							$i = 0;
							$a = 1;
						?>				
							<div class="table-responsive">
								<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th>S. No</th>
											<th><a title="<?php echo $order;?>" href="clientlocationdetail.php?orderby=<?php echo $order;?>&name=firstname"> First Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a></th>
											<th><a title="<?php echo $order;?>" href="clientlocationdetail.php?orderby=<?php echo $order;?>&name=lastname">Last Name  <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a></th>
											<th class="span4 srtHeadEdit srtHeadBorder emailheader">Email</th>
											<th class="span4 srtHeadEdit srtHeadBorder phoneheader">PhoneNumber</th>
										</tr>
									</thead>
									<tbody class="changeonsearch">
									<?php 
										foreach($clientdata as $cldata){
											if($i%2==0){
												$bgdata = "bgnone";	
											} else { 
												$bgdata = "bgdata";
											}
									?>
											<tr class="<?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>">
												<td><?php echo $a;?></td>
												<td><?php echo ucfirst($cldata['FirstName']);?></td>
												<td class="text-align"><?php echo $cldata['LastName']?></td>
												<td class="text-align"><?php echo $cldata['Email'];?></td>
												<td class="text-align"><?php echo $cldata['PhoneNumber'];?></td>
											</tr>
											<?php 
											$i++; 
											$a++;
										} 
									?>
									</tbody>
								</table>
							</div>
							<?php
							echo $clientDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
							?>
							<?php
						} else { ?>
							<div class="table-responsive">
								<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th>S. No</th>
											<th><a title="<?php echo $order;?>" href="clientlocationdetail.php?orderby=<?php echo $order;?>&name=firstname"> First Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a></th>
											<th><a title="<?php echo $order;?>" href="clientlocationdetail.php?orderby=<?php echo $order;?>&name=lastname">Last Name  <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>"></a></th>
											<th class="span4 srtHeadEdit srtHeadBorder emailheader">Email</th>
											<th class="span4 srtHeadEdit srtHeadBorder phoneheader">PhoneNumber</th>
										</tr>
									</thead>
									<tbody class="changeonsearch">
										<tr style='text-align:center;color:#c0c0c0'>
											<td colspan=5>No record found!!</td>
										</tr>
									</tbody>
								</table>
							</div>
							<?php
						} ?>
						
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
