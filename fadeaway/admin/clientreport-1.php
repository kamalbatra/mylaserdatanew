<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	if( !in_array(13,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$BTable = "tbl_business";
	$BCondition = "order by BusinessID desc";
	$BData = $empInfo->selectTableRows($BTable,$BCondition);
	
?>	
<!--script type="text/javascript" src="../js/datepicker/jquery.js"></script>   
<script type="text/javascript" src="../js/datepicker/datepicker.js"></script>
<script type="text/javascript" src="../js/datepicker/eye.js"></script-->   
<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.js"></script>
<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.plus.js"></script>
<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.picker.js"></script>
<script>
	//var jQuery = jQuery.noConflict();
	$(document).ready(function() {
		$('#clientReportBtn').click(function(){
			$('#lodingMsgReport').show(); 
			var str = $("#clientReportForm" ).serialize();
			$.ajax({
				type: "POST",
				url: "ajax_clientReport-1.php",
				data: str,
				cache: false,
				success: function(result) {
					if(result ==0) {
						$('#lodingMsgReport').hide();
						$("#clientReportResult").css('padding-top','13px');
						$("#clientReportResult").css('text-align','center');
					    $("#clientReportResult").html("No record found.");
					} else {
						$('#lodingMsgReport').hide(); 
					    $("#clientReportResult").html(result);
					}
				}
			});
		});
		/*
		$('.inputDate').DatePicker({
			format:'m/d/Y',
			date: new Date(),
			current: new Date(),
			starts: 1,
			position: 'right',
			onBeforeShow: function() {
				$('#inputDate').DatePickerSetDate($('#inputDate').val(), true);
			},
			onChange: function(formated, dates) {
				$('#inputDate').val(formated);
				if ($('#closeOnSelect input').attr('checked')) {
					$('#inputDate').DatePickerHide();
				}
			}
		});
		$('.inputEnddate').DatePicker({
			format:'m/d/Y',
			date: $('#inputEnddate').val(),
			current: $('#inputEnddate').val(),
			starts: 1,
			position: 'right',
			onBeforeShow: function() {
				$('#inputEnddate').DatePickerSetDate($('#inputEnddate').val(), true);
			},
			onChange: function(formated, dates) {
				$('#inputEnddate').val(formated);
				if ($('#closeOnSelectEnd input').attr('checked')) {
					$('#inputEnddate').DatePickerHide();
				}
			}
		});*/
		$('ul#pageUl li a').live('click', function() {
			//$('ul#pageUl li a').click(function() {
			$(".flash").show();
            $(".flash").fadeIn(400)
			var pageId = $(this).attr('id');
			var liId = $(this).attr('liId');
			//var liId = pageId+ '_no';
			var startDate = $("#inputDate").val();
			var endtDate = $("#inputEnddate").val();
			var select_business = $("#select_business").val();
			var dataString = 'pageId='+ pageId +'&startDate='+startDate+'&endtDate='+endtDate+'&select_business='+select_business;
			$.ajax({
				type: "GET",
				url: "ajax_clientReport-1.php",
				data: dataString,
				cache: false,
				success: function(result){
					$("#clientReportResult").html(result);
					$(".flash").hide();
					$(".link a").css('background-color','#fff') ;
					$("#"+liId+" a").css('background-color','#11719F') ;
					$("#"+liId+" a").css('color','#fff') ;
					$("#pageData").html(result);
				}
			});
		});
		$('ul#pageComing li a').live('click', function() {
			//$('ul#pageUl li a').click(function() {		
			$(".flash").show();
            $(".flash").fadeIn(400)
			var pageId = $(this).attr('id');
			var liId = $(this).attr('liId');
			//var liId = pageId+ '_no';
			var startDate = $("#inputDate").val();
			var endtDate = $("#inputEnddate").val();
			var dataString = 'pageIdComing='+ pageId +'&startDate='+startDate+'&endtDate='+endtDate;
			$.ajax({
				type: "GET",
				url: "ajax_clientReport-1.php",
				data: dataString,
				cache: false,
				success: function(result){
					$("#clientReportResult").html(result);
					$(".flash").hide();
					$(".link a").css('background-color','#fff') ;
					$("#"+liId+" a").css('background-color','#11719F') ;
					$("#"+liId+" a").css('color','#fff') ;
					$("#pageData").html(result);
				}
			});
		});
	});
</script>
<script type="text/javascript">
	$(function() {
		$('#inputDate').calendarsPicker({format:'Y/m/d'});
		$('#inputEnddate').calendarsPicker({format:'Y/m/d'});
	});
</script>
<script>
	$(function() {
		$('#notcomming').change(function() {
			$('#notcommingAftr_Few').removeAttr('checked');
			$('#days').hide();
			$('#endDate').show();
			$("#inputDate").removeAttr("disabled"); 
		});
		$('#notcommingAftr_Few').change(function() {
			$('#notcomming').removeAttr('checked');
			$('#endDate').hide();
			$('#days').show();
			$("#inputDate").attr("disabled", "disabled"); 
			if(!$(this).is(':checked')) {
				$('#days').hide();
				$('#endDate').show();
				$("#inputDate").attr("disabled", "disabled"); 
				$("#inputDate").removeAttr("disabled"); 
			} 
		});
	});
</script>
<script>
	function chechError(e) {
		if(e.keyCode==9) {
			return true;
		}
		return false;
	}
</script>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHeadReport" style="width:55%">Client Report</h1>
	<?php
		//if($_SESSION['Usertype']=="Admin" && !isset($_SESSION["loginuser"])) {
		//if($_SESSION['Usertype']=="Admin") { ?>
			<div class="addNewReport" style="margin-right:20px"><a class="empLinks" href="revenuesreport"class="submit-btn"> Revenue Report</a></div>		
			<div class="addNewReport" style="width:20%"><a class="empLinks" href="marketing" class="submit-btn"> Marketing Report</a></div>
	<?php 
	//	} ?>
	</div>	<!--heading-container-->
	<div class="user-entry-block">
		<div class="row">
			<form id="clientReportForm" name="clientReportForm" method="post">
				<div class="span6">
					<label id="" class="user-name" style="text-transform:none">Start date: (MM/DD/YYYY)</label>
					<?php 
					$starDate =  date('m/d/Y');
					$newStarDate = date('m/d/Y',strtotime($starDate . "-30 days"));
					?>
				    <input class="inputDate text-input-field" id="inputDate" name="inputDate" value="<?php echo $newStarDate; ?>" onkeypress="return chechError(event);" />
					<label id="closeOnSelect" style="display:none;"><input type="checkbox" checked /> </label>
				</div>
				<div class="span6 last" id="endDate">
					<label id="" class="user-name" style="text-transform:none">End date: (MM/DD/YYYY)</label>
				    <input class="inputEnddate text-input-field" id="inputEnddate" name="inputEnddate" value="<?php echo date('m/d/Y'); ?>" onkeypress="return chechError(event);" />
					<label id="closeOnSelectEnd" style="display:none;"><input type="checkbox" checked /> </label>
				</div>
				<div class="span6 last" id="days" style="display:none">
					<label id="" class="user-name">Please select days:</label>
				    <select class="text-input-field select-option" name="leftdays">
						<option value="60">Last 60 days</option>
						<option value="90">Last 90 days</option>
						<option value="120"> Last 120 days</option>
						<option value="150">Last 150 days</option>
				    </select>	
				</div>
				<div class="span6 notcoming-span">						
				     <input  type="checkBox" class="largerCheckbox" name="notcomming" id="notcomming" value="Yes" />
				     <label id="Label1" class="user-name">Search for clients who completed consent forms but were never treated.  </label>
					<!--Please select to see the clients which didn't visit within the above specified date range after taking few treatments.
					Please select to see the clients which didn't visit for treatement within the above specified date range after filling consent form.-->
				</div>				
				<!-- few treatments-->
				<div class="span6 last notcoming-span">						
				     <input  type="checkBox" class="largerCheckbox" name="notcommingAftr_Few" id="notcommingAftr_Few" value="Yes" />
				     <label id="Label1" class="user-name">Please select to see clients with prior treatments who haven't been treated within the past number of days.</label>					
				</div>		
				<?php  
						if(isset($_SESSION["loginuser"]) && $_SESSION["loginuser"]=="sitesuperadmin" && $_SESSION["BusinessID"]==0) {
					?>
				<div class="span6">
					<label for="username" class="user-name">Select Business:</label>
					<select class="select-option" name="select_business" id="select_business" >
						<option value="0">All Business</option>
						<?php
							foreach($BData as $Business){  ?> 
								<option value="<?php echo $Business['BusinessID']; ?>" ><?php echo $Business["BusinessName"]; ?></option>
						<?php } ?>
					</select>
				</div>		
				<?php } else { ?>
					<input type="hidden" name="select_business" id="select_business" value="<?php echo $_SESSION['BusinessID']; ?>" >
				<?php } ?>
				<div class="span6">										
				    <input type="button" class="submit-btn" id="clientReportBtn" value="Submit" />
				     <span id="lodingMsgReport"><img src="<?php echo $domain; ?>/images/loading.gif" alt="loading...."/></span>					
				</div>
			</form>	
		</div><!--@row-->	
					<div class="tablebushead">	
	<div class="tablebusinner">	
	<div id="clientReportResult"></div>
	</div><!--@user-entry-block-->
	</div><!--@user-entry-block-->
	</div><!--@user-entry-block-->
</div><!--@form-container-->
</div><!-- container-->
<script>
	clintReportLoad();
	function clintReportLoad() {
		var startDate = $("#inputDate").val();
		var endtDate = $("#inputEnddate").val();
		var select_business = $("#select_business").val();
		var pageId=1;
		var dataString = 'pageIdComing='+ pageId +'&startDate='+startDate+'&endtDate='+endtDate+'&select_business='+select_business;
		$('#lodingMsgReport').show(); 
		$.ajax({
			type: "GET",
			url: "ajax_clientReport-1.php",
			data: dataString,
			cache: false,
			success: function(html) {
				setTimeout(function() { 
					$('#lodingMsgReport').hide(); // function show popup 
				}, 2000); // .5 second
				$("#clientReportResult").html(html).show();
			}
		});
		return false;
	} 
</script>			
<?php
	include('admin_includes/footer.php');
?>

