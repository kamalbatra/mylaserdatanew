<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	if( !in_array(13,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$BTable = "tbl_business";
	$BCondition = "order by BusinessID desc";
	$BData = $empInfo->selectTableRows($BTable,$BCondition);
	
?>	
<!--script type="text/javascript" src="../js/datepicker/jquery.js"></script>   
<script type="text/javascript" src="../js/datepicker/datepicker.js"></script>
<script type="text/javascript" src="../js/datepicker/eye.js"></script-->   
<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.js"></script>
<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.plus.js"></script>
<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.picker.js"></script>
<script>
	//var jQuery = jQuery.noConflict();
	$(document).ready(function() {
		$('#clientReportBtn').click(function(){
			$('#lodingMsgReport').show(); 
			var str = $("#clientReportForm" ).serialize();
			$.ajax({
				type: "POST",
				url: "ajax_clientReport.php",
				data: str,
				cache: false,
				success: function(result) {
					if(result ==0) {
						$('#lodingMsgReport').hide();
						$("#clientReportResult").css('padding-top','13px');
						$("#clientReportResult").css('text-align','center');
					    $("#clientReportResult").html("No record found.");
					} else {
						$('#lodingMsgReport').hide(); 
					    $("#clientReportResult").html(result);
					}
				}
			});
		});
					$("#inputDate").datepicker({
						dateFormat:'mm/dd/yy',
					});
					$("#inputEnddate").datepicker({
						dateFormat:'mm/dd/yy',
					});
		/*			
		$('.inputDate').DatePicker({
			format:'m/d/Y',
			date: new Date(),
			current: new Date(),
			starts: 1,
			position: 'right',
			onBeforeShow: function() {
				$('#inputDate').DatePickerSetDate($('#inputDate').val(), true);
			},
			onChange: function(formated, dates) {
				$('#inputDate').val(formated);
				if ($('#closeOnSelect input').attr('checked')) {
					$('#inputDate').DatePickerHide();
				}
			}
		});
		$('.inputEnddate').DatePicker({
			format:'m/d/Y',
			date: $('#inputEnddate').val(),
			current: $('#inputEnddate').val(),
			starts: 1,
			position: 'right',
			onBeforeShow: function() {
				$('#inputEnddate').DatePickerSetDate($('#inputEnddate').val(), true);
			},
			onChange: function(formated, dates) {
				$('#inputEnddate').val(formated);
				if ($('#closeOnSelectEnd input').attr('checked')) {
					$('#inputEnddate').DatePickerHide();
				}
			}
		});*/
		$('ul#pageUl li a').live('click', function() {
			//$('ul#pageUl li a').click(function() {
			$(".flash").show();
            $(".flash").fadeIn(400)
			var pageId = $(this).attr('id');
			var liId = $(this).attr('liId');
			//var liId = pageId+ '_no';
			var startDate = $("#inputDate").val();
			var endtDate = $("#inputEnddate").val();
			var select_business = $("#select_business").val();
			var dataString = 'pageId='+ pageId +'&startDate='+startDate+'&endtDate='+endtDate+'&select_business='+select_business;
			$.ajax({
				type: "GET",
				url: "ajax_clientReport.php",
				data: dataString,
				cache: false,
				success: function(result){
					$("#clientReportResult").html(result);
					$(".flash").hide();
					$(".link a").css('background-color','#fff') ;
					$("#"+liId+" a").css('background-color','#11719F') ;
					$("#"+liId+" a").css('color','#fff') ;
					$("#pageData").html(result);
				}
			});
		});
		$('ul#pageComing li a').live('click', function() {
			//$('ul#pageUl li a').click(function() {		
			$(".flash").show();
            $(".flash").fadeIn(400)
			var pageId = $(this).attr('id');
			var liId = $(this).attr('liId');
			//var liId = pageId+ '_no';
			var startDate = $("#inputDate").val();
			var endtDate = $("#inputEnddate").val();
			var dataString = 'pageIdComing='+ pageId +'&startDate='+startDate+'&endtDate='+endtDate;
			$.ajax({
				type: "GET",
				url: "ajax_clientReport.php",
				data: dataString,
				cache: false,
				success: function(result){
					$("#clientReportResult").html(result);
					$(".flash").hide();
					$(".link a").css('background-color','#fff') ;
					$("#"+liId+" a").css('background-color','#11719F') ;
					$("#"+liId+" a").css('color','#fff') ;
					$("#pageData").html(result);
				}
			});
		});
	});
</script>
<script type="text/javascript">
	$(function() {
		/*
		$('#inputDate').calendarsPicker({format:'Y/m/d'});
		$('#inputEnddate').calendarsPicker({format:'Y/m/d'}); */
	});
</script>
<script>
	$(function() {
		$('#notcomming').change(function() {
			$('#notcommingAftr_Few').removeAttr('checked');
			$('#days').hide();
			$('#endDate').show();
			$("#inputDate").removeAttr("disabled"); 
		});
		$('#notcommingAftr_Few').change(function() {
			$('#notcomming').removeAttr('checked');
			$('#endDate').hide();
			$('#days').show();
			$("#inputDate").attr("disabled", "disabled"); 
			if(!$(this).is(':checked')) {
				$('#days').hide();
				$('#endDate').show();
				$("#inputDate").attr("disabled", "disabled"); 
				$("#inputDate").removeAttr("disabled"); 
			} 
		});
	});
</script>
<script>
	function chechError(e) {
		if(e.keyCode==9) {
			return true;
		}
		return false;
	}
</script>
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Client Report</h1>
				</div>
								
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="revenuesreport"class="submit-btn"><button class="addnewbtn">Revenue Report</button></a>
							<a class="empLinks" href="marketing" class="submit-btn"><button class="addnewbtn">Marketing Report</button></a>
						</div>
					</div>					
					<div class="formcontentblock-ld">
						<form id="clientReportForm" name="clientReportForm" method="post">
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
										<?php 
											$starDate =  date('m/d/Y');
											$newStarDate = date('m/d/Y',strtotime($starDate . "-30 days"));
										?>
											<label></label>
											<input class="inputDate text-input-field" id="inputDate" name="inputDate" value="<?php echo $newStarDate; ?>" onkeypress="return chechError(event);" />
										</div>
									</div>
								</div>
								<div class="half" id="endDate">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label></label>
											<input class="inputEnddate text-input-field" id="inputEnddate" name="inputEnddate" value="<?php echo $starDate; ?>" onkeypress="return chechError(event);" />
										</div>
									</div>
								</div>

								<div class="half busniss-search last" id="days" style="display:none">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="" class="user-name">Please select days:</label>
											<select class="text-input-field select-option" name="leftdays">
												<option value="60">Last 60 days</option>
												<option value="90">Last 90 days</option>
												<option value="120"> Last 120 days</option>
												<option value="150">Last 150 days</option>
											</select>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
												<div class="radioblocksBtns">
													<div class="radioBtn ckeckboxres">		
														<input  type="checkBox" class="largerCheckbox" name="notcomming" id="notcomming" value="Yes" />
														<label for="notcomming" class="user-name">Search for clients who completed consent forms but were never treated.  </label>
													</div>
												</div>
													
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
												<div class="radioblocksBtns">
													<div class="radioBtn ckeckboxres">	
														<input  type="checkBox" class="largerCheckbox" name="notcommingAftr_Few" id="notcommingAftr_Few" value="Yes" />
														<label for="notcommingAftr_Few" class="user-name">Please select to see clients with prior treatments who haven't been treated within the past number of days.</label>
													</div>
												</div>
										</div>
									</div>
								</div>
							</div>
							<?php  
							if(isset($_SESSION["loginuser"]) && $_SESSION["loginuser"]=="sitesuperadmin" && $_SESSION["BusinessID"]==0) {
							?>
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label for="username" class="user-name">Select Business:</label>
											<select class="select-option" name="select_business" id="select_business" >
												<option value="0">All Business</option>
												<?php
													foreach($BData as $Business){  ?> 
														<option value="<?php echo $Business['BusinessID']; ?>" ><?php echo $Business["BusinessName"]; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>		
							</div>		
							<?php  } else { ?>
								<input type="hidden" name="select_business" id="select_business" value="<?php echo $_SESSION['BusinessID']; ?>" >
							<?php 
							} ?>

							<div class="form-row-ld">
								<div class="backNextbtn">
									<button type="button" id="clientReportBtn" value="submit" class="submit-btn clicktosearchreferral" style="float:left;">Submit</button>
								</div>
							</div>
						</form>
					</div>
					<div class="card-body">
						<div id="clientReportResult"></div>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
<script>
	clintReportLoad();
	function clintReportLoad() {
		var startDate = $("#inputDate").val();
		var endtDate = $("#inputEnddate").val();
		var select_business = $("#select_business").val();
		var pageId=1;
		var dataString = 'pageIdComing='+ pageId +'&startDate='+startDate+'&endtDate='+endtDate+'&select_business='+select_business;
		$('#lodingMsgReport').show(); 
		$.ajax({
			type: "GET",
			url: "ajax_clientReport.php",
			data: dataString,
			cache: false,
			success: function(html) {
				setTimeout(function() { 
					$('#lodingMsgReport').hide(); // function show popup 
				}, 2000); // .5 second
				$("#clientReportResult").html(html).show();
			}
		});
		return false;
	} 
</script>
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
