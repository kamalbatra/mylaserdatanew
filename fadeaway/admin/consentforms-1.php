<?php
//session_start();
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$consentForms = new dbFunctions();
if( !in_array(4,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
$table = "tbl_clients";
$EmpLocation = $_SESSION['Location'];
$tableUpdate = "tbl_consent_form";
/*** numrows**/
 $adjacents = 3;
 $reload="consentforms";
 if($_SESSION['Usertype']!="Admin"){
	 $conditionNumRows = " where Location =".$EmpLocation." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC";
	 }
	else{
	$conditionNumRows  =  " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY ClientID ASC ";
	} 
 $totalNumRows	= $consentForms->totalNumRows($tableUpdate,$conditionNumRows);
$total_pages = $totalNumRows;
//$page="";
 if(isset($_GET['page'])){
	 $page=$_GET['page'];
	}
	else{
		$page="";
		}
 $limit =10;                                  //how many items to show per page
    if($page)
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0; 
/*** numrow**/
if($_SESSION['Usertype']!="Admin"   || (isset($_GET['ClientID']) && base64_decode($_GET['user']) !="Admin")){
	if(isset($_GET['ClientID']) && $_GET['ClientID'] !=""){
       $condition = " where Location =".$EmpLocation." and ClientID =".base64_decode($_GET['ClientID'])." ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT ".$start.", ".$limit."";
    }else{
		 $condition = " where Location =".$EmpLocation." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT ".$start.", ".$limit."";
	}
}
if($_SESSION['Usertype']=="Admin"   || (isset($_GET['ClientID']) && base64_decode($_GET['user']) =="Admin")){
	if(isset($_GET['ClientID']) && $_GET['ClientID'] !=""){
	     $condition = "where ClientID =".base64_decode($_GET['ClientID'])." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT  ".$start.", ".$limit."";
    }else{
		  $condition = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT  ".$start.", ".$limit."";
	}
}
$cols="*";
$consentdata = $consentForms->selectTableRows($tableUpdate,$condition);

?>
<script type="text/javascript">
$(function(){
		$(".searchclient").keyup(function() 
		{ 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid  + '&Location='+$("#location").val();
		if(searchid!='')
		{
			$.ajax({
			type: "POST",
			url: "ajax_ConsentFormClientSearch.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
			   $("#resultClientSerch").html(html).show();
			}
			});
		}return false;    
		});
		jQuery("#result").live("click",function(e){ 
			var $clicked = $(e.target);
			var $name = $clicked.find('.name').html();
			//alert(name)
			var decoded = $("<div/>").html($name).text();
			$('#searchid').val(decoded);
			$('#fname').val(decoded);
			//alert(decoded);
		});
		jQuery(document).live("click", function(e) { 
			var $clicked = $(e.target);
			if (! $clicked.hasClass("search")){
			jQuery("#result").fadeOut(); 
			}
		});
		$('#searchid').click(function(){
			jQuery("#result").fadeIn();
		});
});
</script>
<div class="form-container" id="top1">
	<h1 class="heading">Consent form review</h1>
	<div class="user-entry-block">
		<div class="row">
				
				 <div class="span6 last user-block-span user-block-last">
					 <label id="Label1" class="user-name">First name, last name or phone number:</label>	
					  <input type="hidden" name="location" id="location" class="text-input-field" id="pricing_amt" value="<?=$_SESSION['Location']?>"/>				 
				    <input type="text" class="searchclient text-input-field" id="searchid" />
		          <div id="resultClientSerch" style="font-family:Verdana,Geneva,Tahoma,sans-serif"></div>
		         </div> 
		     </div>
		     
	<div class="tablebushead">	
	<div class="tablebusinner">	
	<div class="row sortingHead">	
	    <div class="span3 srtHeadEditEmp srtHeadBorder" style="Width:7%"> S.No.</div>
	    <div class="span3 srtHead srtHeadBorder" style="Width:16%"> First Name</div>
	    <div class="span3 srtHead srtHeadBorder" style="Width:15%"> Last Name</div>
	    <div class="span3 srtHeadloc srtHeadBorder"> Office Location</div>
	    <div class="span3 srtHeadloc srtHeadBorder" style="width:27%">Review Status</div>
	    <div class="span3 srtHeadEditEmp srtHeadBorder" style="width:7%;">View</div>
	    <div class="span3 srtHeadEditEmp srtHeadBorder" style="border:none; width:6%;">Print</div>
	</div>
<?php if(!empty($consentdata)){
	$i = 1;
foreach($consentdata as $clients){
	$clientcondition = " WHERE ClientID = $clients[ClientID] ";
	$clientcols = "AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName";
	$clientsname = $consentForms->selectTableSingleRow("tbl_clients",$clientcondition, $clientcols);

 if($clients['ConsentID'] !=0 && $clients['ClientID'] !=0 && $clientsname['FirstName'] !="" && $clientsname['LastName'] !="" && $clients['Location'] !="" ){	
	if($i%2==0){
			$bgdata = "bgdata";
		}
		else{
			$bgdata = "bgnone";
		}
?>
   <div class="row treatment <?php echo $bgdata;?>">	
	<!--echo '<a href="client.php?clientid='.$clients['ClientID'].'">'.$clients['ClientID']." ".$clients['FirstName']." ".$clients['LastName']."<br/>".'</a>';-->
	<div class="span3 srtHeadEditEmp srtcontent" style="Width:7%;text-align:center"><label class="user-name"><?php echo $i;//$clients['ConsentID'];?></label></div>
	<div class="span6 srtHead srtcontent" style="Width:16%"><label  class="user-name"><a href ="client.php?clientid=<?php echo $clients['ClientID'];?>">	<?php echo ucfirst($clientsname['FirstName']); ?> </a></label></div>
	<div class="span6 srtHead srtcontent" style="Width:15%"><label  class="user-name"><a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><?php echo $clientsname['LastName'];?></a></label></div>	
	<div class="span6 srtHeadloc srtcontent"><label  class="user-name"><a href ="client.php?clientid=<?php echo $clients['ClientID'];?>">
	           <?php  if($clients['Location']!=""){
					     $clentLocation = explode(",",$clients['Location']);					        
					     for($j=0 ; $j<count($clentLocation);$j++){								 
							$tbl_manage_location	= "tbl_manage_location";
							$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
							$cols="*";
							$locationData	= $consentForms->selectTableSingleRow($tbl_manage_location,$condition1);
							  echo $locationData['LocationName'];							  
							 if($j !=count($clentLocation)-1){
							  echo ",&nbsp;";
						     }							
						  }//for loop close					 
				     }					 				 
				?>
	      </a></label></div>		 
	 <div class="span6 cMain ">
			<label  class="user-name">
				<?php if($clients['TechnicianReview']==""){?>
			<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="pandingReview">Pending for technician review </span></a>
			<?php }
			else if($clients['TechnicianReview'] !="" && $clients['MedicalDirectorNotes'] ==""){?>
			<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="pandingReviewDoctor">Pending for doctor review </span></a>
			<?php }					
			 else {?>
				<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="compleReview">Completed</span></a>
			<?php } ?>			
			</label>
	 </div>	
	 <div class="span3 srtHeadEditEmp srtcontent" style="width:6%;">
		 <a href ="client.php?clientid=<?php echo $clients['ClientID'];?>">
	      <img width="30px" title="View consent form" src="<?php echo $domain; ?>/images/Images-icon.png">
	      </a>												
	</div>
	 <div class="span3 srtHeadEditEmp srtcontent" style="width:6%;">
		 <a href="clientconsentform?ClientID=<?php echo base64_encode($clients['ClientID'])?>&action=clientdetails">
	      <img width="25px" title="Edit consent form" src="<?php echo $domain; ?>/images/printicon.png">
	      </a>												
	</div>	
	</div><!--End @row-block-->
<?php   
  $i++; 
   }
 } //foreach end
if(isset($_POST) && $_POST != NULL  )
{
	if($consentForms->update_user($tableUpdate,$_POST))
	{
		echo "<span style='color:green;'>Information updated successfully.</span>";
	}
}
?>
<?php echo $consentForms->paginateShow($page,$total_pages,$limit,$adjacents,$reload)?>
<?php }else echo "<div class='not-found-data'>No consent form review found.</div>"; ?>
<?php
 /* Setup page vars for display. */
   /* if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1
   */
    /*
        Now we apply our rules and draw the pagination object.
        We're actually saving the code to a variable in case we want to draw it more than once.
    */
    
    /*
    $pagination = "";
    if($lastpage > 1)
    {  
        $pagination .= "<div class=\"pagination\">";
        //previous button
        if ($page > 1)
            $pagination.= "<a href=\"consentforms.php?page=$prev\">« previous</a>";
        else
            $pagination.= "<span class=\"disabled\">« previous</span>"; 
       
        //pages
        if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
        {  
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a href=\"consentforms.php?page=$counter\">$counter</a>";                  
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 2))       
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"consentforms.php?page=$counter\">$counter</a>";                  
                }
                $pagination.= "...";
                $pagination.= "<a href=\"consentforms.php?page=$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"consentforms.php?page=$lastpage\">$lastpage</a>";    
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<a href=\"consentforms.php?page=1\">1</a>";
                $pagination.= "<a href=\"consentforms.php?page=2\">2</a>";
                $pagination.= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"consentforms.php?page=$counter\">$counter</a>";                  
                }
                $pagination.= "...";
                $pagination.= "<a href=\"consentforms.php?page=$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"consentforms?page=$lastpage\">$lastpage</a>";    
            }
            //close to end; only hide early pages
            else
            {
                $pagination.= "<a href=\"consentforms.php?page=1\">1</a>";
                $pagination.= "<a href=\"consentforms.php?page=2\">2</a>";
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"consentforms.php?page=$counter\">$counter</a>";                  
                }
            }
        }
       
        //next button
        if ($page < $counter - 1)
            $pagination.= "<a href=\"consentforms.php?page=$next\">next »</a>";
        else
            $pagination.= "<span class=\"disabled\">next »</span>";
        $pagination.= "</div>\n";      
    }
    echo $pagination;
    */
?>
</div><!--End @user-entry-block-->
</div><!--End @form-container-->
</div><!--End @container-->
</div><!--End @container-->
</div><!--End @container-->
<?php
	include('admin_includes/footer.php');
	?>
