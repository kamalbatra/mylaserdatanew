<?php
	//~ error_reporting(0);
	session_start();
	$domain = $_SERVER['DOMAIN'];
	include('admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$consentForms = new dbFunctions();
	if( !in_array(4,$_SESSION["menuPermissions"])){ ?> 
		<script>window.location.replace("dashboard");</script>
	<?php }
	$table = "tbl_clients";
	$EmpLocation = $_SESSION['Location'];
	$tableUpdate = "tbl_consent_form";
	/*** numrows**/
	$adjacents = 3;
	$reload="consentforms";
	if($_SESSION['Usertype']!="Admin"){
		$conditionNumRows = " where Location =".$EmpLocation." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC";
	} else{
		$conditionNumRows  =  " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY ClientID ASC ";
	}
	$totalNumRows	= $consentForms->totalNumRows($tableUpdate,$conditionNumRows);
	$total_pages = $totalNumRows;
	//$page="";
	if(isset($_GET['page'])){
		$page=$_GET['page'];
	} else{
		$page="";
	}
	$limit =10;	//how many items to show per page
    if($page){
		$start = ($page - 1) * $limit;	//first item to display on this page
	} else {
		$start = 0;
	}
	/*** numrow**/
	if($_SESSION['Usertype']!="Admin"   || (isset($_GET['ClientID']) && base64_decode($_GET['user']) !="Admin")){
		if(isset($_GET['ClientID']) && $_GET['ClientID'] !=""){
			$condition = " where Location =".$EmpLocation." and ClientID =".base64_decode($_GET['ClientID'])." ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT ".$start.", ".$limit."";
		}else{
			$condition = " where Location =".$EmpLocation." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT ".$start.", ".$limit."";
		}
	}
	if($_SESSION['Usertype']=="Admin"   || (isset($_GET['ClientID']) && base64_decode($_GET['user']) =="Admin")) {
		if(isset($_GET['ClientID']) && $_GET['ClientID'] !="") {
			$condition = "where ClientID =".base64_decode($_GET['ClientID'])." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT  ".$start.", ".$limit."";
		} else {
			$condition = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT  ".$start.", ".$limit."";
		}
	}
	$cols="*";
	$consentdata = $consentForms->selectTableRows($tableUpdate,$condition);
?>
	<script type="text/javascript">
		$(function(){
			$(".searchclient1").keyup(function()  {
				var searchid = $(this).val();
				var dataString = 'search='+ searchid  + '&Location='+$("#location").val();
				if(searchid!='') {
					$.ajax({
						type: "POST",
						url: "ajax_ConsentFormClientSearch.php",
						data: dataString,
						cache: false,
						success: function(html) {
						   $("#resultClientSerch1").html(html).show();
						}
					});
				} return false;
			});
			jQuery("#result").live("click",function(e){
				var $clicked = $(e.target);
				var $name = $clicked.find('.name').html();
				var decoded = $("<div/>").html($name).text();
				$('#searchid1').val(decoded);
				$('#fname').val(decoded);
			});
			jQuery(document).live("click", function(e) { 
				var $clicked = $(e.target);
				if (! $clicked.hasClass("search")){
					jQuery("#result").fadeOut(); 
				}
			});
			$('#searchid1').click(function(){
				jQuery("#result").fadeIn();
			});
		});
	</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Consent Form Review</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock manageemp">
						<div class="busniss-search searchbussiness last">
							<img src="../img/searchblk.png">
							<input type="hidden" name="location" id="location" class="text-input-field" id="pricing_amt" value="<?=$_SESSION['Location']?>"/>				 
							<input type="text" class="searchclient1 text-input-field" id="searchid1" placeholder="First name, last name or phone number ..."/>
							<div id="resultClientSerch1" class="suggestionbox" style="font-family:Verdana,Geneva,Tahoma,sans-serif"></div>
						</div>
						<!-- div class="search-btn">
							<button>Search</button>
							<button class="addnewbtn"><img src="img/plus.png"> Add New</button>
						</div -->
					</div>		
					<div class="card-body">
					<?php
						if( !empty($consentdata) ) {
							$i = 1;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Office Location</th>
										<th>Review Status</th>
										<th>View</th>
										<th>Print</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $consentdata as $clients ) {
											$clientcondition = " WHERE ClientID = $clients[ClientID] ";
											$clientcols = "AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName";
											$clientsname = $consentForms->selectTableSingleRow("tbl_clients",$clientcondition, $clientcols);
											if($clients['ConsentID'] !=0 && $clients['ClientID'] !=0 && $clientsname['FirstName'] !="" && $clientsname['LastName'] !="" && $clients['Location'] !="" ){
												if( $i%2==0 ) {
													$bgdata = "bgdata";
												} else {
													$bgdata = "bgnone";
												}
									?>
												<tr class="treatment <?php echo $bgdata;?>">
													<td class="span3 srtHeadEditEmp srtcontent"><label class="user-name"><?php echo $i;?></label></td>
													<td class="span6 srtHead srtcontent"><label class="user-name"><a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><?php echo ucfirst($clientsname['FirstName']); ?></a></label></td>
													<td class="span6 srtHead srtcontent"><label class="user-name"><a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><?php echo $clientsname['LastName'];?></a></label></td>
													<td class="span6 srtHeadloc srtcontent"><label class="user-name">
														<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>">
														<?php
														if($clients['Location']!=""){
															$clentLocation = explode(",",$clients['Location']);
															for( $j=0 ; $j<count($clentLocation);$j++ ) {
																$tbl_manage_location	= "tbl_manage_location";
																$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
																$cols="*";
																$locationData	= $consentForms->selectTableSingleRow($tbl_manage_location,$condition1);
																echo $locationData['LocationName'];
																if($j !=count($clentLocation)-1){
																	echo ",&nbsp;";
																}
															}//for loop close
														}
														?>
														</a></label>
													</td>
													<td class="span6 cMain ">
														<label class="user-name">
														<?php 
															if( $clients['TechnicianReview']=="" ) { ?>
																<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="pandingReview">Pending for technician review </span></a>
															<?php } else if( $clients['TechnicianReview'] !="" && $clients['MedicalDirectorNotes'] =="" ) { ?>
																<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="pandingReviewDoctor">Pending for doctor review </span></a>
															<?php } else { ?>
																<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="compleReview">Completed</span></a>
															<?php }
														?>
														</label>
													</td>
													<td class="span3 srtHeadEditEmp srtcontent">
														<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>">
															<img width="30px" title="View consent form" src="<?php echo $domain; ?>/img/eye-img.jpg">
														</a>
													</td>
													<td class="span3 srtHeadEditEmp srtcontent">
														<a href="clientconsentform?ClientID=<?php echo base64_encode($clients['ClientID'])?>&action=clientdetails">
															<img width="25px" title="Edit consent form" src="<?php echo $domain; ?>/img/print-img.jpg">
														</a>
													</td>
												</tr><!--End @row-block-->
												<?php
												$i++;
											}
										} //foreach end
										if(isset($_POST) && $_POST != NULL  )  {
											if( $consentForms->update_user($tableUpdate,$_POST) ) { 
												echo "<span style='color:green;'>Information updated successfully.</span>"; 
											}
										}
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $consentForms->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No consent form review found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
