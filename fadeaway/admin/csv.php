<?php
	ob_start();
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$clientReport = new dbFunctions();
	header("Content-Disposition: attachment; filename=client-report.csv");
	header("Content-type: text/csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	$newStartDate = $_GET['startDate'];
	$newEndtDate = $_GET['endDate']; 
	$table1= "tbl_consent_form";
	$table2= "tbl_clients";
	$join="INNER";
	$cols = "tbl_clients.ClientID,AES_DECRYPT(tbl_clients.FirstName, '".SALT."') as FirstName,AES_DECRYPT(tbl_clients.LastName, '".SALT."') as LastName ,tbl_clients.Location,AES_DECRYPT(tbl_clients.PhoneNumber, '".SALT."') as PhoneNumber,AES_DECRYPT(tbl_clients.Email, '".SALT."') as Email";
	$conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID where tbl_clients.BusinessID = $_SESSION[BusinessID] and tbl_clients.ClientID NOT IN  (SELECT ClientID FROM tbl_treatment_log ) and tbl_clients.TimeStamp between '".$newStartDate."' and '".$newEndtDate."' ORDER BY tbl_clients.ClientID  DESC";
	$clientdata	= $clientReport->selectTableJoin($table1,$table2,$join,$conditionNumRows,$cols);
	if($clientdata !=NULL) {
		$content = '';
		$title = '';
		foreach($clientdata as $cdata) {
			$cdata['ClientID'];
			$tableTreat	= "tbl_treatment_log";
			$cols="ClientID";
			$condition = " where ClientID =".$cdata['ClientID']." ";	            
			$clientdataExist = $clientReport->selectNumRows($tableTreat,$condition,$cols);
			$content .= stripslashes($cdata['ClientID']). ',';
			$content .= stripslashes(ucfirst($cdata['FirstName'])). ',';
			$content .= stripslashes($cdata['LastName']). ',';
			$content .= stripslashes($cdata['Email']). ',';
			$content .= stripslashes($cdata['PhoneNumber']). ',';
			$content .= "\n";
		}		
		$title .= "ClientID,First name,Last name, Email ,PhoneNumber,"."\n";
		echo $title;
		echo $content;
	}	
?>
