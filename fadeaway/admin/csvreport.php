<?php
	error_reporting(0);
	//$domain=$_SERVER['DOMAINPATH'];
	//include ($domain."/includes/config.php");
	//include ($domain."/includes/dbFunctions.php");
	include ("../includes/config.php");
	include ("../includes/dbFunctions.php");
	$clientReport = new dbFunctions();
	//print_r($_GET);
	/* Please select to see the clients which didn t visit within the above specified date range after taking few treatments*/
	/* Please select to see the clients which didn t visit within the above specified date range after taking few treatments*/ 
	if(isset($_GET['leftdays'])) {
		header("Content-Disposition: attachment; filename=report-of-breaking-client.csv");
		header("Content-type: text/csv");
		header("Pragma: no-cache");
		header("Expires: 0");			
		/* $newStartDate = $_GET['startDate'];
		$newEndtDate = $_GET['endDate'];*/
		$currenrDate =  date('Y-m-d H:i:s');
		$lessDate  = date('Y-m-d H:i:s',strtotime($currenrDate . "-30 days"));
		$table1= "tbl_treatment_log";
		$table2= "tbl_clients";
		$join="INNER";
		$cols = "tbl_treatment_log.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') as Client_First_Name, AES_DECRYPT(tbl_clients.LastName, '".SALT."') as Client_Last_Name, MAX(tbl_treatment_log.Date_of_treatment) as DateOfTreatment ,tbl_treatment_log.SessionNumber, count( * ) AS number, MAX(tbl_treatment_log.SessionNumber) as lastSession,AES_DECRYPT(tbl_clients.Email, '".SALT."') as Email,AES_DECRYPT(tbl_clients.PhoneNumber, '".SALT."') as PhoneNumber";
		//$condition1 = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_treatment_log.Date_of_treatment NOT between '".$newStartDate."' and '".$newEndtDate."'   and tbl_clients.TimeStamp < '".$newEndtDate."' GROUP BY tbl_treatment_log.ClientID   HAVING max(tbl_treatment_log.Date_of_treatment) < '".$lessDate."' "; 
		$condition1 = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] and tbl_clients.TimeStamp < '".$currenrDate."' GROUP BY tbl_treatment_log.ClientID   having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=".$_GET['leftdays']." and DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) <=150"; 
		$clientReportData = $clientReport->selectTableJoin($table1,$table2,$join,$condition1,$cols);
		if($clientReportData !=NULL) {
			$content = '';
			$title = '';			    				   
			foreach($clientReportData as $cdata) {
				 $date = new DateTime($cdata['DateOfTreatment']);
				$date->setTimezone(new DateTimeZone('CST')); // +04
				$treatment=$date->format('Y/m/d'); // 2012-07-15 05:00:00 
				date_default_timezone_set(timezone_name_from_abbr("UTC"));
				
				$content .= stripslashes($cdata['ClientID']). ',';
				$content .= stripslashes(ucfirst($cdata['Client_First_Name'])). ',';
				$content .= stripslashes($cdata['Client_Last_Name']). ',';
				$content .= stripslashes($cdata['Email']). ',';
				$content .= stripslashes($cdata['PhoneNumber']). ',';
				//$content .= stripslashes($cdata['DateOfTreatment']). ',';
				$content .= stripslashes($treatment). ',';
				$content .= stripslashes($cdata['lastSession']). ',';
				$content .= stripslashes($cdata['number']). ',';						
				$content .= "\n";
			}
			$title .= "ClientID,First name,Last name,Email,Phone Number,Date of treatment ,Last session no,Total session,"."\n";
			echo $title;
			echo $content;
		}
	}// if isset
	/************ All client who have takinng treatment*****************/
	/*****************************/
	/*****************************/		//if(isset($_GET['notcommingAftr_Few']) && $_GET['notcommingAftr_Few']=="NO" ){
	if(isset($_GET['startDate'])  &&  (isset($_GET['endDate']))) {	
		
		header("Content-Disposition: attachment; filename=report-of-client.csv");
		header("Content-type: text/csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$newStartDate = $_GET['startDate'];
		$newEndtDate = $_GET['endDate'];
		$currenrDate =  date('Y-m-d H:i:s');
		$lessDate  = date('Y-m-d H:i:s',strtotime($currenrDate . "-30 days"));
		//$table = "tbl_treatment_log";
		$table1= "tbl_treatment_log as l";
		$table2= "tbl_clients as c";
		$join="INNER";
		
		$cols="c.ClientID ,AES_DECRYPT(c.FirstName, '".SALT."') as Client_First_Name, AES_DECRYPT(c.LastName, '".SALT."') as Client_Last_Name, MAX(l.Date_of_treatment) as DateOfTreatment ,l.SessionNumber, count( * ) AS number, MAX(l.SessionNumber) as lastSession";	
		$condition1 = "c.ClientID = l.ClientID WHERE c.BusinessID = $_SESSION[BusinessID] and l.Date_of_treatment between '".$newStartDate."' and '".$newEndtDate."' GROUP BY c.FirstName ORDER BY MAX( l.SessionNumber ) DESC ";
		$clientReportData= $clientReport->selectTableJoin($table1,$table2,$join,$condition1,$cols);
		if($clientReportData !=NULL) {
			$content = '';
			$title = '';
			foreach($clientReportData as $cdata) {
					 $date = new DateTime($cdata['DateOfTreatment']);
				$date->setTimezone(new DateTimeZone('CST')); // +04
				$treatment=$date->format('Y-m-d H:i:s'); // 2012-07-15 05:00:00 
				date_default_timezone_set(timezone_name_from_abbr("UTC"));
				
				$content .= stripslashes($cdata['ClientID']). ',';
				$content .= stripslashes(ucfirst($cdata['Client_First_Name'])). ',';
				$content .= stripslashes($cdata['Client_Last_Name']). ',';
				//$content .= stripslashes($cdata['DateOfTreatment']). ',';
				$content .= stripslashes($treatment). ',';
				$content .= stripslashes($cdata['lastSession']). ',';
				$content .= stripslashes($cdata['number']). ',';
				$content .= "\n";
			}
			$title .= "ClientID,First name,Last name, Date of treatment ,Last session no,Total session,"."\n";
			echo $title;
			echo $content;
		}
	}// if isset
	
	if(isset($_GET['bid'])  &&  (isset($_GET['loc']))) {	
		
		header("Content-Disposition: attachment; filename=report-of-client.csv");
		header("Content-type: text/csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$table1= "tbl_clients";
		$businessid = $_GET['bid'];
		$locationid = $_GET['loc'];
		if( $locationid == 0 ){
			$location = "AND Location =''" ;
		} else {
			$location = "AND Location =".$locationid;
		}
		$cols = "ClientID, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, Location, AES_DECRYPT(Email, '".SALT."') AS Email, AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber";
		$condition  = "WHERE BusinessID = ".$businessid." ".$location." ORDER BY FirstName ASC";		
		$clientReportData	= $clientReport->selectclientrecord($table1,$cols,$condition);	
		
		if($clientReportData !=NULL) {
			$content = '';
			$title = '';
			foreach($clientReportData as $cdata) {
				$content .= stripslashes(ucfirst($cdata['FirstName'])). ',';
				$content .= stripslashes($cdata['LastName']). ',';
				$content .= stripslashes($cdata['Email']). ',';
				$content .= $cdata['PhoneNumber']. ',';
				$content .= "\n";
			}
			$title .= "First Name, Last Name, Email, Phone Number"."\n";
			echo $title;
			echo $content;
		}
	}
	
?>
