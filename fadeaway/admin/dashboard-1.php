﻿<?php
	$doaminPath = $_SERVER['DOMAINPATH'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header.php');
	setcookie('dashbord',1, time()+60*60*24*30); // 30 days
	$domain = $_SERVER['DOMAIN'];
?> 
	<div class="form-container">
		<?php
		if( $_SESSION["periodtime"]<=3 && $_SESSION["Usertype"]=="Admin" && !isset($_SESSION["loginuser"]))	{ 
		?>		
			<!-- h5 class="warning">Your Subscription plan will be finished in <?php echo $_SESSION["periodtime"]; ?> day(s), please maintain enough balance in your account.</h5 -->
		<?php	
		}
		if( isset($_GET["sub"]) && $_GET["sub"]=="success"){	
		?>
			<h5 class="success">Thank you. You have successfully subscribed. The renewal date of your plan is <?php echo $_SESSION["ExpireDate"]."."; ?></h5>
		<?php
		}
		if( $_GET["msg"] == "noper") { 
		?>		
			<h5 class="warning">You don't have the access permission for the following page.</h5>
		<?php	
		}
		if($_SESSION['protocol_status']==1)
		{
			$protocol_name='href="protocols"';
		}
		else
		{
			$protocol_name='';
		}
		?>
		<div class="heading-container">
			<h1 class="heading empHead" style="width:60%">Dashboard</h1>
			<?php 
			if($_SESSION["Usertype"]=="Admin" && !isset($_SESSION["loginuser"])) { ?>
				<!--div class="account-renewal-date">Account Renewal Date:- <?php echo date('F d, Y', strtotime('+'.$_SESSION["periodtime"].' days')); ?></div-->
			<?php 
			}?>
		</div>
		<?php  
		if(isset($_SESSION["Superadmin"]) && $_SESSION["Superadmin"]==1) {
		?>
			<div class="dashboard-block">
				<ul class="thumbnails">
					<li class="span4">
						<div class="thumbnail">
							<div class="gallery">
								<a href="newbusiness"><img alt="ab" src="<?php echo $domain; ?>/images/new-consent-form.png" />
									<span class="title">Add New Business</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a> 							
							</div>
						</div><!--End @thumbnail-->
					</li><!--End @span4-->
					<li class="span4">
						<div class="thumbnail">
							<div class="gallery">
								<a href="managebusiness"><img alt="ab" src="<?php echo $domain; ?>/images/All-Business.png">
									<span class="title">All Businesses</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>					
							</div>
						</div><!--End @thumbnail-->
					</li><!--End @span4-->
					<li class="span4">
						<div class="thumbnail">
							<div class="gallery">
								<a href="emailtemplates"><img alt="ab" src="<?php echo $domain; ?>/images/email.png" />
									<span class="title">Email Templates</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							
							</div>					
						</div><!--End @thumbnail-->
					</li><!--End @span4-->
					<li class="span4">
						<div class="thumbnail">
							<div class="gallery">
								<a href="manage-plans"><img alt="ab" src="<?php echo $domain; ?>/images/Manage-Plans.png" />
									<span class="title">Manage Plans</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							
							</div>					
						</div><!--End @thumbnail-->
					</li><!--End @span4-->
					<li class="span4">
						<div class="thumbnail">
							<div class="gallery">
								<a href="superadmin-report"><img alt="ab" src="<?php echo $domain; ?>/images/report.png" />
									<span class="title">Reports</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							
							</div>					
						</div><!--End @thumbnail-->
					</li><!--End @span4-->
					<li class="span4">
						<div class="thumbnail">
							<div class="gallery">
								<a href="clientlocationdetail"><img alt="ab" src="<?php echo $domain; ?>/images/report.png" />
									<span class="title">Export Clients</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							
							</div>					
						</div><!--End @thumbnail-->
					</li><!--End @span4-->					
				</ul><!--End @thumbnails-->		
			</div><!--End @dashboard-block-->
		<?php
		} else { 
		?>
			<div class="dashboard-block">
				<ul class="thumbnails">
				<?php  
					if( in_array(1,$_SESSION["menuPermissions"])){  
				?>
						<li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="user-entry"><img alt="ab" src="<?php echo $domain; ?>/images/new-consent-form.png" />
										<span class="title">New Client</span>
										<p>Add a New Client</p>
									</a> 
								</div>
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
					<?php
					}
					if($_SESSION['logintype']!="client") {
						if( in_array(2,$_SESSION["menuPermissions"])){ 
						?>		
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="treatment"><img alt="ab" src="<?php echo $domain; ?>/images/client-lookup.png">
											<span class="title">Client Lookup</span>
											<p>Find Existing Clients and Perform Treatments</p>
										</a>					
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php
						}
						if( in_array(3,$_SESSION["menuPermissions"])){ 
						?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<!--a href="pricing.php"><img alt="ab" src="<?php echo $domain; ?>/admin/images/pricing-calculator.png" /-->
										<a href="pricing"><img alt="ab" src="<?php echo $domain; ?>/images/pricing-calculator.png" />
											<span class="title">Pricing Calculator</span>
											<p>Calculate Prices Based on Treatment Area</p>
										</a>					
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php 
						} if( in_array(4,$_SESSION["menuPermissions"])){   
						?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="consentforms"><img alt="ab" src="<?php echo $domain; ?>/images/concent-form-review.png" />
										<!--a href="consentforms.php"><img alt="ab" src="<?php echo $domain; ?>/admin/images/concent-form-review.png" /-->
											<span class="title">Consent Form Review</span>
											<p>View, Print, or Review Client Consent Forms</p>
										</a> 					
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php
						} if( in_array(5,$_SESSION["menuPermissions"])){ 
						?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="clientdetails"><img alt="ab" src="<?php echo $domain; ?>/images/people.png" width="72px" height="71px"/>
										<!--a href="clientdetails.php"><img alt="ab" src="<?php echo $domain; ?>/admin/images/manage-employees.png" /-->
											<span class="title">Manage Clients</span>
											<p>Assign Clients to Locations, View Consent Forms and Treatment Photos</p>
										</a> 					
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php
						}
						//if($_SESSION['Usertype']=="Admin"){
						if( in_array(6,$_SESSION["menuPermissions"])){ 
						?>			
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="empdetails"><img alt="ab" src="<?php echo $domain; ?>/images/manage-employees.png" />
										<!--a href="manage-employee.php"><img alt="ab" src="<?php echo $domain; ?>/admin/images/manage-employees.png" /-->
											<span class="title">Manage Employees</span>
											<p>Add or Update Employees and Set Employee Permissions</p>
										</a> 					
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php  
						} 
						if( in_array(7,$_SESSION["menuPermissions"])){ 
							// if($_SESSION['loginuser'] == "sitesuperadmin") {
							if($_SESSION['loginuser'] == "sitesuperadmin" || $_SESSION["Usertype"] == 'Admin') {
						?>
								<li class="span4">
									<div class="thumbnail">
										<div class="gallery">
											<a href="manage_location_details"><img alt="ab" src="<?php echo $domain; ?>/images/manage-office-location.png" />
												<span class="title">Manage Locations</span>
												<p>Create or Update Store Locations</p>
											</a>					
										</div>
									</div><!--End @thumbnail-->
								</li><!--End @span4-->
						<?php 
							} 	
						} if( in_array(8,$_SESSION["menuPermissions"])){   ?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="devicedetails"><img alt="ab" src="<?php echo $domain; ?>/images/devices.png" width="72px" height="71px" />
											<span class="title">Manage Devices</span>
											<p>Add New Lasers and Parameters</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php 
						} 
						if( in_array(9,$_SESSION["menuPermissions"])){  ?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="<?php if(in_array("1",$_SESSION["services"])) echo 'pricingdetails'; else if(in_array("2",$_SESSION["services"])) echo 'pricingdetailshair';?>"><img alt="ab" src="<?php echo $domain; ?>/images/Manage-Pricing.png" width="72px" height="71px" />
											<span class="title">Manage Pricing</span>
											<p>Set Pricing Tables for Pricing Calculator</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php 
						} 
						if( in_array(10,$_SESSION["menuPermissions"])){  ?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="trans_history"><img alt="ab" src="<?php echo $domain; ?>/images/Transaction-History.png" width="72px" height="71px" />
											<span class="title">Transactions History</span>
											<p>See Subscription Payments</p>
										</a>
									
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php } 
						if( in_array(11,$_SESSION["menuPermissions"])){  ?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="your_sub"><img alt="ab" src="<?php echo $domain; ?>/images/Subscription-Status.png" width="72px" height="71px" />
											<span class="title">Subscription Status</span>
											<p>See Current Subscription Plan</p>
										</a>
									
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php 
						} 
						if( in_array(12,$_SESSION["menuPermissions"])){ 
							if($_SESSION['loginuser'] != "sitesuperadmin") { ?>
								<li class="span4">
									<div class="thumbnail">
										<div class="gallery">
											<a href="lapsed-client-status"><img alt="ab" src="<?php echo $domain; ?>/images/Client-Status-Update.png" width="72px" height="71px" />
												<span class="title">Client Status Update</span>
												<p>Client Status Update</p>
											</a>
										</div>
									</div><!--End @thumbnail-->
								</li><!--End @span4-->
						<?php 
							} 
						} 
						if( in_array(13,$_SESSION["menuPermissions"])){ ?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="clientreport"><img alt="ab" src="<?php echo $domain; ?>/images/report.png" width="72px" height="71px" />
											<span class="title">Reports</span>
											<p>Marketing Reports, Revenue Reports, Lapsed and Lost Client Reports</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php 
						}?>
						<li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a target="_blank" <?php echo $protocol_name; ?>><img alt="ab" src="<?php echo $domain; ?>/images/protocol.png" />
										<span class="title">Protocols</span>
										<p>Review Business Protocols</p>
									</a>					
								</div>					
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
						<?php 
						if( in_array(14,$_SESSION["menuPermissions"])){ ?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="manage_protocol"><img alt="ab" src="<?php echo $domain; ?>/images/manage-protocol.png" width="72px" height="71px" />
											<span class="title">Manage Protocol</span>
											<p>Manage Protocol</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php 
						}  
						if( in_array(15,$_SESSION["menuPermissions"])){  ?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="listdisclaimer"><img alt="ab" src="<?php echo $domain; ?>/images/disclaimer.png" width="72px" height="71px" />
											<span class="title">Manage Disclaimers</span>
											<p>Manage Disclaimers</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php 
						}
						if($_SESSION['Admin'] == 'TRUE'){
						?>
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="categories"><img alt="ab" src="<?php echo $domain; ?>/images/category.svg" width="72px" height="71px" />
											<span class="title">Manage Categories</span>
											<p>Manage Categories</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="services"><img alt="ab" src="<?php echo $domain; ?>/images/service.svg" width="72px" height="71px" />
											<span class="title">Manage Services</span>
											<p>Manage Services</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="areas"><img alt="ab" src="<?php echo $domain; ?>/images/area.svg" width="72px" height="71px" />
											<span class="title">Manage Areas</span>
											<p>Manage Areas</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
							<li class="span4">
								<div class="thumbnail">
									<div class="gallery">
										<a href="iframes"><img alt="ab" src="<?php echo $domain; ?>/images/iframe.png" width="72px" height="71px" />
											<span class="title">Generate Widget</span>
											<p>Generate widget</p>
										</a>
									</div>
								</div><!--End @thumbnail-->
							</li><!--End @span4-->
						<?php
						}	
						?>
						<li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="calander"><img alt="ab" src="<?php echo $domain; ?>/images/calendar.svg" width="72px" height="71px" />
										<span class="title">Appointment Calendar</span>
										<p>Appointment Calendar</p>
									</a>
								</div>
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
						<?php
					} 
					?>	
				</ul><!--End @thumbnails-->		
			</div><!--End @dashboard-block-->
		<?php 
		}
		?>
		<!-- show popup notification-->	
		<?php  
		if(!isset($_COOKIE["dashbord"])) {  
			if(($_SESSION['Superadmin']!=1) || ($_SESSION['logintype']!="client")){
			?>			
				<div class="overlay-bg">
					<div class="overlay-content" id="totalPandingReviewDashbord">							
						<div style="text-align:center;padding:40px 5px;" id="loadingImg"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
					</div>
				</div>
			<?php 
			}  
		} 
		?>			
	</div><!--End @form-container-->	
</div><!--End @container-->
<?php
	include('admin_includes/footer.php');
?>
