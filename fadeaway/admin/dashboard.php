﻿<?php
	error_reporting(E_ALL);
	session_start();
	include('admin_includes/header-new.php'); 
	setcookie('dashbord',1, time()+60*60*24*30); // 30 days
	$domain = $_SERVER['DOMAIN'];
	
	?>
	<!-- Page Wrapper -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js-new/app.js"></script>
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
		<?php
			include("../includes/dbFunctions.php");
			$clientDetails = new dbFunctions();
		?>
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<script>
				jQuery(document).ready(function(){
				});
			</script>
			<!-- Begin Page Content -->
			<div class="container-fluid">
			<?php
			if( $_SESSION["periodtime"]<=3 && $_SESSION["Usertype"]=="Admin" && !isset($_SESSION["loginuser"]))	{  ?>		
			<!-- h5 class="warning">Your Subscription plan will be finished in <?php echo $_SESSION["periodtime"]; ?> day(s), please maintain enough balance in your account.</h5 -->
			<?php	
			}
			if( isset($_GET["sub"]) && $_GET["sub"]=="success"){  ?>
				<div class="u_mess" style="display: block;">Thank you. You have successfully subscribed. The renewal date of your plan is <?php echo $_SESSION["ExpireDate"]."."; ?></div>
			<?php
			}
			if( $_GET["msg"] == "noper") {  ?>
				<div class="errormessage" style="display: block;">You don't have the access permission for the following page.</div>		
				
			<?php	
			}
			if($_SESSION['protocol_status']==1) {
				$protocol_name='href="protocols"';
			} else {
				$protocol_name='';
			}
			
			if( isset($_SESSION["Superadmin"]) && $_SESSION["Superadmin"]==1 ) {  ?>
					<div class="superadminDashboard">
						<div class="superadminrow">
							<div class="superadmincol">
								<a href="newbusiness"><img alt="ab" src="<?php echo $domain; ?>/images/new-consent-form.png" />
									<span class="title">Add New Business</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a> 
							</div>
							<div class="superadmincol">
								<a href="managebusiness"><img alt="ab" src="<?php echo $domain; ?>/images/All-Business.png">
									<span class="title">All Businesses</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>	
							</div>
							<div class="superadmincol">
								<a href="emailtemplates"><img alt="ab" src="<?php echo $domain; ?>/images/email.png" />
									<span class="title">Email Templates</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							</div>
							<div class="superadmincol">
								<a href="manage-plans"><img alt="ab" src="<?php echo $domain; ?>/images/Manage-Plans.png" />
									<span class="title">Manage Plans</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							</div>
							<div class="superadmincol">
								<a href="superadmin-report"><img alt="ab" src="<?php echo $domain; ?>/images/report.png" />
									<span class="title">Reports</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							</div>
							<div class="superadmincol">
								<a href="clientlocationdetail"><img alt="ab" src="<?php echo $domain; ?>/images/report.png" />
									<span class="title">Export Clients</span>
									<p>Duis autem vel eum iriure dolor in hendrerit</p>
								</a>
							</div>
						</div>
					</div>
					<ul class="thumbnails">
						<!--li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="newbusiness"><img alt="ab" src="<?php echo $domain; ?>/images/new-consent-form.png" />
										<span class="title">Add New Business</span>
										<p>Duis autem vel eum iriure dolor in hendrerit</p>
									</a> 							
								</div>
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
						<!--li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="managebusiness"><img alt="ab" src="<?php echo $domain; ?>/images/All-Business.png">
										<span class="title">All Businesses</span>
										<p>Duis autem vel eum iriure dolor in hendrerit</p>
									</a>					
								</div>
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
						<!--li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="emailtemplates"><img alt="ab" src="<?php echo $domain; ?>/images/email.png" />
										<span class="title">Email Templates</span>
										<p>Duis autem vel eum iriure dolor in hendrerit</p>
									</a>
								
								</div>					
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
						<!--li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="manage-plans"><img alt="ab" src="<?php echo $domain; ?>/images/Manage-Plans.png" />
										<span class="title">Manage Plans</span>
										<p>Duis autem vel eum iriure dolor in hendrerit</p>
									</a>
								
								</div>					
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
						<!--li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="superadmin-report"><img alt="ab" src="<?php echo $domain; ?>/images/report.png" />
										<span class="title">Reports</span>
										<p>Duis autem vel eum iriure dolor in hendrerit</p>
									</a>
								
								</div>					
							</div><!--End @thumbnail-->
						</li><!--End @span4-->
						<!--li class="span4">
							<div class="thumbnail">
								<div class="gallery">
									<a href="clientlocationdetail"><img alt="ab" src="<?php echo $domain; ?>/images/report.png" />
										<span class="title">Export Clients</span>
										<p>Duis autem vel eum iriure dolor in hendrerit</p>
									</a>
								
								</div>					
							</div><!--End @thumbnail-->
						</li><!--End @span4-->					
					</ul><!--End @thumbnails-->		
			<?php
			} else { 
				
				/**************** All Clients ****************/
				$tbl_manage_location2 = 'tbl_clients';
				$condition2 = "where `BusinessID` = ".$_SESSION['BusinessID'];
				$cols2 = "COUNT(*) AS totalpatient";
				$totalpatient = $clientDetails->selectTableSingleRowNew($tbl_manage_location2,$condition2,$cols2);	
				/********************************************/	
				/**************** Total Revenue *************/	
				$location_con = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY LocationName";
				$locations = $clientDetails->selectTableRowsNew('tbl_manage_location', $location_con);
				$oldlocation = '';
				$arra = array();
				$uniqueids = array();
				$locationids = array();
				foreach($locations AS $location){
					if($oldlocation != $location['LocationName']){
						$i = 0;
						$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
						$oldlocation = $location['LocationName'];
						array_push($locationids,$location['ManageLocationId']);
					}else{
						$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
						array_push($locationids,$location['ManageLocationId']);
					}	
					$i++;
				}
				$locationids = implode(',',$locationids);
	
				$tbl_manage_location1 = 'tbl_treatment_log';
				$condition1 = "where Location IN (".$locationids.") ORDER BY `tbl_treatment_log`.`Location` ASC";
				$cols1 = "SUM(Price) as price";
				$revenue = $clientDetails->selectTableSingleRowNew($tbl_manage_location1,$condition1,$cols1);
				/********************************************/
				/**************** All Loactions *************/
				$tbl_manage_location = 'tbl_manage_location';
				$condition = "where BusinessID=".$_SESSION["BusinessID"]." GROUP BY LocationName ORDER BY ManageLocationId";
				$cols="ManageLocationId,LocationName";
				$ManagesData = $clientDetails->selectallLocations($tbl_manage_location,$condition,$cols);
				/********************************************/
				/************** Pending Reviews *************/
				$table = "tbl_consent_form as c";
				if($_SESSION['Usertype']!="Admin") {
					$condition = " left join tbl_clients as cl on c.ClientID=cl.ClientID where c.TechnicianReview ='' and c.location ='".$_SESSION['Location']."' AND c.BusinessID = $_SESSION[BusinessID] and cl.ClientID!='NULL'";
				} else { 
					$condition = " left join tbl_clients as cl on c.ClientID=cl.ClientID where c.TechnicianReview ='' AND c.BusinessID = $_SESSION[BusinessID] and cl.ClientID!='NULL'";
				}
				$cols=" c.* ";
				$sizedata= $clientDetails->selectTableRows($table,$condition,$cols);
				$countPendingReview =count($sizedata);
				/**** Review for dirctor ********/
				$conditionDir = " left join tbl_clients as cl on c.ClientID=cl.ClientID where c.TechnicianReview !='' AND c.MedicalDirectorNotes='' AND c.BusinessID = $_SESSION[BusinessID] and cl.ClientID!='NULL' ";
				$sizedataDir = $clientDetails->selectTableRows($table,$conditionDir,$cols);
				/********************************************/
				/********** Recent Added Clients ********/
				$table1= "tbl_consent_form";
				$table2= "tbl_clients";
				$join="INNER";
				$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location, tbl_clients.TimeStamp, tbl_consent_form.TimeStampClient";
				$limit = 5;
				$start = 0; 
				$condition = "tbl_clients.ClientID=tbl_consent_form.ClientID WHERE tbl_clients.BusinessID = $_SESSION[BusinessID] ORDER BY tbl_clients.ClientID".$order." DESC LIMIT ".$start.", ".$limit." ";
				$clientdata	= $clientDetails->selectTableJoinNew($table1,$table2,$join,$condition,$cols);
				/********************************************/
				/************** Consent form review *********/
				$table = "tbl_clients";
				$EmpLocation = $_SESSION['Location'];
				$tableUpdate = "tbl_consent_form";
				/*** numrows**/
				$limit =5;
				$start = 0; 
				/*** numrow**/
				if($_SESSION['Usertype']!="Admin"   || (isset($_GET['ClientID']) && base64_decode($_GET['user']) !="Admin")){
					if(isset($_GET['ClientID']) && $_GET['ClientID'] !=""){
					   $condition = " where Location =".$EmpLocation." and ClientID =".base64_decode($_GET['ClientID'])." ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT ".$start.", ".$limit."";
					}else{
						 $condition = " where Location =".$EmpLocation." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT ".$start.", ".$limit."";
					}
				}
				if($_SESSION['Usertype']=="Admin"   || (isset($_GET['ClientID']) && base64_decode($_GET['user']) =="Admin")){
					if(isset($_GET['ClientID']) && $_GET['ClientID'] !=""){
						 $condition = "where ClientID =".base64_decode($_GET['ClientID'])." AND BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT  ".$start.", ".$limit."";
					}else{
						  $condition = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY TechnicianReview,MedicalDirectorNotes  ASC LIMIT  ".$start.", ".$limit."";
					}
				}
				$cols="*";
				$consentdata = $clientDetails->selectTableRowsNew($tableUpdate,$condition);
				/********************************************/
				/*************** Pricing Calculator ********/
				$Services = implode(",",$_SESSION["services"]);
				$table_service = "tbl_master_services";
				$SCond = "where status=1 AND id in($Services)";
				$SCols = "id,name";
				$SData = $clientDetails->selectTableRowsNew($table_service,$SCond,$SCols);
				/*
				$table_sizes = "tbl_master_sizes";
				$SizeCond = "where status=1 AND serviceId=2";
				$SizeCols = "id,size";
				$SizeData = $clientDetails->selectTableRows($table_sizes,$SizeCond,$SizeCols);
				*/
				/********************************************/
				/*************** Subscription****************/
				$BusinessDetails = new dbFunctions();
				$BTable 	= "tbl_business tblB";
				$SubTable 	= "tbl_subscription_history tblS";
				$ETable 	= "tbl_employees tblE";
				$PTable 	= "tbl_master_plans tblP";
				$cols 		= "tblB.BusinessID,tblB.BusinessName,tblB.Logo,tblB.PlanType,tblB.status,tblS.ID,tblS.PlanID,tblS.ExpireDate";

				$conditionNumRows = "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID WHERE tblB.BusinessID = ".$_SESSION['BusinessID']." ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID ORDER BY BusinessID DESC";
				$condition	= $conditionNumRows." LIMIT $start , $limit";
				$var = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);
				$RES = mysqli_query($var,$condition) or die($condition);
				$results=array();
				while($rows=mysqli_fetch_assoc($RES)) {
					$results[]=$rows;
				}
				/********************************************/				
			?>

					<!-- Page Heading -->
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Dashboard</h1>
						<a href="javascript:void(0)" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
					</div>
					<!-- Content Row -->
					<div class="row">
						<!-- Earnings (Monthly) Card Example -->
						<div class="col-xl-3 col-md-6 col-6 mb-4">
							<div class="card border-bottom-primary shadow h-100 py-2 all-clients-outer">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="all-clients-text">All <span>Clients</span></div>
											<h3><?php echo $totalpatient['totalpatient']; ?></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Earnings (Monthly) Card Example -->
						<div class="col-xl-3 col-md-6 col-6 mb-4">
							<div class="card border-bottom-success shadow h-100 py-2 total-revenue-outer">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="all-clients-text">Total <span>Revenue</span></div>
											<h3>$<?php echo number_format($revenue['price'], 2); ?></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Earnings (Monthly) Card Example -->
						<div class="col-xl-3 col-md-6 col-6 mb-4">
							<div class="card border-bottom-danger shadow h-100 py-2 all-loactions-outer">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="all-clients-text">All <span>Loactions</span></div>
											<h3><?php echo count($ManagesData); ?></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Pending Requests Card Example -->
						<div class="col-xl-3 col-md-6 col-6 mb-4">
							<div class="card border-bottom-warning shadow h-100 py-2 technician-director-outer">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div class="all-clients-text">Pending Reviews <span>Technician/Director</span></div>
											<h3><?php echo count($sizedata).'/'.count($sizedataDir);?></h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card shadow mb-4 table-main-con">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Recent Added Clients</h6>
							<span><a href="clientdetails">View All</a></span>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th>FIRST NAME</th>
											<th>Last Name</th>
											<th>Office Location(s)</th>
											<th>registered Date</th>
											<th>Treatment Date</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									if(!empty($clientdata)) {
										$i = 0;
										foreach($clientdata as $cldata){
									?>
											<tr id="<?php echo $cldata['ClientID']?>">
												<td><?php echo ucfirst($cldata['FirstName']); ?></td>
												<td><?php echo ucfirst($cldata['LastName']);  ?></td>
												<td><?php 
													if( $cldata['Location'] !="" ){
														$clentLocation = explode( ",",$cldata['Location'] );
														for( $j=0 ; $j<count($clentLocation);$j++ ){
															$tbl_manage_location = "tbl_manage_location";
															$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
															$cols = "*";
															$locationData = $clientDetails->selectTableSingleRow($tbl_manage_location,$condition1);
															echo $locationData['LocationName'];
															if($j !=count($clentLocation)-1) {
																echo ",&nbsp;";
															}
														}
													} ?>
												</td>
												<td><?php echo date('M d, Y',strtotime($cldata['TimeStamp'])); ?></td>
												<td><?php echo date('M d, Y',strtotime($cldata['TimeStampClient'])); ?></td>
											</tr>
										<?php
										} 
									} else {
									?>
										<tr><td colspan='5'>No record found.</td></tr>
									<?php
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- Content Row -->
					<div class="row">
						<!-- Area Chart -->
						<div class="col-xl-8 col-lg-7">
							<div class="card shadow mb-4">
								<div class="card shadow mb-4 table-main-con total-revenue-con-outer">
									<div class="card-header py-3">
										<h6 class="m-0 font-weight-bold text-primary">Total Revenue</h6>
									</div>
									<div class="card-body">
										<div class="total-revenue-con">Total Revenue (in $)</div>  	
										<div class="chart-bar">
											<!-- canvas id="myBarChart"></canvas -->
											<canvas id="chartnew"></canvas>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-5">
							<div class="pricing-calculater-con">
								<h6>Pricing Calculator <a id="backbtn" class="backbtn"><img src="../images/left-arrow.svg" class="backarrowimg"> Back</a></h6>
								<div class="formblkouter">
									<div class="norecordblockouter" style="display:none;" id="pricing-result"></div>
									<form action="" method="post" name="pricingform" id="pricingform">
										<div class="pricing-calculater-inner">
											<span>Select Service</span>
											<div class="dropdown mb-4">
												<select class="select-option" name="Service" id="Service">						
												<?php
													if($SData !=NULL) {
														foreach($SData as $services) {  ?>
															<option value="<?php echo $services['id']?>" ><?php echo $services['name']?></option>
														<?php
														}
													}
												?>
												</select>
											</div>
											<span>Please Input Size</span>
											<input type="text" placeholder="Enter size here" name="pricing_amt" class="text-input-field" id="pricing_amt_dash" autocomplete="off"/>
											<div class="span6 select" id="select-size">
												<label id="" class="user-name">&nbsp;</label>
												<select class="select-option" name="Size" id="Size">						
													<option value="0">-Select Size-</option>
													<?php
													if($SizeData !=NULL) {
														foreach($SizeData as $sizes) { ?>
															<option value="<?php echo $sizes['size']?>" ><?php echo $sizes['size']?></option>						
														<?php
														}
													}
													?>
												</select>
											</div>
										</div>
										<input type="hidden" name="location" class="text-input-field" id="location" value="<?=$_SESSION['Location']?>"/>							
										<input type="hidden" name="pagename" class="text-input-field" id="pagename" value="dashboard"/>
									</form>
								</div>
							</div>
							<div class="pricing-calculater-con subscription-main-con">
								<h6>Subscription</h6>
								<div class="subscription-inner-con">
								<?php  
									$empcond = "where BusinessID='".$results[0]["BusinessID"]."'";
									$empdata = $clientDetails->selectTableSingleRow($ETable,$empcond,$cols="*");
									$plancond = "where id='".$results[0]["PlanID"]."'";
									$plandata = $clientDetails->selectTableSingleRow($PTable,$plancond,$cols="*");
								?>
									<strong><?php echo $plandata["title"];?> <?php if($plandata["Type"] == "Free") echo "<span>( Free Trial )</span>"; ?></strong>
									<span><?php echo date('M d, Y', strtotime($plandata['created_date']))?>  to  <?php echo date('M d, Y', strtotime($results[0]['ExpireDate']))?></span>
								</div>     
								<div class="renewal-date">Renewal Date: <?php echo date('M d, Y', strtotime($results[0]['ExpireDate']))?></div>            	
							</div>
						</div>
					</div>
					<!-- Content Row -->
					<div class="card shadow mb-4 table-main-con">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Consent form review</h6>
							<span><a href="consentforms">View All</a></span>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th>FIRST NAME</th>
											<th>Last Name</th>
											<th>Office Location(s)</th>
											<th>Review Status</th>
											<th>ACTION</th>
										</tr>
									</thead>
									<tbody>
									<?php
										if(!empty($consentdata)){
											foreach($consentdata as $clients){
												$clientcondition = " WHERE ClientID = $clients[ClientID] ";
												$clientcols = "AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName";
												$clientsname = $clientDetails->selectTableSingleRow("tbl_clients",$clientcondition, $clientcols);
												if($clients['ConsentID'] !=0 && $clients['ClientID'] !=0 && $clientsname['FirstName'] !="" && $clientsname['LastName'] !="" && $clients['Location'] !="" ){
									?>
													<tr>
														<td><a href ="client.php?clientid=<?php echo $clients['ClientID'];?>">	<?php echo ucfirst($clientsname['FirstName']); ?> </a></td>
														<td><a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><?php echo $clientsname['LastName'];?></a></td>
														<td>
															<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>">
															<?php
															if($clients['Location']!=""){
																$clentLocation = explode(",",$clients['Location']);					        
																for($j=0 ; $j<count($clentLocation);$j++){								 
																	$tbl_manage_location	= "tbl_manage_location";
																	$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
																	$cols="*";
																	$locationData	= $clientDetails->selectTableSingleRow($tbl_manage_location,$condition1);
																	echo $locationData['LocationName'];							  
																	if($j !=count($clentLocation)-1){
																		echo ",&nbsp;";
																	}						
																}				 
															}
															?>
															</a>
														</td>
														<td class="pending-for-technician">
															<?php 
															if($clients['TechnicianReview']==""){ ?>
																<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="pandingReview">Pending for technician review </span></a>
															<?php
															} else if($clients['TechnicianReview'] !="" && $clients['MedicalDirectorNotes'] ==""){ ?>
																<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="pandingReviewDoctor">Pending for doctor review </span></a>
															<?php }
															else  { ?>
																<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><span class="compleReview">Completed</span></a>
															<?php } ?>
														</td>
														<td>
															<a href ="client.php?clientid=<?php echo $clients['ClientID'];?>"><img src="<?php echo $domain; ?>/img/eye-img.jpg" alt=""/></a> <a href="clientconsentform?ClientID=<?php echo base64_encode($clients['ClientID'])?>&action=clientdetails"><img src="<?php echo $domain; ?>/img/print-img.jpg" alt=""/></a>
														</td>
													</tr>
										<?php	
												}
											}
										} else {
										?>	
											<tr><td colspan='5'>No record found.</td></tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

		<?php } ?>	
			</div>
				<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
