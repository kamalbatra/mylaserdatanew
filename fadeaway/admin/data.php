<?php
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$pieChart = new dbFunctions();

	$search_query = "SELECT ReferralSource,COUNT(*) as totalnum  FROM tbl_clients Where ReferralSource !='' AND BusinessID=".$_SESSION["BusinessID"]."";
	if(isset($_GET['from']) && $_GET['from'] != '' && isset($_GET['to']) && $_GET['to'] != ''){
		$_GET['to'] = date("Y-m-d", strtotime('+1 day', strtotime($_GET['to'])));
		$search_query .= " AND TimeStamp>='".$_GET['from']."' AND TimeStamp<='".$_GET['to']."'";
	}if(isset($_GET['loc']) && $_GET['loc'] != ''){
		$search_query .= " AND Location IN(".$_GET['loc'].")";
	}
	$search_query .= " GROUP BY ReferralSource";
	//echo $search_query; die;
	$result = mysqli_query($pieChart->con,$search_query);
	$rows = array();
	while($r = mysqli_fetch_array($result)) {
		$row[0] = $r[0];
		$row[1] = (int)$r[1];
		array_push($rows,$row);		
	}
	if(isset($_GET['get_tabular'])){
		if(!empty($rows)){
			$tabfulldata = '';
			$lastkey = 0;
			$total = 0; foreach($rows AS $key => $ref_data){ $total += $ref_data[1];
				$tabfulldata .= "<tr class='";
				if(($key%2)==0)
					$tabfulldata .= "even";
				else
					$tabfulldata .= "odd";
					if($ref_data[0]=="Tattoo Artist")
					{
						$artist='<span style="cursor:pointer;color:#11719f;" onclick="tattoo_artist(); ">'.$ref_data[0].'</span>';
					}else
					{
						$artist='<span>'.$ref_data[0].'</span>';
					}
				$tabfulldata .= "'><td class='text-align'>".++$key."</td><td>".$artist."</td><td class='text-align'>".$ref_data[1]."</td></tr>";
				$lastkey = $key;
			}
			$tabfulldata .= "<tr class='";
			if(($lastkey%2)==0)
				$tabfulldata .= "even";
			else
				$tabfulldata .= "odd";
			$tabfulldata .= "'><td colspan=2 style='text-align:right'><b>Total</b></td><td class='text-align'>".$total."</td></tr>";
		}else{
			$tabfulldata = "<tr style='text-align:center;color:#c0c0c0'><td colspan=3>No record found!!</td></tr>";
		}
		echo $tabfulldata;
		die;
	}else{
		print json_encode($rows);
	}
?> 
