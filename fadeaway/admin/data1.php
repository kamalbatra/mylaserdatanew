<?php
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$pieChart = new dbFunctions();
	
	$location_con = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY LocationName";
	$locations = $pieChart->selectTableRows('tbl_manage_location', $location_con);
	$oldlocation = '';
	$arra = array();
	$uniqueids = array();
	$locationids = array();
	foreach($locations AS $location){
		if($oldlocation != $location['LocationName']){
			$i = 0;
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
			$oldlocation = $location['LocationName'];
			array_push($locationids,$location['ManageLocationId']);
		}else{
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
			array_push($locationids,$location['ManageLocationId']);
		}	
		$i++;
	}
	$locationids = implode(',',$locationids);
	
	$search_query = "SELECT MONTH(TimeStamp) AS m, COUNT(*) AS totalpatient FROM `tbl_clients` WHERE `BusinessID` = ".$_SESSION['BusinessID'];
	
	$search_query1 = "SELECT MONTH(Date_of_treatment) AS m, COUNT(*) AS totaltreatment, SUM(Price) as price FROM `tbl_treatment_log` WHERE ";
	
	if( isset($_GET['from']) && $_GET['from'] != '' ){
		$search_query .= " AND YEAR(TimeStamp) ='".$_GET['from']."'" ;
		$search_query1 .= " YEAR(Date_of_treatment) ='".$_GET['from']."'" ;
	} else {
		$search_query .= " AND YEAR(TimeStamp) ='".date('Y')."'" ;
		$search_query1 .= " YEAR(Date_of_treatment) ='".date('Y')."'" ;	
	}
	
	if(isset($_GET['loc']) && $_GET['loc'] != ''){
		$search_query .= " AND Location IN(".$_GET['loc'].")";
		$search_query1 .= " AND Location IN(".$_GET['loc'].")";
	} else {
		//~ $search_query .= " AND Location IN(".$_GET['loc'].")";
		$search_query1 .= " AND Location IN(".$locationids.")";
	}
	
	$search_query .= " GROUP BY m";
	$search_query1 .= "  GROUP BY m ORDER BY `tbl_treatment_log`.`Location` ASC";
	
	$result = mysqli_query($pieChart->con,$search_query);
	$result1 = mysqli_query($pieChart->con,$search_query1);
	$rows = array();
	
	$i = 1;
	while( $r = mysqli_fetch_array($result) ) {
		$rows[$i]['month'] = $r[0];
		$rows[$i][0] = $r[1];
		$i++;
	}	
	$j=1;
	while($r1 = mysqli_fetch_array($result1)) {
		$rows[$j]['month1'] = $r1[0];
		$rows[$j][1] = $r1[1];
		$rows[$j][2] = (int)$r1[2];
		$j++;
	}

	if(isset($_GET['get_tabular'])){
		if(!empty($rows)){
			$tabfulldata = '';
				
			$totalpatient = $totaltreatment = $totalrevenue = 0; $lastkey = 0; 
			
			foreach($rows as $key => $ref_data){	
				
				$totalpatient += @$ref_data[0]; 
				$totaltreatment += @$ref_data[1]; 
				$totalrevenue += @$ref_data[2]; 
				if( isset($ref_data['month']) ){
					$month = $ref_data['month'];
				} else{
					$month = $ref_data['month1'];
				}
				
				$tabfulldata .= "<tr class='";
				if(($key%2)==0){
					$tabfulldata .= "even";
				}
				else {
					$tabfulldata .= "odd";
				}

				$tabfulldata .= "'><td class='text-align1'>".$pieChart->monthname1(@$month)."</td><td class='text-align'>".@$ref_data[0]."</td><td class='text-align'>".@$ref_data[1]."</td><td class='text-align'>".@$ref_data[2]."</td></tr>";
				$lastkey = $key;
			}
			$tabfulldata .= "<tr class='";
			if(($lastkey%2)==0)
				$tabfulldata .= "even";
			else
				$tabfulldata .= "odd";
			$tabfulldata .= "'><td class='text-align'><b>Total</b></td><td class='text-align'><b>".$totalpatient."</b></td><td class='text-align'><b>".$totaltreatment."</b></td><td class='text-align'><b>".$totalrevenue."</b></td></tr>";
		}else{
			$tabfulldata = "<tr style='text-align:center;color:#c0c0c0'><td colspan=5>No record found.</td></tr>";
		}
		echo $tabfulldata;
		die;
	}else{
		print json_encode($rows);
	}
?> 
