/*  jQuery ready function. Specify a function to execute when the DOM is fully loaded.  */
$(document).ready(
  /* This is the function that will get executed after the DOM is fully loaded */
  function () {
    $("#DOB").datepicker({
		changeMonth: true,//this option for allowing user to select month
		changeYear: true, //this option for allowing user to select from year range
		showOn: "both",
		buttonImage: "datepicker/images/calendar.png",
		buttonImageOnly: true,
		buttonText: "Select Date of birth",
		yearRange: "c-100:c",
		onSelect: function (date) {
			var inputDate = date;
			var from = date.split("-");
			//Now find the Age based on the Birth Date
				getAge(new Date(from[2], from[0]-1, from[1]));
			function getAge(birth) {
				var today = new Date();
				var nowyear = today.getFullYear();
				var nowmonth = today.getMonth();
				var nowday = today.getDate();
				var birthyear = birth.getFullYear();
				var birthmonth = birth.getMonth();
				var birthday = birth.getDate();
				var age = nowyear - birthyear;
				
				var age_month = nowmonth - birthmonth;
				var age_day = nowday - birthday;
				
				if(age_month < 0 || (age_month == 0 && age_day <0)) {
					age = parseInt(age);
				}
				//alert(age);
				document.getElementById('Age').value=age;	
				/*if ((age == 18 && age_month <= 0 && age_day <=0) || age < 18) {
				
				}
				else {
					alert("You have crossed your 18th birthday !");
				}*/
 			}	
		}		
    });
   
  }

);
