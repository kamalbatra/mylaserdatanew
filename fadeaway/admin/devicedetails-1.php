<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$adminForms	= new dbFunctions();
	if( !in_array(8,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php
	}
	
	$services = implode(",",$_SESSION["services"]);
	
	/*** fetch All device Name**/
	$tableDevice = "tbl_devicename";
	$tableService = "tbl_master_services";
	$condition1 = "WHERE BusinessID=$_SESSION[BusinessID] AND serviceId in($services) ANd status=1 ORDER BY deviceId  DESC ";
	$cols1="deviceId,DeviceName,serviceId";
	$DeviceData	= $adminForms->selectTableRows($tableDevice,$condition1,$cols1);
	foreach($DeviceData as $dname) {
		$devid[]= $dname;
	}
	/*** fetch All device Name**/
?>
<script type="text/javascript">
	$(document).ready(function() {
	//Delete flunce from database
		$('a.deleteDevice').click(function() {
			if (confirm("Are you sure you want to delete this device?")) {
				var id = $(this).parent().parent().attr('id');
				var data = 'deviceId=' + id +'&devices='+'devices';
				var parent = $(this).parent().parent();			
				$.ajax({
					type: "POST",
					url: "delete_manageDevice.php",
					data: data,
					cache: false,				
					success: function(data) {					   
						parent.fadeOut('slow', function() {$(this).remove();});
						$('.showmsg').show();
							$('.successmsg').html("");
							$('.successmsgtext').html('Device deleted successfully.');
							setTimeout(function() {
								$('.showmsg').hide();
								location.reload();
								}, 4000)
					}
				});
			}
		});
		// style the table with alternate colors
		// sets specified color for every odd row
		/*$('#'+id).css('background',' #FFFFFF');*/
	});
</script>
<style>
	.srtHeadloc { width:18%; }
	.successmsg1 {
    float: left;
    margin-bottom: 15px !important;
    text-align: center;
    width: 100%;
}
.deleteDevice
{
cursor:pointer;
}
</style>
<div class="showmsg" style="display:none;">
		<span class='successmsg1'><font color='green' class="successmsgtext"></font></span>
		</div>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Manage Device</h1>
		<div class="addNew"><a class="empLinks" href="adminentry" class="submit-btn">Add New Device </a></div>
	</div>
	<div class="user-entry-block">
		<div class="tablebushead">	
	<div class="tablebusinner">	
		<div class="row sortingHead">	
			<div class="span3 srtHeadloc srtHeadBorder"> Device Name</div>
			<div class="span3 srtHeadloc srtHeadBorder"> Service Name</div>
			<div class="span3 srtHeadloc srtHeadBorder"> Wavelength</div>
			<div class="span3 srtHeadloc srtHeadBorder"> Spotsize</div>
			<div class="span3 srtHeadEdit srtHeadBorder"> Action</div>
			<div class="span3 srtHeadEdit srtHeadBorder" style="border:none;"> Action</div>
		</div>
  <?php 
		/*** fetch All Wave length Name**/	
		if($DeviceData !=NULL) {
			$bgColor=0;
			foreach($devid as $d) {
				if( $bgColor %2==0 ) { $bgdata = "bgnone"; }
				else { $bgdata = "bgdata"; }
				$tbl_ta2_wavelengths = "tbl_ta2_wavelengths";
				$condition = "where deviceId=".$d['deviceId']." ORDER BY WavelengthID DESC ";
				$cols="*";
				$wavelengthsData = $adminForms->selectTableRows($tbl_ta2_wavelengths,$condition,$cols);
			?>
				<div class="row  <?php echo $bgdata;?>" id="<?php echo $d['deviceId'];?>" >
					<div class="span3 srtHeadloc srtcontent">
						<label id="" class="user-name"><?php echo $d['DeviceName'];?> </label>						
					</div>
					<div class="span3 srtHeadloc srtcontent">
					<?php 
						$SCond = "where id=".$d['serviceId'];
						$SCols = "name";
						$SData = $adminForms->selectTableSingleRow($tableService,$SCond,$SCols);
					?>
						<label id="" class="user-name"><?php echo $SData['name'];?> </label>
					</div>
					<div class="span3 srtHeadloc srtcontent">
						<label id="" class="user-name" style="margin-left:15%;">					
							<!-- wavelength-->							
							<?php 
							//foreach($wavelengthsData as $data){
							for($jwav = 0; $jwav<count($wavelengthsData); $jwav++) {
								echo $wavelengthsData[$jwav]['Wavelength'];
								if($jwav !=count($wavelengthsData)-1) {
									echo ",&nbsp;";
								}
							} ?>
						</label>
					</div>
					<!--wavelength-->
					<!--spot size-->
					<div class="span3 srtHeadloc srtcontent">					
						<label id="" class="user-name" style="margin-left:20%;">	
						<?php 
							/*** fetch All Spot Size Name**/
							$tbl_ta2_spot_sizes	= "tbl_ta2_spot_sizes";
							$condition = "where deviceId=".$d['deviceId']." ORDER BY 	spotsizeID  DESC ";
							$cols="*";
							$spotData = $adminForms->selectTableRows($tbl_ta2_spot_sizes,$condition);
							//foreach($spotData as $sdata){					
							for($jSpot = 0 ;$jSpot<count($spotData);$jSpot++) {
								echo $spotData[$jSpot]['spotsize'];
								if($jSpot !=count($spotData)-1) { 
									echo ",&nbsp;";
								}					
							} ?>
						</label>
					</div>
					<!--spot size-->
					<div class="span3 srtHeadEdit srtcontent text-align">
						<a href="deviceedit?deviceId=<?php echo base64_encode($d['deviceId']); ?>&action=device">
							<img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit wavelength"/>
						</a>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align">
						<a class="deleteDevice"><img src="<?php echo $domain; ?>/images/b_drop.png" title="Delete device"/></a>
					</div>
			    </div><!--End @row-->
			<?php
				$bgColor++; 
			}
		}//if $devid not null
		else 
			echo "<div class='not-found-data'>No device found.</div>"; ?>
	</div><!--End @user-entry-block-->
</div><!--End @form-container-->
</div><!--End @form-container-->
</div><!--End @form-container-->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
