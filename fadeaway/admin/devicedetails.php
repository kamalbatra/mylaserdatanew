<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$adminForms	= new dbFunctions();
	if( !in_array(8,$_SESSION["menuPermissions"])){ ?> 
		<script>
			window.location.replace("dashboard.php");
		</script>
	<?php }

	$services = implode(",",$_SESSION["services"]);
	
	/*** fetch All device Name**/
	$tableDevice = "tbl_devicename";
	$tableService = "tbl_master_services";
	$condition1 = "WHERE BusinessID=$_SESSION[BusinessID] AND serviceId in($services) ANd status=1 ORDER BY deviceId  DESC ";
	$cols1="deviceId,DeviceName,serviceId";
	$DeviceData	= $adminForms->selectTableRows($tableDevice,$condition1,$cols1);
	foreach($DeviceData as $dname) {
		$devid[]= $dname;
	}
	/*** fetch All device Name**/
?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('a.deleteDevice').click(function() {
				if (confirm("Are you sure you want to delete this device?")) {
					var id = $(this).parent().parent().attr('id');
					var data = 'deviceId=' + id +'&devices='+'devices';
					var parent = $(this).parent().parent();			
					$.ajax({
						type: "POST",
						url: "delete_manageDevice.php",
						data: data,
						cache: false,				
						success: function(data) {					   
							parent.fadeOut('slow', function() {$(this).remove();});
							$('.showmsg').show();
							$('.successmsg').html("");
							$('.successmsgtext').html('Device deleted successfully.');
							setTimeout(function() {
								$('.showmsg').hide();
								location.reload();
							}, 4000)
						}
					});
				}
			});
		});
	</script>
	<style>
		.srtHeadloc { width:18%; }
		.successmsg1 { float: left; margin-bottom: 15px !important; text-align: center; width: 100%; }
		.deleteDevice { cursor:pointer; }
	</style>
	
	<div class="showmsg" style="display:none;">
		<span class='successmsg1'><font color='green' class="successmsgtext"></font></span>
	</div>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Device</h1>
				</div>
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="adminentry" class="submit-btn"><button class="addnewbtn">Add New Device </button></a>
						</div>
					</div>
					<div class="card-body">
					<?php
					    if($DeviceData !=NULL) { 
							$bgColor=0;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th class="span3 srtHeadloc srtHeadBorder">Device Name</th>
										<th class="span3 srtHeadloc srtHeadBorder">Service Name</th>
										<th class="span3 srtHeadloc srtHeadBorder">Wavelength</th>
										<th class="span3 srtHeadloc srtHeadBorder">Spotsize</th>
										<th class="span3 srtHeadEdit srtHeadBorder center-text">Action</th>
										<th class="span3 srtHeadEdit srtHeadBorder center-text">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									foreach($devid as $d) {
										if( $bgColor %2==0 ) { 
											$bgdata = "bgnone"; 
										} else { 
											$bgdata = "bgdata"; 
										}
										$tbl_ta2_wavelengths = "tbl_ta2_wavelengths";
										$condition = "where deviceId=".$d['deviceId']." ORDER BY WavelengthID DESC ";
										$cols="*";
										$wavelengthsData = $adminForms->selectTableRows($tbl_ta2_wavelengths,$condition,$cols);
									?>
										<tr class=" <?php echo $bgdata;?>" id="<?php echo $d['deviceId'];?>">
											<td class="span3 srtHeadloc srtcontent"><label id="" class="user-name"><?php echo $d['DeviceName'];?></label></td>
											<td class="span3 srtHeadloc srtcontent">
											<?php
												$SCond = "where id=".$d['serviceId'];
												$SCols = "name";
												$SData = $adminForms->selectTableSingleRow($tableService,$SCond,$SCols);
											?>
												<label id="" class="user-name"><?php echo $SData['name'];?> </label>
											</td>
											<td class="span3 srtHeadloc srtcontent">
												<label id="" class="user-name" style="margin-left:15%;">
													<?php
													for($jwav = 0; $jwav<count($wavelengthsData); $jwav++) {
														echo $wavelengthsData[$jwav]['Wavelength'];
														if($jwav !=count($wavelengthsData)-1) {
															echo ",&nbsp;";
														}
													}
													?>
												</label>
											</td>
											<td class="span3 srtHeadloc srtcontent">
												<label id="" class="user-name" style="margin-left:20%;">
													<?php
													/*** fetch All Spot Size Name**/
													$tbl_ta2_spot_sizes	= "tbl_ta2_spot_sizes";
													$condition = "where deviceId=".$d['deviceId']." ORDER BY 	spotsizeID  DESC ";
													$cols="*";
													$spotData = $adminForms->selectTableRows($tbl_ta2_spot_sizes,$condition);
													for($jSpot = 0 ;$jSpot<count($spotData);$jSpot++) {
														echo $spotData[$jSpot]['spotsize'];
														if($jSpot !=count($spotData)-1) {
															echo ",&nbsp;";
														}
													}
													?>
												</label>
											</td>
											<td class="span3 srtHeadEdit srtcontent text-align center-text">
												<a href="deviceedit?deviceId=<?php echo base64_encode($d['deviceId']); ?>&action=device">
													<img src="<?php echo $domain; ?>/img/editimg.png" title="Edit wavelength"/>
												</a>
											</td>
											<td class="span3 srtHeadEdit srtcontent text-align center-text">
												<a class="deleteDevice"><img src="<?php echo $domain; ?>/img/minusbtn.png" title="Delete device"/></a>
											</td>
										</tr><!--End @row-block-->
										<?php
										$i++;
									} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
						}
						else {
							echo "<div class='not-found-data'>No device found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
