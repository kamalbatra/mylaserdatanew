<?php
	//session_start();
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$adminForms	= new dbFunctions();
?>
<?php $deviceId  = base64_decode($_GET['deviceId']); ?>
<!-- Edit form Validation-->
<!---- Validation for empty field--->
<script type="text/javascript">
	jQuery(document).ready(function() {		
		jQuery("#manageDeviceEdit").validate({
			rules: {
				DeviceName: "required",              
				DeviceName: {
					required: true,
				},
			},                
			messages: {
				DeviceName: "This field is required.",
			}
		});
		$('#manageDeviceEditBtn').click(function() {
			if( $("#manageDeviceEdit").valid()){
				//alert("ss");
				//var strForm = $( "#manageDeviceEdit" ).serialize();
				var $textboxes = $('input[name="Wavelength[]"]')
				var $textboxesSpot = $('input[name="spotsize[]"]')			
				var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;			
				var arr = $('.deviceclass').map(function(i, e) {
					if(!numberRegex.test(e.value)) {
						//return e.value; 
						//alert("ee:-"+e.id);				 
						$("#"+e.id).css("border","1px solid red");	
						$("#"+e.id).val("");
						$("#"+e.id).focus();
						$("#"+e.id).addClass('placeholderInt');
						$("#"+e.id).attr("placeholder", "Enter numeric value only");			  
						return e.id; 
					} else {					
						$("#"+e.id).css("border","1px solid #ccc");	
					}				 
				}).toArray();
				/*var arrSpot = $('.deviceclassSpot').map(function(i, e) {
					if(!numberRegex.test(e.value)){
						//return e.value; 
						//alert("ee:-"+e.id);				 
						$("#"+e.id).css("border","1px solid red");	
						$("#"+e.id).focus();
						$("#"+e.id).addClass('placeholderInt');
						$("#"+e.id).attr("placeholder", "Enter numeric value only");			  
						return e.id; 
					}else{					
						$("#"+e.id).css("border","1px solid #ccc");	
					}				 
				}).toArray*/			
				//alert(arr.length);	
				if(arr.length ==0) {
					// alert("ok");
					edit_manageDetail('manageDeviceEdit');
				}			
			} else {		
			}
		});
	});
</script>
<script>
	function edit_manageDetail(strVal){
		//alert(strVal);
		//var deviceId=15;	
		var str = $("#manageDeviceEdit").serialize();
		$.ajax({
			type: "POST",
			url: "editajax_manageDetail.php",
			data: str,
			cache: false,
			success: function(result) {
				//location.reload();
				//alert(result);
				$("#successUpdate").html(result);						
				setTimeout(function() {location.reload()	}, 2000);
			}
		});
	}	
</script>
<!-- --- ADD text and remove text box for Wave length -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendWavelengthBtn").click( function() {
			$("#textBoxwave").show();
			if(counter1>10){
				   // alert("Only 10 textboxes allow");
					return false;	
			}
			$("#addwave").append('<div class="boxx"><a href="#" class="remove_wave btn btn-danger">.</a> <input type="text" class="deviceclass text-input-field" name="Wavelength[]" id="wav'+counter1+'"></div>');	
			//alert(counter1);
			counter1++;
			return false;
		});	
		$('.remove_wave').live('click', function() {
			if(counter1==1){	
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});	
	});
</script>
<!-- --- ADD text and remove text box for SpotSize -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendSpotSize").click( function() {
			$("#textBoxSpot").show();
			if(counter1>10) {
				// alert("Only 10 textboxes allow");
				return false;	
			}
			$("#addSpot").append('<div class="boxx"><a href="#" class="remove_spot btn btn-danger">.</a> <input type="text" class="deviceclass text-input-field" name="spotsize[]" id="spt'+counter1+'"></div> ');
			//alert(counter1);
			counter1++;
			return false;
		});		
		$('.remove_spot').live('click', function() {
			if(counter1==1) {	
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});
	});
</script>
<script>
	jQuery(document).ready( function() {
		$(".del").click( function() {	
			var delId = $(this).attr('id');
			$("#"+delId).val(delId);
			//$("#wave"+delId).attr("disabled", "disabled");
			//$("#wave"+delId).css("background-color", "#cccccc");		
			//$("#spot"+delId).attr("type", "hidden");
			//hideSpotRow
			alert($(this).parent().parent().attr('id'));
			var id = $(this).parent().parent().attr('id');
			//alert(id);
			//$("#spot"+delId).css("background-color", "#cccccc");
			$("#"+id).css("display", "none");
		});
	});
</script>
<?php
	if(isset($deviceId)) {
?>
		<div class="form-container">
			<div class="heading-container">
				<h1 class="heading empHead">Edit device </h1>
				<div class="addNew">
					<a class="empLinks" href="devicedetails" class="submit-btn"> Manage device</a>
				</div>
			</div>	
			<div class="user-entry-block">			
				<!--Edit wave lenght -->
				<?php 
				if(isset($deviceId)) { 					
					//device name
					$tableDevice	= "tbl_devicename";
					$conditionDevice = "where deviceId=".$deviceId." ";
					$colsDevice="*";
					$DeviceData	= $adminForms->selectTableSingleRow($tableDevice,$conditionDevice,$colsDevice);
					//print_r($DeviceData);							  
					//Wavelength 	
					$tbl_ta2_wavelengths	= "tbl_ta2_wavelengths";
					$condition = "where deviceId=".$deviceId." ORDER BY WavelengthID DESC ";
					$cols="*";
					$wavData	= $adminForms->selectTableRows($tbl_ta2_wavelengths,$condition,$cols);									                
					//sopt size
					$tbl_ta2_spot_sizes	= "tbl_ta2_spot_sizes";
					$conditionSpotSize = "where deviceId=".$deviceId." ORDER BY spotsizeID DESC ";
					$colsSpotSize = "*";
					$spotData = $adminForms->selectTableRows($tbl_ta2_spot_sizes,$conditionSpotSize,$colsSpotSize);	
					//Service Name
					$tbl_service = "tbl_master_services";
					$SCond = "where id=".$DeviceData["serviceId"];
					$SCols = "name";
					$SData = $adminForms->selectTableSingleRow($tbl_service,$SCond,$SCols);	
				?>
					<form action="" method="post" name="manageDeviceEdit" id="manageDeviceEdit">
						<input type="hidden" name="deviceId" value="<?php echo $DeviceData['deviceId'];?>" />					 
						<div class="row treatment">								   
							<div class="span3" style="text-align:right;padding-top:16px;">
								<label class="user-name">Service Name:</label>
							</div>
							<div class="span6">
								<input type="text" class="text-input-field" value="<?php echo $SData["name"];?>" name="DeviceName"  disabled/>
							</div>				  
						</div><!--End @row-block-->
						<div class="row treatment">								   
							<div class="span3" style="text-align:right;padding-top:16px;">
								<label class="user-name">Device Name:</label>
							</div>
							<div class="span6">
								<input type="text" class="text-input-field" value="<?php echo $DeviceData['DeviceName'];?>" name="DeviceName" />
							</div>				  
						</div><!--End @row-block-->
						<div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;"><label class="user-name">Numeric validation:</label></div>
							<div class="span6">
								<select class="select-option" name="Required" id="Required">
									<option value="Yes" <?php if($DeviceData['Required']=="Yes"){  echo 'selected=selected' ;}?>>Yes</option>
									<option value="No" <?php if($DeviceData['Required']=="No"){  echo 'selected=selected' ;}?>>No</option>
								</select>	
								<span id="deviceResult" class="mngdevicesName"> </span>										   
							</div>									  
						</div><!-- @row end-->								 
						<?php
						$n=1; foreach($wavData as $wdata) {	?>
							<input type="hidden" name="WavelengthID[]" value="<?php echo $wdata['WavelengthID'];?>" />
							<input type="hidden" name="DeleteWave[]"  id="<?php echo $wdata['WavelengthID'];?>" value="" />
							<div class="row treatment" id="wavelen<?php echo $wdata['WavelengthID'];?>">
								<div class="span3" style="text-align:right;padding-top:16px;">
									<label class="user-name"><?php if($n ==1){ ?> Wavelength: <?php }?></label>
								</div>
								<div class="span6">
									<input type="text" class="deviceclass text-input-field" value="<?php echo $wdata['Wavelength'];?>" name="Wavelength[]" id="wave<?php echo $wdata['WavelengthID'];?>"/>
								</div> 
							<?php if($n !=1) { ?>
								<div class="span3 editMng" style="padding-top:18px;">
									<a class="del" id="<?php echo $wdata['WavelengthID'];?>"><img src="<?php echo $domain; ?>/images/b_drop.png" title="Delete"/></a>
								</div>
							<?php } ?>
							</div><!-- rowend-->
						<?php 
						$n++; 
						} ?>
						<div class="row treatment"  id="textBoxwave" style="display:none;">
							<div class="span3" style="text-align:right;padding-top:16px;"></div>
							<div id="addwave" class="span6 last" style="">	 </div>
						</div>
						<!-- add more btn--->
						<div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;"></div>
							<div id="addwavbt" class="span6 last" style="">
								<button class="btn btn-info" type="button" id="appendWavelengthBtn" name="append">Add More</button>
							</div>
						</div><!-- rowend-->
						<!-- add more btn--->
						<!--Spot size-->
						<?php 
						$s=1; 
						foreach($spotData as $sdata) {	?>
							<input type="hidden" name="spotsizeID[]" value="<?php echo $sdata['spotsizeID'];?>" />
							<input type="hidden" name="DeleteSpot[]"  id="<?php echo $sdata['spotsizeID'];?>" value="" />
							<div class="row treatment" id="hideSpotRow<?php echo $sdata['spotsizeID'];?>">								 
								<div class="span3" style="text-align:right;padding-top:16px;">
									<label class="user-name"> <?php if($s ==1){ ?>Spot Size:<?php } ?></label>
								</div>
								<div class="span6">
									<input type="text" class="deviceclass text-input-field" value="<?php echo $sdata['spotsize'];?>" name="spotsize[]" id="spot<?php echo $sdata['spotsizeID'];?>"/>
								</div> 
								<div class="span3 mmlable" style="width:40px;" >
									<label class="user-name">(mm)</label>
								</div>
								<?php if($s !=1){ ?>
									<div class="span3 editMng" style="padding-top:18px;" >
										<a class="del" id="<?php echo $sdata['spotsizeID'];?>"><img src="<?php echo $domain; ?>/images/b_drop.png" title="Delete"/></a>
									</div>
								<?php }?>
							</div><!-- rowend-->	   
						<?php
							$s++; 
						} ?>
						<div class="row treatment"  id="textBoxSpot" style="display:none;">
							<div class="span3" style="text-align:right;padding-top:16px;"></div>
							<div id="addSpot" class="span6 last" style="">	 </div>
						</div>							   
						<!-- add more btn for spot size--->
						<div class="row treatment">
							<div class="span3" style="text-align:right;padding-top:16px;"></div>
							<div id="addspotSize" class="span6 last" style="">
								<button class="btn btn-info" type="button" id="appendSpotSize" name="append">Add More</button>
							</div>
						</div><!-- rowend-->
						<!-- add more btn for spot size--->						   
						<div class="row treatment">
							<div class="span3"></div>	  
							<div class="span6">
								<!--input type="submit" id="waveSubBtnEdit" value="submit" class="submit-btn" /-->
								<input type="button" class="submit-btn" id="manageDeviceEditBtn" value="Submit" style="cursor:pointer;" onclick=""/>
								<div id="successUpdate" class="msgSucess" style=""></div>
							</div>
						</div><!-- @row end-->
					</form>
				<?php 
				} ?>							
			</div><!--End @user-entry-block-->
		</div><!--End @form-container-->
	<?php
	} // if isset $deviceId  ?>
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
