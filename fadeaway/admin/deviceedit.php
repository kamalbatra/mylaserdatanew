<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$adminForms	= new dbFunctions();
?>
<?php $deviceId  = base64_decode($_GET['deviceId']); ?>
<!-- Edit form Validation-->
<!---- Validation for empty field--->
<script type="text/javascript">
	jQuery(document).ready(function() {		
		jQuery("#manageDeviceEdit").validate({
			rules: {
				DeviceName: "required",              
				DeviceName: {
					required: true,
				},
			},                
			messages: {
				DeviceName: "This field is required.",
			}
		});
		$('#manageDeviceEditBtn').click(function() {
			if( $("#manageDeviceEdit").valid()){
				//alert("ss");
				//var strForm = $( "#manageDeviceEdit" ).serialize();
				var $textboxes = $('input[name="Wavelength[]"]')
				var $textboxesSpot = $('input[name="spotsize[]"]')			
				var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;			
				var arr = $('.deviceclass').map(function(i, e) {
					if(!numberRegex.test(e.value)) {
						//return e.value; 
						//alert("ee:-"+e.id);				 
						$("#"+e.id).css("border","1px solid red");	
						$("#"+e.id).val("");
						$("#"+e.id).focus();
						$("#"+e.id).addClass('placeholderInt');
						$("#"+e.id).attr("placeholder", "Enter numeric value only");			  
						return e.id; 
					} else {					
						$("#"+e.id).css("border","1px solid #ccc");	
					}				 
				}).toArray();
				/*var arrSpot = $('.deviceclassSpot').map(function(i, e) {
					if(!numberRegex.test(e.value)){
						//return e.value; 
						//alert("ee:-"+e.id);				 
						$("#"+e.id).css("border","1px solid red");	
						$("#"+e.id).focus();
						$("#"+e.id).addClass('placeholderInt');
						$("#"+e.id).attr("placeholder", "Enter numeric value only");			  
						return e.id; 
					}else{					
						$("#"+e.id).css("border","1px solid #ccc");	
					}				 
				}).toArray*/			
				//alert(arr.length);	
				if(arr.length ==0) {
					// alert("ok");
					edit_manageDetail('manageDeviceEdit');
				}			
			} else {		
			}
		});
	});
</script>
<script>
	function edit_manageDetail(strVal){
		//alert(strVal);
		//var deviceId=15;	
		var str = $("#manageDeviceEdit").serialize();
		$.ajax({
			type: "POST",
			url: "editajax_manageDetail.php",
			data: str,
			cache: false,
			success: function(result) {
				//location.reload();
				//alert(result);
				$("#successUpdate").html(result);
				$(".successUpdate").css('display','inline-block');
				//~ setTimeout(function() {location.reload()	}, 2000);
			}
		});
	}	
</script>
<!-- --- ADD text and remove text box for Wave length -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendWavelengthBtn").click( function() {
			$("#textBoxwave").show();
			if(counter1>10){
				   // alert("Only 10 textboxes allow");
					return false;	
			}
			
			$("#addwave").append('<div class="half boxx"><a href="#" class="remove_wave btn btn-danger">x</a><div class="form-col-ld"><div class="inputblock-ld"> <input type="text" class="deviceclass text-input-field" name="Wavelength[]" id="wav'+counter1+'"> </div></div></div>');	
			//alert(counter1);
			counter1++;
			return false;
		});	
		$('.remove_wave').live('click', function() {
			if(counter1==1){	
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});	
	});
</script>
<!-- --- ADD text and remove text box for SpotSize -->
<script type="text/javascript">
	jQuery(document).ready( function () {
		var counter1 = 2;
		$("#appendSpotSize").click( function() {
			$("#textBoxSpot").show();
			if(counter1>10) {
				// alert("Only 10 textboxes allow");
				return false;	
			}
		
			$("#addSpot").append('<div class="half boxx"><a href="#" class="remove_spot btn btn-danger">x</a><div class="form-col-ld"><div class="inputblock-ld"><small>(mm)</small><input type="text" class="deviceclass text-input-field" name="spotsize[]" id="spt'+counter1+'"></div></div></div>');
			//alert(counter1);
			counter1++;
			return false;
		});		
		$('.remove_spot').live('click', function() {
			if(counter1==1) {	
				return false;
			}
			counter1--;
			//alert("kk");
			jQuery(this).parent().remove();
			return false;
		});
	});
</script>
<script>
	jQuery(document).ready( function() {
		$(".del").click( function() {	
			var delId = $(this).attr('id');
			$("#"+delId).val(delId);
			var id = $(this).parent().parent().parent().attr('id');
			$("#"+id).css("display", "none");
		});
	});
</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Edit Device</h1>
				</div>	
				
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="devicedetails" class="submit-btn"><button class="addnewbtn">Manage device </button></a>
						</div>
					</div>
					<div class="formcontentblock-ld">
						<?php 
						if(isset($deviceId)) { 					
							//device name
							$tableDevice	= "tbl_devicename";
							$conditionDevice = "where deviceId=".$deviceId." ";
							$colsDevice="*";
							$DeviceData	= $adminForms->selectTableSingleRow($tableDevice,$conditionDevice,$colsDevice);
							//print_r($DeviceData);							  
							//Wavelength 	
							$tbl_ta2_wavelengths	= "tbl_ta2_wavelengths";
							$condition = "where deviceId=".$deviceId." ORDER BY WavelengthID DESC ";
							$cols="*";
							$wavData	= $adminForms->selectTableRows($tbl_ta2_wavelengths,$condition,$cols);									                
							//sopt size
							$tbl_ta2_spot_sizes	= "tbl_ta2_spot_sizes";
							$conditionSpotSize = "where deviceId=".$deviceId." ORDER BY spotsizeID DESC ";
							$colsSpotSize = "*";
							$spotData = $adminForms->selectTableRows($tbl_ta2_spot_sizes,$conditionSpotSize,$colsSpotSize);	
							//Service Name
							$tbl_service = "tbl_master_services";
							$SCond = "where id=".$DeviceData["serviceId"];
							$SCols = "name";
							$SData = $adminForms->selectTableSingleRow($tbl_service,$SCond,$SCols);	
						}	
					?>
						<form action="" method="post" name="manageDeviceEdit" id="manageDeviceEdit">
							<input type="hidden" name="deviceId" value="<?php echo $DeviceData['deviceId'];?>" />
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld radiolabel">
											<label>Service Name</label>
											<input type="text" class="text-input-field" value="<?php echo $SData["name"];?>" name="DeviceName"  disabled/>
										</div>
									</div>
								</div>
							</div>	
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label>Device</label>
											<input type="text" class="text-input-field" value="<?php echo $DeviceData['DeviceName'];?>" name="DeviceName" />
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label>Numeric Validation</label>
											<select class="select-option" name="Required" id="Required">
												<option value="Yes" <?php if($DeviceData['Required']=="Yes"){  echo 'selected=selected' ;}?>>Yes</option>
												<option value="No" <?php if($DeviceData['Required']=="No"){  echo 'selected=selected' ;}?>>No</option>
											</select>	
											<span id="deviceResult" class="mngdevicesName"> </span>										   
										</div>
									</div>
								</div>
							</div>
							<div class="form-row-ld half-container">
							<?php
							$n=1; 
							foreach($wavData as $wdata) {	?>
								<div class="half boxx" id="wavelen<?php echo $wdata['WavelengthID'];?>">
									<input type="hidden" name="WavelengthID[]" value="<?php echo $wdata['WavelengthID'];?>" />
									<input type="hidden" name="DeleteWave[]"  id="<?php echo $wdata['WavelengthID'];?>" value="" />
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label><?php if($n ==1){ ?> Wavelength: <?php }?></label>
											<input type="text" class="deviceclass text-input-field" value="<?php echo $wdata['Wavelength'];?>" name="Wavelength[]" id="wave<?php echo $wdata['WavelengthID'];?>"/>
											<?php if($n !=1) { ?>
												<a class="del btn btn-danger" id="<?php echo $wdata['WavelengthID'];?>">x</a>
												
												<!-- a class="del" id="<?php echo $wdata['WavelengthID'];?>"><img src="<?php echo $domain; ?>/images/b_drop.png" title="Delete"/></a -->
											<?php 
											} ?>	
										</div>
									</div>
								</div>
								<?php 
								$n++; 
							} ?>
							<div id="addwave" class="span6 last" style=""></div>
							</div>
<div class="form-row-ld">
		<div class="form-col-ld">
			<div id="addwavbt" class="span3" style="width: 12%;"> 
				<div id="appendWavelengthBtn" class="addMoreBtn addMoreblk">
					Add More
				</div>
			</div>
		</div>
</div>								
							<div class="form-row-ld half-container">
							<?php 
							$s=1; 
							foreach($spotData as $sdata) {	?>
								<div class="half boxx" id="hideSpotRow<?php echo $sdata['spotsizeID'];?>">
									<input type="hidden" name="spotsizeID[]" value="<?php echo $sdata['spotsizeID'];?>" />
									<input type="hidden" name="DeleteSpot[]"  id="<?php echo $sdata['spotsizeID'];?>" value="" />
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label> <?php if($s ==1){ ?>Spot Size:<?php } ?></label>
											<small>(mm)</small>
											<input type="text" class="deviceclass text-input-field" value="<?php echo $sdata['spotsize'];?>" name="spotsize[]" id="spot<?php echo $sdata['spotsizeID'];?>"/>
											<?php 
											if($s !=1){ ?>
												<a class="del btn btn-danger" id="<?php echo $wdata['WavelengthID'];?>">x</a>
												<!-- a class="del" id="<?php echo $sdata['spotsizeID'];?>"><img src="<?php echo $domain; ?>/images/b_drop.png" title="Delete"/></a -->
											<?php 
											}	?>
										</div>
									</div>
								</div>
								
							<?php
								$s++; 
							} 
							?>
							<div id="addSpot" class="span6 last" style=""></div>
							</div>
<div class="form-row-ld">
		<div class="form-col-ld">
			<div id="addspotSize" class="span3" style="width: 12%;"> 
				<div id="appendSpotSize" class="addMoreBtn addMoreblk">
					Add More
				</div>
			</div>
		</div>
</div>							
							<div class="form-row-ld">
								<div class="backNextbtn">
									<button type="button" id="manageDeviceEditBtn" value="submit" class="submit-btn nextbtn" style="float:left;cursor:pointer;">Submit</button>
									<div class="successUpdate" id="successUpdate"></div>
								</div>
							</div>
						</form>

					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
