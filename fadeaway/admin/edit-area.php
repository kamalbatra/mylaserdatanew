<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$editArea = new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	if(isset($_GET['id']) && $_GET['id']!= NULL){	

		$tbl_category = "tbl_categories";
		$catcondition = "WHERE businessID = $_SESSION[BusinessID] AND status = 1 ORDER by categoryName ASC";
		$cols = "*";
		$categoryData = $editArea->selectTableRowsNew($tbl_category,$catcondition);

		$tbl_services = "tbl_services";
		$condition = "WHERE businessID = $_SESSION[BusinessID] AND status = 1 ORDER BY serviceName ASC";
		$cols = "*";
		$serviceData = $editArea->selectTableRowsNew($tbl_services,$condition);	
		
		$areaID = $_GET['id'];
		$table	= "tbl_areas";
		$condition = " Where areaID = ".$areaID." AND businessID = $_SESSION[BusinessID]";
		$cols = "*";
		$editArea = $editArea->selectTableSingleRowNew($table,$condition,$cols);
?>	
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
		<style>
			.right-margin-6 { margin-right: 6%; }
			.menu-checkbox{ float: left; width: 25%; }
			.addNewReport { float: right; }
			.formdonly {display:none;}
		</style>
		<script>
			jQuery(document).ready(function(){
				jQuery("#editArea").validate({
					errorClass: 'errorblocks',
					errorElement: 'div',
					rules: {
						areaName: {
							required: true,
						},
						seviceID: {
							required: true,
						},
						price: {
							required: true,
						},
						status: {
							required: true,
						}
					},	
					messages: {
						areaName: {
							required: "Please enter area name.",
						},
						seviceID: {
							required: "Please select a service.",
						},
						price: {
							required: "Please enter price for area.",
						},
						status: {
							required: "Please select a status.",
						}
					},
					submitHandler: function(form){
						$('.loadingOuter').show();
						var str = $("#editArea").serialize();
						$.ajax({
							type: "POST",
							url: "ajax_newform.php",
							data: str,
							cache: false,
							success: function(result){
								if(result == 1){
									$("#insertResult").show();
									$("#insertResult").html("<span style='color:green;'>Information updated successfully.</span>");
									setTimeout(function() {
										location.href = 'areas';
									}, 3000);
								}
							}
						}); 
					}
				});
				
				$('#seviceID').change(function(){
					$('.loadingOuter').show();
					var str = $(this).val();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: 'id='+str+'&formname=fetchcategory',
						cache: false,
						success: function(result){
							$('.loadingOuter').hide();
							var res = JSON.parse(result);
							$('#categoryID1').html('<option value="'+res.id+'">'+res.categoryName+'</option>');
							$('#categoryID').val(res.id);
						}
					}); 
				});
									
			});
		</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Edit Area</h1>
					</div>
					<div class="card shadow mb-4 table-main-con">
						<div class="bussiness-searchblock no-searchbox">
							<div class="search-btn">
								<a class="empLinks" href="categories" class="submit-btn"><button class="addnewbtn">Areas List</button></a>
							</div>
						</div>
						<div class="formcontentblock-ld">	
					<form action="" name="editArea" id="editArea" method="post">
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Area Name:</label>
													<input class="text-input-field" type="text" name="areaName" id="areaName" value="<?php echo $editArea['areaName']; ?>"/>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Service Name:</label>
													<select name="seviceID" id="seviceID" class="select-option">
														<option value="">Select a Sercvice</option>
														<?php 
														foreach( $serviceData as $servData ) {
														?>
															<option value="<?php echo $servData['seviceID']; ?>" <?php if($servData['seviceID'] == $editArea['seviceID'] ){ echo 'selected'; } ?>><?php echo ucfirst($servData['serviceName']); ?></option>
														<?php	
														}
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Category Name:</label>
													<select name="categoryID1" id="categoryID1" class="select-option" disabled>
														<option value="">Select a Category</option>
														<?php 
														foreach( $categoryData as $catData ) {
														?>
															<option value="<?php echo $catData['id']; ?>" <?php if($catData['id'] == $editArea['categoryID'] ){ echo 'selected'; } ?>><?php echo ucfirst($catData['categoryName']); ?></option>
														<?php	
														}
														?>
													</select>						
													<input type="hidden" name="categoryID" id="categoryID" value="<?php echo $editArea['categoryID']; ?>">
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Price<span class='signvalue'>(in $)</span>:</label>
													<input class="text-input-field" type="text" name="price" id="price" value="<?php echo $editArea['price']; ?>"/>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Time Duration<span class='signvalue'>(in mins)</span>:</label>
													<input class="text-input-field" type="text" name="durationTime" id="durationTime" value="<?php echo $editArea['durationTime']; ?>"/>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Status:</label>
													<select name="status" id="status" class="select-option">
														<option value="">Select a status</option>
														<option value="1" <?php if( $editArea['status'] == 1 ){ echo 'selected'; } ?> >Active</option>
														<option value="0" <?php if( $editArea['status'] == 0 ){ echo 'selected'; } ?> >De-Active</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="areaID" value="<?php echo $editArea['areaID']; ?>"/>
									<input type="hidden" name="businessID" value="<?php echo $editArea['businessID']; ?>"/>
									<input type="hidden" name="dateAdded" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
									<input type="hidden" name="formname" value="editarea"/>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="submit"  id="submitForm" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
											<div id="insertResult" class="u_mess" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				
						</div>
					</div>
				
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php
	}
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
