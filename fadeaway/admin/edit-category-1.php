<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$editCat = new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"]) ) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	if(isset($_GET['id']) && $_GET['id']!= NULL) {	
		$empId = $_GET['id'];
		$table	= "tbl_categories";
		$condition = " Where id = ".$empId." AND businessID = $_SESSION[BusinessID] ";
		$cols = "*";
		$editcat = $editCat->selectTableSingleRowNew($table,$condition,$cols);
	}
?>	
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<style>
		.right-margin-6 { margin-right: 6%; }
		.menu-checkbox{ float: left; width: 25%; }
		.addNewReport { float: right; }
		.formdonly {display:none;}
	</style>
	<script>
		jQuery(document).ready(function(){
			jQuery("#editcategory").validate({
				rules: {
					categoryName: {
						required: true,
					},
					status: {
						required: true,
					}
				},	
				messages: {
					categoryName: {
						required: "Please enter category name",
					},
					status: {
						required: "Please select a status.",
					}
				},
				submitHandler: function(form){
					$('.loadingOuter').show();
					var str = $("#editcategory" ).serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								$("#insertResult").show();
								$("#insertResult").html("<span style='color:green;'>Category updated successfully.</span>");
								setTimeout(function() {
									location.href = 'categories';
								}, 3000);
							}
						}
					}); 
				}
			});					
		});
	</script>
	<div class="form-container">
		<div class="loadingOuter"><img src="../images/loader.svg"></div>
		<div class="heading-container">
			<h1 class="heading empHeadReport">Edit Category</h1>
			<div class="addNewReport"><a class="empLinks" href="categories" class="submit-btn">All Categories</a></div>
		</div>
		<div class="user-entry-block fix-error">
			<form action="" name="editcategory" id="editcategory" method="post">	
				<div class="row">
					<input type="hidden" name="id" value="<?php echo $editcat['id']; ?>"/>
					<input type="hidden" name="businessID" value="<?php echo $editcat['businessID']; ?>"/>
					<input type="hidden" name="dateAdded" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
					<input type="hidden" name="formname" value="editcategory"/>
					<div class="span6 right-margin-6">
						<label id="Label1" class="user-name">Category Name:</label>
						<input class="text-input-field" type="text" name="categoryName" id="categoryName" value="<?php echo $editcat['categoryName']; ?>"/>
					</div>
					<div class="span6 right-margin-6">
						<label id="Label1" class="user-name">Status:</label>
						<select name="status" id="status" class="select-option">
							<option value="">Select a status</option>
							<option value="1" <?php if($editcat['status'] == 1 ){ echo 'selected'; } ?> >Active</option>
							<option value="0" <?php if($editcat['status'] == 0 ){ echo 'selected'; } ?>>De-Active</option>
						</select>
					</div>
				</div><!--End @row-->		
				<div class="row">
					<div class="span12">
						<input type="submit" id="submitForm" value="Submit" class="submit-btn" style="float:left;">
						<div id="insertResult" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
					</div>			
				</div><!--End @row-->
			</form>
		</div>
	</div>
</div>
<?php include('admin_includes/footer.php'); ?>
