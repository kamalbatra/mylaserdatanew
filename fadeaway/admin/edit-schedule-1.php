<?php
	include('admin_includes/header.php');
?>
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $domain; ?>/css/jquery.timepicker.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo $domain; ?>/js/jquery.timepicker.min.js"></script>
<?php	

	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();

	$table = "tbl_employees";
	$condition = " where Emp_ID=".base64_decode($_GET['empid'])." ";
	$cols = "dailySchdeule";
	$editinfo = $empInfo->selectTableSingleRowNew($table,$condition,$cols);
	$editinfo1 = unserialize($editinfo['dailySchdeule']);
	
?>	
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#schedule").validate({
			ignore: [],
			rules: {                 
                "hourfrom[]":{
					required: true,
				},                                
                "hourto[]":{
					required: true,
				},                                
			},                
			messages: {                    
                "hourfrom[]":{
					required: "Please enter start time.",
				},                                
                "hourto[]":{
					required: "Please enter end time.",
				},                                
			},
			
			submitHandler: function(form){
				var str = $("#schedule").serialize();
				$.ajax({
					type: "POST",
					url: "ajax_newform.php",
					data: str,
					cache: false,
					success: function(result){
						if(result == 1){
							//~ $("#insertResult").show();
							//~ $("#insertResult").html("<span style='color:green;'>Information updated successfully.</span>");
							setTimeout(function() {
								var pagename = "<?php echo $_GET['empid']; ?>";
								location.href = "schedule?empid="+pagename;
							}, 3000);
						}
					}
				}); 
			}
			
		});	   

		$(".switch input").on('change', function() {
			if ($(this).attr('checked')) {
				$(this).val('1');
				$(this).parent().parent().find("input.hiddenday").val(1);
			} else {
				$(this).val('0');
				$(this).parent().parent().find("input.hiddenday").val(0);
			}
		});

		$('.timepicker').timepicker({
			minTime: '9:00',
			maxTime: '18:00',
			showDuration: true,
			dropdown: true,
			scrollbar: true
		});

	});
	
</script>
<div class="form-container Schedule-outer-block">
	<div class="heading-container">
		<h1 class="heading empHead">Edit Schedule</h1>
		<div class="addNew">
			<a class="empLinks" href="empdetails" class="submit-btn">Employee list </a>
		</div>
	</div>	
	<div id="insertResult" class="insertResult"></div>
	<div class="user-entry-block">

		<form action="" name="schedule" id="schedule" method="post">
			<input type="hidden" name="formname" value="scheduleadd">
			<input type="hidden" name="empid" value="<?php echo base64_decode($_GET['empid']); ?>">
			<?php 
				for( $i = 0; $i < 7; $i++ ){
					$schedulefrag = explode('-',$editinfo1[$i]);
					//~ echo $schedulefrag[0]."--".$schedulefrag[1]."--".$schedulefrag[2];
			?>	
					<div class="row">
						<div class="row calrow">
							<div class="dayname-container">
								<label id="Label1" class="user-name"><b><?php echo $empInfo->daysname($i); ?></b></label>
								<label class="switch">
									<input class="switch" name="day[]" value="0" type="checkbox" <?php if( isset($schedulefrag[0]) && $schedulefrag[0] == 1 ){ echo 'checked'; }?> >
									<span class="slider round"></span>
								</label>
								<input name="day1[]" value="<?php if( isset($schedulefrag[0]) && $schedulefrag[0] == 1 ){ echo 1; } else { echo 0; } ?>" class="hiddenday" type="hidden">	
							</div>	
							<div class="operatinghour-container">
								<label id="Label1" class="user-name">Operating Hours:</label>
								<div class="rowblock-ToFrom row-operate">
									<div class="operatinghour-from">
										<label class="user-name">From: </label>
										<input class="text-input-field timepicker" type="text" name="hourfrom[]" value="<?php if(isset($schedulefrag[1]) && $schedulefrag[1] != ''){ echo $schedulefrag[1]; } else { echo ''; }?>"/>
									</div>
									<div class="operatinghour-to">
										<label class="user-name">To: </label>
										<input class="text-input-field timepicker" type="text" name="hourto[]" value="<?php if(isset($schedulefrag[2]) && $schedulefrag[2] != ''){ echo$schedulefrag[2]; } else { echo ''; } ?>"/>	
									</div>
								</div>
							</div>		
						</div>
						
					</div>	<!-- @end of row -->
			<?php
				}
			?>
			<div class="row">
				<div class="span12">				
     				<input type="submit" id="changepwdBtn" value="submit" class="submit-btn"><br/>
					<div id="pwdResult" style="display:none;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
				</div>
			</div><!--End @row-->
		</form>
	</div>
</div>
</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
