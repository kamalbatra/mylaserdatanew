<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
?>
	<!-- link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css"/ -->
	<link href="<?php echo $domain; ?>/css/jquery.timepicker.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo $domain; ?>/js/jquery.timepicker.min.js"></script>
<?php	
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();

	$table = "tbl_employees";
	$condition = " where Emp_ID=".base64_decode($_GET['empid'])." ";
	$cols = "dailySchdeule";
	$editinfo = $empInfo->selectTableSingleRowNew($table,$condition,$cols);
	$editinfo1 = unserialize($editinfo['dailySchdeule']);
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#schedule").validate({
			errorClass: 'errorblocks',
			errorElement: 'div',
			ignore: [],
			rules: {                 
                "hourfrom[]":{
					required: true,
				},                                
                "hourto[]":{
					required: true,
				},                                
			},                
			messages: {                    
                "hourfrom[]":{
					required: "Please enter start time.",
				},                                
                "hourto[]":{
					required: "Please enter end time.",
				},                                
			},
			
			submitHandler: function(form){
				var str = $("#schedule").serialize();
				$.ajax({
					type: "POST",
					url: "ajax_newform.php",
					data: str,
					cache: false,
					success: function(result){
						if(result == 1){
							//~ $("#insertResult").show();
							//~ $("#insertResult").html("<span style='color:green;'>Information updated successfully.</span>");
							setTimeout(function() {
								var pagename = "<?php echo $_GET['empid']; ?>";
								location.href = "schedule?empid="+pagename;
							}, 3000);
						}
					}
				}); 
			}
			
		});	   

		$(".switch input").on('change', function() {
			if ($(this).attr('checked')) {
				$(this).val('1');
				$(this).parent().parent().find("input.hiddenday").val(1);
			} else {
				$(this).val('0');
				$(this).parent().parent().find("input.hiddenday").val(0);
			}
		});

		$('.timepicker').timepicker({
			minTime: '9:00',
			maxTime: '18:00',
			showDuration: true,
			dropdown: true,
			scrollbar: true
		});

	});
	
</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Edit Schedule</h1>
				</div>	

				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="empdetails" class="submit-btn"><button class="addnewbtn">Employee List </button></a>
						</div>
					</div>
					<div class="editSchedule">
						<form action="" name="schedule" id="schedule" method="post">
							
							<div class="HoursBlock">
								<input type="hidden" name="formname" value="scheduleadd">
								<input type="hidden" name="empid" value="<?php echo base64_decode($_GET['empid']); ?>">
								<?php 
									for( $i = 0; $i < 7; $i++ ){
										$schedulefrag = explode('-',$editinfo1[$i]);
										//~ echo $schedulefrag[0]."--".$schedulefrag[1]."--".$schedulefrag[2];
								?>	
										<div class="HoursHalf">
											<div class="hoursInner">
												<div class="HoursMain">
													<span  id="Label1" class="day"><?php echo $empInfo->daysname($i); ?></span>
													<label class="switch">
														<input class="switch" name="day[]" value="0" type="checkbox" <?php if( isset($schedulefrag[0]) && $schedulefrag[0] == 1 ){ echo 'checked'; }?> >
														<span class="slider round"></span>
													</label>
													<input name="day1[]" value="<?php if( isset($schedulefrag[0]) && $schedulefrag[0] == 1 ){ echo 1; } else { echo 0; } ?>" class="hiddenday" type="hidden">	
												</div>
												<h2 id="Label1" class="user-name">Operating Hours:</h2>
												<div class="form-row-ld">
													
													<div class="rowblock-ToFrom row-operate">
														<div class="operatinghour-from half">
															<div class="form-col-ld">
																<div class="inputblock-ld">
																	<label class="user-name">From: </label>
																	<input class="text-input-field timepicker" type="text" name="hourfrom[]" value="<?php if(isset($schedulefrag[1]) && $schedulefrag[1] != ''){ echo $schedulefrag[1]; } else { echo ''; }?>"/>
																</div>
															</div>
														</div>
														<div class="operatinghour-to half">
															<div class="form-col-ld">
																<div class="inputblock-ld">
																	<label class="user-name">To: </label>
																	<input class="text-input-field timepicker" type="text" name="hourto[]" value="<?php if(isset($schedulefrag[2]) && $schedulefrag[2] != ''){ echo$schedulefrag[2]; } else { echo ''; } ?>"/>
																</div>
															</div>
														</div>
													</div>
												</div>		
											</div>
											
										</div>	<!-- @end of row -->
								<?php
									}
								?>
							</div><!--End @row-->
							<div class="HoursBlock">	
								<div class="HoursHalf">
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="submit" id="changepwdBtn" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
											<div id="pwdResult" style="display:none;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
										</div>
									</div>
								</div><!--End @row-->
							</div><!--End @row-->
			
						</form>                  
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
