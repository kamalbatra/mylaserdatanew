<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$editService = new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	if(isset($_GET['id']) && $_GET['id']!= NULL){	
		$tbl_category = "tbl_categories";
		$catcondition = "WHERE businessID = $_SESSION[BusinessID] AND status = 1 ORDER by categoryName ASC";
		$cols = "*";
		$categoryData = $editService->selectTableRowsNew($tbl_category,$catcondition);
		$tbl_location = "tbl_manage_location";
		$condition = "WHERE businessID = $_SESSION[BusinessID] ORDER by LocationName ASC";
		$cols = "*";
		$locationData = $editService->selectTableRowsNew($tbl_location,$condition);
		$seviceID = $_GET['id'];
		$table	= "tbl_services";
		$condition = " Where seviceID = ".$seviceID." AND businessID = $_SESSION[BusinessID]";
		$cols = "*";
		$editService = $editService->selectTableSingleRowNew($table,$condition,$cols);

?>
<script>
	jQuery(document).ready(function(){
		jQuery("#editservice").validate({
		errorClass: 'errorblocks',
		errorElement: 'div',
			rules: {
				serviceName: {
					required: true,
				},
				categoryID: {
					required: true,
				},
				locationID: {
					required: true,
				},
				bufferTime: {
					required: true,
					number:true,
				},
				status: {
					required: true,
				}
			},	
			messages: {
				serviceName: {
					required: "Please enter service name.",
				},
				categoryID: {
					required: "Please select a category.",
				},
				locationID: {
					required: "Please select a location.",
				},
				bufferTime: {
					required: "Please enter a buffer time.",
					number: "Please enter time in numbers.",
				},
				status: {
					required: "Please select a status.",
				}
			},
			submitHandler: function(form){
				$('.loadingOuter').show();					
				var str = $("#editservice" ).serialize();
				$.ajax({
					type: "POST",
					url: "ajax_newform.php",
					data: str,
					cache: false,
					success: function(result){
						if(result == 1){
							$("#insertResult").show();
							$("#insertResult").html("<span style='color:green;'>Service Updated Successfully.</span>");
							setTimeout(function() {
								location.href = 'services';
							}, 1000);
						}
					}
				}); 
			}
		});					
	});
</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Edit Service</h1>
					</div>
					<div class="card shadow mb-4 table-main-con">
						<div class="bussiness-searchblock no-searchbox">
							<div class="search-btn">
								<a class="empLinks" href="services" class="submit-btn"><button class="addnewbtn">All Services</button></a>
							</div>
						</div>
						<div class="formcontentblock-ld">
								
					<form action="" name="editservice" id="editservice" method="post">
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Service Name:</label>
													<input class="text-input-field" type="text" name="serviceName" id="serviceName" value="<?php echo $editService['serviceName']; ?>"/>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Category Name:</label>
													<select name="categoryID" id="categoryID" class="select-option">
														<option value="">Select a Category</option>
														<?php 
														foreach( $categoryData as $catData ) {
														?>
															<option value="<?php echo $catData['id']; ?>" <?php if($editService['categoryID'] == $catData['id'] ){ echo 'selected'; } ?>><?php echo ucfirst($catData['categoryName']); ?></option>
														<?php	
														}
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Location Name:</label>
													<select name="locationID" id="locationID" class="select-option">
														<option value="">Select a Location</option>
														<?php 
														foreach( $locationData as $locData ) {
														?>
															<option value="<?php echo $locData['ManageLocationId']; ?>" <?php if($editService['locationID'] == $locData['ManageLocationId'] ){ echo 'selected'; } ?> ><?php echo ucfirst($locData['LocationName']); ?></option>
														<?php	
														}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Buffer Time<span class='signvalue'>(in mins)</span>:</label>
													<input class="text-input-field" type="text" name="bufferTime" id="bufferTime" value="<?php echo $editService['bufferTime']; ?>"/>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Status:</label>
													<select name="status" id="status" class="select-option">
														<option value="">Select a status</option>
														<option value="1" <?php if($editService['status'] == 1 ){ echo 'selected'; } ?> >Active</option>
														<option value="0" <?php if($editService['status'] == 0 ){ echo 'selected'; } ?> >De-Active</option>
													</select>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<div class="radioblocksBtns">
														<div class="radioBtn ckeckboxres">		
															<input class="service-check" type="checkbox" name="agreecheckbox" id="agreecheckbox" <?php if ($editService['isArea'] == 1) { echo "checked='checked'"; } ?>>
															<label for="agreecheckbox" class="user-name service-check-label">No area required.</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="seviceID" value="<?php echo $editService['seviceID']; ?>"/>
									<input type="hidden" name="businessID" value="<?php echo $editService['businessID']; ?>"/>
									<input type="hidden" name="dateAdded" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
									<input type="hidden" name="formname" value="editservice"/>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="submit"  id="submitForm" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
											<div id="insertResult" class="u_mess" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				
					</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php
		}
		include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php'); 
	?>
