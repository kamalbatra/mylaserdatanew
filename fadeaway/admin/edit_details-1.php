<?php
error_reporting(0);
//include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$consentForms		= new dbFunctions();
 $clientid= preg_replace("/[^0-9,.]/", "", $_POST['id']);
$table			= "tbl_clients";
$condition = " where ClientID=".$clientid." ";
$cols=" ClientID,AES_DECRYPT(FirstName, '".SALT."') AS FirstName,AES_DECRYPT(LastName, '".SALT."') AS LastName,AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber,AES_DECRYPT(Occupation, '".SALT."') AS Occupation,AES_DECRYPT(Age, '".SALT."') AS Age,AES_DECRYPT(Address1, '".SALT."') AS Address1,AES_DECRYPT(Address2, '".SALT."') AS Address2,AES_DECRYPT(City, '".SALT."') AS City,AES_DECRYPT(State, '".SALT."') AS State,AES_DECRYPT(Zip, '".SALT."') AS Zip,AES_DECRYPT(Email, '".SALT."') AS Email, ReferralSource, ReferralSourceName ";
$editinfo = $consentForms->selectTableSingleRow($table,$condition,$cols);

$refferalChoices = new dbFunctions();

$table	= "tbl_refferal_choices";
$choices = $refferalChoices->selectTableRows($table);

$domain=$_SERVER['DOMAIN'];
?>

<script type="text/javascript" src="<?php echo $domain; ?>/js/jquery-1.9.1.js" ></script>
<script type="text/javascript" src="<?php echo $domain; ?>/js/jquery.validate.min.js" ></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	
	jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
	phone_number = phone_number.replace(/\s+/g, "");
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");
	
	jQuery("#edit_details").validate({
		rules: {
                FirstName: "required",
                LastName:"required",
                Age:"required",
                Occupation:"required",
                Address1:"required",
                City:"required",
                State:"required",
                Zip:"required",
                PhoneNumber:"required",
                ReferralSource:"required",
                Email:"required",
                
                  Email: {
					required: true,
					email: true,
					},
					Age: {
						required: true,
						number:true
					},
					Zip: {
					required: true,
					//number:true
					},
					PhoneNumber: {
					required: true,
					phoneUS: true
					},
                  },
                
                messages: {
                    FirstName: "Please enter First Name.",
                    LastName:"Please enter Last Name.",
                    Occupation:"Please enter Occupation.",
                    Address1:"Please enter Address1.",
                    City:"Please enter City.",
                    State:"Please enter State.",
                    PhoneNumber:"Please enter Phone Number.",
                     ReferralSource:"Please select an option.",
                    Email: {
						required: "Please enter Email." ,
						email: "Please enter valid email address.",
					},
					Age: {
						required: "Please enter Age.",
						number: "Please enter a valid Age."
					},
					Zip: {
						required: "Please enter Zip Code.",
					//	number: "Please enter a valid zip code."
					},
					PhoneNumber: {
						required: "Please enter Phone Number.",
						number: "Please enter a valid Phone Number.",
						minlength: "Please enter ten digit Phone Number.",
						maxlength: "Please enter ten digit Phone Number."
					},
                  }


	});
	
	$('#editClientBtn').click(function() {
		if( $("#edit_details").valid()){
			edit_form('<?php echo $editinfo['ClientID']; ?>');
			$("#u_mess").show();
		}else{
			$("#u_mess").hide();		
		}
        
    });
    $('#ReferralSource').change(function() {
			 $('.friendShopName').hide();
		 	if($(this).val()=='Friend')
		 	{
				$('.friendShopName label').html('Friend Name');
			  $('.friendShopName').show();
			  $('#ReferralSourceName').val('');
			}
			
		 	if($(this).val()=='Tattoo Artist')
		 	{
				$('.friendShopName label').html('Name of Shop');
			  $('.friendShopName').show();
			  $('#ReferralSourceName').val('');
			}
			
		 	if($(this).val()=='Other')
		 	{
				$('.friendShopName label').html('');
			  $('.friendShopName').show();
			  $('#ReferralSourceName').val('');
			}
			
		});
    
});
</script>
<div class="form-container" style="border:none;">
		<h1 class="heading">Edit Client Information</h1>
		<form action="" method="POST" id="edit_details" name="edit_details">
			<?php if(isset($_GET['entry']) && $_GET['entry']=='fails'){ echo "<p style='color: #FF0000;font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 13px;margin-bottom:13px;margin-top:-12px;'>"."This Email Id Already Exists !"."</p>"; } ?>
		<div class="user-entry-block">
		
			<div class="row">
				<div class="span6">
					<label for="FirstName" class="user-name">First Name:</label>
					<input class="text-input-field" name="FirstName" id="FirstName" type="text" value="<?php echo ucfirst($editinfo['FirstName']);  ?>" autocomplete="off" />
					<input type="hidden" name="clientid" value="<?php echo $editinfo['ClientID']; ?>">
				</div>
				
				<div class="span3">
					<label for="LastName" class="user-name">Last Name:</label>
					<input class="text-input-field" name="LastName" id="LastName" type="text" value="<?php echo ucfirst($editinfo['LastName']);  ?>" autocomplete="off" />
				</div>

			</div><!--End @row-->
			
			<div class="row">
				<div class="span3">
					<label for="Age" class="user-name">Age:</label>
					<input class="text-input-field" name="Age" id="Age" type="text" value="<?php echo $editinfo['Age']; ?>" autocomplete="off" />
				</div>
				
				
									

			</div><!--End @row-->
			
			<div class="row">
			<div class="span6">
					<label for="Occupation" class="user-name">Occupation:</label>
					<input class="text-input-field" name="Occupation" id="Occupation" type="text" value="<?php echo $editinfo['Occupation']; ?>" autocomplete="off" />
				</div>
				
									

			</div><!--End @row-->
			
			<div class="row">
				<div class="span6">
					<label for="Address1" class="user-name">Address1:</label>
					<textarea name="Address1" id="Address1" cols="20" rows="4" class="text-area-field" value=""> <?php echo trim($editinfo['Address1']); ?></textarea>
				</div>
				
				<div class="span6 last">
					<label for="Address2" class="user-name">Address2:</label>
					<textarea name="Address2" id="Address2" cols="20" rows="4" class="text-area-field" value=""><?php echo trim($editinfo['Address2']); ?> </textarea>
				</div>

			</div><!--End @row-->
			
			<div class="row">
				<div class="span6">
					<label for="City" class="user-name">City:</label>
			<input class="text-input-field" name="City" id="City" type="text" value="<?php echo $editinfo['City']; ?>" autocomplete="off" />
					</div>
				
				<div class="span3 last">
					<label for="State" class="user-name">State:</label>
		<input class="text-input-field" name="State" id="State" type="text" value="<?php echo $editinfo['State']; ?>" autocomplete="off" />
				</div>

			</div><!--End @row-->
			
			<div class="row">
				<div class="span6">
					<label for="Zip" class="user-name">Zip:</label>
					<input class="text-input-field" name="Zip" id="Zip" type="text" value="<?php echo $editinfo['Zip']; ?>" autocomplete="off" />
				</div>
				
				<div class="span3 last">
					<label for="PhoneNumber" class="user-name">Phone Number:</label>
					<input class="text-input-field" name="PhoneNumber" id="PhoneNumber" type="text" value="<?php echo $editinfo['PhoneNumber']; ?>" autocomplete="off" />
				</div>

			</div><!--End @row-->
			
			<div class="row">
				<div class="span6">
					<label for="Email" class="user-name">Email:</label>
					<input class="text-input-field" name="Email" id="Email" type="text" value="<?php echo $editinfo['Email']; ?>" autocomplete="off" />
				</div>
			</div><!--End @row-->
			
			<div class="row">
			<div class="span6">
					<label for="Email" class="user-name">How did you hear about us?:</label>
					<select class="text-input-field select-option" name="ReferralSource" id="ReferralSource">
					<option value="">Select an option</option>
					<?php
					//$refference=mysql_fetch_array($que)
						foreach($choices as $refferal){
							$val="";
						if($refferal['ReferralID']==1)
						{
						 $val="disabled";
						}
						$select='';
						if(($refferal['Referrer']==$editinfo['ReferralSource']) && ($editinfo['ReferralSource']!='Online Search')){
						 $select="selected";
						}	
						if($refferal['parent']==0)
						{
					?>
					
					<option <?php echo $val; echo $select;  ?> value="<?=$refferal['Referrer'];?>">
					<?=$refferal['Referrer'];?>
					</option>
					
					<?php
					}
					foreach($choices as $ref)
							{
							$select='';
						if($ref['Referrer']==$editinfo['ReferralSource']){
						 $select="selected";
						}	
						if($refferal['ReferralID']==$ref['parent'])
								{ ?>
								<option <?php echo $select; ?> value="<?=$ref['Referrer'];?>">
									--<?=$ref['Referrer'];?>
								</option>
								<?php }
						} 
					}
					?>
					</select>
					
					
				</div>
				<?php if($editinfo['ReferralSource']=='Friend') {
					$style="display:block";
					$label="Friend name :";
					  } else if(($editinfo['ReferralSource']=='Tattoo Artist')) { 
						$label="Name of shop :";
						  $style="display:block";
					  } else if(($editinfo['ReferralSource']=='Other')) { 
						$label="";
						  $style="display:block";
						  }else
						  {
							  $label="";
							  $style="display:none";
							  }?>
			
				<div class="span3 last friendShopName" style="<?php echo $style; ?>">
					<label class="user-name friendShopNamelevel" for="PhoneNumber"><?php echo $label; ?></label>
					<input id="ReferralSourceName" class="text-input-field" type="text" autocomplete="off" name="ReferralSourceName" value="<?php echo $editinfo['ReferralSourceName']; ?>">
				</div>
			</div><!--End @row-->	
			
			
			<input type="hidden" name="Consent" id="Consent" value="FALSE"/>
			<input type="hidden" name="Minneapolis" id="Minneapolis" value="<?=$_SESSION['Emp_Location_Minneapolis']?>"/>
			<input type="hidden" name="St_Paul" id="St_Paul" value="<?=$_SESSION['Emp_Location_St_Paul']?>"/>
			<input type="hidden" name="Duluth" id="Duluth" value="<?=$_SESSION['Emp_Location_Duluth']?>"/>
			<input type="hidden" name="Emp_Name" id="Emp_Name" value="<?=$_SESSION['First_Name']?>"/>
			<input type="hidden" name="Emp_Last_Name" id="Emp_Last_Name" value="<?=$_SESSION['Last_Name']?>"/>
			<input type="hidden" name="TimeStamp" id="TimeStamp" value="<?=date('Y-m-d H:i:s'); ?>"/>		
			<div class="row">
				<div class="span12">
				<input type="button" class="submit-btn u_p" id="editClientBtn" value="Update" onclick=""/>	<div class="u_mess" id="u_mess"></div>	</div>			
			</div><!--End @row-->		
		</div><!--End @user-entry-block-->
		</form>
	</div><!--End @form-container-->
	</div><!--End @container-->
	<?php
/*echo "ClientID :- ".$sizedata['ClientID']."<br/>";
echo "Age :- ".$sizedata['Age']."<br/>";
echo "Occupation :- ".$sizedata['Occupation']."<br/>";
echo "PhoneNumber :- ".$sizedata['PhoneNumber']."<a onclick='aa(".$sizedata['ClientID'].")' class='edit-btn' href='#'>Edit Details</a>";
*/?>
<!-- form action="process.php" method="post" name="clientformsearch">
<input type="hidden" name="clientid" id="clientid" value="<?php echo $sizedata['ClientID']; ?>"/>
<input type="hidden" name="clientfname" id="clientfname" value="<?php echo $sizedata['FirstName']; ?>"/>
<input type="hidden" name="clientlname" id="clientlname" value="<?php echo $sizedata['LastName']; ?>"/>
</form-->
