<?php
	error_reporting(0);

	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$obj_user_profile = new dbFunctions();
	/***** Device Edit ****/
	if(isset($_POST['DeviceName']) && $_POST['DeviceName']!= NULL){	
		//print_r($_POST);
		$tbldevicename = "tbl_devicename";
		$condition = "Where DeviceName 	='".$_POST['DeviceName']."' AND deviceId ='".$_POST['deviceId']."' ";			
		$deviceName = $obj_user_profile->selectTableRows($tbldevicename,$condition);
		if(isset($deviceName [0]['DeviceName'])){
			$deviceData1['deviceId'] = $_POST['deviceId'];
			$deviceData1['Required'] = $_POST['Required'];
			$obj_user_profile->update_user($tbldevicename,$deviceData1);
			//echo $msg = "<span style='color:green;font-size:14px;'>Information updated successfully.</span>"; 
		} else {
			$condition = "Where DeviceName 	='".$_POST['DeviceName']."'";			
			$deviceName = $obj_user_profile->selectNumRows($tbldevicename,$condition);
			if($deviceName ==1) {
				//echo $msg = "<span style='color:red;font-size:14px;'> Device name already exist!</span>"; 
			} else {					
				$deviceData['deviceId'] = $_POST['deviceId'];
				$deviceData['DeviceName'] = $_POST['DeviceName'];
				$deviceData['Required'] = $_POST['Required'];					
				$obj_user_profile->update_user($tbldevicename,$deviceData);
				//echo $msg = "<span style='color:green;font-size:14px;'>Information updated successfully.</span>"; 
			}		
		}	
	}
	//Wavelength edit 
	if(isset($_POST['Wavelength']) && $_POST['Wavelength'] !=NULL){		
		$arrWave = $_POST['Wavelength'];
		if(isset($_POST['WavelengthID'])){
			$arrWaveId = $_POST['WavelengthID'];
		} else {
			$arrWaveId ="";
		}	 
		for($i = 0; $i < count($arrWave);$i++){	
			$_POST['Wavelength']= $arrWave[$i];		 
			if(isset($arrWaveId[$i])) {
				$_POST['WavelengthID'] = $arrWaveId[$i];
			} else {
				$_POST['WavelengthID'] ="";
			}		 
			if (is_numeric($_POST['Wavelength'])) {			
				$tbl_ta2_wavelengths = "tbl_ta2_wavelengths";
				$condition = "Where Wavelength 	='".$_POST['Wavelength']."' AND deviceId  	='".$_POST['deviceId']."' ";
				$cols="Wavelength";
				$waveName = $obj_user_profile->selectNumRows($tbl_ta2_wavelengths,$condition);
				//echo "<br>";
				//echo "Selectnum:-".$waveName;	
				if($waveName ==1) {
				} elseif($waveName ==0 && $_POST['WavelengthID']!="") {
					$data['deviceId'] = $_POST['deviceId'];
					$data['Wavelength']= $arrWave[$i];
					$data['WavelengthID'] =$arrWaveId[$i];
					$obj_user_profile->update_length($tbl_ta2_wavelengths,$data);	
				} else {
					$insert_data['Wavelength'] = $_POST['Wavelength'];			
					$insert_data['deviceId'] = $_POST['deviceId'];			
					$obj_user_profile->insert_data($tbl_ta2_wavelengths,$insert_data);
				}
			} else {	
				//echo "noooot mumeric"	;	  
				$notnumeric=1;
			} //is_numric
		}//for loop
	}
	//spotsize edit 
	if(isset($_POST['spotsize']) && $_POST['spotsize'] !=NULL) {	
		$arrSpot = $_POST['spotsize'];
		if(isset($_POST['spotsizeID'])) {
			$arrSpotId = $_POST['spotsizeID'];
		} else {
			$arrSpotId ="";
		}    	 
		for($j = 0; $j < count($arrSpot);$j++) {	
			$_POST['spotsize']= $arrSpot[$j];
			if(isset($arrSpotId[$j])) {
				$_POST['spotsizeID'] = $arrSpotId[$j];
			} else {
				$_POST['spotsizeID'] ="";
			}
			if (is_numeric($_POST['spotsize'])) {
				$tbl_ta2_spot_sizes	= "tbl_ta2_spot_sizes";
				$conditionSpot = "Where spotsize 	='".$_POST['spotsize']."' AND  deviceId ='".$_POST['deviceId']."' ";			
				$spotName = $obj_user_profile->selectNumRows($tbl_ta2_spot_sizes,$conditionSpot);
				//echo "<br>";
				//echo "SelectnumSpot:-".$spotName;
				if($spotName ==1) {
				} elseif($spotName ==0 && $_POST['spotsizeID']!="") {
					$dataSpot['deviceId'] = $_POST['deviceId'];
					$dataSpot['spotsize']= $arrSpot[$j];
					$dataSpot['spotsizeID'] =$arrSpotId[$j];
					$obj_user_profile->update_spot($tbl_ta2_spot_sizes,$dataSpot);	
				} else {
					$insert_data_spot['spotsize'] = $_POST['spotsize'];			
					$insert_data_spot['deviceId'] = $_POST['deviceId'];			
					$obj_user_profile->insert_data($tbl_ta2_spot_sizes,$insert_data_spot);
				}
			} else {	
				//echo "noooot mumeric"	;	  
				$notnumeric=1;
			} //is_numric
		}//For loop end spot
	}
	/****** Delete *****/
	//Wavewlength
	if(isset($_POST['DeleteWave']) && $_POST['DeleteWave'] !=NULL){	
		//print_r($_POST['DeleteWave']);
		if(isset($_POST['DeleteWave'])) {
			$delWaveId = $_POST['DeleteWave'];
		} else {
			$delWaveId ="";
		}
		for($k = 0; $k < count($delWaveId);$k++) {
			if(isset($delWaveId[$k]) && $delWaveId[$k]!="" ) {
				$_POST['DeleteWave']= $delWaveId[$k];
				$tbl_ta2_wavelengths = "tbl_ta2_wavelengths";
				$conditionDel = " where WavelengthID=".$_POST['DeleteWave']." ";
				$obj_user_profile->deleteRow($tbl_ta2_wavelengths,$conditionDel);
			}	
		}
	}
	//Spot size
	if(isset($_POST['DeleteSpot']) && $_POST['DeleteSpot'] !=NULL) {	
		//print_r($_POST['DeleteWave']);
		if(isset($_POST['DeleteSpot'])) {
			$delspotId = $_POST['DeleteSpot'];
		} else {
			$delspotId ="";
		}
		for($jk = 0; $jk < count($delspotId);$jk++) {
			if(isset($delspotId[$jk]) && $delspotId[$jk]!="" ) {
				$_POST['DeleteSpot']= $delspotId[$jk];
				$tbl_ta2_spot_sizes = "tbl_ta2_spot_sizes";
				$conditionDelSpot = " where spotsizeID=".$_POST['DeleteSpot']." ";
				$obj_user_profile->deleteRow($tbl_ta2_spot_sizes,$conditionDelSpot);
			}	
		}
	}
	/******End Delete *****/
	echo $msg = "Information updated successfully.";
?>
