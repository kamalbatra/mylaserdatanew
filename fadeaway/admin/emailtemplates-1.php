<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<?php
	$emailTemplate	= new dbFunctions();
	/*** fetch Add Manage location**/
	$tbl_email_templates = " tbl_email_templates";
	$condition = "ORDER BY email_id  asc ";
	$cols="*";
	$EmailData = $emailTemplate->selectTableRows($tbl_email_templates,$condition);
?>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Manage Email Templates</h1>
	</div>	
	<div class="user-entry-block">			
		<div class="row">
			<div class="span6 last">
				<label class="user-name">Select Template:</label>
				<select class="select-option" name="email_page" id="email_page">						
				<?php
					if($EmailData !=NULL) {
						foreach($EmailData as $emails) { ?>
							<option value="<?php echo $emails['email_title']?>" ><?php echo $emails['email_title']?></option>
						<?php
						}
					}		
				?>
				</select>
			</div><!--end span6-->				
		</div>
		<form action="" name="ManageEmailEdit" id="ManageEmailEdit" method="post">
			<input type="hidden" name="email_title" id="email_title" value="<?php echo $templates['email_title']; ?>" />
			<input type="hidden" name="email_id" id="email_id"/>
			<div class="row">
				<div class="span6">
					<label id="" class="user-name">Subject:</label>
					<input class="text-input-field" type="text" name="email_subject" id="email_subject"/>
					<input class="error" type="text" name="email_subject_err" id="email_subject_err"/>
				</div>
			</div><!--End @row-->		
			<div class="row">
				<div class="span6" style="width:100%">
					<label class="user-name" for="Address" style="float:none">Content:</label>
					<textarea class="text-area-field" id="email_msg" name="email_msg"></textarea>
					<input class="error" type="text" name="email_msg_err" id="email_msg_err"/>
				</div>
				<script type="text/javascript">
					CKEDITOR.replace( 'email_msg' );
				</script>
			</div><!--End @row-->
			<div class="row">
				<div class="span6" style="width:2%" >
					<input type="button" id="submitEmailEdit" value="Update" class="submit-btn" >				  
					<br/>
				</div>	
				<span id="emailMsgUpdate" style="display:none;"><img src="../images/loading.gif"></span>
				<div id="res" class="res"></div>
			</div><!--End @row-->
		</form>	
	</div> <!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<script type="text/javascript">
	$(document).ready(function() {
		ajax_email('Monthly Newsletter');
		$('#email_page').on('change', function() {
			var email_temp = this.value;
			ajax_email(email_temp);
		});		
		function ajax_email(email_temp) {
			$('#emailMsgUpdate').show();
			$.ajax ({
				type: "POST",
				url: "ajax_emailtemplate.php",
				datatype: 'jsone',
				data: "page="+email_temp,
				success: function(msg) {
					$('#emailMsgUpdate').hide();
					var obj  = $.parseJSON(msg );
					if(obj.result == 'not found') {						
					} else {
						//alert(obj.email_id);
						$("#email_id").val(obj.email_id);
						$("#email_title").val(obj.email_title);
						$("#email_subject").val(obj.email_subject);
						//$("#email_msg").val(obj.email_msg);
						//	$('#email_msg').ckeditor().editor.insertHtml(obj.email_msg);
						CKEDITOR.instances['email_msg'].setData(obj.email_msg);
					}
				}
			});
		}			
		$('#submitEmailEdit').click(function() {
			//	$("#ManageEmailEdit").valid();
			var email_id = $('#email_id').val();
			var email_temp = $('#email_page').val();
			var email_subject = $('#email_subject').val();
			var editorText = CKEDITOR.instances.email_msg.getData();
			if(email_subject!='' && editorText !='') {
				var data = {};
				data.id = email_id;
				data.temp = email_temp;
				data.sub = email_subject;
				data.content = editorText;
				$('#emailMsgUpdate').show();
				$.ajax ({
					type: "POST",
					url: "ajax_emailtemplate_update.php",
					//	datatype: 'jsone',
					data: data,
					success: function(msg) {
						var obj  = $.parseJSON(msg );
						$('#emailMsgUpdate').hide();
						//$('#res').html(obj.result);
						if(obj.result == 'success')	{
							$('#res').css('color','green');
							$('#res').html("Template Updated Successfully !");
						} else {
							$('#res').css('color','red');
							$('#res').html("Error occured !");
						}
						/*var obj  = $.parseJSON(msg );
						if(obj.result == 'not found') {
						} else {
							$("#email_subject").val(obj.email_subject);
							//$("#email_msg").val(obj.email_msg);
							//	$('#email_msg').ckeditor().editor.insertHtml(obj.email_msg);
							CKEDITOR.instances['email_msg'].setData(obj.email_msg);
						}*/
					}
				});						
			} else {
				if(email_subject == '')	{
					$('#email_subject_err').val('Please enter the subject.');							
				} else {
					$('#email_subject_err').val('');
				}
				if(editorText == '') {
					$('#email_msg_err').val('Please enter the content.');
				} else {
					$('#email_msg_err').val('');
				}						
			}
		});			  
	});
</script>
<style>
	a.cke_button {
		clear:none!important;
	}
	.error {
		border:none!important;
	}
</style>
<?php
	include('admin_includes/footer.php');	
?>
