<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<?php
	$emailTemplate	= new dbFunctions();
	/*** fetch Add Manage location**/
	$tbl_email_templates = " tbl_email_templates";
	$condition = "ORDER BY email_id  asc ";
	$cols="*";
	$EmailData = $emailTemplate->selectTableRows($tbl_email_templates,$condition);
?>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Manage Email Templates</h1>
						<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
					</div>
					<form action="" name="ManageEmailEdit" id="ManageEmailEdit" method="post">
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label class="user-name">Select Template:</label>
													<select class="select-option" name="email_page" id="email_page">						
													<?php
														if($EmailData !=NULL) {
															foreach($EmailData as $emails) { ?>
																<option value="<?php echo $emails['email_title']?>" ><?php echo $emails['email_title']?></option>
															<?php
															}
														}		
													?>
													</select>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="" class="user-name">Subject:</label>
													<input class="text-input-field" type="text" name="email_subject" id="email_subject"/>
													<!-- <input class="error" type="text" name="email_subject_err" id="email_subject_err"/> -->
													<div id="email_subject_err" class="errorblocks"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div>
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label class="user-name" for="Address" style="float:none">Content:</label>
													<textarea class="text-area-field" id="email_msg" name="email_msg"></textarea>
													<!-- <input class="error" type="text" name="email_msg_err" id="email_msg_err"/> -->
													<div id="email_msg_err" class="errorblocks"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Status:</label>
													<select name="status" id="status" class="select-option">
														<option value="">Select a status</option>
														<option value="1">Active</option>
														<option value="0">De-Active</option>
													</select>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<div class="radioblocksBtns">
														<div class="radioBtn ckeckboxres">		
															<input class="service-check" type="checkbox" name="agreecheckbox" id="agreecheckbox">
															<label for="agreecheckbox" class="user-name service-check-label">No area required.</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="email_title" id="email_title" value="<?php echo $templates['email_title']; ?>" />
									<input type="hidden" name="email_id" id="email_id"/>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button"  id="submitEmailEdit" value="Update" class="submit-btn nextbtn" style="float:left;">Update</button>
											<span id="emailMsgUpdate" style="display:none;"><img src="../images/loading.gif"></span>
											<div id="res" class="res"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
<script type="text/javascript">
	CKEDITOR.replace( 'email_msg' );
</script>
<script type="text/javascript">
	$(document).ready(function() {
		ajax_email('Monthly Newsletter');
		$('#email_page').on('change', function() {
			var email_temp = this.value;
			ajax_email(email_temp);
		});		
		function ajax_email(email_temp) {
			$('#emailMsgUpdate').show();
			$.ajax ({
				type: "POST",
				url: "ajax_emailtemplate.php",
				datatype: 'jsone',
				data: "page="+email_temp,
				success: function(msg) {
					$('#emailMsgUpdate').hide();
					var obj  = $.parseJSON(msg );
					if(obj.result == 'not found') {						
					} else {
						//alert(obj.email_id);
						$("#email_id").val(obj.email_id);
						$("#email_title").val(obj.email_title);
						$("#email_subject").val(obj.email_subject);
						//$("#email_msg").val(obj.email_msg);
						//	$('#email_msg').ckeditor().editor.insertHtml(obj.email_msg);
						CKEDITOR.instances['email_msg'].setData(obj.email_msg);
					}
				}
			});
		}			
		$('#submitEmailEdit').click(function() {
			//	$("#ManageEmailEdit").valid();
			var email_id = $('#email_id').val();
			var email_temp = $('#email_page').val();
			var email_subject = $('#email_subject').val();
			var editorText = CKEDITOR.instances.email_msg.getData();
			if(email_subject!='' && editorText !='') {
				var data = {};
				data.id = email_id;
				data.temp = email_temp;
				data.sub = email_subject;
				data.content = editorText;
				$('#emailMsgUpdate').show();
				$.ajax ({
					type: "POST",
					url: "ajax_emailtemplate_update.php",
					//	datatype: 'jsone',
					data: data,
					success: function(msg) {
						var obj  = $.parseJSON(msg );
						$('#emailMsgUpdate').hide();
						//$('#res').html(obj.result);
						if(obj.result == 'success')	{
							$('#res').css('color','green');
							$('#res').html("Template Updated Successfully !");
						} else {
							$('#res').css('color','red');
							$('#res').html("Error occured !");
						}
						/*var obj  = $.parseJSON(msg );
						if(obj.result == 'not found') {
						} else {
							$("#email_subject").val(obj.email_subject);
							//$("#email_msg").val(obj.email_msg);
							//	$('#email_msg').ckeditor().editor.insertHtml(obj.email_msg);
							CKEDITOR.instances['email_msg'].setData(obj.email_msg);
						}*/
					}
				});						
			} else {
				if(email_subject == '')	{
					$('#email_subject_err').text('Please enter the subject.');							
				} else {
					$('#email_subject_err').text('');
				}
				if(editorText == '') {
					$('#email_msg_err').text('Please enter the content.');
				} else {
					$('#email_msg_err').text('');
				}						
			}
		});			  
	});
</script>
<style>
	a.cke_button {
		clear:none!important;
	}
	.error {
		border:none!important;
	}
</style>
<?php	
include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
?>