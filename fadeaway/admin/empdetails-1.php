<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$empDetails	= new dbFunctions();
if( !in_array(6,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
/*** fetch all Employee location**/
$tbl_employees	= "tbl_employees";
$condition = "WHERE BusinessID = $_SESSION[BusinessID] ORDER BY Emp_ID  ASC ";
$cols="*";
$empData = $empDetails->selectTableRows($tbl_employees,$condition);
/*** fetch All device Name**/
?>
<script type="text/javascript">
	$(document).ready(function(){
		//Delete flunce from database
		$('a.activeUser').click(function(){
			var statusMsg =$(this).attr('id'); 
			if (confirm("Are you sure you want to "+statusMsg+" this user?")){
				var id = $(this).parent().parent().attr('id');
				var status = $(this).parent().parent().attr('status');
				var data = 'Emp_ID=' + id +'&status='+status;
				var parent = $(this).parent().parent();			
				//alert(data);
				$.ajax({
					type: "POST",
					url: "ajax_changetatus.php",
					data: data,
					cache: false,				
					success: function(data){						   
						//parent.fadeOut('slow', function() {$(this).show();});
						setTimeout(function() {location.reload()	}, 1000);
						$("#statuResult").html(data);
				    }
				});			
			}
		});
		// style the table with alternate colors
		// sets specified color for every odd row
		//$('#'+id).css('background',' #FFFFFF');
	});
</script>
<style>
.srtHeadEditEmp { width:10%; }
.srtHead { width:15%; }
.addNewReport { float: right; }
</style>
<div class="form-container">
	   <div class="heading-container">
		  <h1 class="heading empHeadReport">Employees Detail</h1>
		  <div class="addNewReport addReportemp"><a class="empLinks" href="manage_employee">Add New</a></div>
	   </div>
		<div class="user-entry-block">
<div class="tablebushead">	
	<div class="tablebusinner">	
			<div class="row sortingHead">	
				<div class="span3 srtHead srtHeadBorder"> First Name</div>
				<div class="span3 srtHead srtHeadBorder"> Last Name</div>
				<div class="span3 srtHead srtHeadBorder"> Username</div>
				<div class="span3 srtHead srtHeadBorder"  style="width:20%"> Email</div>
				<div class="span3 srtHead srtHeadBorder"> Office Location</div>
				<div class="span3 srtHeadEditEmp srtHeadBorder">Action</div>
				<div class="span3 srtHeadEditEmp srtHeadBorder" style="border:none;">Action</div>
	       </div>        
        	<?php $i = 0;
				foreach($empData as $dataemp){
					//print_r($dataemp);					
					if($i%2==0){$bgdata = "bgnone";	}
						else{$bgdata = "bgdata";}	
												
					if(($_SESSION['id']==$dataemp['Emp_ID']) && (strtoupper($_SESSION['Medicaldirector'])!="YES")){
						$currentLogin= "currentLogin";
					}else{ $currentLogin= "";}
				?>	
                   <div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>" id="<?php echo $dataemp['Emp_ID']?>" status="<?php echo $dataemp['status'];?>" >
					 <div class="span3 srtHead srtcontent">
						<label id="" class="user-name"><?php echo $dataemp['First_Name']?> </label>						
					</div>					
					<div class="span3 srtHead srtcontent">	
					  <label id="" class="user-name"><?php echo $dataemp['Last_Name']?></label>
					</div>					
					<div class="span3 srtHead srtcontent">	
					  <label id="" class="user-name" style="text-transform:none;"> <?php echo $dataemp['Username']?>	</label>
					</div>
					<div class="span3 srtHead srtcontent"  style="width:20%">	
					  <label id="" class="user-name" style="text-transform:none;"> <?php echo $dataemp['Emp_email']?>	</label>
					</div>
					<div class="span3 srtHead srtcontent">	
					  <label id="" class="user-name"> <?php 
					   //echo $dataemp['Location']	
							if( isset($dataemp['Location']) && $dataemp['Location']!="") {
								$tbl_manage_location	= "tbl_manage_location";
								$condition1 = " where ManageLocationId=".$dataemp['Location']." ";
								$cols="*";
								$locationData = $empDetails->selectTableSingleRow($tbl_manage_location,$condition1);
								echo $locationData['LocationName'];					  
							} else {
								echo "";
							}
					  ?>	</label>
					</div>
		<?php if((isset($_SESSION['loginuser']) && $_SESSION['loginuser']=="sitesuperadmin") || $_SESSION['Admin']=="TRUE" || (strtoupper($_SESSION['Medicaldirector'])=="YES") || $_SESSION['id']==$dataemp['Emp_ID']){?>						
						<?php //if((isset($_SESSION['loginuser']) && $_SESSION['loginuser']=="sitesuperadmin") || ( $_SESSION['id']==$dataemp['Emp_ID']) ) { ?>
							 <div class="span3 srtHeadEditEmp srtcontent text-align">
							   <label id="" class="user-name"><a href="manage_admin?Empid=<?php echo $dataemp['Emp_ID']?>&action=empdetails"><img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit Employee"/></a></label>							
							 </div>
							<?php if($_SESSION['id']!=$dataemp['Emp_ID']) { ?>
							<?php if($dataemp['status']==1) { ?>
							    <div class="span3 srtHeadEditEmp srtcontent text-align" style="cursor:pointer;">
									<a class="activeUser" id="deactive" ><img src="<?php echo $domain; ?>/images/activeUser.png" title="Deactive" width="20px" height="20"/> </a>
							   </div>
							<?php } else {?>
							     <div class="span3 srtHeadEditEmp srtcontent text-align" style="cursor:pointer;">
									 <a class="activeUser" id="active" ><img src="<?php echo $domain; ?>/images/no.png" title="Active" width="20px" height="20"/> </a> 
								 </div>
							<?php } }?>							
						<?php //} ?>								
		 <?php }?>
			    </div><!--End @row-->				  
			  <?php $i++;  }?>			  
			  <div id="statuResult"></div>      
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
</div><!--End @container-->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
