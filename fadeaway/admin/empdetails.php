<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php'); 
	
	include("../includes/dbFunctions.php");
	$empDetails	= new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"])){ ?>
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	/*** fetch all Employee location**/
	$tbl_employees	= "tbl_employees";
	$condition = "WHERE BusinessID = $_SESSION[BusinessID] ORDER BY Emp_ID  ASC ";
	$cols="*";
	$empData = $empDetails->selectTableRows($tbl_employees,$condition);
	/*** fetch All device Name**/
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('a.activeUser').click(function(){
				var statusMsg =$(this).attr('id'); 
				if (confirm("Are you sure you want to "+statusMsg+" this user?")){
					var id = $(this).parent().parent().attr('id');
					var status = $(this).parent().parent().attr('status');
					var data = 'Emp_ID=' + id +'&status='+status;
					var parent = $(this).parent().parent();			
					$.ajax({
						type: "POST",
						url: "ajax_changetatus.php",
						data: data,
						cache: false,				
						success: function(data){						   
							setTimeout(function() {location.reload()	}, 1000);
							$("#statuResult").html(data);
						}
					});			
				}
			});
		});
	</script>
	<style>
		.srtHeadEditEmp { width:10%; }
		.srtHead { width:15%; }
		.addNewReport { float: right; }
	</style>	
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Employees</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks addnew-button" href="manage_employee"><button class="addnewbtn">Add New </button></a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>FIRST NAME</th>
										<th>Last Name</th>
										<th>Username</th>
										<th>Email</th>
										<th>Office Location</th>
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$i = 0;
								foreach($empData as $dataemp){
									if( $i%2==0 ) { 
										$bgdata = "bgnone";	
									}  else { $bgdata = "bgdata"; }
									if(($_SESSION['id']==$dataemp['Emp_ID']) && (strtoupper($_SESSION['Medicaldirector'])!="YES")){
										$currentLogin= "currentLogin";
									} else { 
										$currentLogin= "";
									}
								?>
									<tr>
										<td><?php echo $dataemp['First_Name']?></td>
										<td><?php echo $dataemp['Last_Name']?></td>
										<td><?php echo $dataemp['Username']?></td>
										<td><?php echo $dataemp['Emp_email']?></td>
										<td><?php 
											if( isset($dataemp['Location']) && $dataemp['Location']!="") {
												$tbl_manage_location	= "tbl_manage_location";
												$condition1 = " where ManageLocationId=".$dataemp['Location']." ";
												$cols="*";
												$locationData = $empDetails->selectTableSingleRow($tbl_manage_location,$condition1);
												echo $locationData['LocationName'];					  
											} else {
												echo "";
											}
										?>
										</td>
										<td>
											<a href="manage_admin?Empid=<?php echo $dataemp['Emp_ID']?>&action=empdetails"><img src="<?php echo $domain; ?>/img/editimg.png" title="Edit Employee"/></a>
											<?php 
											if($_SESSION['id']!=$dataemp['Emp_ID']) {
												if($dataemp['status']==1) { ?>
							    					<a class="activeUser" id="deactive" ><img src="<?php echo $domain; ?>/img/tickimg.png" title="Deactive"/></a>
												<?php 
												} else  { ?>
													<a class="activeUser" id="active" ><img src="<?php echo $domain; ?>/img/notallow.png" title="Active"/></a>
												<?php 
												}
											} ?>	
										</td>
									</tr>								
								<?php $i++;
								}
								?>										
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
