<?php
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$obj_waveLenth = new dbFunctions();
	//$tableUpdate = "tbl_consent_form";
	//print_r($_POST['requiredNumeric']);
	if(isset($_POST['deviceID'])) {
		//requiredNumeric
		//$dataVal = $obj_waveLenth->WaveLengthData($_POST['deviceID']);
		$tbl_ta2_wavelengths = "tbl_ta2_wavelengths";
		$condition = "where deviceId=".$_POST['deviceID']." ORDER BY convert(Wavelength, decimal) DESC";
		$cols="*";
		$dataVal = $obj_waveLenth->selectTableRows($tbl_ta2_wavelengths,$condition,$cols); 
		//$dataValSpotSize = $obj_waveLenth->SpotLengthData($_POST['deviceID']); 
		$tbl_ta2_spot_sizes	= "tbl_ta2_spot_sizes";
		$condition = "where deviceId=".$_POST['deviceID']." ORDER BY convert(spotsize, decimal) DESC ";
		$cols="*";
		$dataValSpotSize = $obj_waveLenth->selectTableRows($tbl_ta2_spot_sizes,$condition);
		//$dataValfluence = $obj_waveLenth->fluenceData($_POST['deviceID']);
		$service = $_POST['service'];
	?>
		<script type="text/javascript">
			jQuery(document).ready( function () {
				var Service = "<?php echo $service; ?>";
				var counter1 = 2;
				$("#dropBtnAdd").click( function() {
					$("#textBoxDiv").show();
					if(counter1>10){
						// alert("Only 10 textboxes allow");
						return false;	
					}
					if( Service == "tattoo" ) { 
					$("#addTxtbox").append(
						'<div class="dropCover"> <a href="#" class="remove_flunce btn btn-danger">X</a>'+
						'<div class="form-row-ld"><div class="onethirdblock"><div class="form-col-ld"><div class="inputblock-ld"><label class="user-name"> Fluence '+counter1+':</label><small class="user-name">(J/cm2)</small><input type="text" class="flclass text-input-field" name="Fluence[]" id="Fluence' + counter1 +'"/></div></div></div>'+
						'<div class="onethirdblock"><div class="form-col-ld"><div class="inputblock-ld"><label class="user-name"> Wavelength '+counter1+ ':</label><select class="select-option" name="Wavelength[]" id="Wavelength' + counter1 +'" >  <?php	foreach($dataVal as $data)	{            ?><option value="<?php echo $data['Wavelength'];?>"><?php echo $data['Wavelength'];?></option><?php }?></select></div></div></div>'+
						'<div class="onethirdblock"><div class="form-col-ld"><div class="inputblock-ld"><label class="user-name"> SpotSize '+counter1+ ':</label><select  class="select-option" name="SpotSize[]" id="SpotSize' + counter1 +'" >  <?php	foreach($dataValSpotSize as $SpotSize)	{ ?><option value="<?php echo $SpotSize['spotsize'];?>"><?php echo $SpotSize['spotsize'];?>mm</option><?php }?><  </select></div></div></div>'+'</div></div>'
					);
					} else if( Service=="hair") {
					$("#addTxtbox").append(
						'<div class="dropCover"> <a href="#" class="remove_flunce btn btn-danger">X</a>'+
						'<div class="form-row-ld"><div class="half"><div class="form-col-ld"><div class="inputblock-ld"><label class="user-name">Pulse Duration '+counter1+':</label><small>(Milliseconds)</small><input type="text" class="flclass text-input-field" name="Pulse[]" id="Pulse' + counter1 +'"/></div></div></div>'+
						'<div class="half"><div class="form-col-ld"><div class="inputblock-ld"><label class="user-name"> Fluence '+counter1+':</label><small>(J/Cm2)</small><input type="text" class="flclass text-input-field" name="Fluence[]" id="Fluence' + counter1 +'"/></div></div></div></div>'+
						'<div class="form-row-ld"><div class="half"><div class="form-col-ld"><div class="inputblock-ld"><label class="user-name"> Wavelength '+counter1+ ':</label><select  class="select-option" name="Wavelength[]" id="Wavelength' + counter1 +'" ><?php foreach($dataVal as $data){?><option value="<?php echo $data['Wavelength'];?>"><?php echo $data['Wavelength'];?></option><?php }?></select></div></div></div>'+'<div class="half"><div class="form-col-ld"><div class="inputblock-ld"><label class="user-name"> SpotSize '+counter1+ ':</label><select  class="select-option" name="SpotSize[]" id="SpotSize' + counter1 +'" >  <?php	foreach($dataValSpotSize as $SpotSize)	{ ?><option value="<?php echo $SpotSize['spotsize'];?>"><?php echo $SpotSize['spotsize'];?>mm</option><?php }?><  </select></div></div></div></div>'+'</div>'
					);		
					}
					//alert(counter1);
					counter1++;
					return false;
				});
				$('.remove_flunce').live('click', function() {
					if(counter1==1) {		
						return false;
					}
					counter1--;
					//alert("kk");
					jQuery(this).parent().remove();
					return false;
				});
			});
		</script>
		
		<?php if($service=="hair") { ?>
		<div class="form-row-ld">
			<div class="half">
				<div class="form-col-ld">
					<div class="inputblock-ld flu">
						<label>Pulse Duration</label>
						<small>(Milliseconds)</small>
						<input type="text" name="Pulse[]" id="Pulse" class="flclass text-input-field" value="" autocomplete="off" />
						<span class="error"id="err1"></span>
					</div>
				</div>
			</div>
			<div class="half">
				<div class="form-col-ld">
					<div class="inputblock-ld">
						<label>Fluence</label>
						<small><?php if(isset($_POST['requiredNumeric'])){?>(J/cm2) <?php }?></small>
						<input type="text" name="Fluence[]"id="Fluence" class="flclass text-input-field" value="<?php if(isset($_POST['requiredNumeric']) && $_POST['requiredNumeric'] =="No"){echo "Unable to record";} ?>" autocomplete="off" />
						<span class="error"id="err1"></span>
					</div>
				</div>
			</div>
		</div>		
		<div class="form-row-ld">
			<div class="half">
				<div class="form-col-ld">
					<div class="inputblock-ld">
						<label>Wavelength</label>
						<select name="Wavelength[]" id="Wavelength" class="select-option">
						<?php				 
							foreach($dataVal as $data) {
						?>
								<option value="<?php echo $data['Wavelength'];?>"><?php echo $data['Wavelength'];?></option>
						<?php
							} 
						?>
						</select>
					</div>
				</div>
			</div>
			<div class="half">
				<div class="form-col-ld">
					<div class="inputblock-ld">
						<label>SpotSize</label>
						<select name="SpotSize[]" id="SpotSize" class="select-option">
						<?php				 
							foreach($dataValSpotSize as $SpotSize) {
						?>
								<option value="<?php echo $SpotSize['spotsize'];?>"><?php echo $SpotSize['spotsize'];?>&nbsp;mm</option>
						<?php 
							} 
						?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<?php } else { ?>
		<div class="form-row-ld">
			<div class="onethirdblock">
				<div class="form-col-ld">
					<div class="inputblock-ld">
						<label>Fluence</label>
						<small><?php if(isset($_POST['requiredNumeric'])){?>(J/cm2) <?php }?></small>
						<input type="text" name="Fluence[]"id="Fluence" class="flclass text-input-field" value="<?php if(isset($_POST['requiredNumeric']) && $_POST['requiredNumeric'] =="No"){echo "Unable to record";} ?>" autocomplete="off" />
						<span class="error"id="err1"></span>
					</div>
				</div>
			</div>
			<div class="onethirdblock">
				<div class="form-col-ld">
					<div class="inputblock-ld">
						<label>Wavelength</label>
						<select name="Wavelength[]" id="Wavelength" class="select-option">
						<?php				 
							foreach($dataVal as $data) {
						?>
								<option value="<?php echo $data['Wavelength'];?>"><?php echo $data['Wavelength'];?></option>
						<?php
							} 
						?>
						</select>
					</div>
				</div>
			</div>
			<div class="onethirdblock">
				<div class="form-col-ld">
					<div class="inputblock-ld">
						<label>SpotSize</label>
						<select name="SpotSize[]" id="SpotSize" class="select-option">
						<?php				 
							foreach($dataValSpotSize as $SpotSize) {
						?>
								<option value="<?php echo $SpotSize['spotsize'];?>"><?php echo $SpotSize['spotsize'];?>&nbsp;mm</option>
						<?php 
							} 
						?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		
		<div class="form-row-ld">
			<div class="" id="dropDownGroup" style="margin:0px;"> </div>			 
		</div><!--End @row-->	
		<div class="form-row-ld addBoxMore" id="textBoxDiv" style="display:none;">
			<div id="addTxtbox" class="" style="margin:0px;">
		</div>
	</div>	  

	<div class="form-row-ld">
		<div class="form-col-ld">
			<div  id="addMoreDrop"class="span3" style="width: 12%;" > 
				<div id='dropBtnAdd' class="addMoreBtn addMoreblk">
					Add More
				</div>
			</div>
		</div>
	</div>	
	
<?php 
	} ?>		  
		  	
