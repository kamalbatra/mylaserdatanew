<?php $domain=$_SERVER['DOMAIN']; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
	<title>Login</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'/>
	<link href="<?php echo $domain; ?>/css/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" language="javascript" src="<?php echo $domain; ?>/js/html5.js"></script>
	<script type="text/javascript"  language="javascript" src="<?php echo $domain; ?>/js/PIE.htc"></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/jquery-1.9.1.js" ></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/jquery.validate.min.js" ></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/pwdwidget.js" ></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/theme4.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/pwdwidget.css" />
	<?php
		//include('admin_includes/header.php');
		include("../includes/dbFunctions.php");
		$empInfo = new dbFunctions();
	?>	
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#changePwdEmp").validate({
				rules: {                 
					Password:"required",
					Username:"required",
					Emp_email:"required",                
					Emp_email: {
						required: true,
						email: true,
					},						
				},					
				messages: {
					Username:"Please enter user name.",				
					Emp_email: {
						required: "Please enter Email." ,
						email: "Please enter valid email address.",
					},
					Password: {
						required: "Please enter Password.",
						minlength: "Please enter at least 5 digit Pssword.",						
					},					
				}
			});			   
			$('#changepwdBtn').click(function() {
				if( $("#changePwdEmp").valid())	{
					//$("#pwdResult").show();	
					changepwd_form();					
				}else{	
					$("#pwdResult").hide();		
				}
			});			   
		});
		function changepwd_form() {
			var str = $("#changePwdEmp" ).serialize();
			$.ajax({
				type: "POST",
				url: "ajax_forgotpwd.php",
				data: str,
				cache: false,
				success: function(result){
					if(result ==1){
						$("#emailNotExist").html("Username or email does not exist !").show();
						$("#pwdResult").hide();
					}
					else{
						$("#pwdResult").show();
						$("#emailNotExist").hide();
						$("#pwdResult").html(result);
						//setTimeout(function() {location.reload()	}, 2000);
				   }
				}
			});	
		}
	</script>
	<div class="container">
		<div class="logo">
			<a href="#"><img alt="logo" src="<?php echo $domain; ?>/images/Logo.png"/></a>
		</div>
		<div class="form-container">
			<div class="heading-container">
				<h1 class="heading empHead">Forgot password</h1>
				<div class="addNew">
					<a class="empLinks" href="index.php" class="submit-btn">Login</a>
				</div>
			</div>	
			<div class="user-entry-block">
			<!--  dummy form to submit data  -->
				<form action="" name="changePwdEmp" id="changePwdEmp" method="post">
					<div class="row">
						<div class="span6">
							<label id="Label1" class="user-name">Username:</label>
							<input class="text-input-field" type="text" name="Username" id="Username" value=""/>
								<!--span id="emailNotExist" class="error"></span-->
						</div>
						<div class="span6">
							<label id="Label1" class="user-name">Email:</label>
							<input class="text-input-field" type="email" name="Emp_email" id="Emp_email" value=""/>
							<span id="emailNotExist" class="error"></span>
						</div>			
					</div><!--End @row-->	
					<div class="row">
						<div class="span6">
							<label id="Label1" class="user-name">Password:</label>
							<div class='pwdwidgetdiv--' id='thepwddiv'></div>
							<script  type="text/javascript" >
								var pwdwidget = new PasswordWidget('thepwddiv','Password');
								pwdwidget.MakePWDWidget();
							</script>
							<noscript>
								<div>
									<input class="text-input-field" type="password" name="Password" id="Password"/>
								</div>
							</noscript>
						</div>
					</div><!--End @row-->
					<!--div class='para'>
					<label for='regpwd'>Password:</label> <br />
					<div class='pwdwidgetdiv' id='thepwddiv'></div>
					<script  type="text/javascript" >
					var pwdwidget = new PasswordWidget('thepwddiv','regpwd');
					pwdwidget.MakePWDWidget();
					</script>
					<noscript>
					<div><input type='password' id='regpwd' name='regpwd' /></div>		
					</noscript>
			   </div->
			  <div class="row">
					<div class="span12">				
						<input type="button"  id="changepwdBtn" value="submit" class="submit-btn"><br/>
					<div id="pwdResult" style="display:none;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
					</div>
				</div><!--End @row-->
				</form>
			</div>
		</div>
	</div><!-- container-->
	<?php
		include('admin_includes/footer.php');
	?>
