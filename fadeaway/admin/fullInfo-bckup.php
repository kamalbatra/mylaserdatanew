<?php
	//session_start();
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$fullinfo = new dbFunctions();
	$table = "tbl_clients";
	if( isset($_POST) && $_POST != NULL ) {	
		$tableConsent1="tbl_clients";
		$cols=" ClientID ";
		$condition=" where Email='".$_POST['Email']."' ";
		$checkExistence = $fullinfo->selectTableSingleRow($tableConsent1,$condition,$cols);
		if($checkExistence==NULL){ 
			$fullinfo->insert_data($table,$_POST);
			$id1=mysql_insert_id();
			$_SESSION['client_id'] = $id1;
			$id = $_SESSION['client_id'];
		} else {
	?>
		<script>
			window.location.href = 'user-entry.php?entry=fails';
		</script>
	<?php
		}
	}
	if(isset($_GET) && $_GET != NULL  ) {		
		$_SESSION['client_id'] = $_GET['clientId'];
		$id = $_SESSION['client_id'];
	}
	if(!isset($_SESSION['client_id'])) { 	
		echo '<script>window.location.assign("user-entry.php")</script>';				 
	}
	$conditionForID = " where ClientID=".$_SESSION['client_id'];	
	$clientInfo	= $fullinfo->selectTableSingleRow($table,$conditionForID);
?>
<head>
	<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->
	<script src="<?php echo $domain; ?>/js/fullinfoValidate.js"></script>
	<script>
		function enterSign(strVal) {
			document.getElementById("ClientSignaturePrompt").value='/'+ strVal+'/';				
		}
	</script>	
	<script>
		/*
		$(document).ready(function(){
			$('#CurrentMeds1').blur(function(){
				$("#mydiv").append(
					'<input type="text" name="CurrentMeds2" id="CurrentMeds2"/>' 
				);
			})
			$('#CurrentMeds2').blur(function(){
				$("#mydiv1").append(
					'<input type="text" name="CurrentMeds3" id="CurrentMeds3"/>' 
				);
			})
		})*/
	</script>
</head>
<div class="form-container">
	<h1 class="heading">Medical History</h1> 	
	<div class="medical-history-block">		
		<?php 
		if(isset($_SESSION['client_id'])) { ?>		
			<?php
			$fname = strtolower($clientInfo['FirstName']); 
			$lname = strtolower($clientInfo['LastName']); 
		} ?>		
		<!--thankyou.php-->
		<form action="thankyou.php" name="form"  method="post" onsubmit="return validateForm('<?php echo $fname;?>','<?php echo $lname;?>')">
			<input type="hidden" value="<?php echo $_SESSION['client_id']; ?>" name="ClientID"/>
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">First Name:</label>
					<input class="text-input-field" type="text" name="FirstName" id="FirstName" value="<?php if(isset($clientInfo['FirstName'])){ echo $clientInfo['FirstName'];} ?>" readonly="readonly"/>
				</div>				
				<div class="span6 last">
					<label id="Label1" class="user-name">Last Name:</label>
					<input class="text-input-field" type="text" name="LastName" id="LastName" value="<?php if(isset($clientInfo['LastName'])){ echo $clientInfo['LastName'];} ?>" readonly="readonly"/>
				</div>
			</div><!--End @row-->
			<h2>Past Medical History</h2>
			<div class="row">
				<div class="span12">
					<label id="Label1" class="user-name">Please describe the tattoo(s) you wish to have treated:</label>
					<textarea rows="4" cols="30" name="TreatmentRequest" id="TreatmentRequest" class="text-area-field"></textarea>
					<span id="treatmentRequestLenMsg" class="error"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">Are you currently under the care of a physician?</label>
					<input value="Yes" type="radio" name="PhysicianCare" id="PhysicianCare" class="radio-fields" onClick="enablePhysicanCareReason();" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="PhysicianCare" id="PhysicianCare1" class="radio-fields" onClick="disablePhysicanCareReason();"/><span class="select-txt">No</span>  
					<span id="PhysicianCareMsg" class="error"></span>
				</div>
				<div class="span6 last">
					<label id="Label1" class="user-name">If yes, then for what reason?</label>
					<input type="text" name="PhysicanCareReason" id="PhysicanCareReason" class="text-input-field" />
					<span id="PhysicanCareReasoneMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<h6>Please indicate any of the following medical conditions for which you have been treated in the past:</h6>
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Cancer:</span>
					<input type="radio" name="Cancer" id="Cancer" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Cancer" id="Cancer" class="radio-fields" /><span class="select-txt">No</span> 
					 <span id="CancerMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">High Blood Pressure:</span>
					<input type="radio" name="HighBloodPressure" id="HighBloodPressure" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HighBloodPressure" id="HighBloodPressure" class="radio-fields" /><span class="select-txt">No</span>
					<span id="HighBloodPressureMsg" class="error ermsg"></span>
				</div>	
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Arthritis:</span>
					<input type="radio" name="Arthritis" id="Arthritis" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Arthritis" id="Arthritis" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="ArthritisMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Cold Sores:</span>
					<input type="radio" name="ColdSores" id="ColdSores" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="ColdSores" id="ColdSores" class="radio-fields" /><span class="select-txt">No</span>
					<span id="ColdSoresMsg" class="error ermsg"></span>
				</div>					
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Keloids: </span>
					<input type="radio" name="Keloids" id="Keloids" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Keloids" id="Keloids" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="KeloidsMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Disease of the Skin:</span>
					<input type="radio" name="SkinDisease" id="SkinDisease" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="SkinDisease" id="SkinDisease" class="radio-fields" /><span class="select-txt">No</span>
					<span id="SkinDiseaseMsg" class="error ermsg"></span>
				</div>					
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Diabetes:</span>
					<input type="radio" name="Diabetes" id="Diabetes" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Diabetes" id="Diabetes" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="DiabetesMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">HIV / AIDS:</span>
					<input type="radio" name="HIV" id="HIV" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HIV" id="HIV" class="radio-fields" /><span class="select-txt">No</span>
					<span id="HIVMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Hormone Imbalance:</span>
					<input  type="radio" name="HormoneImbalance" id="HormoneImbalance" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HormoneImbalance" id="HormoneImbalance" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="HormoneImbalanceMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Seizures:</span>
					<input  type="radio" name="Seizures" id="Seizures" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Seizures" id="Seizures" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="SeizuresMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Blood Clotting Abnormality:</span>
					<input type="radio" name="BloodClottingAbnormality" id="BloodClottingAbnormality" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="BloodClottingAbnormality" id="BloodClottingAbnormality" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="BloodClottingAbnormalityMsg" class="error ermsg"></span>
				</div>	
				<div class="span6 last">
					<span class="medical-condition-txt">Hepatitis:</span>
					<input type="radio" name="Hepatitis" id="Hepatitis" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Hepatitis" id="Hepatitis" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="HepatitisMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Thyroid Imbalance:</span>
					<input type="radio" name="ThyroidImbalance" id="ThyroidImbalance" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="ThyroidImbalance" id="ThyroidImbalance" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="ThyroidImbalanceMsg" class="error ermsg"></span>
				</div>
				<div class="span6 last">
					<span class="medical-condition-txt">Any Active Infection:</span>
					<input type="radio" name="ActiveInfection" id="ActiveInfection" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="ActiveInfection" id="ActiveInfection" class="radio-fields" /><span class="select-txt">No</span>
					<span id="ActiveInfectionMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<label id="Label1" class="user-name">Please list any other medical conditions you may suffer:</label>
					<input class="text-input-field" type="text" name="MedicalProblemsOther" id="MedicalProblemsOther" />
					<span id="MedicalProblemsOtherMsg" class="error ermsg"></span>
				</div>	
			</div><!--End @row-->
			<div class="divider-dotted">&nbsp;</div>		
			<h2>Allergies</h2>
			<h6>Have you ever had an allergic reaction to any of the following?</h6>
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Food Allergy:</span>
					<input type="radio" name="FoodAllergy" id="FoodAllergy" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="FoodAllergy" id="FoodAllergy" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="FoodAllergyMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Hydrocortisone Allergy:</span>
					<input type="radio" name="HydrocortisoneAllergy" id="HydrocortisoneAllergy" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HydrocortisoneAllergy" id="HydrocortisoneAllergy" class="radio-fields" /><span class="select-txt">No</span>
					<span id="HydrocortisoneAllergyMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Hydroquinone Allergy: </span>
					<input type="radio" name="HydroquinoneAllergy" id="HydroquinoneAllergy" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HydroquinoneAllergy" id="HydroquinoneAllergy" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="HydroquinoneAllergyMsg" class="error ermsg"></span>
				</div>	
				<div class="span6 last">
					<span class="medical-condition-txt">Aspirin Allergy:</span>
					<input type="radio" name="AspirinAllergy" id="AspirinAllergy" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="AspirinAllergy" id="AspirinAllergy" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="AspirinAllergyMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->	
			<div class="row">
				<div class="span6">
					<span class="medical-condition-txt">Latex Allergy:</span>
					<input type="radio" name="LatexAllergy" id="LatexAllergy" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="LatexAllergy" id="LatexAllergy" class="radio-fields" /><span class="select-txt">No</span>  
					<span id="LatexAllergyMsg" class="error ermsg"></span>
				</div>				
				<div class="span6 last">
					<span class="medical-condition-txt">Lidocaine Allergy:</span>
					<input type="radio" name="LidocaineAllergy" id="LidocaineAllergy" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="LidocaineAllergy" id="LidocaineAllergy" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="LidocaineAllergyMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->			
			<div class="row">
				<div class="span12">
					<label id="Label1" class="user-name">List any other allergies:</label>
					<textarea type="text" name="OtherAllergy" id="OtherAllergy" class="text-area-field"></textarea>
					<span id="OtherAllergyMsg" class="error ermsg"></span>					
				</div>
			</div><!--End @row-->			
			<div class="divider-dotted">&nbsp;</div>			
			<h2>Medications</h2>
			<h6>What oral medications are you presently taking? (Please check all that apply)</h6>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Blood Thinners (Aspirin, Plavix, Coumadin (Warfarin), Ticlid, etc.):</span>
					<input type="radio" name="BloodThinners" id="BloodThinners" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="BloodThinners" id="BloodThinners" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="BloodThinnersMsg" class="error ermsg mederr"></span>				   
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Immunosuppressive Drugs (Therapy for Autoimmune Diseases (Lupus, Rheumatoid Arthritis, etc.) Chemotherapy for Cancer, Steroids, etc.):</span>
					<input type="radio" name="Immunosuppressants" id="Immunosuppressants" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input type="radio" name="Immunosuppressants" id="Immunosuppressants" value="No" class="radio-fields" /><span class="select-txt">No</span>
					<span id="ImmunosuppressantsMsg" class="error ermsg mederr"></span>				
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Hormone Therapy (Estrogen Replacement Therapy (ERT), Birth Control Pills, etc.):</span>
					<input type="radio" name="HormoneTherapy" id="HormoneTherapy" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="HormoneTherapy" id="HormoneTherapy" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="HormoneTherapyMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="divider-dotted">&nbsp;</div>
			<!----List your current medications-->
			<h2>List your current medications</h2>
			<div class="row">
				<div class="span3">
					<label id="Label1" class="user-name">Medication 1:</label>				
				</div>
				<div class="span6 last" style="margin:0px;">
					<input class="text-input-field" type="text" name="CurrentMeds1" id="CurrentMeds1" />
					<span id="CurrentMeds1Msg" class="error ermsg"></span>
				</div>
				<!--div class="span3" style="width: 12%;padding-left:21px;"> 
				   <!--div class="addMoreBtn"><input type='button' value='Add More' class="submit-btn" id='addButton' style="padding: 5px 20px!important;"></div-->
				 <!--/div-->
			</div><!--End @row-->
			<div class="row" id="textBoxDiv" style="display:none;">
		        <div class="span3"></div>
		        <div id="addTxtbox" class="span6 last" style="margin:0px;">	 </div>
			</div>
			<div class="row">
				<!--div class="inc">				 
					<div class="controls">
						<input type="text" value="ooko">
						<button class="btn btn-info" type="button" id="append" name="append">Add Textbox</button>
						<br>
						<br>
					</div-->
					<div class="span3" style=""></div>
					<!--div class="span6 addMoreBtn"><input type='button' value='Add More' class="submit-btn" id='addButton' style="padding: 5px 20px!important;"></div-->	 
					<button class="btn btn-info" type="button" id="append" name="append">Add More</button>
			</div>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Are you on any mood altering or anti-depression medication?</span>
					<input type="radio" name="PsychMeds" id="PsychMeds" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="PsychMeds" id="PsychMeds" class="radio-fields" /><span class="select-txt">No</span>
					<span id="PsychMedsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Have you ever used Accutane?</span>
					<input  type="radio" name="Accutane" id="Accutane" value="Yes" class="radio-fields" onClick="enableTxt();" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Accutane" id="Accutane" class="radio-fields" onClick="disableTxt();"/><span class="select-txt">No</span>				
					<span id="AccutaneMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">If yes, when enter your last use?</label>				
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="AccutaneLastUse" id="AccutaneLastUse" />
					<span id="AccutaneLastUseMsg" class="error ermsg"></span>
				</div>				
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Are you presently using topcal Retinoid creams?</span>
					<input type="radio" name="Retinoids" id="Retinoids" value="Yes" class="radio-fields" onClick="enableTxtBox();" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="Retinoids" id="Retinoids" class="radio-fields" onClick="disableTxtBox();" /><span class="select-txt">No</span>				
					<span id="RetinoidsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">If yes, then was the date of last retinoid use?</label>
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="RetinoidsLastUse" id="RetinoidsLastUse" />
					<span id="RetinoidsLastUseMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">What herbal supplements do you use regularly?</label>
				</div>
				<div class="span6 last">
					<input class="text-input-field"  type="text" name="Herbals" id="Herbals"/>
					<span id="HerbalsMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="divider-dotted">&nbsp;</div>
			<!----List your current medications-->
			<h2>History</h2>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Have you received laser tattoo removal before?</span>
					<input type="radio" name="PriorTattooLaser" id="PriorTattooLaser" value="Yes" class="radio-fields" onClick="enablePriorTattooLaserInfo();" /> <span class="select-txt">Yes</span>
					<input value="No" type="radio" name="PriorTattooLaser" id="PriorTattooLaser" class="radio-fields"  onClick="disablePriorTattooLaserInfo();" /><span class="select-txt">No</span>				
					<span id="PriorTattooLaserMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span3">
					<label id="Label1" class="user-name">If yes, then please describe:</label>
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="PriorTattooLaserInfo" id="PriorTattooLaserInfo" />
					<span id="PriorTattooLaserInfoMsg" class="error ermsg"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Have you had any recent tanning or sun exposure that changed the color of your skin?</span>
					<input type="radio" name="RecentTanning" id="RecentTanning" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input name="RecentTanning" type="radio" id="RecentTanning"  class="radio-fields" value="No"/><span class="select-txt">No</span>
					<span id="RecentTanningMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Have you recently used any self-tanning lotions or treatments?</span>
					<input type="radio" name="RecentSelfTanningLotions" id="RecentSelfTanningLotions" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input type="radio" name="RecentSelfTanningLotions" id="RecentSelfTanningLotions" value="No" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="RecentSelfTanningLotionsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Do you form thick or raised scars from cuts or burns?</span>
					<input type="radio" name="BadScars" id="BadScars" value="Yes" class="radio-fields" onClick="enableBadScarsInfoInfo();" /> <span class="select-txt">Yes</span>
					<input type="radio" name="BadScars" id="BadScars" value="NO" class="radio-fields"  onClick="disableBadScarsInfoInfo();" /><span class="select-txt">No</span>				
					<span id="BadScarsMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">If yes, please describe any scarring issues:</label>				
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="BadScarsInfo" id="BadScarsInfo" />
					<span id="BadScarsInfoMsg" class="error ermsg"></span>
				</div>				
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Do you have Hyper-pigmentation (darkening of the skin) or Hypo-pigmentation(lightening of the skin), or marks after a physical trauma?</span>
					<input type="radio" name="Dyspigmentation" id="Dyspigmentation" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input type="radio" name="Dyspigmentation" id="Dyspigmentation" value="No" class="radio-fields" /><span class="select-txt">No</span>				
                    <span id="DyspigmentationMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->			
			<div class="divider-dotted">&nbsp;</div>			
			<!----female clients-->
			<h2>Female Clients</h2>
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt lnTab">Are you pregnant or trying to become pregnant?</span>
					<input type="radio" name="PregnancyRisk" id="PregnancyRisk" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input type="radio" name="PregnancyRisk" id="PregnancyRisk" value="No" class="radio-fields" /><span class="select-txt">No</span>	
					<span id="PregnancyRiskMsg" class="error ermsg mederr"></span>					
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Are you breastfeeding?</span>
					<input type="radio" name="BreastFeeding" id="BreastFeeding" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input type="radio" name="BreastFeeding" id="BreastFeeding" value="No" class="radio-fields" /><span class="select-txt">No</span>	
					<span id="BreastFeedingMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span12">
					<span class="medical-condition-txt">Are you using contraception?</span>
					<input type="radio" name="ContraceptionUse" id="ContraceptionUse" value="Yes" class="radio-fields" /> <span class="select-txt">Yes</span>
					<input type="radio" name="ContraceptionUse" id="ContraceptionUse" value="No" class="radio-fields" /><span class="select-txt">No</span>				
					<span id="ContraceptionUseMsg" class="error ermsg mederr"></span>
				</div>
			</div><!--End @row-->			
			<div class="divider-dotted">&nbsp;</div>
			<!--notice--->
			<h2>Notice</h2>
			<p>Northeast Laser Tattoo Removal,LLC and its dba (doing business as) entities include Highbridge Laser and Fade Away Laser.</p>
			<div class="divider-dotted">&nbsp;</div>
			<!--notice--->
			<h2>Risks</h2>
			<p><strong><i>Incomplete Removal:</i></strong> There is a chance that the treated tattoo will not remove completely and some tattoos will have minimal reaction to the laser treatments. We cannot predict	whether your tattoo can be completely removed.</p>
			<p class="space"></p>			
			<p> <strong><i>Scarring and Skin Textural Changes:</i></strong> However slight, there is always a risk of scarring. Skin texture changes (roughness, thickening, discoloration) can occur and may be permanent.</p>				
			<p class="space"></p>
			<p> <strong><i>Hyper-pigmentation:	</i></strong>Darkening of your skin can occur after laser treatments. This is more commonly seen in patients with darker skin types, particularly those with Asian heritage, but can occur in any skin type. Hyper-pigmentation usually resolves spontaneously but can require additional treatment in some cases.</p>		
			<p class="space"></p>
			<p> <strong><i>Hypo-pigmentation:	</i></strong>  Lightening of your skin after treatment can occur after laser treatments.This can occur with any skin type and although it may resolve spontaneously, it can be permanent.</p>
			<p class="space"></p>
			<p> <strong><i>Infection:</i></strong> Although unusual; bacterial, fungal and viral infections can occur after being treated. Notify us if you have a history of cold sores prior to any treatments involving the face or any active infections for which you are being treated or should be treated prior to laser tattoo removal.</p>
			<p class="space"></p>
			<p> <strong><i>Bleeding:</i></strong> Pinpoint bleeding can occur during a laser treatment, although it is rare in most cases. If you are on blood thinners such as Coumadin (Warfarin), Aspirin, Plavix, etc., please notify us for discussion with our physician prior to treatment.</p>
			<p class="space"></p>
			<p> <strong><i>Blistering:</i></strong> Blistering is common after Q-switched laser treatments and is discussed in the after-care forms.</p>
			<p class="space"></p>
			<p><strong><i>Allergic Reactions:</i></strong> Allergic reactions have been reported after Q-switched laser treatments of certain tattoo pigments, most commonly red inks, but can occur with other colors.Breakdown of tattoo inks is thought to trigger this reaction.</p>
			<p class="space"></p>
			<p><strong><i>Darkening of Flesh Colored Tattoos:</i></strong> Some types of tattoo inks become darker upon treatment with Q-switched lasers. This is particularly common with white pigments.However, we can never know with certainty if an ink in your tattoo has been mixed with these pigments. If you experience tattoo ink darkening, this is often correctable, but may require additional treatments.</p>
			<p class="space"></p>
			<p><strong><i>Eye protection:</i></strong> I understand that exposure of my unprotected eyes to the light created by laser operation could harm my vision. The eye protection provided will be worn at all times when the laser is in use (Eyewear will be provided).</p>
			<p class="space"></p>
			<p><strong><i>Aftercare:</i></strong> Compliance with the provided aftercare is crucial to the healing of the affected area in the prevention of infection, scarring, timely healing and many other health factors.</p>				
			<div class="divider-dotted">&nbsp;</div>				
			<!--notice--->
			<h2>Privacy Rule</h2>
			<p>Northeast Laser Tattoo Removal will use appropriate safeguards to protect the privacy of personal health information, and will not sell or give such information without patient authorization.</p>
			<div class="divider-dotted">&nbsp;</div>
			<!--notice--->
			<h2>Photo Release</h2>
			<p>We photograph all tattoos undergoing treatment for documentation. Selected tattoos may be used in marketing materials but will not include your name or other identifying information.</p>
			<div class="divider-dotted">&nbsp;</div>
			<!--notice--->
			<h2>Liability Release</h2>
			<p>I certify that the information described above is accurate and complete to the best of my knowledge. Any questions I had have been answered to my satisfaction, and I understand the process and been explained all of the risks. By signing this I hereby release Northeast Laser Tattoo Removal and its dba entities Highbridge Laser and Fade Away Laser; Thomas Barrows, MD, Aleksandar Nedich, and any designated individual in relation to Northeast Laser Tattoo Removal from any and all legal or financial responsibilities. By signing this I am also allowing Northeast Laser Tattoo Removal to proceed with laser treatment of my tattoo.</p>
			<br/>
			<div class="row">
				<div class="span3">
					<label id="Label1" class="user-name">Client Electronic Signature:</label>				
				</div>
				<div class="span6 last">
					<input class="text-input-field" type="text" name="ClientSignature" id="ClientSignature" onKeyup="" />
					<input class="text-input-field" type="hidden" name="ClientSignaturePrompt" id="ClientSignaturePrompt" />					
					<span id="ClientSignatureMsg" class="error"></span>
				</div>				
			</div><!--End @row-->
			<input type="hidden" name="TimeStampClient" id="TimeStampClient" value="<?=date('Y-m-d H:i:s'); ?>"/>
			<input type="hidden" name="Duluth" id="Duluth" value="<?=$_SESSION['Emp_Location_Duluth']?>"/>
			<input type="hidden" name="Minneapolis" id="Minneapolis" value="<?=$_SESSION['Emp_Location_Minneapolis']?>"/>
			<input type="hidden" name="St_Paul" id="St_Paul" value="<?=$_SESSION['Emp_Location_St_Paul']?>"/>
			<input type="hidden" name="Location" id="Location" value="<?=$_SESSION['Location']?>"/>
			<div class="row">
				<div class="span12">					
				<input class="submit-btn" type="submit" value="Submit" />
				</div>
			</div><!--End @row-->
		</form>
	</div>
</div>
</div>
<?php
	include('admin_includes/footer.php');
?>
