<?php
//session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
//~ include('admin_includes/header.php');
?>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<?php
			include("../includes/dbFunctions.php");
			require('../SendyPHP/SendyPHP.php');
			$fullinfo = new dbFunctions();
			$table = "tbl_clients";
			$tableLegal="tbl_legal_disclaimer";
			$cols=" * ";
			$condition=" where bussines_id='".$_SESSION['BusinessID']."' and user_id=".$_SESSION['id']." order by id desc";
			$checkLegalExistance = $fullinfo->selectTableData($tableLegal,$condition,$cols);
			$legalVersionId = $checkLegalExistance->id;
			$legalnotice = $checkLegalExistance->notice;
			$legalrisk = $checkLegalExistance->risk;
			$legalliability = $checkLegalExistance->liability_release;
			$BusinessName=$_SESSION['businessdata']['BusinessName'];
			if(isset($_POST) && $_POST != NULL  ){
				$tableConsent1="tbl_clients";
				$cols=" ClientID ";
				$condition=" where AES_DECRYPT(Email, '".SALT."')='".$_POST['Email']."' and BusinessID=".$_SESSION['BusinessID'];
				$checkExistence = $fullinfo->selectTableSingleRow($tableConsent1,$condition,$cols);
				if($checkExistence==NULL){
					/**************** Subscribe Users For Monthly Newsletter ****************/
						/*$list_name = "Monthly Newsletter Clients";
						$sendy = new \SendyPHP\SendyPHP($list_name);
						$listdata = $sendy->get_list_id($list_name);
						$subscribe['name'] = $_POST['FirstName'].' '.$_POST['LastName'];
						$subscribe['email'] = $_POST['Email'];
						$sendy->setListId($listdata['list_id']);
						$sendy->subscribe($subscribe);*/
					/********************************* End *********************************/
					$_POST['FirstName'] = strtolower($_POST['FirstName']);
					$_POST['LastName'] = strtolower($_POST['LastName']);
					$_POST['BusinessID']=$_SESSION['BusinessID'];
					$_POST['disclaimer_version']=$legalVersionId;
					if($_POST['ReferralSource'] == 'Friend' || $_POST['ReferralSource'] == 'Tattoo Artist' || $_POST['ReferralSource'] == 'Other') {
						$_POST['ReferralSourceName']=$_POST['ReferralSourceName'];
					}	else 	{
						$_POST['ReferralSourceName']='';
					}
					$fullinfo->insert_data_aes($table,$_POST);
					$id1=mysqli_insert_id($fullinfo->con);
					$_SESSION['client_id'] = $id1;
					$id = $_SESSION['client_id'];
				}	else 	{
				?>
					<script>
						window.location.href = 'user-entry?entry=fails';
					</script>
				<?php
				}
			}
			if(isset($_GET) && $_GET != NULL  ){
				$_SESSION['client_id'] = $_GET['clientId'];
				$id = $_SESSION['client_id'];
			}
			if(!isset($_SESSION['client_id'])){ 	
				echo '<script>window.location.assign("user-entry")</script>';			 
			}
			$conditionForID = " where ClientID=".$_SESSION['client_id'];
			$cols = "ClientID, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, AES_DECRYPT(DOB, '".SALT."') AS DOB";
			$clientInfo	= $fullinfo->selectTableSingleRow($table,$conditionForID, $cols);

			$table	= "tbl_tattoremove_part";
			$bodypartchoices = $fullinfo->selectTableRows($table);

			$table	= "tbl_tatto_colors";
			$tattocolors = $fullinfo->selectTableRows($table);
			?>
			<script src="<?php echo $domain; ?>/js/fullinfoValidate.js"></script>
			<script>
				function enterSign(strVal){
					document.getElementById("ClientSignaturePrompt").value='/'+ strVal+'/';				
				}
			</script>
			<script>
				$(function(){	
					$("input:radio[name=Gender]").click(function() {
						var value = $(this).val();
						if(value == "male"){
							$('#female-client').hide();
						} else if(value == "female"){
							$('#female-client').show();
						}  
					});
				});
			</script>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					$('.backbtn').show();
					jQuery("#medicalhistory").validate({
						errorClass: 'errorblocks',
						errorElement: 'div',
						ignore:[],
						rules: {
							Gender: "required",
							TreatmentRequest:"required",
							PhysicianCare:"required",
							PhysicanCareReason: {
								required: {
									depends:"input[name='PhysicianCare']:checked"
								}
							},
						},
						messages: {
							Gender: "Please select your gender!",
							TreatmentRequest:"Please enter details!",
							PhysicianCare:"Please select an option!",
							PhysicanCareReason:"Please enter reason!",
						},
						submitHandler: function (form) {
							$('.new-client-block-content').hide();
							$('#block-2').show();
						}
					});

					jQuery("#medicalconditions").validate({
						errorClass: 'errorblocks',
						errorElement: 'div',
						ignore:[],
						rules: {
							Cancer: "required",
							Arthritis: "required",
							Keloids: "required",
							Diabetes: "required",
							HighBloodPressure: "required",
							HormoneImbalance: "required",
							BloodClottingAbnormality: "required",
							ThyroidImbalance: "required",
							ColdSores: "required",
							SkinDisease: "required",
							HIV: "required",
							Seizures: "required",
							Hepatitis: "required",
							ActiveInfection: "required",
						},
						messages: {
							Cancer: "Please select an option!",
							Arthritis: "Please select an option!",
							Keloids: "Please select an option!",
							Diabetes: "Please select an option!",
							HighBloodPressure: "Please select an option!",
							HormoneImbalance: "Please select an option!",
							BloodClottingAbnormality: "Please select an option!",
							ThyroidImbalance: "Please select an option!",
							ColdSores: "Please select an option!",
							SkinDisease: "Please select an option!",
							HIV: "Please select an option!",
							Seizures: "Please select an option!",
							Hepatitis: "Please select an option!",
							ActiveInfection: "Please select an option!",
						},
						submitHandler: function (form) {
							$('.new-client-block-content').hide();
							$('#block-3').show();
						}
					});

					jQuery("#allergies").validate({
						errorClass: 'errorblocks',
						errorElement: 'div',
						ignore:[],
						rules: {
							FoodAllergy: "required",
							HydroquinoneAllergy: "required",
							LatexAllergy: "required",
							HydrocortisoneAllergy: "required",
							AspirinAllergy: "required",
							LidocaineAllergy: "required",
						},
						messages: {
							FoodAllergy: "Please select an option!",
							HydroquinoneAllergy: "Please select an option!",
							LatexAllergy: "Please select an option!",
							HydrocortisoneAllergy: "Please select an option!",
							AspirinAllergy: "Please select an option!",
							LidocaineAllergy: "Please select an option!",
						},
						submitHandler: function (form) {
							$('.new-client-block-content').hide();
							$('#block-4').show();
						}
					});

					jQuery("#medications").validate({
						errorClass: 'errorblocks',
						errorElement: 'div',
						ignore:[],
						rules: {
							BloodThinners: "required",
							Immunosuppressants: "required",
							HormoneTherapy: "required",
							PsychMeds: "required",
							Accutane: "required",
							AccutaneLastUse: {
								required: {
									depends:"input[name='Accutane']:checked"
								}
							},
							Retinoids: "required",
							RetinoidsLastUse: {
								required: {
									depends:"input[name='Retinoids']:checked"
								}
							},
						},
						messages: {
							BloodThinners: "Please select an option!",
							Immunosuppressants: "Please select an option!",
							HormoneTherapy: "Please select an option!",
							PsychMeds: "Please select an option!",
							Accutane: "Please select an option!",
							AccutaneLastUse: "Please enter details of last use!",
							Retinoids: "Please select an option!",
							RetinoidsLastUse: "Please enter date of last use!",
						},
						submitHandler: function (form) {
							$('.new-client-block-content').hide();
							$('#block-5').show();
						}
					});
					
					jQuery("#history").validate({
						errorClass: 'errorblocks',
						errorElement: 'div',
						ignore:[],
						rules: {
							PriorTattooLaser: "required",
							PriorTattooLaserInfo: {
								required: {
									depends:"input[name='PriorTattooLaser']:checked"
								}
							},
							RecentTanning: "required",
							RecentSelfTanningLotions: "required",
							BadScars: "required",
							BadScarsInfo: {
								required: {
									depends:"input[name='BadScars']:checked"
								}
							},							
							Dyspigmentation: "required",
						},
						messages: {
							PriorTattooLaser: "Please select an option!",
							PriorTattooLaserInfo: "Please select an option!",
							RecentTanning: "Please select an option!",
							RecentSelfTanningLotions: "Please select an option!",
							BadScars: "Please select an option!",
							BadScarsInfo: "Please select an option!",
							Dyspigmentation: "Please select an option!",
						},
						submitHandler: function (form) {
							var gender = $('#Gender').val();
							$('.new-client-block-content').hide();
							if(gender == "male"){ 
								$('#block-7').show();
							} else if(gender == "female"){
								$('#block-6').show();
							}
						}
					});
					
					jQuery("#femaleclient").validate({
						errorClass: 'errorblocks',
						errorElement: 'div',
						ignore:[],
						rules: {
							PregnancyRisk: "required",
							BreastFeeding: "required",
							ContraceptionUse: "required",
						},
						messages: {
							PregnancyRisk: "Please select an option!",
							BreastFeeding: "Please select an option!",
							ContraceptionUse: "Please select an option!",
						},
						submitHandler: function (form) {
							$('.new-client-block-content').hide();
							$('#block-7').show();
						}
					});
					
					jQuery("#notice").validate({
						errorClass: 'errorblocks',
						errorElement: 'div',
						ignore:[],
						rules: {
							risk1: "required",
							risk2: "required",
							risk3: "required",
							risk4: "required",
							risk5: "required",
							risk6: "required",
							risk7: "required",
							risk8: "required",
						},
						messages: {
							risk1: "Please agree with risk!",
							risk2: "Please agree with risk!",
							risk3: "Please agree with risk!",
							risk4: "Please agree with risk!",
							risk5: "Please agree with risk!",
							risk6: "Please agree with risk!",
							risk7: "Please agree with risk!",
							risk8: "Please agree with risk!",
						},
						submitHandler: function (form) {
							$('.new-client-block-content').hide();
							$('#block-8').show();
						}
					});
					
					$('#block-8-forward').click(function(){
						$('.new-client-block-content').hide();
						$('#block-9').show();						
					});
					
					$('#block-2-back').click(function(){
						$('.new-client-block-content').hide();
						$('#block-1').show();						
					});

					$('#block-3-back').click(function(){
						$('.new-client-block-content').hide();
						$('#block-2').show();						
					});
					
					$('#block-4-back').click(function(){
						$('.new-client-block-content').hide();
						$('#block-3').show();		
					});
					
					$('#block-5-back').click(function(){
						$('.new-client-block-content').hide();
						$('#block-4').show();		
					});

					$('#block-6-back').click(function(){
						$('.new-client-block-content').hide();
						$('#block-5').show();		
					});

					$('#block-7-back').click(function(){
						$('.new-client-block-content').hide();
						var gender = $('#Gender').val();
						if(gender == "male"){ 
							$('#block-5').show();
						} else if(gender == "female"){
							$('#block-6').show();
						}
					});

					$('#block-8-back').click(function(){
						$('.new-client-block-content').hide();
						$('#block-7').show();
					});

					$('#block-9-back').click(function(){
						$('.new-client-block-content').hide();
						$('#block-8').show();
					});

					$("#clickMe").click(function () {
					var str = $("#medicalhistory").serialize() + "&" + $("#medicalconditions").serialize() + "&" + $("#allergies").serialize() + "&" + $("#medications").serialize() + "&" + $("#history").serialize() + "&" + $("#femaleclient").serialize() + "&" + $("#liability").serialize();
					$.ajax({
						type: "POST",
						url: "ajax_consentformsumit.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								window.location.href = 'thankyou';
							} else {
								window.location.href = 'user-entry?entry=notsubmit';
							}
							
						}
					}); 
   
    
					});					
					
				});
			</script>
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="headingmain">
						New Client Entry
					</div>
					<?php
					if(isset($_SESSION['client_id'])) {
						$fname = strtolower($clientInfo['FirstName']);
						$lname = strtolower($clientInfo['LastName']);
					}
					?>
					
					<!-- -------- Medical History ---------------------- -->
					<div id="block-1" class="new-client-block-content">
						<form id="medicalhistory" action="thankyou" name="medicalhistory" class="myForms" method="post">
							<input type="hidden" value="<?php echo $_SESSION['client_id']; ?>" name="ClientID"/>
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">2</span>
									<span>Medical History</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld radiolablesouter">
									<div class="form-row-ld">
										<div class="onethirdblock">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>First Name</label>
													<input class="text-input-field" type="text" name="" id="FirstName" value="<?php if(isset($clientInfo['FirstName'])){ echo ucfirst($clientInfo['FirstName']);} ?>" readonly="readonly"/>
												</div>
											</div>
										</div>
										<div class="onethirdblock">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Last Name</label>
													<input class="text-input-field" type="text" name="" id="LastName" value="<?php if(isset($clientInfo['LastName'])){ echo ucfirst($clientInfo['LastName']);} ?>" readonly="readonly"/>
												</div>
											</div>
										</div>
										<div class="onethirdblock">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Gender</label>
													<select name="Gender" id="Gender">
														<option value="">Please select</option>
														<option value="male">Male</option>
														<option value="female">Female</option>
													</select>
													<span id="GenderMsg" class="error"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<h2 class="pastHistory">Past Medical History</h2>
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld bigtextarea">
													<label>Please describe the tattoo(s) you wish to have treated</label>
													<textarea rows="4" cols="30" name="TreatmentRequest" id="TreatmentRequest" class="text-area-field"></textarea>
													<span id="treatmentRequestLenMsg" class="errorblocks"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Are you currently under the care of a physician?</label>
													<div class="radioblocksBtns radioBtn-container">
														<div class="radioBtn">
															<input value="Yes" type="radio" name="PhysicianCare" id="PhysicianCare" onClick="enablePhysicanCareReason();" /><label for="PhysicianCare">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="PhysicianCare" id="PhysicianCare1" class="radio-fields" onClick="disablePhysicanCareReason();"/>
															<label for="PhysicianCare1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>If yes, then for what reason?</label>
													<input type="text" name="PhysicanCareReason" id="PhysicanCareReason" class="text-input-field" autocomplete="off" />
													<span id="PhysicanCareReasoneMsg" class="error ermsg"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<!-- button class="backbtn">Back</button -->
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- --------- Medical History --------------------- -->
					<!-- -------- Medical Conditions ------------------- -->
					<div id="block-2" class="new-client-block-content" style="display:none;">
						<form id="medicalconditions" action="thankyou" name="medicalconditions" class="myForms" method="post">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">3</span>
									<span>Medical Conditions</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld">
									<h2 class="pastHistory grycolor">Please indicate any of the following medical conditions for which you have been treated in the past:</h2>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolbl">
													<label>Cancer</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Cancer" id="Cancer" value="Yes" class="radio-fields" /><label for="Cancer">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Cancer" id="Cancer1" class="radio-fields" /><label for="Cancer1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Arthritis</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Arthritis" id="Arthritis" value="Yes" class="radio-fields" /><label for="Arthritis">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Arthritis" id="Arthritis1" class="radio-fields" /><label for="Arthritis1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Keloids</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Keloids" id="Keloids" value="Yes" class="radio-fields" /><label for="Keloids">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Keloids" id="Keloids1" class="radio-fields" /><label for="Keloids1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Diabetes</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Diabetes" id="Diabetes" value="Yes" class="radio-fields" /><label for="Diabetes">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Diabetes" id="Diabetes1" class="radio-fields" /><label for="Diabetes1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>High Blood Pressure</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="HighBloodPressure" id="HighBloodPressure" value="Yes" class="radio-fields" /><label for="HighBloodPressure">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="HighBloodPressure" id="HighBloodPressure1" class="radio-fields" /><label for="HighBloodPressure1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Hormone Imbalance</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input  type="radio" name="HormoneImbalance" id="HormoneImbalance" value="Yes" class="radio-fields" /><label for="HormoneImbalance">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="HormoneImbalance" id="HormoneImbalance1" class="radio-fields" /><label for="HormoneImbalance1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Blood Clotting Abnormality</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="BloodClottingAbnormality" id="BloodClottingAbnormality" value="Yes" class="radio-fields" /><label for="BloodClottingAbnormality">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="BloodClottingAbnormality" id="BloodClottingAbnormality1" class="radio-fields" /><label for="BloodClottingAbnormality1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolbl">
													<label>Thyroid Imbalance</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="ThyroidImbalance" id="ThyroidImbalance" value="Yes" class="radio-fields" /><label for="ThyroidImbalance">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="ThyroidImbalance" id="ThyroidImbalance1" class="radio-fields" /><label for="ThyroidImbalance1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Cold Sores</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="ColdSores" id="ColdSores" value="Yes" class="radio-fields" /><label for="ColdSores">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="ColdSores" id="ColdSores1" class="radio-fields" /><label for="ColdSores1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Disease of the Skin</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="SkinDisease" id="SkinDisease" value="Yes" class="radio-fields" /><label for="SkinDisease">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="SkinDisease" id="SkinDisease1" class="radio-fields" /><label for="SkinDisease1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>HIV / AIDS</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="HIV" id="HIV" value="Yes" class="radio-fields" /><label for="HIV">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="HIV" id="HIV1" class="radio-fields" /><label for="HIV1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Seizures</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input  type="radio" name="Seizures" id="Seizures" value="Yes" class="radio-fields" /><label for="Seizures">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Seizures" id="Seizures1" class="radio-fields" /><label for="Seizures1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Hepatitis</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Hepatitis" id="Hepatitis" value="Yes" class="radio-fields" /><label for="Hepatitis">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Hepatitis" id="Hepatitis1" class="radio-fields" /><label for="Hepatitis1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Any Active Infection</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="ActiveInfection" id="ActiveInfection" value="Yes" class="radio-fields" /><label for="ActiveInfection">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="ActiveInfection" id="ActiveInfection1" class="radio-fields" /><label for="ActiveInfection1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Please list any other medical conditions you may suffer</label>
													<input class="text-input-field" type="text" name="MedicalProblemsOther" id="MedicalProblemsOther" autocomplete="off" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-2-back" class="backbtn">Back</button>
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>						
						</form>
					</div>
					<!-- -------- Medical Conditions ------------------- -->
					<!-- -------- Allergies ---------------------------- -->
					<div id="block-3" class="new-client-block-content" style="display:none;">
						<form id="allergies" action="thankyou" name="allergies" class="myForms" method="post">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">4</span>
									<span>Allergies</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld radiolablesouter">
									<h2 class="pastHistory grycolor">Have you ever had an allergic reaction to any of the following?</h2>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolbl">
													<label>Food Allergy</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="FoodAllergy" id="FoodAllergy" value="Yes" class="radio-fields" /><label for="FoodAllergy">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="FoodAllergy" id="FoodAllergy1" class="radio-fields" /><label for="FoodAllergy1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Hydroquinone Allergy</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="HydroquinoneAllergy" id="HydroquinoneAllergy" value="Yes" class="radio-fields" /><label for="HydroquinoneAllergy">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="HydroquinoneAllergy" id="HydroquinoneAllergy1" class="radio-fields" /><label for="HydroquinoneAllergy1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Latex Allergy</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="LatexAllergy" id="LatexAllergy" value="Yes" class="radio-fields" /><label for="LatexAllergy">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="LatexAllergy" id="LatexAllergy1" class="radio-fields" /><label for="LatexAllergy1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolbl">
													<label>Hydrocortisone Allergy</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="HydrocortisoneAllergy" id="HydrocortisoneAllergy" value="Yes" class="radio-fields"/><label for="HydrocortisoneAllergy">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="HydrocortisoneAllergy" id="HydrocortisoneAllergy1" class="radio-fields" /><label for="HydrocortisoneAllergy1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Aspirin Allergy</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="AspirinAllergy" id="AspirinAllergy" value="Yes" class="radio-fields" /><label for="AspirinAllergy">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="AspirinAllergy" id="AspirinAllergy1" class="radio-fields" /><label for="AspirinAllergy1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Lidocaine Allergy</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="LidocaineAllergy" id="LidocaineAllergy" value="Yes" class="radio-fields" /><label for="LidocaineAllergy">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="LidocaineAllergy" id="LidocaineAllergy1" class="radio-fields" /><label for="LidocaineAllergy1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>List any other allergies</label>
													<textarea type="text" name="OtherAllergy" id="OtherAllergy" class="text-area-field"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-3-back" class="backbtn">Back</button>
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- -------- Allergies ------------------- -->
					<!-- -------- Medications ------------------- -->
					<div id="block-4" class="new-client-block-content" style="display:none;">
						<form id="medications" action="" name="medications"  method="post">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">5</span>
									<span>Medications</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld">
									<h2 class="pastHistory"><span class="grycolor">What oral medications are you presently taking?</span> (Please check all that apply)</h2>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolbl">
													<label>Blood Thinners (Aspirin, Plavix, Coumadin (Warfarin), Ticlid, etc.)</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="BloodThinners" id="BloodThinners" value="Yes" class="radio-fields" /><label for="BloodThinners">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="BloodThinners" id="BloodThinners1" class="radio-fields" /><label for="BloodThinners1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Immunosuppressive Drugs (Therapy for Autoimmune Diseases (Lupus, Rheumatoid Arthritis, etc.) Chemotherapy for Cancer, Steroids, etc.)</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Immunosuppressants" id="Immunosuppressants" value="Yes" class="radio-fields" /><label for="Immunosuppressants">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="Immunosuppressants" id="Immunosuppressants1" value="No" class="radio-fields" /><label for="Immunosuppressants1">No</label>
														</div>
													</div>
												</div>
												<div class="inputblock-ld radiolbl">
													<label>Hormone Therapy (Estrogen Replacement Therapy (ERT), Birth Control Pills, etc.)</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="HormoneTherapy" id="HormoneTherapy" value="Yes" class="radio-fields" /><label for="HormoneTherapy">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="HormoneTherapy" id="HormoneTherapy1" class="radio-fields" /><label for="HormoneTherapy1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<h2 class="pastHistory">List your current medications</h2>
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Medication 1</label>
													<input class="text-input-field medicationautocmp" type="text" name="CurrentMeds1" id="CurrentMeds1" autocomplete="off" /><input type="hidden" class="text-input-field" id="CurrentMedsId1" value="2">
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld"  id="textBoxDiv" style="display:none;">
										<div class="textBoxDivouterblk">
											<div class="inputblock-ld">
												<div class="span3"></div>
												<div id="addTxtbox" class="span6 last"></div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="form-col-ld">
											<div class="addMoreblk" class="btn btn-info" id="append">
												Add More
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Are you on any mood altering or anti-depression medication?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="PsychMeds" id="PsychMeds" value="Yes" class="radio-fields" /><label for="PsychMeds">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="PsychMeds" id="PsychMeds1" class="radio-fields" /><label for="PsychMeds1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Have you ever used Accutane?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input  type="radio" name="Accutane" id="Accutane" value="Yes" class="radio-fields" onClick="enableTxt();" /><label for="Accutane">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Accutane" id="Accutane1" class="radio-fields" onClick="disableTxt();"/><label for="Accutane1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>If yes, when enter your last use?</label>
													<input class="text-input-field" type="text" name="AccutaneLastUse" id="AccutaneLastUse" autocomplete="off" />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Are you presently using topcal Retinoid creams?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Retinoids" id="Retinoids" value="Yes" class="radio-fields" onClick="enableTxtBox();"/><label for="Retinoids">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="Retinoids" id="Retinoids1" class="radio-fields" onClick="disableTxtBox();"/><label for="Retinoids1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>If yes, then was the date of last retinoid use?</label>
													<input class="text-input-field" type="text" name="RetinoidsLastUse" id="RetinoidsLastUse" autocomplete="off" />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>What herbal supplements do you use regularly?</label>
													<input class="text-input-field"  type="text" name="Herbals" id="Herbals" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-4-back" class="backbtn">Back</button>
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- -------- Medications ------------------- -->
					<!-- -------- History ------------------- -->
					<div id="block-5" class="new-client-block-content" style="display:none;">
						<form id="history" action="" name="history"  method="post">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">6</span>
									<span>History</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Have you received laser tattoo removal before?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="PriorTattooLaser" id="PriorTattooLaser" value="Yes" class="radio-fields" onClick="enablePriorTattooLaserInfo();" /><label for="PriorTattooLaser">Yes</label>
														</div>
														<div class="radioBtn">
															<input value="No" type="radio" name="PriorTattooLaser" id="PriorTattooLaser1" class="radio-fields"  onClick="disablePriorTattooLaserInfo();" /><label for="PriorTattooLaser1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>If yes, then please describe</label>
													<input class="text-input-field" type="text" name="PriorTattooLaserInfo" id="PriorTattooLaserInfo" autocomplete="off" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Have you had any recent tanning or sun exposure that changed the color of your skin?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="RecentTanning" id="RecentTanning" value="Yes" class="radio-fields" /><label for="RecentTanning">Yes</label>
														</div>
														<div class="radioBtn">
															<input name="RecentTanning" type="radio" id="RecentTanning1" class="radio-fields" value="No"/><label for="RecentTanning1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Have you recently used any self-tanning lotions or treatments?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="RecentSelfTanningLotions" id="RecentSelfTanningLotions" value="Yes" class="radio-fields"/><label for="RecentSelfTanningLotions">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="RecentSelfTanningLotions" id="RecentSelfTanningLotions1" value="No" class="radio-fields"/><label for="RecentSelfTanningLotions1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Do you form thick or raised scars from cuts or burns?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="BadScars" id="BadScars" value="Yes" class="radio-fields" onClick="enableBadScarsInfoInfo();" /><label for="BadScars">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="BadScars" id="BadScars1" value="No" class="radio-fields"  onClick="disableBadScarsInfoInfo();" /><label for="BadScars1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>If yes, please describe any scarring issues</label>
													<input class="text-input-field" type="text" name="BadScarsInfo" id="BadScarsInfo" autocomplete="off" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Do you have Hyper-pigmentation (darkening of the skin) or Hypo-pigmentation (lightening of the skin), or marks after a physical trauma?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="Dyspigmentation" id="Dyspigmentation1" value="Yes" class="radio-fields" /><label for="Dyspigmentation1">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="Dyspigmentation" id="Dyspigmentation1" value="No" class="radio-fields" /><label for="Dyspigmentation1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Do you smoke cigarettes?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="SmokeCigarette" id="SmokeCigarette" value="Yes" class="radio-fields" /><label for="SmokeCigarette">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="SmokeCigarette" id="SmokeCigarette1" value="No" class="radio-fields" /><label for="SmokeCigarette1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Approximately how many years have you had the tattoo you wish to remove?</label>
													<select name="TattooAge" id="TattooAge">
														<option>Select an option</option>
														<option value="-1"> <1 year </option>
														<option value="1">1 year</option>
														<option value="2">2 years</option>
														<option value="3">3 years</option>
														<option value="4">4 years</option>
														<option value="5">5 years</option>
														<option value="6">6 years</option>
														<option value="7">7 years</option>
														<option value="8">8 years</option>
														<option value="9">9 years</option>
														<option value="10">10 years</option>
														<option value="11">11 years</option>
														<option value="12">12 years</option>
														<option value="13">13 years</option>
														<option value="14">14 years</option>
														<option value="15">15 years</option>
														<option value="16">16 years</option>
														<option value="17">17 years</option>
														<option value="18">18 years</option>
														<option value="19">19 years</option>
														<option value="20">20 years</option>
														<option value="20+"> >20 years</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Which colors are present in your tattoo?</label>
													<div class="radioblocksBtns">
													<?php
														foreach($tattocolors as $tattocolor){
														?>
															<div class="radioBtn ckeckboxres">
																<input type="checkbox" name="TattoColor[]" id="TattoColor_<?php echo $tattocolor['id']; ?>" value="<?php echo $tattocolor['id']; ?>" class="radio-fields" /><label for="TattoColor_<?php echo $tattocolor['id']; ?>"><?php echo ucfirst($tattocolor['name']); ?></label>
															</div>
														<?php
														}
														?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>On which body part is the tattoo you wish to remove?</label>
													<div class="radioblocksBtns">
													<?php
													foreach($bodypartchoices as $bodypartchoice){
													?>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" name="BodyPart[]" id="BodyPart_<?php echo $bodypartchoice['id']; ?>" value="<?php echo $bodypartchoice['id']; ?>" class="radio-fields" /><label for="BodyPart_<?php echo $bodypartchoice['id']; ?>"><?php echo ucfirst($bodypartchoice['part_name']); ?> </label>
														</div>
													<?php
													}
													?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-5-back" class="backbtn">Back</button>
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- -------- History ------------------- -->
					<!-- -------- Female client ------------------- -->
					<div id="block-6" class="new-client-block-content" style="display:none;">
						<form id="femaleclient" action="" name="femaleclient"  method="post">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">7</span>
									<span>Female Clients</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Are you pregnant or trying to become pregnant?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="PregnancyRisk" id="PregnancyRisk" value="Yes" class="radio-fields" /><label for="PregnancyRisk">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="PregnancyRisk" id="PregnancyRisk1" value="No" class="radio-fields" /><label for="PregnancyRisk1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Are you breastfeeding?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="BreastFeeding" id="BreastFeeding" value="Yes" class="radio-fields" /><label for="BreastFeeding">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="BreastFeeding" id="BreastFeeding1" value="No" class="radio-fields" /><label for="BreastFeeding1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label>Are you using contraception?</label>
													<div class="radioblocksBtns">
														<div class="radioBtn">
															<input type="radio" name="ContraceptionUse" id="ContraceptionUse" value="Yes" class="radio-fields" /><label for="ContraceptionUse">Yes</label>
														</div>
														<div class="radioBtn">
															<input type="radio" name="ContraceptionUse" id="ContraceptionUse1" value="No" class="radio-fields" /><label for="ContraceptionUse1">No</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-6-back" class="backbtn">Back</button>
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- -------- Female client ------------------- -->
					<!-- -------- Notice & Risks ------------------- -->
					<div id="block-7" class="new-client-block-content" style="display:none;">
						<form id="notice" action="" name="notice"  method="post">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">8</span>
									<span>Notice & Risks</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="noticeblock">
													<div class="noticeHeading">
														Notice
													</div>
													<?php 
													if($legalnotice!='') { 
														echo $legalnotice; 
													} else { ?>
														<p>This section is deliberately left blank.</p>
													<?php 
													} ?>													
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="noticeblock">
													<div class="noticeHeading">
														Risks
													</div>
													<?php 
													/*
													if($legalrisk!='') { 
														echo $legalrisk;
													} else { 
														*/
													?>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk1" id="risk1">
																	<label for="risk1">
																	<b>Incomplete Removal:</b> 
																	<span>There is a chance that the treated tattoo will not remove completelyand some tattoos will have minimal reaction to the laser treatments. We cannot predict whether your tattoo can be completely removed.</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk2" id="risk2">
																	<label for="risk2">
																	<b>Scarring and Skin Textural Changes: </b> 
																	<span>However slight, there is always a risk of scarring. Skin texture changes (roughness, thickening, discoloration) can occur and may be permanent.</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk3" id="risk3">
																	<label for="risk3">
																	<b>Hyper-pigmentation:</b> 
																	<span>Darkening of your skin can occur after laser treatments. This is more commonly seen in patients with darker skin types, particularly those with Asian heritage, but can occur in any skin type. Hyper-pigmentation usually resolves spontaneously but can require additional treatment in some cases.</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk4" id="risk4">
																	<label for="risk4">
																	<b>Hypo-pigmentation: </b> 
																	<span>Lightening of your skin after treatment can occur after laser treatments.This can occur with any skin type and although it may resolve spontaneously, it can be permanent.</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk5" id="risk5">
																	<label for="risk5">
																	<b>Infection: </b> 
																	<span>Although unusual; bacterial, fungal and viral infections can occur after being treated. Notify us if you have a history of cold sores prior to any treatments involving the face or any active infections for which you are being treated or should be treated prior to laser tattoo removal.</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk6" id="risk6">
																	<label for="risk6">
																	<b>Bleeding: </b> 
																	<span>Pinpoint bleeding can occur during a laser treatment, although it is rare in most cases. If you are on blood thinners such as Coumadin (Warfarin), Aspirin, Plavix, etc., please notify us for discussion with our physician prior to treatment.</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk7" id="risk7">
																	<label for="risk7">
																	<b>Blistering:  </b> 
																	<span>Blistering is common after Q-switched laser treatments and is discussed in the after-care forms.</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="inputblock-ld radiolabel">
															<div class="radioblocksBtns">
																<div class="radioBtn ckeckboxres">
																	<input type="checkbox" name="risk8" id="risk8">
																	<label for="risk8">
																	<b>Allergic Reactions: </b> 
																	<span>Allergic reactions have been reported after Q-switched laser treatments of certain tattoo pigments, most commonly red inks, but can occur with other colors. Breakdown of tattoo inks is thought to trigger this reaction.</span>
																	</label>
																</div>
															</div>
														</div>
													<?php 
													//~ }
													?>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-7-back" class="backbtn">Back</button>
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- -------- Notice & Risks ------------------- -->
					<!-- -------- Privacy Rule & Photo Release ------------------- -->
					<div id="block-8" class="new-client-block-content" style="display:none;">
						<form id="privacy-photo" action="" name="privacy-photo"  method="post">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">9</span>
									<span>Privacy Rule & Photo Release</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="noticeblock">
													<div class="noticeHeading">
														Privacy Rule
													</div>
													<p><?php echo $BusinessName; ?> will use appropriate safeguards to protect the privacy of personal health information, and will not sell or give such information without patient authorization. </p>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="noticeblock">
													<div class="noticeHeading">
														Photo Release
													</div>
													<p>We photograph all tattoos undergoing treatment for documentation. Selected tattoos may be used in marketing materials but will not include your name or other identifying information.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-8-back" class="backbtn">Back</button>
											<button type="button" id="block-8-forward" class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- -------- Privacy Rule & Photo Release ------------------- -->
					<!-- -------- Liability Release ------------------- -->
					<div id="block-9" class="new-client-block-content" style="display:none;">
						<form id="liability" action="" name="liability"  method="post">	
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">10</span>
									<span>Liability Release</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
										<div class="formcircle active"></div>
									</div>
								</div>
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="noticeblock">
												<?php 
												if($legalliability!="") {  
													echo $legalliability;
												} else {  ?>
													<p>By signing this document,  I certify that my questions and concerns have adequately been addressed and I hereby release <?php echo $BusinessName; ?> and its owners and operators from any legal or financial liabilities pertaining to my laser treatments.</p>
												<?php
												}
												?>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Client Electronic Signature</label>
													<input class="text-input-field" type="text" name="ClientSignature" id="ClientSignature" onKeyup="" autocomplete="off" />
													<input class="text-input-field" type="hidden" name="ClientSignaturePrompt" id="ClientSignaturePrompt" />					
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="TimeStampClient" id="TimeStampClient" value="<?=date('Y-m-d H:i:s'); ?>"/>
									<input type="hidden" name="Duluth" id="Duluth" value="<?=$_SESSION['Emp_Location_Duluth']?>"/>
									<input type="hidden" name="Minneapolis" id="Minneapolis" value="<?=$_SESSION['Emp_Location_Minneapolis']?>"/>
									<input type="hidden" name="St_Paul" id="St_Paul" value="<?=$_SESSION['Emp_Location_St_Paul']?>"/>
									<input type="hidden" name="Location" id="Location" value="<?=$_SESSION['Location']?>"/>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="noticeblock">
													<p><strong><i>AGREEMENT:</i></strong> By signing this Electronic Signature Acknowledgment Form, I agree that my electronic signature is the legally binding equivalent to my handwritten signature. Whenever I execute an electronic signature, it has the same validity and meaning as my handwritten signature. I will not, at any time in the future, repudiate the meaning of my electronic signature or claim that my electronic signature is not legally binding.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button" id="block-9-back" class="backbtn">Back</button>
											<button type="button" class="nextbtn" id="clickMe">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- -------- Liability Release ------------------- -->
				
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
		<style>
			.ui-autocomplete-loading {
				background: white url("https://jqueryui.com/resources/demos/autocomplete/images/ui-anim_basic_16x16.gif") right center no-repeat !important;
			}
			.ui-autocomplete{width: 613px !important;font-size: 13px !important;max-height: 300px;overflow: auto;}
		</style>
		<script>
			$(function() {
				function split( val ) {
				  return val.split( /,\s*/ );
				}
				function extractLast( term ) {
				  return split( term ).pop();
				}

				$( "#CurrentMeds1" )
				// don't navigate away from the field on tab when selecting an item
				.bind( "keydown", function( event ) {
					if ( event.keyCode === $.ui.keyCode.TAB &&
					$( this ).autocomplete( "instance" ).menu.active ) {
						event.preventDefault();
					}
				})
				.autocomplete({
					source: function( request, response ) {
						$.getJSON( "search_meds.php", {
							term: extractLast( request.term )
						}, response );
					},
					search: function() { // custom minLength
						var term = extractLast( this.value );
						if ( term.length < 2 ) {
							return false;
						}
					},
					focus: function() { // prevent value inserted on focus
						return false;
					},
					select: function( event, ui ) {
						this.value = ui.item.value;
						return false;
					}
				});
			});
			</script>		
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
