<?php
	include('admin_includes/header.php');
	include('../includes/dbFunctions.php');
	if( !in_array(6,$_SESSION["menuPermissions"]) ) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$geniframe = new dbFunctions();
	
	$table = "tbl_manage_location";
	$condition = "where businessID=".$_SESSION['BusinessID']." GROUP BY LocationName";
	$cols = "ManageLocationId,LocationName";
	$locations = $geniframe->selectTableRowsNew($table,$condition,$cols);
?>	
	<style>
		.right-margin-6 { margin-right: 6%; }
		.menu-checkbox{ float: left; width: 25%; }
		.addNewReport { float: right; }
		.formdonly {display:none;}
	</style>
	<script>
		jQuery(document).ready(function() {
			jQuery("#genrateiframe").validate({
				rules: {
					location: {
						required: true,
					}
				},	
				messages: {
					location: {
						required: "Please select a location.",
					}
				},
				submitHandler: function(form) {
					var str = $("#genrateiframe").serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result) {
							if(result == 1){
								$("#insertResult").show();
								$("#insertResult").html("<span style='color:green;'>widget generated successfully.</span>");
								setTimeout(function() {
									location.href = 'iframes'
								}, 1000);
							} else if(result == 0){
								$("#insertResult").show();
								$("#insertResult").html("<span style='color:red;'>widget already generated for this location.</span>");
								setTimeout(function() {
									location.href = 'iframes'
								}, 1000);
							}
						}
					}); 
				}
			});					
		});
	</script>
	<div class="form-container">
		<div class="heading-container">
			<h1 class="heading empHeadReport">Generate Widget</h1>
		</div>
		<div class="user-entry-block">
			<form action="" name="genrateiframe" id="genrateiframe" method="post">	
				<div class="row">
					<input type="hidden" name="businessID" value="<?php echo $_SESSION[BusinessID]; ?>"/>
					<input type="hidden" name="dateAdded" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
					<input type="hidden" name="formname" value="genrateiframe"/>
					<div class="span6 right-margin-6">
						<label id="Label1" class="user-name">Status:</label>
						<select name="location" id="location" class="select-option">
							<option value="">Select a location</option>
							<?php
								foreach($locations as $location){
								?>	
									<option value="<?php echo $location['ManageLocationId']; ?>"><?php echo $location['LocationName']; ?></option>
								<?php
								}
							?>
						</select>
					</div>
				</div><!--End @row-->		
				<div class="row">
					<div class="span12">
						<input type="submit" id="genrate" value="Generate" class="submit-btn" style="float:left;">
						<div id="insertResult" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
					</div>			
				</div><!--End @row-->
			</form>
		</div><!--End @user-entry-block-->
	</div><!--End @form-container-->
</div>
<?php include('admin_includes/footer.php'); ?>
