<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include('../includes/dbFunctions.php');
	if( !in_array(6,$_SESSION["menuPermissions"]) ) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$geniframe = new dbFunctions();
	
	$table = "tbl_manage_location";
	$condition = "where businessID=".$_SESSION['BusinessID']." GROUP BY LocationName";
	$cols = "ManageLocationId,LocationName";
	$locations = $geniframe->selectTableRowsNew($table,$condition,$cols);
?>	
	<style>
		.right-margin-6 { margin-right: 6%; }
		.menu-checkbox{ float: left; width: 25%; }
		.addNewReport { float: right; }
		.formdonly {display:none;}
	</style>
	<script>
		jQuery(document).ready(function() {
			jQuery("#genrateiframe").validate({
				errorClass: 'errorblocks',
				errorElement: 'div',
				rules: {
					location: {
						required: true,
					}
				},	
				messages: {
					location: {
						required: "Please select a location.",
					}
				},
				submitHandler: function(form) {
					var str = $("#genrateiframe").serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result) {
							if(result == 1){
								$("#insertResult").show();
								$("#insertResult").html("<span style='color:green;'>widget generated successfully.</span>");
								setTimeout(function() {
									location.href = 'iframes'
								}, 1000);
							} else if(result == 0){
								$("#insertResult").show();
								$("#insertResult").html("<span style='color:red;'>widget already generated for this location.</span>");
								setTimeout(function() {
									location.href = 'iframes'
								}, 1000);
							}
						}
					}); 
				}
			});					
		});
	</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Generate Widget</h1>
					</div>
					<div class="card shadow mb-4 table-main-con">
						<div class="bussiness-searchblock no-searchbox">
							<div class="search-btn">
								<a class="empLinks" href="iframes" class="submit-btn"><button class="addnewbtn">All Widgets</button></a>
							</div>
						</div>
						<div class="formcontentblock-ld">
					<form action="" name="genrateiframe" id="genrateiframe" method="post">
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label id="Label1" class="user-name">Status:</label>
													<select name="location" id="location" class="select-option">
														<option value="">Select a location</option>
														<?php
															foreach($locations as $location){
															?>	
																<option value="<?php echo $location['ManageLocationId']; ?>"><?php echo $location['LocationName']; ?></option>
															<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="businessID" value="<?php echo $_SESSION[BusinessID]; ?>"/>
									<input type="hidden" name="dateAdded" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
									<input type="hidden" name="formname" value="genrateiframe"/>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="submit"  id="submitForm" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
											<div id="insertResult" class="u_mess" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
