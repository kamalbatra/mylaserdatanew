<?php
	include('admin_includes/header.php');
	include('../includes/dbFunctions.php');
	$iframeDetails	= new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"]) ) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	/*** fetch all Employee location**/
	$tbl_iframe	= "tbl_iframe";
	$tbl_location	= "tbl_manage_location";
	$join = "LEFT";
	$condition =  "$tbl_iframe.loactionID = $tbl_location.ManageLocationId WHERE $tbl_iframe.businessID = $_SESSION[BusinessID] ORDER BY $tbl_iframe.loactionID DESC";
	$cols = "$tbl_iframe.*,$tbl_location.LocationName";
	$iframeData = $iframeDetails->selectTableJoinNew($tbl_iframe,$tbl_location,$join,$condition,$cols);
	/*** fetch All device Name**/
	?>
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function() {
			
			$('a.activeUser').click(function() {
				var statusMsg = $(this).attr('id');
				if ( confirm("Are you sure you want to "+statusMsg+" this category?") ) {
					var id = $(this).parent().parent().attr('id');
					var status = $(this).parent().parent().attr('status');
					var data = 'id='+id+'&status='+status;
					var parent = $(this).parent().parent();			
					$.ajax({
						type: "POST",
						url: "ajax_newchangestatus.php",
						data: data,
						cache: false,				
						success: function(data) {
							if(data == 1) {
								data = 'Status updated succesfully.';
								setTimeout(function(){ location.reload() }, 1000);							
							}
						}
					});		
				}
			});
			
			$('.pricingEdit').live('click',function() {
				var pid = $(this).attr('id');
				var str ='frameid='+pid;
				$.ajax ({
					type: "POST",
					url: "ajax_iframe.php",
					data: str,
					cache: false,
					success: function(result) {
						//alert(result);
						$('.overlay-bg-iframe').html(result).show(); //display your popup
						//$("#u_mess").html(result);
					}
				});
			});
		
			$('.close-btn-pricing').live('click',function() {
				$('.overlay-bg-iframe').html('').hide();
			});
		//. pricingEdit  
		
		
		});
	</script>
	<style>
		.srtHeadEditEmp { width:10%; }
		.tablecategories .span4.srtHead.srtHeadBorder { width:40%; }
		.tablecategories .span4.srtHead.srtcontent { width:40%;padding: 10px 0px 2px 12px; }
		.addNewReport { float: right; }
	</style>
	<div class="form-container">
		<div class="heading-container">
			<h1 class="heading empHeadReport">Appointment Widget</h1>
			<div class="addNewReport addReportemp"><a class="empLinks" href="generate-iframe">Generate New</a></div>
		</div>
		<div class="user-entry-block">
			<div class="tablebushead">	
				<div class="tablebusinner tablecategories">	
					<div class="row sortingHead">	
						<div class="span4 srtHead srtHeadBorder">Location Name</div>
						<div class="span4 srtHead srtHeadBorder">Date Added</div>
						<div class="span4 srtHeadEditEmp srtHeadBorder" style="border:none;">Action</div>
					</div>        
					<?php 
					if( !empty($iframeData) ) {
						$i = 0;
						foreach( $iframeData as $dataemp ){
							//~ print_r($dataemp['iframeID']);
							if(	$i%2 == 0 ){ 
								$bgdata = "bgnone";	
							} else {
								$bgdata = "bgdata";
							}	
							if(($_SESSION['id'] == $dataemp['Emp_ID']) && (strtoupper($_SESSION['Medicaldirector'])!="YES")){
								$currentLogin= "currentLogin";
							} else { 
								$currentLogin= "";
							}
							?>	
							<div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>" id="<?php echo $dataemp['id']?>" status="<?php echo $dataemp['status'];?>" >
								<div class="span4 srtHead srtcontent">
									<label id="" class="user-name"><?php echo $dataemp['LocationName']?> </label>						
								</div>					
								<div class="span4 srtHead srtcontent">	
									<label id="" class="user-name" style="text-transform:none;">
										<?php echo date("F j, Y", strtotime($dataemp['dateAdded'])); ?>
									</label>
								</div>
								<?php 
								if( ( isset($_SESSION['loginuser']) && $_SESSION['loginuser']=="sitesuperadmin" ) || $_SESSION['Admin']=="TRUE" || ( strtoupper($_SESSION['Medicaldirector'])=="YES" ) || $_SESSION['id']==$dataemp['Emp_ID'] ) { ?>						
									<div class="span4 srtHeadEditEmp srtcontent text-align">
										<label class="user-name">
											<a class="pricingEdit" id="<?php echo $dataemp['iframeID']; ?>"><img src="<?php echo $domain; ?>/images/Images-icon.png" title="view iframe"/></a>
										</label>
									</div>
									<?php 
								} ?>
							</div><!--End @row-->				  
						<?php 
						$i++;  
						} 
					} else {
					?>	
						<div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>">
							<div class="srtcontent">
								No record found.
							</div>
						</div>
					<?php
					} 
					?>			  
					<div id="statuResult"></div>  
					<div class="overlay-bg-iframe"></div>
				</div><!--End @user-entry-block-->
			</div><!-- End  @form-container--->
		</div><!--End @container-->
	</div><!--End @container-->
	</div><!--End @container-->
	<?php include('admin_includes/footer.php');	?>
