<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include('../includes/dbFunctions.php');
	$iframeDetails	= new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"]) ) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	/*** fetch all Employee location**/
	$tbl_iframe	= "tbl_iframe";
	$tbl_location	= "tbl_manage_location";
	$join = "LEFT";
	/*** numrows**/
	$adjacents = 3;
	$reload="iframes";
	$conditionNumRows  =  " WHERE $tbl_iframe.businessID = $_SESSION[BusinessID] ORDER BY $tbl_iframe.loactionID DESC ";
	$totalNumRows	= $iframeDetails->totalNumRows($tbl_iframe, $conditionNumRows);
	$total_pages = $totalNumRows;
	if(isset($_GET['page'])){
		$page=$_GET['page'];
	} else{
		$page="";
	}
	$limit =10;	//how many items to show per page
    if($page){
		$start = ($page - 1) * $limit;	//first item to display on this page
	} else {
		$start = 0;
	}
	/*** numrow**/
	$condition =  "$tbl_iframe.loactionID = $tbl_location.ManageLocationId WHERE $tbl_iframe.businessID = $_SESSION[BusinessID] ORDER BY $tbl_iframe.loactionID DESC LIMIT  ".$start.", ".$limit."";
	$cols = "$tbl_iframe.*,$tbl_location.LocationName";
	$iframeData = $iframeDetails->selectTableJoinNew($tbl_iframe,$tbl_location,$join,$condition,$cols);
	/*** fetch All device Name**/
	?>
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function() {
			
			$('a.activeUser').click(function() {
				var statusMsg = $(this).attr('id');
				if ( confirm("Are you sure you want to "+statusMsg+" this category?") ) {
					var id = $(this).parent().parent().attr('id');
					var status = $(this).parent().parent().attr('status');
					var data = 'id='+id+'&status='+status;
					var parent = $(this).parent().parent();			
					$.ajax({
						type: "POST",
						url: "ajax_newchangestatus.php",
						data: data,
						cache: false,				
						success: function(data) {
							if(data == 1) {
								data = 'Status updated succesfully.';
								setTimeout(function(){ location.reload() }, 1000);							
							}
						}
					});		
				}
			});
			
			$('.pricingEdit').live('click',function() {
				var pid = $(this).attr('id');
				var str ='frameid='+pid;
				$.ajax ({
					type: "POST",
					url: "ajax_iframe.php",
					data: str,
					cache: false,
					success: function(result) {
						//alert(result);
						$('.overlay-bg-iframe').html(result).show(); //display your popup
						//$("#u_mess").html(result);
					}
				});
			});
		
			$('.close-btn-pricing').live('click',function() {
				$('.overlay-bg-iframe').html('').hide();
			});
		//. pricingEdit  
		
		
		});
	</script>

<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Appointment Widget</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="generate-iframe"><button class="addnewbtn">Generate New </button></a>
						</div>
					</div>
					<div class="card-body">
					<?php
						if( !empty($iframeData) ) {
							$i = 1;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>Location Name</th>
										<th>Date Added</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $iframeData as $dataemp ) {
											if(	$i%2 == 0 ){ 
												$bgdata = "bgnone";	
											} else {
												$bgdata = "bgdata";
											}	
											if(($_SESSION['id'] == $dataemp['Emp_ID']) && (strtoupper($_SESSION['Medicaldirector'])!="YES")){
												$currentLogin= "currentLogin";
											} else { 
												$currentLogin= "";
											}
									?>
												<tr class="treatment <?php echo $bgdata;?> <?php echo $currentLogin;?>" id="<?php echo $dataemp['id']?>" status="<?php echo $dataemp['status'];?>">
													<td class="span3 srtHeadEditEmp srtcontent"><label class="user-name"><?php echo $i;?></label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $dataemp['LocationName']?> </label></td>
													<td class="span6 srtHead srtcontent">
														<label id="" class="user-name" style="text-transform:none;">
															<?php echo date("F j, Y", strtotime($dataemp['dateAdded'])); ?>
														</label>
													</td>
													<td class="span6 cMain ">
														<?php 
														if( ( isset($_SESSION['loginuser']) && $_SESSION['loginuser']=="sitesuperadmin" ) || $_SESSION['Admin']=="TRUE" || ( strtoupper($_SESSION['Medicaldirector'])=="YES" ) || $_SESSION['id']==$dataemp['Emp_ID'] ) { ?>						
															<label class="user-name">
																<a class="pricingEdit" id="<?php echo $dataemp['iframeID']; ?>"><img src="<?php echo $domain; ?>/img/eye-img.jpg" title="view iframe"/></a>
															</label>
															<?php 
														} ?>
													</td>
												</tr><!--End @row-block-->
												<?php
												$i++;
										} //foreach end
									?>
								</tbody>
							</table>
							<div class="overlay-bg-iframe"></div>
						</div>
						<?php 
							echo $iframeDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<div class="loadingOuter"><img src="../images/loader.svg"></div>
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
