<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$importCsv = new dbFunctions();
	/*** fetch Add Manage location**/
	$tbl_manage_location = "tbl_manage_location";
	$condition = "ORDER BY ManageLocationId  DESC ";
	$cols = "*";
	$ManagesData = $importCsv->selectTableRows($tbl_manage_location,$condition);
	//echo count($ManagesData);
	/*
	// path where your CSV file is located
	//define('CSV_PATH','C:/wamp/www/');
	define('CSV_PATH','/var/www/frame/');
	// Name of your CSV file
	$csv_file = CSV_PATH . "p.csv";
	if (($getfile = fopen($csv_file, "r")) !== FALSE) { 
		$data = fgetcsv($getfile, 1000, ",");
		while (($data = fgetcsv($getfile, 1000, ",")) !== FALSE) {
			$num = count($data); 
			for ($c=0; $c < $num; $c++) {
				$result = $data;              
				$str = implode(" ", $result); 
				$slice = explode(" ", $str);
				$col1 = $slice[0]; 
				$col2 =str_replace("$","", $slice[1]);
				$col3 = str_replace("$","",$slice[2]); 
				$col4 = str_replace("$","",$slice[3]); 
				$col5 = str_replace("$","",$slice[4]); 
				$col6 = str_replace("$","",$slice[5]); 
				// SQL Query to insert data into DataBase
				$query = "INSERT INTO  tbl_pricing_table(Size,Rec_Price,discount5,discount10,savings5,savings10)
					VALUES('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
				$s=mysql_query($query); 
			}
		} 
	}
	echo "File data successfully imported to database!!"; */
?>
<?php 
	if(isset($_POST['upload'])) {
		$mimes = array('text/csv');
		if(in_array($_FILES['file']['type'],$mimes)) {			
			$csv_file = $_FILES['file']['tmp_name'];	
			if (($getfile = fopen($csv_file, "r")) !== FALSE) { 
				$data = fgetcsv($getfile, 1000, ",");
				while (($data = fgetcsv($getfile, 1000, ",")) !== FALSE) {
					$num = count($data); 
					for ($c=0; $c < $num; $c++) {
						$result = $data;              
						$str = implode(" ", $result); 
						$slice = explode(" ", $str);
						$col1 = $slice[0]; 
						$col2 =str_replace("$","", $slice[1]);
						$col3 = str_replace("$","",$slice[2]); 
						$col4 = str_replace("$","",$slice[3]); 
						$col5 = str_replace("$","",$slice[4]); 
						$col6 = str_replace("$","",$slice[5]); 
						// SQL Query to insert data into DataBase
						if($_POST['location'] =="2") {
							$query = "INSERT INTO  tbl_pricing_table(Size,Rec_Price,discount5,discount10,savings5,savings10)
								VALUES('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
							$s=mysql_query($query);
						}// if  close of DuLuth location	
						if($_POST['location'] =="3") {
							$query = "INSERT INTO  tbl_pricingminne_table(Size,Rec_Price,discount5,discount10,savings5,savings10)
								VALUES('".$col1."','".$col2."','".$col3."','".$col4."','".$col5."','".$col6."')";
							$s=mysql_query($query); 
						}// if close of  Minneapolis location	
						//End SQL Query to insert data into DataBase		
					}
				} 
			}
			$successmsg="<span class='successMsg' style='color:#008000'>File imported successfully.</span>";		  
		} else {
			$error =  "<span class='error' style='margin-top: 6px;'>Please upload only csv file!</span>";
		}
	}
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#csvUploaform").validate({
			rules: {
				location: "required",
                csvUploadFile: "required",					
			},                
			messages: {
				location: "This is required field.",
				csvUploadFile: "This is required field.",					
			}
		});
		/* $('#uploadtBtn').click(function() {
			if( $("#csvUploaform").valid()){
				uploadCsv_form();
			} else {
				$("#insertResult").hide();		
			}
		});*/
	});
	/*
	function uploadCsv_form() {
		var str= 'file'+ file;
		$.ajax({
			type: "POST",
			url: "ajax_importcsv.php",
			data: str,
			cache: false,
			success: function(result) {		
				$("#insertResult").show();
				$("#insertResult").html(result);
				//setTimeout(function() {location.reload()	}, 2000);				   
			}
		});					
	}
	*/
</script>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHeadReport">Manage pricing </h1>
		<div class="addNewReport"><a class="empLinks" href="revenuesreport.php"class="submit-btn"> </a></div>		
		<div class="addNewReport"><a class="empLinks" href="marketing.php" class="submit-btn"> </a></div>
	</div>	<!--heading-container-->
	<div class="user-entry-block">	
		<div class="row">				
			<form method="post" name="csvUploaform" id="csvUploaform" enctype="multipart/form-data">	
				<div class="span6">
					<label id="" class="user-name">Select location:</label>
					<select class="select-option" name="location" id="location">
						<option value="" >Select location</option>
						<?php
							if($ManagesData !=NULL) {
								foreach($ManagesData as $manages) {
							?>
									<option value="<?php echo $manages['ManageLocationId']?>" ><?php echo $manages['LocationName']?></option>
							<?php	
								}
							}		
						?>
					</select>
				</div><!--end span6-->	
				<div class="span6 last">
					<label id="" class="user-name">Select csv file:</label>
					<input type="file" name="file" id="file" class="text-input-field"/>
					<?php
					if(isset($error)){ 
						echo $error;
					} ?>
				</div><!--end span6-->
				<div class="span6">
					<input id="uploadtBtn" class="submit-btn" type="submit" value="Upload" name="upload"/>
					<div id="insertResult" style="float: right; margin-top: 16px;"><?php if(isset($successmsg)){echo $successmsg;}?></div>
				</div>
			</form>
		</div><!--End row-->
	</div><!--@user-entry-block-->
</div><!--@form-container-->
</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
