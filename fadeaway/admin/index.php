<?php
	$cookie_name = 'dashbord';
	unset($_COOKIE[$cookie_name]);
	// empty value and expiration one hour before
	$res = setcookie($cookie_name, '', time() - 3600);
	
	define("SITE_URL","http://dexteroustechnologies.co.in/mylaserdata/fadeaway/");
	session_start(); 
	session_unset(); 
	session_destroy(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
		<title>Login</title>
		<link href="<?php echo SITE_URL; ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'/>
		<link href="<?php echo SITE_URL; ?>css-new/sb-admin-2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo SITE_URL; ?>css-new/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" language="javascript" src="<?php echo SITE_URL; ?>js/html5.js"></script>
		<script type="text/javascript"  language="javascript" src="<?php echo SITE_URL; ?>js/PIE.htc"></script>
		<script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery-1.9.1.js" ></script>
		<script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery.validate.min.js" ></script>
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL; ?>css/theme4.css" />
		<script>
			jQuery(document).ready(function() {
				jQuery("#login_form").validate({
					rules: {
						username: {
							required: true
						},
						password: {
							required: true
						}
					},					
					messages: {
						username: {
							required: "Please enter username." 
						},
						password: {
							required: "Please enter password."
						}
					}
				});
			});
		</script>
		<style>
			.no-script {margin-bottom: -19px !important;padding-top: 10px;text-align: center;font-size:14px}
		</style>
	</head>
	<?php
		error_reporting(0);
		session_start();
		if( isset($_SESSION['id']) || @$_SESSION['id'] != null ) {
			"Welcome".$_SESSION['First_Name']." ".$_SESSION['Last_Name'];
		}
	?>
	<body>
		<noscript><div class="no-script"><b>Warning:</b> Javascript must be enabled to use this application correctly. Here are the <a href="http://www.enable-javascript.com" target="_blank"> instructions how to enable JavaScript in your web browser</a></div></noscript>
		
		<div class="col-xl-12 col-lg-12 col-md-12 pad-none">
			<div class="card o-hidden border-0">
				<div class="card-body p-0">
					<!-- Nested Row within Card Body -->
					<div class="row">
						<div class="col-lg-8 d-none d-lg-block login-right-con">
							<div class="col-12 login-logo-con">
								<img src="<?php echo SITE_URL; ?>img/login-logo-con.png" alt=""/> 
							</div>
							<div class="login-heading-con">
								<h1>Stay Organized <br>Improve Efficiency<br> Track Sales</h1>        	
								<h2>Practice Mangement Software <span>for Laser Tattoo Removal Clinics</span></h2>
							</div>
							<p class="login-bottom-con">Copyright Northeast Laser Tattoo Removal, LLC <?php echo date("Y"); ?></p>
						</div>
						<div class="col-lg-4 login-inner-auto-con">
							<div class="login-right-inner">
								<div class="login-logo">
									<img src="http://localhost/mylaserdata/fadeaway/img/login-logo-con.png"/>
								</div>
								<div class="text-center">
									<h2 class="login-text-con">Login<span>Welcome back! Please login here!</span></h2>
								</div>
								<?php
								if(isset($_GET['login']) && $_GET['login']=='fail') {
									echo '<p class="err_incorrect">Username or Password does not exist !</p>';				
								}
								if(isset($_GET['login']) && $_GET['login']=='block') {
									echo '<p class="err_incorrect">Your account is blocked. Please contact to admin.</p>';				
								}
								if(isset($_GET['login']) && $_GET['login']=='expire') {
									echo '<p class="err_incorrect">Your account is expired. Please contact to your business admin.</p>';				
								}
								if(isset($_GET['logout']) && $_GET['logout']=='success') {
									echo '<p class="err_incorrect success">You are successfully logged out.</p>';				
								}
								?>
								<form class="user login-form-con" method="post" action="<?php echo SITE_URL; ?>includes/login.php" id="login_form" name="login_form">
									<div class="form-group">
										<input class="form-control form-control-user" name="username" id="username" type="text" value="<?php echo $_COOKIE['remember_me']; ?>" placeholder="User name"/>
									</div>
									<div class="form-group">
									<input class="form-control form-control-user" name="password" id="password" type="password" placeholder="Password" />
									</div>
									<div class="text-left forgot-password">
										<a class="small" href="forgotpassword">Forgot Password?</a>
									</div>
									<div class="form-group login-remember-con">
										<div class="custom-control custom-checkbox small">
											<input name="loginType" type="checkbox" class="custom-control-input" value="client" id="customCheck" />
											<label class="custom-control-label" for="customCheck">Login as a client</label>
										</div>
										<div class="custom-control custom-checkbox small">
											<input name="remember_me" type="checkbox" class="custom-control-input" value="1" <?php if(isset($_COOKIE['remember_me'])) { echo 'checked="checked"';} ?> id="customCheck1"/>
											<label class="custom-control-label" for="customCheck1">Remember Me</label>
										</div>
									</div>
									<input type="submit" name="submit" style="cursor:pointer" class="submit-button" value="Log In">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="<?php echo SITE_URL; ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="<?php echo SITE_URL; ?>vendor/jquery-easing/jquery.easing.min.js"></script>
		<script src="<?php echo SITE_URL; ?>js-new/sb-admin-2.min.js"></script>

	</body>
</html>
