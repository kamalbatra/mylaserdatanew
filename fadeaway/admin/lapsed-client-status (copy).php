<?php
include("../includes/dbFunctions.php");
$clientDetails = new dbFunctions();
if( !in_array(5,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard.php");
	</script>
<?php }
if(isset($_POST['reason']) && $_POST['reason'] != ''){
	$reasondata['ClientID'] = $_POST['client_id'];
	$reasondata['lapsed_close_reason'] = $_POST['reason'].'<br/>Status updated on: '.date("M d, Y");
	$clientDetails->update_user('tbl_clients',$reasondata);
	echo $reasondata['lapsed_close_reason'];
	die;
}
include('admin_includes/header.php');
if(isset($_GET['orderby'])&& $_GET['name'] ){
	if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
		$order ="DESC";
		$name = "FirstName";
		$ascDescImg="../images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
		$order ="ASC";
		$name = "FirstName";
		$ascDescImg="../images/s_asc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
		$order ="DESC";
		$name = "LastName";
		$ascDescImg="../images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
		$order ="ASC";
		$name = "LastName";
		$ascDescImg="../images/s_asc.png";
	}
}else{
	$order ="DESC";
	$name = "ClientID";
	$ascDescImg="../images/s_desc.png";
}
?>
<script type="text/javascript">
$(function(){
	$(".searchclient").keyup(function(){ 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		if(searchid!=''){
			$.ajax({
				type: "POST",
				url: "ajax_clientsearch.php",
				data: dataString,
				cache: false,
				success: function(html){
				   $("#resultClientSerch").html(html).show();
				}
			});
		}return false;    
	});
	jQuery("#result").live("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		var decoded = $("<div/>").html($name).text();
		$('#searchid').val(decoded);
		$('#fname').val(decoded);
	});
	
	jQuery("#resultClientSerch div.show").live("click", function(){
		var hrefval = $(this).find("a").attr("href");
		var hrefvalsplit = hrefval.split("?");
		window.location.replace('lapsed-client-status.php?'+hrefvalsplit[1]);
		return false;
	});
	
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
			jQuery("#result").fadeOut(); 
		}
	});
	$('#searchid').click(function(){
		jQuery("#result").fadeIn();
	});
	
	$(".update-lost-status input[type='button']").click(function(){
		var reason = $(this).prev().val();
		if(reason == '')
			return false;
		var cnfrm = confirm("Are you sure you want to change status?");
		if(cnfrm){
			var client_id = $(this).attr('rel');
			var _this = $(this);
			$.ajax({
				url: 'lapsed-client-status.php',
				type: "POST",
				data: {client_id, reason},
				success:function(res){
					$(_this).parent().html(res)
					alert("Status changed successfully.");
				}
			});
		}else{
			$(this).prev().val('');
		}
	});
});
</script>
<?php
/*** fetch all Client with location**/
$table= "tbl_clients";
$join="INNER";
$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, AES_DECRYPT(tbl_clients.PhoneNumber, '".SALT."') AS PhoneNumber, tbl_clients.Location, tbl_clients.TimeStamp, tbl_clients.lost_close_reason, tbl_clients.lapsed_close_reason";
/*** numrows**/
$adjacents = 3;
$reload="lapsed-client-status.php";
if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
	$conditionNumRows  = " WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY FirstName ";
}else{
	$conditionNumRows  = " WHERE lapsed_mail_status = 0 AND tbl_clients.BusinessID = $_SESSION[BusinessID] ORDER BY tbl_clients.ClientID DESC";
}
$totalNumRows = $clientDetails->totalNumRows($table,$conditionNumRows);
$total_pages = $totalNumRows;
//$page="";
if(isset($_GET['page'])){
	$page=$_GET['page'];
}
else{
	$page="";
}
$limit = 10;                                  //how many items to show per page
if($page)
	$start = ($page - 1) * $limit;          //first item to display on this page
else
	$start = 0; 
/*** numrow**/

if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
  $condition = " WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY FirstName ";
}else{
  $condition = " WHERE lapsed_mail_status = 0 AND tbl_clients.BusinessID = $_SESSION[BusinessID] ORDER BY ".$name." ".$order." LIMIT ".$start.", ".$limit." ";
}
$clientdata	= $clientDetails->selectTableRows($table,$condition,$cols);
/*** fetch All client with location Name**/
?>
<div class="form-container" id="top1">
	   <div class="heading-container">
		  <h1 class="heading empHead">Lapsed Clients</h1>
		  <div class="addNew"><a class="empLinks" href="lost-client-status.php"> Lost Clients </a></div>		 
	   </div>
		<div class="user-entry-block">
			 <div class="row">
				 <div class="span6"></div>
				 <div class="span6 last">
					 <label id="Label1" class="user-name">First name, last name or phone number:</label>					 
				  <input type="text" class="searchclient text-input-field" id="searchid" />
		          <div id="resultClientSerch"></div>
		         </div> 
		     </div>
			<div class="row sortingHead">	
				<div class="span3 srtHead srtHeadBorder" style="width:22%"> <a title="<?php echo $order;?>" href="lapsed-client-status.php?orderby=<?php echo $order;?>&name=firstname"> Client Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a> </div>
				<div class="span3 srtHeadloc srtHeadBorder" style="width:18%"> Office Location(s) </div>
				<div class="span3 srtHeadEdit srtHeadBorder" style="width:15%">Phone No.</div>
				<div class="span3 srtHeadEdit srtHeadBorder" style="width:14%">Registered On</div>
				<div class="span3 srtHeadBorder" style="width:31%">Status</div>
			</div>
        	<?php if(!empty($clientdata)) { $i = 0;
				foreach($clientdata as $cldata){
					if($i%2==0){$bgdata = "bgnone";	}
						else{$bgdata = "bgdata";}								
			?>	
                   <div class="row  <?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>" style="height:53px">
					 <div class="span3 srtHead srtcontent" style="width:22%">
						<label id="" class="user-name"><?php echo ucfirst($cldata['FirstName'].' '.$cldata['LastName']);?> </label>						
					</div>					
					<div class="span3 srtHeadloc srtcontent" style="width:18%">	<label id="" class="user-name">
					<?php 
					      if($cldata['Location'] !=""){							  
					         $clentLocation = explode(",",$cldata['Location']);
					         for($j=0 ; $j<count($clentLocation);$j++){
							$tbl_manage_location	= "tbl_manage_location";
							$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
							$cols="*";
							$locationData	= $clientDetails->selectTableSingleRow($tbl_manage_location,$condition1);
							  echo $locationData['LocationName'];							  
							  if($j !=count($clentLocation)-1){
							  echo ",&nbsp;";
						     }
						  }//for loop close
						}
                     ?>
					  </label>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align" style="width:15%">	
					  <label id="" class="user-name"><?php echo $cldata['PhoneNumber']; ?></label>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align" style="width:14%">	
					  <label id="" class="user-name"><?php echo date("M d, Y",strtotime($cldata['TimeStamp'])); ?></label>
					</div>
					<div class="span3 srtHeadEdit srtcontent" style="width:31%">	
					  <label class="user-name update-lost-status">
						  <?php if($cldata['lapsed_close_reason'] == ''){?>
						  <select class="text-input-field" style="padding:2px;width:70%;margin-right:5px">
							  <option value="">Select Reason</option>
							  <option>Treatment Completed</option>
							  <option>Client Request</option>
						  </select><input type="button" value="Update" rel="<?php echo $cldata['ClientID']; ?>" class="submit-btn" style="margin-top: 0;padding: 5px 10px">
						  <?php }else echo $cldata['lapsed_close_reason']; ?>
					  </label>
					</div>					
			    </div><!--End @row-->				  
			  <?php $i++;  }?>
			  <?php echo $clientDetails->paginateShow($page,$total_pages,$limit,$adjacents,$reload)?>
			  <div id="statuResult"></div>
			  <?php }else echo "<div class='not-found-data'>No client found.</div>"; ?>
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
