<?php
include("../includes/dbFunctions.php");
$clientDetails = new dbFunctions();
if( !in_array(5,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
if(isset($_SESSION['loginuser']) && $_SESSION['loginuser'] == "sitesuperadmin"){?>
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
if(isset($_POST['reason']) && $_POST['reason'] != ''){
	$reasondata['ClientID'] = $_POST['client_id'];
	$reasondata['lapsed_close_reason'] = $_POST['reason'].'<br/>Status updated on: '.date("M d, Y");
	$reasondata['lapsed_mail_status'] = 1;
	$clientDetails->update_user('tbl_clients',$reasondata);
	
	/****************** Unsubscribe From Sendy ******************/
	require('../SendyPHP/SendyPHP.php');
	$list_name = "Lapsed Newsletter Clients";
	$sendy = new \SendyPHP\SendyPHP($list_name);
	$listdata = $sendy->get_list_id($list_name);
	$sendy->setListId($listdata['list_id']);
	$sendy->unsubscribe($_POST['client_email']);
	/**************** End unsubscribe From Sendy ****************/
	
	echo $reasondata['lapsed_close_reason'];
	die;
}
include('admin_includes/header.php');
if(isset($_GET['orderby'])&& $_GET['name'] ){
	if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
		$order ="DESC";
		$name = "FirstName";
		$ascDescImg="../images/s_desc.png";
		$lastTreatAscDescImg="../images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
		$order ="ASC";
		$name = "FirstName";
		$ascDescImg="../images/s_asc.png";
		$lastTreatAscDescImg="../images/s_desc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
		$order ="DESC";
		$name = "LastName";
		$ascDescImg="../images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
		$order ="ASC";
		$name = "LastName";
		$ascDescImg="../images/s_asc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lasttreat"){
		$order ="DESC";
		$name = "Date_of_treatment";
		$lastTreatAscDescImg="../images/s_desc.png";
		$ascDescImg="../images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lasttreat"){
		$order ="ASC";
		$name = "Date_of_treatment";
		$lastTreatAscDescImg="../images/s_asc.png";
		$ascDescImg="../images/s_desc.png";
	}
}else{
	$order ="DESC";
	$name = "ClientID";
	$ascDescImg="../images/s_desc.png";
	$lastTreatAscDescImg="../images/s_desc.png";
}
?>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css" />
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootbox.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".searchclient").keyup(function(){ 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		if(searchid!=''){
			$.ajax({
				type: "POST",
				url: "ajax_lapsedlostclientsearch.php",
				data: {'search' : searchid, 'searchfor': 'lapsed'},
				cache: false,
				success: function(html){
				   $("#resultClientSerch").html(html).show();
				}
			});
		}return false;    
	});
	jQuery("#result").live("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		var decoded = $("<div/>").html($name).text();
		$('#searchid').val(decoded);
		$('#fname').val(decoded);
	});
	
	jQuery("#resultClientSerch div.show").live("click", function(){
		var hrefval = $(this).find("a").attr("href");
		var hrefvalsplit = hrefval.split("?");
		window.location.replace('lapsed-client-status?'+hrefvalsplit[1]);
		return false;
	});
	
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
			jQuery("#result").fadeOut(); 
		}
	});
	$('#searchid').click(function(){
		jQuery("#result").fadeIn();
	});
	
	$(".update-lost-status input[type='image']").click(function(){
		var _this = $(this);
		var reason = _this.prev().val();
		var client_id = _this.attr('rel');
		var client_email = _this.prev().prev().val();
		if(reason == ''){
			bootbox.alert("Please select a reason.");
			return false;
		}
		bootbox.confirm("Are you sure you want to change status?", function(result){
			if(result){
				$.ajax({
					url: 'lapsed-client-status.php',
					type: "POST",
					data: {client_id, reason, client_email},
					success:function(res){
						$(_this).parent().html(res)
						$(".showstatusupdate").text("Status changed successfully.");
					}
				});
			}else{
				_this.prev().val('');
			}
		});
	});
});
</script>
<?php
/*** fetch all Client with location**/
$table1 = "tbl_treatment_log";
$table2 = "tbl_clients";
$join = "INNER";
$currentDate = date("Y-m-d 00:00:00");
$cols = "AES_DECRYPT(Email, '".SALT."') AS Email, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber, tbl_clients.Location, tbl_clients.TimeStamp, lapsed_close_reason, tbl_treatment_log.ClientID, tbl_treatment_log.Date_of_treatment";

/*** numrows**/
$adjacents = 3;
$reload="lapsed-client-status.php";
if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
	$conditionNumRows = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' AND tbl_clients.TimeStamp < '".$currentDate."' GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=90 and DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) <=150 ORDER BY FirstName";
}else{
	$conditionNumRows = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.TimeStamp < '".$currentDate."' AND tbl_clients.BusinessID = $_SESSION[BusinessID] GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=90 and DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) <=150 ORDER BY tbl_clients.ClientID DESC";
}
$totalNumRows = $clientDetails->joinTotalNumRows($table1,$table2,$join,$conditionNumRows);
$total_pages = $totalNumRows;
//$page="";
if(isset($_GET['page'])){
	$page=$_GET['page'];
}
else{
	$page="";
}
$limit = 10;                                  //how many items to show per page
if($page)
	$start = ($page - 1) * $limit;          //first item to display on this page
else
	$start = 0; 
/*** numrow**/

if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
	$condition = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' AND tbl_clients.TimeStamp < '".$currentDate."' GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=90 and DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) <=150 ORDER BY FirstName";	  
}else{
	$condition = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.TimeStamp < '".$currentDate."' AND tbl_clients.BusinessID = $_SESSION[BusinessID] GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=90 and DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) <=150 ORDER BY ".$name." ".$order." LIMIT ".$start.", ".$limit." ";
}
$clientdata	= $clientDetails->selectLapsedLostTableJoin($table1,$table2,$join,$condition,$cols);
/*** fetch All client with location Name**/
?>
<div class="form-container" id="top1">
	   <div class="heading-container">
		  <h1 class="heading empHead" style="width:40%">Lapsed Clients</h1>
		  <div style="float:left;margin-top:10px;color:green;width:30%" class="showstatusupdate">&nbsp;</div>
		  <div class="addNew"><a class="empLinks" href="lost-client-status"> Lost Clients </a></div>		 
	   </div>
		<div class="user-entry-block">
			 <div class="row">
				 <div class="span6"></div>
				 <div class="span6 last">
					 <label id="Label1" class="user-name">First name, last name or phone number:</label>					 
				  <input type="text" class="searchclient text-input-field" id="searchid" />
		          <div id="resultClientSerch"></div>
		         </div> 
		     </div>
			<div class="row sortingHead">	
				<div class="span3 srtHead srtHeadBorder" style="width:22%"> <a title="<?php echo $order;?>" href="lapsed-client-status?orderby=<?php echo $order;?>&name=firstname"> Client Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a> </div>
				<div class="span3 srtHeadloc srtHeadBorder" style="width:18%"> Office Location(s) </div>
				<div class="span3 srtHeadEdit srtHeadBorder" style="width:15%">Phone No.</div>
				<div class="span3 srtHeadEdit srtHeadBorder" style="width:14%"><a title="<?php echo $order;?>" href="lapsed-client-status?orderby=<?php echo $order;?>&name=lasttreat">Last Treatment <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $lastTreatAscDescImg;?>"></a></div>
				<!--div class="span3 srtHeadEdit srtHeadBorder" style="width:14%"> Register On</div-->
				<div class="span3 srtHeadBorder" style="width:31%">Status</div>
			</div>
        	<?php if(!empty($clientdata)) { $i = 0;
				foreach($clientdata as $cldata){
					if($i%2==0){$bgdata = "bgnone";	}
						else{$bgdata = "bgdata";}								
			?>	
                   <div class="row  <?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>" style="min-height:53px">
					 <div class="span3 srtHead srtcontent" style="width:22%">
						<label id="" class="user-name"><?php echo ucfirst($cldata['FirstName'].' '.$cldata['LastName']);?> </label>						
					</div>					
					<div class="span3 srtHeadloc srtcontent" style="width:18%">	<label id="" class="user-name">
					<?php 
					      if($cldata['Location'] !=""){							  
					         $clentLocation = explode(",",$cldata['Location']);
					         for($j=0 ; $j<count($clentLocation);$j++){
							$tbl_manage_location	= "tbl_manage_location";
							$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
							$cols="*";
							$locationData	= $clientDetails->selectTableSingleRow($tbl_manage_location,$condition1);
							  echo $locationData['LocationName'];							  
							  if($j !=count($clentLocation)-1){
							  echo ",&nbsp;";
						     }
						  }//for loop close
						}
                     ?>
					  </label>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align" style="width:15%">	
					  <label id="" class="user-name"><?php echo $cldata['PhoneNumber']; ?></label>
					</div>
					<div class="span3 srtHeadEdit srtcontent text-align" style="width:14%">	
					  <label id="" class="user-name"><?php echo date("M d, Y",strtotime($cldata['Date_of_treatment'])); ?></label>
					  <!--label id="" class="user-name"><?php echo date("M d, Y",strtotime($cldata['TimeStamp'])); ?></label-->
					</div>
					<div class="span3 srtHeadEdit srtcontent" style="width:31%;padding:8px 0 2px 5px">	
					  <label class="user-name update-lost-status">
						  <?php if($cldata['lapsed_close_reason'] == ''){?>
						  <input type="hidden" value="<?php echo $cldata['Email']; ?>" name="client_email" />
						  <select class="text-input-field" style="padding:2px;width:75%;margin-right:5px;height:35px">
							  <option value="">Select Reason</option>
							  <option>Treatment Completed</option>
							  <option>Client Request</option>
						  </select><input title="Update Status" type="image" src="../images/update.png" rel="<?php echo $cldata['ClientID']; ?>" style="border:none">
						  <?php }else echo $cldata['lapsed_close_reason']; ?>
					  </label>
					</div>					
			    </div><!--End @row-->				  
			  <?php $i++;  }?>
			  <?php echo $clientDetails->paginateShow($page,$total_pages,$limit,$adjacents,$reload)?>
			  <div id="statuResult"></div>
			  <?php }else echo "<div class='not-found-data'>No client found.</div>"; ?>
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
