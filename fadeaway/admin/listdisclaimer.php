<?php
session_start();
$doaminPath = $_SERVER['DOMAINPATH'];
$domain = $_SERVER['DOMAIN'];
include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
include("../includes/dbFunctions.php");
$empDetails	= new dbFunctions();
if( !in_array(6,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
/*** fetch all Employee location**/
$tbl_employees	= "tbl_legal_disclaimer";
/*** numrows**/
$adjacents = 3;
$reload="listdisclaimer";
$conditionNumRows  =  " WHERE bussines_id = ".$_SESSION[BusinessID]." and user_id=".$_SESSION[id]." ORDER BY id ASC ";
$totalNumRows	= $empDetails->totalNumRows($tbl_employees, $conditionNumRows);
$total_pages = $totalNumRows;
if(isset($_GET['page'])){
	$page=$_GET['page'];
} else{
	$page="";
}
$limit =10;	//how many items to show per page
if($page){
	$start = ($page - 1) * $limit;	//first item to display on this page
} else {
	$start = 0;
}
/*** numrow**/
$condition = "WHERE bussines_id = ".$_SESSION[BusinessID]." and user_id=".$_SESSION[id]." ORDER BY id ASC LIMIT  ".$start.", ".$limit."";
$cols="*";
$empData = $empDetails->selectTableRows($tbl_employees,$condition);
/*** fetch All device Name**/
?>
<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function(){
		//Delete flunce from database
		$('a.activeUser').click(function(){
			var statusMsg =$(this).attr('id'); 
			if (confirm("Are you sure you want to "+statusMsg+" this user?")){
				$('.loadingOuter').show();
				var id = $(this).parent().parent().attr('id');
				var status = $(this).parent().parent().attr('status');
				var data = 'Emp_ID=' + id +'&status='+status;
				var parent = $(this).parent().parent();			
				//alert(data);
				$.ajax({
					type: "POST",
					url: "ajax_changetatus.php",
					data: data,
					cache: false,				
					success: function(data){						   
						//parent.fadeOut('slow', function() {$(this).show();});
						setTimeout(function() {location.reload()	}, 1000);
						$("#statuResult").html(data);
				    }
				});			
			}
		});
	});
</script>

<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Legal Disclaimers</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="update_disclaimer"><button class="addnewbtn">Add Disclaimer </button></a>
						</div>
					</div>
					<div class="card-body">
					<?php
						if( !empty($empData) ) {
							$i = 0;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>Version</th>
										<th>From</th>
										<th>To</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $empData as $dataemp ) {
											$i++; $to="";		
											if($i%2==0){$bgdata = "bgnone";	}
												else{$bgdata = "bgdata";}							
											if($_SESSION['id']==$dataemp['Emp_ID']){
												$currentLogin= "currentLogin";
											}else{ $currentLogin= "";}
											if($dataemp['date_to']!='0000-00-00'){
											$to=date('F d,Y',strtotime($dataemp['from']));
											}
									?>
												<tr class="treatment <?php echo $bgdata;?> <?php echo $currentLogin;?>" id="<?php echo $dataemp['Emp_ID']?>" status="<?php echo $dataemp['status'];?>">
													<td class="span3 srtHeadEditEmp srtcontent"><label id="" class="user-name"><?php echo $i; ?> </label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $dataemp['version']?></label></td>
													<td class="span6 srtHead srtcontent">
														<label id="" class="user-name" style="text-transform:none;"> <?php echo date('F d,Y',strtotime($dataemp['from']));?>	</label>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<label id="" class="user-name" style="text-transform:none;"> <?php echo $to; ?>	</label>
													</td>
												</tr><!--End @row-block-->
												<?php
										} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $empDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<div class="loadingOuter"><img src="../images/loader.svg"></div>
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
