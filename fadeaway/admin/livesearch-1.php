<?php
	include("../includes/dbFunctions.php");
	$domain=$_SERVER['DOMAIN'];
	$consentForms = new dbFunctions();
	if(isset($_GET['name']) && $_GET['name'] !=null) {
		$clientid= preg_replace("/[^0-9,.]/", "", $_GET['name']);
		$clientid = str_replace('.','',$clientid);
		 $clientid = preg_replace('/[;.,-]/', '', $clientid);
		if(isset($clientid) && $clientid !="") {
			$table = "tbl_clients";
			$condition = " where ClientID=".$clientid." ";
			$cols=" ClientID,AES_DECRYPT(FirstName, '".SALT."') AS FirstName,AES_DECRYPT(LastName, '".SALT."') AS LastName,AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber,AES_DECRYPT(Occupation, '".SALT."') AS Occupation,AES_DECRYPT(Age, '".SALT."') AS Age ";
			$sizedata = $consentForms->selectTableSingleRow($table,$condition,$cols);
			// Get medicazl directot note
			$tableName = "tbl_consent_form";		
			$conditionName = " where ClientID=".$clientid." ";
			$colsName="*";
			$clntData = $consentForms->selectTableData($tableName,$conditionName,$colsName);
?>
			<div class="client-info" style="font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 14px;" >
			<?php 
				echo "ClientID : ".$sizedata['ClientID']."<br/>";
				echo "Age : ".$sizedata['Age']."<br/>";
				echo "Occupation : ".$sizedata['Occupation']."<br/>";
				echo "Phone Number : ".$sizedata['PhoneNumber']."<a onclick='aa(".$sizedata['ClientID'].")' class='edit-btn' href='javascript:void(0);'>Edit Details</a>";
			?>
				<form action="process.php" method="post" name="clientformsearch">
					<input type="hidden" name="clientid" id="clientid" value="<?php echo $sizedata['ClientID']; ?>"/>
					<input type="hidden" name="clientfname" id="clientfname" value="<?php echo $sizedata['FirstName']; ?>"/>
					<input type="hidden" name="clientlname" id="clientlname" value="<?php echo $sizedata['LastName']; ?>"/>
				</form>
			</div>
			<!--- Medical director  note or red --> 
			<fieldset class="client-info" style="float:left;padding: 0 6px;font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 14px;">
				<legend>
					<span id="showPanel"  style='display:none;' > <img src="<?php echo $domain; ?>/images/plus.png" alt="Plus">  <span class="txtfont">Important information </span> </span>
					<span id="hidePanel" style='display:block;' > <img src="<?php echo $domain; ?>/images/mm.png" alt="minus" style="width: 22px;"><span class="txtfont">Important information </span> </span> 
				</legend>
				<div class="client-info-" id="detailpanel" style="" >	
				<?php
					if($clntData == '') {
						echo "<div id='missingconsent' class='missingconsent'>Missing Consent Form</div>"; ?>
						<div class="consentformagain"><a href="<?php echo $domain.'/admin/fullInfo.php?&clientId='.$sizedata['ClientID'] ?>">Please fill consent form again</a></div>
				<?php 
					} else {
				?>
						<input type="hidden" name="techreview" id="techreview" value="<?php echo $clntData->TechnicianSignature; ?>" />
					<?php
						foreach($clntData as $key=>$data) {
							if($data =="Yes") {
					?> 
								<div class="keyinfo" id="keyinfo" ><?php echo $key ?></div>
								<div class="datainfo" id="datainfo"><?php echo $data ?></div>
					<?php	
							}
							if($key  == "MedicalDirectorNotes") {
								echo '<div class="medinfo" id="keyinfo">Medical Director Notes : </div>';	
								echo '<div class="noteinfo" id="keyinfo" >'.$data.'</div>'	;
							}
						}
					} 
				?>
				</div>	 
			</fieldset> 
			<!--- Medical director  note or red --> 
		<?php
		} 	
	} else {
		echo "Result not found";
	}
	?>
