<?php
	include("../includes/dbFunctions.php");
	$domain=$_SERVER['DOMAIN'];
	$consentForms = new dbFunctions();
	if(isset($_GET['name']) && $_GET['name'] !=null) {
		$clientid= preg_replace("/[^0-9,.]/", "", $_GET['name']);
		$clientid = str_replace('.','',$clientid);
		 $clientid = preg_replace('/[;.,-]/', '', $clientid);
		if(isset($clientid) && $clientid !="") {
			$table = "tbl_clients";
			$condition = " where ClientID=".$clientid." ";
			$cols=" ClientID,AES_DECRYPT(FirstName, '".SALT."') AS FirstName,AES_DECRYPT(LastName, '".SALT."') AS LastName,AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber,AES_DECRYPT(Occupation, '".SALT."') AS Occupation,AES_DECRYPT(Age, '".SALT."') AS Age ";
			$sizedata = $consentForms->selectTableSingleRow($table,$condition,$cols);
			// Get medicazl directot note
			$tableName = "tbl_consent_form";		
			$conditionName = " where ClientID=".$clientid." ";
			$colsName="*";
			$clntData = $consentForms->selectTableData($tableName,$conditionName,$colsName);
?>
			<div class="card shadow mb-4 clientDetailInfo">
				<div class="client-info" style="font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 14px;" >
					<div class="clientDetailOuter">
						<div class="clientdeatilhalf">
							<div class="clientdetailsblk">
								<div class="clientDetailtitle">
									ClientID
								</div>
								<div class="clientDetailvalue">
									<?php echo $sizedata['ClientID']; ?>
								</div>
							</div>
						</div>
						<div class="clientdeatilhalf">
							<div class="clientdetailsblk">
								<div class="clientDetailtitle">
									Occupation
								</div>
								<div class="clientDetailvalue">
									<?php echo $sizedata['Occupation']; ?>
								</div>
							</div>
						</div>
						<div class="clientdeatilhalf">
							<div class="clientdetailsblk">
								<div class="clientDetailtitle">
									Age
								</div>
								<div class="clientDetailvalue">
									<?php echo $sizedata['Age']; ?>
								</div>
							</div>
						</div>
						<div class="clientdeatilhalf">
							<div class="clientdetailsblk">
								<div class="clientDetailtitle">
									Phone Number
								</div>
								<div class="clientDetailvalue">
									<?php echo $sizedata['PhoneNumber']; ?>
								</div>
							</div>
						</div>
					</div>
					<form action="process.php" method="post" name="clientformsearch">
						<input type="hidden" name="clientid" id="clientid" value="<?php echo $sizedata['ClientID']; ?>"/>
						<input type="hidden" name="clientfname" id="clientfname" value="<?php echo $sizedata['FirstName']; ?>"/>
						<input type="hidden" name="clientlname" id="clientlname" value="<?php echo $sizedata['LastName']; ?>"/>
					</form>
				</div>
			</div>
			<div class="importantinformation livesearch-edit-button">
				<div class="btnblocksfull">
					<div class="buttoncolblk">
						<!-- button class="btnconsent" style="background:#2a2a2a;">EDIT DETAILS</button -->
						<?php echo "<a onclick='aa(".$sizedata['ClientID'].")' class='edit-btn' href='javascript:void(0);'>Edit Details</a>"; ?>
					</div>
				</div>
			</div>
			<!--- Medical director  note or red --> 
			<div class="importantinformation">
				<span id="showPanel" style='display:none;' class="pointer" title="Expand"><img src="<?php echo $domain; ?>/images/plus.png" alt="Plus"><h2>Important information</h2></span>
				<span id="hidePanel" style='display:block;' class="pointer" title="Collapse"><img src="<?php echo $domain; ?>/images/mm.png" alt="minus" style="width: 22px;"><h2>Important information</h2></span>
				<div class="client-info-" id="detailpanel" style="" >
					<div class="card shadow mb-4 detailouterblkmain">
						<div class="clientDetailOuter">
							
								<?php
								if($clntData == '') {
								?>	
									<div class="clientdeatilhalf full">
										<div class="clientdetailsblk">
											<div id='missingconsent' class='missingconsent'>Missing Consent Form</div>
											<div class="consentformagain"><a href="<?php echo $domain.'/admin/fullInfo.php?&clientId='.$sizedata['ClientID'] ?>">Please fill consent form again</a></div>
										</div>
									</div>
								<?php 
								} else {
								?>
									<input type="hidden" name="techreview" id="techreview" value="<?php echo $clntData->TechnicianSignature; ?>" />
									<?php
									foreach($clntData as $key=>$data) {
										if($data =="Yes") {
									?>
											<div class="clientdeatilhalf full"><div class="clientdetailsblk">
												<div class="clientDetailtitle" id="keyinfo" ><?php echo $key ?></div>
												<div class="clientDetailvalue" id="datainfo"><?php echo $data ?></div>
											</div>	
											</div>	
									<?php
										}
										if($key  == "MedicalDirectorNotes") {
											echo '<div class="clientdeatilhalf full"><div class="clientdetailsblk">';	
											echo '<div class="clientDetailtitle" id="keyinfo">Medical Director Notes : </div>';	
											echo '<div class="clientDetailvalue" id="keyinfo" >'.$data.'</div>'	;
											echo '</div></div>'	;
										}
									}
								}
								?>							
							
						</div>
					</div>
				</div>
			</div>
			<!--- Medical director  note or red --> 
		<?php
		} 	
	} else {
		echo "Result not found";
	}
?>
