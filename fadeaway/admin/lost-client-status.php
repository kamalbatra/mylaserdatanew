<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
include("../includes/dbFunctions.php");
$clientDetails = new dbFunctions();
if( !in_array(5,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
if(isset($_SESSION['loginuser']) && $_SESSION['loginuser'] == "sitesuperadmin"){?>
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
if(isset($_POST['reason']) && $_POST['reason'] != ''){
	$reasondata['ClientID'] = $_POST['client_id'];
	$reasondata['comment'] = $_POST['client_msg'];
	$reasondata['last_status_updated'] = date("Y-m-d");
	$reasondata['lost_close_reason'] = $_POST['reason'].'<br/>Status updated on: <br/>'.date("M d, Y");
	$reasondata['lost_mail_status'] = 1;
	$clientDetails->update_user('tbl_clients',$reasondata);
	
	/****************** Unsubscribe From Sendy ******************/
	require('../SendyPHP/SendyPHP.php');
	$list_name = "Lost Newsletter Clients";
	$sendy = new \SendyPHP\SendyPHP($list_name);
	$listdata = $sendy->get_list_id($list_name);
	$sendy->setListId($listdata['list_id']);
	$sendy->unsubscribe($_POST['client_email']);
	/**************** End unsubscribe From Sendy ****************/
	echo $reasondata['lost_close_reason'];
	die;
}
$doaminPath = $_SERVER['DOMAINPATH'];
$domain = $_SERVER['DOMAIN'];
include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
if(isset($_GET['orderby'])&& $_GET['name'] ){
	if($_GET['orderby']=="ASC" && $_GET['name']=="firstname"){
		$order ="DESC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_desc.png";
		$lastTreatAscDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="firstname"){		
		$order ="ASC";
		$name = "FirstName";
		$ascDescImg=$domain."/images/s_asc.png";
		$lastTreatAscDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
		$order ="DESC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
		$order ="ASC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_asc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lasttreat"){
		$order ="DESC";
		$name = "Date_of_treatment";
		$lastTreatAscDescImg=$domain."/images/s_desc.png";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lasttreat"){
		$order ="ASC";
		$name = "Date_of_treatment";
		$lastTreatAscDescImg=$domain."/images/s_asc.png";
		$ascDescImg=$domain."/images/s_desc.png";
	}
}else{
	$order ="DESC";
	$name = "Date_of_treatment";
	$ascDescImg=$domain."/images/s_desc.png";
	$lastTreatAscDescImg=$domain."/images/s_desc.png";
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/bootstrap.css" />
<script src="<?php echo $domain; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo $domain; ?>/js/bootbox.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".searchclientTable").keyup(function(){ 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		if(searchid!=''){
			$.ajax({
				type: "POST",
				url: "ajax_lapsedlostclientsearch.php",
				data: {'search' : searchid, 'searchfor': 'lost'},
				cache: false,
				success: function(html){
				   $("#resultClientSerchTable").html(html).show();
				}
			});
		}return false;    
	});
	jQuery("#result").live("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		var decoded = $("<div/>").html($name).text();
		$('#searchidTable').val(decoded);
		$('#fname').val(decoded);
	});
	
	jQuery("#resultClientSerchTable div.show").live("click", function(){
		var hrefval = $(this).find("a").attr("href");
		var hrefvalsplit = hrefval.split("?");
		window.location.replace('lost-client-status?'+hrefvalsplit[1]);
		return false;
	});
	
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
			jQuery("#result").fadeOut(); 
		}
	});
	$('#searchidTable').click(function(){
		jQuery("#result").fadeIn();
	});
	$('.reason_lost_lapsed').change(function() {
		var change = $(this).val();
		var data_id = $(this).data('id');
		// if(change === 'Client is Scheduled' || change === 'Left Voice Message')
		if(change !== '')
		{
			$('.comment_msg_'+data_id).show();
		}
		else
		{
			$('.comment_msg_'+data_id).hide();
			$('.comment_msg_'+data_id).val('');
		}
		});
	
	$(".update-lost-status input[type='image']").click(function(){
		var _this = $(this);
		var reason = _this.prev().val();
		var client_id = _this.attr('rel');
		var client_email = _this.prev().prev().val();
		var client_msg = $('.comment_msg_'+client_id).val();
		if(reason == ''){
			bootbox.alert("Please select a reason.");
			return false;
		}		
		bootbox.confirm("Are you sure you want to change status?", function(result){
			if(result){
				$.ajax({
					url: 'lost-client-status.php',
					type: "POST",
					data: {client_id, reason, client_email, client_msg},
					success:function(res){
						//$(_this).parent().html(res)
						$(".showstatusupdate").text("Status changed successfully.");
						setTimeout(function () {
							location.reload(); 
							}, 1000);
					}
				});
			}else{
				_this.prev().val('');
			}
		});
	});
	$('[data-toggle="tooltip"]').tooltip();

	$('#businessid').change(function() {
		if( $(this).val() != 0 ){
			var str = 'lost-client-status?location='+$(this).val();
			window.location.href = str;
		} else {
			
		}
	});
});
</script>
<?php
/*** fetch all Client with location**/
$table1 = "tbl_treatment_log";
$table2 = "tbl_clients";
$join = "INNER";
$currentDate = date("Y-m-d 00:00:00");
$cols = "AES_DECRYPT(Email, '".SALT."') AS Email, AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, AES_DECRYPT(PhoneNumber, '".SALT."') AS PhoneNumber, tbl_clients.Location, tbl_clients.TimeStamp, lost_close_reason, tbl_treatment_log.ClientID, tbl_treatment_log.Date_of_treatment, tbl_clients.last_status_updated,  tbl_clients.comment";
/*** numrows**/
$adjacents = 3;
$reload="lost-client-status.php";

if(isset($_GET['location'])&& $_GET['location']!=""){
	
	$locationid = base64_decode($_GET['location']);
	$location = " AND tbl_clients.Location LIKE '%".$locationid."%'";
	 
}else {
	$location = '';
}

if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
	$conditionNumRows = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID']). $location."' AND tbl_clients.TimeStamp < '".$currentDate."' GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=180 ORDER BY FirstName";
}else{
	$conditionNumRows = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.TimeStamp < '".$currentDate."' AND tbl_clients.BusinessID = $_SESSION[BusinessID] $location GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=180 ORDER BY tbl_clients.ClientID DESC";
}	
$totalNumRows = $clientDetails->joinTotalNumRows($table1,$table2,$join,$conditionNumRows);
$total_pages = $totalNumRows;
//$page="";
if(isset($_GET['page'])){
	$page=$_GET['page'];
}
else{
	$page="";
}
$limit = 10;                                  //how many items to show per page
if($page)
	$start = ($page - 1) * $limit;          //first item to display on this page
else
	$start = 0; 
/*** numrow**/

if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
  $condition = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID']).$location."' AND tbl_clients.TimeStamp < '".$currentDate."' GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=180 ORDER BY FirstName";
}else{
  $condition = "tbl_clients.ClientID = tbl_treatment_log.ClientID  WHERE tbl_clients.TimeStamp < '".$currentDate."' AND tbl_clients.BusinessID = $_SESSION[BusinessID] $location GROUP BY tbl_treatment_log.ClientID having DATEDIFF(CURDATE(), max(tbl_treatment_log.Date_of_treatment)) >=180 ORDER BY ".$name." ".$order." LIMIT ".$start.", ".$limit." ";
}
$clientdata	= $clientDetails->selectLapsedLostTableJoin($table1,$table2,$join,$condition,$cols);

$ManagesData = array();
$tbl_manage_location = 'tbl_manage_location';
$condition = "where BusinessID=".$_SESSION["BusinessID"]." GROUP BY LocationName ORDER BY ManageLocationId";
$cols="ManageLocationId,LocationName";
$ManagesData = $clientDetails->selectallLocations($tbl_manage_location,$condition,$cols);

/*** fetch All client with location Name**/

?>
<style>
.tooltip {
    position: relative;
    display: inline-block;
    opacity: 9;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: left;
    border-radius: 6px;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    -ms-border-radius: 6px;
    -o-border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 150%;
    left: 50%;
    margin-left: -100px;
}

.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: black transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
</style>

<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Lost Clients</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="lapsed-client-status"><button class="addnewbtn"> Lapsed Clients </button></a>
						</div>
					</div>
					<div class="bussiness-searchblock">
						<div class="busniss-search last">
							<select class="select-option businessid" name="businessid" id="businessid">
								<option value="0">Please select a busniess</option>
								<?php 
								foreach( $ManagesData as $business){
									$test = base64_encode($business['ManageLocationId']); 	
								?>	
									<option value="<?php echo $test; ?>" <?php if($business['ManageLocationId'] == $locationid ){ echo 'selected="selected"'; } ?>><?php echo ucfirst($business['LocationName']); ?></option>
								<?php
								} 
								?>
							</select>
						</div>  	 
						<div class="busniss-search">
							<input placeholder="First name, last name or phone number:" type="text" class="searchclientTable text-input-field" id="searchidTable" />
							<div id="resultClientSerchTable"></div>
						</div> 
					</div>
					<?php
						if( $locationid ){
							$loc = "location=". base64_encode($locationid);
						} else {
							$loc = '';
						}
					?>	
					<div class="card-body">
					<?php
						if( !empty($clientdata) ) {
							$i = 0;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th><a title="<?php echo $order;?>" href="lost-client-status?orderby=<?php echo $order;?>&name=firstname&<?php echo $loc;?>"> Client Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a></th>
										<th>Office Location(s)</th>
										<th>Phone No.</th>
										<th><a title="<?php echo $order;?>" href="lost-client-status?orderby=<?php echo $order;?>&name=lasttreat&<?php echo $loc;?>">Last Treatment <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $lastTreatAscDescImg;?>"></a></th>
										<th>Last Session Price</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $clientdata as $cldata ) {
											if($i%2==0){$bgdata = "bgnone";	}
											else{$bgdata = "bgdata";}
											/*****************check date diff*****************/
												$date1 = $cldata['last_status_updated'];
												$date2 = date('Y-m-d');
												$diff = abs(strtotime($date2) - strtotime($date1));
												$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
												
											/*****************check date diff*****************/
											$reaon_status=explode('<br/>', $cldata['lost_close_reason']);	
											$final_status=$reaon_status[0];
									?>
												<tr class="treatment <?php echo $bgdata;?>" id="<?php echo $cldata['ClientID']?>" style="min-height:53px">
													<td class="span3 srtHeadEditEmp srtcontent"><label id="" class="user-name"><?php echo ucfirst($cldata['FirstName'].' '.$cldata['LastName']);?> </label></td>
													<td class="span6 srtHead srtcontent">
														<label id="" class="user-name">
														<?php 
														      if($cldata['Location'] !=""){							  
														         $clentLocation = explode(",",$cldata['Location']);
														         for($j=0 ; $j<count($clentLocation);$j++){
																$tbl_manage_location	= "tbl_manage_location";
																$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
																$cols="*";
																$locationData	= $clientDetails->selectTableSingleRow($tbl_manage_location,$condition1);
																  echo $locationData['LocationName'];							  
																  if($j !=count($clentLocation)-1){
																  echo ",&nbsp;";
															     }
															  }//for loop close
															}
									                    ?>
														</label>
													</td>
													<td class="span6 srtHead srtcontent">
														<label id="" class="user-name"><?php echo $cldata['PhoneNumber']; ?></label>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<label id="" class="user-name"><?php echo date("M d, Y",strtotime($cldata['Date_of_treatment'])); ?></label>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<?php 
															$tbl_manage_location	= "tbl_treatment_log";
															$cols="*";
															$PriceData	= $clientDetails->selectLastPrice($tbl_manage_location,$cldata['ClientID'], $cols);
															echo '<label id="" class="user-name">$'.$PriceData[0]['Price'].'</label>';
														?>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<label class="user-name update-lost-status">
														<?php 
															if(($cldata['lost_close_reason'] == '') || (($final_status==='Client is Scheduled' || $final_status === 'Left Voice Message') && $days >= 30)){ ?>
																<input type="hidden" value="<?php echo $cldata['Email']; ?>" name="client_email" />
																<select class="text-input-field reason_lost_lapsed" data-id="<?php echo $cldata['ClientID']?>" style="padding:2px;width:70%;margin-right:5px;height:35px">
																	<option value="">Select Reason</option>
																	<option <?php if($final_status=='Treatment Completed') { echo "selected"; } ?>>Treatment Completed</option>
																	<option <?php if($final_status=='Transferred to Another Business') { echo "selected"; } ?>>Transferred to Another Business</option>
																	<option <?php if($final_status=='Client Requests No') { echo "selected"; } ?>>Client Requests No Futher Contact</option>
																	<option <?php if($final_status=='Client is Scheduled') { echo "selected"; } ?>>Client is Scheduled</option>
																	<option <?php if($final_status=='Left Voice Message') { echo "selected"; } ?>>Left Voice Message</option>
																	<option <?php if($final_status=='Other') { echo "selected"; } ?>>Other</option>
																</select>
																<input class="update_comment" type="image" title="Update Status" src="<?php echo $domain; ?>/images/update_new.png" rel="<?php echo $cldata['ClientID']; ?>" style="border:none"> 
																<?php 
																if($cldata['comment'] != "") { ?>
																	<div class="tooltip"><input type="image" src="<?php echo $domain; ?>/images/chat.png" style="border:none;"> <span class="tooltiptext"><?php echo $cldata['comment']; ?></span> </div>
																<?php 
																} 
															} 	else { 
																echo '<p>'.nl2br($cldata['lost_close_reason']).'</p>';  
																if($cldata['comment'] != "") { ?>
																	<div class="tooltip"><img src="<?php echo $domain; ?>/images/chat.png" style="border:none;"> <span class="tooltiptext"><?php echo $cldata['comment']; ?></span> </div>
														<?php 	} 
															}
														  ?>
														</label>
														<textarea placeholder="Comment" class="comment_msg_<?php echo $cldata['ClientID']?>" style="display:none;height: 71px; width: 65%;"></textarea>
													</td>
												</tr><!--End @row-block-->
												<?php
												$i++;
										} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $clientDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload,$loc);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<!-- <div class="loadingOuter"><img src="../images/loader.svg"></div> -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
