<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$mnglocation = new dbFunctions();
if( !in_array(7,$_SESSION["menuPermissions"])){ 
		?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }

if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] != 'Admin') { ?>
<script>
		// window.location.replace("dashboard");
	</script>
<?php } else if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] == 'Admin'){
	
	}
$services = implode(",",$_SESSION["services"]);

/*** fetch All device Name**/
$tableDevice = "tbl_devicename as d";
$tableService = "tbl_master_services as s";
$tbl_manage_location = "tbl_manage_location";
$Condition = "d.serviceId=s.id where d.BusinessID=".$_SESSION["BusinessID"]." and s.id in(".$services.") order by d.serviceId";
$DeviceData = $mnglocation->selectTableJoin($tableDevice,$tableService, $join="", $Condition,$cols="*");

/*** fetch All device Name**/
if(isset($_POST['LocationName']) && $_POST['LocationName']!= NULL){	
	$LData['LocationName'] = $_POST['LocationName'];
	$LData['ManageAddress'] = $_POST['ManageAddress'];
	$LData['BusinessID'] = $_SESSION['BusinessID'];
	$Devices = $_POST['devices'];
	for( $i=0;$i<count($Devices);$i++ ) { 
		$LData['deviceId'] = $Devices[$i];		
		$CheckCond = "where LocationName='".$_POST['LocationName']."' AND deviceId=".$Devices[$i]." AND BusinessID=".$_SESSION['BusinessID'];
		$Check= $mnglocation->selectNumRows($tbl_manage_location,$CheckCond);
		if( $Check==0 ) {
			$return= $mnglocation->insert_data($tbl_manage_location,$LData); 	
		}
	}
	?>
	<script>
		window.location.replace("manage_location_details?msg=success");
	</script>
	<?php
}
?>
<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery("#ManageLocationFrom").validate({
		rules: {
            LocationName: "required",              
			ManageAddress: "required",              
			'devices[]':"required",
        },                
        messages: {
			LocationName: "This field is required.",
			ManageAddress: "This field is required.",
			'devices[]':"Please select atleast one device."                  
        },
          errorPlacement: function(error, element) {
			if (element.attr("name") == "devices[]" ) {
				$(".errorDevice").html('Please select atleast one device.');
			} else {
			  error.insertAfter(element);
			}
		  }
	});
});

</script>
<style>
.span3 { 
	width:40%; 
	font-family: Verdana,Geneva,Tahoma,sans-serif;
	font-size: 13px;
	font-weight: 500;
	// margin-top: 16px;
	color: #666666;
	}
#devicemsg > label {
  color: black;
  font-size: 19px;
}

.devicename {
    color: #666666;
    float: right;
    font-size: 13px;
    line-height: 20px;
    width: 93%;
}
.error { width:auto; }
 .errorDevice
 {
 color: red;
    float: left;
    font-family: Verdana,Geneva,Tahoma,sans-serif;
    font-size: 12px;
    margin-bottom: 5px;
    width: auto;
}
</style>
<div class="form-container">
	<div class="heading-container">		
		<h1 class="heading empHead">Manage Location</h1>
		<div class="addNew"><a class="empLinks" href="manage_location_details" class="submit-btn">Location List</a></div>
		</div>
		<div class="user-entry-block">			
			<div class="row">	
				<!--div class="span3 adhead"><a href="manage-location.php">Add location</a></div>
				<div class="span3 adhead"><a href="manage-location-details.php">  Locations list</a></div-->
				<!--div class="span3 adhead"><a href=""> Detail </a></div>
				<div class="span3 adhead"> .</div-->
			</div>
        <form action="" name="ManageLocationFrom" id="ManageLocationFrom" method="post">
			<div class="leftblocklocation">	
				<div class="row">
					 <div class="span6">
						<label id="" class="user-name">Location name:</label>
						<input class="text-input-field" type="text" name="LocationName" id="LocationName"/>
					</div>		
					
				</div><!--End @row-->		
           <div class="row">
			   <!--div class="span6">
					<label id="Label1" class="user-name">User Name:</label>
					<input class="text-input-field"  type="text" name="Username" id="Username"/>
				</div-->				
				<div class="span6 last">
					<label class="user-name" for="Address">Address:</label>
					<textarea class="text-area-field" rows="4" cols="20" id="ManageAddress" name="ManageAddress"></textarea>
				</div>				
			</div><!--End @row-->
			</div><!--End @row-->
			<div class="rightblocklocation">	
				<div class="span3">				
					<?php 
						$ServiceID = "";
						if(!empty($DeviceData)) {
							$i=0;
						foreach( $DeviceData as $Devices ) {
							
							if($ServiceID != $Devices["serviceId"]){
								$i++;
								$ServiceID = $Devices["serviceId"];
							?>
								<br/><label id="user<?php echo $i; ?>" class="user-name"><?php echo $Devices["name"]; ?> Devices:</label>
							<?php
							}
							//echo $Devices["deviceId"]."<br/>";
						?>											
							<div class="outer-inptbox"><input type="checkbox" name="devices[]" value="<?php echo $Devices["deviceId"]; ?>" /><span class="devicename"><?php echo $Devices["DeviceName"]; ?><br/></span></div>
						<?php 
							}
						} else { ?>
								<div id="devicemsg" style="margin-top: 15px;"><label>Please add atleast one device.<a href="adminentry" style="color: #2086B7;text-decoration:underline;"> Click to add devices.</a></label></div>
						<?php	
						}
						?>	
						<span class="errorDevice"></span>			
				</div>	
			</div>	
            <div class="row">
				<div class="span12">
				  <input type="submit" id="submitManageForm" value="Submit" class="submit-btn">
				  <?php if(isset($msg) && $$msg!="no"){ echo "<span style='color:green;'>Information inserted successfully.</span>";}?>
				  <br/>
				</div>			
			</div><!--End @row-->
        </form>
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
