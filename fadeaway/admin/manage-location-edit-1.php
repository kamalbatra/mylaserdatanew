<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$mnglocation = new dbFunctions();
/*** fetch All device Name**/
$tableDevice = "tbl_devicename";
$tableService = "tbl_master_services";
$tbl_manage_location = "tbl_manage_location";
$services = implode(",",$_SESSION["services"]);
$Condition = "tbl_devicename.serviceId=tbl_master_services.id where tbl_devicename.BusinessID=".$_SESSION["BusinessID"]." ANd tbl_master_services.id in(".$services.") order by tbl_devicename.serviceId";
$DeviceData = $mnglocation->selectTableJoin($tableDevice,$tableService, $join="", $Condition,$cols="*");

//~ $tableDevice	= "tbl_devicename";
//~ $condition = "WHERE BusinessID=$_SESSION[BusinessID] ORDER BY deviceId  DESC ";
//~ $cols="*";
//~ $DeviceData	= $mnglocation->selectTableRows($tableDevice,$condition);
//print_r($DeviceData);
/*** fetch All device Name**/
if(isset($_GET['ManageLocationId']) && $_GET['ManageLocationId']!= NULL){	
	$_GET['ManageLocationId']; 
    $LCond = " where ManageLocationId =".$_GET['ManageLocationId']." ";
    $LCols = "LocationName,ManageAddress";
    $LData = $mnglocation->selectTableSingleRow($tbl_manage_location,$LCond,$LCols);
    
    $LCond2 = "where LocationName='".$LData["LocationName"]."'";
    $LCols2 = "ManageLocationId,deviceId";
    $LData2 = $mnglocation->selectTableRows($tbl_manage_location,$LCond2,$LCols2);
    foreach ( $LData2 as $Location ) {
		$LocationID[] = $Location["ManageLocationId"];
		$DeviceID[] = $Location["deviceId"];
	}
	$LocationIDs = implode(",",$LocationID);
}
?>	
<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery("#ManageLocationFromEdit").validate({
		rules: {
                LocationName: "required",              
                ManageAddress: "required",              
                deviceId: "required", 
                  LocationName: {
					required: true,					
					},
					ManageAddress: {
					required: true,					
					},
					deviceId: {
					required: true,					
					},
                  },
                messages: {
                    LocationName: "This field is required.",
                    ManageAddress: "This field is required.",
                    deviceId: "This field is required.",
                  }
	});
	$('#submitManageFormEdit').click(function() {
		if( $("#ManageLocationFromEdit").valid()){
			edit_manageLocation();
		}else{				
		}        
    });	
});
</script>
<script type="text/javascript">
		function edit_manageLocation(){
			var str = $("#ManageLocationFromEdit").serialize();
				$.ajax({
				type: "POST",
				url: "editajax_manageLocation.php",
				data: str,
				cache: false,
				success: function(result){					
					$("#successUpdateLocation").html(result);						
					//setTimeout(function() {location.reload();}, 1000);					
				}		
				});
		}
</script>
<style>
.span3 { 
	width:40%; 
	font-family: Verdana,Geneva,Tahoma,sans-serif;
	font-size: 13px;
	font-weight: 500;
	color: #666666;	
}
</style>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Edit Location</h1>
		<div class="addNew"><a class="empLinks" href="manage_location_details" class="submit-btn">Manage Locations </a></div>
	</div>	
		<div class="user-entry-block">
			<div class="row">	
				<!--div class="span3 adhead"><a href="manage-location.php">Add location</a></div>
				<div class="span3 adhead"><a href="manage-location-details.php">  Locations list</a></div-->
				<!--div class="span3 adhead"><a href=""> Detail </a></div>
				<div class="span3 adhead"> .</div-->
			</div>
        <form action="" name="ManageLocationFromEdit" id="ManageLocationFromEdit" method="post">
				<input type="hidden" name="ManageLocationId" id="ManageLocationId" value="<?php echo $_GET['ManageLocationId']; ?>"/>
               <div class="row">
					 <div class="span6">
						<label id="" class="user-name">Location name:</label>
						<input class="text-input-field" type="text" name="LocationName" id="LocationName" value="<?php echo $LData['LocationName']; ?>"/>
					</div>					
					<div class="span3">				
					<?php 
						$ServiceID = "";
						foreach( $DeviceData as $Devices ) {
							if($ServiceID != $Devices["serviceId"]){
								$ServiceID = $Devices["serviceId"];
							?>
								<br/><label id="" class="user-name">Device name For <?php echo $Devices["name"]; ?>:</label>
							<?php
							}
							//echo $Devices["deviceId"]."<br/>";
						?>											
							<input type="checkbox" name="devices[]" value="<?php echo $Devices["deviceId"]; ?>" <?php if(in_array($Devices["deviceId"],$DeviceID)) echo "checked"; ?>/><?php echo $Devices["DeviceName"]; ?><br/>
						<?php 
							}
						?>				
						</div>	
			</div><!--End @row-->	
           <div class="row">				
				<div class="span6 last">
					<label class="user-name" for="Address">Address:</label>
					<textarea class="text-area-field" rows="4" cols="20" id="ManageAddress" name="ManageAddress"><?php echo $LData['ManageAddress']; ?></textarea>
				</div>				
			</div><!--End @row-->
            <div class="row">
				<div class="span12">
				  <input type="button" id="submitManageFormEdit" value="Submit" class="submit-btn">				  
				  <div id="successUpdateLocation" style="color:green;"></div>
				  <br/>
				</div>			
			</div><!--End @row-->
			<input type="hidden" name="LocationIDs" value="<?php echo $LocationIDs; ?>" />
        </form>
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
