<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
include("../includes/dbFunctions.php");
$mnglocation = new dbFunctions();
/*** fetch All device Name**/
$tableDevice = "tbl_devicename";
$tableService = "tbl_master_services";
$tbl_manage_location = "tbl_manage_location";
$services = implode(",",$_SESSION["services"]);
$Condition = "tbl_devicename.serviceId=tbl_master_services.id where tbl_devicename.BusinessID=".$_SESSION["BusinessID"]." ANd tbl_master_services.id in(".$services.") order by tbl_devicename.serviceId";
$DeviceData = $mnglocation->selectTableJoin($tableDevice,$tableService, $join="", $Condition,$cols="*");

//~ $tableDevice	= "tbl_devicename";
//~ $condition = "WHERE BusinessID=$_SESSION[BusinessID] ORDER BY deviceId  DESC ";
//~ $cols="*";
//~ $DeviceData	= $mnglocation->selectTableRows($tableDevice,$condition);
//print_r($DeviceData);
/*** fetch All device Name**/
if(isset($_GET['ManageLocationId']) && $_GET['ManageLocationId']!= NULL){	
	$_GET['ManageLocationId']; 
    $LCond = " where ManageLocationId =".$_GET['ManageLocationId']." ";
    $LCols = "LocationName,ManageAddress";
    $LData = $mnglocation->selectTableSingleRow($tbl_manage_location,$LCond,$LCols);
    
    $LCond2 = "where LocationName='".$LData["LocationName"]."'";
    $LCols2 = "ManageLocationId,deviceId";
    $LData2 = $mnglocation->selectTableRows($tbl_manage_location,$LCond2,$LCols2);
    foreach ( $LData2 as $Location ) {
		$LocationID[] = $Location["ManageLocationId"];
		$DeviceID[] = $Location["deviceId"];
	}
	$LocationIDs = implode(",",$LocationID);
}
?>	
<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery("#ManageLocationFromEdit").validate({
		rules: {
                LocationName: "required",              
                ManageAddress: "required",              
                deviceId: "required", 
                  LocationName: {
					required: true,					
					},
					ManageAddress: {
					required: true,					
					},
					deviceId: {
					required: true,					
					},
                  },
                messages: {
                    LocationName: "This field is required.",
                    ManageAddress: "This field is required.",
                    deviceId: "This field is required.",
                  }
	});
	$('#submitManageFormEdit').click(function() {
		if( $("#ManageLocationFromEdit").valid()){
			edit_manageLocation();
		}else{				
		}        
    });	
});
</script>
<script type="text/javascript">
		function edit_manageLocation(){
			var str = $("#ManageLocationFromEdit").serialize();
				$.ajax({
				type: "POST",
				url: "editajax_manageLocation.php",
				data: str,
				cache: false,
				success: function(result){					
					$("#successUpdateLocation").show().html(result);						
					//setTimeout(function() {location.reload();}, 1000);					
				}		
				});
		}
</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Edit Location</h1>
				</div>	
				
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="manage_location_details" class="submit-btn"><button class="addnewbtn">Manage Locations </button></a>
						</div>
					</div>
					<div class="formcontentblock-ld">
						<form action="" name="ManageLocationFromEdit" id="ManageLocationFromEdit" method="post">
							<input type="hidden" name="ManageLocationId" id="ManageLocationId" value="<?php echo $_GET['ManageLocationId']; ?>"/>
							<div class="formcontentblock-ld">
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Location name</label>
												<input class="text-input-field" type="text" name="LocationName" id="LocationName" value="<?php echo $LData['LocationName']; ?>"/>
											</div>
										</div>
									</div>
								</div>						
								<div class="form-row-ld">
									<div class="full">
										<div class="form-col-ld">
											<div class="inputblock-ld bigtextarea">
												<label>Address</label>
												<textarea class="text-area-field" rows="4" cols="20" id="ManageAddress" name="ManageAddress"><?php echo $LData['ManageAddress']; ?></textarea>
											</div>
										</div>
									</div>
								</div>				
												
								<div class="form-row-ld">
									<div class="full">
										<div class="form-col-ld">
											<div class="inputblock-ld radiolabel">
												<div class="radioblocksBtns">
											<?php 
											$ServiceID = "";
											foreach( $DeviceData as $Devices ) {
												if($ServiceID != $Devices["serviceId"]){
													$ServiceID = $Devices["serviceId"];
												?>
													<label id="" class="user-name">Device name For <?php echo $Devices["name"]; ?>:</label>
												<?php
												}
												//echo $Devices["deviceId"]."<br/>";
											?>		
												<div class="radioBtn ckeckboxres">		
													<input type="checkbox" name="devices[]" id="<?php echo $Devices["deviceId"]; ?>" value="<?php echo $Devices["deviceId"]; ?>" <?php if(in_array($Devices["deviceId"],$DeviceID)) echo "checked"; ?>/><label for="<?php echo $Devices["deviceId"]; ?>"><?php echo $Devices["DeviceName"]; ?></label>
												</div>
											<?php 
												}
											?>	
											<span class="errorDevice"></span>			
				
											
											
									
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="backNextbtn">
										<button type="button" id="submitManageFormEdit" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
										<div id="successUpdateLocation" class="u_mess"></div>
									</div>
								</div>
								<input type="hidden" name="LocationIDs" value="<?php echo $LocationIDs; ?>" />
							</div>
						</form>
                    

					</div>
				</div>
				
				
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
