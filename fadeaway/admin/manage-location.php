<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");

	$mnglocation = new dbFunctions();
	if( !in_array(7,$_SESSION["menuPermissions"])){ 
	?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}

	if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] != 'Admin') { ?>
	<script>
		// window.location.replace("dashboard");
	</script>
	<?php 
	} else if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] == 'Admin'){
	
	}
	$services = implode(",",$_SESSION["services"]);

	/*** fetch All device Name**/
	$tableDevice = "tbl_devicename as d";
	$tableService = "tbl_master_services as s";
	$tbl_manage_location = "tbl_manage_location";
	$Condition = "d.serviceId=s.id where d.BusinessID=".$_SESSION["BusinessID"]." and s.id in(".$services.") order by d.serviceId";
	$DeviceData = $mnglocation->selectTableJoin($tableDevice,$tableService, $join="", $Condition,$cols="*");

	/*** fetch All device Name**/
	if(isset($_POST['LocationName']) && $_POST['LocationName']!= NULL){	
		$LData['LocationName'] = $_POST['LocationName'];
		$LData['ManageAddress'] = $_POST['ManageAddress'];
		$LData['BusinessID'] = $_SESSION['BusinessID'];
		$Devices = $_POST['devices'];
		for( $i=0;$i<count($Devices);$i++ ) { 
			$LData['deviceId'] = $Devices[$i];		
			$CheckCond = "where LocationName='".$_POST['LocationName']."' AND deviceId=".$Devices[$i]." AND BusinessID=".$_SESSION['BusinessID'];
			$Check= $mnglocation->selectNumRows($tbl_manage_location,$CheckCond);
			if( $Check==0 ) {
				$return= $mnglocation->insert_data($tbl_manage_location,$LData); 	
			}
		}
		?>
		<script>
			window.location.replace("manage_location_details?msg=success");
		</script>
		<?php
	}
?>
<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery("#ManageLocationFrom").validate({
		ignore: [],
		errorClass: 'errorblocks',
		errorElement: 'div',
		rules: {
            LocationName: "required",              
			ManageAddress: "required",              
			'devices[]':"required",
        },                
        messages: {
			LocationName: "This field is required.",
			ManageAddress: "This field is required.",
			'devices[]':"Please select atleast one device."                  
        },
          //~ errorPlacement: function(error, element) {
			//~ if (element.attr("name") == "devices[]" ) {
				//~ $(".errorDevice").html('Please select atleast one device.');
			//~ } else {
			  //~ error.insertAfter(element);
			//~ }
		  //~ }
	});
});

</script>
<style>
.span3 { 
	width:40%; 
	font-family: Verdana,Geneva,Tahoma,sans-serif;
	font-size: 13px;
	font-weight: 500;
	// margin-top: 16px;
	color: #666666;
	}
#devicemsg > label {
  color: black;
  font-size: 19px;
}

.devicename {
    color: #666666;
    float: right;
    font-size: 13px;
    line-height: 20px;
    width: 93%;
}
.error { width:auto; }
 .errorDevice
 {
 color: red;
    float: left;
    font-family: Verdana,Geneva,Tahoma,sans-serif;
    font-size: 12px;
    margin-bottom: 5px;
    width: auto;
}
</style>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Location</h1>
				</div>	
				
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="manage_location_details" class="submit-btn"><button class="addnewbtn">Location List </button></a>
						</div>
					</div>	
					<div class="formcontentblock-ld">
						<form action="" name="ManageLocationFrom" id="ManageLocationFrom" method="post">
							
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Location name</label>
												<input class="text-input-field" type="text" name="LocationName" id="LocationName"/>
											</div>
										</div>
									</div>
								</div>						
								<div class="form-row-ld">
									<div class="full">
										<div class="form-col-ld">
											<div class="inputblock-ld bigtextarea">
												<label>Address</label>
												<textarea class="text-area-field" rows="4" cols="20" id="ManageAddress" name="ManageAddress"></textarea>
											</div>
										</div>
									</div>
								</div>				
												
								<div class="form-row-ld">
									<div class="full">
										<div class="form-col-ld">
										<?php 
											$ServiceID = "";
											if(!empty($DeviceData)) {
												$i=0;
											?>
												<div class="inputblock-ld radiolabel">
													<div class="radioblocksBtns Differror">
											<?php
												foreach( $DeviceData as $Devices ) {
													if($ServiceID != $Devices["serviceId"]){
														$i++;
														$ServiceID = $Devices["serviceId"];
													?>
													<label id="user<?php echo $i; ?>" class="user-name"><?php echo $Devices["name"]; ?> Devices:</label>
													<?php
													}
													?>
													<div class="radioBtn ckeckboxres">
														<input type="checkbox" name="devices[]" id="device_<?php echo $Devices["deviceId"]; ?>" value="<?php echo $Devices["deviceId"]; ?>" />
														<label for="device_<?php echo $Devices["deviceId"]; ?>" class="checkbox-txt"><?php echo $Devices["DeviceName"]; ?></label>
													</div>
												
											<?php 
												}
											?>
													</div>
												</div>
											<?php	
											} else { ?>
												<div id="devicemsg" style="margin-top: 15px;"><label>Please add atleast one device.<a href="adminentry" style="color: #2086B7;text-decoration:underline;"> Click to add devices.</a></label></div>
											<?php	
											}
											?>
											<span class="errorDevice"></span>			
				
											
											
									
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="backNextbtn">
										<button type="submit" id="submitManageForm" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
										<?php if(isset($msg) && $$msg!="no"){ echo "<span style='color:green;'>Information inserted successfully.</span>";}?>
									</div>
								</div>
						
						</form>
                    

					</div>
				</div>
				
				
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
