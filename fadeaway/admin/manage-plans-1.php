<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
if($_SESSION['loginuser'] != "sitesuperadmin"){
	?>
	<script>
		$(function(){
			window.location.replace("dashboard?msg=noper");
		});
	</script>
	<?php
	die;
}
$mnglocation = new dbFunctions();
if( !in_array(7,$_SESSION["menuPermissions"]))
{
?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php
}
$condition = "where `Type`='Paid'";
$PlanData = $mnglocation->selectTableRows("tbl_master_plans",$condition);
?>
<div class="form-container" id="top1">
	<div class="heading-container">
		<h1 class="heading empHead">Manage Plans</h1>
		<div class="addNew">
			<a class="empLinks" href="add-plan" class="submit-btn">Add New Plan</a>
		</div>
		<div class="updatebusstatus">Plan status updated successfully.</div>	 
	</div>		
	<div class="user-entry-block">
	<?php
		if(isset($_GET["msg"]))
		{
			$message = "";
			$color = "green";
			if($_GET["msg"]=="add")
				$message = "Plan details added successfully.";
			if($_GET["msg"]=="update")
				$message = "Plan details updated successfully.";	
			echo "<p style='color:".$color.";font-family:Verdana,Geneva,Tahoma,sans-serif;font-size:13px;margin-bottom:13px;margin-top:-12px;'>".$message."</p>"; 
		}
	?>
		<!--div class="row">
			<div class="span6"></div>
			<div class="span6 last">
				<label id="Label1" class="user-name">First name, last name or phone number:</label>					 
				<input type="text" class="searchclient text-input-field" id="searchid" />
				<div id="resultClientSerch"></div>
			</div> 
		</div-->
		<div class="row sortingHead">	
			<div class="span3 busHeadSn srtHeadBorder">S. No.</div>
			<div class="span3 planHeadTitle srtHeadBorder">Title</div>
			<div class="span3 busHeadName srtHeadBorder">Amount</div>
			<div class="span3 busHeadLogo srtHeadBorder">No. of days</div>
			<div class="span3 busHeadAction srtHeadBorder text-align">Action</div>
	   </div>
	<?php
		$i = 0;
		$srno=$i+1;
		foreach($PlanData as $bsdata)
		{
			if($bsdata['id'] == 2 || $bsdata['id'] == 1) continue;
			if($i%2==0)
			{
				$bgdata = "bgnone";	
			}
			else
			{
				$bgdata = "bgdata";
			}
		?>	
			<div class="row  <?php echo $bgdata;?>" id="<?php echo $bsdata['id']; ?>">
				<div class="span3 busHeadSn srtcontent">
					<label id="" class="user-name"><?php echo $srno; ?> </label>						
				</div>				
				<div class="span3 planHeadTitle srtcontent">	
					<label id="" class="user-name"><?php echo $bsdata['title']; ?></label>
				</div>
				<div class="span3 busHeadName srtcontent">	
					<label id="" class="user-name"><?php echo '$ '.$bsdata['amount']; ?></label>
				</div>
				<div class="span3 busHeadLogo srtcontent">	
					<label id="" class="user-name"><?php echo $bsdata['days']; ?></label>
				</div>
				<div class="span3 busHeadAction srtcontent text-align">	
					<label id="" class="user-name">
						<a href="add-plan?id=<?php echo $bsdata['id']; ?>">
							<img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit Plan"/>
						</a><?php if($bsdata['id'] != 1) { ?>&nbsp;|&nbsp;
						<a href="javascript:;" rel="<?php echo $bsdata['id'].'|'.$bsdata['status']; ?>" class="clicktochange">
							<img src="<?php echo $domain; ?>/images/<?php echo ($bsdata['status'] == 1) ? 'active.gif' : 'inactive.gif'; ?>" title="Click to <?php echo ($bsdata['status'] == 1) ? 'inactive' : 'active'; ?>"/>
						</a><?php }?>
					</label>
				</div>
			</div><!--End @row-->	
		<?php
			$i++; $srno++; 
		}
	?>
		<div id="statuResult"></div>
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php 
	include('admin_includes/footer.php');	
?>
<style>
.span3 {
	text-align: center;
}
.busHeadSn {
	width: 12%;
}
.planHeadTitle {
	width: 28%;
}
.busHeadName {
	width: 19%;
}
.busHeadLogo {
	width: 21%;
}
.busHeadAction {
	width: 20%;
}
</style>
