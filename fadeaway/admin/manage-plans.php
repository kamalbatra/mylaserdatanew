<?php
session_start();
$doaminPath = $_SERVER['DOMAINPATH'];
$domain = $_SERVER['DOMAIN'];
include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
include("../includes/dbFunctions.php");
if($_SESSION['loginuser'] != "sitesuperadmin"){
	?>
	<script>
		$(function(){
			window.location.replace("dashboard?msg=noper");
		});
	</script>
	<?php
	die;
}
$mnglocation = new dbFunctions();
if( !in_array(7,$_SESSION["menuPermissions"]))
{
?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php
}
$tbl_category	= "tbl_master_plans";
/*** numrows**/
$adjacents = 3;
$reload="manage-plans";
$conditionNumRows  =  " where `Type`='Paid' ";
$totalNumRows	= $mnglocation->totalNumRows($tbl_category, $conditionNumRows);
$total_pages = $totalNumRows;
if(isset($_GET['page'])){
	$page=$_GET['page'];
} else{
	$page="";
}
$limit =12;	//how many items to show per page
if($page){
	$start = ($page - 1) * $limit;	//first item to display on this page
} else {
	$start = 0;
}
/*** numrow**/
$condition = "where `Type`='Paid' LIMIT  ".$start.", ".$limit."";
$PlanData = $mnglocation->selectTableRowsNew($tbl_category, $condition);
?>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Plans</h1>

					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
									<div class="updatebusstatus">
						<?php
							if(isset($_GET["msg"]))
							{
								$message = "";
								$color = "green";
								if($_GET["msg"]=="add") {
									$message = "Plan details added successfully.";
								}
								if($_GET["msg"]=="update") {
									$message = "Plan details updated successfully.";	
								}
								echo '<div class="u_mess" id="u_mess" style="display: block;">'.$message.'</div>';
							}
						?>
					</div>
				<div class="card shadow mb-4 table-main-con">

<div class="bussiness-searchblock no-searchbox">
	<div class="search-btn">
		<a class="empLinks" href="add-plan" class="submit-btn"><button class="addnewbtn">Add New Plan </button></a>
	</div>
</div>	
					<div class="card-body">
					<?php
						if( !empty($PlanData) ) {
							$i = 0;
							$srno=$i+1;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>Title</th>
										<th>Amount</th>
										<th>No. of days</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $PlanData as $bsdata ) {
											if($bsdata['id'] == 2 || $bsdata['id'] == 1) continue;
											if($i%2==0)
											{
												$bgdata = "bgnone";	
											}
											else
											{
												$bgdata = "bgdata";
											}
									?>
												<tr class="<?php echo $bgdata;?>" id="<?php echo $bsdata['id']; ?>">
													<td class="span3 srtHeadEditEmp srtcontent"><label id="" class="user-name"><?php echo $srno; ?> </label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $bsdata['title']; ?></label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo '$ '.$bsdata['amount']; ?></label></td>
													<td class="span6 srtHeadloc srtcontent"><label id="" class="user-name"><?php echo $bsdata['days']; ?></label></td>
													<td class="span6 cMain ">
														<label id="" class="user-name">
															<a href="add-plan?id=<?php echo $bsdata['id']; ?>">
																<img src="<?php echo $domain; ?>/img/editimg.png" title="Edit Plan"/>
															</a><?php if($bsdata['id'] != 1) { ?>
															<a href="javascript:;" rel="<?php echo $bsdata['id'].'|'.$bsdata['status']; ?>" class="clicktochange">
																<img src="<?php echo $domain; ?>/img/<?php echo ($bsdata['status'] == 1) ? 'tickimg.png' : 'notallow.png'; ?>" title="Click to <?php echo ($bsdata['status'] == 1) ? 'inactive' : 'active'; ?>"/>
															</a><?php }?>
														</label>
													</td>
												</tr><!--End @row-block-->
												<?php
											$i++; $srno++;
										} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $mnglocation->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
