<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$empInfo = new dbFunctions();
if(isset($_GET['Empid']) && $_GET['Empid']!= NULL){	
$empId = $_GET['Empid'];
$table	= "tbl_employees";
$condition = " where Emp_ID=".$empId." ";
$cols="*";
$editinfo = $empInfo->selectTableSingleRow($table,$condition,$cols);
$menuPermissionArr = explode("|", $editinfo['menuPermissions']);
/**** Fetch all  locations******/
$tbl_manage_location	= "tbl_manage_location";
$condition1 = "WHERE BusinessID = $_SESSION[BusinessID] ORDER BY ManageLocationId  ASC ";
$cols="*";
$locationData = $empInfo->selectTableRows($tbl_manage_location,$condition1);
?>
<style>
	.right-margin-6{margin-right: 6%;}
	.menu-checkbox{float:left; width:25%}
	.edit-emp-link{font-size:15px;width:auto;}
</style>
<div class="form-container">
	<div class="heading-container edit-employe">
		<h1 class="heading empHeadReport">Edit Employee Information</h1>
		<div class="addNewReport edit-emp-link">
			<a class="empLinks" href="empdetails" class="submit-btn">Employee List</a>&nbsp;|&nbsp;
			<a class="empLinks" href="manage_employee">Add New</a>&nbsp;|&nbsp;
			<a class="empLinks" href="changepassword?empid=<?php echo base64_encode($editinfo['Emp_ID'])?>&action=clientdetails">Change Password</a>&nbsp;|&nbsp;
			<a class="empLinks" href="schedule?empid=<?php echo base64_encode($editinfo['Emp_ID'])?>&action=clientdetails">Schedule</a>
		</div>
	</div>	
		<div class="user-entry-block user-entry-main">
<!--  dummy form to submit data  -->
<form action="" name="insertFromEdit" id="insertFromEdit" method="post">	
<div class="row">
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">First Name:</label>
					<input type="hidden" value="<?php echo $editinfo['Emp_ID']; ?>" name="Emp_ID">
					<input class="text-input-field" type="text" name="First_Name" id="First_Name" value="<?php echo $editinfo['First_Name']; ?>"/>
				</div>				
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Last Name:</label>
					<input class="text-input-field" type="text" name="Last_Name" id="Last_Name" value="<?php echo $editinfo['Last_Name']; ?>"/>
				</div>
			</div><!--End @row-->		

<div class="row">
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Email:</label>
					<input class="text-input-field" type="email" name="Emp_email" id="Emp_email" value="<?php echo $editinfo['Emp_email']; ?>"/>
					<span id="emailExistEdit" class="error"></span>
				</div>				
				<!--<div class="span3" style="padding-top:16px;">
					<h6 class="submit-btn"><a class="changepwd" href="changepassword.php?empid=<?php echo base64_encode($editinfo['Emp_ID'])?>&action=clientdetails" style="color:#11719F;">Change password</a></h6>
				</div>-->
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Address:</label>
					<textarea name="Office_Addr" id="Office_Addr" rows="4" class="text-area-field"><?php echo $editinfo['Office_Addr'];?></textarea>
				</div>
	</div>	
<div class="row">			
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Medical director:</label>
					<select class="select-option" name="Medicaldirector" id="Medicaldirector">										
					<?php if(($_SESSION['Superadmin']=="1") || (strtoupper($_SESSION['Medicaldirector'])=="YES" && ($editinfo['Emp_ID']!=$_SESSION['id']))){?>
					<option value="Yes" <?php if($editinfo['Medicaldirector']=="Yes"){  echo 'selected' ;}?>>Yes</option>
					<option value="No" <?php if($editinfo['Medicaldirector']=="No"){  echo 'selected' ;}?>>No</option>
					<?php }else{ if($editinfo['Medicaldirector'] !=""){?>						
							<option value="<?php echo $editinfo['Medicaldirector']?>"><?php echo $editinfo['Medicaldirector']?></option>
					<?php } } ?>					
					</select>
				</div>
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Office locations:</label>
					<select class="select-option" name="Location" id="Location">
					<option value="">Select an option</option>
					<?php 
						if( !empty($locationData) ) {
							$LastLocation = "";
							foreach($locationData as $ldata) {
								if( $LastLocation != $ldata["LocationName"] ) {				
					?>
									<option value="<?php echo $ldata['ManageLocationId'];?>"<?php if($editinfo['Location']==$ldata['ManageLocationId']){  echo 'selected' ;}?>><?php echo $ldata['LocationName'];?></option>					
					<?php		
									$LastLocation = $ldata["LocationName"];
								}
							}
						} 
					?>
					</select>
				</div>
			</div><!--End @row-->
<?php 
//if($_SESSION['Superadmin'] == 0 && !isset($_SESSION['loginuser'])) {
if($_SESSION['Admin'] == "TRUE" && $_SESSION['id'] != $editinfo['Emp_ID'] || (strtoupper($_SESSION['Medicaldirector'])=="YES")){ ?>
<div class="row">
	<div class="span12">
		<label id="Label1" class="user-name">Menu Permissions:</label>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_new_client" name="menu_permission[]" value="1" <?php echo (in_array(1, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_new_client" class="checkbox-txt">New Client</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_client_lookup" name="menu_permission[]" value="2" <?php echo (in_array(2, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_client_lookup" class="checkbox-txt">Client Lookup</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_pricing_calculator" name="menu_permission[]" value="3" <?php echo (in_array(3, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_pricing_calculator" class="checkbox-txt">Pricing Calculator</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_consent_form_review" name="menu_permission[]" value="4" <?php echo (in_array(4, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_consent_form_review" class="checkbox-txt">Consent Form Review</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_manage_clients" name="menu_permission[]" value="5" <?php echo (in_array(5, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_manage_clients" class="checkbox-txt">Manage Clients</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_manage_employees" name="menu_permission[]" value="6" <?php echo (in_array(6, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_manage_employees" class="checkbox-txt">Manage Employees</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_manage_locations" name="menu_permission[]" value="7" <?php echo (in_array(7, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_manage_locations" class="checkbox-txt">Manage Locations</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_manage_devices" name="menu_permission[]" value="8" <?php echo (in_array(8, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_manage_devices" class="checkbox-txt">Manage Devices</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="9" <?php echo (in_array(9, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_manage_pricing" class="checkbox-txt">Manage Pricing</label>
		</div>
		<div class="menu-checkbox formdonly">
			<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="10" <?php echo (in_array(10, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_transactionhistory" class="checkbox-txt">Transaction History</label>
		</div>
		<div class="menu-checkbox formdonly">
			<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="11" <?php echo (in_array(11, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_subscription_status" class="checkbox-txt">Subscription Status</label>
		</div>
		<div class="menu-checkbox formdonly">
			<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="12" <?php echo (in_array(12, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_client_status_update" class="checkbox-txt">Client Status Update</label>
		</div>
		<div class="menu-checkbox formdonly">
			<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="13" <?php echo (in_array(13, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_reports" class="checkbox-txt">Reports</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="14" <?php echo (in_array(14, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_reports" class="checkbox-txt">Manage Protocols</label>
		</div>
		<div class="menu-checkbox">
			<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="15" <?php echo (in_array(15, $menuPermissionArr)) ? 'checked' : ''; ?> />
			<label for="menu_reports" class="checkbox-txt">Manage Legal Disclaimers</label>
		</div>
	</div>
</div>
<?php } ?>
       <!--div class="row">
			<div class="span6">
					<label id="Label1" class="user-name">Options:</label>
					<select class="select-option" name="admin_co" id="admin_co">
					<option value="">Select an option-</option>
					
					<option value="Counter" <?php if($editinfo['Counter']=="TRUE"){  echo 'selected' ;}?> >Counter</option>
					<option value="Rewind"  <?php if($editinfo['Rewind']=="TRUE"){  echo 'selected' ;}?>>Rewind</option>
					</select>
				</div>
			</div--><!--End @row-->
<div class="row">
				<div class="span12">
				<!--input type="submit" name="name" id="submitForm" value="submit" class="submit-btn"><br/-->
				<input type="button"  id="submitFormEdit" value="Submit" class="submit-btn"><br/>
				<div id="insertResultEdit" style="display:none;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
				</div>			
			</div><!--End @row-->
</form>
</div>
</div>
<?php }?>
</div>
<?php
	include('admin_includes/footer.php');
	?>
