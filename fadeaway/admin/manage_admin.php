<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();

?>
	<style>
		.right-margin-6 { margin-right: 6%; }
		.menu-checkbox{ float: left; width: 25%; }
		.addNewReport { float: right; }
		.formdonly {display:none;}
	</style>
	<script>
		$(document).ready(function() {
			$('#Medicaldirector').change(function() {
				var val=$(this).val();
				if(val=='Yes') {
					$('.formdonly').show();	
				} else {
					$('.formdonly').hide();	
				}
			});	
		});
	</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Edit Employee Information</h1>
				</div>	
				<?php
				if(isset($_GET['Empid']) && $_GET['Empid']!= NULL){	
					$empId = $_GET['Empid'];
					$table	= "tbl_employees";
					$condition = " where Emp_ID=".$empId." ";
					$cols="*";
					$editinfo = $empInfo->selectTableSingleRow($table,$condition,$cols);
					$menuPermissionArr = explode("|", $editinfo['menuPermissions']);
					/**** Fetch all  locations******/
					$tbl_manage_location	= "tbl_manage_location";
					$condition1 = "WHERE BusinessID = $_SESSION[BusinessID] ORDER BY ManageLocationId  ASC ";
					$cols="*";
					$locationData = $empInfo->selectTableRows($tbl_manage_location,$condition1);
				?>
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="empdetails" class="submit-btn"><button class="addnewbtn">Employee list </button></a> <a class="empLinks" href="manage_employee"><button class="addnewbtn">Add New </button></a> <a class="empLinks" href="changepassword?empid=<?php echo base64_encode($editinfo['Emp_ID'])?>&action=clientdetails"><button class="addnewbtn">Change Password </button></a> <a class="empLinks" href="schedule?empid=<?php echo base64_encode($editinfo['Emp_ID'])?>&action=clientdetails"><button class="addnewbtn">Schedule </button></a>
						</div>
					</div>
					<div class="formcontentblock-ld">
						<form action="" name="insertFromEdit" id="insertFromEdit" method="post">	
							<div class="formcontentblock-ld">
								<div class="form-row-ld">
									<div class="onethirdblock">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>First Name:</label>
												<input type="hidden" value="<?php echo $editinfo['Emp_ID']; ?>" name="Emp_ID">
												<input class="text-input-field" type="text" name="First_Name" id="First_Name" value="<?php echo $editinfo['First_Name']; ?>"/>
											</div>
										</div>
									</div>
									<div class="onethirdblock">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Last Name:</label>
												<input class="text-input-field" type="text" name="Last_Name" id="Last_Name" value="<?php echo $editinfo['Last_Name']; ?>"/>
											</div>
										</div>
									</div>
									<div class="onethirdblock">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Email</label>
												<input class="text-input-field" type="email" name="Emp_email" id="Emp_email" value="<?php echo $editinfo['Emp_email']; ?>"/>
												<span id="emailExistEdit" class="error"></span>
											</div>
										</div>
									</div>
								</div>						
								<div class="form-row-ld">
									<div class="full">
										<div class="form-col-ld">
											<div class="inputblock-ld bigtextarea">
												<label>Address</label>
												<textarea name="Office_Addr" id="Office_Addr" rows="4" class="text-area-field"><?php echo $editinfo['Office_Addr'];?></textarea>
											</div>
										</div>
									</div>
								</div>								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Medical director</label>
												<select class="select-option" name="Medicaldirector" id="Medicaldirector">										
												<?php if(($_SESSION['Superadmin']=="1") || (strtoupper($_SESSION['Medicaldirector'])=="YES" && ($editinfo['Emp_ID']!=$_SESSION['id']))){?>
												<option value="Yes" <?php if($editinfo['Medicaldirector']=="Yes"){  echo 'selected' ;}?>>Yes</option>
												<option value="No" <?php if($editinfo['Medicaldirector']=="No"){  echo 'selected' ;}?>>No</option>
												<?php }else{ if($editinfo['Medicaldirector'] !=""){?>						
														<option value="<?php echo $editinfo['Medicaldirector']?>"><?php echo $editinfo['Medicaldirector']?></option>
												<?php } } ?>					
												</select>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Office locations</label>
												<select class="select-option" name="Location" id="Location">
													<option value="">Select an option</option>
													<?php 
													if( !empty($locationData) ) {
														$LastLocation = "";
														foreach($locationData as $ldata) {
															if( $LastLocation != $ldata["LocationName"] ) {				
													?>
																<option value="<?php echo $ldata['ManageLocationId'];?>"<?php if($editinfo['Location']==$ldata['ManageLocationId']){  echo 'selected' ;}?>><?php echo $ldata['LocationName'];?></option>					
													<?php		
																$LastLocation = $ldata["LocationName"];
															}
														}
													} 
													?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<?php 
								if($_SESSION['Superadmin'] == 0 && !isset($_SESSION['loginuser'])) { ?>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<label id="Label1" class="user-name">Menu Permissions:</label>
												<div class="inputblock-ld radiolabel">
													<div class="radioblocksBtns">
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_new_client" name="menu_permission[]" value="1" <?php echo (in_array(1, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_new_client" class="checkbox-txt">New Client</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_client_lookup" name="menu_permission[]" value="2" <?php echo (in_array(2, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_client_lookup" class="checkbox-txt">Client Lookup</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_pricing_calculator" name="menu_permission[]" value="3" <?php echo (in_array(3, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_pricing_calculator" class="checkbox-txt">Pricing Calculator</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_consent_form_review" name="menu_permission[]" value="4" <?php echo (in_array(4, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_consent_form_review" class="checkbox-txt">Consent Form Review</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_manage_clients" name="menu_permission[]" value="5" <?php echo (in_array(5, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_clients" class="checkbox-txt">Manage Clients</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_manage_employees" name="menu_permission[]" value="6" <?php echo (in_array(6, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_employees" class="checkbox-txt">Manage Employees</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_manage_locations" name="menu_permission[]" value="7" <?php echo (in_array(7, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_locations" class="checkbox-txt">Manage Locations</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_manage_devices" name="menu_permission[]" value="8" <?php echo (in_array(8, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_devices" class="checkbox-txt">Manage Devices</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="9" <?php echo (in_array(9, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_pricing" class="checkbox-txt">Manage Pricing</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="10" <?php echo (in_array(10, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_pricing" class="checkbox-txt">Transactions History</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="11" <?php echo (in_array(11, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_pricing" class="checkbox-txt">Subscription Status</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="12" <?php echo (in_array(12, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_pricing" class="checkbox-txt">Client Status Update</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="13" <?php echo (in_array(13, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_pricing" class="checkbox-txt">Reports</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="14" <?php echo (in_array(14, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_pricing" class="checkbox-txt">Manage Protocols</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_manage_pricing" name="menu_permission[]" value="15" <?php echo (in_array(15, $menuPermissionArr)) ? 'checked' : ''; ?> />
															<label for="menu_manage_pricing" class="checkbox-txt">Manage Legal Disclaimer</label>
														</div>
													
													</div>
												</div>
											</div>
										</div>
									</div>
									
								<?php 
								} ?>

								<div class="form-row-ld">
									<div class="backNextbtn">
										<button type="button" id="submitFormEdit" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
										<div id="insertResultEdit" class="u_mess" style="display:none;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
									</div>
								</div>
						
							</div>
						</form>
                    

					</div>
				</div>
				
				<?php }?>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
