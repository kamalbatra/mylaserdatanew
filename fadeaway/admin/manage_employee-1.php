<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$mangeEmp = new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php 
	}
	
	$tbl_manage_location = "tbl_manage_location";
	$condition = "WHERE BusinessID = $_SESSION[BusinessID] Group BY LocationName ORDER BY ManageLocationId ASC";
	$cols = "*";
	$locationData = $mangeEmp->selectTableRows($tbl_manage_location,$condition);
	
	//print_r($locationData);

?>	
<style>
	.right-margin-6 { margin-right: 6%; }
	.menu-checkbox{ float: left; width: 25%; }
	.addNewReport { float: right; }
	.formdonly {display:none;}
</style>
<script>
	$(document).ready(function() {
		$('#Medicaldirector').change(function() {
			var val=$(this).val();
			if(val=='Yes')
			{
				$('.formdonly').show();	
			}
			else
			{
				$('.formdonly').hide();	
			}
			});	
	});
	</script>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHeadReport">Add New Employee</h1>
		<div class="addNewReport"><a class="empLinks" href="empdetails" class="submit-btn">Employee List </a></div>
	</div>
	<div class="user-entry-block">
	<!--  dummy form to submit data  -->
		<form action="" name="insertFrom" id="insertFrom" method="post">	
			<div class="row">
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">First Name:</label>
					<input class="text-input-field" type="text" name="First_Name" id="First_Name"/>
				</div>				
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Last Name:</label>
					<input class="text-input-field" type="text" name="Last_Name" id="Last_Name"/>
				</div>
			</div><!--End @row-->		
			<div class="row">
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Username:</label>
					<input class="text-input-field"  type="text" name="Username" id="Username"/>
					<span id="usernameExist" class="error"></span>					
				</div>				
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Password:</label>
					<input class="text-input-field" type="password" name="Password" id="Password"/>
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Email:</label>
					<input class="text-input-field" type="email" name="Emp_email" id="Emp_email"/>
					<span id="emailExist" class="error"></span>
				</div>
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">User Type:</label>					
					<select name="Admin" id="Admin" class="select-option">
						<option value="">Select an option-</option>
						<?php
						if(($_SESSION['Superadmin']=="1")) { ?>
							<option value="Admin">Admin</option>
						<?php
						} ?>
						<option value="Employee">Employee</option>
					</select>
				</div>			
			</div><!--End @row-->	
			<div class="row">
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Medical director:</label>					
					<select name="Medicaldirector" id="Medicaldirector" class="select-option">
						<option value="">Select an option-</option>
						<?php 
							if(($_SESSION['Superadmin']=="1")  || (strtoupper($_SESSION['Medicaldirector'])=="YES")) { ?>
								<option value="Yes">Yes</option>
						<?php
							} ?>
						<option value="No">No</option>
					</select>
				</div>
				<!--div class="span6">
					<label id="Label1" class="user-name">locations:</label>
					<select class="select-option" name="location" id="location">
					<option value="">Select an option</option>					
					<option value="Duluth">Duluth</option>
					<option value="Minneapolis">Minneapolis and St Paul</option>
					</select>
				</div-->				
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Office locations:</label>
					<select class="select-option" name="Location" id="Location">
						<option value="">Select an option</option>
						<?php 
							if(!empty($locationData)){
								$OldLocation = "";
								foreach($locationData as $ldata) { 
									if( $OldLocation != $ldata['LocationName'] ) {
										echo "<option value='$ldata[ManageLocationId]'>$ldata[LocationName]</option>";
										$OldLocation = 	$ldata['LocationName'];				
									}
								}
							}
						?>
					</select>					
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6 right-margin-6">
					<label id="Label1" class="user-name">Address:</label>
					<textarea name="Office_Addr" id="Office_Addr" rows="4" class="text-area-field"></textarea>
				</div>
			</div><!--End @row-->
			<input type="hidden" name="hidenMenuPer" id="hidenMenuPer" value="1|2|3|4|5|6|7|8|9|10|11|12|13|14|15">
		<?php //echo $_SESSION['loginuser'];
			if($_SESSION['Superadmin'] == 0 && !isset($_SESSION['loginuser'])) { ?>
				<div class="row">
					<div class="span12">
						<label id="Label1" class="user-name">Menu Permissions:</label>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_new_client" name="menu_permission[]" value="1" />
							<label for="menu_new_client" class="checkbox-txt">New Client</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" id="menu_client_lookup" name="menu_permission[]" value="2" />
							<label for="menu_client_lookup" class="checkbox-txt">Client Lookup</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_pricing_calculator" name="menu_permission[]" value="3" />
							<label for="menu_pricing_calculator" class="checkbox-txt">Pricing Calculator</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_consent_form_review" name="menu_permission[]" value="4" />
							<label for="menu_consent_form_review" class="checkbox-txt">Consent Form Review</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_manage_clients" name="menu_permission[]" value="5" />
							<label for="menu_manage_clients" class="checkbox-txt">Manage Clients</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_manage_employees" name="menu_permission[]" value="6" />
							<label for="menu_manage_employees" class="checkbox-txt">Manage Employees</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_manage_locations" name="menu_permission[]" value="7" />
							<label for="menu_manage_locations" class="checkbox-txt">Manage Locations</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_manage_devices" name="menu_permission[]" value="8" />
							<label for="menu_manage_devices" class="checkbox-txt">Manage Devices</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="9" />
							<label for="menu_manage_pricing" class="checkbox-txt">Manage Pricing</label>
						</div>
						<div class="menu-checkbox formdonly">
							<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="10" />
							<label for="menu_manage_pricing" class="checkbox-txt">Transactions History</label>
						</div>
						<div class="menu-checkbox formdonly">
							<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="11" />
							<label for="menu_manage_pricing" class="checkbox-txt">Subscription Status</label>
						</div>
						<div class="menu-checkbox formdonly">
							<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="12" />
							<label for="menu_manage_pricing" class="checkbox-txt">Client Status Update</label>
						</div>
						<div class="menu-checkbox formdonly">
							<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="13" />
							<label for="menu_manage_pricing" class="checkbox-txt">Reports</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="14" />
							<label for="menu_manage_pricing" class="checkbox-txt">Manage Protocols</label>
						</div>
						<div class="menu-checkbox">
							<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="15" />
							<label for="menu_manage_pricing" class="checkbox-txt">Manage Legal Disclaimer</label>
						</div>
					</div>
				</div>
			<?php 
			} ?>
			
			
			<!--div class="row">
				<div class="span6">
					<label id="Label1" class="user-name">Options:</label>
					<select class="select-option" name="admin_co" id="admin_co">
					<option value="">Select an option-</option>
					
					<option value="Counter">Counter</option>
					<option value="Rewind">Rewind</option>
					</select>
				</div>
			</div--><!--End @row-->
			<div class="row">
				<div class="span12">
					<!--input type="submit" name="name" id="submitForm" value="submit" class="submit-btn"><br/-->
					<input type="button"  id="submitForm" value="submit" class="submit-btn" style="float:left;">
					<div id="insertResult" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
				</div>			
			</div><!--End @row-->
		</form>
	</div>
</div>
</div>
<?php include('admin_includes/footer.php'); ?>
