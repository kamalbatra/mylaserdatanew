<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$mangeEmp = new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php 
	}
	$tbl_manage_location = "tbl_manage_location";
	$condition = "WHERE BusinessID = $_SESSION[BusinessID] Group BY LocationName ORDER BY ManageLocationId ASC";
	$cols = "*";
	$locationData = $mangeEmp->selectTableRows($tbl_manage_location,$condition);
?>
	<style>
		.right-margin-6 { margin-right: 6%; }
		.menu-checkbox{ float: left; width: 25%; }
		.addNewReport { float: right; }
		.formdonly {display:none;}
	</style>
	<script>
		$(document).ready(function() {
			$('#Medicaldirector').change(function() {
				var val=$(this).val();
				if(val=='Yes') {
					$('.formdonly').show();	
				} else {
					$('.formdonly').hide();	
				}
			});	
		});
	</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Add New Employee</h1>
				</div>	
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="empdetails" class="submit-btn"><button class="addnewbtn">Employee list </button></a>
						</div>
					</div>
					<div class="formcontentblock-ld">
						<form action="" name="insertFrom" id="insertFrom" method="post">	
							<div class="formcontentblock-ld">
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>First Name</label>
												<input class="text-input-field" type="text" name="First_Name" id="First_Name"/>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Last Name:</label>
												<input class="text-input-field" type="text" name="Last_Name" id="Last_Name"/>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Username</label>
												<input class="text-input-field"  type="text" name="Username" id="Username"/>
												<div id="usernameExist" class="errorblocks"></div>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Password:</label>
												<input class="text-input-field" type="password" name="Password" id="Password"/>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Email</label>
												<input class="text-input-field" type="email" name="Emp_email" id="Emp_email"/>
												<span id="emailExist" class="error"></span>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>User Type:</label>
												<select name="Admin" id="Admin" class="select-option">
												<option value="">Select an option-</option>
												<?php
												if(($_SESSION['Superadmin']=="1")) { ?>
													<option value="Admin">Admin</option>
												<?php
												} ?>
												<option value="Employee">Employee</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-row-ld">
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Medical director</label>
												<select name="Medicaldirector" id="Medicaldirector" class="select-option">
													<option value="">Select an option-</option>
													<?php 
														if(($_SESSION['Superadmin']=="1")  || (strtoupper($_SESSION['Medicaldirector'])=="YES")) { ?>
															<option value="Yes">Yes</option>
													<?php
														} ?>
													<option value="No">No</option>
												</select>
											</div>
										</div>
									</div>
									<div class="half">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label>Office locations</label>
												<select class="select-option" name="Location" id="Location">
													<option value="">Select an option</option>
													<?php 
														if(!empty($locationData)){
															$OldLocation = "";
															foreach($locationData as $ldata) { 
																if( $OldLocation != $ldata['LocationName'] ) {
																	echo "<option value='$ldata[ManageLocationId]'>$ldata[LocationName]</option>";
																	$OldLocation = 	$ldata['LocationName'];				
																}
															}
														}
													?>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="form-row-ld">
									<div class="full">
									  <div class="form-col-ld">
										  <div class="inputblock-ld bigtextarea">
											<label>Address</label>
											<textarea name="Office_Addr" id="Office_Addr" rows="4" class="text-area-field"></textarea>
										  </div>
									  </div>
									</div>
								</div>
								<input type="hidden" name="hidenMenuPer" id="hidenMenuPer" value="1|2|3|4|5|6|7|8|9|10|11|12|13|14|15">
								<?php //echo $_SESSION['loginuser'];
								if($_SESSION['Superadmin'] == 0 && !isset($_SESSION['loginuser'])) { ?>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<label id="Label1" class="user-name">Menu Permissions:</label>
												<div class="inputblock-ld radiolabel">
													<div class="radioblocksBtns">
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_new_client" name="menu_permission[]" value="1" />
															<label for="menu_new_client" class="checkbox-txt">New Client</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" id="menu_client_lookup" name="menu_permission[]" value="2" />
															<label for="menu_client_lookup" class="checkbox-txt">Client Lookup</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_pricing_calculator" name="menu_permission[]" value="3" />
															<label for="menu_pricing_calculator" class="checkbox-txt">Pricing Calculator</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_consent_form_review" name="menu_permission[]" value="4" />
															<label for="menu_consent_form_review" class="checkbox-txt">Consent Form Review</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_manage_clients" name="menu_permission[]" value="5" />
															<label for="menu_manage_clients" class="checkbox-txt">Manage Clients</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_manage_employees" name="menu_permission[]" value="6" />
															<label for="menu_manage_employees" class="checkbox-txt">Manage Employees</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_manage_locations" name="menu_permission[]" value="7" />
															<label for="menu_manage_locations" class="checkbox-txt">Manage Locations</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_manage_devices" name="menu_permission[]" value="8" />
															<label for="menu_manage_devices" class="checkbox-txt">Manage Devices</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="9" />
															<label for="menu_manage_pricing" class="checkbox-txt">Manage Pricing</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="10" />
															<label for="menu_manage_pricing" class="checkbox-txt">Transactions History</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="11" />
															<label for="menu_manage_pricing" class="checkbox-txt">Subscription Status</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="12" />
															<label for="menu_manage_pricing" class="checkbox-txt">Client Status Update</label>
														</div>
														<div class="radioBtn ckeckboxres formdonly">
															<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="13" />
															<label for="menu_manage_pricing" class="checkbox-txt">Reports</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="14" />
															<label for="menu_manage_pricing" class="checkbox-txt">Manage Protocols</label>
														</div>
														<div class="radioBtn ckeckboxres">
															<input type="checkbox" class="permison" id="menu_manage_pricing" name="menu_permission[]" value="15" />
															<label for="menu_manage_pricing" class="checkbox-txt">Manage Legal Disclaimer</label>
														</div>


													</div>
												</div>
											</div>
										</div>
									</div>
									
								<?php 
								} ?>
							
							<div class="form-row-ld">
								<div class="backNextbtn">
									<button type="button"  id="submitForm" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
									<div id="insertResult" class="u_mess" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
								</div>
							</div>
							
							</div>
						</form>
                    

					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
