<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$mnglocationDetails	= new dbFunctions();
	
	if( !in_array(7,$_SESSION["menuPermissions"])) { 
		
		?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php
	}

	if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] != 'Admin') { ?>
<script>
		// window.location.replace("dashboard");
</script>
<?php } else if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] == 'Admin'){
	
	}
	$services = implode(",",$_SESSION["services"]);
	/*** fetch Add Manage location**/
	$tbl_manage_location = "tbl_manage_location";
	$tbl_devicename	= "tbl_devicename";
	$tbl_servicename = "tbl_master_services";
	
	/*$DCond = "Where BusinessID='".$_SESSION["BusinessID"]."' AND status=1 AND serviceId in($services)";
	$DCols = "deviceId,DeviceName,serviceId";
	$DeviceData = $mnglocationDetails->selectTableRows($tbl_devicename,$DCond,$DCols);
	if( count($DeviceData) >0 ) {
		$DeviceIDs = "";
		for( $i=0;$i<count($DeviceData);$i++ ) {
			$DeviceIDs.=$DeviceData[$i]["deviceId"];
			if( $i < count($DeviceData)-1 )
				$DeviceIDs.=",";
		}
		$LCond = "WHERE BusinessID=$_SESSION[BusinessID] AND deviceId in($DeviceIDs) Group BY LocationName ";
	} else {
		$LCond = "WHERE BusinessID=$_SESSION[BusinessID] AND deviceId=0 Group BY LocationName ";
	}*/
	$LCond = "WHERE BusinessID=$_SESSION[BusinessID] Group BY LocationName ";
	$LCols = "*";
	$LocationData = $mnglocationDetails->selectTableRows($tbl_manage_location,$LCond,$LCols);	 
	$countlocdata = count($LocationData);
	
	$tbl_business = "tbl_business";
	$LColsbusiness = "LocationPackage";
	$LCondbusiness = "WHERE BusinessID=$_SESSION[BusinessID] ";
	$LocationbusinessData = $mnglocationDetails->selectTableRows($tbl_business,$LCondbusiness,$LColsbusiness);	 
	
	$LocationPackage = $LocationbusinessData[0]['LocationPackage'];		
	
	/*** fetch All device Name**/
?>
	<script type="text/javascript">
		$(document).ready(function() {
			var hostname=document.domain;
		var pathname = window.location.pathname; // Returns path only
		var url      = window.location.href;     // Returns full URL
			//Delete flunce from database
			setTimeout(function() {
				history.pushState('', 'Dashboard', 'http://'+hostname+''+pathname); 
				},1000);
			$('a.deletelocation').click(function() {
				if (confirm("Are you sure you want to delete this location?")) {
					var id = $(this).parent().parent().attr('id');
					var data = 'ManageLocationId=' + id +'&location='+'location';
					var parent = $(this).parent().parent();			
					//alert(data);
					$.ajax({
						type: "POST",
						url: "delete_location.php",
						data: data,
						cache: false,
						success: function(data) {
							parent.fadeOut('slow', function() {$(this).remove();});
							$('.showmsg').show();
							$('.successmsg').html("");
							$('.successmsgtext').html('Location deleted successfully.');
							setTimeout(function() {
								$('.showmsg').hide();
								location.reload();
								}, 4000);
						}
					});			
				}
			});
			// style the table with alternate colors
			// sets specified color for every odd row
			/*$('#'+id).css('background',' #FFFFFF');*/
		});
	</script>	
	<style>
	.srtHeadloc { width:18%; }
	.successmsg
	{
	 float:left;
	 width:100%;
	 text-align:center;
	 margin-bottom:15px !important;
	}
	
.successmsg1 {
    float: left;
    margin-bottom: 15px !important;
    text-align: center;
    width: 100%;
}
.deletelocation
{
cursor:pointer;
}
	</style>
	<?php if(isset($_GET["msg"]) && $_GET["msg"]=="success")
			echo "<span class='successmsg'><font color='green'>Location(s) added successfully.</font></span>";
		?> 
	<div class="showmsg" style="display:none;">
		<span class='successmsg1'><font color='green' class="successmsgtext"></font></span>
		</div>
	<div class="form-container">
		
		<div class="heading-container">
			<h1 class="heading empHead">Manage Locations</h1>
			<?php
			if( $countlocdata == 0 &&  $LocationPackage == 1 ){
			?>	
			<div class="addNew">
				<a class="empLinks" href="manage-location" class="submit-btn">Add New Location</a>
			</div>			
			<?php
			} else if( $LocationPackage > 1 ){
			?>
			<div class="addNew">
				<a class="empLinks" href="manage-location" class="submit-btn">Add New Location</a>
			</div>
			<?php
			} else {
				
			}
			?>			
		</div>
		<div class="row">	
			<!--div class="span3 adhead"><a href="manage-location.php">Add location</a></div>
			<div class="span3 adhead"><a href="manage-location-details.php"> Locations list</a></div-->
			<!--div class="span3 adhead"><a href=""> Detail </a></div>
			<div class="span3 adhead"> .</div-->
		</div>
		<div class="user-entry-block">
<div class="tablebushead">	
	<div class="tablebusinner">	
			<div class="row sortingHead">	
				<div class="span3 srtHeadloc srtHeadBorder"> Location Name</div>
				<div class="span3 srtHeadloc srtHeadBorder"> Address</div>
				<div class="span3 srtHeadloc srtHeadBorder"> Device Name</div>
				<div class="span3 srtHeadloc srtHeadBorder"> Service Name</div>
				<div class="span3 srtHeadEdit srtHeadBorder"> Action</div>
				<div class="span3 srtHeadEdit srtHeadBorder" style="border:none;"> Action</div>
			</div>        
        	<?php 
        	if(!empty($LocationData)) { 
				$i = 0;
				foreach($LocationData as $location) {
					$devsercol = "deviceId";
					$devsercond = " WHERE LocationName = '$location[LocationName]'";
					$devSerData = $mnglocationDetails->selectTableRows($tbl_manage_location,$devsercond,$devsercol);
					$alldev = $allser = '';
					$i=$j=0;
					foreach($devSerData AS $key => $devId){
						$condition = "Where deviceId=".$devId['deviceId']."";
						$cols="DeviceName,serviceId";
						$deviceName = $mnglocationDetails->selectTableSingleRow($tbl_devicename,$condition,$cols);
						if (strpos($alldev, $deviceName['DeviceName']) === false){
							if($deviceName['DeviceName']!='')
							{
								if($i > 0)
								{
								$alldev .= ', ';	
								}
								$alldev .= $deviceName['DeviceName'];
								$i++;
							}
						}
						
						$SCond = "where id=".$deviceName['serviceId'];
						$SCols = "name";
						$ServiceName = $mnglocationDetails->selectTableSingleRow($tbl_servicename,$SCond,$SCols);
						if (strpos($allser, $ServiceName['name']) !== false)
							continue;
						if($ServiceName['name']!='')
						{
							if($j > 0)
							{
							$allser .= ', ';	
							}
							$allser .= $ServiceName['name'];
							$j++;
						}
					}
					if($i%2==0) {
						$bgdata = "bgnone";	
					} else {
						$bgdata = "bgdata";
					}
					$condition = "Where deviceId=".$location['deviceId']."";
					$cols="DeviceName,serviceId";
					$deviceName = $mnglocationDetails->selectTableSingleRow($tbl_devicename,$condition,$cols);
				?>
					<div class="row  <?php echo $bgdata;?>" id="<?php echo $location['ManageLocationId']?>" >
						<div class="span3 srtHeadloc srtcontent">
							<label id="" class="user-name"><?php echo $location['LocationName']?> </label>						
						</div>
						<div class="span3 srtHeadloc srtcontent">	
							<label id="" class="user-name"><?php echo $location['ManageAddress']?></label>
						</div>
						<div class="span3 srtHeadloc srtcontent">	
							<label id="" class="user-name">
							<?php echo $alldev; ?>
							</label>
						</div>
						<div class="span3 srtHeadloc srtcontent">
							<label id="" class="user-name"><?php echo $allser; ?></label>
						</div>
						<div class="span3 srtHeadEdit srtcontent text-align">	
							<label id="" class="user-name">
								<a href="manage-location-edit?ManageLocationId=<?php echo $location['ManageLocationId']?>&action=manage"><img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit location"/></a>
							</label>
						</div>
						<div class="span3 srtHeadEdit srtcontent text-align">
							<a class="deletelocation">
								<img src="<?php echo $domain; ?>/images/b_drop.png" title="Delete flunce name"/>
							</a>
						</div>
					</div><!--End @row-->	
				<?php
					$i++;
				}
			} else 
				echo "<div class='not-found-data'>No location found.</div>"; ?>
		</div><!--End @user-entry-block-->
	</div><!-- End  @form-container--->
</div><!--End @container-->
</div><!--End @container-->
</div><!--End @container-->
<?php
	include('admin_includes/footer.php');	
?>
