<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$mnglocationDetails	= new dbFunctions();
	if( !in_array(7,$_SESSION["menuPermissions"])){ ?> 
		<script>
			window.location.replace("dashboard.php");
		</script>
	<?php }
	if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] != 'Admin') { ?>
	<script>
		// window.location.replace("dashboard");
	</script>
	<?php 
	} else if($_SESSION['loginuser'] != "sitesuperadmin" && $_SESSION["Usertype"] == 'Admin'){
	
	}
	$services = implode(",",$_SESSION["services"]);
	/*** fetch Add Manage location**/
	$tbl_manage_location = "tbl_manage_location";
	$tbl_devicename	= "tbl_devicename";
	$tbl_servicename = "tbl_master_services";
	
	$LCond = "WHERE BusinessID=$_SESSION[BusinessID] Group BY LocationName ";
	$LCols = "*";
	$LocationData = $mnglocationDetails->selectTableRows($tbl_manage_location,$LCond,$LCols);	 
	$countlocdata = count($LocationData);
	
	$tbl_business = "tbl_business";
	$LColsbusiness = "LocationPackage";
	$LCondbusiness = "WHERE BusinessID=$_SESSION[BusinessID] ";
	$LocationbusinessData = $mnglocationDetails->selectTableRows($tbl_business,$LCondbusiness,$LColsbusiness);	 
	
	$LocationPackage = $LocationbusinessData[0]['LocationPackage'];		
	
	/*** fetch All device Name**/
?>
	<script type="text/javascript">
		$(document).ready(function() {
			var hostname=document.domain;
		var pathname = window.location.pathname; // Returns path only
		var url      = window.location.href;     // Returns full URL
			//Delete flunce from database
			setTimeout(function() {
				history.pushState('', 'Dashboard', 'http://'+hostname+''+pathname); 
				},1000);
			$('a.deletelocation').click(function() {
				if (confirm("Are you sure you want to delete this location?")) {
					var id = $(this).parent().parent().attr('id');
					var data = 'ManageLocationId=' + id +'&location='+'location';
					var parent = $(this).parent().parent();			
					//alert(data);
					$.ajax({
						type: "POST",
						url: "delete_location.php",
						data: data,
						cache: false,
						success: function(data) {
							parent.fadeOut('slow', function() {$(this).remove();});
							$('.showmsg').show();
							$('.successmsg').html("");
							$('.successmsgtext').html('Location deleted successfully.');
							setTimeout(function() {
								$('.showmsg').hide();
								location.reload();
								}, 4000);
						}
					});			
				}
			});
		});
	</script>
	<style>
		.srtHeadloc { width:18%; }
		.successmsg	{ float:left;width:100%;text-align:center;margin-bottom:15px !important;	}
		.successmsg1 { float: left; margin-bottom: 15px !important; text-align: center; width: 100%; }
		.deletelocation { cursor:pointer; }
	</style>
	<?php if(isset($_GET["msg"]) && $_GET["msg"]=="success")
		echo "<span class='successmsg'><font color='green'>Location(s) added successfully.</font></span>";
	?> 
	<div class="showmsg" style="display:none;">
		<span class='successmsg1'><font color='green' class="successmsgtext"></font></span>
	</div>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Locations</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<?php
							if( $countlocdata == 0 &&  $LocationPackage == 1 ){
							?>					
								<a class="empLinks" href="manage-location" class="submit-btn"><button class="addnewbtn">Add New Location </button></a>
							<?php
							} else if( $LocationPackage > 1 ){ ?>					
								<a class="empLinks" href="manage-location" class="submit-btn"><button class="addnewbtn">Add New Location </button></a>
							<?php
							} else {   } 
							?>	
						</div>
					</div>
					<div class="card-body">
					<?php
					    if(!empty($LocationData)) { 
							$i = 0;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th class="span3 srtHeadloc srtHeadBorder">Location Name</th>
										<th class="span3 srtHeadloc srtHeadBorder">Address</th>
										<th class="span3 srtHeadloc srtHeadBorder">Device Name</th>
										<th class="span3 srtHeadloc srtHeadBorder">Service Name</th>
										<th class="span3 srtHeadEdit srtHeadBorder center-text">Action</th>
										<th class="span3 srtHeadEdit srtHeadBorder center-text">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									foreach($LocationData as $location) {
										$devsercol = "deviceId";
										$devsercond = " WHERE LocationName = '$location[LocationName]'";
										$devSerData = $mnglocationDetails->selectTableRows($tbl_manage_location,$devsercond,$devsercol);
										$alldev = $allser = '';
										$i=$j=0;
										foreach($devSerData AS $key => $devId){
											$condition = "Where deviceId=".$devId['deviceId']."";
											$cols="DeviceName,serviceId";
											$deviceName = $mnglocationDetails->selectTableSingleRow($tbl_devicename,$condition,$cols);
											if (strpos($alldev, $deviceName['DeviceName']) === false){
												if($deviceName['DeviceName']!='')
												{
													if($i > 0)
													{
													$alldev .= ', ';	
													}
													$alldev .= $deviceName['DeviceName'];
													$i++;
												}
											}											
											$SCond = "where id=".$deviceName['serviceId'];
											$SCols = "name";
											$ServiceName = $mnglocationDetails->selectTableSingleRow($tbl_servicename,$SCond,$SCols);
											if (strpos($allser, $ServiceName['name']) !== false)
												continue;
											if($ServiceName['name']!='')
											{
												if($j > 0)
												{
												$allser .= ', ';	
												}
												$allser .= $ServiceName['name'];
												$j++;
											}
										}
										if($i%2==0) {
											$bgdata = "bgnone";	
										} else {
											$bgdata = "bgdata";
										}
										$condition = "Where deviceId=".$location['deviceId']."";
										$cols="DeviceName,serviceId";
										$deviceName = $mnglocationDetails->selectTableSingleRow($tbl_devicename,$condition,$cols);
									?>
										<tr class=" <?php echo $bgdata;?>" id="<?php echo $location['ManageLocationId']?>" >
											<td class="span3 srtHeadloc srtcontent"><label id="" class="user-name"><?php echo $location['LocationName']?></label></td>
											<td class="span3 srtHeadloc srtcontent"><label id="" class="user-name"><?php echo $location['ManageAddress']?></label></td>
											<td class="span3 srtHeadloc srtcontent"><label id="" class="user-name"><?php echo $alldev; ?></label></td>
											<td class="span3 srtHeadloc srtcontent"><label id="" class="user-name"><?php echo $allser; ?></label></td>
											<td class="span3 srtHeadEdit srtcontent text-align center-text"><label id="" class="user-name"><a href="manage-location-edit?ManageLocationId=<?php echo $location['ManageLocationId']?>&action=manage"><img src="<?php echo $domain; ?>/img/editimg.png" title="Edit location"/></a></label></td>
											<td class="span3 srtHeadEdit srtcontent text-align center-text"><a class="deletelocation"><img src="<?php echo $domain; ?>/img/minusbtn.png" title="Delete flunce name"/></a></td>										
										</tr><!--End @row-block-->
										<?php
										$i++;
									} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
						}
						else {
							echo "<div class='not-found-data'>No location found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
