<?php

	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$mangeEmp = new dbFunctions();
	
	if( !in_array(14,$_SESSION["menuPermissions"])) { ?> 
		<script>
			var hostname=document.domain;
			var pathname = window.location.pathname; // Returns path only
			var url      = window.location.href;  
			window.location.replace("dashboard");
		</script>
<?php 
	}
	$tbl_manage_protocols = "tbl_protocols";
	$condition = " left join tbl_employees as e on e.Emp_ID=tbl_protocols.uploaded_by where tbl_protocols.bussiness_id=".$_SESSION['BusinessID']." ORDER BY tbl_protocols.id DESC";
	$cols = " tbl_protocols.protocol_name, tbl_protocols.status, tbl_protocols.uploaded_on, e.First_Name, e.Last_Name, tbl_protocols.id ";
	$protcolData = $mangeEmp->selectTableRows($tbl_manage_protocols,$condition,$cols);
	
	$condition = " where bussiness_id=".$_SESSION['BusinessID']." and status=1 ORDER BY tbl_protocols.id DESC";
	$cols = " id ";
	$activeProtocol = $mangeEmp->selectTableRows($tbl_manage_protocols,$condition,$cols);
	$status=0;
	if(empty($activeProtocol))
	{
	 $status=1;
	}
	$data=array();
	if(isset($_POST) && ($_POST['submitProtocol']=="Submit"))
	{
		$original_name=$_FILES["updProtocol"]["name"];
		$target_dir = getcwd()."/upload_protocols/";
		$temp = explode(".", $_FILES["updProtocol"]["name"]);
		$rand=strtotime(date('Y-m-d H:i:s'));
		$newfilename = $rand.'_'.$original_name;
		$target_file = $target_dir . basename($newfilename);
		$uploadOk = 1;
		if (file_exists($target_file)) {
			echo "Sorry, file already exists.";
			$uploadOk = 0;
		}
		if($uploadOk=='1')
		{
			$host='http://'.$_SERVER['HTTP_HOST'];
			$full_url=$host.$_SERVER['REQUEST_URI'].'?msg=1'; 
			if (move_uploaded_file($_FILES["updProtocol"]["tmp_name"], $target_file)) {
					$data['protocol_name']=$newfilename;
					$data['uploaded_by']=$_SESSION['id'];
					$data['bussiness_id']=$_SESSION['BusinessID'];
					$data['status']=$status;
					$fullinfo = new dbFunctions();
					$fullinfo->insert_data($tbl_manage_protocols,$data);
					$id=mysqli_insert_id($fullinfo->con);
					if($id!='')
					{
						//header('Location:'.$full_url);
						echo '<script>location.href="'.$full_url.'"</script>';
					}
			}
		}
	}
?>	
<style>
	.right-margin-6 { margin-right: 6%; }
	.menu-checkbox{ float: left; width: 25%; }
	.addNewReport { float: right; }
	.insertResultSuccess {
		display: none;
		float: left;
		line-height: 48px;
		padding-left: 11px !important;
		color:green;font-size:14px;
	}
	.insertResult{
		float: left;
		line-height: 48px;
		padding-left: 11px !important;
		color:green;font-size:14px;
	}
</style>
<script>
	$(document).ready(function() {
		var hostname=document.domain;
		var pathname = window.location.pathname; // Returns path only
		var url      = window.location.href;     // Returns full URL
		//alert(url);
		<?php if($_REQUEST['msg']) { ?>
		 $('html,body').animate({
			scrollTop: $("#containerManageProtocol").offset().top},
        'slow');
        setTimeout(function() {
			$('.insertResult').html("");
			history.pushState('', 'Dashboard', 'http://'+hostname+''+pathname); 
		}, 3000);
        
        <?php } ?>
		$('input[type=radio][name=statusRadio]').change(function() {
			var staus=$(this).val();
			var data = 'statusPro=' + staus;
			$("#insertResult").show();	
			$.ajax({
				type: "POST",
				url: "ajax_changetatus.php",
				data: data,
				cache: false,
				success: function(result){
					if(result ==1)	{
						/*$('html,body').animate({
							scrollTop: $("#containerManageProtocol").offset().top},
						'slow');
						$("#insertResult").hide('slow');	
						$(".insertResultSuccess").html('Status activated successfully.');
						$('.insertResultSuccess').show('slow');*/
						setTimeout(function() { 
							window.location.href='http://'+hostname+''+pathname+'?msg=2';
							 }, 800);
					}
				}
			});
		});
	});
	function delFunction(id, protocol_name)
	{
		var hostname=document.domain;
		var pathname = window.location.pathname; // Returns path only
		var url      = window.location.href;
		if(confirm("Are you sure you want to delete this protocol?"))
		{
			var data="protocol_id="+id+"& protocol_name="+protocol_name;
		 	$.ajax({
				type: "POST",
				url: "delete_protocol.php",
				data: data,
				cache: false,
				success: function(result){
					if(result==1)	{
						setTimeout(function() { 
							window.location.href='http://'+hostname+''+pathname+'?msg=3';
							 }, 1000);
					}
				}
			});
		}
		else
		{
		 return false;
		}
	}
</script>
<div class="form-container" id="containerManageProtocol">
	<div class="heading-container">
		<h1 class="heading empHeadReport">Manage Protocols</h1>
		<!--div class="addNewReport"><a class="empLinks" href="empdetails.php" class="submit-btn">Employee list </a></div-->
	</div>
	<div class="user-entry-block">
	<!--  dummy form to submit data  -->
		<form action="" name="manageProtocol" id="manageProtocol" method="post" enctype="multipart/form-data">	
			<div class="row updprotocolSection-row">
				<div class="span6 right-margin-6 updprotocolSection">
					<label id="Label1" class="user-name">Upload Protocol</label>
					<input accept="application/pdf" class="text-input-field" type="file" name="updProtocol" id="updProtocol"/><p class="helptext">(Upload only pdf file.)</p>
					<span for="DeviceName" class="error fileupProto" style="display:none;">Please upload protocol file.</span>
				</div>				
				
			</div><!--End @row-->		
			<div class="row">
				<div class="span12">
					<!--input type="submit" name="name" id="submitForm" value="submit" class="submit-btn"><br/-->
					<input type="submit" name="submitProtocol" id="submitFormPro" value="Submit" class="submit-btn" style="float:left;">
					<div id="insertResult" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
					<?php if($_REQUEST['msg']!="" && ($_REQUEST['msg']=='1')) { ?>
					<div class="insertResult">Result inserted successfully.</div>
					<?php }else if($_REQUEST['msg']!="" && ($_REQUEST['msg']=='2')) { ?>
					<div class="insertResult">Status activated successfully.</div>
					<?php } else if($_REQUEST['msg']!="" && ($_REQUEST['msg']=='3')) { ?>
					<div class="insertResult">Protocol deleted successfully.</div>
					<?php } ?>
				</div>			
			</div><!--End @row-->
		</form>
	</div>
	  <div class="heading-container">
		  <h1 class="heading empHeadReport">Protocols</h1>
	   </div>
		<div class="user-entry-block updProtocolDiv">
			<div class="tablebushead">
		<div class="tablebusinner">
			<div class="row sortingHead">	
				<div class="span3 srtHead srtHeadBorder snfiled"> S.No</div>
				<div class="span3 srtHead srtHeadBorder" style="width:27%;"> Protocol</div>
				<div class="span3 srtHead srtHeadBorder"> Uploaded By</div>
				<div class="span3 srtHead srtHeadBorder"  style="width:20%"> Uploaded On</div>
				<div class="span3 srtHead srtHeadBorder"> Status</div>
				<div class="span3 srtHead srtHeadBorder"> Action</div>
	       </div>        
        	<?php $i = 0;
        	if(!empty($protcolData))
        	{
				foreach($protcolData as $dataemp){
					$i++;
					//print_r($dataemp);					
					/*if($i%2==0){$bgdata = "bgnone";	}
						else{$bgdata = "bgdata";}							
					if($_SESSION['id']==$dataemp['Emp_ID']){
						$currentLogin= "currentLogin";
					}else{ $currentLogin= "";}*/
				?>	
                   <div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>" id="<?php echo $dataemp['Emp_ID']?>" status="<?php echo $dataemp['status'];?>" >
					 <div class="span3 srtHead srtcontent snfiled">
						<label id="" class="user-name"><?php echo $i; ?> </label>						
					</div>					
					<div class="span3 srtHead srtcontent" style="width:27%;">	
					  <label id="" class="user-name"><?php echo $dataemp['protocol_name']?></label>
					</div>					
					<div class="span3 srtHead srtcontent">	
					  <label id="" class="user-name" style="text-transform:none;"> <?php echo $dataemp['First_Name'].' '.$dataemp['Last_Name']; ?>	</label>
					</div>
					<div class="span3 srtHead srtcontent" style="width:20%">	
					  <label id="" class="user-name" style="text-transform:none;"> <?php echo date('F m,Y', strtotime($dataemp['uploaded_on'])); ?>	</label>
					</div>
					<div class="span3 srtHead srtcontent" >	
					  <label id="" class="user-name" style="text-transform:none;margin-left: 30px;"> 
					  <?php 
					  $checked="";$status="Inactive";
					  if($dataemp['status']==1) { 
						  $checked="checked";
					  }
						  ?>
					  <input class="cursorPointer" type="radio" value="<?php echo $dataemp['id']; ?>" name="statusRadio" <?php echo $checked; ?>><?php //echo $status; ?></label>
					</div>
					 <div class="span3 srtHead srtcontent text-align" >
						<label id="" class="user-name">
							<?php  if($dataemp['status']!=1) {  ?>
								<a class="deletelocation cursorPointer" onclick="return delFunction(<?php echo $dataemp['id']; ?>,'<?php echo $dataemp['protocol_name']?>')">
								<img title="Delete Protocol" src="<?php echo $domain; ?>/images/b_drop.png">
							</a>
							<?php } ?>
						</label>						
					</div>	
			    </div><!--End @row-->				  
			  <?php   }} else { ?>
			  <div class="row">
						<p style="text-align:center; font-size:13px;padding-top:12px;">No Protocol updated yet.</p>	
			  </div>
			  <?php } ?>			  
			  <!--div id="statuResult"></div-->      
    </div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!-- End  @form-container--->
</div><!-- End  @form-container--->
</div>


</div>
	 
<?php include('admin_includes/footer.php'); ?>
