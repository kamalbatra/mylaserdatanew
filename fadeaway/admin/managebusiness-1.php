<?php
session_start();
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
if($_SESSION['loginuser'] != "sitesuperadmin"){
	?>
	<script>
		$(function(){
			window.location.replace("dashboard?msg=noper");
		});
	</script>
	<?php
	die;
}
if( !isset($_GET["page"])) {
	unset($_SESSION["searchname"]);
	unset($_SESSION["searchdate"]);
}

$var = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);

$BusinessDetails = new dbFunctions();
$BTable 	= "tbl_business tblB";
$SubTable 	= "tbl_subscription_history tblS";
$ETable 	= "tbl_employees tblE";
$PTable 	= "tbl_master_plans tblP";
$cols 		= "tblB.BusinessID,tblB.BusinessName,tblB.Logo,tblB.PlanType,tblB.status,tblS.ID,tblS.PlanID,tblS.ExpireDate";
$adjacents 	= 3;
$reload 	= "managebusiness";
if(isset($_GET['page'])){
	$page=$_GET['page'];
}
else{
	$page="";
}
$limit = 5;                                  //how many items to show per page
if($page)
	$start = ($page - 1) * $limit;          //first item to display on this page
else
	$start = 0;

if(isset($_POST["searchname"])) 
	$_SESSION["searchname"] = $_POST["searchname"];
if(isset($_POST["searchdate"]))
	$_SESSION["searchdate"] = $_POST["searchdate"];

if((isset($_SESSION["searchname"]) && $_SESSION["searchname"]!="") || (isset($_SESSION["searchdate"]) && $_SESSION["searchdate"]!="")) {
 	$BName = $_SESSION['searchname'];
	$ExpDate = $_SESSION['searchdate'];
	if(isset($_SESSION['searchdate']) && $_SESSION['searchdate']!= "") {
		$cdate = date('Y-m-d');
		//$cdate = strtotime($cdate,"Y-m-d");
		$Expire = date("Y-m-d", strtotime($ExpDate . ' +1 day'));
	}
	if($BName!="" && $ExpDate!="") {
		//$conditionNumRows	= "tblB.BusinessID=tblS.BusinessID GROUP BY tblS.BusinessID HAVING tblB.BusinessName like '%".$BName."%' AND tblS.ExpireDate between '$cdate' AND '$Expire'  ORDER BY tblS.ID DESC";
		$conditionNumRows	= "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID HAVING BusinessName like '%".$BName."%' and ExpireDate between '$cdate' AND '$Expire' ORDER BY BusinessID DESC";
	} else if($BName != "") {
		//$conditionNumRows	= "tblB.BusinessID=tblS.BusinessID where tblB.BusinessName like '%".$BName."%' GROUP BY tblS.BusinessID ORDER BY tblS.ID DESC";
		$conditionNumRows	= "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID HAVING BusinessName like '%".$BName."%' ORDER BY BusinessID DESC";
	} else if($ExpDate != "") {
		//$conditionNumRows = "tblB.BusinessID=tblS.BusinessID where tblS.ExpireDate between '$cdate' and '$Expire' GROUP BY tblB.BusinessID ORDER BY tblS.ID DESC";
		//$conditionNumRows = "tblB.BusinessID=tblS.BusinessID GROUP BY tblS.BusinessID HAVING tblS.ExpireDate between '$cdate' and '$Expire' ORDER BY tblS.ID DESC";
		$conditionNumRows = "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID HAVING ExpireDate between '$cdate' AND '$Expire' ORDER BY BusinessID DESC";
	}
} else {
	  $conditionNumRows = "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID ORDER BY BusinessID DESC";
}

$condition	= $conditionNumRows." LIMIT $start , $limit";

$NO 		= mysqli_query($var,$conditionNumRows) or die($conditionNumRows);
$TOTAL 		= array();
	while($row=mysqli_fetch_array($NO)) {
		$TOTAL[]=$row;
	}
$total_pages = sizeof($TOTAL);

$RES = mysqli_query($var,$condition) or die($condition);
$results=array();
	while($rows=mysqli_fetch_array($RES)) {
		$results[]=$rows;
	}
//$total_pages = sizeof($NO);
//$results = $BusinessDetails->selectTableJoin($BTable,$SubTable,$join="",$condition,$cols);
//~	 $Arr = Array();
//~ $UsedID = Array();
//~ $i = 0;
//~ foreach( $results as $value ) {
	//~ if( !in_array($value['BusinessID'], $UsedID)) {
		//~ $Arr[$i]['BusinessID'] = $value['BusinessID'];
		//~ $Arr[$i]['BusinessName'] = $value['BusinessName'];
		//~ $Arr[$i]['Logo'] = $value['Logo'];
		//~ $Arr[$i]['PlanType'] = $value['PlanType'];
		//~ $Arr[$i]['status'] = $value['status'];
		//~ $Arr[$i]['ID'] = $value['ID'];
		//~ $Arr[$i]['PlanID'] = $value['PlanID'];
		//~ $Arr[$i]['ExpireDate'] = $value['ExpireDate'];
		//~ array_push($UsedID, $value['BusinessID']);
		//~ $i++;
	//~ }
//~ }
//$totalNumRows = $BusinessDetails->joinTotalNumRows($BTable,$SubTable,$join="",$conditionNumRows);
//$total_pages = $totalNumRows;

?>
<?php
	function foldersize($path) {
		$total_size = 0;
		$files = scandir($path);
		$cleanPath = rtrim($path, '/'). '/';
		foreach($files as $t) {
			if ($t<>"." && $t<>"..") {
				$currentFile = $cleanPath . $t;
				if (is_dir($currentFile)) {
					$size = foldersize($currentFile);
					$total_size += $size;
				}
				else {
					$size = filesize($currentFile);
					$total_size += $size;
				}
			}   
		}
		$total_size=$total_size/(1024*1024); // Bytes to Mega Bytes
		$total_size=round($total_size, 3); 
		return $total_size;
	}
?>
<style>
.row.search {
  margin-left: 35%;
  width: 85%;
  margin-top: -10px;
}
.span6.last.search {
  margin-left: 4%;
  width: 25%;
}
img#search-img {
  margin-left: 15px;
  cursor: pointer;
}
img#clear-img {
  margin-left: 0px;
  cursor: pointer;
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!--script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script-->
<script type="text/javascript">
	$(function() {
		$( "#searchdate" ).datepicker({dateFormat: "M d,yy"});
	});
	$(function() {
		$( "#search-img" ).click(function(){
			$("#search-form").submit();
		});
	});
	$(function() {
		$( "#clear-img" ).click(function(){
			$("#searchname").val('');
			$("#searchdate").val('');
			$("#search-form").submit();
		});
	});
</script>
<div class="form-container" id="top1">
	<div class="heading-container">
		<h1 class="heading empHead">List Businesses</h1>
		<div class="updatebusstatus">Business status updated successfully.</div>	 
	</div>		
	<div class="user-entry-block user-block-search">
		<div class="row search">
			<form name="search-form" id="search-form" method="post">
				<div class="span6 last search">				 
					<input type="text" placeholder="Business Name" class="searchclient text-input-field" id="searchname" name="searchname" <?php if(isset($_SESSION["searchname"])) echo "value='$_SESSION[searchname]'"; ?>/>
				</div>
				<div class="span6 last search">				 
					<input type="text" placeholder="Expire Within" class="searchclient text-input-field" id="searchdate" name="searchdate" <?php if(isset($_SESSION["searchdate"])) echo "value='$_SESSION[searchdate]'"; ?>/>
				</div>				 
				<img src="<?php echo $domain; ?>/images/search.png" id="search-img" name="search-img" title="Search"/>
				<img src="<?php echo $domain; ?>/images/clear.jpg" id="clear-img" name="clear-img" title="Clear"/>
				<input type="hidden" name="Submit" value="true" />
			</form>
		</div>
	<div class="tablebushead">
		<div class="tablebusinner">
		<div class="row sortingHead">	
			<div class="span3 busHeadSn srtHeadBorder">S. No.</div>
			<div class="span3 busHeadLogo srtHeadBorder">Logo</div>
			<div class="span3 busHeadName srtHeadBorder">Business Name</div>
			<div class="span3 busHeadEmail srtHeadBorder">E-Mail Address</div>
			<div class="span3 busHeadSize srtHeadBorder">Space</div>
			<div class="span3 busHeadSize srtHeadBorder">Plan</div>
			<div class="span3 busHeadSize srtHeadBorder">Expire On</div>
			<div class="span3 busHeadAction srtHeadBorder text-align">Action</div>
		</div>
	<?php 
		if(!empty($results)) { 
			$i = 0;
			$srno=$start+1;
			foreach($results as $bsdata) {
				if($i%2==0) {
					$bgdata = "bgnone";	
				} else { 
					$bgdata = "bgdata";
				}
				$empcond = "where BusinessID='".$bsdata["BusinessID"]."'";
				$empdata = $BusinessDetails->selectTableSingleRow($ETable,$empcond,$cols="*");
				$plancond = "where id='".$bsdata["PlanID"]."'";
				$plandata = $BusinessDetails->selectTableSingleRow($PTable,$plancond,$cols="*");
			?>	
				<div class="row  <?php echo $bgdata;?>" id="<?php echo $bsdata['BusinessID']; ?>">
					<div class="span3 busHeadSn srtcontent" style="text-align:center">
						<label id="" class="user-name"><?php echo $srno; ?> </label>						
					</div>				
					<div class="span3 busHeadLogo srtcontent">
						<a href="viewbusiness?id=<?php echo $bsdata['BusinessID']; ?> ">
							<img src="Business-Uploads/<?php if($bsdata['Logo']!='' && file_exists("Business-Uploads/$bsdata[BusinessID]/$bsdata[Logo]")){ echo $bsdata['BusinessID'].'/'.$bsdata['Logo']; } else { echo 'no-image.jpg'; } ?>" style="width:50px;height:50px;" alt="No Image" />
						</a>
					</div>
					<div class="span3 busHeadName srtcontent">
						<label id="" class="user-name">
							<a href="viewbusiness?id=<?php echo $bsdata['BusinessID']; ?> " title="View business detail" style="color:#11719F">
								<?php echo $bsdata['BusinessName']; ?></a>
						</label>
					</div>
					<div class="span3 busHeadEmail srtcontent">	
					  <label id="" class="user-name" style="text-transform: none"><?php echo $empdata['Emp_email']; ?></label>
					</div>
					<div class="span3 busHeadSize srtcontent">	
						<label id="" class="user-name">
							<?php 
								$dirpath="Business-Uploads/".$bsdata['BusinessID']; 
								if(file_exists($dirpath))
									echo foldersize($dirpath)." MB";
							?>
						</label>
					</div>
					<div class="span3 busHeadSize srtcontent">	
					  <label id="" class="user-name"><?php if($plandata["Type"] == "Free") echo "Free Trial - "; echo $plandata["title"];?></label>
					</div>
					<div class="span3 busHeadSize srtcontent">	
						<label id="" class="user-name">
							<?php 
								$period = (strtotime($bsdata["ExpireDate"])-strtotime(date('Y-m-d')))/24/3600;
								if($period >= 0)
									echo date('M j, Y',strtotime($bsdata["ExpireDate"]));
								else
									echo "<font color=red>".date('M j, Y',strtotime($bsdata["ExpireDate"]))."</font>";
							?>
						</label>
					</div>
					<div class="span3 busHeadAction srtcontent text-align">	
						<label id="" class="user-name">
							<a href="newbusiness?id=<?php echo $bsdata['BusinessID']; ?>">
								<img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit Business"/>
							</a>&nbsp;|&nbsp;
							<a href="javascript:;" rel="<?php echo $bsdata['BusinessID'].'|'.$bsdata['status']; ?>" class="clicktochangestatus">
								<img src="<?php echo $domain; ?>/images/<?php echo ($bsdata['status'] == 1) ? 'active.gif' : 'inactive.gif'; ?>" title="Click to <?php echo ($bsdata['status'] == 1) ? 'inactive' : 'active'; ?>"/>
							</a>
						</label>
					</div>
			    </div><!--End @row-->	
			<?php
				$i++; 
				$srno++;
			} ?>
		<?php
			echo $BusinessDetails->paginateShow($page,$total_pages,$limit,$adjacents,$reload); ?>
			<div id="statuResult"></div>
		<?php
		} else
			echo "<div class='not-found-data'>No business found. <a href='newbusiness'>Add New Business</a></div>"; ?>
	</div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
</div><!--End @container-->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
