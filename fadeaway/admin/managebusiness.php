<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php'); ?>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<?php
			include("../includes/dbFunctions.php");
			if($_SESSION['loginuser'] != "sitesuperadmin"){ ?>
				<script>
					$(function(){
						window.location.replace("dashboard?msg=noper");
					});
				</script>
			<?php
			die;
			}			
			if( !isset($_GET["page"])) {
				unset($_SESSION["searchname"]);
				unset($_SESSION["searchdate"]);
			}

			$var = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);

			$BusinessDetails = new dbFunctions();
			$BTable 	= "tbl_business tblB";
			$SubTable 	= "tbl_subscription_history tblS";
			$ETable 	= "tbl_employees tblE";
			$PTable 	= "tbl_master_plans tblP";
			$cols 		= "tblB.BusinessID,tblB.BusinessName,tblB.Logo,tblB.PlanType,tblB.status,tblS.ID,tblS.PlanID,tblS.ExpireDate";
			$adjacents 	= 3;
			$reload 	= "managebusiness";
			if(isset($_GET['page'])){
				$page=$_GET['page'];
			}
			else{
				$page="";
			}
			$limit = 10;                                  //how many items to show per page
			if($page)
				$start = ($page - 1) * $limit;          //first item to display on this page
			else
				$start = 0;

			if(isset($_POST["searchname"])) {
				$_SESSION["searchname"] = $_POST["searchname"];
			}
			if(isset($_POST["searchdate"])) {
				$_SESSION["searchdate"] = $_POST["searchdate"];
			}

			if((isset($_SESSION["searchname"]) && $_SESSION["searchname"]!="") || (isset($_SESSION["searchdate"]) && $_SESSION["searchdate"]!="")) {
				$BName = $_SESSION['searchname'];
				$ExpDate = $_SESSION['searchdate'];
				if(isset($_SESSION['searchdate']) && $_SESSION['searchdate']!= "") {
					$cdate = date('Y-m-d');
					$Expire = date("Y-m-d", strtotime($ExpDate . ' +1 day'));
				}
				if($BName!="" && $ExpDate!="") {
					$conditionNumRows	= "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID HAVING BusinessName like '%".$BName."%' and ExpireDate between '$cdate' AND '$Expire' ORDER BY BusinessID DESC";
				} else if($BName != "") {
					$conditionNumRows	= "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID HAVING BusinessName like '%".$BName."%' ORDER BY BusinessID DESC";
				} else if($ExpDate != "") {
					$conditionNumRows = "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID HAVING ExpireDate between '$cdate' AND '$Expire' ORDER BY BusinessID DESC";
				}
			} else {
				  $conditionNumRows = "SELECT * FROM (select $cols FROM tbl_business AS tblB JOIN tbl_subscription_history AS tblS ON tblB.BusinessID=tblS.BusinessID ORDER BY tblS.ID DESC) AS results GROUP BY BusinessID ORDER BY BusinessID DESC";
			}

			$condition	= $conditionNumRows." LIMIT $start , $limit";
			$NO = mysqli_query($var,$conditionNumRows) or die($conditionNumRows);
			$TOTAL = array();
			while($row=mysqli_fetch_array($NO)) {
				$TOTAL[]=$row;
			}
			$total_pages = sizeof($TOTAL);

			$RES = mysqli_query($var,$condition) or die($condition);
			$results=array();
			while($rows=mysqli_fetch_array($RES)) {
				$results[]=$rows;
			}			
			
			function foldersize($path) {
				$total_size = 0;
				$files = scandir($path);
				$cleanPath = rtrim($path, '/'). '/';
				foreach($files as $t) {
					if ($t<>"." && $t<>"..") {
						$currentFile = $cleanPath . $t;
						if (is_dir($currentFile)) {
							$size = foldersize($currentFile);
							$total_size += $size;
						}
						else {
							$size = filesize($currentFile);
							$total_size += $size;
						}
					}   
				}
				$total_size=$total_size/(1024*1024); // Bytes to Mega Bytes
				$total_size=round($total_size, 3); 
				return $total_size;
			}
		?>			
			<style>
				.row.search { margin-left: 35%; width: 85%; margin-top: -10px; }
				.span6.last.search { margin-left: 4%; width: 25%; }
				img#search-img { margin-left: 15px; cursor: pointer; }
				img#clear-img { margin-left: 0px; cursor: pointer; }
			</style>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
			<script type="text/javascript">
				$(function() {
					$( "#searchdate" ).datepicker({dateFormat: "M d,yy"});
				});
				$(function() {
					$( "#search-img" ).click(function(){
						$("#search-form").submit();
					});
				});
				$(function() {
					$( "#clear-img" ).click(function(){
						$("#searchname").val('');
						$("#searchdate").val('');
						$("#search-form").submit();
					});
				});
			</script>
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">All Businesses</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="updatebusstatus">
				<?php
					if(isset($_GET["msg"])) {
						$message = "";
						$color = "green";
						if($_GET["msg"]=="add")
						{
							$message = "Business details added successfully.";
							echo '<div class="u_mess" id="u_mess" style="display: block;">'.$message.'</div>';
						}
						else if($_GET["msg"]=="update")
						{
							$message = "Business details updated successfully.";	
							echo '<div class="u_mess" id="u_mess" style="display: block;">'.$message.'</div>';
						}
						if($_GET["msg"]=="failed"){
							$message = "Uploaded image is not valid.";	
							echo '<div class="errormessage" id="u_mess" style="display: block;">'.$message.'</div>';
						}
					}
				?>
				</div>
				<div class="card shadow mb-4 table-main-con">
					<form name="search-form" id="search-form" method="post">
						<div class="bussiness-searchblock">
							<div class="busniss-search searchbussiness">
								<img src="../img/searchblk.png">
								<input type="search" placeholder="Business Name" class="searchclient text-input-field" id="searchname" name="searchname" <?php if(isset($_SESSION["searchname"])) echo "value='$_SESSION[searchname]'"; ?>/>
							</div>
							<div class="busniss-search last">
								<input type="text" placeholder="Expire Within" class="searchclient text-input-field" id="searchdate" name="searchdate" <?php if(isset($_SESSION["searchdate"])) echo "value='$_SESSION[searchdate]'"; ?>/>
							</div>
							<div class="search-btn">
								<button id="search-img" name="search-img" title="Search"/>Search</button>
								<!-- img src="<?php echo $domain; ?>/images/search.png" id="search-img" name="search-img" title="Search"/ -->
								<img src="<?php echo $domain; ?>/images/clear.jpg" id="clear-img" name="clear-img" title="Clear"/>
								<input type="hidden" name="Submit" value="true" />
									<a href="newbusiness" class="addnew-button addbussinessbutton"><img src="../img/plus.png"> Add New</a>
							</div>
						</div>
					</form>			
					<div class="card-body">
						<?php 
						if(!empty($results)) {
						?>				
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>LOGO</th>
										<th>business Name</th>
										<th>Email</th>
										<th>Space</th>
										<th>Plan</th>
										<th>Expire On</th>
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$i = 0;
										$srno=$start+1;
										foreach($results as $bsdata) {
											if($i%2==0) {
												$bgdata = "bgnone";	
											} else {
												$bgdata = "bgdata";
											}
											$empcond = "where BusinessID='".$bsdata["BusinessID"]."'";
											$empdata = $BusinessDetails->selectTableSingleRow($ETable,$empcond,$cols="*");
											$plancond = "where id='".$bsdata["PlanID"]."'";
											$plandata = $BusinessDetails->selectTableSingleRow($PTable,$plancond,$cols="*");
										?>
											<tr>
											<!-- tr class="row <?php echo $bgdata;?>" id="<?php echo $bsdata['BusinessID']; ?>" -->
												<td><?php echo $srno; ?>.</td>
												<td><div class="logoblk">
													<a href="viewbusiness?id=<?php echo $bsdata['BusinessID']; ?> ">
														<img src="Business-Uploads/<?php if($bsdata['Logo']!='' && file_exists("Business-Uploads/$bsdata[BusinessID]/$bsdata[Logo]")){ echo $bsdata['BusinessID'].'/'.$bsdata['Logo']; } else { echo 'no-image.jpg'; } ?>" style="width:40px;height:40px;" alt="No Image" />
													</a></div>
												</td>
												<td><a href="viewbusiness?id=<?php echo $bsdata['BusinessID']; ?> " title="View business detail" style="color:#11719F"><?php echo $bsdata['BusinessName']; ?></a></td>
												<td style="text-transform: none"><?php echo $empdata['Emp_email']; ?></td>
												<td><?php 
													$dirpath="Business-Uploads/".$bsdata['BusinessID']; 
													if(file_exists($dirpath)){
														echo foldersize($dirpath)." MB";
													}
												?>
												</td>
												<td class="gry"><?php if($plandata["Type"] == "Free") echo "Free Trial - "; echo $plandata["title"];?></td>
												<td class="gry">
													<?php 
													$period = (strtotime($bsdata["ExpireDate"])-strtotime(date('Y-m-d')))/24/3600;
													if($period >= 0){
														echo date('M j, Y',strtotime($bsdata["ExpireDate"]));
													} else{
														echo "<font color=red>".date('M j, Y',strtotime($bsdata["ExpireDate"]))."</font>";
													}
													?>
												</td>
												<td>
													<a href="newbusiness?id=<?php echo $bsdata['BusinessID']; ?>">
														<img src="<?php echo $domain; ?>/img/editimg.png" title="Edit Business"/>
													</a>
													<a href="javascript:;" rel="<?php echo $bsdata['BusinessID'].'|'.$bsdata['status']; ?>" class="clicktochangestatus">
														<img src="<?php echo $domain; ?>/img/<?php echo ($bsdata['status'] == 1) ? 'tickimg.png' : 'tickimg.png'; ?>" title="Click to <?php echo ($bsdata['status'] == 1) ? 'inactive' : 'active'; ?>"/>
													</a>
												</td>
											</tr>
										<?php
											$i++;
											$srno++;
										}
									?>
								</tbody>
							</table>
						</div>
						<?php	
							echo $BusinessDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload); ?>
							<div id="statuResult"></div>
						<?php
						}
						else {
							echo "<div class='not-found-data'>No business found. <a href='newbusiness'>Add New Business</a></div>"; 
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
