<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	if($_SESSION['Usertype']!="Admin") {
		echo '<script>window.location.assign("clientreport.php")</script>';		
	}
?>	
	<script type="text/javascript">
		$(document).ready(function() {
			var options = {
				chart: {
	                renderTo: 'container',
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false,
	                animation:true
	            },
	            credits: {
					text: '',
					href: 'https://www.example.com'
				},
				click: function(e) {
					console.log(
						Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', e.xAxis[0].value), 
						e.yAxis[0].value						
					)
				},
	            title: {
	                text: 'All referral sources.'
	            },
	            tooltip: {
	                formatter: function() {
	                    return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
	                }
	            },
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    dataLabels: {
	                        enabled: true,
	                        color: '#000000',
	                        connectorColor: '#000000',
	                        formatter: function() {
								var num = this.percentage;
								var num1 =  num.toFixed(2);
	                            return '<b>'+ this.point.name +'</b>: '+ num1 +' %';
	                            //return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
	                        }
	                    }
	                }
	            },
				series: [{ 
					type: 'pie',
					name: 'chart name',
					point: {
						events: {
							click: function(e) {
								// alert(e.point.name);
								//this.slice();
								var clicked = this;
								setTimeout(function(){location.href = clicked.config[2];}, 100)
								e.preventDefault();
							}
						}
					},
					data:[
						['X-value1', 10,'https://yahoo.com'],
						['X-value2', 14,'https://google.com'],
					]
				}]
	        }	        
	        $.getJSON("data.php", function(json) {
				options.series[0].data = json;
	        	chart = new Highcharts.Chart(options);
	        });
		});   
	</script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<div class="form-container">
		<div class="heading-container">
			<h1 class="heading empHeadReport" style="width:62%">Referral Sources</h1>
		<?php
			if($_SESSION['Usertype']=="Admin") { ?>
				<div class="addNewReport" style="margin-right:15px"><a class="empLinks" href="revenuesreport.php" class="submit-btn">Revenue Report </a></div>
				<div class="addNewReport" style="width:16%"><a class="empLinks" href="clientreport.php" class="submit-btn">Client Report </a></div>
			<?php
			} ?>
		</div>	
		<div class="user-entry-block">
			<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
		</div><!--@user-entry-block-->
	</div><!--@form-container-->
</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
