<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include('admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	$location_con = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY LocationName";
	$locations = $empInfo->selectTableRows('tbl_manage_location', $location_con);
	$oldlocation = '';
	$arra = array();
	$uniqueids = array();
	foreach($locations AS $location){
		if($oldlocation != $location['LocationName']){
			$i = 0;
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
			$oldlocation = $location['LocationName'];
		}else
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
		$i++;
	}
	
	$result = mysqli_query($empInfo->con,"SELECT ReferralSource,COUNT(*) as totalnum  FROM tbl_clients Where ReferralSource !='' AND BusinessID=".$_SESSION["BusinessID"]." GROUP BY ReferralSource");
	$row = array();
	$rows = array();
	while($r = mysqli_fetch_array($result)) {
		$row[0] = $r[0];
		$row[1] = (int)$r[1];
		array_push($rows,$row);		
	}
	/*if($_SESSION['Usertype']!="Admin") {
		echo '<script>window.location.assign("clientreport")</script>';		
	}*/
?>	
<style>
	.referral-source-search input,select{width:auto !important;margin:-15px 15px 15px 0}
	.referral-source-search {border-bottom: 1px dotted #666666;}
	.referral-source-search div.search-clear {margin-top:-15px}
	.referral-source-search div.search-clear img{cursor:pointer}
	.search-tabular{border-top: 1px dotted #666666;padding-top:15px}
	.search-tabular table{width:100%}
	.search-tabular table th, td{border: 0px solid #91bde1;font-weight:normal;font-size:13px;border: 1px solid #fff}
	.search-tabular table th{color:#000;}
	.search-tabular table td{color:#666666;padding-left:10px}
	.search-tabular table tr.odd{background:rgb(220,220,220);}
	.search-tabular table tr{height:43px}
	.search-tabular table tbody tr:hover {background: #b6c6d7 none repeat scroll 0 0;}
	.search-tabular table thead{background: rgb(220,220,220);}
	.search-tabular table thead tr th{font-weight:bold;font-size:14px}
</style>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<script type="text/javascript">
				$(document).ready(function() {
					var options = {
						chart: {
							renderTo: 'container',
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false,
							animation:true
						},
						credits: {
							text: '',
							href: 'https://www.example.com'
						},
						click: function(e) {
							console.log(
								Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', e.xAxis[0].value), 
								e.yAxis[0].value						
							)
						},
						title: {
							text: 'All referral sources.'
						},
						tooltip: {
							formatter: function() {
								return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
							}
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: true,
									color: '#000000',
									connectorColor: '#000000',
									formatter: function() {
										var num = this.percentage;
										var num1 =  num.toFixed(2);
										return '<b>'+ this.point.name +'</b>: '+ num1 +' %';
										//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
									}
								}
							}
						},
						series: [{ 
							type: 'pie',
							name: 'chart name',
							point: {
								events: {
									click: function(e) {
										// alert(e.point.name);
										//this.slice();
										var clicked = this;
										setTimeout(function(){location.href = clicked.config[2];}, 100)
										e.preventDefault();
									}
								}
							},
							data:[
								['X-value1', 10,'https://yahoo.com'],
								['X-value2', 14,'https://google.com'],
							]
						}]
					}	        
					$.getJSON("data.php", function(json) {
						options.series[0].data = json;
						chart = new Highcharts.Chart(options);
					});
					
					$(".clicktosearchreferral").click(function(){
						$.getJSON("data.php?from="+$("#referral-from").val()+"&to="+$("#referral-to").val()+"&loc="+$("#referral-loc").val(), function(json) {
							options.series[0].data = json;
							chart = new Highcharts.Chart(options);
						});
						$.ajax({
							url:"data.php?from="+$("#referral-from").val()+"&to="+$("#referral-to").val()+"&loc="+$("#referral-loc").val(),
							type:"GET",
							//data:{date_from:$("#referral-from").val(), date_to:$("#referral-to").val(), ref_loc:$("#referral-loc").val()},
							data:{get_tabular:'tabular'},
							success:function(res){
								$(".changeonsearch").html(res);
							}
						});
					});

					$(".clicktoclearreferral").click(function(){
						location.reload(); 
					});
					$("#referral-from").datepicker({
						dateFormat:'yy-mm-dd'
					});
					$("#referral-to").datepicker({
						dateFormat:'yy-mm-dd',
						beforeShow: function() {
							$(this).datepicker('option', 'minDate', $('#referral-from').val());
							if ($('#referral-from').val() === '') $(this).datepicker('option', 'minDate', 0);                             
						 }
					});
					$('.close-btn-pricing').click(function() {
					 $('.overlay-bg-pricing').hide();	
					});
				});   
				function tattoo_artist()
				{
				  $.ajax({
							url:"get_artist.php?from="+$("#referral-from").val()+"&to="+$("#referral-to").val()+"&loc="+$("#referral-loc").val(),
							type:"GET",
							//data:{date_from:$("#referral-from").val(), date_to:$("#referral-to").val(), ref_loc:$("#referral-loc").val()},
							data:{get_tabular:'tabular'},
							success:function(res){
								$('.fortableartist').html(res);
								$('.overlay-bg-pricing').show();
							}
						});
				}
			</script>
			<script src="https://code.highcharts.com/highcharts.js"></script>
			<script src="https://code.highcharts.com/modules/exporting.js"></script>
			<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/pricing.css" />			
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Referral Sources</h1>
				</div>
								
				<div class="card shadow mb-4 table-main-con marketing-report">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<?php
							if($_SESSION['Usertype']=="Admin") { ?>
								<a class="empLinks" href="revenuesreport" class="submit-btn"><button class="addnewbtn">Revenue Report </button></a>
								<a class="empLinks" href="clientreport" class="submit-btn"><button class="addnewbtn">Client Report </button></a>
							<?php
							} ?>
						</div>
					</div>
<div class="bussiness-searchblock">
              <div class="busniss-search">
                <input type="text" placeholder="Referral from date" class="text-input-field" id="referral-from" />
              </div>
              <div class="busniss-search ">
                <input type="text" placeholder="Referral to date" class="text-input-field" id="referral-to" />
              </div>
              <div class="busniss-search last">
                			<select class="select-option" id="referral-loc">
								<option value=''>Select business location</option>
								<?php foreach($arra AS $key=>$arr){?>
									<option value="<?php echo implode(",", $arr); ?>"><?php echo ucfirst($key); ?></option>
								<?php }?>
							</select>
              </div>
              <div class="search-btn">
                <button type="button" class="clicktosearchreferral" title="Search">Search</button>
              </div>
</div>										
				
					
					
					<div class="card-body">
						<div class="marketingOuterBlk">
							<div class="marketingTable">
								<div class="table-responsive">
									<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>S. N.</th>
												<th>Referral Source</th>
												<th>Client Count</th>
											</tr>
										</thead>
										<tbody class="changeonsearch">
											<?php 
											if(!empty($rows)){
												$total = 0; $lastkey = 0; 
												foreach($rows AS $key => $ref_data){
													$total += $ref_data[1]; ?>
													<tr class=<?php echo (($key%2)==0) ? 'even' : 'odd'; ?>>
														<td class="text-align">
															<?php echo ++$key; ?>
														</td>
														<td>
															<?php if($ref_data[0]=='Tattoo Artist') { echo '<span style="cursor:pointer;color:#11719f;" onclick="tattoo_artist(); ">'.$ref_data[0].'</span>'; } else { echo $ref_data[0]; }  ?>
														</td>
														<td class="text-align">
															<?php echo $ref_data[1]; ?>
														</td>
													</tr>
													<?php 
													$lastkey = $key;
												} ?>
												<tr class=<?php echo (($lastkey%2)==0) ? 'even' : 'odd'; ?>>
													<td colspan=2 style="text-align:right"><b>Total</b></td>
													<td class="text-align">
														<?php echo $total; ?>
													</td>
												</tr>
											<?php
											} else { ?>
												<tr style='text-align:center;color:#c0c0c0'>
													<td colspan=3>No record found!!</td>
												</tr>
											<?php
											} ?>
										</tbody>
									</table>
								</div>
							</div>
							<div id="container" class="marketinggraph"></div>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
