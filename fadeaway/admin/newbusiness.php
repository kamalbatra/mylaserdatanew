<?php
session_start();
$doaminPath = $_SERVER['DOMAINPATH'];
$domain = $_SERVER['DOMAIN'];
include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
include("../includes/dbFunctions.php");
if($_SESSION['loginuser'] != "sitesuperadmin"){
	?>
	<script>
		$(function(){
			window.location.replace("dashboard?msg=noper");
		});
	</script>
	<?php
	die;
}
$newbusiness = new dbFunctions();

$editid=isset($_GET["id"])?$_GET["id"]:"";
$status="new";

$tbl_plans="tbl_master_plans";
$condition = "where title IN ('Free Trial', 'No Trial') ORDER BY id  desc ";
$cols="*";
$PlanData = $newbusiness->selectTableRows($tbl_plans,$condition);

$tbl_services = "tbl_master_services";
$condition = "where status = 1";
$Services = $newbusiness->selectTableRows($tbl_services,$condition);

$trialPlanCond = "where title NOT IN ('Free Trial', 'No Trial') and Type='Free' ORDER BY id  asc ";
$cols="*";
$trialPlanData = $newbusiness->selectTableRows($tbl_plans,$trialPlanCond);
$servicearr = array();
if($editid){
	$table="tbl_business";
	$condition="where BusinessID=".$editid;
	$bdetails=$newbusiness->selectTableSingleRow($table,$condition,$cols="*");
	$table2="tbl_employees";
	$empdetails=$newbusiness->selectTableSingleRow($table2,$condition,$cols="*");
	$sub_plan_condition = "where BusinessID=".$editid." Order by ID DESC";
	$sub_plan_id=$newbusiness->selectTableSingleRow("tbl_subscription_history",$sub_plan_condition,$cols="PlanID");
	$status="edit";
	
	$servicearr = explode(',',$bdetails["serviceId"]);

}

?>	
<script>
$(document).ready(function(){
	$("select[name='select-plan']").change(function(){
		if($(this).find("option:selected").text() == "Free Trial")
			$(".showontrial").show();
		else
			$(".showontrial").hide();
	});
	$("#delete-image").click(function(){
		var checkstr =  confirm('Are you sure you want to remove logo?');
		if (checkstr == true){
			var editid = 'id='+'<?php echo $editid; ?>'+'&imgname='+'<?php echo isset($bdetails['Logo']) ? $bdetails['Logo'] : ''; ?>';
			//alert(editid);
			$.ajax({
				type: "POST",
				url: "delete-image.php",
				data: editid,
				cache: false,
				success: function(result){							
					if(result=="True"){
						//$("#image-div").hide();
						$("#logofile").show();
						$("#image-div").html('<input name="logofile" id="logofile" type="file" style="border:none;" />');
					}
				}
			});		
		}
		else{
			return false;
		}
	});
});
</script>
<style>
.span6 {
	width:40%; 
	font-family: Verdana,Geneva,Tahoma,sans-serif;
	font-size: 13px;
	font-weight: 500;
	color: #666666;
}
</style>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0"><?php if($status=="edit"){ echo "Edit Business"; } else{ echo "New Business Entry"; } ?></h1>

						<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
					</div>
					
					<div class="updatebusstatus">
							<?php
							if(isset($_GET["msg"])) {
								$message = "";
								if($_GET["msg"]=="add") {								
									$message = "Business details added successfully.";
									echo "<div class='updatebusstatus'><div class='u_mess' id='u_mess' style='display: block;'>".$message."</div></div>"; 
								}	
								else if($_GET["msg"]=="update") {
									$message = "Business details updated successfully.";	
									echo "<div class='updatebusstatus'><div class='u_mess' id='u_mess' style='display: block;'>".$message."</div></div>"; 
								}	
								if($_GET["msg"]=="failed"){
									$message = "Uploaded image is not valid.";	
									echo "<div class='updatebusstatus'><div class='errormessage' id='u_mess'>"."Unable to submit form please try again later!"."</div></div>";
								}
							}
							?>
					</div>
					<div class="card shadow mb-4 table-main-con">
					<form action="addbusiness" method="post" id="add-business" name="add-business" enctype="multipart/form-data">
						<?php
							if(isset($_GET["id"]) && $_GET["id"] != "")
								echo "<input type='hidden' name='updatedId' value=$_GET[id] />";
						?>
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="logo" class="user-name">Business Logo:</label>
													<?php if($status=="edit" && $bdetails['Logo'] != "" && file_exists("Business-Uploads/$bdetails[BusinessID]/$bdetails[Logo]")) { ?>
														<div id="image-div">
															<img src="Business-Uploads/<?php echo $editid; ?>/<?php echo $bdetails['Logo']; ?>" style="height:100px;width:400px;" />
															<input type="button" value="Remove Logo" name="delete-image" id="delete-image" class="remove-logo"/>
														</div>
													<?php } else{ ?>
														<input name="logofile" id="logofile" type="file" class="fileclass" /><br/>
														<span style="font-size:11px;font-family: Verdana,Geneva,Tahoma,sans-serif">(Best Size 950px X 222px)</span>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<div class="radioblocksBtns">
															<label class="user-name">Services:</label>
															<div class="radioBtn ckeckboxres">
															<?php
																foreach ( $Services as $key=>$Service ) { ?>
																	<div class="radioBtn ckeckboxres">
																		<input class="service-check" type="checkbox" name="service[]" id="service<?php echo $Service["id"]; ?>" value="<?php echo $Service["id"]?>" <?php if(in_array($Service['id'],$servicearr)) echo 'checked'; ?>>
																		<label for="service<?php echo $Service["id"]; ?>" class="user-name service-check-label"><?php echo $Service["name"]; ?></label>
																	</div>
															<?php } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php 
										if(isset($sub_plan_id["PlanID"])) 
											$trial_plan_id["PlanID"] = ($sub_plan_id["PlanID"] == 2) ? $sub_plan_id["PlanID"] : 1; 
									?>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="username" class="user-name">Select Trial:</label>
													<select class="select-option" name="select-plan" id="select-plan" >
														<?php
															foreach($PlanData as $plans){  ?> 
																<option value="<?php echo $plans['id']; ?>" <?php if(isset($trial_plan_id["PlanID"]) && $trial_plan_id["PlanID"]==$plans['id']) echo "selected";?> ><?php echo $plans["title"]; ?></option>
														<?php } ?>
													</select>
												</div>
											</div>
										</div>
										<div class="half showontrial" style="display:<?php echo (isset($trial_plan_id["PlanID"]) && $trial_plan_id["PlanID"] != 2) ? '' : 'none'; ?>">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="username" class="user-name">Free Trial Period:</label>
													<select class="select-option" name="trialplan" id="trialplan" >
														<?php
															foreach($trialPlanData as $trialPlans){  ?> 
																<option value="<?php echo $trialPlans['id']; ?>" <?php if(isset($sub_plan_id["PlanID"]) && $sub_plan_id["PlanID"]==$trialPlans['id']) echo "selected";?> ><?php echo $trialPlans["title"]; ?></option>
														<?php } ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="bname" class="user-name">Business Name:</label>
													<input class="text-input-field" name="bname" id="bname" type="text" value="<?php echo (isset($bdetails['BusinessName'])) ? $bdetails['BusinessName'] : '';?>" />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="username" class="user-name">Username:</label>
													<input class="text-input-field" name="username" id="username" type="text" value="<?php echo (isset($empdetails['Username'])) ? $empdetails['Username'] : '';?>" <?php echo ($status == 'edit') ? 'disabled' : '' ?> />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="First_Name" class="user-name">First Name:</label>
													<input class="text-input-field" name="First_Name" id="First_Name" type="text" value="<?php echo (isset($empdetails['First_Name'])) ? $empdetails['First_Name'] : '';?>" />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Last_Name" class="user-name">Last Name:</label>
													<input class="text-input-field" name="Last_Name" id="Last_Name" type="text" value="<?php echo (isset($empdetails['Last_Name'])) ? $empdetails['Last_Name'] : ''; ?>" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Address1" class="user-name">E-Mail:</label>
													<input class="text-input-field" name="email" id="email" type="text" value="<?php echo (isset($empdetails['Emp_email'])) ? $empdetails['Emp_email'] : '';?>" />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Address2" class="user-name">Contact No:</label>
													<input class="text-input-field" name="contact" id="contact" type="text" maxlength="10" value="<?php echo (isset($bdetails['Contact'])) ? $bdetails['Contact'] : '';?>" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Address1" class="user-name">Address:</label>
													<textarea rows="5" class="text-area-field" name="Address" id="Address"><?php echo (isset($bdetails['Address'])) ? $bdetails['Address'] : '';?></textarea>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Address2" class="user-name">City:</label>
													<input class="text-input-field" name="City" id="City" type="text" value="<?php echo (isset($bdetails['City'])) ? $bdetails['City'] : '';?>" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Address1" class="user-name">State:</label>
													<input class="text-input-field" name="State" id="State" type="text" value="<?php echo (isset($bdetails['State'])) ? $bdetails['State'] : '';?>"/>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Address2" class="user-name">Country:</label>
													<input class="text-input-field" name="Country" id="Country" type="text" value="<?php echo (isset($bdetails['Country'])) ? $bdetails['Country'] : '';?>" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label for="Address1" class="user-name">Sendy Brand ID:</label>
													<input class="text-input-field" name="sendy_brand_id" id="sendy_brand_id" type="text" value="<?php echo (isset($bdetails['sendy_brand_id'])) ? $bdetails['sendy_brand_id'] : '';?>"/>
												</div>
											</div>
										</div>
									</div>
									<input name="employee_updated_id" type="hidden" value="<?php echo isset($empdetails['Emp_ID']) ? $empdetails['Emp_ID'] : 0;?>" />
									<div class="form-row-ld">
										<div class="backNextbtn">
											<?php if($status=="edit") { ?>
												<button type="submit" id="submit-btn" value="Update" class="submit-btn nextbtn" style="float:left;">Update</button>
											<?php } else { ?>
												<button type="submit" id="submit-btn" value="Submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
											<?php } ?>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<script>
		$(document).ready(function(){
			//$("#submit-btn").click(function(){
			$("#add-business").validate({
				errorClass: 'errorblocks',
				errorElement: 'div',
				rules:{
					bname:"required",
					'service[]':"required",
					First_Name:"required",
					username:{
						required:true,
						remote:{
							url:"checkemail.php",
							type:"post",
							data: { 
								username: function(){ 
									return $("#username").val(); 
								},
								emp_id: $("input[name='employee_updated_id']").val()
							}		
						}
					},
					contact:{
						number:true,
						maxlength: 10
					},
					sendy_brand_id:{
						required:true,
						number:true,
					},
					email:{
						required:true,
						email:true,
						remote:{
							url:"checkemail.php",
							type:"post",
							data: { 
								email: function(){ 
									return $("#email").val(); 
								},
								emp_id: $("input[name='employee_updated_id']").val()
							}		
						}
					}
				},
				messages:{
					bname:"Please enter name for business.",
					'service[]':"Please select atleast one service.",
					First_Name:"Please enter first name.",
					username:{
						required:"Please enter username.",
						remote:"Username is already exists."
					},
					contact:"Please enter a valid number.",
					sendy_brand_id:{
						required:"Please enter sendy brand id.",
						number:"Please enter valid sendy brand id."
					},
					email:{
						required:"Please enter email address.",
						email:"Please enter valid email address.",
						remote:"Email address is already registred."
					}
				}
			});
		//});
		});
	</script>
	<?php
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
