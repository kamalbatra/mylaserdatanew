<?php
	//session_start();
	include("../includes/dbFunctions.php");
	$consentForms = new dbFunctions();
	$table = "tbl_consent_form as c";
	if($_SESSION['Usertype']!="Admin") {
		$condition = " left join tbl_clients as cl on c.ClientID=cl.ClientID where c.TechnicianReview ='' and c.location ='".$_SESSION['Location']."' AND c.BusinessID = $_SESSION[BusinessID] and cl.ClientID!='NULL'";
	} else { 
		$condition = " left join tbl_clients as cl on c.ClientID=cl.ClientID where c.TechnicianReview ='' AND c.BusinessID = $_SESSION[BusinessID] and cl.ClientID!='NULL'";
	}
	$cols=" c.* ";
	$sizedata= $consentForms->selectTableRows($table,$condition,$cols);
	$countPendingReview =count($sizedata);
	//$_SESSION['pendingReview'] = $countPendingReview;
	/**** Review for dirctor ********/
	$conditionDir = " left join tbl_clients as cl on c.ClientID=cl.ClientID where c.TechnicianReview !='' AND c.MedicalDirectorNotes='' AND c.BusinessID = $_SESSION[BusinessID] and cl.ClientID!='NULL' ";
	$sizedataDir= $consentForms->selectTableRows($table,$conditionDir,$cols);
		
?>
<?php
	if(isset($_POST['review']) && $_POST['review']=='header') { ?>
		<a href='consentforms' style='color:red;padding-right:20px;'>
			<span style="background-color: #FF0000; border-radius: 5px; color: #FFFFFF;padding: 6px 15px;" title="Pending consent reviews for technician">
			<?php echo count($sizedata);?></span>
		</a>
		<a href='consentforms' style='color:red;padding-right:20px;'>
			<span style="background-color: #FA660F; border-radius: 5px; color: #FFFFFF;padding: 6px 15px;" title="Pending consent reviews for director">
			<?php echo count($sizedataDir);?></span>
		</a>
<?php
	} ?>
<?php
	if(isset($_POST['review']) && $_POST['review']=='dashbord') { ?>
		<div class="close-btn"></div>
		<h1>Pending reviews</h1>									
		<div class="penContainer"><a class="close-link" href='consentforms'><span class="text">Technician:</span><span  class="digit"><?php echo count($sizedata);?></span></a></div>
		<div class="penContainer"><a class="close-link" href='consentforms'><span class="text">Director:</span><span class="digit" style="background-color:#FA660F;"><?php echo count($sizedataDir);?></span></a></div>
<?php 
	} ?>	
	<script>
		$('.close-btn').click(function() {
			$('.overlay-bg').hide(); // hide the overlay
		});
		$('.close-link').click(function() {
			$('.overlay-bg').hide(); // hide the overlay
			$(location).attr('href', 'consentforms');
		});
	</script>
