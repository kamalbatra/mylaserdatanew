<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	
	if( !in_array(3,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php 
	}
	$Services = implode(",",$_SESSION["services"]);
	$table_service = "tbl_master_services";
	$SCond = "where status=1 AND id in($Services)";
	$SCols = "id,name";
	$SData = $empInfo->selectTableRows($table_service,$SCond,$SCols);	

	$table_sizes = "tbl_master_sizes";
	$SizeCond = "where status=1 AND serviceId=2";
	$SizeCols = "id,size";
	$SizeData = $empInfo->selectTableRows($table_sizes,$SizeCond,$SizeCols);	
?>	
	<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->

<style>
	.span6.input { margin-left: 5%; }
	.span6.select { margin-left: 5%; }
	.span6 { margin-right: 0; }
</style>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHeadReport">Pricing Calculator</h1>
		<!--div class="addNewReport"><a class="empLinks" href="revenuesreport.php"class="submit-btn"> Revenues</a></div>		
		<div class="addNewReport"><a class="empLinks" href="marketing.php" class="submit-btn"> Marketing</a></div-->
	</div>	<!--heading-container-->
	<div class="user-entry-block user-block-price">
		<div class="row">		
			<form action="" method="post" name="pricingform" id="pricingform">
				<div class="span6 last">
					<label id="" class="user-name">Select Service:</label>
					<select class="select-option" name="Service" id="Service">						
					<?php
						if($SData !=NULL) {
							foreach($SData as $services) {  ?>
								<option value="<?php echo $services['id']?>" ><?php echo $services['name']?></option>						
							<?php
							}
						}		
					?>
					</select>
				</div><!--end span6-->	
				<div class="span6 input" id="input-size">
					<label id="" class="user-name">Please input size:</label>
					<input type="text" name="pricing_amt" class="text-input-field" id="pricing_amt" autocomplete="off"/>
				</div>						
				<div class="span6 select" id="select-size">
					<label id="" class="user-name">&nbsp;</label>
					<select class="select-option" name="Size" id="Size">						
						<option value="0">-Select Size-</option>
					<?php
						if($SizeData !=NULL) {
							foreach($SizeData as $sizes) { ?>
								<option value="<?php echo $sizes['size']?>" ><?php echo $sizes['size']?></option>						
							<?php
							}
						}		
					?>
					</select>
				</div>						
				<input type="hidden" name="location" class="text-input-field" id="location" value="<?=$_SESSION['Location']?>"/>
				<!--div class="span6 last">
					<label id="" class="user-name" style="margin-bottom:13px;"></label>
					<input type="submit" name="submit" class="submit-btn" value="Submit">
				</div-->    
			</form>				
		</div><!-- @End  row -->
		<div class="tablebushead">
		<div class="tablebusinner">				
		<div id="prices" class="row price_back1" style="width:100%;display:none;">
			
		</div>
		</div>
		</div>
	</div><!--@user-entry-block-->
</div><!--@form-container-->
</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
