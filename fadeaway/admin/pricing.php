<?php
	include('admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	
	if( !in_array(3,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php 
	}
	$Services = implode(",",$_SESSION["services"]);
	$table_service = "tbl_master_services";
	$SCond = "where status=1 AND id in($Services)";
	$SCols = "id,name";
	$SData = $empInfo->selectTableRows($table_service,$SCond,$SCols);	

	$table_sizes = "tbl_master_sizes";
	$SizeCond = "where status=1 AND serviceId=2";
	$SizeCols = "id,size";
	$SizeData = $empInfo->selectTableRows($table_sizes,$SizeCond,$SizeCols);	
?>	
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>

		
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Pricing Calculator</h1>
				</div>
								
				<div class="card shadow mb-4 table-main-con">
					<div class="formcontentblock-ld">
						<form action="" method="post" name="pricingform" id="pricingform">
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="" class="user-name">Select Service:</label>
											<select class="select-option" name="Service" id="Service">						
											<?php
												if($SData !=NULL) {
													foreach($SData as $services) {  ?>
														<option value="<?php echo $services['id']?>" ><?php echo $services['name']?></option>						
													<?php
													}
												}		
											?>
											</select>
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld" id="input-size">
											<label id="" class="user-name">Please Input Size:</label>
											<input type="text" name="pricing_amt" class="text-input-field" id="pricing_amt" autocomplete="off"/>
										</div>
										<div class="inputblock-ld span6 select" id="select-size">
											<label id="" class="user-name">Select Size</label>
											<select class="select-option" name="Size" id="Size">						
												<option value="0">-Select Size-</option>
												<?php
												if($SizeData !=NULL) {
													foreach($SizeData as $sizes) { ?>
														<option value="<?php echo $sizes['size']?>" ><?php echo $sizes['size']?></option>						
													<?php
													}
												}		
											?>
											</select>
										</div>	
									</div>
								</div>
							</div>
							<input type="hidden" name="location" class="text-input-field" id="location" value="<?=$_SESSION['Location']?>"/>
						</form>
						<div class="form-row-ld">
							<div class="tablebushead">
								<div class="tablebusinner">				
									<div id="prices" class="row price_back1" style="display:none;"></div>
								</div>
							</div>					
						</div>					
					
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
