<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");	
	$pricingDetails	= new dbFunctions();
	if( !in_array(9,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php 
	}
	/*** fetch Add Manage location**/
	$tbl_manage_location = "tbl_manage_location";
	$tbl_manage_device = "tbl_devicename";
	$DCond = "where serviceId=2 AND BusinessID=".$_SESSION["BusinessID"];
	$Dcols = "deviceId";
	$DData = $pricingDetails->selectTableRows($tbl_manage_device,$DCond,$Dcols);
	$DIDs = "";
	if( $DData != "" ) {
		for( $i=0;$i<count($DData);$i++ ) {
			$DIDs.= $DData[$i]["deviceId"];
			if( $i < count($DData)-1 )
				$DIDs.= ",";
		} 
	}
	$ManagesData = array();
	if( $DIDs != '' ) {
		$condition = "where BusinessID=".$_SESSION["BusinessID"]." AND deviceId in(".$DIDs.") ORDER BY deviceId";
		$cols="*";
		$ManagesData = $pricingDetails->selectTableRows($tbl_manage_location,$condition);
	}
	/*** fetch All device Name**/
?>
<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/pricing.css" />
<script type="text/javascript">
	$(document).ready(function() {
		function loading_show() {
			$('#loading').html("<img src='<?php echo $domain; ?>/images/loading.gif'/>").fadeIn('fast');
		}
		function loading_hide() {
			$('#loading').fadeOut('fast');
		}                
		function loadData(page) {					
			loading_show();                    
			$.ajax ({
				type: "POST",
				url: "ajax_pricingdetailshair-1.php",
				data: "page="+page +'&Location='+$('#Location').val(),
				success: function(msg) {
					$("#container").ajaxComplete(function(event, request, settings) {
						loading_hide();
						$("#container1").hide()
						$("#container").html(msg).show();
					});
				}
			});
		}
		loadData(1);  // For first time page load default results
		$('#container .pagination li.active').live('click',function() {
			var page = $(this).attr('p');
			loadData(page);                    
		});  
		$('#Location').change(function() {
			loadData(1);                    
		});                          
		$('#go_btn').live('click',function() {
			var page = parseInt($('.goto').val());
			var no_of_pages = parseInt($('.total').attr('a'));
			if(page != 0 && page <= no_of_pages) {
				loadData(page);
			} else 	{
				alert('Enter a PAGE between 1 and '+no_of_pages);
				$('.goto').val("").focus();
				return false;
			}             
		});
		$('.pricingEdit').live('click',function() {
			var page = $(this).attr('page');
			var pid = $(this).attr('id');
			var size = $(this).attr('size');
			var str ='pid='+pid+'&Size='+size+'&page='+page;
			$.ajax ({
				type: "POST",
				url: "ajax_pricingedit.php",
				data: str,
				cache: false,
				success: function(result) {
					//alert(result);
					$('.overlay-bg-pricing').html(result).show(); //display your popup
					//$("#u_mess").html(result);
				}
			});
		});//. pricingEdit                 
		$('#updateBtn').live('click',function() {
			var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;				
			var arr = $('.pricingclass').map(function(i, e) {
				if(!numberRegex.test(e.value)) {
					//return e.value; 
					//alert("ee:-"+e.id);				 
					$("#"+e.id).css("border","1px solid red");	
					$("#"+e.id).focus();
					$("#"+e.id).addClass('placeholderInt');
					$("#"+e.id).attr("placeholder", "Enter numeric value only");	
					$("#pricingMsg").html("Please enter numeric value!").show();		  
					return e.id; 
				} else {
					$("#"+e.id).css("border","1px solid #ccc");
					//$("#pricingMsg").html("").hide();	
				}			 
			}).toArray();
			if(arr.length ==0) {	
				var str1 = $("#pricingform").serialize();
				$.ajax ({			
					type: "GET",
					url: "ajax_pricingUpdate.php",
					data: str1,
					success: function(msg1) {	
						$("#pricingMsg").hide();							
						var page = $('#page').val();							    
						$("#pricingMsgUpdate").css("color","green");
						$("#pricingMsgUpdateLoding").show();							      
						$("#pricingMsgUpdate").html("Record Updated successfully.");
						setTimeout(function() {
							loadData(page);
							$('.overlay-bg-pricing').hide();
							return false;
						}, 2000);
					}
				});
			}					
		});
		//End #updateBtn
		$('.close-btn-pricing').live('click',function() {
			$('.overlay-bg-pricing').hide();
		});
		
		$('#AddPrice').click(function(){
			var page = $(this).attr('page');
			var Location = $('#Location').val();
			var str ='Location='+Location+'&service=Hair&page='+page;
			$.ajax ({
				type: "POST",
				url: "ajax_addprice-1.php",
				data: str,
				cache: false,
				success: function(result) {
					//alert(result);
					$('.overlay-bg-pricing').html(result).show(); //display your popup
					//$("#u_mess").html(result);
				}
			});
		});
		//. pricingAdd               
		$('#addBtn').live('click',function() {
			var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;				
			var Arr = $('.pricingclass').map(function(i, e) {
				if(!numberRegex.test(e.value)) {
					//return e.value; 
					//alert("ee:-"+e.id);				 
					$("#"+e.id).css("border","1px solid red");	
					$("#"+e.id).focus();
					$("#"+e.id).addClass('placeholderInt');
					$("#"+e.id).attr("placeholder", "Enter numeric value only");	
					$("#pricingMsg").html("Please enter numeric value!").show();		  
					return e.id; 
				} else {
					$("#"+e.id).css("border","1px solid #ccc");
					//$("#pricingMsg").html("").hide();	
				}			 
			}).toArray();
			if(Arr.length ==0) {	
				var checkData = $("#addpricingform").serialize();
				$.ajax ({			
					type: "POST",
					url: "ajax_pricingcheck.php",
					data: checkData,
					success: function(res) {	
						if( res=="area" ) {
							$("#pricingMsg").html("Please select size and area.").show();
						} else if( res=="exists" ) {
							var addData = $("#addpricingform").serialize();
							$.ajax ({			
								type: "POST",
								url: "ajax_pricingUpdate.php",
								data: addData,
								success: function(res) {	
									$("#pricingMsg").hide();							
									var page = $('#page').val();							    
									$("#pricingMsgUpdate").css("color","green");
									$("#pricingMsgUpdateLoding").show();							      
									$("#pricingMsgUpdate").html("Record Updated successfully.");
									setTimeout(function() {
										loadData(1);
										$('.overlay-bg-pricing').hide();
										return false;
									}, 2000);
								}
							});
						} else if( res=="notexists" ) {
							var addData = $("#addpricingform").serialize();
							$.ajax ({			
								type: "POST",
								url: "ajax_pricingadd.php",
								data: addData,
								success: function(res) {	
									if( res=="Yes" ) {
										$("#pricingMsg").hide();							
										var page = $('#page').val();							    
										$("#pricingMsgUpdate").css("color","green");
										$("#pricingMsgUpdateLoding").show();							      
										$("#pricingMsgUpdate").html("Record Added successfully.");
										setTimeout(function() {
											loadData(1);
											$('.overlay-bg-pricing').hide();
											return false;
										}, 2000);
									}
									else {
										$("#pricingMsg").html(res).show();
									}
								}
							});
						}
					}
				});
			}					
		});
		//End #addBtn
	});
</script>
<style>
	.addNew { width: 17%; }
	.empHead { width: 66%; }
	.srtHead { width: 15%; }
	.heading-container h1.heading { width: 53%; font-size: 23px; }
</style>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHead">Manage Pricing for Hair Removal</h1>
		<?php if(in_array("1",$_SESSION["services"])) { ?> <div class="addNew"><a class="empLinks" href="pricingdetails" class="submit-btn">Tattoo Removal</a></div> <?php } ?>
		<?php if($ManagesData !=NULL){ ?><div class="addNew" style="width:15%;"><a class="empLinks" href="import?service=Hair" class="submit-btn">Import csv </a></div>
		<div class="addNew" style="width:15%;"><a class="empLinks" id="AddPrice" class="submit-btn" style="cursor:pointer;">Add Price</a></div> <?php } ?>
	</div >
	<div class="user-entry-block">
		<div class="row">				
			<form method="post" name="csvUploaform" id="csvUploaform" enctype="multipart/form-data">				
				<div class="span6 last">
					<label id="" class="user-name">Select Location:</label>
					<select class="select-option" name="Location" id="Location">						
					<?php
						if($ManagesData !=NULL)	{
							$DID = "";
							foreach($ManagesData as $manages)
							{ 
								if( $manages["deviceId"] != $DID ) {
									$COLS = "DeviceName";
									$COND = "where deviceId=".$manages["deviceId"];
									$DName = $pricingDetails->selectTableSingleRow($tbl_manage_device,$COND,$COLS);
									echo "<optgroup label='$DName[DeviceName]'>";
									$DID = $manages["deviceId"];
								}
							?>
								<option value="<?php echo $manages['ManageLocationId']?>" ><?php echo $manages['LocationName']?></option>						
					<?php	}
						}		
					?>
					</select>
				</div><!--end span6-->	
			</form>	
		</div><!--End row-->
		<div class="row sortingHead">	
			<div class="span3 srtHeadEditEmp srtHeadBorder">Size <br>&nbsp;</div>
			<div class="span3 srtHeadEditEmp srtHeadBorder" style="width:11%">Area<br>&nbsp;</div>
			<div class="span3 srtHead srtHeadBorder"> Recommended Price</div>
			<div class="span3 srtHead srtHeadBorder"> Four Session Discount</div>
			<div class="span3 srtHead srtHeadBorder" style="width:14%"> Six Session Discount</div>
			<div class="span3 srtHead srtHeadBorder"> Savings for Four Sessions</div>
			<div class="span3 srtHead srtHeadBorder"> Savings for Six Sessions</div>
			<div class="span3 srtHeadEditEmp srtHeadBorder" style="border:none;width:8%;">Action <br>&nbsp;</div>
		</div>
        <div id="container"></div>
        <div id="container1"></div>
        <div class="row">
			<div id="loading"></div>
        </div>	 
 		<div class="overlay-bg-pricing"> </div>
	</div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php 
	include('admin_includes/footer.php');	
?>
