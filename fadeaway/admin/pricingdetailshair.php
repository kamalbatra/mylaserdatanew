<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include('admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");	
	$pricingDetails	= new dbFunctions();
	if( !in_array(9,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
<?php 
	}
	/*** fetch Add Manage location**/
	$tbl_manage_location = "tbl_manage_location";
	$tbl_manage_device = "tbl_devicename";
	$DCond = "where serviceId=2 AND BusinessID=".$_SESSION["BusinessID"];
	$Dcols = "deviceId";
	$DData = $pricingDetails->selectTableRows($tbl_manage_device,$DCond,$Dcols);
	$DIDs = "";
	if( $DData != "" ) {
		for( $i=0;$i<count($DData);$i++ ) {
			$DIDs.= $DData[$i]["deviceId"];
			if( $i < count($DData)-1 )
				$DIDs.= ",";
		} 
	}
	$ManagesData = array();
	if( $DIDs != '' ) {
		$condition = "where BusinessID=".$_SESSION["BusinessID"]." AND deviceId in(".$DIDs.") ORDER BY deviceId";
		$cols="*";
		$ManagesData = $pricingDetails->selectTableRows($tbl_manage_location,$condition);
	}
	/*** fetch All device Name**/
?>
<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/pricing.css" />
<script type="text/javascript">
	$(document).ready(function() {
		function loading_show() {
			$('#loading').html("<img src='<?php echo $domain; ?>/images/loading.gif'/>").fadeIn('fast');
		}
		function loading_hide() {
			$('#loading').fadeOut('fast');
		}                
		function loadData(page) {					
			loading_show();                    
			$.ajax ({
				type: "POST",
				url: "ajax_pricingdetailshair.php",
				data: "page="+page +'&Location='+$('#Location').val(),
				success: function(msg) {
					$("#container").ajaxComplete(function(event, request, settings) {
						loading_hide();
						$("#container1").hide()
						$("#container").html(msg).show();
					});
				}
			});
		}
		loadData(1);  // For first time page load default results
		$('#container .pagination li.active').live('click',function() {
			var page = $(this).attr('p');
			loadData(page);                    
		});  
		$('#Location').change(function() {
			loadData(1);                    
		});                          
		$('#go_btn').live('click',function() {
			var page = parseInt($('.goto').val());
			var no_of_pages = parseInt($('.total').attr('a'));
			if(page != 0 && page <= no_of_pages) {
				loadData(page);
			} else 	{
				alert('Enter a PAGE between 1 and '+no_of_pages);
				$('.goto').val("").focus();
				return false;
			}             
		});
		$('.pricingEdit').live('click',function() {
			var page = $(this).attr('page');
			var pid = $(this).attr('id');
			var size = $(this).attr('size');
			var str ='pid='+pid+'&Size='+size+'&page='+page;
			$.ajax ({
				type: "POST",
				url: "ajax_pricingedit.php",
				data: str,
				cache: false,
				success: function(result) {
					//alert(result);
					$('.overlay-bg-pricing').html(result).show(); //display your popup
					//$("#u_mess").html(result);
				}
			});
		});//. pricingEdit                 
		$('#updateBtn').live('click',function() {
			var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;				
			var arr = $('.pricingclass').map(function(i, e) {
				if(!numberRegex.test(e.value)) {
					//return e.value; 
					//alert("ee:-"+e.id);				 
					$("#"+e.id).css("border","1px solid red");	
					$("#"+e.id).focus();
					$("#"+e.id).addClass('placeholderInt');
					$("#"+e.id).attr("placeholder", "Enter numeric value only");	
					$("#pricingMsg").show().html("Please enter numeric value!").show();		  
					return e.id; 
				} else {
					$("#"+e.id).css("border","1px solid #ccc");
					//$("#pricingMsg").html("").hide();	
				}			 
			}).toArray();
			if(arr.length ==0) {	
				var str1 = $("#pricingform").serialize();
				$.ajax ({			
					type: "GET",
					url: "ajax_pricingUpdate.php",
					data: str1,
					success: function(msg1) {	
						$("#pricingMsg").hide();							
						var page = $('#page').val();							    
						$("#pricingMsgUpdate").css("color","green");
						$("#pricingMsgUpdateLoding").show();							      
						$("#pricingMsgUpdate").show().html("Record Updated successfully.");
						setTimeout(function() {
							loadData(page);
							$('.overlay-bg-pricing').hide();
							return false;
						}, 2000);
					}
				});
			}					
		});
		//End #updateBtn
		$('.crossiconblk').live('click',function() {
			$('.overlay-bg-pricing').hide();
		});
		
		$('#AddPrice').click(function(){
			var page = $(this).attr('page');
			var Location = $('#Location').val();
			var str ='Location='+Location+'&service=Hair&page='+page;
			$.ajax ({
				type: "POST",
				url: "ajax_addprice.php",
				data: str,
				cache: false,
				success: function(result) {
					//alert(result);
					$('.overlay-bg-pricing').html(result).show(); //display your popup
					//$("#u_mess").html(result);
				}
			});
		});
		//. pricingAdd               
		$('#addBtn').live('click',function() {
			var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;				
			var Arr = $('.pricingclass').map(function(i, e) {
				if(!numberRegex.test(e.value)) {
					//return e.value; 
					//alert("ee:-"+e.id);				 
					$("#"+e.id).css("border","1px solid red");	
					$("#"+e.id).focus();
					$("#"+e.id).addClass('placeholderInt');
					$("#"+e.id).attr("placeholder", "Enter numeric value only");	
					$("#pricingMsg").show().html("Please enter numeric value!").show();		  
					return e.id; 
				} else {
					$("#"+e.id).css("border","1px solid #ccc");
					//$("#pricingMsg").html("").hide();	
				}			 
			}).toArray();
			if(Arr.length ==0) {	
				var checkData = $("#addpricingform").serialize();
				$.ajax ({			
					type: "POST",
					url: "ajax_pricingcheck.php",
					data: checkData,
					success: function(res) {	
						if( res=="area" ) {
							$("#pricingMsg").html("Please select size and area.").show();
						} else if( res=="exists" ) {
							var addData = $("#addpricingform").serialize();
							$.ajax ({			
								type: "POST",
								url: "ajax_pricingUpdate.php",
								data: addData,
								success: function(res) {	
									$("#pricingMsg").hide();							
									var page = $('#page').val();							    
									$("#pricingMsgUpdate").css("color","green");
									$("#pricingMsgUpdateLoding").show();							      
									$("#pricingMsgUpdate").html("Record Updated successfully.");
									setTimeout(function() {
										loadData(1);
										$('.overlay-bg-pricing').hide();
										return false;
									}, 2000);
								}
							});
						} else if( res=="notexists" ) {
							var addData = $("#addpricingform").serialize();
							$.ajax ({			
								type: "POST",
								url: "ajax_pricingadd.php",
								data: addData,
								success: function(res) {	
									if( res=="Yes" ) {
										$("#pricingMsg").hide();							
										var page = $('#page').val();							    
										$("#pricingMsgUpdate").css("color","green");
										$("#pricingMsgUpdateLoding").show();							      
										$("#pricingMsgUpdate").html("Record Added successfully.");
										setTimeout(function() {
											loadData(1);
											$('.overlay-bg-pricing').hide();
											return false;
										}, 2000);
									}
									else {
										$("#pricingMsg").html(res).show();
									}
								}
							});
						}
					}
				});
			}					
		});
		//End #addBtn
	});
</script>
<style>
	.addNew { width: 17%; }
	.empHead { width: 66%; }
	.srtHead { width: 15%; }
	.heading-container h1.heading { width: 53%; font-size: 23px; }
</style>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Manage Pricing for Hair Removal</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock manageemp">
						<div class="busniss-search searchbussiness last searchbox-select">
							<form method="post" name="csvUploaform" id="csvUploaform" enctype="multipart/form-data">				
								<div>
									<select class="select-option" name="Location" id="Location">						
									<?php
										if($ManagesData !=NULL) {
											$DID = "";
											foreach($ManagesData as $manages) {
												if( $manages["deviceId"] != $DID ) {
													$COLS = "DeviceName";
													$COND = "where deviceId=".$manages["deviceId"];
													$DName = $pricingDetails->selectTableSingleRow($tbl_manage_device,$COND,$COLS);
													echo "<optgroup label='$DName[DeviceName]'>";
													$DID = $manages["deviceId"];
												}
											?>
												<option value="<?php echo $manages['ManageLocationId']?>" ><?php echo $manages['LocationName']?></option>						
									<?php	}
										}
									?>
									</select>
								</div><!--end span6-->
							</form>
						</div>
						<div class="search-btn">
							<?php 
							if(in_array("1",$_SESSION["services"])) { ?> 
								<a class="empLinks" href="pricingdetails" class="submit-btn"><button class="addnewbtn">Tattoo Removal</button></a>
							<?php } ?>
							<?php if($ManagesData !=NULL){ ?>
								<a class="empLinks" href="import?service=Hair" class="submit-btn"><button class="addnewbtn">Import csv</button></a>
								<a class="empLinks" id="AddPrice" class="submit-btn" style="cursor:pointer;"><button class="addnewbtn">Add Price</button></a>
							<?php } ?>							
						
						</div>
					
				</div>					
					<div class="card-body">
						<?php 
						if($_SESSION['BusinessID']=='6') {
							$label="Three";
						} else {
							$label="Four";
						}
						?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th class="span3 srtHeadEditEmp srtHeadBorder">Size <br>&nbsp;</th>
										<th class="span3 srtHeadEditEmp srtHeadBorder">Area<br>&nbsp;</th>
										<th class="span3 srtHead srtHeadBorder"> Recommended Price</th>
										<th class="span3 srtHead srtHeadBorder"> Four Session Discount</th>
										<th class="span3 srtHead srtHeadBorder" style="width:14%"> Six Session Discount</th>
										<th class="span3 srtHead srtHeadBorder"> Savings for Four Sessions</th>
										<th class="span3 srtHead srtHeadBorder"> Savings for Six Sessions</th>
										<th class="span3 srtHeadEditEmp srtHeadBorder">Action <br>&nbsp;</th>
									</tr>
								</thead>
								
								<tbody id="container">
									<!-- div id="container"></div>
									<div id="container1"></div>
									<div class="row">
										<div id="loading"></div>
									</div>
									<div class="overlay-bg-pricing"></div -->
								</tbody>
							</table>
						</div>
						
					</div>
				</div>
				<div class="overlay-bg-pricing"></div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
