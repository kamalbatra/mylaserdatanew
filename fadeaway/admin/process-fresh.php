<?php
	session_start();
	$domain=$_SERVER['DOMAIN'];
	include("../includes/dbFunctions.php");
	$sizes	= new dbFunctions();
	error_reporting(0);
	if(isset($_POST['name']))
	{
		$table = "tbl_pricing_table";
		$condition = " where Size=".$_POST['name'];
		$sizedata = $sizes->selectTableSingleRow($table,$condition);
		echo "Suggested Pricing:"."<br/>";
		echo "Size in Square Inches ".$_POST['name']."<br/>";
		echo "Price per session ".$sizedata['Rec_Price']."<br/>";
		echo "3 session 5% discount price ".$sizedata['discount5']." Savings ".$sizedata['savings5']."<br/>";
		echo "6 session 10% discount price ".$sizedata['discount10']." Savings ".$sizedata['savings10']."<br/>";
	}
	else if(isset($_POST['clientname']))
	{
		$_POST['clientnameid'];
		if($_POST['clientnameid']!=NULL)
		{
			$pieces = explode(" ", $_POST['clientname']); 
			$table = "tbl_consent_form";
			$tableSkin= "tbl_FitzPatrick_skin_types";
			$condition = " where ClientID=".$_POST['clientnameid']." ";
			$cols = "*";
			$sizedata = $sizes->selectTableData($table,$condition,$cols);
			$clientdata = $sizes->selectTableSingleRow($table,$condition);
		?>
			<h1 id="search_rep" class="heading">Search Report</h1>
		<?php
			if(!empty($sizedata))
			{
			$i = 0;
			foreach($sizedata as $key=>$data)
			{
				if($i%2==0)
				{
					$bgdata = "bgdata";
				}
				else
				{
					$bgdata = "bgnone";
				}
			?>
				<div class="row treatment <?php echo $bgdata;?>">
					<div class="span6" style="width:16%;"><label id="Label1" class="user-name">
					<?php //echo $key; ?>
					<?php $with_space = preg_replace('/[A-Z]/'," $0",$key); 
						$str= ucwords($with_space);
						$str = explode(" ",$str);
						// print_r($str);
						echo $str = $str[1]."&nbsp;".strtolower($str[2]).strtolower($str[3]);
					?>
					</label></div>
					<div class="span6" style="width:1%;"><label id="Label1" class="user-name">:</label></div>
					<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data;?></label></div>							
				</div><!--End @row-->
			<?php
				$i++;	
			}
		?>
			<form action="" method="POST" name="" id="edit_ConsetDetails">
				<div class="user-entry-block" style="margin-top:50px;">					
					<div class="row">
					   <input type="hidden" name="ConsentID" value="<?php echo $clientdata['ConsentID'];?>" />
					   <div class="span6"> 
							<label id="Label1" class="user-name">Fitzpatrick Skin Type: </label>
							<select name="SkinType" id="SkinType" class="text-input-field select-option">
							<?php
								$clientdata = $sizes->selectTableRows($tableSkin);
								foreach($clientdata as $skin)
								{
								?>
									<option> <?=$skin['SkinType']; ?> </option>
								<?php
								}	?>
							</select>
						</div>	
						<div class="span6 last">
							<label id="Label1" class="user-name">	Additional information notes for this client:</label>
							<textarea name="TechNotes" id="TechNotes" class="text-area-field" rows="4" cols="20"></textarea>
						</div>	  
					</div>  
					<div class="row">	
						<div class="span6"> 
							<label id="Label1" class="user-name">Indicate the price quoted:</label> 
							<input type="text" class="text-input-field search_for_client" name="PriceQuote" id="PriceQuote"/>
						</div> 
						<div class="span6 last">
							<label id="Label1" class="user-name">Check to complete the review process:</label>
							<input type="checkbox" name="TechnicianReview" value="Y">
						</div>	  
					</div>	
					<div class="row">
						<div class="span6"> 
							<input type="button" class="submit-btn" value="submit" style="cursor:pointer;" onclick="return edit_formConsentAdd();"/>
							<div class="u_mess" id="conset_massage"></div>
						</div>
					</div>
				</div>
			</form>
		<?php 
			echo '<script>document.getElementById("errorMsgPrint").style.display="none"</script>';
			}
			else
			{
				print "No record found!";
				echo '<script>document.getElementById("errorMsgPrint").style.display="none"</script>';
			}	
		}	
		else if($_POST['clientnameid']==NULL)
		{
			//echo '<p style="color:#FF0000;font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 13px;margin-top: -90px;">'.'please enter name'.'</p>';
			echo '<script>document.getElementById("errorMsgPrint").innerHTML="Please enter name."</script>';
		}
	}
	else if(isset($_POST['clientnameid']) && isset($_POST['firstname']) && isset($_POST['lastname']))
	{
		$table = "tbl_ta2_wavelengths";
		$sizedata = $sizes->selectTableRows($table);
		$table1	= "tbl_ta2_spot_sizes";
		$sizedata1 = $sizes->selectTableRows($table1);
		$tableConsent1 = "tbl_treatment_log";
		$cols = " ClientID,SessionNumber,TattooInfo,Size,Price,Treatment_notes,blisters,purpura,pain,other ";
		$condition = " where ClientID=".$_POST['clientnameid']." ";
		$checkExistence = $sizes->selectTableSingleRow($tableConsent1,$condition,$cols);
		$sessionValue = $checkExistence['SessionNumber']+1;
		//if($checkExistence==NULL){ $status="inserted";}	else{ $status="updated"; }
		//print_r($checkExistence);
		if($sessionValue<=3) 
		{
		?>
			<!-- Use javascript for load treatment.php file .*** call  from  heading Prior Treatments-->
			<!--script src="http://code.jquery.com/jquery-latest.min.js"type="text/javascript"></script-->
			<script type="text/javascript" src="<?php echo $domain; ?>/js/jquery-1.9.1.js" ></script>
			<!--script src="<?php echo $domain; ?>/js/popUp.js"type="text/javascript"></script-->
			<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/popUp.css" />
			<script>
				$(document).ready(function() 
				{
					$(".priorheading").click(function() 
					{ 
						//alert('ss');
						var ClientID = $(this).attr('id');
						var dataString = 'ClientID='+ ClientID;
						//alert(dataString);
						if(ClientID!='')
						{
							$.ajax({
								type: "POST",
								url: "treatments.php",
								data: dataString,
								cache: false,
								success: function(html)
								{
									// loading();
									$("#treatmentsResult").html(html).show();
									setTimeout(function()
									{ // then show popup, deley in .5 second
										loadPopup(); // function show popup 
									}, 500); // .5 second							   
								}
							});
						}return false;   
					});
					var popupStatus = 0; // set value	
					function loadPopup() 
					{ 
						if(popupStatus == 0) 
						{ // if value is 0, show popup
							//closeloading(); // fadeout loading
							$("#toPopup").fadeIn(0500); // fadein popup div
							$(".container").css("opacity", "0.7"); // css opacity, supports IE7, IE8
							//$(".container").css("position", "fixed");
							//$(".container").addClass("backgroundPopup");
							$(".container").fadeIn(0001); 
							popupStatus = 1; // and set value to 1
						}
					}
					/*function loading() 
					{
						$("div.loaderp").show();  
					}
					function closeloading() 
					{
						$("div.loader").fadeOut('normal');  
						*/
						$("div.close").click(function() 
						{
							disablePopup();  // function close pop up
						});
						function disablePopup() 
						{
							if(popupStatus == 1) 
							{ // if value is 1, close popup
								$("#toPopup").fadeOut("normal");  
								//$(".container").fadeOut("normal");
								$(".container").css("opacity", "1");
								//$(".container").removeClass("backgroundPopup");
								popupStatus = 0;  // and set value to 0
							}
						}
				});
			</script>
			<div class="search-client-block">
				<div class="divider-dotted">&nbsp;</div>
				<h2>Fade Away Laser Treatment Log</h2>
				<form action="" method="post" name="treatmentForm" id="treatmentForm">
					<input type="hidden" name="ClientID" id="ClientID" value="<?php echo $_POST['clientnameid']; ?>"/>
					<h5>Tattoo Info</h5>
					<!--a href="#" onclick="javascript:void window.open('treatments.php?ClientID=<?php echo $_POST['clientnameid']; ?>','1391686753074','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;"><h6>Prior Treatments</h6></a><br/-->
					<h6 class="priorheading" id="<?php echo $_POST['clientnameid']; ?>">Prior Treatments</h6><br/>
					<!--a href="#" class="topopup">Click Here Trigger</a-->    
					<div id="toPopup">
						<div class="close"> </div>
						<div id="treatmentsResult"> </div>
					</div>	
					<div class="loader"></div>
					<!-- start new div for show result of treatments.php -->
					<!-- end new div for show result -->
					<div class="row">
						<div class="span3">
							<label id="Label1" class="user-name">Session Number:</label>
						</div>
						<div class="span3">
							<input class="text-input-field" type="text" name="SessionNumber" id="SessionNumber" value="<?php echo $sessionValue;?>" readonly="readonly"/>
						</div>
					</div><!--End @row-->			
					<div class="row">
						<div class="span12">
							<label id="Label1" class="user-name">Tattoo Description and/or response to last treatment:</label>
							<textarea rows="3" cols="20" name="TattooInfo" id="TattooInfo" class="text-area-field"><?php echo $checkExistence['TattooInfo']; ?></textarea>	
						</div>
					</div><!--End @row-->
					<div class="row">
						<div class="span3">
							<label id="Label1" class="user-name">Tattoo size in square inches:</label>
						</div>				
						<div class="span6 last">
							<input type="text" class="text-input-field" name="Size" id="Size" value="<?php echo $checkExistence['Size']; ?>"/>
						</div>
					</div><!--End @row-->
					<div class="row">
						<div class="span3">
							<a href="#" onclick="javascript:void window.open('addphoto.php','1391686753074','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;"><label id="Label1" class="user-name">Upload Client Photo:</label></a>
						</div>				
						<div class="span6 last">
							<input class="text-input-field" name="Tattoo_Photo_original" type="file" />
						</div>
					</div><!--End @row-->
					<div class="divider-dotted">&nbsp;</div>
					<h2>Treatment Parameters</h2>			
					<div class="row">
						<div class="span3">
							<label id="Label1" class="user-name">Wavelength:</label>
						</div>		
					<?php
						if($sessionValue==1){ $wavelength="Wavelength"; $spotsize="SpotSize";}
						else if($sessionValue==2){ $wavelength="Wavelength2"; $spotsize="SpotSize2";}
						else if($sessionValue==3){ $wavelength="Wavelength3"; $spotsize="SpotSize3";}
					?>							
						<div class="span3 last">
							<select class="select-option" name="<?php echo $wavelength; ?>" id="<?php echo $wavelength; ?>">
							<?php
								foreach($sizedata as $value)
								{
								?>
									<option><?php echo $value['Wavelength']; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div><!--End @row-->
					<div class="row">
						<div class="span3">
							<label id="Label1" class="user-name">Handpiece:</label>
						</div>				
						<div class="span3 last">
							<select class="select-option" name="<?php echo $spotsize; ?>" id="<?php echo $spotsize; ?>">
							<?php
								foreach($sizedata1 as $value)
								{
							?>
									<option><?php echo $value['spotsize']; ?></option>
							<?php
								}
							?>
							</select>
						</div>
					</div><!--End @row-->
					<div class="row">
						<div class="span12">
							<label id="Label1" class="user-name">Treatment notes:</label>
							<textarea class="text-area-field" rows="4" cols="20" name="Treatment_notes" id="Treatment_notes"><?php echo $checkExistence['Treatment_notes']; ?></textarea>
						</div>
					</div><!--End @row-->
					<div class="divider-dotted">&nbsp;</div>
					<h2>Pricing</h2>			
					<div class="row">
						<div class="span3">
							<label id="Label1" class="user-name">Price in $</label>
						</div>				
						<div class="span3 last">
							<input class="text-input-field" type="text" name="Price" id="Price" value="<?php echo $checkExistence['Price']; ?>"/>
						</div>
					</div><!--End @row-->
					<div class="row">
						<div class="span3">
							<label id="Label1" class="user-name">Calculate Price</label>
						</div>				
						<div class="span3 last">
							<input class="text-input-field" type="text"  id="pricing_amt"/>			
							<div id="prices"></div>
						</div>
					</div><!--End @row-->
					<!--div class="row">
						<div class="span12">
							<a href="#" class="pricing-calculator-link" onclick="javascript:void window.open('pricing.php','1391686753074','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Pricing Calculator</a>
						</div>
					</div><!--End @row-->
					<div class="divider-dotted">&nbsp;</div>
					<h2>Complications</h2>
					<div class="row">
						<div class="span12">
							<input type="checkbox" name="blisters" id="blisters" value="TRUE" <?php if($checkExistence['blisters']=="TRUE"){ ?> checked="checked" <?php } ?>/><span class="checkbox-txt">Blisters</span>
							<input type="checkbox" name="purpura" id="purpura" value="TRUE" <?php if($checkExistence['purpura']=="TRUE"){ ?> checked="checked" <?php } ?>/><span class="checkbox-txt">Purpura (pin point bleeding)</span>
							<input type="checkbox" name="pain" id="pain" value="TRUE" <?php if($checkExistence['pain']=="TRUE"){ ?> checked="checked" <?php } ?>/><span class="checkbox-txt">Pain Tolerance Problems</span>				
						</div>
					</div><!--End @row-->
					<div class="row">
						<div class="span12">
							<label id="Label1" class="user-name">other:</label>
							<textarea  cols="20" rows="4" class="text-area-field" name="other" id="other"><?php echo $checkExistence['other']; ?></textarea>
						</div>
					</div><!--End @row-->
					<input type="hidden" name="Client_First_Name" id="Client_First_Name" value="<?php echo $_POST['firstname']; ?>"/>
					<input type="hidden" name="Client_Last_Name" id="Client_Last_Name" value="<?php echo $_POST['lastname']; ?>"/>
					<input type="hidden" name="St_Paul" id="St_Paul" value="<?=$_SESSION['Emp_Location_St_Paul']?>"/>
					<input type="hidden" name="Minneapolis" id="Minneapolis" value="<?=$_SESSION['Emp_Location_Minneapolis']?>"/>
					<input type="hidden" name="Duluth" id="Duluth" value="<?=$_SESSION['Emp_Location_Duluth']?>"/>
					<input type="hidden" name="Emp_First_Name" id="Emp_First_Name" value="<?=$_SESSION['First_Name']?>"/>
					<input type="hidden" name="Emp_Last_Name" id="Emp_Last_Name" value="<?=$_SESSION['Last_Name']?>"/>
					<input type="hidden" name="Date_of_treatment" id="Date_of_treatment" value="<?=date('Y-m-d H:i:s'); ?>"/>
					<div class="row">
						<div class="span12">
							<input type="button" value="Submit" class="submit-btn u_p" onclick="treat_client();"/>
							<div id="u_mess" class="u_mess"></div>
						</div>
					</div><!--End @row-->
				</form>
			</div>
		<?php
		}
		else
			echo "session ended for this client";
	}
?>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#pricing_amt').keyup(function()
		{
			$('#prices').load('process.php', { 'name': $('input[id="pricing_amt"]').val()});
			$('#prices').addClass('price_back');                   
			return false;
	    });
	});
</script>
