<?php
//session_start();
include("../includes/dbFunctions.php");
$domain=$_SERVER['DOMAIN'];
$sizes	= new dbFunctions();
error_reporting(0);
if(isset($_POST['name'])){
	$table	= "tbl_pricing_table";
	$condition = " where Size=".$_POST['name'];
	$sizedata = $sizes->selectTableSingleRow($table,$condition);
	if(!empty($sizedata)){
		//echo '<script>document.getElementById("prices").style.display="block"</script>';
		echo "Suggested Pricing:"."<br/>";
		echo "Size in Square Inches     ".$_POST['name']."<br/>";
		echo "Price per session     ".$sizedata['Rec_Price']."<br/>";
		echo "3 session 5% discount price     ".$sizedata['discount5']." Savings ".$sizedata['savings5']."<br/>";
		echo "6 session 10% discount price	   ".$sizedata['discount10']." Savings ".$sizedata['savings10']."<br/>";
	}
} else if(isset($_POST['clientname'])) {
	$_POST['clientnameid'];
	if($_POST['clientnameid']!=NULL){
		
		$pieces 		= explode(" ", $_POST['clientname']); 
		$table			= "tbl_consent_form";
		$tableSkin 		= "tbl_FitzPatrick_skin_types";
		$condition 		= " where ClientID=".$_POST['clientnameid']." ";
		$cols 			= "*";
		$sizedata 		= $sizes->selectTableData($table,$condition,$cols);
		$clientdata 	= $sizes->selectTableSingleRow($table,$condition);
		$clientcols = "AES_DECRYPT(FirstName, '".SALT."') AS FirstName, AES_DECRYPT(LastName, '".SALT."') AS LastName, AES_DECRYPT(DOB, '".SALT."') AS DOB";
		$clientdob 		= $sizes->selectTableSingleRow("tbl_clients", $condition, $clientcols);
		$sizedata->dob 	= $clientdob['DOB'];
		$sizedata->ClientSignature = ucfirst($clientdob['FirstName']).' '.ucfirst($clientdob['LastName']);
		$sizedata->ClientSignaturePrompt = '/'.ucfirst($clientdob['FirstName']).' '.ucfirst($clientdob['LastName']).'/'
		?>
		<script>
			$(document).ready(function(){
				$('#pricing_amt').keyup(function() {		  
					if($.isNumeric($('#pricing_amt').val()) && $('#pricing_amt').val() !="" ) {				
						var str = $("#pricingform").serialize();
						$.ajax({
							type: "POST",
							url: "ajax_pricing.php",
							data: str,
							cache: false,
							success: function(result){
								$("#prices").show("slow");			
								$("#prices").html(result);
							}
						});	    
			   	} //if is numreic
			   	else{
			   		$("#prices").hide("slow");		
			   	}
			   });
			});	   
		</script>
		<div class="searchReportBlk">
            <h2 id="search_rep" class="heading">Search Report</h2>
            <div class="searchReportOuterblk">
			<?php
				if(!empty($sizedata)){
					$i = 0;
					$sizedata = objectToArray($sizedata);
					list($array1, $array2) = array_chunk($sizedata, ceil((count($sizedata))/2), true);
			?>
					<div class="half">
						<div class="card shadow mb-4 searchReportcard">
						<?php
						foreach($array1 as $key=>$data){
							if($data !=""){
								if($key=="TattooAge"){
									if( $data == "-1" ){
										$data = "<1 year";
									} else if( $data == 1 ){
										$data = $data.' Year';
									}else if ( $data == "20+" ){
										$data = ">20 years";
									} else {
										$data = $data.' Years';
									}
								}
								if($key=="TattoColor"){
									$tablecolor = "tbl_tatto_colors";
									$colorcols = "name";
									$colorcondition = " where id IN (".$data.") LIMIT 0,10";
									$tattocolors = $sizes->selectclientrecord($tablecolor,$colorcols,$colorcondition);
									$colorarray = array();
									foreach($tattocolors as $tattocolor ){
										array_push($colorarray, ucfirst($tattocolor['name']));
									}
									$data = implode(', ',$colorarray);
								}
								if($key=="BodyPart"){
									$tablebody = "tbl_tattoremove_part";
									$bodycols = "part_name";
									$bodycondition = " where id IN (".$data.") LIMIT 0,10";
									$bodyparts = $sizes->selectclientrecord($tablebody,$bodycols,$bodycondition);
									$bodypararray = array();
									foreach($bodyparts as $bodypart ){
										array_push($bodypararray, ucfirst($bodypart['part_name']));
									}
									$data = implode(', ',$bodypararray);
								}
								if($key=="TechnicianReview" || $key=="EmployeeID" || $key=="TechnicianFirstName" || $key=="TechnicianLastName" || $key=="Location" || $key=="St_Paul"  || $key=="Duluth" || $key=="Minneapolis"  ) {
									$style="display:none;";
								} else {
									$style="";
								}
								if($data=="Yes"){
									$style1="color:red;";
								} else {
									$style1="";
								}
								if($i%2==0){
									$bgdata = "bgdata";
								}
								else{
									$bgdata = "bgnone";
								}
								?>		
								<div class="searchreportrow <?php echo $bgdata;?>" style="<?php echo $style; ?>">
									<div class="serachreportTital">				
									<?php 
										$with_space = preg_replace('/[A-Z]/'," $0",$key); 
										$str 		= ucwords($with_space);
										$str 		= explode(" ",$str);           
										$strUnde 	= $str[1];
										$journalName = str_replace("_", " ",$strUnde);        
										if(strlen($str[3]) <=1){
											$code = $str[3];
										}else{
											$code = "&nbsp;".$str[3];
										}
										if($key=="HIV"){
											echo $key;
										}
										if($key=="SmokeCigarette"){
											echo "Do you smoke cigarettes?";
										} else if($key=="TattooAge"){
											echo "Approximately how many years have you had the tattoo you wish to remove?";
										} else if($key=="TattoColor"){
											echo "Which colors are present in your tattoo?";
										} else if($key=="BodyPart"){
											echo "On which body part is the tattoo you wish to remove?";
										} else if($key=="dob"){
											echo "Date of Birth";
										}
										else{										
											echo ($key=="HIV") ? '' : ($str = $journalName."&nbsp;".strtolower($str[2]).strtolower($code));
										}                 
									?>
										<b>:</b>
									</div>
									<div class="searchReportvalue">
										<?php echo $data;?>
									</div>
								</div>
								<?php 
								if($key=="Location"){ ?>
									<div class="searchreportrow treatment bgnone">
										<div class="serachreportTital">
											<?php echo $key; ?><b>:</b>
										</div>				
										<div class="searchReportvalue">
											<label id="Label1" class="user-name" style="<?php echo $style1; ?>">
												<?php
												$clentLocation = explode(",",$data);					        
												for($j=0 ; $j<count($clentLocation);$j++){								 
													$tbl_manage_location	= "tbl_manage_location";
													$condition1 	= " where ManageLocationId=".$clentLocation[$j]." ";
													$cols 			= "*";
													$locationData	= $sizes->selectTableSingleRow($tbl_manage_location,$condition1);
													echo $locationData['LocationName'];
													if($j !=count($clentLocation)-1){
														echo ",&nbsp;";
													}							
												}//for loop close ?>
											</label>
										</div>							
									</div>
								<?php
								}
								$i++;
							}
						}
						?>	
						</div>
					</div>
					<div class="half">
						<div class="card shadow mb-4 searchReportcard">
						<?php
						foreach($array2 as $key=>$data){
							if($data !=""){
								if($key=="TattooAge"){
									if( $data == "-1" ){
										$data = "<1 year";
									} else if( $data == 1 ){
										$data = $data.' Year';
									}else if ( $data == "20+" ){
										$data = ">20 years";
									} else {
										$data = $data.' Years';
									}
								}
								if($key=="TattoColor"){
									$tablecolor = "tbl_tatto_colors";
									$colorcols = "name";
									$colorcondition = " where id IN (".$data.") LIMIT 0,10";
									$tattocolors = $sizes->selectclientrecord($tablecolor,$colorcols,$colorcondition);
									$colorarray = array();
									foreach($tattocolors as $tattocolor ){
										array_push($colorarray, ucfirst($tattocolor['name']));
									}
									$data = implode(', ',$colorarray);
								}
								if($key=="BodyPart"){
									$tablebody = "tbl_tattoremove_part";
									$bodycols = "part_name";
									$bodycondition = " where id IN (".$data.") LIMIT 0,10";
									$bodyparts = $sizes->selectclientrecord($tablebody,$bodycols,$bodycondition);
									$bodypararray = array();
									foreach($bodyparts as $bodypart ){
										array_push($bodypararray, ucfirst($bodypart['part_name']));
									}
									$data = implode(', ',$bodypararray);
								}
							
								if($key=="TechnicianReview" || $key=="EmployeeID" || $key=="TechnicianFirstName" || $key=="TechnicianLastName" || $key=="Location" || $key=="St_Paul"  || $key=="Duluth" || $key=="Minneapolis"  ){$style="display:none;";}else{$style="";}	
								if($data=="Yes"){$style1="color:red;";}else{$style1="";}	
								if($i%2==0){
									$bgdata = "bgdata";
								}
								else{
									$bgdata = "bgnone";
								}
									
								?>		
								<div class="searchreportrow <?php echo $bgdata;?>" style="<?php echo $style; ?>">
									<div class="serachreportTital">				
										<?php 
										$with_space = preg_replace('/[A-Z]/'," $0",$key); 
										$str 		= ucwords($with_space);
										$str 		= explode(" ",$str);           
										$strUnde 	= $str[1];
										$journalName = str_replace("_", " ",$strUnde);        
										if(strlen($str[3]) <=1){
											$code = $str[3];
										}else{
											$code = "&nbsp;".$str[3];
										}
										if($key=="HIV"){
											echo $key;
										}
			
										if($key=="SmokeCigarette"){
											echo "Do you smoke cigarettes?";
										} else if($key=="TattooAge"){
											echo "Approximately how many years have you had the tattoo you wish to remove?";
										} else if($key=="TattoColor"){
											echo "Which colors are present in your tattoo?";
										} else if($key=="BodyPart"){
											echo "On which body part is the tattoo you wish to remove?";
										} else if($key=="dob"){
											echo "Date of Birth";
										}
										else{										
											echo ($key=="HIV") ? '' : ($str = $journalName."&nbsp;".strtolower($str[2]).strtolower($code));
										}                 
										?>
										<b>:</b>
									</div>
									<div class="searchReportvalue">
										<?php echo $data;?>
									</div>
								</div>
								<?php if($key=="Location"){ ?>
									<div class="searchreportrow treatment bgnone">
										<div class="serachreportTital">
											<?php echo $key; ?><b>:</b>
										</div>				
										<div class="searchReportvalue">
											<label id="Label1" class="user-name" style="<?php echo $style1; ?>">
												<?php  
												$clentLocation = explode(",",$data);					        
												for($j=0 ; $j<count($clentLocation);$j++){								 
													$tbl_manage_location	= "tbl_manage_location";
													$condition1 	= " where ManageLocationId=".$clentLocation[$j]." ";
													$cols 			= "*";
													$locationData	= $sizes->selectTableSingleRow($tbl_manage_location,$condition1);
													echo $locationData['LocationName'];
													if($j !=count($clentLocation)-1){
														echo ",&nbsp;";
													}							
												}//for loop close?>				
											</label>
										</div>							
									</div>
							<?php 
								}
								$i++;
							}
						}
						?>
						</div>
					</div>
					<div class="agreementblock">
						<i>AGREEMENT:</i> By signing this Electronic Signature Acknowledgment Form, I agree that my electronic signature is the legally binding equivalent to my handwritten signature. Whenever I execute an electronic signature, it has the same validity and meaning as my handwritten signature. I will not, at any time in the future, repudiate the meaning of my electronic signature or claim that my electronic signature is not legally binding.
					</div>
					<!--  conset form additinal Technician review-->
					
					<?php 
					if($clientdata['TechnicianReview'] =="" && $_SESSION['Technician']=="TRUE"){ ?>
					<div class="formWhiteBlock">
						<form action="" method="POST" name="edit_ConsetDetails" id="edit_ConsetDetails" class="processedit_ConsetDetails">
							<div class="form-row-ld">
								<input type="hidden" name="ConsentID" value="<?php echo $clientdata['ConsentID'];?>" />
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Fitzpatrick Skin Type: </label>
											<select name="SkinType" id="SkinType" class="text-input-field select-option">
											<?php
												$clientdata = $sizes->selectTableRows($tableSkin);
												foreach($clientdata as $skin){ ?>
													<option> <?php echo $skin['SkinType']; ?> </option>
											<?php 
												}
											?>
											</select>
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld ">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">	Additional information notes for this client.</label>
											<textarea name="TechNotes" id="TechNotes" class="text-area-field" rows="4" cols="20"></textarea>
											<div id="TechNotesError" class="errorblocks"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label id="Label1" class="user-name">Indicate the price quoted.</label> 
											<input type="text" class="text-input-field search_for_client" name="PriceQuote" id="PriceQuote" autocomplete="off"/>
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<div class="radioblocksBtns">
												<div class="radioBtn ckeckboxres">	
													<input type="checkbox" name="TechnicianReview" id="TechnicianReview" value="Y" class="largerCheckbox" style="float:left">
													<label for="TechnicianReview" class="user-name">Check to complete the review process.</label>
													<div id="TechnicianReviewError" class="errorblocks"></div>
													<input type="hidden" name="TimeStampTechnician" id="TimeStampTechnician" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
													<input type="hidden" name="TechnicianSignature" id="TechnicianSignature" value="/<?php echo $_SESSION['First_Name'].' '.$_SESSION['Last_Name']; ?>/"/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-bottom: 23px;">	
								<div id="prices" class="row price_back1" style="width:100%;display:none;border-bottom:1px solid;"></div>
							</div> 
							<div class="form-row-ld">
								<div class="backNextbtn">
									<input type="button" class="submit-btn clicktosearchreferral" value="Submit" style="cursor:pointer;float:left;" onclick="return edit_formConsentAdd();"/>
									<div class="u_mess" id="conset_massage"></div>
								</div>
							</div>						

							</div>
						</form>
						</div>
					<?php 
					} ?>
					<?php
					if($clientdata['MedicalDirectorNotes'] =="" && $_SESSION['Medicaldirector']=="Yes") {	
						$check = 1;
					}
					else {
						$check = 0;
					}
					?>	
					<?php 
					if($check ==1){ ?>
					<div class="formWhiteBlock">
						<div class="docAddContainer" style="border-top: 1px dotted #ccc;float:left;width:100%;margin-top: 30px;">	
							<form action="" method="POST" name="" id="edit_DocDetails">
								<input type="hidden" name="ConsentID" value="<?php echo $clientdata['ConsentID'];?>" />								
								<div class="form-row-ld">
									<div class="full">
										<div class="form-col-ld">
											<div class="inputblock-ld">
												<label id="Label1" class="user-name">Additional information notes for this client(By director).</label>
												<textarea name="MedicalDirectorNotes" id="MedicalDirectorNotes" class="text-area-field" rows="4" cols="20"></textarea>
												<div id="errMsg" class="errorblocks"></div>
												<input type="hidden" name="MedicalDirectorSignature" id="MedicalDirectorSignature" value="/<?php echo $_SESSION['First_Name'].' '. $_SESSION['Last_Name']?>,MD/"/>
												<input type="hidden" name="TimeStampDoctor" id="TimeStampDoctor" value="<?=date('Y-m-d H:i:s'); ?>"/>
											</div>
										</div>
									</div>
								</div>
								<div class="form-row-ld">
									<div class="backNextbtn">
										<input type="button" class="submit-btn clicktosearchreferral" value="Submit" style="cursor:pointer;float:left;" onclick="return edit_docformConsentAdd();"/>
										<div class="u_mess" id="conset_Docmassage"></div>
									</div>
								</div>								
							</form>
						</div><!-- @End docAddContainer -->
					</div>
					<?php 
					} ?>
					
					<!----End Consent Additional review For Doctor(Admin)-->
					<?php 
					echo '<script>document.getElementById("errorMsgPrint").style.display="none"</script>';
				} else {
					print "No record found!";
					echo '<script>document.getElementById("errorMsgPrint").style.display="none"</script>';
				}
				?>
			</div>
		</div>
		<?php
	}
	else if($_POST['clientnameid']==NULL){
		echo '<script>document.getElementById("errorMsgPrint").innerHTML="Please enter name."</script>';
	}
}
	/******************************* Prior treatments ************************/
	/******************************* Prior treatments ************************/
	/******************************* Prior treatments ************************/
	else if(isset($_POST['clientnameid']) && isset($_POST['firstname']) && isset($_POST['lastname']) || isset($_POST['SessionNumber'])) {
	?>
		<script>$('#loadingTatto').show();</script>
		<?php
		/** Selected Decice Name Show According to Technician location **/
		$tbl_manage_location= "tbl_manage_location";
		$colsName 			= "*";
		$conditionLocation 	= " where ManageLocationId='".$_SESSION['Location']."' ";
		$locationData 		= $sizes->selectTableSingleRow($tbl_manage_location,$conditionLocation,$colsName);
		$locationData['deviceId'];	
		/****/
		$conditionForDevice = " WHERE BusinessID = $_SESSION[BusinessID] AND status=1 AND serviceId=1";
		if($locationData['deviceId']!=""){
			if($_SESSION['Usertype']=="Employee"){
				//$conditionForDevice .=" AND deviceId=".$locationData['deviceId']."";
			}
		}
		$table			= "tbl_devicename";
		$sizedataDeVice	= $sizes->selectTableRows($table,$conditionForDevice);
		$table1			= "tbl_ta2_spot_sizes";
		$sizedata1		= $sizes->selectTableRows($table1);
		$tableConsent1	= "tbl_treatment_log";
		$cols 			= " ClientID,SessionNumber,TattooInfo,Size,Price,Treatment_notes,blisters,purpura,pain,other,TattoNumber ";
		$condition 		= " where ClientID=".$_POST['clientnameid']." ORDER BY SessionNumber DESC ";
		$checkExistence = $sizes->selectTableSingleRow($tableConsent1,$condition,$cols);
		if(isset($_POST['SessionNumber'])){
			$sessionValue = $_POST['SessionNumber']+1;
		}else{	
			$sessionValue=1;
		}
		/** check tatto number*/
		$cols1="TattoNumber";
		$condition1=" where ClientID=".$_POST['clientnameid']." and TattoNumber !='' AND serviceId = ".$_POST['serviceId']." GROUP BY TattoNumber  ORDER BY TattoNumber DESC ";
		$checkExistenceTatto = $sizes->selectTableRows($tableConsent1,$condition1,$cols1);
		if($sessionValue<=10000) { ?>
			<!--script type="text/javascript" src="<?php echo $domain; ?>/js/jquery-1.9.1.js" ></script-->
			<script type="text/javascript" src="<?php echo $domain; ?>/js/fileUpload/jquery.min.js"></script>
			<script type="text/javascript" src="<?php echo $domain; ?>/js/jquery.wallform.js"></script>
			<script>
				$(document).ready(function() {
					$('.check').hide();
					$('#dropBtnAdd1').click(function() {
						$('#imageloadbutton').show();
						$('.check').hide();
					});
					$(document).on('click','.forposset', function() {
						$(this).parent().remove();
					});	
					$(".priorheading").click(function() {
						var ClientID = $(this).attr('id');					
						var TattoNumber = $(this).attr('tatto');
						var Service = 'Tattoo';					
						var dataString = 'ClientID='+ ClientID+'&TattoNumber='+TattoNumber+'&Service='+Service;
						if(ClientID!=''){
							$("#loadingImg").show();
							$.ajax({
								type: "POST",
								url: "treatments.php",
								data: dataString,
								cache: false,
								success: function(html) {
									$("#treatmentsResult").html(html).show();
									setTimeout(function(){ // then show popup, deley in .5 second
										$("#loadingImg").hide();
										loadPopup(); // function show popup 
									}, 100); // .5 second
								}
							});
						}
						return false;   
					});
					var popupStatus = 0; // set value
	
					function loadPopup() {
						if(popupStatus == 0) { // if value is 0, show popup
							//closeloading(); // fadeout loading
							$("#toPopup").fadeIn(0500); // fadein popup div
							$(".container").css("opacity", "0.7"); // css opacity, supports IE7, IE8
							//$(".container").css("position", "fixed");
							//$(".container").addClass("backgroundPopup");
							
							
							$(".container").fadeIn(0001); 
							
							popupStatus = 1; // and set value to 1
						}	
					}
					$("div.close").click(function() {
						disablePopup();  // function close pop up
					});
					function disablePopup() {
						if(popupStatus == 1) { // if value is 1, close popup
							$("#toPopup").fadeOut("normal");  
							//$(".container").fadeOut("normal");
							$(".container").css("opacity", "1");
							//$(".container").removeClass("backgroundPopup");
							popupStatus = 0;  // and set value to 0
						}
					}
					//multipal file uploading
					//file upload end
					//Fetch wavelength  accoding to deviceName
					$("#DeviceName").change(function(){ 
						var deviceID = $("#DeviceName").val();
						var requiredNumeric = $("#DeviceName option:selected").attr("id")
						var service = "tattoo";
						var dataString = 'deviceID='+ deviceID +'&requiredNumeric='+requiredNumeric+'&service='+service ;	
						if(deviceID!='') {
							$('#lodingMsg').show();
							$.ajax({
								type: "POST",
								url: "findWavelenght_ajax.php",
								data: dataString,
								cache: false,
								success: function(html) {
									setTimeout(function(){ // then show popup, deley in .5 second
										$('#lodingMsg').hide(); // function show popup 
									}, 500); // .5 second
									$("#waveLengthResult").html(html).show();
								}
							});
						}
						$("#waveLengthResult").hide();				 
						//$("#addMoreDrop").hide();
						return false;
					});
				}); 
			</script>
			<script>
				$('.remove_img').live('click', function() {	
					var id 		= $(this).attr('id');
					var ImgName = $("#img_"+id).val();
					$("#img_"+id).val("");	
					$("#img_"+id).remove();		 
					jQuery(this).parent().remove();
					var imagenlink = 'imagename='+ ImgName;	
					$.ajax({
						type: "POST",
						url: "ajax_imageUnlink.php",
						data: imagenlink,
						cache: false,
						success: function(html){}
					});
					var numItems = $('.imgupload').length;
					if(numItems < 3){
						$('.check').show();
					}else{
						$('.check').hide();
					}
					return false;
				});
			</script>
			<script> 
				var f = jQuery.noConflict();
				//f('#photoimg').die('click').live('change', function(){ 
				$('#photoimg').on('change',function(){	
					//alert("ok");
					//$("#preview").html('');
					f("#treatmentForm").ajaxForm({target: '#preview', 
						beforeSubmit:function(){ 
							console.log('v');
							f("#imageloadstatus").show();
							f("#imageloadbutton").show();
						}, 
						success:function(){ 
							console.log('z');
							var numItems = $('.imgupload').length;
							if(numItems==3) {
								$('.check').hide();
							} else {
								$('.check').show();
							}
							f("#imageloadstatus").hide();
							// f("#imageloadbutton").show();
							f("#imageloadbutton").hide();
						}, 
						error:function(){ 
							console.log('d');
							f("#imageloadstatus").show();
							f("#imageloadbutton").show();
						} 
					}).submit();
				}); 
			</script>

			<div class="card shadow mb-4 editforminformation">
				<div class="formcontentblock-ld">
					<div class="commonboldHeading"><?php echo (isset($_SESSION['businessdata']) && $_SESSION['businessdata']['BusinessName'] != '') ? $_SESSION['businessdata']['BusinessName'] : 'Fade Away'; ?> Laser Treatment Log</div>
						<form action="ajaximage.php" method="post" name="treatmentForm" id="treatmentForm" enctype="multipart/form-data">
							<input type="hidden" name="ClientID" id="ClientID" value="<?php echo $_POST['clientnameid']; ?>"/>
							<input type="hidden"  id="firstname" value="<?php echo $_POST['firstname']; ?>"/>
							<input type="hidden"  id="lastname" value="<?php echo $_POST['lastname']; ?>"/>
							<input type="hidden"  id="alreadyExist" value=""/>
							
							<h2 class="clientHeadingblk">Tattoo Info</h2>

							<div class="form-row-ld">
								<div class="onethirdblock">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label>New tattoo name</label>
											<input class="text-input-field" type="text" name="TattoNumber" id="TattoNumber"   value="" <?php if(isset($_POST['TattoNumber'])){echo "disabled=disabled";}?> autocomplete="off" />					
											<div id="TattoNumberError" class="errorblocks"></div>
											<div id="TattoNumberError1" class="errorblocks"></div>
										</div>
									</div>
								</div>
			   				<?php  
			   				if($checkExistenceTatto !=NULL){ ?>
								<div class="onethirdblock">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label>Previous Treatments</label>
											<select class="text-input-field select-option" id="existTatto" name="existTatto">
												<option value="select">Please select tattoo</option>
												<?php 
												foreach($checkExistenceTatto as $tatto){ ?>
													<option value="<?php echo $tatto['TattoNumber'];?>" <?php if(isset($_POST['TattoNumber']) && $_POST['TattoNumber']==$tatto['TattoNumber']){ echo "selected= selected"; }?>><?php echo $tatto['TattoNumber'];?></option>
												<?php
												} ?>
											</select>
										</div>
									</div>
								</div>
			   				<?php  
			   				} ?>
								<div class="onethirdblock">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<div style="text-align: center; padding: 4px 5px; display: none;" id="loadingTatto">
												<img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif">
											</div>
										</div>
									</div>
								</div>
						  </div>			   			

			   			<?php 
			   			if( isset($_POST['TattoNumber']) ) { ?>
						<div class="priortattoobtn">
							<h6 class="priorheading submit-btn" id="<?php echo $_POST['clientnameid']; ?>" tatto="<?php if(isset($_POST['TattoNumber'])){ echo $_POST['TattoNumber'];}?>" >Prior Treatments</h6>
							<h6 class="submit-btn"><a target="_blank"href="selectedtreatments.php?ClientID=<?php echo base64_encode($_POST['clientnameid']) ?>&TattoNumber=<?php echo base64_encode($_POST['TattoNumber']) ?>&action=clientdetails">All treatment photos</a></h6>
						</div>
			   			<?php } ?>
			   			<!--a href="#" class="topopup">Click Here Trigger</a-->
			   			<div id='loadingImg' style='display:none;text-align:center;padding:4px 5px;'>
			   				<img src="<?php echo $domain; ?>/images/loading.gif" alt="loading...."/>
			   			</div>
			   			<div id="toPopup">
			   				<div class="close"> </div>
			   				<div id="treatmentsResult"> </div>
			   			</div>	
			   			<div class="loader"></div>

			   			<!-- start new div for show result of treatments.php -->

			   			<!-- end new div for show result -->
						<div class="form-row-ld">
							<div class="onethirdblock">
								<div class="form-col-ld">
									<div class="inputblock-ld">
										<label id="Label1" class="user-name">Session Number:</label>
										<input class="text-input-field" type="text" name="SessionNumber" id="SessionNumber" value="<?php echo $sessionValue;?>" autocomplete="off" />										
									</div>
								</div>
							</div>
						</div>			
			   			
						<div class="form-row-ld">
							<div class="full">
								<div class="form-col-ld">
									<div class="inputblock-ld bigtextarea">
										<label>Tattoo Description And/Or Response To Last Treatment</label>
										<textarea rows="3" cols="20" name="TattooInfo" id="TattooInfo" class="text-area-field"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-row-ld">
							<div class="onethirdblock">
								<div class="form-col-ld">
									<div class="inputblock-ld">
										<label>Tattoo Size In Square Inches</label>
										<input type="text" class="text-input-field" name="Size" id="Size" value="<?php if(isset($_POST['Size']) && $_POST['Size'] != ""){ echo $_POST['Size'] ; } ?>" onkeypress="return isNumberKey(event)" maxlength="3" autocomplete="off"/>
									</div>
								</div>
							</div>
							<div class="onethirdblock upload-clientphoto">
								<div class="form-col-ld">
									<div class="inputblock-ld">
										<label>Upload Client Photo</label>
										<div class="fileblock">
											<div id='imageloadbutton'>
												<input type="text" />
												<input type="file" name="photoimg" id="photoimg" />
												<div id="photoimgError" class="errorblocks"></div>
												<div class="loadimage"></div>
											</div>
											<div class="check">
												<div class="addMoreBtn1">
													<input type="button" style="padding: 5px 20px!important;" id="dropBtnAdd1" class="submit-btn" value="Add More">
												</div>
											</div>
											<div id="my_div"></div>
											<div id='imageloadstatus' style='display:none'>
												<img src="<?php echo $domain; ?>/images/loading.gif" alt="Uploading...."/>
											</div>
											<div id="preview" class="preViewImg"></div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="commonboldHeading">Treatment Parameters</div>

						<div class="form-row-ld">
							<div class="half">
								<div class="form-col-ld">
									<div class="inputblock-ld">
										<label>Device Name</label>
										<select class="select-option" name="DeviceName" id="DeviceName">
											<?php 
											foreach($sizedataDeVice as $value){	?>
												<option id="<?php echo $value['Required']; ?>" value="<?php echo $value['deviceId']; ?>" <?php if($value['deviceId']==$locationData['deviceId']){  echo 'selected=selected' ;}?>><?php echo $value['DeviceName']; ?></option>
											<?php }  ?>
										</select>
									</div>
								</div>
							</div>
							<div class="half" id="lodingMsg" style="display:none;">
								<div class="form-col-ld">
									<div class="inputblock-ld">
										<img src="<?php echo $domain; ?>/images/loading.gif" alt="loading...."/>
									</div>
								</div>
							</div>
						</div>
						<div id="waveLengthResult"> </div>
						<div class="form-row-ld">
							<div class="full">
								<div class="form-col-ld">
									<div class="inputblock-ld bigtextarea">
										<label>Treatment Notes</label>
										<textarea class="text-area-field" rows="4" cols="20" name="Treatment_notes" id="Treatment_notes"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="commonboldHeading">Pricing</div>

						<div class="form-row-ld">
							<div class="onethirdblock">
								<div class="form-col-ld">
									<div class="inputblock-ld">
										<label>Price In $</label>
										<input class="text-input-field" type="text" name="Price" id="Price" value="" onkeypress="return isNumberKey(event)" maxlength="3" autocomplete="off"/>
									</div>
								</div>
							</div>
							<div class="onethirdblock">
								<div class="form-col-ld">
									<div class="inputblock-ld">
										<label>Pricing Calculator</label>
										<input class="text-input-field" type="text"  id="pricing_amt" autocomplete="off" onkeypress="return isNumberKey(event)" maxlength="3" autocomplete="off"/>
									</div>
								</div>
							</div>
						</div>
						<div id="prices" class="card shadow mb-4 table-main-con" style="width: 100%;"></div>
						
						<div class="commonboldHeading">Complications</div>

						<div class="form-row-ld">
							<div class="full">
								<div class="form-col-ld">
									<div class="inputblock-ld radiolabel">
										<div class="radioblocksBtns">
											<div class="radioBtn ckeckboxres">
												<input type="checkbox" name="blisters" id="blisters" value="TRUE" />
												<label for="blisters">Blisters</label>
											</div>
											<div class="radioBtn ckeckboxres">
												<input type="checkbox" name="purpura" id="purpura" value="TRUE" />
												<label for="purpura">Purpura (pin point bleeding)</label>
											</div>
											<div class="radioBtn ckeckboxres">
												<input type="checkbox" name="pain" id="pain" value="TRUE" />
												<label for="pain">Pain Tolerance Problems</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>			   			
						<div class="form-row-ld">
							<div class="full">
								<div class="form-col-ld">
									<div class="inputblock-ld bigtextarea">
										<label>Other:</label>
										<textarea  cols="20" rows="4" class="text-area-field" name="other" id="other"></textarea>
									</div>
								</div>
							</div>
						</div>
			   			<input type="hidden" name="serviceId" id="serviceId" value="<?php echo $_POST['serviceId']; ?>"/>
			   			<input type="hidden" name="Emp_First_Name" id="Emp_First_Name" value="<?=$_SESSION['First_Name']?>"/>
			   			<input type="hidden" name="Emp_Last_Name" id="Emp_Last_Name" value="<?=$_SESSION['Last_Name']?>"/>
			   			<input type="hidden" name="Location" id="Location" value="<?=$_SESSION['Location']?>"/>
			   			<input type="hidden" name="Date_of_treatment" id="Date_of_treatment" value="<?=date('Y-m-d H:i:s'); ?>"/>

						<div class="form-row-ld">
							<div class="backNextbtn">
								<button type="button" class="submit-btn u_p nextbtn" onclick="treat_client();">Submit</button>
								<div id="u_mess1" class="u_mess"></div>
							</div>
						</div>

			   		</form>
					
					</div>
			   	</div>

			   	<script>

			   		DeviceAutoLOad();
			   		function DeviceAutoLOad(){
			   			var deviceID = $("#DeviceName").val();
			   			var requiredNumeric = $("#DeviceName option:selected").attr("id");
			   			var service = "tattoo";

			   			var dataString = 'deviceID='+ deviceID +'&requiredNumeric='+requiredNumeric+'&service='+service;
			   			if(deviceID!='') {
			   				$('#lodingMsg').show();
			   				$('#loadingTatto').show();
			   				$.ajax({
			   					type: "POST",
			   					url: "findWavelenght_ajax.php",
			   					data: dataString,
			   					cache: false,
			   					success: function(html) {

									//  $("#addMoreDrop").html.show();
									// loading();							

									setTimeout(function(){ // then show popup, deley in .5 second
										$('#lodingMsg').hide(); // function show popup 
										$('#loadingTatto').hide();
									}, 2000); // .5 second
									$("#waveLengthResult").html(html).show();
								}
							});
			   				return false;
			   			}
			   		} 
			   	</script>	
			   	<?php
			   }
			   else
			   	echo "Session ended for this client!";
	}
?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#pricing_amt').keyup(function(){
				if($.isNumeric($('#pricing_amt').val()) && $('#pricing_amt').val() !="" ) {
					var str = 'pricing_amt='+$('#pricing_amt').val()+'&location=<?=$_SESSION['Location']?>'+'&Service=1';
					//var dataString = 'pageIdComing='+ pageId +'&startDate='+startDate+'&endtDate='+endtDate;
					$.ajax({
						type: "POST",
						url: "ajax_pricing.php",
						data: str,
						cache: false,
						success: function(result){
							$("#prices").show("slow");
							//$('#prices').addClass('price_back'); 	
							$("#prices").html(result);
						}
					});

				} //if is numreic
				else{
					$("#prices").hide("slow");		
				}
			});
		});
	</script>
	<style>
	.forpositionset { position: relative; }
	.forposset { background: rgba(0, 0, 0, 0) url("../images/closebox.png") no-repeat scroll 0 0; height: 30px; position: absolute; right: 17% !important; top: 0 !important; width: 30px; }
	</style>
	<?php
	function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object // with get_object_vars function
			$d = get_object_vars($d);
		}
		if (is_array($d)) {
			/* Return array converted to object Using __FUNCTION__ (Magic constant) for recursive call */
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}
	?>
