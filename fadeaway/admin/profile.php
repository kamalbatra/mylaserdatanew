<?php
$domain = $_SERVER['DOMAIN'];
include('admin_includes/header-new.php');
include("../includes/dbFunctions.php");
$profile = new dbFunctions();
$table1="tbl_business";
$table2="tbl_employees";
$condition="where BusinessID=".$_GET["id"];
$bdetails=$profile->selectTableSingleRow($table1,$condition,$cols="*");
$empdetails=$profile->selectTableSingleRow($table2,$condition,$cols="*");
$id=$_GET["id"];
?>
<script>
$(document).ready(function(){
	$("#delete-image").click(function(){
		var checkstr =  confirm('Are you sure you want to remove this logo?');
		if (checkstr == true){
			var editid = 'id='+'<?php echo $id; ?>'+'&imgname='+'<?php echo $bdetails['Logo']; ?>';
			//alert(editid);
			$.ajax({
				type: "POST",
				url: "delete-image.php",
				data: editid,
				cache: false,
				success: function(result){							
					if(result=="True"){
						//$("#image-div").hide();
						$("#logofile").show();
						$("#image-div").html('<input name="logofile" id="logofile" type="file" style="border:none;" /><br/><span style="font-size:11px;font-family: Verdana,Geneva,Tahoma,sans-serif">(Preferred size 950px X 222px)</span>');
					}
				}
			});		
		}
		else{
			return false;
		}
	});
});
</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Edit Profile</h1>
						<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
					</div>
					<?php if(isset($_GET["msg"])){
						$message = "";
						$color = "green";
						if($_GET["msg"]=="update")
							$message = "Business details updated successfully.";	
						if($_GET["msg"]=="failed"){
							$message = "Uploaded image is not valid.";	
							$color = "red";
						}
						echo "<p style='color:".$color.";font-family:Verdana,Geneva,Tahoma,sans-serif;font-size:13px;margin-bottom:13px;margin-top:0px;'>".$message."</p>"; 
					}
					if(isset($_GET["id"]) && $_GET["id"] != "") {
						echo "";
					}	
					?>					
					<div class="card shadow mb-4 editforminformation">
						<div class="bussiness-searchblock no-searchbox">
							<div class="search-btn">
								<a class="empLinks addnew-button" href="manage_employee"><button class="addnewbtn">Add New </button></a>
							</div>
						</div>
						<div class="formcontentblock-ld">
							<form action="updateprofile.php" method="post" id="profile-form" name="profile-form" enctype="multipart/form-data">
			<input type='hidden' name='updatedId' value="<?php echo $id; ?>" />
		<input type='hidden' name='empId' value="<?php echo $empdetails['Emp_ID']; ?>" />
								<div class="formClientBlock">
									<div class="formcontentblock-ld">
										<div class="form-row-ld">
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
														<label for="logo" class="user-name">Business Logo:</label>
														<?php
														if($bdetails['Logo'] != "" && file_exists("Business-Uploads/$bdetails[BusinessID]/$bdetails[Logo]")){
														?>
															<div id="image-div">
																<img src="Business-Uploads/<?php echo $id; ?>/<?php echo $bdetails['Logo']; ?>" style="height:100px;width:400px;" />
																<input type="button" value="Remove Logo" name="delete-image" id="delete-image" class="remove-logo"/>
															</div>
														<?php 
														} else { ?>
															<input name="logofile" id="logofile" type="file" style="border:none;" /><br/>
															<span style="font-size:11px;font-family: Verdana,Geneva,Tahoma,sans-serif">(Preferred size 950px X 222px)</span>
														<?php 
														} ?>
													</div>
												</div>
											</div>
										</div>
										<div class="form-row-ld">
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
														<label for="bname" class="user-name">Business Name:</label>
														<input class="text-input-field" name="bname" id="bname" type="text" value="<?=$bdetails['BusinessName'];?>" />
													</div>
												</div>
											</div>
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
														<label for="username" class="user-name">First Name:</label>
														<input class="text-input-field" name="fname" id="fname" type="text" value="<?=$empdetails['First_Name'];?>" <?php echo ($status == 'edit') ? 'disabled' : '' ?> />
													</div>
												</div>
											</div>
										</div>
										<div class="form-row-ld">
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
														<label for="Address1" class="user-name">Last Name:</label>
														<input class="text-input-field" name="lname" id="lname" type="text" value="<?=$empdetails['Last_Name'];?>" <?php echo ($status == 'edit') ? 'disabled' : '' ?> />
													</div>
												</div>
											</div>
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
														<label for="Address2" class="user-name">Contact No:</label>
														<input class="text-input-field" name="contact" id="contact" type="text" value="<?=$bdetails['Contact'];?>" />
													</div>
												</div>
											</div>
										</div>
										<div class="form-row-ld">
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
					<label for="Address1" class="user-name">Address:</label>
					<textarea rows="5" class="text-area-field" name="Address" id="Address"><?=$bdetails['Address'];?></textarea>
													</div>
												</div>
											</div>
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
					<label for="Address2" class="user-name">City:</label>
					<input class="text-input-field" name="City" id="City" type="text" value="<?=$bdetails['City'];?>" />
													</div>
												</div>
											</div>
										</div>
										<div class="form-row-ld">
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
					<label for="Address1" class="user-name">State:</label>
					<input class="text-input-field" name="State" id="State" type="text" value="<?=$bdetails['State'];?>"/>
													</div>
												</div>
											</div>
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
						<label for="Address2" class="user-name">Country:</label>
					<input class="text-input-field" name="Country" id="Country" type="text" value="<?=$bdetails['Country'];?>" />
													</div>
												</div>
											</div>
										</div>
										<div class="form-row-ld">
											<div class="half">
												<div class="form-col-ld">
													<div class="inputblock-ld">
					<label for="Address1" class="user-name">Liability Release:</label>
					<textarea rows="5" class="text-area-field" name="LiabilityRelease" id="LiabilityRelease"><?=$bdetails['LiabilityRelease'];?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="form-row-ld">
											<div class="backNextbtn">
												<button type="submit" id="update-btn" value="Update" class="submit-btn nextbtn" style="float:left;">Update</button>
											</div>
										</div>
									</div>
									
								</div>
						
							</form>
				
						</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
<script>
$(document).ready(function(){
	$("#profile-form").validate({
		rules:{
			bname:"required"
		},
		messages:{
			bname:"Please enter business name"
		}
	});
});
</script>
