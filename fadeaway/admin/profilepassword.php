<?php
	//~ error_reporting(0);
	session_start();
	
	$domain = $_SERVER['DOMAIN'];
	include('admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$empid=$_SESSION["id"];
	$password = new dbFunctions();
	if(isset($_POST["update-btn"])) {
		$table="tbl_employees";
		$condition="where Emp_ID=".$empid;
		$row=$password->selectTableSingleRow($table,$condition,$cols="*");
		//echo $row["Password"];
		$oldpass=md5($_POST["oldpass"]);
		if($oldpass != $row["Password"]) {
			$msg="failed";
		}
		else {
			$newpass=md5($_POST["newpass"]);
			$data["Emp_ID"]=$empid;
			$data["Password"]=$newpass;
			$result=$password->update_spot($table,$data);
			$msg="update";
		}
	}	
?>	

	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Change Password</h1>
				</div>	
				<div class="card shadow mb-4 editforminformation">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="profile.php?id=<?php echo $_SESSION['BusinessID'] ?>" class="submit-btn"><button class="addnewbtn">Update Profile </button></a>
						</div>
					</div>
					<form action="" method="post" id="password-form" name="password-form" enctype="multipart/form-data">
					<?php
						if(isset($msg)) {
							$message = "";
							$color = "green";
							if($msg=="update")
								$message = "Password updated successfully.";	
							if($msg=="failed") {
								$message = "Old password is incorrect";	
								$color = "red";
							}
							echo "<p style='color:".$color.";font-family:Verdana,Geneva,Tahoma,sans-serif;font-size:13px;margin-bottom:13px;margin-top:0px;'>".$message."</p>"; 
						}
						if(isset($_GET["id"]) && $_GET["id"] != "")
						echo "";
					?>						
						<div class="formcontentblock-ld">
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label for="bname" class="user-name">Old Password:</label>
											<input class="text-input-field" name="oldpass" id="oldpass" type="password" />
										</div>
									</div>
								</div>
							</div>
							
							<div class="form-row-ld">
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label for="bname" class="user-name">New Password:</label>
											<input class="text-input-field" name="newpass" id="newpass" type="password"/>
										</div>
									</div>
								</div>
								<div class="half">
									<div class="form-col-ld">
										<div class="inputblock-ld">
											<label for="bname" class="user-name">Confirm Password:</label>
											<input class="text-input-field" name="confirmpass" id="confirmpass" type="password"/>
										</div>
									</div>
								</div>
							</div>
							
							<div class="form-row-ld">
								<div class="backNextbtn">
									<button type="submit" class="submit-btn" value="Update" name="update-btn" id="update-btn"/>SUBMIT</button>
								</div>
							</div>

						</div>

					</form>
                    

				</div>
				
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<script>
		$(document).ready(function(){
			$("#password-form").validate({
				errorClass: 'errorblocks',
				errorElement: 'div',				
				rules:{
					oldpass:"required",
					newpass:"required",
					confirmpass:{
						required:true,
						equalTo:"#newpass"
					}
				},
				messages:{
					oldpass:"Please enter current password",
					newpass:"Please enter new password",
					confirmpass:{
						required:"Please re-enter new password",
						equalTo:"Password are not same"
					}
				}
			});
		});
	</script>		
	<?php	
	include('admin_includes/footer-new.php');	
	?>
