<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	if($_SESSION['Usertype']!="Admin"){ 	
		echo '<script>window.location.assign("clientreport.php")</script>';		
	}
	//$domain=$_SERVER['DOMAIN'];
	
?>	
    <!--script type="text/javascript" src="../js/datepicker/jquery.js"></script>   
    <script type="text/javascript" src="../js/datepicker/datepicker.js"></script>
    <script type="text/javascript" src="../js/datepicker/eye.js"></script--> 
    <script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.js"></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.plus.js"></script>
	<script type="text/javascript" src="<?php echo $domain; ?>/js/newdatepicker/jquery.calendars.picker.js"></script>  
<?php    
	$tbl_employees	= "tbl_employees";
	$condition = " WHERE Technician='TRUE' AND BusinessID=".$_SESSION["BusinessID"]." ORDER BY Emp_ID DESC ";
	$cols="Emp_ID,First_Name,Last_Name,Emp_email,Location";
	$empData = $empInfo->selectTableRows($tbl_employees,$condition,$cols);
	//print_r($empData);
?>
<script>
	$(document).ready(function() {
		$('#revenusReportBtn').click(function() {
			$('#lodingMsgReport').show(); 
			var str = $( "#revenuesReportForm" ).serialize();
			$.ajax({
				type: "POST",
				url: "ajax_RevenuesData.php",
				data: str,
				cache: false,
				success: function(result) {
					alert(result);
					if(result ==0) {
						$('#lodingMsgReport').hide();
						$("#clientReportResult").css('padding-top','13px');
						$("#clientReportResult").css('text-align','center');						 
					    $("#clientReportResult").html("No record found.");				
					} else {
						$('#lodingMsgReport').hide(); 
					    $("#clientReportResult").html(result);
					}					 
				}
			});
		});
		/*
		$('.inputDate').DatePicker({
			format:'m/d/Y',
			date: new Date(),
			current: new Date(),
			starts: 1,
			position: 'right',
			onBeforeShow: function(){
				$('#inputDate').DatePickerSetDate($('#inputDate').val(), true);
			},
			onChange: function(formated, dates){
				$('#inputDate').val(formated);
				if ($('#closeOnSelect input').attr('checked')) {
					$('#inputDate').DatePickerHide();
				}
			}
		});*/		
		/*	
		$('.inputEnddate').DatePicker({
			format:'m/d/Y',
			date: $('#inputEnddate').val(),
			current: $('#inputEnddate').val(),
			starts: 1,
			position: 'right',
			onBeforeShow: function(){
				$('#inputEnddate').DatePickerSetDate($('#inputEnddate').val(), true);
			},
			onChange: function(formated, dates){
				$('#inputEnddate').val(formated);
				if ($('#closeOnSelectEnd input').attr('checked')) {
					$('#inputEnddate').DatePickerHide();
				}
			}
		});*/
	});
</script>
<script type="text/javascript">
	$(function() {
		$('#inputDate').calendarsPicker({format:'Y/m/d'});
		$('#inputEnddate').calendarsPicker({format:'Y/m/d'});
	});
</script>
<script>
	$(function() {			
		$('#technicianName').change(function() {
			var technicianName = $('#technicianName').val();
			if(technicianName !=""){
				$('#viewByAll').removeAttr('checked');
			}				   
		})
		$('#viewByAll').change(function() {			   
			if($(this).is(':checked')) {				 
				$('#technicianName option:first-child').attr("selected", "selected");
				// $("#technicianName").attr("disabled", "disabled");		     				   
			} else { 			    
				//$("#technicianName").removeAttr("disabled"); 
			}  
		});
	});
</script>
<?php
	/*
	include ("libchart/classes/libchart.php");
	$chart = new PieChart();
	$dataSet = new XYDataSet();
	$dataSet->addPoint(new Point("Mozilla Firefox (80)", 100));
	$dataSet->addPoint(new Point("Konqueror (75)", 75));
	$dataSet->addPoint(new Point("Opera (50)", 50));
	$dataSet->addPoint(new Point("Safari (37)", 37));
	$dataSet->addPoint(new Point("Dillo (37)", 37));
	$dataSet->addPoint(new Point("Other (72)", 70));
	$dataSet->addPoint(new Point("Test (30)", 30));
	$chart->setDataSet($dataSet);
	$chart->setTitle("User agents for www.example.com");
	$chart->render("generated/demo3.png");
	*/
?>
<div class="form-container" id="top">
	<div class="heading-container">
		<h1 class="heading empHeadReport" style="width:45%">Tattoo Revenue Report</h1>
		<?php
		$serviceIds=$_SESSION['businessdata']['serviceId'];
		$serviceId=explode(',',$serviceIds);
	
		
		if(in_array('2',$serviceId))
		{
		 
		}
		
		if($_SESSION['Usertype']=="Admin") { 
			if(in_array('2',$serviceId))
			{
			?>
			<div class="addNewReport" style="width:25%"><a class="empLinks" href="revenuesreport-hair.php"class="submit-btn"> Hair Revenue Report</a></div>
			 <?php 
			} ?>
			<div class="addNewReport" style="width:17%"><a class="empLinks" href="clientreport.php"class="submit-btn"> Client Report</a></div>
			<div class="addNewReport" style="width:13%"><a class="empLinks" href="marketing.php" class="submit-btn"> Marketing </a></div>
		<?php
		} ?>
	</div>	<!--heading-container-->
	<div class="user-entry-block">
		<div class="row">
			<form id="revenuesReportForm" name="revenuesReportForm" method="post" action="revenuesreport-1.php#top">
				<div class="span6">
					<label id="" class="user-name">Start date: (MM/DD/YYYY)</label>
					<?php
					$starDate =  date('m/d/Y');
					$newStarDate = date('m/d/Y',strtotime($starDate . "-90 days"));
					?>
				    <input class="inputDate text-input-field" id="inputDate" name="inputDate" value="<?php if(isset($_POST['inputDate'])){ echo $_POST['inputDate'];}else{echo $newStarDate; }?>" onkeypress="return false;" />
					<label id="closeOnSelect" style="display:none;"><input type="checkbox" checked /> </label>
				</div>
				<div class="span6 last">
					<label id="" class="user-name">End date: (MM/DD/YYYY)</label>
				    <input class="inputEnddate text-input-field" id="inputEnddate" name="inputEnddate" value="<?php if(isset($_POST['inputEnddate'])){ echo $_POST['inputEnddate'];}else{echo date('m/d/Y'); }?>" onkeypress="return false;" />
					<label id="closeOnSelectEnd" style="display:none;"><input type="checkbox" checked /> </label>
				</div>		
				<div class="span6">
					<label id="" class="user-name">Technician</label>
				    <select id="technicianName" name="technicianName" class="text-input-field select-option">
						<option value="">Please select technician</option>
						<?php foreach($empData as $emp){?>
						<option value="<?php echo $emp['First_Name'] ?>,<?php echo $emp['Location'] ?>" 
						<?php if(isset($_POST['technicianName']) && $_POST['technicianName']==  $emp['First_Name'].",".$emp['Location']){ echo "selected=selected" ;}?>>
						<?php echo $emp['First_Name']." ".$emp['Last_Name']; ?></option>						
						<?php } ?>					
					</select>
				</div>				
				<div class="span6 last">
					<label id="" class="user-name">&nbsp;</label>
					<input type="checkbox"  class="largerCheckbox" name="viewByAll" id="viewByAll" <?php if(isset($_POST['viewByAll'])){ echo "checked";} ?> />
					<label for="viewByAll" id="" class="user-name user-name-small">View by all technicians</label>				    
				</div>
				<div class="span6">
					<label id="" class="user-name"></label>					
					<input type="submit" class="submit-btn" id="revenusReportBtn--" value="Submit" />
					<span id="lodingMsgReport" style="display:none;"><img src="<?php echo $domain; ?>/images/loading.gif" alt="loading...."/></span>					
				</div>
			</form>	
		</div><!--@row-->
		<?php   
		//print_r($_POST);
		if(isset($_POST['technicianName']) && $_POST['technicianName'] !=NULL) {
			if(strtotime($_POST['inputDate']) <= strtotime($_POST['inputEnddate']) ){
				$startDate =  $_POST['inputDate'];
				$newStartDate = date("Y-m-d H:i:s", strtotime($startDate));
				$endtDate  =  $_POST['inputEnddate'];
				$newEndtDate = date("Y-m-d H:i:s", strtotime($endtDate.' +1 day'));
				$technicianName =  explode(",",$_POST['technicianName']);
				$table = "tbl_treatment_log as t";
				$clienttable = "tbl_clients as c";
				$cols="c.ClientID ,AES_DECRYPT(c.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(c.LastName, '".SALT."') AS LastName,t.Location,t.Size,t.Price,COUNT(*) as number,AVG(t.Price) as avgrage";
				$condition = "c.ClientID=t.ClientID WHERE c.BusinessID=".$_SESSION["BusinessID"]." AND t.Date_of_treatment between '".$newStartDate."' and '".$newEndtDate."' and t.Emp_First_Name = '".$technicianName[0]."' and t.Size !='' GROUP BY t.Size ";
				//$RevenuesReportData= $empInfo->selectTableRows($table,$condition,$cols);					 
				$RevenuesReportData= $empInfo->selectTableJoin($table,$clienttable,$join="",$condition,$cols);
				// print_r($RevenuesReportData);
				if($RevenuesReportData !=NULL) {
					$condLoc = " WHERE LocationName = (SELECT `LocationName` FROM `tbl_manage_location` WHERE `ManageLocationId` = $technicianName[1])";
					$colLoc = "ManageLocationId";
					$all_location_ids = $empInfo->selectTableRows('tbl_manage_location',$condLoc,$colLoc);
					$allLoc = array();
					foreach($all_location_ids AS $alllocation){
						array_push($allLoc, $alllocation['ManageLocationId']);
					}
					$allLoc = implode(",", $allLoc);
					
					/*** Count total tattoo size from pricing table**/
					$tbl_pricing_table = "tbl_pricing_table";	
					$condition1 = " WHERE `Area` IS NULL AND `Location` IN ( $allLoc ) AND `BusinessID` =$_SESSION[BusinessID] order by pid desc";
				
					$totalSize = $empInfo->totalNumRows($tbl_pricing_table,$condition1);
					/*** end Count total tattoo size from pricing table**/
					if($totalSize ==0) {
						$totalSize=200;
					}	 
					include ("libchart/classes/libchart.php");
					/*temp*/
					$totalSize1 =$totalSize/2 ;
					if($totalSize <=100) {
						$height  = 450;
						$Width= 1800;    
						$height1  = 500;
						$Width1= 7000;	
					} else {
						$height  = 400;
						$Width= $totalSize*30;
						$height1  = 500;
						$Width1= $totalSize*90;
					}	 
					/*$height1  = $totalSize1*5;
					$Width1= $totalSize1*20;*/
					/*temp*/						 
					$vchart = new VerticalBarChart($Width,$height);
					$vchart2 = new VerticalBarChart($Width1,$height1);												 
					/**** Vartical  chart **/
					$vdataSet = new XYDataSet();
					/*** below graph*/
					$serie1 = new XYDataSet();
					$serie2 = new XYDataSet();
					/*foreach($RevenuesReportData as $revenuesData){
						$dataSet->addPoint(new Point("Size of tattoo (".$revenuesData['Size']."),  Average price (".$revenuesData['avgrage'].")", $revenuesData['avgrage']));
						$vdataSet->addPoint(new Point("Size(".$revenuesData['Size'].")", $revenuesData['number']));						  
					}*/
					for($i=0 ; $i<=$totalSize; $i++ ) {							  
						$table1 = "tbl_treatment_log as t";
						$clienttable = "tbl_clients as c";
						$cols1="*";
						$cols2="c.ClientID,AES_DECRYPT(c.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(c.LastName, '".SALT."') AS LastName,t.Location,t.Size,t.Price,COUNT(*) as number,AVG(t.Price) as avgrage";
						$conditionNum = "c.ClientID=t.ClientID WHERE c.BusinessID=".$_SESSION["BusinessID"]." AND `Area` IS NULL AND t.Size=".$i." and t.Emp_First_Name = '".$technicianName[0]."'";
						//$Revenues= $empInfo->totalNumRows($table1,$conditionNum,$cols1);
						$Revenues= $empInfo->joinTotalNumRows($table1,$clienttable,$join="",$conditionNum,$cols1);
						//$averagePrice= $empInfo->selectTableRows($table1,$conditionNum,$cols2);
						$averagePrice= $empInfo->selectTableJoin($table1,$clienttable,$join="",$conditionNum,$cols2);
						$vdataSet->addPoint(new Point($i, $Revenues));
						$table2 = "tbl_pricing_table";
						$cols2="*";						     
						$condition2 = "WHERE Size=".$i." and BusinessID=".$_SESSION["BusinessID"]." order by pid desc";
						$pricingData= $empInfo->selectTableSingleRow($table2,$condition2,$cols2);						        
						$average = round($averagePrice[0]['avgrage'],1);
						$rec_Price = round($pricingData['Rec_Price'],1);						    
						$serie1->addPoint(new Point($i,  $average));
						$serie2->addPoint(new Point($i,  $rec_Price));
					}	 
					$vchart->setDataSet($vdataSet);
					$vchart->setTitle("Tattoo size and number of tatto according to technician.");											
					$vchart->render("generated/demo1.png");
					
					$vdataSet2 = new XYSeriesDataSet();
					$vdataSet2->addSerie("Average price of tattoo", $serie1);
					$vdataSet2->addSerie("Rec price of tattoo", $serie2);						
					$vchart2->setDataSet($vdataSet2);
					$vchart2->getPlot()->setGraphCaptionRatio(0.65);
					$vchart2->setTitle("Tattoo size and number of tatto according to technician.");											
					$vchart2->render("generated/demo12.png");	                    
					/**** End Vertcal chart*/    
	                //echo '<div class="Row" ><h5>1. Represent number of tattoo in y-axis and size of tattoo in x-axis.</h5></div>' ;
					echo '<div id="vert" style="filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);">
						<h5>Number of tattoo.</h5>
						</div>';
					echo '<div id="vertCover" style="width:100%;overflow-x:scroll;"> 
						<div class="row text-align" style="width:'.$Width.'px;"><img alt="Vertical bars chart" src="generated/demo1.png" style="border: 0px solid gray;"/></div>
						<div id="" class="x-axisBottom" style="text-align: center;"> <h5>Tattoo size in square inch.</h5></div>
						</div>'; 
					/***************** Next  bar graph **********/                        
					echo '<div class="Row" style="margin-top:50px;"><h5>2. Represent average price of tattoo in y-axis and size of tattoo in x-axis.</h5></div>' ;
					echo '<div id="vert" style="filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);">
						<h5>Average price of tattoo.</h5>
						</div>';
					echo '<div id="vertCover" style="width:100%;overflow-x:scroll;"> 
						<div class="row text-align" style="width:'.$Width1.'px;"><img alt="Vertical bars chart" src="generated/demo12.png" style="border: 0px solid gray;"/></div>
						<div id="" class="x-axisBottom" style="text-align: center;"> <h5>Tattoo size in square inch.</h5></div>
						</div>';    
				   /***************** Next  bar graph **********/                                 
					// echo '<div class="row text-align"> <img alt="Horizontal bars chart"  src="generated/demo2.png" style="border: 1px solid gray;"/> </div>';    
					//SELECT ClientID ,Client_First_Name, Client_Last_Name,Date_of_treatment,SessionNumber,Size,Price, COUNT(*) as number ,AVG(Price) as avgrage FROM tbl_treatment_log WHERE   Date_of_treatment between '2011-04-15 00:00:00' and '2014-04-21 00:00:00'  and `Emp_First_Name` = 'pratibha'  GROUP BY Size
				} else {
					echo "<div class='row'><span class='error' style='float:left;padding:10px 0 0;text-align:center;width:100%;'>No record found.</span></div>";
				}	 
			} else { 
				echo "<span class='error' style='float:left;padding:10px 0 0;text-align:center;width:100%;'>Please select correct date!</span>";
			}
		}//If isset $_POST
	?>		 
	<!--- View By all technician -----------> 
	<?php if(isset($_POST['viewByAll'])) {
		if(strtotime($_POST['inputDate']) <= strtotime($_POST['inputEnddate']) ){
			$startDate =  $_POST['inputDate'];	 
			$newStartDate = date("Y-m-d H:i:s", strtotime($startDate));
			$endtDate  =  $_POST['inputEnddate'];
			$newEndtDate = date("Y-m-d H:i:s", strtotime($endtDate.'+1 day'));
			$technicianName =  explode(",",$_POST['technicianName']);
			$table = "tbl_treatment_log as t";
			$clienttable = "tbl_clients as c";
			$cols="c.ClientID,AES_DECRYPT(c.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(c.LastName, '".SALT."') AS LastName,t.Location,t.Size,t.Price,COUNT(*) as number,AVG(t.Price) as avgrage";
			$condition = "c.ClientID=t.ClientID WHERE c.BusinessID=".$_SESSION["BusinessID"]." AND t.serviceId = 1 AND t.Date_of_treatment between '".$newStartDate."' and '".$newEndtDate."' and t.Size !='' GROUP BY t.Size ";
			//$RevenuesReportData= $empInfo->selectTableRows($table,$condition,$cols);
			$RevenuesReportData= $empInfo->selectTableJoin($table,$clienttable,$join="",$condition,$cols);
			if($RevenuesReportData !=NULL) {
				$condLoc = " WHERE LocationName = (SELECT `LocationName` FROM `tbl_manage_location` WHERE `ManageLocationId` = $_SESSION[Location])";
				$colLoc = "ManageLocationId";
				$all_location_ids = $empInfo->selectTableRows('tbl_manage_location',$condLoc,$colLoc);
				$allLoc = array();
				foreach($all_location_ids AS $alllocation){
					array_push($allLoc, $alllocation['ManageLocationId']);
				}
				$allLoc = implode(",", $allLoc);
				
				/*** Count total tattoo size from pricing table**/
				$tbl_pricing_table = "tbl_pricing_table";	
				$condition1 = " WHERE `Area` IS NULL AND `Location` IN ( $allLoc ) AND `BusinessID` =$_SESSION[BusinessID] order by pid desc";
				$totalSize = $empInfo->totalNumRows($tbl_pricing_table,$condition1);					 
				if($totalSize ==0) {
					$totalSize=200;
				}
				/*** end Count total tattoo size from pricing table**/
				include ("libchart/classes/libchart.php");
				/*temp*/
				$totalSize1 =$totalSize/2 ;
				if($totalSize <=100) {
					$height  = 450;
					$Width= 1800;
					$height1  = 500;
					$Width1= 7000;	
				} else {
					$height  = 400;
					$Width= $totalSize*30;
					$height1  = 500;
					$Width1= $totalSize*70;
				}				
				/*temp*/ 
				$vchart = new VerticalBarChart($Width,$height);
				$vchart2 = new VerticalBarChart($Width1,$height1);												 
				/**** Vartical  chart **/
				$vdataSet = new XYDataSet();
				/*** below graph*/
				$serie1 = new XYDataSet();
				$serie2 = new XYDataSet();						 
				/*foreach($RevenuesReportData as $revenuesData) {
					$dataSet->addPoint(new Point("Size of tattoo (".$revenuesData['Size']."),  Average price (".$revenuesData['avgrage'].")", $revenuesData['avgrage']));
					$vdataSet->addPoint(new Point("Size(".$revenuesData['Size'].")", $revenuesData['number']));
				}*/
				for($i =0 ; $i<$totalSize; $i++ ) {
					$table1 = "tbl_treatment_log as t";
					$table2 = "tbl_clients as c";
					$cols1="*";
					$cols2="c.ClientID,AES_DECRYPT(c.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(c.LastName, '".SALT."') AS LastName,t.Location,t.Size,t.Price,COUNT(*) as number,AVG(t.Price) as avgrage";
					$conditionNum = "c.ClientID=t.ClientID WHERE c.BusinessID=".$_SESSION["BusinessID"]." AND `Area` IS NULL AND t.Size=".$i." ";
					//$Revenues= $empInfo->totalNumRows($table1,$conditionNum,$cols1);
					$Revenues= $empInfo->joinTotalNumRows($table1,$table2,$join="",$conditionNum,$cols1);
					//$averagePrice= $empInfo->selectTableRows($table1,$conditionNum,$cols2);
					$averagePrice= $empInfo->selectTableJoin($table1,$table2,$join="",$conditionNum,$cols2);
					$vdataSet->addPoint(new Point($i, $Revenues));
					
					$table2 = "tbl_pricing_table";
					$cols2="*";						     
					$condition2 = "WHERE Size=".$i." and BusinessID=".$_SESSION["BusinessID"]." order by pid desc";
					$pricingData= $empInfo->selectTableSingleRow($table2,$condition2,$cols2);
					$average = round($averagePrice[0]['avgrage'],1);
					$rec_Price = round($pricingData['Rec_Price'],1);
					$serie1->addPoint(new Point($i, $average));
					$serie2->addPoint(new Point($i, $rec_Price));
				}		 
				$vchart->setDataSet($vdataSet);
				$vchart->setTitle("Tattoo size and number of tatto according to technician.");
				
				$vchart->render("generated/demo1.png");
				$vdataSet2 = new XYSeriesDataSet();
				$vdataSet2->addSerie("Average price of tattoo", $serie1);
				$vdataSet2->addSerie("Rec price of tattoo", $serie2);						
				$vchart2->setDataSet($vdataSet2);
				$vchart2->getPlot()->setGraphCaptionRatio(0.65);
				$vchart2->setTitle("Tattoo size and number of tatto according to technician.");											
				$vchart2->render("generated/demo12.png");	                    
				/**** End Vertcal chart*/
				echo '<div id="vert" style="filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);">
					<h5>Number of tattoo.</h5>
					</div>';
				echo '<div id="vertCover" style="width:100%;overflow-x:scroll;"> 
					<div class="row text-align" style="width:'.$Width.'px"><img alt="Vertical bars chart" src="generated/demo1.png" style="border: 0px solid gray;"/></div>
					<div id="" class="x-axisBottom" style="text-align: center;"> <h5>Tattoo size in square inch.</h5></div>
					</div>'; 
				/***************** Next  bar graph **********/                        
				echo '<div id="vert" style="filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);">
					<h5>Average price of tattoo.</h5>
					</div>';
				echo '<div id="vertCover" style="width:100%;overflow-x:scroll;"> 
					<div class="row text-align" style="width:'.$Width1.'px"><img alt="Vertical bars chart" src="generated/demo12.png" style="border: 0px solid gray;"/></div>
					<div id="" class="x-axisBottom" style="text-align: center;"> <h5>Tattoo size in square inch.</h5></div>
					</div>';    
				/*****************End Next  bar graph **********/     
			} else {
				echo "<div class='row'><span style='float:left;padding:10px 0 0;text-align:center;width:100%;'>No record found.</span></div>";
			}		 
		} else {
			echo "<span class='error' style='float:left;padding:10px 0 0;text-align:center;width:100%;'>Please select correct date!</span>";
		}		
	}
?>
	<!--- End View By all technician -----------> 
	<div id="clientReportResult"></div>
	</div><!--@user-entry-block-->
</div><!--@form-container-->
</div><!-- container-->
<script>
	/*
	clintReportLoad();
	function clintReportLoad(){
		var dataString = 'clientreport=1';	
		$('#lodingMsgReport').show(); 
		$.ajax({
			type: "POST",
			url: "ajax_clientReport.php",
			data: dataString,
			cache: false,
			success: function(html)	{
				setTimeout(function(){ 
					$('#lodingMsgReport').hide(); // function show popup 
				}, 2000); // .5 second
				$("#clientReportResult").html(html).show();
			}
		});
		return false;
	} */
</script>
<?php
	include('admin_includes/footer.php');
?>
