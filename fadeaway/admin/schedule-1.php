<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();

	$table = "tbl_employees";
	$condition = " where Emp_ID = ".base64_decode($_GET['empid']);
	$cols = "dailySchdeule";
	$editinfo = $empInfo->selectTableSingleRowNew($table,$condition,$cols);
	
	$table1 = "tbl_employees_leave";
	$condition1 = "where employeeID=".base64_decode($_GET['empid']);
	$cols1 = "*";
	$employeeleavedata = $empInfo->selectTableRowsNew($table1,$condition1,$cols1);
	
	$calanderdata = array();
	if( !empty($employeeleavedata) ){
		for( $i=0;$i< count($employeeleavedata); $i++){
			if( $employeeleavedata[$i]['status'] == 0 ){
				$title = 'Leave';
				$color = '#ff0000';
			} else if( $employeeleavedata[$i]['status'] == 1 ){
				$title = 'Away';
				$color = '#257E4A';
			}
			$calanderdata[$i]['id'] = $employeeleavedata[$i]['id'];
			$calanderdata[$i]['status'] = $employeeleavedata[$i]['status'];
			$calanderdata[$i]['title'] = $title;
			$calanderdata[$i]['color'] = $color;
			$calanderdata[$i]['description'] = $employeeleavedata[$i]['reason'];
			$calanderdata[$i]['start'] = $employeeleavedata[$i]['startDate'];
			$calanderdata[$i]['end'] = $employeeleavedata[$i]['endDate'];
			
			$calanderdata[$i]['startDate'] = $employeeleavedata[$i]['startDate'];
			$calanderdata[$i]['endDate'] = $employeeleavedata[$i]['endDate'];
			$date1 = date_create($employeeleavedata[$i]['startDate']);
			$date2 = date_create($employeeleavedata[$i]['endDate']);
			if( $employeeleavedata[$i]['status'] == 0 ){
				$dateDifference = date_diff($date1, $date2)->format('%d');
			} else if( $employeeleavedata[$i]['status'] == 1 ){
				$dateDifference = date_diff($date1, $date2)->format('%h');
			} 
			$calanderdata[$i]['dateDifference'] = $dateDifference;
		}
	} 
	$calanderdata = JSON_encode($calanderdata);
?>	
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/core/main.css' rel='stylesheet' />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/timegrid/main.css' rel='stylesheet' />
	<link href='<?php echo $domain; ?>/admin/fullcalendar/packages/list/main.css' rel='stylesheet' />
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/core/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/interaction/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/daygrid/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/timegrid/main.js'></script>
	<script src='<?php echo $domain; ?>/admin/fullcalendar/packages/list/main.js'></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var calendarEl = document.getElementById('calendar');
			var timer;
			var calendar = new FullCalendar.Calendar(calendarEl, {
				plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
				defaultView: 'dayGridMonth',
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
				},
				defaultDate:  '<?php echo date("Y-m-d"); ?>',
				navLinks: true,
				editable: true,
				events: <?php echo $calanderdata; ?>,
				eventClick: function(info) {
					var eventid = info.event.id;
					var eventtitle = info.event.title;
					var eventdesc = info.event.extendedProps.description;
					var eventstatus = info.event.extendedProps.status;
					var eventstartDate = info.event.extendedProps.startDate;
					var dateDifference = info.event.extendedProps.dateDifference;
					if ( eventstatus == 0 ) {
						$('.editnumbers').hide();
						$('#editnumdays').show();
						$('#editnoofdays').val(dateDifference);
					} else if ( eventstatus == 1 ) {
						$('.editnumbers').hide();
						$('#editnumhours').show();
						$('#editnoofhours').val(dateDifference);
					}
					$('#editeventid').val(eventid);
					$('#deleteevent').attr('idattr',eventid);
					$('#editeventdesc').val(eventdesc);
					$('#editeventstatus').val(eventstatus);
					$('#editstartDate').val(eventstartDate);
					$('#editModal').show();
				},
				/*
				eventMouseEnter: function(info) {
					timer = setTimeout(function() {
						$("#showModal").html("<div class='primary_heading'><span style='font-weight:bold; font-size:16px;'>Event Description</span><span style='font-weight:bold; font-size:16px;margin-left:10px'>"+info.event.extendedProps.description+"</span></div>");
						$("#showModal").fadeIn();
					}, 1000); 
				},
				eventMouseLeave : function(info) {
					setTimeout(function() { 
						clearTimeout(timer);
						$("#showModal").fadeOut();
					},7000);
				},
				*/
				dateClick: function(info) {
					var date1 = info.dateStr;
					date1 = date1.split('+');
					$('#startDate').val(date1[0]);
					$('#myModal').show();
				},
			});
			calendar.render();
		});

		$(document).ready(function(){
			
			$('#close').click(function(){
				$('#leaveaddform')[0].reset();
				$('#myModal').hide();
			});		

			$('#close1').click(function(){
				$('#leaveeditform')[0].reset();
				$('#editModal').hide();
			});		

			$('#deleteevent').click(function(){
				var str = 'id='+$(this).attr('idattr')+'&formname=leavedelete';
				$.ajax({
					type: "POST",
					url: "ajax_newform.php",
					data: str,
					cache: false,
					success: function(result){
						if(result == 1){
							setTimeout(function() {
								location.reload();
							}, 1000);
						}
					}
				});				
			});		
		
		});

		jQuery(document).ready(function(){
			
			/* ********* Add form ********* */
			jQuery("#leaveaddform").validate({
				ignore: [],
				rules: {                 
					status:{
						required: true,
					},                                
					reason:{
						required: true,
					},                                
				},                
				messages: {                    
					status:{
						required: "Please select a status.",
					},                                
					reason:{
						required: "Please enter a reason.",
					},                                
				},
				submitHandler: function(form){
					var str = $("#leaveaddform").serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								setTimeout(function() {
									location.reload();
								}, 2000);
							}
						}
					}); 
				}
			});
			$("#status").on('change', function() {
				var statusval = $(this).val();
				if ( statusval == 0 ) {
					$('#numhours').hide();
					$('#numdays').removeClass('numbers');
					$('#numdays').show();
					
				} else if ( statusval == 1 ) {
					$('#numdays').hide();
					$('#numhours').removeClass('numbers');
					$('#numhours').show();
				} else {
					$('#numhours,#numhours').hide();
				}
			});
			/* ********* Add form  ********* */
			/* ********* Edit form ********* */
			jQuery("#leaveeditform").validate({
				ignore: [],
				rules: {                 
					editeventstatus:{
						required: true,
					},                                
					reason:{
						required: true,
					},                                
				},                
				messages: {                    
					editeventstatus:{
						required: "Please select a status.",
					},                                
					reason:{
						required: "Please enter a reason.",
					},                                
				},
				submitHandler: function(form){
					var str = $("#leaveeditform").serialize();
					$.ajax({
						type: "POST",
						url: "ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								setTimeout(function() {
									location.reload();
								}, 2000);
							}
						}
					}); 
				}
			});	   
			$("#editeventstatus").on('change', function() {
				var statusval = $(this).val();
				if ( statusval == 0 ) {
					$('.editnumbers').hide();
					$('#editnumdays').show();
				} else if ( statusval == 1 ) {
					$('.editnumbers').hide();
					$('#editnumhours').show();
				}
			});
			/* ********* Edit form ********* */
			
		});
	</script>
	<style>
		body { padding: 0; font-family: sArial, Helvetica Neue, Helvetica, sans-serif; font-size: 14px; }
		#calendar { max-width: 900px; margin: 0 auto; }
	</style>

	<div class="form-container Schedule-outer-block">
		<div class="heading-container">
			<h1 class="heading empHead">Manage Schedule</h1>
			<div class="schedulerightblock">
				<div class="addNewReport edit-emp-link">
					<a class="empLinks" href="edit-schedule?empid=<?php echo $_GET['empid']?>" class="submit-btn">Edit Schedule</a>&nbsp;|&nbsp;
				</div>
				<div class="addNewReport edit-emp-link">
					<a class="empLinks" href="empdetails" class="submit-btn">Employee List</a>
				</div>
			</div>
		</div>
		<div class="user-entry-block">
			<div class="schedule-container">
				<div class="row">
					<div class="span12">
						<label id="Label1" class="user-name Schedule-subheading">Schedule:</label>
					</div>	
				</div>
				<?php
				if(!empty($editinfo)) { 
					$editinfo1 = unserialize($editinfo['dailySchdeule']);
					for( $i = 0; $i < 7; $i++ ){
						$schedulefrag = explode('-',$editinfo1[$i]);
				?>	
						<div class="row calrow">
							<div class="dayname-container">
								<label id="Label1" class="user-name"><b><?php echo $empInfo->daysname($i); ?></b></label>
								<label class="switch">
									<input class="switch" name="day[]" value="0" type="checkbox" <?php if( isset($schedulefrag[0]) && $schedulefrag[0] == 1 ){ echo 'checked'; }?> disabled>
									<span class="slider round"></span>
								</label>
								<input name="day1[]" value="<?php if( isset($schedulefrag[0]) && $schedulefrag[0] == 1 ){echo 1;} else {echo 0;} ?>" class="hiddenday" type="hidden">				
							</div>
							<div class="operatinghour-container">
								<label id="Label1" class="user-name label-block-big">Operating Hours:</label>
								<div class="rowblock-ToFrom">
									<div class="operatinghour-from">
										<label class="user-name">From: </label><?php if(isset($schedulefrag[1]) && $schedulefrag[1] != ''){ echo $schedulefrag[1]; } else { echo '-'; }?>
									</div>
									<div class="operatinghour-to">
										<label class="user-name">To: </label><?php if(isset($schedulefrag[2]) && $schedulefrag[2] != ''){ echo$schedulefrag[2]; } else { echo ''; } ?>
									</div>
								</div>
							</div>
						</div>	<!-- @end of row -->
					<?php
					}
				} else {
				?>
					<div class="row calrow">
						<div class="dayname-container">
							<label id="Label1" class="user-name"><b>No record found.</b></label>
						</div>
					</div>	<!-- @end of row -->
				<?php
				}
				?>
			</div>
			
			<div class="calander-container">
				<div class="row">
					<div class="span6">
						<label id="Label1" class="user-name">Leave Management:</label>
					</div>	
				</div>
				<div class="fc-event-inner"></div>
				<div id='calendar'></div>
			</div>

			<!-- The Add leave Modal -->
			<div id="myModal" class="modal addleavemodel modal-custom">
				<!-- Modal content -->
				<div class="modal-content cal-model">
					<span id="close" class="close">&times;</span>
					<h3>Add Leave</h3>
					<div>
						<form id="leaveaddform" name="leaveaddform" action='' type="post">
							<input type="hidden" name="formname" value="leaveadd">
							<input type="hidden" name="employeeID" value="<?php echo base64_decode($_GET['empid']); ?>">
							<input type="hidden" name="businessID" value="<?php echo $_SESSION['BusinessID']; ?>">
							<input type="hidden" name="userID" value="<?php echo $_SESSION['id']; ?>">
							<input type="hidden" name="startDate" id="startDate" value="">
							<div>
								<div class="modal-col-block">
									<span>Reason</span>
									<select name="status" id="status">
										<option value=''>Please select</option>
										<option value='0'>Leave</option>
										<option value='1'>Away</option>
									</select>
								</div>
								<div class="modal-col-block numbers" id="numdays">
									<span>Number of days</span>
									<input type="text" name="noofdays" value="">
								</div>
								<div class="numbers modal-col-block" id="numhours">
									<span>Number of hours</span>
									<input type="text" name="noofhours" value="">
								</div>
								<div class="modal-col-block full-width">
									<span>Reason of leave</span>
									<textarea name="reason" id="reason"></textarea>
								</div>
							</div>
							<div class="modal-s-block">
								<input type="submit" name="submit" value="add">
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- The Edit leave Modal -->
			<div id="editModal" class="modal editleavemodel modal-custom">
				<!-- Modal content -->
				<div class="modal-content cal-model">
					<span id="close1" class="close">&times;</span>
					<h3>Edit event</h3>
					<div>
						<form id="leaveeditform" name="leaveaddform" action='' type="post">
							<input type="hidden" name="formname" value="leaveedit">
							<input type="hidden" name="ID" id="editeventid" value="">
							<input type="hidden" name="businessID" id="editbusid" value="<?php echo $_SESSION['BusinessID']; ?>">
							<input type="hidden" name="userID" id="edituserid" value="<?php echo $_SESSION['id']; ?>">
							<input type="hidden" name="employeeID" id="editeid" value="<?php echo base64_decode($_GET['empid']); ?>">
							<input type="hidden" name="startDate" id="editstartDate" value="">
							<div>
								<div class="modal-col-block">
									<span>Availability</span>
									<select name="editeventstatus" id="editeventstatus">
										<option value=''>Please select</option>
										<option value='0'>Leave</option>
										<option value='1'>Away</option>
									</select>
								</div>
								<div class="editnumbers modal-col-block" id="editnumdays">
									<span>Number of days</span>
									<input type="text" name="editnoofdays" id="editnoofdays" value="">
								</div>
								<div class="modal-col-block editnumbers" id="editnumhours">
									<span>Number of hours</span>
									<input type="text" name="editnoofhours" id="editnoofhours" value="">
								</div>
								<div class="modal-col-block full-width">
									<span>Reason of leave</span>
									<textarea name="reason" id="editeventdesc"></textarea>
								</div>
							</div>
							<div class="modal-s-block">
								<input type="submit" name="submit" value="edit">
								<button title="click to delete leave" id="deleteevent" idattr=''>delete</button>
							</div>
							
						</form>
					</div>
				</div>
			</div>
			
			<div id="showModal" class="modal editleavemodel">TEst tes </div>
		</div>
	</div>

</div><!-- container-->
<?php
	include('admin_includes/footer.php');
?>
