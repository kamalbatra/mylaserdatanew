<!DOCTYPE html>
<?php
	include('admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$clintInfo = new dbFunctions();
	$tableConsent1="tbl_treatment_log";
	$cols="*";
	$condition="where ClientID =".base64_decode($_GET['ClientID'])." and TattoNumber='".base64_decode($_GET['TattoNumber'])."'  AND  Tattoo_Photo_thumbnail !='' ";
	$checkExistence = $clintInfo->selectTableRows($tableConsent1,$condition,$cols);
	//print_r($checkExistence);	
?>
	<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/css_gallery/demo.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/css_gallery/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/css_gallery/elastislide.css" />
	<noscript>
		<style>
			.es-carousel ul {
				display:block;
			}
		</style>
	</noscript>
	<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
		<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
			{{/if}}
			<div class="rg-image"></div>
			<div class="rg-loading"></div>
			<div class="rg-caption-wrapper">
				<div class="rg-caption" style="display:none;">
					<p></p>
				</div>
			</div>
		</div>
	</script>
	<!-- Page Wrapper -->
	<div id="wrapper">
		<!-- Sidebar -->
		<?php  include('admin_includes/sidebar.php');  ?>
		<!-- End of Sidebar -->
		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
				<!-- Topbar -->
				<?php  include('admin_includes/topbar.php');  ?>
				<!-- End of Topbar -->
				<!-- Begin Page Content -->
				<div class="container-fluid all-bussiness">
					<!-- Page Heading -->
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Treatments Log Images</h1>
					</div>
					<div class="card shadow mb-4 table-main-con marketing-report">
						<div class="bussiness-searchblock no-searchbox">
							<div class="search-btn">
								<a class="empLinks" href="clientdetails.php" class="submit-btn"><button class="addnewbtn">Client list </button></a>
							</div>
						</div>
						<div class="managescheduleblock">
							<div class="slickThumbnail">
							<?php
							//print_r ($checkExistence); 
								if($checkExistence !=NULL) {
							?>
									<div id="rg-gallery" class="rg-gallery">
										<div class="rg-thumbs">
											<!-- Elastislide Carousel Thumbnail Viewer -->
											<div class="es-carousel-wrapper">
												<div class="es-nav">
													<span class="es-nav-prev">Previous</span>
													<span class="es-nav-next">Next</span>
												</div>
												<div class="es-carousel">
													<ul>
													<?php
													$lk = 0;
													foreach($checkExistence as $data) {
														if($data['Tattoo_Photo_thumbnail'] !="") {
															$imgSrc = explode(",",$data['Tattoo_Photo_thumbnail']);				
															for($k =0; $k < count($imgSrc);$k++) {
																$filename = "Business-Uploads/".$_SESSION['BusinessID']."/thumbnail/". $imgSrc[$k];
																if(file_exists($filename)) {
																	$date = new DateTime($data['Date_of_treatment']);
																	$date->setTimezone(new DateTimeZone('CST')); // +04
																	$treatment=$date->format('Y-m-d H:i:s'); // 2012-07-15 05:00:00 
																	date_default_timezone_set(timezone_name_from_abbr("UTC"));
																?> 
																	<li><a href="#"><img src="<?php echo $filename; ?>" data-large='<?php echo "Business-Uploads/$_SESSION[BusinessID]/resize/$imgSrc[$k]"; ?>' alt="image01" data-description="Session : <?php echo $data['SessionNumber']; ?>&nbsp; Date : <?php echo $treatment//$data['Date_of_treatment']; ?>" /></a></li>	
																<?php
																} else { ?>
																	<li><a href="#"><img src="uploads/thumbnail/noimg.jpg" data-large="uploads/resize/noimg.jpg" alt="image01" data-description="Session :<?php echo $checkExistence[$lk]['SessionNumber'];?>&nbsp; Date :<?php echo $checkExistence[$lk]['Date_of_treatment'];?>" /></a></li>									
																<?php
																}
															}//close for loop
														}
														$lk++;
													}  //foreach
													?>
													</ul>
												</div>
											</div>
											<!-- End Elastislide Carousel Thumbnail Viewer -->
										</div><!-- rg-thumbs -->
									</div><!-- rg-gallery -->
								<?php
								} else { ?>
									<div> No prior record found! </div>	  
								<?php
								} ?>
								<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/jquery.tmpl.min.js"></script>
								<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/jquery.easing.1.3.js"></script>
								<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/jquery.elastislide.js"></script>
								<script type="text/javascript" src="<?php echo $domain; ?>/js/js_gallery/gallery.js"></script>
							</div>
						</div>
					</div>
				
				</div>
				<!-- /.container-fluid -->
			</div>
		<?php	include('admin_includes/footer-new.php');	?>
