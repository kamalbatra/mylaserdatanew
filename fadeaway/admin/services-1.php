<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	$serDetails	= new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"])){ ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	/*** fetch all Employee location**/
	$tbl_category	= "tbl_services";
	$condition = "WHERE businessID = $_SESSION[BusinessID] ORDER BY seviceID DESC";
	$cols = "*";
	$catData = $serDetails->selectTableRows($tbl_category,$condition);
	/*** fetch All device Name**/
?>
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function(){
			
			$('a.activeUser').click(function(){
				var statusMsg = $(this).attr('id');
				if (confirm("Are you sure you want to "+statusMsg+" this service?")){
					$('.loadingOuter').show();
					var id = $(this).parent().parent().attr('id');
					var status = $(this).parent().parent().attr('status');
					var data = 'seviceID='+id+'&status='+status+'&action=services';
					var parent = $(this).parent().parent();			
					$.ajax({
						type: "POST",
						url: "ajax_newchangestatus.php",
						data: data,
						cache: false,				
						success: function(data){
							if(data == 1 ){
								data = 'Status updated succesfully.';
								//~ $("#statuResult").html(data);
								setTimeout(function(){ location.reload() }, 1000);							
							}
						}
					});		
				}
			});
			
		});
	</script>
	<style>
		.srtHeadEditEmp { width:10%; }
		.tableservices .span4.srtHead.srtHeadBorder { width:13%; }
		.tableservices .span4.srtHead.srtcontent { width:13%;padding: 10px 0px 2px 12px; }
		.addNewReport { float: right; }
	</style>
	<div class="form-container">
		<div class="loadingOuter"><img src="../images/loader.svg"></div>
		<div class="heading-container">
			<h1 class="heading empHeadReport">Services</h1>
			<div class="addNewReport addReportemp"><a class="empLinks" href="add-service">Add New</a></div>
		</div>
		<div class="user-entry-block">
			<div class="tablebushead">	
				<div class="tablebusinner tableservices-outer">	
					<div class="row sortingHead">	
						<div class="span4 srtHead srtHeadBorder">Service Name</div>
						<div class="span4 srtHead srtHeadBorder">Category Name</div>
						<div class="span4 srtHead srtHeadBorder">Location Name</div>
						<div class="span4 srtHead srtHeadBorder">Status</div>
						<div class="span4 srtHead srtHeadBorder">Date Added</div>
						<div class="span4 srtHeadEditEmp srtHeadBorder" style="text-align:center;">Action</div>
						<div class="span4 srtHeadEditEmp srtHeadBorder" style="border:none;text-align:center;">Action</div>
					</div>        
					<?php
					if( !empty($catData) ) { 
						$i = 0;
						foreach( $catData as $dataemp ){
							if(	$i%2 == 0 ){ 
								$bgdata = "bgnone";	
							} else {
								$bgdata = "bgdata";
							}	
							if(($_SESSION['id']==$dataemp['businessID']) && (strtoupper($_SESSION['Medicaldirector'])!="YES")){
								$currentLogin= "currentLogin";
							} else { 
								$currentLogin= "";
							} ?>	
							<div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>" id="<?php echo $dataemp['seviceID']?>" status="<?php echo $dataemp['status'];?>" >
								<div class="span4 srtHead srtcontent">
									<label id="" class="user-name"><?php echo $dataemp['serviceName']?> </label>						
								</div>					
								<div class="span4 srtHead srtcontent">
									<?php
									$table = 'tbl_categories';
									$cond = "WHERE id=".$dataemp['categoryID'];
									$cols = " categoryName";
									$ServiceName = $serDetails->selectTableSingleRow($table,$cond,$cols);
									?>
									<label id="" class="user-name"><?php echo ucfirst($ServiceName['categoryName']); ?> </label>						
								</div>					
								<div class="span4 srtHead srtcontent">
									<?php
									$table = 'tbl_manage_location';
									$cond = "WHERE ManageLocationId=".$dataemp['locationID'];
									$cols = " LocationName";
									$locationName = $serDetails->selectTableSingleRow($table,$cond,$cols);
									?>
									<label id="" class="user-name"><?php echo ucfirst($locationName['LocationName']); ?> </label>						
								</div>
								<div class="span4 srtHead srtcontent">	
									<label id="" class="user-name">
									<?php 
									if( $dataemp['status'] == 0 ){
										$status = 'De-Active';
									} else if ( $dataemp['status'] == 1 ){
										$status = 'Active';									
									}
									echo $status;
									?>
									</label>
								</div>
								<div class="span4 srtHead srtcontent">
									<label id="" class="user-name" style="text-transform:none;">
										<?php echo date("F j, Y", strtotime($dataemp['dateAdded'])); ?>
									</label>
								</div>
								<?php 
								if((isset($_SESSION['loginuser']) && $_SESSION['loginuser']=="sitesuperadmin") || $_SESSION['Admin']=="TRUE" || (strtoupper($_SESSION['Medicaldirector'])=="YES") || $_SESSION['id']==$dataemp['seviceID']) { ?>
									<div class="span4 srtHeadEditEmp srtcontent text-align">
										<label id="" class="user-name"><a href="edit-service?id=<?php echo $dataemp['seviceID']?>&action=servicedetails"><img src="<?php echo $domain; ?>/images/b_edit.png" title="Edit Category"/></a></label>							
									</div>
									<?php 
									if( $dataemp['status'] == 1 ) { ?>
										<div class="span4 srtHeadEditEmp srtcontent text-align" style="cursor:pointer;">
											<a class="activeUser" id="deactive" ><img src="<?php echo $domain; ?>/images/activeUser.png" title="Deactive" width="20px" height="20"/></a>
										</div>
									<?php 		
									}  else { ?>
										<div class="span4 srtHeadEditEmp srtcontent text-align" style="cursor:pointer;">
											<a class="activeUser" id="active" ><img src="<?php echo $domain; ?>/images/no.png" title="Active" width="20px" height="20"/> </a> 
										</div>
									<?php 
									} 
								} ?>
							</div><!--End @row-->				  
							<?php 
							$i++;  
						} 
					} else { ?>
						<div class="row  <?php echo $bgdata;?> <?php echo $currentLogin;?>">
							<div class="srtcontent">
								No record found.
							</div>
						</div>
					<?php
					} ?>			  
					<div id="statuResult"></div>      
				</div><!--End @user-entry-block-->
			</div><!-- End  @form-container--->
		</div><!--End @container-->
	</div><!--End @container-->
	</div><!--End @container-->
	<?php include('admin_includes/footer.php');	?>
