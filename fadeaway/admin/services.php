<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$serDetails	= new dbFunctions();
	if( !in_array(6,$_SESSION["menuPermissions"])){ ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	/*** fetch all Employee location**/
	$tbl_category	= "tbl_services";
	/*** numrows**/
	$adjacents = 3;
	$reload="services";
	$conditionNumRows  =  " WHERE businessID = $_SESSION[BusinessID] ORDER BY seviceID DESC ";
	$totalNumRows	= $serDetails->totalNumRows($tbl_category, $conditionNumRows);
	$total_pages = $totalNumRows;
	if(isset($_GET['page'])){
		$page=$_GET['page'];
	} else{
		$page="";
	}
	$limit =10;	//how many items to show per page
    if($page){
		$start = ($page - 1) * $limit;	//first item to display on this page
	} else {
		$start = 0;
	}
	/*** numrow**/
	$condition = "WHERE businessID = $_SESSION[BusinessID] ORDER BY seviceID DESC LIMIT  ".$start.", ".$limit."";
	$cols = "*";
	$catData = $serDetails->selectTableRows($tbl_category,$condition);
	/*** fetch All device Name**/
?>
	<link href="<?php echo $domain; ?>/css/style-new.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function(){
			
			$('a.activeUser').click(function(){
				var statusMsg = $(this).attr('id');
				if (confirm("Are you sure you want to "+statusMsg+" this service?")){
					$('.loadingOuter').show();
					var id = $(this).parent().parent().attr('id');
					var status = $(this).parent().parent().attr('status');
					var data = 'seviceID='+id+'&status='+status+'&action=services';
					var parent = $(this).parent().parent();			
					$.ajax({
						type: "POST",
						url: "ajax_newchangestatus.php",
						data: data,
						cache: false,				
						success: function(data){
							if(data == 1 ){
								data = 'Status updated succesfully.';
								//~ $("#statuResult").html(data);
								setTimeout(function(){ location.reload() }, 1000);							
							}
						}
					});		
				}
			});
			
		});
	</script>

<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Services</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="add-service"><button class="addnewbtn">Add New </button></a>
						</div>
					</div>
					<div class="card-body">
					<?php
						if( !empty($catData) ) {
							$i = 1;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>Service Name</th>
										<th>Category Name</th>
										<th>Location Name</th>
										<th>Status</th>
										<th>Date Added</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $catData as $dataemp ) {
											if(	$i%2 == 0 ){ 
												$bgdata = "bgnone";	
											} else {
												$bgdata = "bgdata";
											}	
											if(($_SESSION['id']==$dataemp['businessID']) && (strtoupper($_SESSION['Medicaldirector'])!="YES")){
												$currentLogin= "currentLogin";
											} else { 
												$currentLogin= "";
											}
									?>
												<tr class="treatment <?php echo $bgdata; ?> <?php echo $currentLogin; ?>" id="<?php echo $dataemp['seviceID']; ?>" status="<?php echo $dataemp['status']; ?>">
													<td class="span3 srtHeadEditEmp srtcontent"><label class="user-name"><?php echo $i;?></label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $dataemp['serviceName']; ?> </label></td>
													<td class="span6 srtHead srtcontent">
														<?php
														$table = 'tbl_categories';
														$cond = "WHERE id=".$dataemp['categoryID'];
														$cols = " categoryName";
														$ServiceName = $serDetails->selectTableSingleRow($table,$cond,$cols);
														?>
														<label id="" class="user-name"><?php echo ucfirst($ServiceName['categoryName']); ?> </label>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<?php
														$table = 'tbl_manage_location';
														$cond = "WHERE ManageLocationId=".$dataemp['locationID'];
														$cols = " LocationName";
														$locationName = $serDetails->selectTableSingleRow($table,$cond,$cols);
														?>
														<label id="" class="user-name"><?php echo ucfirst($locationName['LocationName']); ?> </label>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<label id="" class="user-name">
														<?php 
														if( $dataemp['status'] == 0 ){
															$status = 'De-Active';
														} else if ( $dataemp['status'] == 1 ){
															$status = 'Active';									
														}
														echo $status;
														?>
														</label>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<label id="" class="user-name" style="text-transform:none;">
															<?php echo date("F j, Y", strtotime($dataemp['dateAdded'])); ?>
														</label>
													</td>
													<td class="span6 cMain ">
														<?php 
														if((isset($_SESSION['loginuser']) && $_SESSION['loginuser']=="sitesuperadmin") || $_SESSION['Admin']=="TRUE" || (strtoupper($_SESSION['Medicaldirector'])=="YES") || $_SESSION['id']==$dataemp['seviceID']) { ?>
																<label id="" class="user-name"><a href="edit-service?id=<?php echo $dataemp['seviceID']?>&action=servicedetails"><img src="<?php echo $domain; ?>/img/editimg.png" title="Edit Category"/></a></label>							
															<?php 
															if( $dataemp['status'] == 1 ) { ?>
																<a class="activeUser" id="deactive" ><img src="<?php echo $domain; ?>/img/tickimg.png" title="Deactive" width="20px" height="20"/></a>
															<?php 		
															}  else { ?>
																<a class="activeUser" id="active" ><img src="<?php echo $domain; ?>/img/notallow.png" title="Active" width="20px" height="20"/> </a> 
															<?php 
															} 
														} ?>
													</td>
												</tr><!--End @row-block-->
												<?php
												$i++;
										} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $serDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<div class="loadingOuter"><img src="../images/loader.svg"></div>
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
