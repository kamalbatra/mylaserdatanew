<?php
	include('admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	$empInfo = new dbFunctions();
	$location_con = " WHERE BusinessID = $_SESSION[BusinessID] ORDER BY LocationName";
	$locations = $empInfo->selectTableRows('tbl_manage_location', $location_con);
	$oldlocation = '';
	$arra = array();
	$uniqueids = array();
	$locationids = array();
	foreach($locations AS $location){
		if($oldlocation != $location['LocationName']){
			$i = 0;
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
			$oldlocation = $location['LocationName'];
			array_push($locationids,$location['ManageLocationId']);
		}else{
			$arra[$location['LocationName']][$i] = $location['ManageLocationId'];
			array_push($locationids,$location['ManageLocationId']);
		}	
		$i++;
	}
	$locationids = implode(',',$locationids);
	$year = date('Y');
	
	$result = mysqli_query($empInfo->con,"SELECT MONTH(Date_of_treatment) AS m, COUNT(*) AS totaltreatment, SUM(Price) as price FROM `tbl_treatment_log` WHERE YEAR(Date_of_treatment) = '".$year."' AND `Location` IN (".$locationids.") GROUP BY m ORDER BY `tbl_treatment_log`.`Location` ASC");
			  
	$result1 = mysqli_query($empInfo->con,"SELECT MONTH(TimeStamp) AS m, COUNT(*) AS totalpatient FROM `tbl_clients` WHERE YEAR(TimeStamp) = '".$year."' AND `BusinessID` = ".$_SESSION['BusinessID']." GROUP BY m");
	
	$row = array();
	$rows = array();
	$i = 1;
	while( $r1 = mysqli_fetch_array($result1) ) {
		$rows[$i]['month'] = $r1[0];
		$rows[$i][0] = $r1[1];
		$i++;
	}
	$j=1;
	while($r = mysqli_fetch_array($result)) {
		$rows[$j][1] = $r[1];
		$rows[$j][2] = (int)$r[2];
		$j++;
	}
?>	
	<style>
		.referral-source-search input,select{ width:auto !important;margin:-15px 15px 15px 0; }
		.referral-source-search {border-bottom: 1px dotted #666666; }
		.referral-source-search div.search-clear {margin-top:-15px; }
		.referral-source-search div.search-clear img{cursor:pointer; }
		.search-tabular{border-top: 1px dotted #666666;padding-top:15px; }
		.search-tabular table{width:100%; }
		.search-tabular table th, td{border: 0px solid #91bde1;font-weight:normal;font-size:13px;border: 1px solid #fff; }
		.search-tabular table th{color:#000; }
		.search-tabular table td{color:#666666;padding-left:10px; }
		.search-tabular table tr.odd{background:rgb(220,220,220); }
		.search-tabular table tr{height:43px; }
		.search-tabular table tbody tr:hover {background: #b6c6d7 none repeat scroll 0 0; }
		.search-tabular table thead{background: rgb(220,220,220); }
		.search-tabular table thead tr th{font-weight:bold;font-size:14px; }
		.ui-datepicker-year { font-size: 15px !important; height: 30px; width: 70px !important; }
		.ui-datepicker-month{display:none; }
		.ui-datepicker table{display:none; }
		.ui-datepicker .ui-datepicker-prev { display: none; }
		.ui-datepicker .ui-datepicker-next { display: none; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() {

	        $(".clicktosearchreferral").click(function(){
				$.ajax({
					url:"data1.php?from="+$("#referral-from").val()+"&loc="+$("#referral-loc").val(),
					type:"GET",
					data:{get_tabular:'tabular'},
					success:function(res){
						$(".changeonsearch").html(res);
					}
				});
			});

	        $(".clicktoclearreferral").click(function(){
				location.reload(); 
			});

			$("#referral-from").datepicker({
				changeYear: true,
				showButtonPanel: true,
				maxDate: '0d',
				dateFormat: 'yy',
				yearRange: "-20:+1",
				onClose: function(dateText, inst) { 
					var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					$(this).datepicker('setDate', new Date(year, 1));
				},
			});
			
			$('.close-btn-pricing').click(function() {
				$('.overlay-bg-pricing').hide();	
			});
			
		});   
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo $domain; ?>/css/pricing.css" />
	<div id="wrapper">
		<!-- Sidebar -->
		<?php  include('admin_includes/sidebar.php');  ?>
		<!-- End of Sidebar -->
		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			<!-- Main Content -->
			<div id="content">
				<!-- Topbar -->
				<?php  include('admin_includes/topbar.php');  ?>
				<!-- End of Topbar -->
				<!-- Begin Page Content -->
				<div class="container-fluid all-bussiness">
					<!-- Page Heading -->
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Clinic Statistics</h1>
						<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
					</div>
					<div class="card shadow mb-4 table-main-con">
						<div class="bussiness-searchblock no-searchbox">
							<div class="busniss-search busniss-search-stats">
								<input type="text" placeholder="Referral from date" class="text-input-field" id="referral-from" />
							</div>
							<div class="busniss-search  busniss-search-stats">
								<select class="select-option" id="referral-loc">
									<option value=''>Select location</option>
									<?php foreach($arra AS $key=>$arr) { ?>
									<option value="<?php echo implode(",", $arr); ?>"><?php echo ucfirst($key); ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="busniss-search last busniss-search-stats-last">
								<img src="<?php echo $domain; ?>/images/search.png" class="clicktosearchreferral" title="Search" />
								<img src="<?php echo $domain; ?>/images/clear.jpg" class="clicktoclearreferral" title="Clear" />
							</div>
						</div>
						
						<div class="card-body">
						<?php
							if( !empty($rows) ) {
								$total = 0; $lastkey = 0;
						?>
								<div class="table-responsive">
									<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th class="center-text">Month</th>
												<th class="center-text">Total Patients</th>
												<th class="center-text">Number of Treatments Performed</th>
												<th class="center-text">Total Revenue($)</th>
											</tr>
										</thead>
										<tbody class="changeonsearch">
										<?php 
											foreach( $rows AS $key => $ref_data ) {
												$totalpatient += $ref_data[0]; 
												$totaltreatment += $ref_data[1]; 
												$totalrevenue += $ref_data[2];
										?>
												<tr class=<?php echo (($key%2)==0) ? 'even' : 'odd'; ?>>
													<td class="text-align1 span3 srtHeadEditEmp srtcontent center-text"><?php echo $empInfo->monthname($key); ?></td>
													<td class="text-align span6 srtHead srtcontent center-text"><?php echo $ref_data[0]; ?></td>
													<td class="text-align span6 srtHead srtcontent center-text"><?php echo $ref_data[1]; ?></td>
													<td class="text-align span6 srtHeadloc srtcontent center-text"><?php echo $ref_data[2]; ?></td>
												</tr><!--End @row-block-->
										<?php
												$lastkey = $key;
											} //foreach end
										?>
											<tr class="<?php echo (($lastkey%2)==0) ? 'even' : 'odd'; ?>">
												<td class="text-align center-text"><b>Total</b></td>
												<td class="text-align center-text"><b><?php echo $totalpatient; ?></b></td>
												<td class="text-align center-text"><b><?php echo $totaltreatment; ?></b></td>
												<td class="text-align center-text"><b><?php echo $totalrevenue; ?></b></td>
											</tr>
										</tbody>
									</table>
								</div>
						<?php
							} else {
								echo "<div class='table-responsive'><table class='table table-bordered bussinessTable' width='100%' cellspacing='0'><thead><tr><th>Month</th><th>Total Patients</th><th>Number of Treatments Performed</th><th>Total Revenue($)</th></tr></thead><tbody class='changeonsearch'><tr style='text-align:center;color:#c0c0c0'><td colspan=5>No record found!!</td></tr></tbody></table></div>";	
							}
						?>
						</div>
					</div>
				</div>
				<!-- /.container-fluid -->
				<div id="statuResult"></div>
			</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
