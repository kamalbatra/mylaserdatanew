<?php
	$page_name = "subscription";
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	unset($_SESSION['planId']);
	$subplan = new dbFunctions();
	if($_SESSION['BusinessPlanType'] == "Subscribed" && $_SESSION['optoutstatus'] == "No"){
	?>
		<style>
		#spinner {
			background: rgb(249, 249, 249) url("<?php echo $domain; ?>/images/page-loader.gif") no-repeat scroll 50% 50%;
			height: 100%;
			left: 0;
			position: fixed;
			top: 0;
			width: 100%;
			z-index: 9999;
		}
		</style>
		<div id="spinner"></div>
		<form action="<?php echo $domain; ?>/membership-subscription/index.php?action=recurring" method="post" id="submitformonload">
			<input type="hidden" value="<?php echo base64_encode($_SESSION["CurrentPlanId"]); ?>" name="planId"/>
		</form>
		<script>
			$(function(){
				$("#submitformonload").submit();
			});
		</script>
	<?php
	}else{
	$plantable = "tbl_master_plans";
	$condition = "where `Type`='Paid' AND id NOT IN(1,2) AND status != 0 order by ID asc";
	$plandetails = $subplan->selectTableRows($plantable,$condition,$cols="*");
?>
<style type="text/css">
	.srtHead { 
		width: 22%; 
	}
	.srno {
		width: 12%;
	}
	.span3 {
		text-align: center;
	}
</style>
<link href="<?php echo $domain; ?>/css/style.css" rel="stylesheet" type="text/css" />
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Our Plans</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="plan-ul-outer">
					<ul>
					<?php foreach($plandetails as $details){ ?>	
						<li>
							<div class="plan-top"> <h4> <?php echo $details["title"]; ?> </h4></div>
							<div class="plan-rate">
								<h5> <span> $ </span> <?php if($details["amount"]!=0) echo $details["amount"]; else echo $details["amount"]; ?> </h5>
							</div>
							<div class="days"> No. of days <span> <?php echo $details["days"]; ?> </span> </div>
							<div class="plan-btn"> 
								<form action="<?php echo $domain; ?>/membership-subscription/index.php" method="post">
								<!--form action="subscribe-payment.php" method="post"-->
									<input type="hidden" value="<?php echo base64_encode($details["id"]); ?>" name="planId"/>
									<button type="submit">Subscribe</button>
								</form>
							</div>
						</li>
					<?php } ?>
					</ul>
				</div>
			</div>
			<!-- /.container-fluid -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php }	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>