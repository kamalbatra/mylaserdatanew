<?php
session_start();
$doaminPath = $_SERVER['DOMAINPATH'];
$domain = $_SERVER['DOMAIN'];
include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
include("../includes/dbFunctions.php");
if($_SESSION['loginuser'] != "sitesuperadmin"){
	?>
	<script>
		$(function(){
			window.location.replace("dashboard");
		});
	</script>
	<?php
	die;
}

$clientDetails = new dbFunctions();
if( !in_array(5,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
if(isset($_GET['orderby'])&& $_GET['name'] ){
	if($_GET['orderby']=="ASC" && $_GET['name']=="businessname"){
		$order ="DESC";
		$name = "BusinessName";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="businessname"){		
		$order ="ASC";
		$name = "BusinessName";
		$ascDescImg=$domain."/images/s_asc.png";
	}
	if($_GET['orderby']=="ASC" && $_GET['name']=="lastname"){
		$order ="DESC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_desc.png";
	}
	if($_GET['orderby']=="DESC" && $_GET['name']=="lastname"){
		$order ="ASC";
		$name = "LastName";
		$ascDescImg=$domain."/images/s_asc.png";
	}
}else{
	$order ="DESC";
	$name = "BusinessID";
	$ascDescImg=$domain."/images/s_desc.png";
}
?>
<script type="text/javascript">
$(function(){
	$(".searchclient").keyup(function(){ 
		var searchid = $(this).val();
		var dataString = 'search='+ searchid;
		if(searchid!=''){
			$.ajax({
				type: "POST",
				url: "ajax_clientsearch.php",
				data: dataString,
				cache: false,
				success: function(html){
				   $("#resultClientSerch").html(html).show();
				}
			});
		}return false;    
	});
	jQuery("#result").live("click",function(e){ 
		var $clicked = $(e.target);
		var $name = $clicked.find('.name').html();
		//alert(name)
		var decoded = $("<div/>").html($name).text();
		$('#searchid').val(decoded);
		$('#fname').val(decoded);
		//alert(decoded);
	});
	jQuery(document).live("click", function(e) { 
		var $clicked = $(e.target);
		if (! $clicked.hasClass("search")){
			jQuery("#result").fadeOut(); 
		}
	});
	$('#searchid').click(function(){
		jQuery("#result").fadeIn();
	});
});
</script>
<?php
/*** fetch all Client with location**/
$table1= "tbl_business";
$table2= "tbl_clients";
$join="INNER";
//$cols = "tbl_clients.ClientID, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS FirstName, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS LastName, tbl_clients.Location";
/*** numrows**/
 $adjacents = 3;
 $reload="superadmin-report";
 if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){
	 $conditionNumRows  = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";	 
 }else{
	$conditionNumRows  = " ORDER BY BusinessID DESC";
 }	
  $totalNumRows	= $clientDetails->totalNumRows($table1,$conditionNumRows);
$total_pages = $totalNumRows;
//$page="";
 if(isset($_GET['page'])){
	 $page=$_GET['page'];
	}
	else{
		$page="";
	}
 $limit = 10;                                  //how many items to show per page
    if($page)
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0; 
/*** numrow**/
if(isset($_GET['ClientID'])&& $_GET['ClientID']!=""){	  
  $condition = "tbl_clients.ClientID=tbl_consent_form.ClientID  WHERE tbl_clients.ClientID='".base64_decode($_GET['ClientID'])."' ORDER BY tbl_clients.FirstName ";
}else{
  $condition = " ORDER BY ".$name." ".$order." LIMIT ".$start.", ".$limit." ";
}
$businessdata	= $clientDetails->selectTableRowsNew($table1, $condition);
//echo count($clientdata);
/*** fetch All client with location Name**/

function foldersize($path) {
	$total_size = 0;
	$files = scandir($path);
	$cleanPath = rtrim($path, '/'). '/';
	foreach($files as $t) {
		if ($t<>"." && $t<>"..") {
			$currentFile = $cleanPath . $t;
			if (is_dir($currentFile)) {
				$size = foldersize($currentFile);
				$total_size += $size;
			}
			else {
				$size = filesize($currentFile);
				$total_size += $size;
			}
		}   
	}
	$total_size=$total_size/(1024*1024); // Bytes to Mega Bytes
	$total_size=round($total_size, 3); 
	return $total_size;
}
?>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">All Businesses Report</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="card-body">
					<?php
						if( !empty($businessdata) ) {
							$i = 0;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th><a title="<?php echo $order;?>" href="superadmin-report?orderby=<?php echo $order;?>&name=businessname"> Business Name <img id="soimg0" class="icon" width="11" height="9" title="" alt="" src="<?php echo $ascDescImg;?>">  </a></th>
										<th>Service Plan</th>
										<th>Last Transaction ID</th>
										<th>Cumulative Payments</th>
										<th>Unique Client Records</th>
										<th>Unique Treatment Records</th>
										<th>Storage</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $businessdata as $cldata ) {
											$subsc_cond = " WHERE BusinessID = $cldata[BusinessID] ORDER BY ID DESC LIMIT 1";
											$subscription_data = $clientDetails->selectTableSingleRowNew("tbl_subscription_history",$subsc_cond);
											
											$plancond = "where id='".$subscription_data["PlanID"]."'";
											$plandata = $clientDetails->selectTableSingleRowNew("tbl_master_plans",$plancond,$cols="*");
										
											$commu_pay_col = "SUM(PaymentAmount) AS Cumulative_Pay";
											$commu_pay_cond = " WHERE BusinessID = $cldata[BusinessID]";
											$commu_pay = $clientDetails->selectTableRowsNew("tbl_subscription_history",$commu_pay_cond, $commu_pay_col);
											
											$unique_client_col = "COUNT(*) AS UniqueClient";
											$unique_client_cond = " WHERE BusinessID = $cldata[BusinessID]";
											$unique_client = $clientDetails->selectTableRowsNew("tbl_clients",$unique_client_cond, $unique_client_col);
											
											$unique_treatment_col = "COUNT(*) AS UniqueTreatment";
											$unique_treatment_cond = " tbl_treatment_log.ClientID=tbl_clients.ClientID WHERE BusinessID = $cldata[BusinessID]";
											$unique_treatment = $clientDetails->selectTableJoinNew("tbl_treatment_log","tbl_clients","INNER",$unique_treatment_cond, $unique_treatment_col);
											if($i%2==0){$bgdata = "bgnone";	}
												else{$bgdata = "bgdata";}
									?>
												<tr class="<?php echo $bgdata;?>" id="<?php echo $cldata['BusinessID']?>">
													<td class="span3 srtHeadEditEmp srtcontent"><label id="" class="user-name"><?php echo ucfirst($cldata['BusinessName']);?> </label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php if($plandata["Type"] == "Free") echo "Free Trial - "; echo $plandata["title"];?></label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $subscription_data['TransactionID']; ?></label></td>
													<td class="span6 srtHeadloc srtcontent">
														<label id="" class="user-name">
															<?php echo ($commu_pay[0]['Cumulative_Pay'] != 0) ? ('$'.$commu_pay[0]['Cumulative_Pay']) : ''; ?>
														</label>
													</td>
													<td class="span6 cMain "><label id="" class="user-name"><?php echo ($unique_client[0]['UniqueClient'] != 0) ? $unique_client[0]['UniqueClient'] : ''; ?></label></td>
													<td class="span6 cMain "><label id="" class="user-name"><?php echo ($unique_treatment[0]['UniqueTreatment']) ? $unique_treatment[0]['UniqueTreatment'] : ''; ?></label></td>
													<td class="span6 cMain ">
														<label id="" class="user-name">
															<?php 
															$dirpath="Business-Uploads/".$cldata['BusinessID']; 
															if(file_exists($dirpath))
																echo foldersize($dirpath)." MB";
															?>
														</label>
													</td>
												</tr><!--End @row-block-->
												<?php
												$i++;
										} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $clientDetails->paginateShowNew($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
