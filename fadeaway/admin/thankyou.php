<?php
	error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php'); ?>
	<!-- Page Wrapper -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script type="text/javascript" src="<?php echo $domain; ?>/js-new/app.js"></script>
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
		<?php
			include("../includes/dbFunctions.php");
			$clientDetails = new dbFunctions();
		?>
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->

			<!-- Begin Page Content -->
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0"></h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>
				<div class="card shadow mb-4 table-main-con">
					<div class="card-body">
						<div class="table-responsive">
							<div class="form-container">	
								<div class="thanksblocksmain">
									<h1>Thank you</h1>
									<h3>Thank you for completing this interview !</h3>
									<p>Please return this tablet to our office staff.</p>
								 </div>
							</div><!--End @form-container-->
						</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
