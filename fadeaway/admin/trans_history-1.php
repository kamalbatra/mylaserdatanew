<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	if( !in_array(10,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$history = new dbFunctions();
	$subtable = "tbl_subscription_history";
	$condition = "where PaymentStatus != 'Success' AND BusinessID=".$_SESSION["BusinessID"]." order by ID desc";
	$adjacents = 3;
	$reload="trans_history.php";
	$total_pages = $history->totalNumRows($subtable,$condition,$cols="*");
	if(isset($_GET['page'])) {
		$page=$_GET['page'];
	} else {
		$page="";
	}
	$limit = 5;                                  //how many items to show per page
	if($page)
		$start = ($page - 1) * $limit;          //first item to display on this page
	else
		$start = 0; 
	$condition2 = "where PaymentStatus != 'Success' AND BusinessID=".$_SESSION["BusinessID"]." order by ID desc limit ".$start.",".$limit;
	$subscription = $history->selectTableRows($subtable,$condition2,$cols="*");

?>
<style type="text/css">
	.srtHead { 
		width: 14%; 
	}
	.srno {
		width: 8%;
	}
	.span3 {
		text-align: center;
	}
	.trans {
		width: 20%;
	}
</style>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHeadReport">Transactions History</h1>
	</div>
	<div class="user-entry-block">
		<div class="tablebushead">	
	<div class="tablebusinner">	
		<div class="row sortingHead">	
			<div class="span3 srtHead srtHeadBorder srno">S.No.</div>
			<div class="span3 srtHead srtHeadBorder trans" style="width:25%">Transaction ID</div>
			<div class="span3 srtHead srtHeadBorder" style="width:20%">Membership Fees</div>
			<div class="span3 srtHead srtHeadBorder" style="width:27%">Duration</div>
			<div class="span3 srtHead srtHeadBorder" style="width:20%">Renewal Date</div>
		</div>        
	<?php
		if(!empty($subscription)){
		$i = 0;
		$srno=$start+1;
		foreach($subscription as $subdata){
			if($i%2==0) {
				$bgdata = "bgnone";	
			} else {
				$bgdata = "bgdata";
			}							
	?>	
			<div class="row  <?php echo $bgdata;?>" id="" >
				<div class="span3 srtHead srtcontent srno">
					<label id="" class="user-name"><?php echo $srno; ?> </label>						
				</div>					
				<div class="span3 srtHead srtcontent trans" style="width:25%">
					<label id="" class="user-name"><?php echo $subdata["TransactionID"]; ?></label>							
				</div>
				<div class="span3 srtHead srtcontent" style="width:20%">	
					<label id="" class="user-name">
						<?php echo "$".$subdata["PaymentAmount"]; ?>
					</label>
				</div>					
				<div class="span3 srtHead srtcontent" style="width:27%">	
					<label id="" class="user-name amt"><?php echo date("M j, Y", strtotime($subdata["RenewalDate"]))." - ".date("M j, Y", strtotime($subdata["ExpireDate"])); ?></label>
				</div>
				<div class="span3 srtHead srtcontent" style="width:20%">	
					<label id="" class="user-name"><?php echo date("M j, Y", strtotime($subdata["ExpireDate"])); ?></label>
				</div>
			</div><!--End @row-->				  
		<?php 
			$i++; $srno++;
		}
		?>
		<?php echo $history->paginateShow($page,$total_pages,$limit,$adjacents,$reload); ?>		
		<?php } else
			echo "<div class='not-found-data'>No transactions history found.</div>"; ?>
	</div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!-- End  @form-container--->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>
