<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	if( !in_array(10,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$history = new dbFunctions();
	$subtable = "tbl_subscription_history";
	$condition = "where PaymentStatus != 'Success' AND BusinessID=".$_SESSION["BusinessID"]." order by ID desc";
	$adjacents = 3;
	$reload="trans_history.php";
	$total_pages = $history->totalNumRows($subtable,$condition,$cols="*");
	if(isset($_GET['page'])) {
		$page=$_GET['page'];
	} else {
		$page="";
	}
	$limit = 5;                                  //how many items to show per page
	if($page)
		$start = ($page - 1) * $limit;          //first item to display on this page
	else
		$start = 0; 
	$condition2 = "where PaymentStatus != 'Success' AND BusinessID=".$_SESSION["BusinessID"]." order by ID desc limit ".$start.",".$limit;
	$subscription = $history->selectTableRows($subtable,$condition2,$cols="*");

?>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Transactions History</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock manageemp">
						<div class="busniss-search searchbussiness last">
							
						</div>
						<!-- div class="search-btn">
							<button>Search</button>
							<button class="addnewbtn"><img src="img/plus.png"> Add New</button>
						</div -->
					</div>		
					<div class="card-body">
					<?php
						if( !empty($subscription) ) {
							$i = 0;
							$srno=$start+1;
					?>
						<div class="table-responsive">
							<table class="table table-bordered bussinessTable" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>S. N.</th>
										<th>Transaction ID</th>
										<th>Membership Fees</th>
										<th>Duration</th>
										<th>Renewal Date</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach( $subscription as $subdata ) {
											if($i%2==0) {
												$bgdata = "bgnone";	
											} else {
												$bgdata = "bgdata";
											}
									?>
												<tr class="treatment <?php echo $bgdata;?>" id="">
													<td class="span3 srtHeadEditEmp srtcontent"><label id="" class="user-name"><?php echo $srno; ?> </label></td>
													<td class="span6 srtHead srtcontent"><label id="" class="user-name"><?php echo $subdata["TransactionID"]; ?></label></label></td>
													<td class="span6 srtHead srtcontent">
														<label id="" class="user-name">
															<?php echo "$".$subdata["PaymentAmount"]; ?>
														</label>
													</td>
													<td class="span6 srtHeadloc srtcontent">
														<label id="" class="user-name amt"><?php echo date("M j, Y", strtotime($subdata["RenewalDate"]))." - ".date("M j, Y", strtotime($subdata["ExpireDate"])); ?></label>
													</td>
													<td class="span6 cMain ">
														<label id="" class="user-name"><?php echo date("M j, Y", strtotime($subdata["ExpireDate"])); ?></label>
													</td>
												</tr><!--End @row-block-->
												<?php
												$i++; $srno++;
										} //foreach end
									?>
								</tbody>
							</table>
						</div>
						<?php 
							echo $history->paginateShow($page,$total_pages,$limit,$adjacents,$reload);
						}
						else {
							echo "<div class='not-found-data'>No record found.</div>";
						}
						?>
					</div>
				</div>
				
			</div>
			<!-- /.container-fluid -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>