<?php
	//error_reporting(0);
	include("../includes/config.php");
	include("../includes/dbFunctions.php");
	$clients = new dbFunctions();
	if(isset($_POST['Wavelength'])) {
		if(count($_POST)>0) {	
			if($_POST['DeviceName'] !="") {
				if(isset($_POST['Tattoo_Photo_thumbnail']) && !empty($_POST['Tattoo_Photo_thumbnail'])) {
					$_POST['Tattoo_Photo_thumbnail'] = implode(',', $_POST['Tattoo_Photo_thumbnail']);
				} else {
					$_POST['Tattoo_Photo_thumbnail'] ='';
				}
				if(isset($_POST['Wavelength']) && !empty($_POST['Wavelength'])) {
					$_POST['Wavelength'] = implode(',', $_POST['Wavelength']);
				} else {
					$_POST['Wavelength'] ='';
				}				 
				if(isset($_POST['SpotSize']) && !empty($_POST['SpotSize'])) {
					$_POST['SpotSize'] = implode(',', $_POST['SpotSize']);
				} else {
					$_POST['SpotSize'] ='';
				}
				if(isset($_POST['Fluence']) && !empty($_POST['Fluence'])) {
					$_POST['Fluence'] = implode(',', $_POST['Fluence']);
				} else {
					$_POST['Fluence'] ='';
				}
				if(isset($_POST['TattoNumber']) && !empty($_POST['TattoNumber'])) {
					$_POST['TattoNumber']= $_POST['TattoNumber'];
				} else {
					$_POST['TattoNumber']= $_POST['existTatto'];
				}
				$tableConsent1 	= "tbl_treatment_log";
				$cols			= " ClientID,SessionNumber";
				$condition 		= " where ClientID=".$_POST['ClientID']." and  SessionNumber=".$_POST['SessionNumber']."  and TattoNumber='".$_POST['TattoNumber']. "'";				
				$checkExistence = $clients->selectTableSingleRow($tableConsent1,$condition,$cols);				

				if($checkExistence ==NULL) {
					$clients->insert_data($tableConsent1,$_POST);
					echo "<span>Information inserted successfully.</span>";
					$table1 = "tbl_treatment_log";
					$table2	= "tbl_clients";
					$join 	= "INNER";
					$cols 	= "AES_DECRYPT(tbl_clients.Email, '".SALT."') AS Email, AES_DECRYPT(tbl_clients.FirstName, '".SALT."') AS Client_First_Name, AES_DECRYPT(tbl_clients.LastName, '".SALT."') AS Client_Last_Name, MAX(tbl_treatment_log.Date_of_treatment) as DateOfTreatment";

					$condition_1 = "tbl_clients.ClientID = tbl_treatment_log.ClientID and tbl_clients.ClientID = ".$_POST['ClientID']; 
					$clientData	 = $clients->selectTableJoin($table1,$table2,$join,$condition_1,$cols);
					
					$email 		 = $clientData[0]['Email'];
					$fname 		 = $clientData[0]['Client_First_Name'];
					$lname 		 = $clientData[0]['Client_Last_Name'];
					$date 		 = $clientData[0]['DateOfTreatment'];
					
					//Sending fourth session mail...
					$fourth_session_email = ($_POST['SessionNumber'] == 4)?true:false;

					$clients->treat_email($fname,$lname,$email,$date,'tattoo',$fourth_session_email);
					
					// if(!empty($_POST['SessionNumber']) && $_POST['SessionNumber'] == 3){						
					// 	$clients->fourth_session_email($fname,$lname,$email,$date,'tattoo');
					// }

				} elseif($checkExistence !=NULL) {

					$clients->update_user($tableConsent1,$_POST);
					echo "<span>Information updated successfully.</span>";
				}
			}//DeviceName check   
			else {
				echo "<span style='color:red'>Please Select Device Name.</span>";
			}
		}
	} //if isset post
?>
