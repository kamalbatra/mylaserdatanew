<?php
	session_start();
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	if( !in_array(2,$_SESSION["menuPermissions"])){ ?> 

		<script>
			window.location.replace("dashboard");
		</script>
<?php 
	}
	$clients = new dbFunctions();
	$table = "tbl_clients";
	$choices = $clients->selectTableRows($table);
	if(isset($_GET['name'])) {  
?>
		<script>
			$(document).ready(function(){		  
				var str = 'name=<?php echo base64_decode($_GET['client']);?>';
				$.ajax({
					type: "GET",
					url: "livesearch-1.php",
					data: str,
					cache: false,
					success: function(result){
						$(".client-info").show();
						$("#myDiv").html(result);
					}
				});
			});
		</script>
<?php
	}
?>
<div class="mobile-menu">
	<img alt="mobile menu" src="<?php echo $domain; ?>/images/mobile-menu-pic.png" /><a href=""></a>
</div><!--End @mobile-menu-->
<div class="form-container">
	<h1 class="heading">Search for a client</h1>
	<form id="suggestSearch" action="">
		<div class="search-client-block block-main-client">
			<div class="row">
				<div class="span6">					
					<div class="row">
						<div class="span12 span-block-treat">
							<label for="FirstName" class="user-name">First name, last name or phone number:</label>
							<input class="text-input-field search_for_client" name="dbTxt" id="dbTxt" value="<?php if(isset($_GET['name'])) { echo base64_decode($_GET['name']);}?>" onKeyUp="searchSuggest();" autocomplete="off" type="text" />
							<div id="layer1"></div>
							<span id="errorMsgPrint" style="color:#FF0000;font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 13px;margin-top: -90px;"></span>
						</div>
					</div>
					<input type="submit" class="submit-btn" name="reviewClient" id="reviewClient" value="Review Consent Form"/>
					<?php if (in_array("1",$_SESSION["services"])) { ?>
					<br/><input type="submit" class="submit-btn" value="Treat for Tattoo" rel='1' name="treatClient" id="treatClient"/>				
					<?php }
						if (in_array("2",$_SESSION["services"])) { ?>
					<input type="submit" class="submit-btn" value="Treat for Hair" rel='2' name="treatClientHair" id="treatClientHair"/> <?php } ?>
					<div class="tech-signature">Consent Form Not Reviewed</div>				
				</div><!--End @span6-->
				<div class="span6 last" style="margin:0px;">
					<div class="row">
						<div class="span12" >								
							<div id="myDiv" style="z-index:1; margin-top:10px;"></div>					
						</div>							
					</div>					
				</div>
			</div><!--End @row-->
		</div>
	</form>
	<div class="search-report-block">
		<div id="mydiv2"></div>
	</div>
</div><!--End @form-container-->
</div><!--End @container-->
<?php
	include('admin_includes/footer.php');
?>
