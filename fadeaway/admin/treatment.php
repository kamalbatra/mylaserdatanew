<?php
	//~ error_reporting(0);
	session_start();
	$domain = $_SERVER['DOMAIN'];
	include('admin_includes/header-new.php');
	
	include("../includes/dbFunctions.php");
	$clients = new dbFunctions();
	if( !in_array(2,$_SESSION["menuPermissions"])){ ?> 
		<script>
			window.location.replace("dashboard.php");
		</script>
	<?php }
	$clients = new dbFunctions();
	$table = "tbl_clients";
	$choices = $clients->selectTableRows($table);
	if(isset($_GET['name'])) {  
?>
		<script>
			$(document).ready(function(){		  
				var str = 'name=<?php echo base64_decode($_GET['client']);?>';
				$.ajax({
					type: "GET",
					url: "livesearch.php",
					data: str,
					cache: false,
					success: function(result){
						$(".client-info").show();
						$("#myDiv").html(result);
					}
				});
			});
		</script>
<?php
	}
?>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Client Lookup</h1>
				</div>
				<div class="searchouterblk">
					<h2>Search For A Client</h2>
					<p>First name, last name or phone number:</p>
				</div>
				<div class="card shadow mb-4 searchblockClient">
					<div class="searchbodyblk">
						<img src="../img/searchicon.png">
						<input class="text-input-field search_for_client" name="dbTxt" id="dbTxt" value="<?php if(isset($_GET['name'])) { echo base64_decode($_GET['name']);}?>" onKeyUp="searchSuggest();" autocomplete="off" type="text" />
						<div id="layer1"></div>
						<span id="errorMsgPrint"></span>
						
					</div>
				</div>	
				<div id="myDiv" style="z-index:1; margin-top:10px;"></div>
				<div class="importantinformation">
					<div class="btnblocksfull">
						<div class="buttoncolblk">
							<input type="submit" class="submit-btn btnconsent" name="reviewClient" id="reviewClient" style="background:#4a78f0;" value="REVIEW CONSENT FORM"/>
							<!-- button class="btnconsent" style="background:#4a78f0;">REVIEW CONSENT FORM</button -->
						</div>
						<div class="buttoncolblk">
							<?php if (in_array("1",$_SESSION["services"])) { ?>
								<input type="submit" class="submit-btn btnconsent" name="treatClient" id="treatClient" style="background:#60e3a1;" value="TREAT FOR TATTOO" rel='1'/>
								<!-- button class="btnconsent" style="background:#60e3a1;">TREAT FOR TATTOO</button -->
						<?php } ?>
						</div>
						<div class="buttoncolblk">
							<?php
							if (in_array("2",$_SESSION["services"])) { ?>
								<input type="submit" class="submit-btn btnconsent" name="treatClientHair" id="treatClientHair" style="background:#ff772d;" value="TREAT FOR HAIR" rel='2'/>
							<?php 
							} ?>
								<!-- button class="btnconsent" style="background:#ff772d;">TREAT FOR HAIR</button -->
						</div>
						<!-- div class="buttoncolblk">
							<button class="btnconsent" style="background:#2a2a2a;">EDIT DETAILS</button>
						</div -->
					</div>
				</div>
				<div id="mydiv2"></div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
