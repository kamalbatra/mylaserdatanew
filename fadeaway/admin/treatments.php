<?php
//error_reporting(0);
include("../includes/dbFunctions.php");
$clients = new dbFunctions();
//TattoNumber
$domain=$_SERVER['DOMAIN'];
if(isset($_POST['ClientID']) &&  isset($_POST['TattoNumber'])){
	$Service = $_POST["Service"];
	$tableConsent1="tbl_treatment_log";
	$cols=" * ";
	$condition=" where ClientID=".$_POST['ClientID']." and TattoNumber='".$_POST['TattoNumber']."' ORDER BY SessionNumber DESC ";
	$checkExistence = $clients->selectTableRows($tableConsent1,$condition,$cols);
	?>	
	<link rel="stylesheet" href="<?php echo $domain; ?>/css/jquery.lightbox-0.5.css" media="screen"/>	
	<!-- library js file-->
	<script type="text/javascript" src="<?php echo $domain; ?>/js/fileUpload/jquery.min.js"></script>
	<script src="<?php echo $domain; ?>/js/lib/jquery.lightbox-0.5.js"></script>	
    <!-- accordian js-->   
	<script type="text/javascript" src="<?php echo $domain; ?>/js/lib/jquery.accordion.js"></script>	
	<script type="text/javascript">
	var acc = jQuery.noConflict();
	 acc(document).ready(function() {
		acc('.accordion').accordion({
			defaultOpen: 'section1',
			cookieName: 'accordion_nav'
		});
	  });
</script>	
	<script type='text/javascript'>
   // $(document).ready(function(){
	//	$(".lightbox").lightbox();
	//	});		
   var k = jQuery.noConflict();
	k(document).ready(function(){
		 k('.galleryBox').mouseover(function(){
		   var id = $(this).attr("id");	
	
        k("#"+id+" a").lightBox();
	  });
    });
    </script>	
	<?php  if($checkExistence!=NULL) { 		?>
		<h1 style="text-align: center;"><?php if($Service=="Tattoo") echo "Tattoo name :"; else if($Service=="Hair") echo "Hair Area:"; ?> <?php echo $_POST['TattoNumber'];?></h1>
		<?php		
		      $i=1;
				foreach($checkExistence as $data)
				{					
					// New format
					if($i%2==0){$bgdata = "bgdata";	}
					else{$bgdata = "bgnone";}
					?>					
					<div class="accordion" style="cursor:pointer;" id="section<?php echo $i; ?>">Session:<?php echo $data['SessionNumber']; ?><span></span></div>
					<div class="containerAcc">
						<div class="content content-main-row">
					<div class="row treatment">							
							<div class="span6" style="width:20%"><label id="Label1" class="user-name"> Technician name</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="" class="user-name"><?php echo $data['Emp_First_Name']." ".$data['Emp_Last_Name']; ?></label></div>							
						</div><!--End @row-->						
						<div class="row treatment">							
							<div class="span6" style="width:20%"><label id="Label1" class="user-name"> Date of treatment</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<?php 
							//$treatment=date_convert($data['Date_of_treatment'], 'UTC', 'Y-m-d H:i:s', 'CST', 'Y-m-d H:i:s');
							$date = new DateTime($data['Date_of_treatment']);
							$date->setTimezone(new DateTimeZone('CST')); // +04

							$treatment=$date->format('Y-m-d H:i:s'); // 2012-07-15 05:00:00 
							date_default_timezone_set(timezone_name_from_abbr("UTC"));
							//$timezone=strtotime($data['Date_of_treatment']);
							//echo date("Y-m-d H:i:s");
							//	echo $treatment=date("Y-m-d H:i:s", $timezone)."<br/>";
							//date_default_timezone_set(timezone_name_from_abbr("CST"));
							//echo date("Y-m-d H:i:s");
							?>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $treatment //$data['Date_of_treatment']; ?></label></div>							
						</div><!--End @row-->						
						<div class="row treatment ">
							<div class="span6" style="width:20%"><label id="Label1" class="user-name"> Session Id</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data['SessionID']; ?></label></div>							
						</div><!--End @row-->						
						<?php if($Service == "Hair") {
							$sizecon = " WHERE id = $data[Size]";
							$hairsize = $clients->selectTableSingleRow('tbl_master_sizes',$sizecon,'size');
							$areacon = " WHERE id = $data[Area]";
							$hairarea = $clients->selectTableSingleRow('tbl_master_areas',$areacon,'area');
						?>
						<div class="row treatment ">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name"> Hair Information</label></div>							
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data['HairType'].' '.$data['HairColour']; ?></label></div>							
						</div><!--End @row-->	
						<div class="row treatment ">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Size </label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo isset($hairsize['size']) ? $hairsize['size'] : ''; ?></label></div>							
						</div><!--End @row-->
						<div class="row treatment ">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Area </label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo isset($hairarea['area']) ? $hairarea['area'] : ''; ?></label></div>							
						</div><!--End @row-->
						<?php }else{ ?>
						<div class="row treatment ">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name"> Tattoo Information</label></div>							
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data['TattooInfo']; ?></label></div>							
						</div><!--End @row-->	
						<div class="row treatment ">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Size </label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data['Size']; ?></label></div>							
						</div><!--End @row-->
						<?php }?>
						<!-- Show wave Length-->
						<?php 
							$nbr =1;
							$spotSize = explode(",",$data['SpotSize']);
							$Wavelength = explode(",",$data['Wavelength']);								
							$deviceName = $data['DeviceName'];
							$condition1 = "where deviceId=".$deviceName;
							$device_res = $clients->selectTableSingleRow('tbl_devicename',$condition1,$cols="*");
							$fluenceName = explode(",",$data['Fluence']);
							if($Wavelength !=""){							
							for($wav =0; $wav< count($Wavelength);$wav++) {
								 ?>	
								 <div class="row treatment ">
									<div class="span6 " style="width:20%"><label id="Label1" class="user-name"> Device Name: <?php echo $nbr; ?></label></div>				
									<div class="span6 dot-span" style="width:5%"><label  class="user-name">:</label></div>
									<div class="span6 last "><label id="Label1" class="user-name"><?php echo $device_res['DeviceName'];?></label></div>							
								</div><!--End @row-->									 
								 <div class="row treatment ">
									<div class="span6 " style="width:20%"><label id="Label1" class="user-name"> Wavelength: <?php echo $nbr; ?></label></div>				
									<div class="span6 dot-span" style="width:5%"><label  class="user-name">:</label></div>
									<div class="span6 last "><label id="Label1" class="user-name"><?php echo $Wavelength[$wav];?></label></div>							
								</div><!--End @row-->								
								<!--Spot Size-->
								<?php for($sizeSpot=$wav;$sizeSpot<=$wav;$sizeSpot++) {?>
								<div class="row treatment ">
									<div class="span6 " style="width:20%"><label id="Label1" class="user-name"> Spot size: <?php echo $nbr; ?></label></div>				
									<div class="span6 dot-span" style="width:5%"><label  class="user-name">:</label></div>
									<div class="span6 last "><label id="Label1" class="user-name"><?php echo $spotSize[$sizeSpot];?>&nbsp;mm</label></div>							
								</div><!--End @row-->
								<?php } ?>
								<!-- spot Size end-->								
								<!--Fluence Name-->
								<?php for($fluName=$wav;$fluName<=$wav;$fluName++) {?>
								<div class="row treatment ">
									<div class="span6 " style="width:20%"><label id="Label1" class="user-name"> Fluence: <?php echo $nbr; ?></label></div>				
									<div class="span6 dot-span" style="width:5%"><label  class="user-name">:</label></div>
									<div class="span6 last "><label id="Label1" class="user-name"><?php echo $fluenceName[$fluName];?>&nbsp;<?php if(is_numeric($fluenceName[$fluName])){ echo "J/cm2";}?></label></div>							
								</div><!--End @row-->
								<?php } ?>
								<!-- Fluence name end-->
								<?php  
							$nbr++;	} } ?>
						<div class="row treatment">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Price</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data['Price']; ?></label></div>							
						</div><!--End @row-->						
						<div class="row treatment">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Treatment notes</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data['Treatment_notes']; ?></label></div>							
						</div><!--End @row-->						
						<div class="row treatment">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Complications</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name">
								<?php
									if( $data['blisters'] == "TRUE"  ){ echo "Blisters <br>";}
									if( $data['purpura'] == "TRUE"){echo "Purpura (pin point bleeding) <br>";}
									if( $data['pain'] == "TRUE" ){echo "Pain Tolerance Problems <br>";}
								?>
							</label>
						 </div>							
						</div><!--End @row-->
						<div class="row treatment">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Complications Others</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name"><?php echo $data['other']; ?></label></div>							
						</div><!--End @row-->						
						<div class="row treatment">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name">Office location</label></div>
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							<div class="span6 last "><label id="Label1" class="user-name">
								<?php if($data['Location']){
								$clentLocation = explode(",",$data['Location']);									
									 for($j=0 ; $j<count($clentLocation);$j++){										 
									$tbl_manage_location	= "tbl_manage_location";
									$condition1 = " where ManageLocationId=".$clentLocation[$j]." ";
									$cols="*";
									$locationData	= $clients->selectTableSingleRow($tbl_manage_location,$condition1);
									  echo $locationData['LocationName']."&nbsp;";									
								  }//for loop close
							  }								
								 ?>
								</label></div>							
						</div><!--End @row-->	
						
						<div class="row treatment">
							<div class="span6 " style="width:20%"><label id="Label1" class="user-name"> Tattoo Photo</label></div>				
							<div class="span6 dot-span" style="width:5%"><label id="Label1" class="user-name">:</label></div>
							
							
							<div class="galleryBox span6 last" id="gallery<?php echo $data['SessionNumber']; ?>" >
								<?php //echo $data['Tattoo_Photo_thumbnail'];
								$imgSrc = explode(",",$data['Tattoo_Photo_thumbnail']);
								if($imgSrc[0] !=""){
								for($k =0; $k < count($imgSrc);$k++){
									$date = new DateTime($data['Date_of_treatment']);
							$date->setTimezone(new DateTimeZone('CST')); // +04

							$treatment=$date->format('Y-m-d H:i:s'); // 2012-07-15 05:00:00 
							date_default_timezone_set(timezone_name_from_abbr("UTC"));
									 $filename = "Business-Uploads/".$_SESSION['BusinessID']."/thumbnail/". $imgSrc[$k];
							         if(file_exists($filename)) {  ?>								 
								 <a  href="Business-Uploads/<?php echo $_SESSION['BusinessID']; ?>/resize/<?php echo $imgSrc[$k]; ?>"  title="Session : <?php echo $data['SessionNumber']; ?>&nbsp; Date : <?php echo $treatment; ?>" >
								  <span style="padding-left:2px;"><img class="thumbBorder" src="Business-Uploads/<?php echo $_SESSION['BusinessID']; ?>/thumbnail/<?php echo $imgSrc[$k]; ?>"/></span>
								</a>
								<?php }
								
								 }//close for loop 
								}
								else{
									echo "No image";
							} 
								 ?>
								 
								 
							</div>	<!--span6 last-->						
						  </div><!--End @row-->
						
									  
						</div> <!-- content-->
						</div><!--End containeAcc-->
					<?php $i++;	 
				}   
				} else	{
				?>
				 <div class="row treatment bgdata">				
				 <div class="span6 "><label id="" class="user-name"> No Prior record found!</label></div>				 
				 </div>			
			<?php }
	}
?>
