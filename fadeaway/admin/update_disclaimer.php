<?php
session_start();
$doaminPath = $_SERVER['DOMAINPATH'];
$domain = $_SERVER['DOMAIN'];
include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php');
include("../includes/dbFunctions.php");
$mangeEmp = new dbFunctions();
if( !in_array(6,$_SESSION["menuPermissions"])) { ?> 
<script>
	window.location.replace("dashboard");
</script>
<?php 
}
$tbl_manage_location = "tbl_legal_disclaimer";
$condition = "WHERE bussines_id = $_SESSION[BusinessID] and user_id= $_SESSION[id] ORDER BY id  desc ";
$cols = "*";
$locationData = $mangeEmp->selectTableRows($tbl_manage_location,$condition);

?>	
<style>
	.right-margin-6 { margin-right: 6%; }
	.menu-checkbox{ float: left; width: 25%; }
	.addNewReport { float: right; }
	.formdonly {display:none;}
	.span6 { width: 100%;}
	.user-entry-block .user-name { width: 20%;}
	.mce-tinymce.mce-container.mce-panel { float: right; width: 75%;}
</style>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector: 'textarea',
		height: 250,
		theme: 'modern',
		plugins: [
		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		'searchreplace wordcount visualblocks visualchars code fullscreen',
		'insertdatetime media nonbreaking save table contextmenu directionality',
		'emoticons template paste textcolor colorpicker textpattern imagetools'
		],
		toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		toolbar2: 'print preview media | forecolor backcolor emoticons',
		image_advtab: true,
		templates: [
		{ title: 'Test template 1', content: 'Test 1' },
		{ title: 'Test template 2', content: 'Test 2' }
		],
		content_css: [
		'//www.tinymce.com/css/codepen.min.css'
		]
	});</script>
	<script>
		$(document).ready(function() {
			$('#Medicaldirector').change(function() {
				var val=$(this).val();

				if(val=='Yes'){
					$('.formdonly').show();	
				}else{
					$('.formdonly').hide();	
				}
			});	
		});
	</script>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<div class="newclient-outer">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="mb-0">Add Disclaimer</h1>
					</div>
				<div class="card shadow mb-4 table-main-con">
					<div class="bussiness-searchblock no-searchbox">
						<div class="search-btn">
							<a class="empLinks" href="categories" class="submit-btn"><button class="addnewbtn">Disclaimers List </button></a>
						</div>
					</div>
					<div class="formcontentblock-ld">						
					<form action="" name="insertLegalForm" id="insertLegalForm" method="post">
						<?php 
						if(empty($locationData)) { ?>
							<input type="hidden" name="hiddenversion" value="v1">
						<?php } else {

							$int = intval(preg_replace('/[^0-9]+/', '', $locationData[0]['version']), 10);
							$int++;
						
							if($int > 1){
								$date = date('Y-m-d');
							}
						?>
							<input type="hidden" name="hiddenversion" value="<?php echo 'v'.$int; ?>">
							<input type="hidden" name="hideLastId" value="<?php echo $locationData[0]['id']; ?>">
							<input type="hidden" name="hiddendateTo" value="<?php echo $date; ?>">
						<?php } ?>
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div>
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label id="Label1" class="user-name">Notice:</label>
													<textarea name="notice"></textarea>
												</div>
											</div>
										</div>
										<div>
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label id="Label1" class="user-name">Risks:</label>
													<textarea name="risks"></textarea>
												</div>
											</div>
										</div>
										<div>
											<div class="form-col-ld">
												<div class="inputblock-ld radiolabel">
													<label id="Label1" class="user-name">Liability Release:</label>
													<textarea name="legal"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<button type="button"  id="submitFormLegal" value="submit" class="submit-btn nextbtn" style="float:left;">Submit</button>
											<div id="insertResult" class="u_mess" style="display:none;float:left;padding:15px 5px;"><img alt="loading...." src="<?php echo $domain; ?>/images/loading.gif"></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
					</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
