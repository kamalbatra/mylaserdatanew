<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
$updateprofile = new dbFunctions();
$table1="tbl_business";
$table2="tbl_employees";
$creds1["BusinessID"]=$_POST["updatedId"];
$creds1["BusinessName"]=$_POST["bname"];
$creds1["Contact"]=$_POST["contact"];
$creds1["LiabilityRelease"]=$_POST["LiabilityRelease"];
$creds1["Address"]=$_POST["Address"];
$creds1["City"]=$_POST["City"];
$creds1["State"]=$_POST["State"];
$creds1["Country"]=$_POST["Country"];

$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
$logofile = "";
if($_FILES["logofile"]["name"] != ""){
	$filename=$_FILES["logofile"]["name"];
	$ext=end((explode(".",$filename)));
	$logofile="businesslogo".time().".".$ext;
	if(!in_array($ext,$valid_formats)){
		?>
		<script>
			$(function(){
				window.location.replace("profile?id=<?php echo $_POST["updatedId"]; ?>&msg=failed");
				});
		</script>
		<?php
		die;
	}
	$creds1["Logo"]=$logofile;
}
$updateprofile->update_spot($table1,$creds1);
$path="Business-Uploads/".$_POST["updatedId"];
if($logofile != ""){
	include("admin_includes/image_resize.php");
	$max_width = 950;
	$max_height = 222;
	list($orig_width, $orig_height) = getimagesize($_FILES['logofile']['tmp_name']);
	$width = $orig_width;
	$height = $orig_height;
	
	if ($height > $max_height) {
		$width = ($max_height / $height) * $width;
		$height = $max_height;
	}

	# wider
	if ($width > $max_width) {
		$height = ($max_width / $width) * $height;
		$width = $max_width;
	}
	resize($width, $height, $path."/".$logofile);
	$_SESSION['businessdata']['Logo'] = $logofile;
	//move_uploaded_file($_FILES["logofile"]["tmp_name"],$path."/".$logofile);
}

$creds2["Emp_ID"]=$_POST["empId"];	
$creds2["First_Name"]=$_POST["fname"];	
$creds2["Last_Name"]=$_POST["lname"];	
$updateprofile->update_spot($table2,$creds2);
?>
<script>
	$(function(){
		window.location.replace("profile?id=<?php echo $_POST["updatedId"]; ?>&msg=update");
		});
</script>
