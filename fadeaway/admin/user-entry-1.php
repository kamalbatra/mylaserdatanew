﻿<?php
include('admin_includes/header.php');
include("../includes/dbFunctions.php");
?>
<!--script type="text/javascript" src="https://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.js" ></script-->
<script type="text/javascript" src="<?php echo $domain; ?>/js/date-plugin.js" ></script>
<script>
	jQuery(function($){
		$('#birthdate').formatDate("mm/dd/yyyy");
		//$("#birthdate").mask("99/99/9999");
		//$("#birthdate").mask("99/99/9999", {selectOnFocus: false});
		$('#ReferralSource').change(function() {
			 $('.friendShopName').hide();
			 $('#ReferralSourceName').val('');
		 	if($(this).val()=='Friend')
		 	{
				$('.friendShopName label').html('Friend Name');
			  $('.friendShopName').show();
			}
			
		 	if($(this).val()=='Tattoo Artist')
		 	{
				$('.friendShopName label').html('Name of Shop');
			  $('.friendShopName').show();
			}
			if($(this).val()=='Other')
		 	{
				$('.friendShopName label').html(' ');
			  $('.friendShopName').show();
			}
			
		});
	});
	var today = new Date()
    function _calcAge() {
		$(".showhideinvaliddate").hide();
		var enteded_date = document.getElementById("birthdate").value;
		enteded_date = enteded_date.replace(/ /g,'');
		if(enteded_date != ''){
			if(/([0-2][0-9])\/([0-2][0-9]|[3][0-1])\/((19|20)[0-9]{2})/.test(enteded_date)){
				var date = enteded_date.split("/");
				var d = parseInt(date[1], 10),
					m = parseInt(date[0], 10),
					y = parseInt(date[2], 10);
				var currentYear = (new Date).getFullYear();
				if(y>currentYear || m>12 || m<1 || d<1 || d>31 || (m==2 && d>29)){
					$(".showhideinvaliddate").show();
					document.getElementById("Age").value='';
					return false;
				}
				var firstdate=new Date(date[2],date[0],date[1]);
				var dayDiff = Math.ceil(today.getTime() - firstdate.getTime()) / (1000 * 60 * 60 * 24 * 365);
				var age = parseInt(dayDiff);
				/*var dt1 = new Date(document.getElementById("birthdate").value);
				var birthDate = new Date(dt1);
				var years = (givenDate.getFullYear() - birthDate.getFullYear());*/
				document.getElementById("Age").value=age;
			}else
				$(".showhideinvaliddate").show();
		}else
			document.getElementById("Age").value='';
	}

</script>
<?php
if( !in_array(1,$_SESSION["menuPermissions"])){ ?> 
	<script>
		window.location.replace("dashboard");
	</script>
<?php }
$refferalChoices = new dbFunctions();

$table	= "tbl_refferal_choices";
$choices = $refferalChoices->selectTableRows($table);

?>	
	<div class="mobile-menu">
		<img alt="mobile menu" src="<?php echo $domain; ?>/images/mobile-menu-pic.png" /><a href=""></a></div><!--End @mobile-menu-->
	<div class="form-container">
		<h1 class="heading">New Client Entry</h1>
		<form action="fullInfo.php" method="post" id="add_page_form" name="add_page_form">
			<?php if(isset($_GET['entry']) && $_GET['entry']=='fails'){ echo "<p style='color: #FF0000;font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 13px;margin-bottom:13px;margin-top:-12px;'>"."This email id already exists!"."</p>"; } ?>
		<div class="user-entry-block">
			<div class="row">
				<div class="span6">
					<label for="FirstName" class="user-name">First Name:</label>
					<input class="text-input-field" name="FirstName" id="FirstName" type="text" autocomplete="off" />
				</div>				
				<div class="span3">
					<label for="LastName" class="user-name">Last Name:</label>
					<input class="text-input-field" name="LastName" id="LastName" type="text" autocomplete="off" />
				</div>
			</div><!--End @row-->			
			<div class="row">
				<div class="span6 dob">
					<span id="gender" class="user-name">Date of birth <span style="text-transform:none">(MM/DD/YYYY)</span>:</span>
					<input type="tel" name="DOB" onchange="_calcAge()" id="birthdate" class="text-input-dob" autocomplete="off" placeholder="__ / __ / ____"/>
					<label for="birthdate" style="display:none;color: red;float: left;font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 12px;margin-bottom: 5px;width: 62%;" class="showhideinvaliddate">Please enter a valid date.</label>
				</div>
				<div class="span3">
					<label for="Age" class="user-name">Age (Years):</label>
					<input class="text-input-field" name="Age" id="Age" type="text" autocomplete="off" readonly />
				</div>
			</div><!--End @row-->			
			<div class="row">
			<div class="span6">
					<label for="Occupation" class="user-name">Occupation:</label>
					<input class="text-input-field" name="Occupation" id="Occupation" type="text" autocomplete="off" />
				</div>
			</div><!--End @row-->			
			<div class="row">
				<div class="span6">
					<label for="Address1" class="user-name">Address1:</label>
					<textarea name="Address1" id="Address1" cols="20" rows="4" class="text-area-field"></textarea>
				</div>				
				<div class="span6 last">
					<label for="Address2" class="user-name">Address2:</label>
					<textarea name="Address2" id="Address2" cols="20" rows="4" class="text-area-field"></textarea>
				</div>
			</div><!--End @row-->			
			<div class="row">
				<div class="span6">
					<label for="City" class="user-name">City:</label>
					<input class="text-input-field" name="City" id="City" type="text" autocomplete="off" />
					</div>				
				<div class="span3 last">
					<label for="State" class="user-name">State:</label>
					<input class="text-input-field" name="State" id="State" type="text" autocomplete="off" />
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label for="Zip" class="user-name">Zip:</label>
					<input class="text-input-field" name="Zip" id="Zip" type="text" autocomplete="off"/>
				</div>				
				<div class="span3 last">
					<label for="PhoneNumber" class="user-name">Phone Number:</label>
					<input class="text-input-field" name="PhoneNumber" id="PhoneNumber" type="text" autocomplete="off" />
				</div>
			</div><!--End @row-->
			<div class="row">
				<div class="span6">
					<label for="Email" class="user-name">Email:</label>
					<input class="text-input-field" name="Email" id="Email" type="text" autocomplete="off" />
				</div>
			</div><!--End @row-->			
			<div class="row">
			<div class="span6">
					<label for="Email" class="user-name">How did you hear about us?:</label>
					<select class="text-input-field select-option" name="ReferralSource" id="ReferralSource">
						<option value="">Select an option</option>
					<?php
						foreach($choices as $refferal){
							$val="";
						if($refferal['ReferralID']==1)
						{
						 $val="disabled";
						}	
						if($refferal['parent']==0)
						{
					?>
					
					<option <?php echo $val; ?> value="<?=$refferal['Referrer'];?>">
					<?=$refferal['Referrer'];?>
					</option>
					
					<?php
					}
					foreach($choices as $ref)
							{
							
						if($refferal['ReferralID']==$ref['parent'])
								{ ?>
								<option value="<?=$ref['Referrer'];?>">
									--<?=$ref['Referrer'];?>
								</option>
								<?php }
						} 
					}
					?>
					</select>
				</div>
				<div class="span3 last friendShopName" style="display:none;">
					<label class="user-name friendShopNamelevel" for="PhoneNumber">Friend Name:</label>
					<input id="ReferralSourceName" class="text-input-field" type="text" autocomplete="off" name="ReferralSourceName">
				</div>
				
			</div><!--End @row-->	
			
			
			<!--div class="span3 last">
<label class="user-name" for="PhoneNumber">Phone Number:</label>
<input id="PhoneNumber" class="text-input-field" type="text" autocomplete="off" name="PhoneNumber">
</div-->
			<input type="hidden" name="Consent" id="Consent" value="FALSE"/>
			<input type="hidden" name="Minneapolis" id="Minneapolis" value="<?=$_SESSION['Emp_Location_Minneapolis']?>"/>
			<input type="hidden" name="St_Paul" id="St_Paul" value="<?=$_SESSION['Emp_Location_St_Paul']?>"/>
			<input type="hidden" name="Duluth" id="Duluth" value="<?=$_SESSION['Emp_Location_Duluth']?>"/>
			<input type="hidden" name="Emp_Name" id="Emp_Name" value="<?=$_SESSION['First_Name']?>"/>
			<input type="hidden" name="Emp_Last_Name" id="Emp_Last_Name" value="<?=$_SESSION['Last_Name']?>"/>
			<input type="hidden" name="TimeStamp" id="TimeStamp" value="<?=date('Y-m-d H:i:s'); ?>"/>
			<input type="hidden" name="Location" id="Location" value="<?=$_SESSION['Location']?>"/>		
			<div class="row">
				<div class="span12">
					<input type="submit" class="submit-btn" value="Submit"/>				
				</div>			
			</div><!--End @row-->		
		</div><!--End @user-entry-block-->
		</form>
	</div><!--End @form-container-->
	</div><!--End @container-->	
	<?php
	include('admin_includes/footer.php');
	?>
