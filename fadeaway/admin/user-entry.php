﻿<?php
	//~ error_reporting(0);
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include($doaminPath.'/fadeaway/admin/admin_includes/header-new.php'); ?>
	<!-- Page Wrapper -->
	<div id="wrapper">
    <!-- Sidebar -->
    <?php  include($doaminPath.'/fadeaway/admin/admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include($doaminPath.'/fadeaway/admin/admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<?php
			include("../includes/dbFunctions.php");
			?>
			<script type="text/javascript" src="<?php echo $domain; ?>/js/date-plugin.js" ></script>
			<script>
				jQuery(function($){
					
					//~ $('#threepartremove').removeClass();
					$('#birthdate').formatDate("mm/dd/yyyy");
					$('#ReferralSource').change(function() {
						$('.friendShopName').hide();
						$('#ReferralSourceName').val('');
						
						if($(this).val()=='Friend') {
							$('#threepartremove').addClass('threeparts');
							$('.friendShopName label').html('Friend Name');
							$('.friendShopName').show();
						}
						else if($(this).val()=='Tattoo Artist') {
							$('#threepartremove').addClass('threeparts');
							$('.friendShopName label').html('Name of Shop');
							$('.friendShopName').show();
						}
						else if($(this).val()=='Other') {
							$('#threepartremove').addClass('threeparts');
							$('.friendShopName label').html(' ');
							$('.friendShopName').show();
						} else {
							$('#threepartremove').removeClass('threeparts');		
						}
					});
				});
				var today = new Date()
				function _calcAge() {
					$(".showhideinvaliddate").hide();
					var enteded_date = document.getElementById("birthdate").value;
					enteded_date = enteded_date.replace(/ /g,'');
					if(enteded_date != ''){
						if(/([0-2][0-9])\/([0-2][0-9]|[3][0-1])\/((19|20)[0-9]{2})/.test(enteded_date)){
							var date = enteded_date.split("/");
							var d = parseInt(date[1], 10),
								m = parseInt(date[0], 10),
								y = parseInt(date[2], 10);
							var currentYear = (new Date).getFullYear();
							if(y>currentYear || m>12 || m<1 || d<1 || d>31 || (m==2 && d>29)){
								$(".showhideinvaliddate").show();
								document.getElementById("Age").value='';
								return false;
							}
							var firstdate=new Date(date[2],date[0],date[1]);
							var dayDiff = Math.ceil(today.getTime() - firstdate.getTime()) / (1000 * 60 * 60 * 24 * 365);
							var age = parseInt(dayDiff);
							document.getElementById("Age").value=age;
						} else {
							$(".showhideinvaliddate").show();
						}
					} else {
						document.getElementById("Age").value='';
					}
				}
			</script>
			<?php
			if( !in_array(1,$_SESSION["menuPermissions"])) { ?> 
				<script>
					window.location.replace("dashboard");
				</script>
			<?php	
			}
			$refferalChoices = new dbFunctions();
			$table	= "tbl_refferal_choices";
			$choices = $refferalChoices->selectTableRows($table);
			?>			
			<!-- Begin Page Content -->
			<div class="container-fluid">
					<?php if(isset($_GET['entry']) && $_GET['entry']=='fails'){
						echo "<div class='updatebusstatus'><div class='errormessage' id='u_mess'>"."This email id already exists!"."</div></div>";
					} else if(isset($_GET['entry']) && $_GET['entry']=='notsubmit'){
						echo "<div class='updatebusstatus'><div class='errormessage' id='u_mess'>"."Unable to submit form please try again later!"."</div></div>";
					}
					?>
				<div class="newclient-outer">
					<div class="headingmain">
						New Client Entry
					</div>
					<form action="fullInfo.php" method="post" id="add_page_form" name="add_page_form">
						<div class="new-client-block-content">
							<div class="formClientBlock">
								<div class="form-headingblock">
									<span class="numberingblock">1</span>
									<span>Personal Information</span>
									<div class="formcirblocks">
										<div class="formcircle active"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
										<div class="formcircle"></div>
									</div>
								</div>
								
								<div class="formcontentblock-ld">
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>First Name</label>
													<input class="text-input-field" placeholder="Aaron" name="FirstName" id="FirstName" type="text" autocomplete="off" />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Last Name</label>
													<input class="text-input-field" placeholder="Aaron" name="LastName" id="LastName" type="text" autocomplete="off" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="threeparts">
												<div class="form-col-ld ">
													<div class="inputblock-ld">
														<label>Date Of Birth (MM/DD/YYYY) </label>
														<input type="tel" name="DOB" onchange="_calcAge()" id="birthdate" class="text-input-dob" autocomplete="off" placeholder="__ / __ / ____"/>
														<label for="birthdate" style="display:none;color: red;float: left;font-family: Verdana,Geneva,Tahoma,sans-serif;font-size: 12px;margin-bottom: 5px;width: 62%;" class="showhideinvaliddate">Please enter a valid date.</label>
													</div>
												</div>
											</div>
											<div class="onepart">
												<div class="form-col-ld ">
													<div class="inputblock-ld">
														<label>Age</label>
														<input class="text-input-field" name="Age" id="Age" type="text" autocomplete="off" readonly />
													</div>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Occupation</label>
													<input class="text-input-field" name="Occupation" id="Occupation" type="text" autocomplete="off" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Address1</label>
													<textarea name="Address1" id="Address1" cols="20" rows="4" class="text-area-field"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="full">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Address2</label>
													<textarea name="Address2" id="Address2" cols="20" rows="4" class="text-area-field"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>City</label>
													<input class="text-input-field" name="City" id="City" type="text" autocomplete="off" />
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>State</label>
													<input class="text-input-field" name="State" id="State" type="text" autocomplete="off" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Zip</label>
													<input class="text-input-field" name="Zip" id="Zip" type="text" autocomplete="off"/>
												</div>
											</div>
										</div>
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Phone Number</label>
													<input class="text-input-field" name="PhoneNumber" id="PhoneNumber" type="text" autocomplete="off" maxlength="10"/>
												</div>
											</div>
										</div>
									</div>

									<div class="form-row-ld">
										<div class="half">
											<div class="form-col-ld">
												<div class="inputblock-ld">
													<label>Email:</label>
													<input class="text-input-field" name="Email" id="Email" type="text" autocomplete="off" />
												</div>
											</div>
										</div>
										
										<div class="half">
											<div id="threepartremove" class="">
												<div class="form-col-ld ">
													<div class="inputblock-ld">
														<label>How Did You Hear About Us?</label>
														<select class="text-input-field select-option" name="ReferralSource" id="ReferralSource">
															<option value="">Select an option</option>
															<?php
															foreach($choices as $refferal){
																$val="";
																if($refferal['ReferralID']==1) {
																	$val="disabled";
																}
																if($refferal['parent']==0) {
																?>
																<option <?php echo $val; ?> value="<?=$refferal['Referrer'];?>"><?=$refferal['Referrer'];?></option>
																<?php
																}
																foreach($choices as $ref) {
																	if($refferal['ReferralID']==$ref['parent'])  {  ?>
																		<option value="<?=$ref['Referrer'];?>">--<?=$ref['Referrer'];?></option>
																<?php 
																	}
																} 
															} 
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="onepart">
												<div class="form-col-ld ">
													<div class="inputblock-ld last friendShopName" style="display:none;">
														<label class="user-name friendShopNamelevel" for="PhoneNumber">Friend Name:</label>
														<input id="ReferralSourceName" class="text-input-field" type="text" autocomplete="off" name="ReferralSourceName">
													</div>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="Consent" id="Consent" value="FALSE"/>
									<input type="hidden" name="Minneapolis" id="Minneapolis" value="<?=$_SESSION['Emp_Location_Minneapolis']?>"/>
									<input type="hidden" name="St_Paul" id="St_Paul" value="<?=$_SESSION['Emp_Location_St_Paul']?>"/>
									<input type="hidden" name="Duluth" id="Duluth" value="<?=$_SESSION['Emp_Location_Duluth']?>"/>
									<input type="hidden" name="Emp_Name" id="Emp_Name" value="<?=$_SESSION['First_Name']?>"/>
									<input type="hidden" name="Emp_Last_Name" id="Emp_Last_Name" value="<?=$_SESSION['Last_Name']?>"/>
									<input type="hidden" name="TimeStamp" id="TimeStamp" value="<?=date('Y-m-d H:i:s'); ?>"/>
									<input type="hidden" name="Location" id="Location" value="<?=$_SESSION['Location']?>"/>
									<div class="form-row-ld">
										<div class="backNextbtn">
											<!-- button class="backbtn">Back</button -->
											<button class="nextbtn">Next <img src="../img/arrownext.png"></button>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
	<?php	
	include($doaminPath.'/fadeaway/admin/admin_includes/footer-new.php');	
	?>
