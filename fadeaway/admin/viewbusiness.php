<?php
	include("../includes/dbFunctions.php");
	$business = new dbFunctions();
	
	if(isset($_GET['id']) && $_GET['id'] != "") {
		$tbl_manage_protocols = "tbl_protocols";
		$condition = " left join tbl_employees as e on e.Emp_ID=tbl_protocols.uploaded_by where tbl_protocols.bussiness_id=".$_GET['id']." and tbl_protocols.status='1' ORDER BY tbl_protocols.id DESC";
		$cols = " tbl_protocols.protocol_name, tbl_protocols.status, tbl_protocols.uploaded_on, e.First_Name, e.Last_Name, tbl_protocols.id ";
		$protcolData = $business->selectTableRows($tbl_manage_protocols,$condition,$cols);
		if(!empty($protcolData))
		{
			$_SESSION['protocol_status']=1;
		}
		$location = mysqli_fetch_object(mysqli_query($business->con, "SELECT First_Name, Last_Name, Location FROM tbl_employees WHERE BusinessID = $_GET[id]"));
		$_SESSION['Loggedinas'] = ($location->First_Name == "" && $location->Last_Name == "") ? '-' : ($location->First_Name.' '.$location->Last_Name);
		$_SESSION['Location'] = $location->Location;
		$_SESSION['Superadmin'] = 0;
		$_SESSION['BusinessID'] = $_GET['id'];
		$table = "tbl_business";
		$condition = " WHERE BusinessID = $_SESSION[BusinessID]";
		$cols = " BusinessName, Logo, Contact, LiabilityRelease, serviceId, created_date, sendy_brand_id , BusinessID ";
		$_SESSION['businessdata'] = $business->selectTableSingleRow($table,$condition, $cols);
		$_SESSION['services'] = explode(",",$_SESSION['businessdata']['serviceId']);
		$_SESSION['SendyBrandID'] = $_SESSION['businessdata']['sendy_brand_id'];
	} else {
		unset($_SESSION['businessdata']);
		unset($_SESSION['protocol_status']);
		$location = mysqli_fetch_object(mysqli_query($business->con, "SELECT Fist_Name, Last_Name, Location FROM tbl_employees WHERE Superadmin = 1"));
		$_SESSION['Location'] = $location->Location;
		$_SESSION['Superadmin'] = 1;
		$_SESSION['BusinessID'] = 0; 
		unset($_SESSION['services']);
		unset($_SESSION['SendyBrandID']);
	}
	header("Location:dashboard");
?>

