<?php
	include('admin_includes/header.php');
	include("../includes/dbFunctions.php");
	if( !in_array(11,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$yoursub = new dbFunctions();
	$subtable = "tbl_subscription_history s";
	$plantable = "tbl_master_plans p";
	$condition = "s.PlanID=p.id where s.BusinessID=".$_SESSION["BusinessID"]." order by s.ID desc limit 0,1";
	$subdata = $yoursub->selectTableJoin($subtable,$plantable, $join="", $condition,$cols="*");
?>
<style type="text/css">
	.srtHead { 
		width: 20%; 
	}
	.span3 {
		text-align: center;
	}
	.heading-container h1.heading {
		width: 40%;
	}
	.addNewReport {
		width: 30%;
	}
	.bgdata:hover {
		background: none repeat scroll 0 0 #f0f0f0;
	}
	.bgnone:hover {
		background: none;
	}
</style>
<div class="form-container">
	<div class="heading-container">
		<h1 class="heading empHeadReport">Subscription Status</h1>
		<?php if(isset($_SESSION['loginuser']) && $_SESSION['loginuser'] != "sitesuperadmin") {?>
		<!--div class="addNewReport"><a class="empLinks" href="subscription.php" class="submit-btn" onclick="return confirm('Are you sure you want to renew your plan now?')">Renew Your Plan</a></div>
		<div class="addNewReport"><a class="empLinks" href="Optout.php?sub=yes" onclick="return confirm('Are you sure you want to withdraw your membership from fadeaway? If you withdraw, then you will not use your account from next billing cycle.')">Withdraw Membership</a></div-->
		<?php }?>
	</div>
	<div class="user-entry-block">
		<div class="row bgdata">
			<div class="span3 srtHead srtcontent title">
				<label class="user-name" style="text-align:right;font-weight:bold">Plan : </label>						
			</div>					
			<div class="span3 srtHead srtcontent" style="width:30%;text-align:left;padding-left:5%">	
				<label id="" class="user-name"><?php echo $subdata[0]["title"]; ?> </label>
			</div>
		</div><!--End @row-->
		<div class="row bgnone">
			<div class="span3 srtHead srtcontent title">
				<label class="user-name" style="text-align:right;font-weight:bold">Membership Fees : </label>						
			</div>					
			<div class="span3 srtHead srtcontent" style="width:30%;text-align:left;padding-left:5%">	
				<label id="" class="user-name"><?php echo ($subdata[0]["PaymentAmount"] != 0) ? ("$".$subdata[0]["PaymentAmount"]) : "Free"; ?> </label>
			</div>
		</div><!--End @row-->
		<div class="row bgdata">
			<div class="span3 srtHead srtcontent title">
				<label class="user-name" style="text-align:right;font-weight:bold">Duration : </label>						
			</div>					
			<div class="span3 srtHead srtcontent" style="width:40%;text-align:left;padding-left:5%">	
				<label id="" class="user-name" style="text-transform:none"><?php echo date("F j, Y", strtotime($subdata[0]["RenewalDate"]))."&nbsp;&nbsp;to&nbsp;&nbsp;".date("F j, Y", strtotime($subdata[0]["ExpireDate"])); ?> </label>
			</div>
		</div><!--End @row-->
		<div class="row bgnone" style="margin-bottom:2%;">
			<div class="span6 srtHead srtcontent title">
				<label class="user-name" style="text-align:right;font-weight:bold">Renewal Date : </label>						
			</div>					
			<div class="span3 srtHead srtcontent" style="width:30%;text-align:left;padding-left:5%">	
				<label id="" class="user-name"><?php echo date("M j, Y", strtotime($subdata[0]["ExpireDate"])); ?></label>
			</div>
		</div><!--End @row-->
		<?php if(isset($_SESSION['loginuser']) && $_SESSION['loginuser'] != "sitesuperadmin") {?>
		<div class="row bgnone" style="border-top: 1px dotted #666666;">
			<div class="span6 srtHead srtcontent title">
				<label class="user-name" style="text-align:center;font-weight:bold;background:#11719f;padding:7px 10px"><a class="empLinks" href="subscription.php" class="submit-btn" style="color:#ffffff" onclick="return confirm('Are you sure you want to renew your membership now?')">Renew Membership</a></label>						
			</div>					
			<div class="span3 srtHead srtcontent" style="width:30%;text-align:left;padding-left:5%;font-weight:bold;">	
				<label id="" class="user-name" style="text-align:center;font-weight:bold;background:#11719f;padding:7px 0px"><a class="empLinks" style="color:#ffffff" href="Optout.php?sub=yes" onclick="return confirm('Are you sure you want to cancel membership from fadeaway? If you cancel, then you will not use your account from next billing cycle.')">Cancel Membership</a></label>
			</div>
		</div><!--End @row-->
		<?php } ?>
	</div><!--End @user-entry-block-->
</div><!-- End  @form-container--->
</div><!--End @container-->
<?php include('admin_includes/footer.php');	?>

