<?php
	session_start();
	$doaminPath = $_SERVER['DOMAINPATH'];
	$domain = $_SERVER['DOMAIN'];
	include('admin_includes/header-new.php');
	include("../includes/dbFunctions.php");
	if( !in_array(11,$_SESSION["menuPermissions"])) { ?> 
		<script>
			window.location.replace("dashboard");
		</script>
	<?php 
	}
	$yoursub = new dbFunctions();
	$subtable = "tbl_subscription_history s";
	$plantable = "tbl_master_plans p";
	$condition = "s.PlanID=p.id where s.BusinessID=".$_SESSION["BusinessID"]." order by s.ID desc limit 0,1";
	$subdata = $yoursub->selectTableJoin($subtable,$plantable, $join="", $condition,$cols="*");
?>
<style type="text/css">
	.srtHead { 
		width: 20%; 
	}
	.span3 {
		text-align: center;
	}
	.heading-container h1.heading {
		width: 40%;
	}
	.addNewReport {
		width: 30%;
	}
	.bgdata:hover {
		background: none repeat scroll 0 0 #f0f0f0;
	}
	.bgnone:hover {
		background: none;
	}
</style>
<div id="wrapper">
    <!-- Sidebar -->
    <?php  include('admin_includes/sidebar.php');  ?>
    <!-- End of Sidebar -->
	<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<!-- Topbar -->
			<?php  include('admin_includes/topbar.php');  ?>
			<!-- End of Topbar -->
			<!-- Begin Page Content -->
			<div class="container-fluid all-bussiness">
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="mb-0">Subscription Status</h1>
					<a href="#" class="btn-latest-activities"> <span class="latest-activities-text">Latest Activities</span> <i class="fas fa-chart-line"></i> </a>
				</div>	
				<div class="searchReportBlk">
					<div class="searchReportOuterblk">
						<div class="half">
							<div class="card shadow mb-4 searchReportcard">
								<div class="searchreportrow">
									<div class="serachreportTital">Plan<b>:</b></div>
									<div class="searchReportvalue"><?php echo $subdata[0]["title"]; ?></div>
								</div>
								<div class="searchreportrow">
									<div class="serachreportTital">Membership Fees<b>:</b></div>
									<div class="searchReportvalue"><?php echo ($subdata[0]["PaymentAmount"] != 0) ? ("$".$subdata[0]["PaymentAmount"]) : "Free"; ?></div>
								</div>
							</div>
						</div>
						<div class="half">
							<div class="card shadow mb-4 searchReportcard">
								<div class="searchreportrow">
									<div class="serachreportTital">Duration<b>:</b></div>
									<div class="searchReportvalue"><?php echo date("F j, Y", strtotime($subdata[0]["RenewalDate"]))."&nbsp;&nbsp;to&nbsp;&nbsp;".date("F j, Y", strtotime($subdata[0]["ExpireDate"])); ?></div>
								</div>
								<div class="searchreportrow">
									<div class="serachreportTital">Renewal Date<b>:</b></div>
									<div class="searchReportvalue"><?php echo date("M j, Y", strtotime($subdata[0]["ExpireDate"])); ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php if(isset($_SESSION['loginuser']) && $_SESSION['loginuser'] != "sitesuperadmin") { ?>
					<div class="importantinformation">
						<div class="btnblocksfull">
							<div class="buttoncolblk">
								<a class="edit-btn" href="subscription.php" class="submit-btn" style="color:#ffffff" onclick="return confirm('Are you sure you want to renew your membership now?')">Renew Membership</a>
							</div>
							<div class="buttoncolblk">
								<a class="edit-btn" style="color:#ffffff" href="Optout.php?sub=yes" onclick="return confirm('Are you sure you want to cancel membership from fadeaway? If you cancel, then you will not use your account from next billing cycle.')">Cancel Membership</a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
			<!-- /.container-fluid -->
			<div id="statuResult"></div>
		</div>
		<!-- End of Main Content -->
	<?php	
	include('admin_includes/footer-new.php');	
	?>
