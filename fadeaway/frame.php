<?php
if(isset($_REQUEST['bussinessid']) &&   is_numeric(base64_decode($_REQUEST['bussinessid'])) ){
	
	$bussinessid = base64_decode($_REQUEST['bussinessid']);
	$location = base64_decode($_REQUEST['location']);
	include("includes/dbFunctions.php");
	
	$frameInfo = new dbFunctions();

	$tableser = "tbl_services";
	$tablearea = "tbl_areas";
	$join = "RIGHT";
	$condition = "WHERE ".$tableser.".businessID = ".base64_decode($_REQUEST['bussinessid'])." AND ".$tableser.".locationID = ".base64_decode($_REQUEST['location'])." AND ".$tableser.".status = 1 AND ".$tablearea.".status = 1 GROUP BY ".$tableser.".serviceName ORDER by ".$tableser.".serviceName ASC";
	$cols = $tableser.".seviceID,".$tableser.".serviceName";
	$serviceinfo = $frameInfo->selectTableJoinNewcases($tableser,$tablearea, $join, $condition,$cols);
	
	$table1 = "tbl_employees";
	$condition1 = "WHERE Location=".base64_decode($_REQUEST['location'])." AND status = 1 ORDER BY First_Name ASC";
	$cols1 = "Emp_ID,First_Name,Last_Name";
	$staffdata = $frameInfo->selectTableRowsNew($table1,$condition1,$cols1);
	
	if(isset($_SESSION['frame']) && $_SESSION['frame'] != ''){
		
	}
?>	
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="https://www.w3.org/1999/xhtml">
		<head>
			<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
			<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
			<title>Appointments</title>
			<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="css/style-iframe.css">
			<link rel="stylesheet" type="text/css" href="jsnew/jquery.timepicker.css" />
			<link rel="stylesheet" type="text/css" href="jsnew/lib/bootstrap-datepicker.css" />
			<link rel="stylesheet" type="text/css" href="jsnew/jquery-ui.css">
			<script type="text/javascript" src="jsnew/jquery-1.12.4.js"></script>
			<script type="text/javascript" src="js/jquery.min.js"></script>
			<script type="text/javascript" src="jsnew/jquery.validate.min.js" ></script>
			<script type="text/javascript" src="jsnew/jquery.timepicker.js"></script>
			<script type="text/javascript" src="jsnew/lib/bootstrap-datepicker.js"></script>
			<script type="text/javascript" src="jsnew/jquery-ui.js"></script>
		</head>
		<body>
		<!--------------Step 1------------->
			<div class="la-form-new step-1" id="containerstep-1">
				<div class="la-container">
					<div class="la-formheader">
						<h3>Schedule a free CONSuLTATION </h3>
						<div class="lasteps">Step 1</div>
					</div>
					<form id="step-1" name="step-1" method="POST" action="">
						<input type="hidden" name="bussinessid" value="<?php echo $bussinessid; ?>">
						<input type="hidden" name="locationid" value="<?php echo $location; ?>">
						<div class="la-threeinp-container">
							<div class="la-threeinp">
								<div class="la-inptype">
									<label>Which service(s)?</label>
									<select name="service[]" id="service" class="service">
										<option value="">Please select a service</option>
										<?php 
										foreach( $serviceinfo as $service ) {
										?>
											<option value="<?php echo $service['seviceID']; ?>"><?php echo ucfirst($service['serviceName']);  ?></option>
										<?php	
										}
										?>
									</select>
								</div><div class="la-inptype">
									<label>Which part of body?</label>
									<select id="area" name="area[]" class="area"></select>
								</div><div class="la-inptype">
									<label>Select staff</label>
									<select id="staff" name="staff[]">
										<option value="0">Any</option>
										<?php
										foreach($staffdata as $staff){
										?>
										<option value="<?php echo $staff['Emp_ID']; ?>"><?php echo ucfirst($staff['First_Name']).' '.ucfirst($staff['Last_Name']);  ?></option>
										<?php
										}
										?>
									</select>
								</div><div class='icon-container'><a class="add-new" id="add-new"  href="javascript:void(0);">+</a></div>
							</div> 
						</div>
						<!-- div class="la-twoinp">
							<div class="la-seltype">
								<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin1</span>
								<a><img class="cross" src="iframe-img/cross.svg"></a>
							</div>
							<div class="la-seltype">
								<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin2</span>
								<a><img class="cross" src="iframe-img/cross.svg"></a>
							</div>
						</div -->
						<div id="la-nxt-btn-step-1" class="la-nxt-btn">
							<input type="submit" value="NEXT">
						</div>
					</form>

				</div>
			</div>
		<!--------------Step 2-------------->
			<div class="la-form-new step-2" id="containerstep-2" style="display:none">
				<div class="la-container">
					<form id="step-2" name="step-2" method="POST" action="">
						<div class="la-formheader la-alignheader">
							<h3>APPOINTMENT INFO</h3>
							<div class="lasteps">Step 2</div>
						</div>
						<div class="la-twoinp">
							<div class="la-seltype">
								<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin</span>
								<a><img class="cross" src="iframe-img/cross.svg"></a>
							</div>
							<div class="la-seltype">
								<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin</span>
								<a><img class="cross" src="iframe-img/cross.svg"></a>
							</div>
						</div>
						<div class="la-calouter">
							<div class="cal-block">
								<!-- input type="text" id="calender" placeholder="">
								<img src="iframe-img/cal.jpg" -->
								<div id="datepicker"></div>
								<div class="la-inptype">
									<label>Please select time</label>
									<input id="timepicker" type="text" name="timepicker" class="time start" placeholder="Click to select time"/>
								</div>
							</div>
							<div class="la-timeslots">
								<div class="la-selectlot">
									<h4>Time slot:</h4>
									<ul class='time-slots'>
										<!-- li><a>09:00 AM</a></li -->
									</ul>
								</div>
								<div class="la-nxt-btn btn-two">
									<input type="button" value="Back" class="backbtn" id="step-2-backbtn">
									<!-- input id="step-2-btn" type="button" value="NEXT" class="nxtbtn" -->
									<input id="step-2-btn" type="submit" value="NEXT" class="nxtbtn">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		<!--------------Step 3-------------->
			<div class="la-form-new step-3" id="containerstep-3" style="display:none">
				<div class="la-container">
					<form id="step-3" name="step-3" method="POST" action="">
						<div class="la-formheader la-alignheader">
							<h3>Personal INFO</h3>
							<div class="lasteps">Step 3</div>
						</div>
						<div class="la-twoinp">
							<div class="la-seltype">
								<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin</span>
								<a><img src="iframe-img/cross.svg"></a>
							</div>
							<div class="la-seltype">
								<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin</span>
								<a><img src="iframe-img/cross.svg"></a>
							</div>
						</div>
						<div class="la-threeinp">
							<div class="la-inptype">
								<label>Name</label>
								<input type="text" name="pname" id="pname" placeholder="Sam Peterson">
							</div>
							<div class="la-inptype">
								<label>Email</label>
								<input type="text" name="pemail" id="pemail" placeholder="sampeterson@gmail.com">
							</div>
							<div class="la-inptype">
								<label>Phone Number</label>
								<input type="text" name="pphone" id="pphone" placeholder="1111111111" maxlength="10">
							</div>
						</div>
						<div class="full-width-la">
							<div class="la-inptype">
								<label>Message</label>
								<textarea name="pmessage" id="pmessage"></textarea>
							</div>
						</div>
						<div class="la-nxt-btn btn-two">
							<input type="button" value="Back" class="backbtn" id="step-3-backbtn">
							<input type="submit" value="NEXT" class="nxtbtn">
						</div>
					</form>					
				</div>
			</div>
		<!--------------Step 4-------------->
			<div class="la-form-new step-4" id="containerstep-4" style="display:none">
				<div class="la-container">
					<div class="la-formheader la-alignheader">
						<h3>TERMS & CONDITIONS</h3>
						<div class="lasteps">Step 4</div>
					</div>
					<div class="la-twoinp">
						<div class="la-seltype">
							<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin</span>
							<a><img src="iframe-img/cross.svg"></a>
						</div>
						<div class="la-seltype">
							<span>Hair Removal,  Legs-$20 - 20 mins,  Andrew Rankin</span>
							<a><img src="iframe-img/cross.svg"></a>
						</div>
					</div>
					<form id="step-4" name="step-4" method="POST" action="">
						<div class="checkbox-type ind-appoint">
							<input type="checkbox" name="agreecheckbox" id="agreecheckbox">
							<label>Agree for individual appointments we require 24 hours’ notice for cancellations or rescheduling. for groups of three or more, we require 48 hours notice for cancellations or rescheduling. please keep in mind that “no-shows” or last minute cancellations leave our estheticians with empty appointment times. as well, we may have had to turn away other guests that could have enjoyed that time slot! because of this, guests that do not honour their appointments will result in 100% of the service price will be charged.</label>
						</div>
						<div class="la-nxt-btn btn-two">
							<!-- input type="button" value="NEXT" class="nxtbtn" -->
							<input type="button" value="Back" class="backbtn" id="step-4-backbtn">
							<input type="submit" value="NEXT" class="nxtbtn">													
						</div>
					</form>
				</div>
			</div>
		<!--------------Step 5-------------->
			<div class="la-form-new step-5" id="containerstep-5" style="display:none">
				<div class="la-container">
					<div class="thanks-block">
						<img src="iframe-img/arr-tick.svg">
						<h3>THANK YOU!</h3>
						<p>Your appointment has been scheduled.</p>
					</div>
				</div>
			</div>
		
		</body>
		<script>
		$(document).ready(function(){
			$('#la-nxt-btn-step-1').hide();
			/*******************************************************/
			$(document).on("click", ".service" , function() {
				
				var place = $(this).closest('.la-threeinp').children('.la-inptype').find(".area");
				var str = $(this).val();
				if( str != '' ){
					$.ajax({
						type: "POST",
						url: "admin/ajax_newform.php",
						data: 'id='+str+'&formname=fetcharea',
						cache: false,
						success: function(result){
							var res = JSON.parse(result);
							resultmes = res.message;
							place.addClass("background");
							place.html('');
							if( res.status == 0 ){
								place.append('<option value="0">Free consultation</option>');
							} else if ( res.status == 1 ){
								place.append(resultmes);
							}
							$('#la-nxt-btn-step-1').show();
						}
					});
				}
			
			});
			
			$(document).on("click", ".add-new" , function() {
				var numItems = $('.la-threeinp').length;	
				$(".la-threeinp-container").append("<div class='la-threeinp'><div class='la-inptype'><label>Which service(s)?</label><select name='service[]' id='service_"+numItems+"' class='service'><option value=''>Please select a service</option><?php foreach( $serviceinfo as $service ) { ?> <option value='<?php echo $service["seviceID"]; ?>'><?php echo $service['serviceName']; ?></option><?php	}?></select></div><div class='la-inptype'><label>Which part of body?</label><select id='area_"+numItems+"' name='area[]' class='area'></select></div><div class='la-inptype'><label>Select staff</label><select id='staff' name='staff[]'><option value='0'>Any</option><?php foreach($staffdata as $staff){ ?><option value='<?php echo $staff["Emp_ID"]; ?>'><?php echo ucfirst($staff['First_Name']).' '.ucfirst($staff['Last_Name']);  ?></option><?php } ?></select></div><div class='icon-container'><a class='add-new' id='add-new' href='javascript:void(0);'>+</a><a href='javascript:void(0);' class='remove'>&times;</a></div></div>"); 
				$('#la-nxt-btn-step-1').hide();
			});
			
			$(document).on("click", "a.remove" , function() {
				$(this).closest('.la-threeinp').remove();
				$('#la-nxt-btn-step-1').show();
			});
			
			
			$("#step-1").validate({
				ignore: [],
				rules: {
					'service[]': { required: true },
					//~ 'area[]':{ required: true },
				},                
				messages: {  
					'service[]':{ required: "Please select a service.", },
					//~ 'area[]':{ required: "Please select an area of treatment." },
				},
				submitHandler: function(form){
					var str = $("#step-1").serialize()+'&formname=framestep&step=1';
					$.ajax({
						type: "POST",
						url: "admin/ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							var res = JSON.parse(result);
							if(res.status == 1){
								$('.la-form-new').hide();
								$('#containerstep-2').show();
								$('.la-twoinp').html(res.message);
							}
						}
					}); 
				}
			});
			/******************************************************/
			/******************************************************/
			
			$('#step-2-backbtn').click(function(){
				$('.time').val('');
				$('.time-slots').html('');				
				$('.la-form-new').hide();
				$('#containerstep-1').show();
			});
			
			$("#datepicker").datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: 0, 
				onSelect: function(dateText, inst) {
					$('.time').val('');					
				},
			});
			
			$('#timepicker').timepicker({
				'showDuration': true,
				'timeFormat': 'H:i',
				'minTime':'09am',
				'maxTime':'06pm',
			});
			
			$('#timepicker').on('changeTime', function() {
				var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
				var time = $(this).val();
				var str = 'date='+date+'&time='+time+'&formname=framestep&step=2&submit=date';
				$.ajax({
					type: "POST",
					url: "admin/ajax_newform.php",
					data: str,
					cache: false,
					success: function(result){
						var res = JSON.parse(result);
						$('.time-slots').html(res.message);
						//~ if(res.status == 1){
							//~ $('.la-form-new').hide();
							//~ $('#containerstep-3').show();
						//~ }
					}
				});
			});
		
			$("#step-2").validate({
				ignore: [],
				rules: {                 
					timepicker:{
						required: true,
					},                          
				},                
				messages: {                    
					timepicker:{
						required: "Please select appointment time.",
					},                                
				},
				submitHandler: function(form){
					var str = $("#step-2").serialize()+'&formname=framestep&step=2&submit=form';
					$.ajax({
						type: "POST",
						url: "admin/ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								$('.la-form-new').hide();
								$('#containerstep-3').show();
							}
						}
					});
				}
			});				
				
			/******************************************************/
			/******************************************************/
			$('#step-3-backbtn').click(function(){
				$('.la-form-new').hide();
				$('#containerstep-2').show();
			});
			
			$("#step-3").validate({
				ignore: [],
				rules: {                 
					pname:{
						required: true,
					},                          
					pemail:{
						required: true,
						email:true,
					},                          
					pphone:{
						required: true,
						number:true,						
					},                          
					pmessage:{
						required: true,
					},                          
				},                
				messages: {                    
					pname:{
						required: "Please enter your name.",
					},                          
					pemail:{
						required: "Please enter email address.",
						email:"Please enter valid email address.",
					},
					pphone:{
						required: "Please enter phone number.",
					},
					pmessage:{
						required: "Please enter your message.",
					},                                
				},
				submitHandler: function(form){
					var str = $("#step-3").serialize()+'&formname=framestep&step=3';
					$.ajax({
						type: "POST",
						url: "admin/ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							if(result == 1){
								$('.la-form-new').hide();
								$('#containerstep-4').show();
							}
						}
					});
				}
			});	
			/******************************************************/
			/******************************************************/
			$('#step-4-backbtn').click(function(){
				$('.la-form-new').hide();
				$('#containerstep-3').show();
			});
			
			$("#step-4").validate({
				ignore: [],
				rules: {                 
					agreecheckbox:{
						required: true,
					},                          
				},                
				messages: {                    
					agreecheckbox:{
						required: "Please click box to agree with conditions.",
					},                            
				},
				submitHandler: function(form){
					var str = $("#step-4").serialize()+'&formname=framestep&step=4';
					$.ajax({
						type: "POST",
						url: "admin/ajax_newform.php",
						data: str,
						cache: false,
						success: function(result){
							//~ alert(result);
							if(result == 1){
								$('.la-form-new').hide();
								$('#containerstep-5').show();
							}
						}
					});
				}
			});	
			/******************************************************/
		});
	    
		</script>
		
	</html>
<?php	
} else {
	echo 'Wrong Bussiness Name or Location Name.';	
}
?>
