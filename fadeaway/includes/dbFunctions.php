<?php
	require_once("config.php");
	class dbFunctions
	{
		public $con;
		function __construct() 
		{
			// $this->con = mysql_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD);
			$this->con = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);
			if (!$this->con) {
				die('Could not connect: ' . mysqli_error());
			}		 
			// mysql_select_db(DB_NAME, $this->con) or die("database not found");
		}
		function selectTableRows($table,$condition=null,$cols="*") {
			$SQL ="SELECT ".$cols." FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			// $MYSQL = mysql_query($SQL) or die($SQL);
			$arr=array();
			while($row=mysqli_fetch_array($MYSQL)) {
				$arr[]=$row;
			}
			return $arr;
		}
		function selectTableSingleRow($table,$condition=null,$cols="*")	{
		    $SQL ="SELECT ".$cols." FROM ".$table." ".$condition; 
			$MYSQL = mysqli_query($this->con,$SQL);
			$row = mysqli_fetch_array($MYSQL);
			return $row;
		}	
		function selectTableData($table,$condition,$cols="*") {
			$SQL ="SELECT ".$cols." FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			if(mysqli_num_rows($MYSQL) > 0)	{
				$row=mysqli_fetch_object($MYSQL);
				return $row;
			}else {
				return '';
			}
		}
		/**** return total num rows ****/
		function totalNumRows($table,$condition,$cols="*") {
			$SQL ="SELECT ".$cols." FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$countRow = mysqli_num_rows($MYSQL);
				return $countRow;
		}	
		/****** selectNumRows***/
		function selectNumRows($table,$condition,$cols="*")	{
			$SQL ="SELECT ".$cols." FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$countRow = mysqli_num_rows($MYSQL);
			if($countRow >0) {			
				return 1;
			}else {
				return 0;
			}
		}
		function selectTreatmentTableData($table,$condition,$cols="*") {
			$SQL ="SELECT ".$cols." FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			while($row=mysqli_fetch_assoc($MYSQL))	{
				$row1[]=$row;
			}	
			return $row1;
		}	
		public function insert_data($table,$val_array)	{
			$keys = array_keys($val_array);
			$value = array_values($val_array);
			if($keys[1]=='TattoNumber' && $val_array["serviceId"]==1) {
				$a = $value[1];
				$a = preg_replace('/[^A-Za-z0-9\ ]/', '', $a); 
				$value[1]= $a;
			}
			$arr = array();
			foreach($value as $v) {
				// $v = preg_replace('/[^A-Za-z0-9\ ]/', '', $v);
				$arr[]=mysqli_real_escape_string($this->con,trim($v));
			}
			$fields	= implode(",",$keys);
			$values	= "('".implode("','",$arr)."')";			
			$SQL = "INSERT INTO ".$table." (".$fields.") values ".$values;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			return $MYSQL;
		}
		public function update_user($table,$data) {		
			$i=0;
			foreach( $data as $field_name => $field_value )	{		
				if($i==0) {
					 $condition = $field_name." = ". $field_value;
					$i++;
					continue;
				}
				if($field_value != 'NULL' )
					$field_updates[] = "$field_name = '".addslashes($field_value)."'";
				else
					$field_updates[] = "$field_name = NULL ";
			}
			if(!isset($condition) && isset($data['SessionNumber']))	{
				$condition = ' SessionNumber='.$data['SessionNumber'];
			}else if(isset($data['SessionNumber']) && isset($condition)){
				$condition .= ' AND SessionNumber='.$data['SessionNumber'];
			}
			if(!isset($condition) && isset($data['TattoNumber'])) {
				$condition = ' TattoNumber='.$data['TattoNumber'];
			}else if(isset($data['TattoNumber']) && isset($condition)) {
				$condition .= " AND TattoNumber='".$data['TattoNumber']."'";
			}
			$SQL = "UPDATE $table SET " .  implode( ", ", $field_updates ) . "  WHERE ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
			return $MYSQL;
		}
		/**** function  for forgot password***/
		/**** function  for forgot password***/
		public function update_forgotpassword($table,$data) {	
			$i=0;
			foreach( $data as $field_name => $field_value )	{	
				if($i==0) {
					 $condition = $field_name." = ". $field_value;
					$i++;
					continue;
				}
				if($field_value != 'NULL' )
					$field_updates[] = "$field_name = '".addslashes($field_value)."'";
				else
					$field_updates[] = "$field_name = NULL ";
			}
			if(!isset($condition) && isset($data['Emp_email'])){
				$condition = 'Emp_email ='.$data['Emp_email'];
			}else if(isset($data['Emp_email']) && isset($condition)){
				$condition .= ' AND Emp_email ='."'".$data['Emp_email']."'";
			}
			$SQL = "UPDATE $table SET " .  implode( ", ", $field_updates ) . "  WHERE ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
			return $MYSQL;
		}	
		/****** Funtion for JOIN QUERY**/
		function selectTableJoin($table1,$table2, $join, $condition=null,$cols="*")	{
			$SQL ="SELECT ".$cols." FROM ".$table1." ".$join." JOIN  ".$table2." ON ".$condition;
		   // echo $SQL; die;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$arrjoin=array();
			while($row=mysqli_fetch_array($MYSQL)) {
				$arrjoin[]=$row;
			}
			return $arrjoin;
		}
		
		/****** Funtion for JOIN QUERY**/
		function selectLapsedLostTableJoin($table1,$table2, $join, $condition=null,$cols="*")	{
			$SQL ="SELECT ".$cols." FROM (SELECT ClientID, Date_of_treatment FROM tbl_treatment_log ORDER BY Date_of_treatment DESC) AS tbl_treatment_log ".$join." JOIN  ".$table2." ON ".$condition;
		   // echo $SQL; die;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$arrjoin=array();
			while($row=mysqli_fetch_array($MYSQL)) {
				$arrjoin[]=$row;
			}
			return $arrjoin;
		}
		
		 /******* Funtion count rows from join table***/
		function joinTotalNumRows($table1,$table2, $join, $condition=null,$cols="*") {
			$SQL ="SELECT ".$cols." FROM ".$table1." ".$join." JOIN  ".$table2." ON ".$condition;	    
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			 $countRowJoin = mysqli_num_rows($MYSQL);	
			return $countRowJoin;	
		}	
		/**** update wavelength*/
		public function update_length($table,$data) {	
			$i=0;
			foreach( $data as $field_name => $field_value )	{			
				if($i==0) {
					 $condition = $field_name." = ". $field_value;
					$i++;
					continue;
				}
				if($field_value != 'NULL' )
					$field_updates[] = "$field_name = '".addslashes($field_value)."'";
				else
					$field_updates[] = "$field_name = NULL ";
			}
			if(!isset($condition) && isset($data['WavelengthID']))	{
				$condition = ' WavelengthID='.$data['WavelengthID'];
			}else if(isset($data['WavelengthID']) && isset($condition))	{
				$condition .= ' AND WavelengthID='.$data['WavelengthID'];
			}				
			$SQL = "UPDATE $table SET " .  implode( ", ", $field_updates ) . "  WHERE ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
			return $MYSQL;
		}
			/****** update email template***/
		public function update_email_template($data) {
			$email_id = $data['id'];
			$email_title = $data['tempid'];
			$email_sub = $data['sub'];
			$email_msg = $data['content'];
			$result = mysqli_query("UPDATE tbl_email_templates SET email_subject= '$email_sub',  email_msg = '$email_msg' where email_id	= $email_id");
			return $result;
		}
		/************************** Function to send email to treated clients-***********************/
		public function treat_email($fname,$lname,$email,$date, $service, $fourth_session_email = NULL){

			/*
			if(($_SESSION['BusinessID']==6) || ($_SESSION['businessdata']['BusinessID']==6)) {
				$template_name='NEWLY TREATED CLIENTS VAMOOSE';
				if($service == 'hair')
				{
					$list_name = "newly treated client ";
				}
			if($service == 'tattoo') {
				$list_name = "newly treated client vamoose";
			  }	
			}else
			{
				if($service == 'hair')
				{
					$list_name = "newly treated client ";
				}
				if($service == 'tattoo') {
					$list_name = "newly treated client fadeaway";
				  }
				$template_name='NEWLY TREATED CLIENTS';
			}*/
			$template_name = 'NEWLY TREATED CLIENTS';
			
			if($service == 'hair'){
				$list_name = "Hair Removal Clients New";
			}

			if($service == 'tattoo') {
				$list_name = "Newly Treated Client";
			}

			if($fourth_session_email){
				$template_name 	= 'FOURTH TREATMENT';
				$list_name 		= 'Fourth Session Treated Client';
			}
			// Activity Log
			$SQL = "INSERT INTO `activity_logs`( `businessID`, `list_name`, `fname`, `lname`, `created_on` ) VALUES (".$_SESSION['BusinessID'].",'$list_name', '$fname', '$lname', '".date('Y-m-d H:i:s')."')";
			mysqli_query($this->con,$SQL);
			// Activity Log
			//echo $template_name . ' = '.$list_name;
			/*
			require('../SendyPHP/SendyPHP.php');
			$sendy = new \SendyPHP\SendyPHP($list_name);
			$listdata = $sendy->get_list_id($list_name);
			$sendy->setListId($listdata['list_id']);
			$listID = $listdata['original_id'];
			*/	
			/**************** Subscribe Users For Newly Treatment Newsletter ****************
				$userID = 1;
				$subscriber_status = $sendy->removeSubscriber($listID, $email, $userID);
				if($subscriber_status == 0){
					$subscribe['name'] = ucfirst($fname).' '.ucfirst($lname);
					$subscribe['email'] = $email;
					$sendy->subscribe($subscribe);
				}
						
				/************* Delete From Lapsed Client List*************
					$list_name = "Lost Newsletter Clients New";
					$listdata  = $sendy->get_list_id($list_name,1);
					$sendy->setListId($listdata['list_id']);
					$substatus = $sendy->substatus($email);
					if($substatus['message'] == 'Subscribed'){
						$listID = $listdata['original_id'];
						$sendy->removeSubscriber($listID, $email, $userID);
					}
				/*********** End Delete From Lapsed Client List***********
				
				/************* Delete From Lost Client List*************
					$list_name = "Lapsed Newsletter Clients New";
					$listdata = $sendy->get_list_id($list_name,1);
					$sendy->setListId($listdata['list_id']);
					$substatus = $sendy->substatus($email);
					if($substatus['message'] == 'Subscribed'){
						$listID = $listdata['original_id'];
						$sendy->removeSubscriber($listID, $email, $userID);
					}
				/*********** End Delete From Lapsed Client List***********/
				/************************** End **************************/
		}


		/**
		* Send fourth session treatment email
		* @param SessionNumber,First name,Last name,Email,date,service(tatto/hair)
		* @return Null
		*/
		public function fourth_session_email($fname,$lname,$email,$date, $service){

			//eDx4aD4FZly763USUFG2jpww
			
			$template_name 	= 'FOURTH TREATMENT';
			// $list_name 		= ( $service == 'hair' )?'Hair Removal Clients New':'Newly Treated Client';
			$list_name 		= 'Fourth Session Treated Client';

			require_once('../SendyPHP/SendyPHP.php');

			$sendyObj 	= new \SendyPHP\SendyPHP($list_name);
			
			$listdata1 	= $sendyObj->get_list_id($list_name);
			
			$sendyObj->setListId($listdata1['list_id']);
			
			$listID   			= $listdata1['original_id'];
			$userID 			= 1;

			$subscriber_status 	= $sendyObj->removeSubscriber($listID, $email, $userID);
			
			if($subscriber_status == 0){
				$subscribe['name'] = ucfirst($fname).' '.ucfirst($lname);
				$subscribe['email'] = $email;
				$sendyObj->subscribe($subscribe);
			}
		}

		



		/************************** Function ends-***********************/
		
		/****** update spot size***/
		public function update_spot($table,$data) {
			$i=0;
			foreach( $data as $field_name => $field_value )	{		
				if($i==0) {
					 $condition = $field_name." = ". $field_value;
					$i++;
					continue;
				}
				if($field_value != 'NULL' )
					$field_updates[] = "$field_name = '".addslashes($field_value)."'";
				else
					$field_updates[] = "$field_name = NULL ";
			}
			if(!isset($condition) && isset($data['spotsizeID'])) {
				$condition = 'spotsizeID='.$data['spotsizeID'];
			}else if(isset($data['spotsizeID']) && isset($condition)) {
				$condition .= ' AND spotsizeID='.$data['spotsizeID'];
			}		
			$SQL = "UPDATE $table SET " .  implode( ", ", $field_updates ) . "  WHERE ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
			return $MYSQL;
		}
		/*****/
		public function update_user_profile($data) {
			$clientid 	= $data['clientid'];
			$name 		= $data['FirstName'];
			$lname 		= $data['LastName'];
			$age 		= $data['Age'];
			$Occupation = $data['Occupation'];
			$Address1 	= $data['Address1'];
			$Address2 	= $data['Address2'];
			$City 		= $data['City'];
			$State 		= $data['State'];
			$Zip 		=  $data['Zip'];
			$PhoneNumber = $data['PhoneNumber'];
			$Email 		= $data['Email'];
			$ReferralSource = $data['ReferralSource'];
			$ReferralSourceName = $data['ReferralSourceName'];
			$result = mysqli_query($this->con,"UPDATE tbl_clients SET FirstName= AES_ENCRYPT('".$name."','".SALT."'),  LastName = AES_ENCRYPT('".$lname."','".SALT."'), Age = AES_ENCRYPT('".$age."','".SALT."'), Occupation = AES_ENCRYPT('".$Occupation."','".SALT."'), Address1 = AES_ENCRYPT('".$Address1."','".SALT."'), Address2 = AES_ENCRYPT('".$Address2."','".SALT."'), City = AES_ENCRYPT('".$City."','".SALT."'), State= AES_ENCRYPT('".$State."','".SALT."'), Zip= AES_ENCRYPT('".$Zip."','".SALT."'), PhoneNumber = AES_ENCRYPT('".$PhoneNumber."','".SALT."'), Email = AES_ENCRYPT('".$Email."','".SALT."'), ReferralSource = '$ReferralSource', ReferralSourceName='".$ReferralSourceName."' WHERE ClientID 	= $clientid");
			//$result = mysqli_query($this->con,"UPDATE tbl_clients SET FirstName= '$name',  LastName = '$lname', Age = $age, Occupation = '$Occupation', Address1 = '$Address1', Address2 = '$Address2', City = '$City', State= '$State', Zip= '$Zip', PhoneNumber = '$PhoneNumber', Email = '$Email', ReferralSource = '$ReferralSource' WHERE ClientID 	= $clientid");
			return $result;
		}			
		/****************** Update Pricing ******************/
		public function update_pricing($table, $data, $condition) {
			foreach($data AS $field_name=>$field_value) {
				if($field_value != 'NULL' )
					$field_updates[] = "$field_name = '".addslashes($field_value)."'";
				else
					$field_updates[] = "$field_name = NULL ";
			}
			$SQL = "UPDATE $table SET " .  implode( ", ", $field_updates ) . "  WHERE ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
			return $MYSQL;
		}
		/************************** Get waveLegth-***********************/
		public function WaveLengthData($value) {
			$deviceID = $value['deviceID'];		
			$sql ="SELECT tbl_ta2_wavelengths.Wavelength FROM `tbl_devicename` 
					LEFT JOIN  `tbl_ta2_wavelengths` ON tbl_devicename.deviceId = tbl_ta2_wavelengths.deviceId 
					WHERE  
					tbl_devicename.deviceId = $deviceID";

			$result = mysqli_query($this->con,$SQL);

			while($data = mysqli_fetch_assoc($result)) {
				$rows[]=$data;
			}
			return $rows;
		}
		public function SpotLengthData($value) {
			$deviceID = $value['deviceID'];		
			$sql ="SELECT  tbl_ta2_spot_sizes.spotsize FROM `tbl_devicename` LEFT JOIN  `tbl_ta2_spot_sizes` ON tbl_devicename.deviceId=tbl_ta2_spot_sizes.deviceId WHERE  tbl_devicename.deviceId=$deviceID";
			$result = mysqli_query($this->con,$SQL);
			while($data = mysqli_fetch_assoc($result))	{
				$spotsize[]=$data;
			}
			return $spotsize;
		}
		public function fluenceData($value)	{
			$deviceID = $value['deviceID'];		
			$sql ="SELECT  tbl_fluence.fluenceName FROM `tbl_devicename` LEFT JOIN  `tbl_fluence` ON tbl_devicename.deviceId=tbl_fluence.deviceId WHERE  tbl_devicename.deviceId=$deviceID";
			$result = mysqli_query($this->con,$SQL);
			while($data = mysqli_fetch_assoc($result)){
				$fluence[]=$data;
			}
			return $fluence;
		}
		/**************** DELETE FUNCTION*******/
		function deleteRow($table,$condition) {		
			$SQL ="DELETE  FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);			
			return $MYSQL;		
		}	
		function paginate_two($reload, $page, $tpages, $adjacents) {
			$firstlabel = "&laquo;&nbsp;";
			$prevlabel  = "&lsaquo;&nbsp;";
			$nextlabel  = "&nbsp;&rsaquo;";
			$lastlabel  = "&nbsp;&raquo;";	
			$out = "<div class=\"pagin\">\n";
			// first
			if($page>($adjacents+1)) {
				$out.= "<a href=\"" . $reload . "\">" . $firstlabel . "</a>\n";
			}
			else {
				$out.= "<span>" . $firstlabel . "</span>\n";
			}
			// previous
			if($page==1) {
				$out.= "<span>" . $prevlabel . "</span>\n";
			}
			elseif($page==2) {
				$out.= "<a href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
			}
			else {
				$out.= "<a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a>\n";
			}
			// 1 2 3 4 etc
			$pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
			$pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
			for($i=$pmin; $i<=$pmax; $i++) {
				if($i==$page) {
					$out.= "<span class=\"current\">" . $i . "</span>\n";
				}
				elseif($i==1) {
					$out.= "<a href=\"" . $reload . "\">" . $i . "</a>\n";
				}
				else {
					$out.= "<a href=\"" . $reload . "&amp;page=" . $i . "\">" . $i . "</a>\n";
				}
			}
			// next
			if($page<$tpages) {
				$out.= "<a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a>\n";
			}
			else {
				$out.= "<span>" . $nextlabel . "</span>\n";
			}
			// last
			if($page<($tpages-$adjacents)) {
				$out.= "<a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a>\n";
			}
			else {
				$out.= "<span>" . $lastlabel . "</span>\n";
			}
			$out.= "</div>";
			return $out;
		}		
		/*******returnn Paggination  ****/
		function paginateShow($page,$total_pages,$limit,$adjacents,$reload,$loc = NULL){
			
			if( $loc ){
				$loc = '&'.$loc;
			}			
		/* Setup page vars for display. */
			if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
			$prev = $page - 1;                          //previous page is page - 1
			$next = $page + 1;                          //next page is page + 1
			$lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
			$lpm1 = $lastpage - 1;                      //last page minus 1
			/*
				Now we apply our rules and draw the pagination object.
				We're actually saving the code to a variable in case we want to draw it more than once.
			*/
			$idTop="#top";
			$pagination = "";
			if($lastpage > 1) {  
				$pagination .= "<div class=\"pagination\" id=\"top\">";
				//previous button
				if ($page > 1)
					$pagination.= "<a href=\"$reload?page=$prev$loc"."$idTop\">« previous</a>";
				else
					$pagination.= "<span class=\"disabled\">« previous</span>"; 
				//pages
				if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
				{  
					for ($counter = 1; $counter <= $lastpage; $counter++) {
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= "<a href=\"$reload?page=$counter$loc"."$idTop\">$counter</a>";                  
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2)) {
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)	{
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href=\"$reload?page=$counter$loc"."$idTop \">$counter</a>";                  
						}
						$pagination.= "...";
						$pagination.= "<a href=\"$reload?page=$lpm1"."$idTop \">$lpm1</a>";
						$pagination.= "<a href=\"$reload?page=$lastpage"."$idTop \">$lastpage</a>";    
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
						$pagination.= "<a href=\"$reload?page=1"."$idTop \">1</a>";
						$pagination.= "<a href=\"$reload?page=2"."$idTop \">2</a>";
						$pagination.= "...";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href=\"$reload?page=$counter$loc"."$idTop\">$counter</a>";                  
						}
						$pagination.= "...";
						$pagination.= "<a href=\"$reload?page=$lpm1 "."$idTop \">$lpm1</a>";
						$pagination.= "<a href=\"$reload?page=$lastpage"."$idTop \">$lastpage</a>";    
					}
					//close to end; only hide early pages
					else {
						$pagination.= "<a href=\"$reload?page=1\">1</a>";
						$pagination.= "<a href=\"$reload?page=2\">2</a>";
						$pagination.= "...";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href=\"$reload?page=$counter$loc"."$idTop\">$counter</a>";                  
						}
					}
				}
				//next button
				if ($page < $counter - 1)
					$pagination.= "<a href=\"$reload?page=$next$loc"."$idTop\">next »</a>";
				else
					$pagination.= "<span class=\"disabled\">next »</span>";
				$pagination.= "</div>\n";      
			}    
			return $pagination;
		}
		/*******Paggination ****/
		/*******returnn Paggination  ****/
		function paginateShowNew($page,$total_pages,$limit,$adjacents,$reload,$loc = NULL){
			
			if( $loc ){
				$loc = '&'.$loc;
			}			
		/* Setup page vars for display. */
			if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
			$prev = $page - 1;                          //previous page is page - 1
			$next = $page + 1;                          //next page is page + 1
			$lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
			$lpm1 = $lastpage - 1;                      //last page minus 1
			/*
				Now we apply our rules and draw the pagination object.
				We're actually saving the code to a variable in case we want to draw it more than once.
			*/
			$idTop="#top";
			$pagination = "";
			if($lastpage > 1) {  
				$pagination .= "<div class=\"paginationmainblk\" id=\"top\"><ul>";
				//previous button
				if ($page > 1)
					$pagination.= "<li><a href=\"$reload?page=$prev$loc"."$idTop\">«</a></li>";
				else
					$pagination.= "<li><span class=\"disabled\">«</span></li>"; 
				//pages
				if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
				{  
					for ($counter = 1; $counter <= $lastpage; $counter++) {
						if ($counter == $page)
							$pagination.= "<li class=\"active\">$counter</li>";
						else
							$pagination.= "<li><a href=\"$reload?page=$counter$loc"."$idTop\">$counter</a></li>"; 
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2)) {
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)	{
							if ($counter == $page)
								$pagination.= "<li class=\"active\">$counter</li>";
							else
								$pagination.= "<li><a href=\"$reload?page=$counter$loc"."$idTop \">$counter</a></li>"; 
						}
						$pagination.= "...";
						$pagination.= "<li><a href=\"$reload?page=$lpm1"."$idTop \">$lpm1</a></li>";
						$pagination.= "<li><a href=\"$reload?page=$lastpage"."$idTop \">$lastpage</a></li>";
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
						$pagination.= "<li><a href=\"$reload?page=1"."$idTop \">1</a></li>";
						$pagination.= "<li><a href=\"$reload?page=2"."$idTop \">2</a></li>";
						$pagination.= "...";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
							if ($counter == $page)
								$pagination.= "<li class=\"active\">$counter</span></li>";
							else
								$pagination.= "<li><a href=\"$reload?page=$counter$loc"."$idTop\">$counter</a></li>";
						}
						$pagination.= "...";
						$pagination.= "<li><a href=\"$reload?page=$lpm1 "."$idTop \">$lpm1</a></li>";
						$pagination.= "<li><a href=\"$reload?page=$lastpage"."$idTop \">$lastpage</a></li>";
					}
					//close to end; only hide early pages
					else {
						$pagination.= "<li><a href=\"$reload?page=1\">1</a></li>";
						$pagination.= "<li><a href=\"$reload?page=2\">2</a></li>";
						$pagination.= "...";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
							if ($counter == $page)
								$pagination.= "<li class=\"active\">$counter</li>";
							else
								$pagination.= "<li><a href=\"$reload?page=$counter$loc"."$idTop\">$counter</a></li>";
						}
					}
				}
				//next button
				if ($page < $counter - 1)
					$pagination.= "<li><a href=\"$reload?page=$next$loc"."$idTop\">»</a></li>";
				else
					$pagination.= "<li><span class=\"disabled\"> »</span></li>";
				$pagination.= "</ul></div>\n";      
			}    
			return $pagination;
		}
		/*******Paggination ****/		
		//define('PAGE_PER_NO',1);
		function getPagination($count,$page_PER_NO){
			$paginationCount= floor($count /$page_PER_NO );
			$paginationModCount= $count % $page_PER_NO ;
			if(!empty($paginationModCount)) {
			 $paginationCount++;
			}
			return $paginationCount;
		}	
		  /*************************** */
		public function __destruct() {
			mysqli_close($this->con) or die(mysqli_error());
		}
		/************************** Insert Encrypted Data**************************/
		public function insert_data_aes($table,$val_array) {
			$keys	= array_keys($val_array);
			$value	= array_values($val_array);
			if($keys[1]=='TattoNumber') {
				$a = $value[1];
				$a = preg_replace('/[^A-Za-z0-9\ ]/', '', $a); 
				$value[1]= $a;
			}
			$fieldencrypt = array('FirstName', 'LastName', 'DOB', 'Age', 'Occupation', 'Address1', 'Address2', 'City', 'State', 'Zip','PhoneNumber', 'Email');
			$insertStr = "";
			$arr	= array();
			foreach($value as $i=>$v) {
				// $v = preg_replace('/[^A-Za-z0-9\ ]/', '', $v);
				$arr_val=mysqli_real_escape_string($this->con,trim($v));
				if($i == 0)
					$insertStr .= "AES_ENCRYPT('".$arr_val."','".SALT."')";
				else {
					if(in_array($keys[$i], $fieldencrypt))
						$insertStr .= ", AES_ENCRYPT('".$arr_val."','".SALT."')";
					else
						$insertStr .= ", '".$arr_val."'";
				}
				//array_push($arr, $arr_val);
			}
			$fields	= implode(",",$keys);
			//$values	= "('".implode("','",$arr)."')";
			$values	= "(".$insertStr.")";
			$SQL 		= "INSERT INTO ".$table." (".$fields.") values ".$values; 
			$MYSQL		= mysqli_query($this->con,$SQL) or die($SQL);
			return $MYSQL;
		}
		/************************** End **************************/
		public function update_device_location($table,$condition,$data) {
			foreach( $data as $field_name => $field_value )	{		
				if($field_value != 'NULL' )
					$field_updates[] = "$field_name = '".addslashes($field_value)."'";
				else
					$field_updates[] = "$field_name = NULL ";
			}
			$SQL = "UPDATE $table SET " .  implode( ", ", $field_updates ) . "  WHERE ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
			return $MYSQL;
		}
		
		public function update_status($table,$condition,$data) {
			foreach( $data as $field_name => $field_value )	{		
				if($i==0) {
					 $val = $field_name." = ". $field_value;
					$i++;
					continue;
				}
			}
			$SQL = "UPDATE $table SET status=0 where bussiness_id=".$_SESSION['BusinessID'];
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);
			$SQL = "UPDATE $table SET " .  $val . "  WHERE ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
		}
		
		public function update_legal($table,$condition,$data) {
			foreach( $data as $field_name => $field_value )	{		
				if($i==0) {
					 $val = $field_name." = '". $field_value."'";
					$i++;
					continue;
				}
			}
			$SQL = "UPDATE $table SET " .  $val . "  WHERE ".$condition;
			//echo $SQL;
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
		}
/*public function new_client_added($fname,$lname,$email)	{
		$list_name='Monthly Newsletter Clients New';
			require('../SendyPHP/SendyPHP.php');
			$sendy = new \SendyPHP\SendyPHP($list_name);
			$listdata = $sendy->get_list_id($list_name);
			$sendy->setListId($listdata['list_id']);
			$listID = $listdata['original_id'];
			
				$userID = 1;
				$subscriber_status = $sendy->removeSubscriber($listID, $email, $userID);
				if($subscriber_status == 0){
					$subscribe['name'] = ucfirst($fname).' '.ucfirst($lname);
					$subscribe['email'] = $email;
					$sendy->subscribe($subscribe);
				}
		}*/
	public function selectLastPrice($table,$condition,$data) {
			$SQL="select one.Price
			from $table as one
			inner join 
			(
				select max(Date_of_treatment) MaxPop
					from $table where ClientID = $condition
			) as two on one.Date_of_treatment = two.maxpop;"; 
			$MYSQL = mysqli_query($this->con,$SQL) or die($MYSQL);	
			while($row=mysqli_fetch_assoc($MYSQL))	{
				$row1[]=$row;
			}	
			return $row1;
		}
		
		function getlocation($table,$condition=null,$cols="*")	{
		    $SQL ="SELECT ".$cols." FROM ".$table." ".$condition; 
			$MYSQL = mysqli_query($this->con,$SQL);
			$row1 = array();
			while($row=mysqli_fetch_assoc($MYSQL))	{
				$row1[]=$row;
			}
			return $row1;
		}
		
		function TotalclientRows($table1,$cols="*",$condition=null) {
			$SQL ="SELECT ".$cols." FROM ".$table1." ".$condition;	    
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$countRowJoin = mysqli_num_rows($MYSQL);	
			return $countRowJoin;	
		}
		
		function selectclientrecord($table1,$cols="*",$condition=null)	{
			$SQL ="SELECT ".$cols." FROM ".$table1." ".$condition;
		   // echo $SQL; die;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$arrjoin=array();
			while($row=mysqli_fetch_array($MYSQL)) {
				$arrjoin[]=$row;
			}
			return $arrjoin;
		}	
		
		function selectallLocations($table,$condition=null,$cols="*") {
			$SQL ="SELECT ".$cols." FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			// $MYSQL = mysql_query($SQL) or die($SQL);
			$arr=array();
			while($row=mysqli_fetch_array($MYSQL)) {
				$arr[]=$row;
			}
			return $arr;
		}		
	/* *************************** New Function *********************** */	
		function selectTableRowsNew($table,$condition=null,$cols="*") {
			$SQL ="SELECT ".$cols." FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$arr=array();
			while($row=mysqli_fetch_array($MYSQL,MYSQLI_ASSOC)) {
				$arr[]=$row;
			}
			return $arr;
		}
		function selectTableSingleRowNew($table,$condition=null,$cols="*")	{
		    $SQL ="SELECT ".$cols." FROM ".$table." ".$condition; 
			$MYSQL = mysqli_query($this->con,$SQL);
			$row = mysqli_fetch_array($MYSQL,MYSQLI_ASSOC);
			return $row;
		}
		
		function selectTableJoinNew($table1,$table2, $join, $condition=null,$cols="*")	{
			$SQL ="SELECT ".$cols." FROM ".$table1." ".$join." JOIN  ".$table2." ON ".$condition;
		   // echo $SQL; die;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$arrjoin=array();
			while($row=mysqli_fetch_array($MYSQL,MYSQLI_ASSOC)) {
				$arrjoin[]=$row;
			}
			return $arrjoin;
		}
		
		function selectTableJoinNewcases($table1,$table2, $join, $condition=null,$cols="*")	{
			$SQL ="SELECT ".$cols." FROM ".$table1." ".$join." JOIN ".$table2." ON CASE WHEN ".$table1.".isArea = 1 THEN 1 WHEN ".$table1.".isArea = 0 AND ".$table1.".seviceID = ".$table2.".seviceID THEN 1 END ".$condition;

			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);
			$arrjoin=array();
			while($row=mysqli_fetch_array($MYSQL,MYSQLI_ASSOC)) {
				$arrjoin[]=$row;
			}
			return $arrjoin;
		}
		
		function daysname($id) {

			switch ($id) {
				case 0:
					echo "Monday";
					break;
				case 1:
					echo "Tuesday";
					break;
				case 2:
					echo "Wednesday";
					break;
				case 3:
					echo "Thrusday";
					break;
				case 4:
					echo "Friday";
					break;
				case 5:
					echo "Saturday";
					break;
				case 6:
					echo "Sunday";
					break;
				default:
					echo "";
			}
			
		}
		
		function monthname($id) {

			switch ($id) {
				case 1:
					echo "January";
					break;
				case 2:
					echo "Febuary";
					break;
				case 3:
					echo "March";
					break;
				case 4:
					echo "April";
					break;
				case 5:
					echo "May";
					break;
				case 6:
					echo "June";
					break;
				case 7:
					echo "July";
					break;
				case 8:
					echo "August";
					break;
				case 9:
					echo "September";
					break;
				case 10:
					echo "October";
					break;
				case 11:
					echo "November";
					break;
				case 12:
					echo "December";
					break;
				default:
					echo "";
			}
			
		}
		
		function monthname1($id) {

			switch ($id) {
				case 1:
					return "January";
					break;
				case 2:
					return "Febuary";
					break;
				case 3:
					return "March";
					break;
				case 4:
					return "April";
					break;
				case 5:
					return "May";
					break;
				case 6:
					return "June";
					break;
				case 7:
					return "July";
					break;
				case 8:
					return "August";
					break;
				case 9:
					return "September";
					break;
				case 10:
					return "October";
					break;
				case 11:
					return "November";
					break;
				case 12:
					return "December";
					break;	
			}
			
		}
		
		function deleteRowNew($table,$condition) {
			$SQL ="DELETE  FROM ".$table." ".$condition;
			$MYSQL = mysqli_query($this->con,$SQL) or die($SQL);			
			return $MYSQL;		
		}
		
	/* *************************** New Function *********************** */			
		
	}
