<?php
	require_once("config.php");
	include("../includes/dbFunctions.php");
	if( isset($_POST['remember_me']) && $_POST['remember_me']=='1'){ 
		$year = time() + 3600;
		setcookie('remember_me', $_POST['username'], $year, '/');
		echo $_COOKIE['remember_me'];  
	} else if(!isset($_POST['remember_me'])) { //echo $_COOKIE['remember_me']; die;
		if(isset($_COOKIE['remember_me'])) {
			$past = time() - 3600; echo "bye";
			setcookie('remember_me','',$past, '/');
		}
	}	
	class login	{
		public $con;	
		function __construct() {
			$this->con = mysql_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD);
			if (!$this->con) {
				die('Could not connect: ' . mysql_error());
			  }
			mysql_select_db(DB_NAME, $this->con) or die("database not found");
		}		
		function admin_login_check($username,$password,$loginType)	{
			
		 $sql = "SELECT Emp_ID, First_Name, Last_Name, Emp_email, St_Paul, Minneapolis, Duluth, Admin, Technician, Location, Medicaldirector, Superadmin, tbl_employees.BusinessID, menuPermissions, tbl_business.PlanType AS BusinessPlanType, tbl_business.serviceId, tbl_business.status AS business_status, tbl_business.created_date AS BusinessCreated, tbl_business.Optout AS Optout, tbl_business.sendy_brand_id AS sendy_brand_id FROM tbl_employees LEFT JOIN tbl_business ON tbl_business.BusinessID=tbl_employees.BusinessID WHERE  Username='$username' and Password='$password' AND tbl_employees.status='1'";
		 $sql_res = mysql_query($sql);
		 $admin_row	= mysql_num_rows($sql_res);
			if($admin_row == 1)	{
				$admin=mysql_fetch_array($sql_res);
				if($admin['Superadmin'] != 1 && $admin['business_status'] != 1)	{
					header("location:../admin/index.php?login=block");
					die;
				}
				$_SESSION['id']	= $admin['Emp_ID'];
				$_SESSION['First_Name'] = $admin['First_Name'];
				$_SESSION['Last_Name'] = $admin['Last_Name'];
				$_SESSION['Emp_email'] = $admin['Emp_email'];
				$_SESSION['Emp_Location_St_Paul'] = $admin['St_Paul'];
				$_SESSION['Emp_Location_Minneapolis'] = $admin['Minneapolis'];
				$_SESSION['Emp_Location_Duluth'] = $admin['Duluth'];
				$_SESSION['logintype'] = $loginType;
				$_SESSION['Admin'] = $admin['Admin'];
				$_SESSION['Technician']	= $admin['Technician'];
				$_SESSION['Location'] = $admin['Location'];
				$_SESSION['Medicaldirector'] = $admin['Medicaldirector'];
				$_SESSION['Superadmin'] = $admin['Superadmin'];
				$_SESSION['BusinessID'] = $admin['BusinessID'];				
				$_SESSION['SendyBrandID'] = $admin['sendy_brand_id'];				
				$_SESSION['services'] = explode(",",$admin['serviceId']);				
				$_SESSION['BusinessPlanType'] = $admin['BusinessPlanType'];
				$_SESSION['optoutstatus'] = $admin['Optout'];
				
				$business = new dbFunctions(); 
				$tbl_manage_protocols = "tbl_protocols";
				$condition = " left join tbl_employees as e on e.Emp_ID=tbl_protocols.uploaded_by where tbl_protocols.status='1' and tbl_protocols.bussiness_id=".$admin['BusinessID']." ORDER BY tbl_protocols.id DESC";
				$cols = " tbl_protocols.protocol_name, tbl_protocols.status, tbl_protocols.uploaded_on, e.First_Name, e.Last_Name, tbl_protocols.id ";
				$protcolData = $business->selectTableRows($tbl_manage_protocols,$condition,$cols);
				if(!empty($protcolData))
				{
					$_SESSION['protocol_status']=1;
				}	
				
				//if( $admin['Superadmin'] != 1 && $admin['Admin']=="TRUE" ) {
				if( $admin['Superadmin'] != 1 ) {
					$plan = new dbFunctions();
					$subtable = "tbl_subscription_history";
					$subcondition = "where BusinessID=".$_SESSION['BusinessID']." order by ID desc";
					$subdetails = $plan->selectTableSingleRow($subtable,$subcondition,$cols="*");
					$expire = date("Y-m-d", strtotime($subdetails["ExpireDate"]));
					$today = date('Y-m-d');
					$period = (strtotime($expire)- strtotime($today))/24/3600;
					$_SESSION["periodtime"] = $period;
					$_SESSION['CurrentPlanId'] = $subdetails['PlanID'];	
				}				
				if($admin['Superadmin'] == 1)
				{
					$_SESSION['loginuser'] = "sitesuperadmin";
				}				
				else  {
					$business_detail = mysql_fetch_array(mysql_query("SELECT BusinessName, Logo, Contact, LiabilityRelease, created_date FROM tbl_business WHERE BusinessID = $_SESSION[BusinessID]"));
					$_SESSION['businessdata'] = $business_detail;
				}				
				/************************Count Pending review***/
				$query = "SELECT * FROM tbl_consent_form  where TechnicianReview ='' AND BusinessID = $_SESSION[BusinessID]";
				$result = mysql_query($query);
				$count = mysql_num_rows($result);
				$_SESSION['pendingReview'] = $count;
				//Director review
				$queryDir = "SELECT * FROM tbl_consent_form  where TechnicianReview !='' AND MedicalDirectorNotes='' AND BusinessID = $_SESSION[BusinessID]";
				$resultDir = mysql_query($queryDir);
				$countDir = mysql_num_rows($resultDir);
				$_SESSION['pendingReviewDir'] = $countDir;			
				/************************End pending review ***/
				if($admin['Admin']=="FALSE")  { 
					$_SESSION['Usertype']="Employee";
				}
				else if($admin['Admin']=="TRUE") { 
					$_SESSION['Usertype']="Admin";
					$admin['menuPermissions']="1|2|3|4|5|6|7|8|9|10|11|12|13|14|15";
				}
				
				$_SESSION['menuPermissions'] = explode("|",$admin['menuPermissions']);	
				$_SESSION["period"] = "true";
				if(isset($period) && $period<=0) {
					if($admin['Admin']=="FALSE"){
						header("location:../admin/index.php?login=expire");
					}
					$_SESSION["period"] = "false";
				?>
					<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
					<script>
						$(function() {
							window.location.replace("../admin/subscription");
						});
					</script>
				<?php die;
				}	 
				//echo $_SESSION['businessdata']['BusinessName'];
				//echo $_SESSION['Usertype']; die;	
				$bussinesDataName = preg_replace('/\s+/', '', $_SESSION['businessdata']['BusinessName']);
				$bussinesDataName = str_replace('-', '', $bussinesDataName);						
				if($loginType == "client")	{
					//echo "admin login";
					
					if($_SESSION['loginuser'] != "sitesuperadmin")
					{
						//~ header("location:http://dexteroustechnologies.co.in/mylaserdata/".$bussinesDataName."/dashboard");	
						header("location:../admin/dashboard");
					}else
					{
						//~ header("location:../client/dashboard");
						header("location:../admin/dashboard");
					}		
					$sql_res = mysql_query($sql);				
				}
				else  {	
					if($_SESSION['loginuser'] != "sitesuperadmin")
					{
						//~ header("location:http://dexteroustechnologies.co.in/mylaserdata/".$bussinesDataName."/dashboard");	
						header("location:../admin/dashboard");
					}else
					{
						header("location:../admin/dashboard");
					}				
					
					$sql_res = mysql_query($sql);
				}
			}		
			else {
				header("location:".LOGIN_URL."?login=fail");
			}
		}
		function logout() {
			session_destroy();
			header("location:".LOGIN_URL."?logout=success");	
		}	
		public function __destruct() {
			mysql_close($this->con) or die(mysql_error());
		}	
	}
	$obj= new  login();
	if(isset($_POST['username'])) {
		if($_POST['loginType']!="") {
			$logintype = $_POST['loginType'];
		}
		else {
			$logintype = 'normal';
		}	
		$obj->admin_login_check($_POST['username'],md5($_POST['password']),$logintype);
	}
	if(isset($_GET['logout']) && $_GET['logout']==true) {
		$obj->logout();
	}
?>
