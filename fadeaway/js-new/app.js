$(document).ready(function(){
  $.ajax({
    url: "api.php",
    method: "GET",
    success: function(data) {
		var player = [];
		var score = [];
		for(var i in data) {
			player.push(data[i].m);
			score.push(data[i].price);
		}

      var chartdata = {
        labels: player,
            tooltips: {
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
        }
      }
    },   legend: {
      display: false
    },
        datasets : [
          {
			label: "Revenue",
			backgroundColor: "#4e73df",
			hoverBackgroundColor: "#2e59d9",
			borderColor: "#4e73df",
            data: score
          }
        ]
      };

      var ctx = $("#chartnew");

      var barGraph = new Chart(ctx, {
        type: 'bar',
        data: chartdata
      });
    },
    error: function(data) {
      console.log(data);
    }
  });
});
