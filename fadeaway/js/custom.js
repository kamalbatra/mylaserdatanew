$(function(){
	$(".clicktochangestatus").click(function(){
		$(".updatebusstatus").hide();
		var _this = $(this);
		var rel_val = _this.attr("rel");
		var split_rel_val = rel_val.split("|");
		var change_status = (split_rel_val[1] == 0) ? 1 : 0;
		var status_text = (split_rel_val[1] == 0) ? 'activate' : 'deactivate';
		var cnfrm = confirm("Are you sure you want to "+status_text+" this business?");
		if(cnfrm){
			$.ajax({
				type: "POST",
				url: "ajax_changestatus.php",
				data: {BusinessID : split_rel_val[0], status : change_status},
				cache: false,
				success: function(responsedata){
					_this.attr("rel", split_rel_val[0]+'|'+change_status);
					if(change_status == 1){
						_this.children("img").attr("src", "../images/active.gif");
						_this.children("img").attr("title", "Click to inactive");
					}
					else{
						_this.children("img").attr("src", "../images/inactive.gif");
						_this.children("img").attr("title", "Click to active");
					}
					$(".updatebusstatus").show();
				}
			});
		}
	});
});

$(function(){
	$(".clicktochange").click(function(){
		$(".updatebusstatus").hide();
		var _this = $(this);
		var rel_val = _this.attr("rel");
		var split_rel_val = rel_val.split("|");
		var change_status = (split_rel_val[1] == 0) ? 1 : 0;
		var status_text = (split_rel_val[1] == 0) ? 'activate' : 'inactivate';
		var cnfrm = confirm("Are you sure you want to "+status_text+" this plan?");
		if(cnfrm){
			$.ajax({
				type: "POST",
				url: "ajax_changestatus.php",
				data: {id : split_rel_val[0], status : change_status},
				cache: false,
				success: function(responsedata){
					_this.attr("rel", split_rel_val[0]+'|'+change_status);
					if(change_status == 1){
						_this.children("img").attr("src", "../images/active.gif");
						_this.children("img").attr("title", "Click to inactive");
					}
					else{
						_this.children("img").attr("src", "../images/inactive.gif");
						_this.children("img").attr("title", "Click to active");
					}
					$(".updatebusstatus").show();
				}
			});
		}
	});
});

$(document).ready(function(){
	$('.backbtn').hide();
	var Service = $('#Service').val();
	if( Service == 1 ) {
			$('#input-size').show();
			$('#select-size').hide();
	} else {
		$('#input-size').hide();	
		$('#select-size').show();	
	}
	
	$('#Service').change(function(){
		$("#prices").html('');
		$('#pricing_amt').val('');
		$('#pricing_amt_dash').val('');
		var ServiceID = this.value;
		if( ServiceID == 1 ) {
			$('#input-size').show();
			$('#select-size').hide();
		} else {
			$('#input-size').hide();	
			$('#select-size').show();	
		}
	});	
	$('#pricing_amt').keyup(function() {
		$("#prices").html('<div style="width:100%;text-align:center"><img alt="loading...." src="../images/loading.gif"></div>');
		if($.isNumeric($('#pricing_amt').val()) && $('#pricing_amt').val() !="" ) {				
			var str = $("#pricingform").serialize();
			$.ajax({
				type: "POST",
				url: "ajax_pricing.php",
				data: str,
				cache: false,
				success: function(result){
					$("#prices").show("slow");			
					$("#prices").html(result);
				}
			});		    
		} //if is numreic
		else{
			$("#prices").hide("slow");		
		}
	});
	$('#pricing_amt_dash').keyup(function() {
		$("#prices").html('<div style="width:100%;text-align:center"><img alt="loading...." src="../images/loading.gif"></div>');
		if($.isNumeric($('#pricing_amt_dash').val()) && $('#pricing_amt_dash').val() !="" ) {				
			var str = $("#pricingform").serialize();
			$.ajax({
				type: "POST",
				url: "ajax_pricing.php",
				data: str,
				cache: false,
				success: function(result){
					if(result != ''){
						$("#pricing-result").html('');
						$("#pricing-result").show("slow");			
						$("#pricingform").css('visibility','hidden');
						$("#pricing-result").html(result);
						$('.backbtn').show();
					} else {
						$("#pricing-result").show("slow");
						$("#pricingform").css('visibility','hidden');
						$("#pricing-result").html('<div class="norecordblock"><span class="norecordspan">No result found</span></div>');
						$('.backbtn').show();
					}
				}
			});		    
		} //if is numreic
		else{
			$("#prices").hide("slow");		
		}
	});
	$('#Size').change(function(){
		$("#prices").html('<div style="width:100%;text-align:center"><img alt="loading...." src="../images/loading.gif"></div>');
		var str = $("#pricingform").serialize();
		$.ajax({
			type: "POST",
			url: "ajax_pricing.php",
			data: str,
			cache: false,
			success: function(result){
				$("#prices").show("slow");			
				$("#prices").html(result);
			}
		});
	});
	
	$('#backbtn').click(function(){
		$('#pricing-result').hide();
		$('.backbtn').hide();
		$("#pricingform").css('visibility','visible');
	});
	
});	   
