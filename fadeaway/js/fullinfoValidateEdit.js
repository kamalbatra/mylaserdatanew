function validateFormEdit(fname,lname)
{ 
	//alert(fname);
	//alert(lname);

  var TreatmentRequest =document.forms["form"]["TreatmentRequest"].value;
   var    TreatmentRequestLen =TreatmentRequest.length; 
 
   if (TreatmentRequestLen < 1)
    {
     document.getElementById('treatmentRequestLenMsg').innerHTML="Please enter details!";
     document.forms["form"]["TreatmentRequest"].focus();
     return false;
    } 
    else if (TreatmentRequestLen > 1){
		document.getElementById('treatmentRequestLenMsg').innerHTML="";
	}
    
   
   
    if (( form.PhysicianCare[0].checked == false ) && ( form.PhysicianCare[1].checked == false ) )
	{		
	 document.getElementById('PhysicianCareMsg').innerHTML="Please select an option!";
     form.PhysicianCare[0].focus();
	 return false;
	}
	else if (( form.PhysicianCare[0].checked == true ) || ( form.PhysicianCare[1].checked == true ) )
	{
		document.getElementById('PhysicianCareMsg').innerHTML="";
	}
	
	
	
	if (form.PhysicianCare[0].checked == true ){	
	var PhysicanCareReason = document.forms["form"]["PhysicanCareReason"].value;
	var PhysicanCareReasonLen = PhysicanCareReason.length;	
	  if(PhysicanCareReasonLen < 1){
			document.getElementById('PhysicanCareReasoneMsg').innerHTML="Please enter reason!";
			
			 document.forms["form"]["PhysicanCareReason"].focus();
	     return false;
		}
		else if(PhysicanCareReasonLen > 1){
			document.getElementById('PhysicanCareReasoneMsg').innerHTML="";
		}
	}	
	
	
	else if (form.PhysicianCare[1].checked == true ){	
	
			document.getElementById('PhysicanCareReasoneMsg').innerHTML="";
		
	}
	
	
		
	//Cancer
	if ( ( form.Cancer[0].checked == false ) && ( form.Cancer[1].checked == false ) )
	{		
	 document.getElementById('CancerMsg').innerHTML="Please select an option!";
       form.Cancer[0].focus();
	 return false;
	}
	else if ( ( form.Cancer[0].checked == true ) || ( form.Cancer[1].checked == true ) )
	{		
	 document.getElementById('CancerMsg').innerHTML="";
       
	}
	
	
	
	
	
	//HighBloodPressure
	if ( ( form.HighBloodPressure[0].checked == false ) && ( form.HighBloodPressure[1].checked == false ) )
	{		
	 document.getElementById('HighBloodPressureMsg').innerHTML="Please select an option!";
       form.HighBloodPressure[0].focus();
	 return false;
	}
	else if ( ( form.HighBloodPressure[0].checked == true ) || ( form.HighBloodPressure[1].checked == true ) )
	{		
	 document.getElementById('HighBloodPressureMsg').innerHTML="";
      
	}
	
	
	
	//Arthritis	
	if ( ( form.Arthritis[0].checked == false ) && ( form.Arthritis[1].checked == false ) )
	{		
	 document.getElementById('ArthritisMsg').innerHTML="Please select an option!";
       form.Arthritis[0].focus();
	 return false;
	}
	else if ( ( form.Arthritis[0].checked == true ) || ( form.Arthritis[1].checked == true ) ){
		document.getElementById('ArthritisMsg').innerHTML="";
	}
	
	
	
	//ColdSores
	if ( ( form.ColdSores[0].checked == false ) && ( form.ColdSores[1].checked == false ) )
	{		
	 document.getElementById('ColdSoresMsg').innerHTML="Please select an option!";
       form.ColdSores[0].focus();
	 return false;
	}
	else if ( ( form.ColdSores[0].checked == true ) || ( form.ColdSores[1].checked == true ) )
	{		
	 document.getElementById('ColdSoresMsg').innerHTML="";
       
	}
	
	
	
	//Keloids
	if ( ( form.Keloids[0].checked == false ) && ( form.Keloids[1].checked == false ) )
	{		
	 document.getElementById('KeloidsMsg').innerHTML="Please select an option!";
       form.Keloids[0].focus();
	 return false;
	}
	else if ( ( form.Keloids[0].checked == true ) || ( form.Keloids[1].checked == true ) )
	{		
	 document.getElementById('KeloidsMsg').innerHTML="";
      
	}
	
	
	
	//SkinDisease
	if ( ( form.SkinDisease[0].checked == false ) && ( form.SkinDisease[1].checked == false ) )
	{		
	 document.getElementById('SkinDiseaseMsg').innerHTML="Please select an option!";
       form.SkinDisease[0].focus();
	 return false;
	}
	else if ( ( form.SkinDisease[0].checked == true ) || ( form.SkinDisease[1].checked == true ) )
	{		
	 document.getElementById('SkinDiseaseMsg').innerHTML="";
       
	}
	
	
	
	//Diabetes
	if ( ( form.Diabetes[0].checked == false ) && ( form.Diabetes[1].checked == false ) )
	{		
	 document.getElementById('DiabetesMsg').innerHTML="Please select an option!";
       form.Diabetes[0].focus();
	 return false;
	}
	else if ( ( form.Diabetes[0].checked == true ) || ( form.Diabetes[1].checked == true ) )
	{		
	 document.getElementById('DiabetesMsg').innerHTML="";
       
	}
	
	
	
	//HIV
	if ( ( form.HIV[0].checked == false ) && ( form.HIV[1].checked == false ) )
	{		
	 document.getElementById('HIVMsg').innerHTML="Please select an option!";
       form.HIV[0].focus();
	 return false;
	}
	else if ( ( form.HIV[0].checked == true ) || ( form.HIV[1].checked == true ) )
	{		
	 document.getElementById('HIVMsg').innerHTML="";
       
	}	
	
	
	
	//HormoneImbalance
	if ( ( form.HormoneImbalance[0].checked == false ) && ( form.HormoneImbalance[1].checked == false ) )
	{		
	 document.getElementById('HormoneImbalanceMsg').innerHTML="Please select an option!";
       form.HormoneImbalance[0].focus();
	 return false;
	}
	else if ( ( form.HormoneImbalance[0].checked == true ) || ( form.HormoneImbalance[1].checked == true ) )
	{		
	 document.getElementById('HormoneImbalanceMsg').innerHTML="";
      
	}
	
	
	
	
	//Seizures
	if ( ( form.Seizures[0].checked == false ) && ( form.Seizures[1].checked == false ) )
	{		
	 document.getElementById('SeizuresMsg').innerHTML="Please select an option!";
       form.Seizures[0].focus();
	 return false;
	}
	else if ( ( form.Seizures[0].checked == true ) || ( form.Seizures[1].checked == true ) )
	{		
	 document.getElementById('SeizuresMsg').innerHTML="";
       
	}
	
	//BloodClottingAbnormality
	if ( ( form.BloodClottingAbnormality[0].checked == false ) && ( form.BloodClottingAbnormality[1].checked == false ) )
	{		
	 document.getElementById('BloodClottingAbnormalityMsg').innerHTML="Please select an option!";
       form.BloodClottingAbnormality[0].focus();
	 return false;
	}
	
	else if ( ( form.BloodClottingAbnormality[0].checked == true ) || ( form.BloodClottingAbnormality[1].checked == true ) )
	{		
	 document.getElementById('BloodClottingAbnormalityMsg').innerHTML="";
       
	}
	
	//Hepatitis
	if ( ( form.Hepatitis[0].checked == false ) && ( form.Hepatitis[1].checked == false ) )
	{		
	 document.getElementById('HepatitisMsg').innerHTML="Please select an option!";
       form.Hepatitis[0].focus();
	 return false;
	}
	else if ( ( form.Hepatitis[0].checked == true ) || ( form.Hepatitis[1].checked == true ) )
	{		
	 document.getElementById('HepatitisMsg').innerHTML="";
       
	}
	
	//ThyroidImbalance
	if ( ( form.ThyroidImbalance[0].checked == false ) && ( form.ThyroidImbalance[1].checked == false ) )
	{		
	 document.getElementById('ThyroidImbalanceMsg').innerHTML="Please select an option!";
       form.ThyroidImbalance[0].focus();
	 return false;
	}
	else if ( ( form.ThyroidImbalance[0].checked == true ) || ( form.ThyroidImbalance[1].checked == true ) )
	{		
	 document.getElementById('ThyroidImbalanceMsg').innerHTML="";
       
	}
	
	
	//ActiveInfection
	if ( ( form.ActiveInfection[0].checked == false ) && ( form.ActiveInfection[1].checked == false ) )
	{		
	 document.getElementById('ActiveInfectionMsg').innerHTML="Please select an option!";
       form.ActiveInfection[0].focus();
	 return false;
	}
	else if ( ( form.ActiveInfection[0].checked == true ) || ( form.ActiveInfection[1].checked == true ) )
	{		
	 document.getElementById('ActiveInfectionMsg').innerHTML="";
    }
	
	
	
	
	//MedicalProblemsOther
	
	/*var MedicalProblemsOther = document.forms["form"]["MedicalProblemsOther"].value;
	var MedicalProblemsOtherLen = MedicalProblemsOther.length;	
	  if(MedicalProblemsOtherLen < 1 || MedicalProblemsOtherLen >100){
			document.getElementById('MedicalProblemsOtherMsg').innerHTML="Please enter minimum 100 word!";
			
			 document.forms["form"]["MedicalProblemsOther"].focus();
	     return false;
		}
	*/	
		
	//FoodAllergy
	if ( ( form.FoodAllergy[0].checked == false ) && ( form.FoodAllergy[1].checked == false ) )
	{		
	 document.getElementById('FoodAllergyMsg').innerHTML="Please select an option!";
       form.FoodAllergy[0].focus();
	 return false;
	}	
	else if ( ( form.FoodAllergy[0].checked == true ) || ( form.FoodAllergy[1].checked == true ) )
	{		
	 document.getElementById('FoodAllergyMsg').innerHTML="";
      
	}	
	
	
	//HydrocortisoneAllergy
	if ( ( form.HydrocortisoneAllergy[0].checked == false ) && ( form.HydrocortisoneAllergy[1].checked == false ) )
	{		
	 document.getElementById('HydrocortisoneAllergyMsg').innerHTML="Please select an option!";
       form.HydrocortisoneAllergy[0].focus();
	 return false;
	}
	else if ( ( form.HydrocortisoneAllergy[0].checked == true ) || ( form.HydrocortisoneAllergy[1].checked == true ) )
	{		
	 document.getElementById('HydrocortisoneAllergyMsg').innerHTML="";
      
	}
	
	
	//HydroquinoneAllergy
	if ( ( form.HydroquinoneAllergy[0].checked == false ) && ( form.HydroquinoneAllergy[1].checked == false ) )
	{		
	 document.getElementById('HydroquinoneAllergyMsg').innerHTML="Please select an option!";
       form.HydroquinoneAllergy[0].focus();
	 return false;
	}
	else if ( ( form.HydroquinoneAllergy[0].checked == true ) || ( form.HydroquinoneAllergy[1].checked == true ) )
	{		
	 document.getElementById('HydroquinoneAllergyMsg').innerHTML="";
     
	}
	
	//AspirinAllergy
	if ( ( form.AspirinAllergy[0].checked == false ) && ( form.AspirinAllergy[1].checked == false ) )
	{		
	 document.getElementById('AspirinAllergyMsg').innerHTML="Please select an option!";
       form.AspirinAllergy[0].focus();
	 return false;
	}
	
	else if ( ( form.AspirinAllergy[0].checked == true ) || ( form.AspirinAllergy[1].checked == true ) )
	{		
	 document.getElementById('AspirinAllergyMsg').innerHTML="";
      
	}
	
	//LatexAllergy
	if ( ( form.LatexAllergy[0].checked == false ) && ( form.LatexAllergy[1].checked == false ) )
	{		
	 document.getElementById('LatexAllergyMsg').innerHTML="Please select an option!";
       form.LatexAllergy[0].focus();
	 return false;
	}
	else if ( ( form.LatexAllergy[0].checked == true ) || ( form.LatexAllergy[1].checked == true ) )
	{		
	 document.getElementById('LatexAllergyMsg').innerHTML="";
      
	}
	
	
	//LidocaineAllergy
	if ( ( form.LidocaineAllergy[0].checked == false ) && ( form.LidocaineAllergy[1].checked == false ) )
	{		
	 document.getElementById('LidocaineAllergyMsg').innerHTML="Please select an option!";
       form.LidocaineAllergy[0].focus();
	 return false;
	}	
	else if ( ( form.LidocaineAllergy[0].checked == true ) || ( form.LidocaineAllergy[1].checked == true ) )
	{		
	 document.getElementById('LidocaineAllergyMsg').innerHTML="";
      
	}
	
	//OtherAllergy
	/*
	
	var OtherAllergy = document.forms["form"]["OtherAllergy"].value;
	var OtherAllergyLen = OtherAllergy.length;	
	  if(OtherAllergyLen < 1){
			document.getElementById('OtherAllergyMsg').innerHTML="Please enter value!";
			
			 document.forms["form"]["OtherAllergy"].focus();
	     return false;
		}
		
		*/
		
	//BloodThinners	
	if ( ( form.BloodThinners[0].checked == false ) && ( form.BloodThinners[1].checked == false ) )
	{		
	 document.getElementById('BloodThinnersMsg').innerHTML="Please select an option!";
       form.BloodThinners[0].focus();
	 return false;
	}
	else if ( ( form.BloodThinners[0].checked == true ) || ( form.BloodThinners[1].checked == true ) )
	{		
	 document.getElementById('BloodThinnersMsg').innerHTML="";
       
	}
	
	//Immunosuppressants	
	if ( ( form.Immunosuppressants[0].checked == false ) && ( form.Immunosuppressants[1].checked == false ) )
	{		
	 document.getElementById('ImmunosuppressantsMsg').innerHTML="Please select an option!";
       form.Immunosuppressants[0].focus();
	 return false;
	}
	else if ( ( form.Immunosuppressants[0].checked == true ) || ( form.Immunosuppressants[1].checked == true ) )
	{		
	 document.getElementById('ImmunosuppressantsMsg').innerHTML="";
      
	}
	
	//	HormoneTherapy
	if ( ( form.HormoneTherapy[0].checked == false ) && ( form.HormoneTherapy[1].checked == false ) )
	{		
	 document.getElementById('HormoneTherapyMsg').innerHTML="Please select an option!";
       form.HormoneTherapy[0].focus();
	 return false;
	}
	
	else if ( ( form.HormoneTherapy[0].checked == true ) || ( form.HormoneTherapy[1].checked == true ) )
	{		
	 document.getElementById('HormoneTherapyMsg').innerHTML="";
      
	}
	
	
	/* List your current medications ************/
	/* List your current medications ************/
	
	//CurrentMeds1
	/*
	var CurrentMeds1 = document.forms["form"]["CurrentMeds1"].value;
	var CurrentMeds1Len = CurrentMeds1.length;	
	  if(CurrentMeds1Len < 1){
			document.getElementById('CurrentMeds1Msg').innerHTML="Please enter value!";
			
			 document.forms["form"]["CurrentMeds1"].focus();
	     return false;
		}
	*/
	
	
	
	//PsychMeds
	if ( ( form.PsychMeds[0].checked == false ) && ( form.PsychMeds[1].checked == false ) )
	{		
	 document.getElementById('PsychMedsMsg').innerHTML="Please select an option!";
       form.PsychMeds[0].focus();
	 return false;
	}
	else if ( ( form.PsychMeds[0].checked == true ) || ( form.PsychMeds[1].checked == true ) )
	{		
	 document.getElementById('PsychMedsMsg').innerHTML="";
       
	}
	
	
	//	Accutane
	
	if ( ( form.Accutane[0].checked == false ) && ( form.Accutane[1].checked == false ) )
	{		
	 document.getElementById('AccutaneMsg').innerHTML="Please select an option!";
       form.Accutane[0].focus();
	 return false;
	}
	else if ( ( form.Accutane[0].checked == true ) || ( form.Accutane[1].checked == true ) )
	{		
	 document.getElementById('AccutaneMsg').innerHTML="";
      
	}
	
	if (form.Accutane[0].checked == true ){
			
			var AccutaneLastUse = document.forms["form"]["AccutaneLastUse"].value;
			var AccutaneLastUseLen = AccutaneLastUse.length;	
		  if(AccutaneLastUseLen < 1){
				document.getElementById('AccutaneLastUseMsg').innerHTML="Please enter details of last use!";
				
				 document.forms["form"]["AccutaneLastUse"].focus();
			 return false;
			}
			else  if(AccutaneLastUseLen > 1){
				document.getElementById('AccutaneLastUseMsg').innerHTML="";
				
			}
		}
	 else if (form.Accutane[1].checked == true ){	
		document.getElementById('AccutaneLastUseMsg').innerHTML="";
    }
	
	
	
	
	//Retinoids
	if (( form.Retinoids[0].checked == false ) && ( form.Retinoids[1].checked == false ) )
	{		
	   document.getElementById('RetinoidsMsg').innerHTML="Please select an option!";
       form.Retinoids[0].focus();
	   return false;
	}
	else if (( form.Retinoids[0].checked == true ) || ( form.Retinoids[1].checked == true ) )
	{		
	   document.getElementById('RetinoidsMsg').innerHTML="";
       
	}
	
		if (form.Retinoids[0].checked == true ){
			
			var RetinoidsLastUse = document.forms["form"]["RetinoidsLastUse"].value;
			var RetinoidsLastUseLen = RetinoidsLastUse.length;	
		  if(RetinoidsLastUseLen < 1){
				document.getElementById('RetinoidsLastUseMsg').innerHTML="Please enter date of last use!";
				
				 document.forms["form"]["RetinoidsLastUse"].focus();
			 return false;
			}
			else  if(RetinoidsLastUseLen > 1){
				document.getElementById('RetinoidsLastUseMsg').innerHTML="";
				
			}
		}
		else if (form.Retinoids[1].checked == true ){
			document.getElementById('RetinoidsLastUseMsg').innerHTML="";
		}
		
		
		
	//Herbals
		var Herbals = document.forms["form"]["Herbals"].value;
	    var HerbalsLen = Herbals.length;	
	  if(HerbalsLen < 1){
			document.getElementById('HerbalsMsg').innerHTML="Please enter details!";			
			document.forms["form"]["Herbals"].focus();
	     return false;
		}
		else if(HerbalsLen > 1){
			document.getElementById('HerbalsMsg').innerHTML="";				
		}
		
		
	//History
	//PriorTattooLaser
	if ((form.PriorTattooLaser[0].checked == false ) && ( form.PriorTattooLaser[1].checked == false ) )
	{		
	   document.getElementById('PriorTattooLaserMsg').innerHTML="Please select an option!";
       form.PriorTattooLaser[0].focus();
	   return false;
	}
	else if ((form.PriorTattooLaser[0].checked == true ) || ( form.PriorTattooLaser[1].checked == true ) )
	{		
	   document.getElementById('PriorTattooLaserMsg').innerHTML="";
       
	}
		
	if (form.PriorTattooLaser[0].checked == true ){
			
			var PriorTattooLaserInfo = document.forms["form"]["PriorTattooLaserInfo"].value;
			var PriorTattooLaserInfoLen = PriorTattooLaserInfo.length;	
		  if(PriorTattooLaserInfoLen < 1){
				document.getElementById('PriorTattooLaserInfoMsg').innerHTML="Please enter details!";
				
				 document.forms["form"]["PriorTattooLaserInfo"].focus();
			 return false;
			}
			else if(PriorTattooLaserInfoLen > 1){
				document.getElementById('PriorTattooLaserInfoMsg').innerHTML="";			
				
			}
		}
	else if (form.PriorTattooLaser[1].checked == true ){
		document.getElementById('PriorTattooLaserInfoMsg').innerHTML="";			
	}
		
	//RecentTanning		
	if ((form.RecentTanning[0].checked == false ) && ( form.RecentTanning[1].checked == false ) )
	{		
	   document.getElementById('RecentTanningMsg').innerHTML="Please select an option!";
       form.RecentTanning[0].focus();
	   return false;
	}
	else if ((form.RecentTanning[0].checked == true ) || ( form.RecentTanning[1].checked == true ) )
	{		
	   document.getElementById('RecentTanningMsg').innerHTML="";
       
	}
	
	//RecentSelfTanningLotions
	if ((form.RecentSelfTanningLotions[0].checked == false ) && ( form.RecentSelfTanningLotions[1].checked == false ) )
	{		
	   document.getElementById('RecentSelfTanningLotionsMsg').innerHTML="Please select an option!";
       form.RecentSelfTanningLotions[0].focus();
	   return false;
	}
	else if ((form.RecentSelfTanningLotions[0].checked == true ) || ( form.RecentSelfTanningLotions[1].checked == true ) )
	{		
	   document.getElementById('RecentSelfTanningLotionsMsg').innerHTML="";
       
	}
	
	//BadScars
	if ((form.BadScars[0].checked == false ) && ( form.BadScars[1].checked == false ) )
	{		
	   document.getElementById('BadScarsMsg').innerHTML="Please select an option!";
       form.BadScars[0].focus();
	   return false;
	}
	else if ((form.BadScars[0].checked == true ) || ( form.BadScars[1].checked == true ) )
	{		
	   document.getElementById('BadScarsMsg').innerHTML="";
      
	}
	if (form.BadScars[0].checked == true ){
			
			var BadScarsInfo = document.forms["form"]["BadScarsInfo"].value;
			var BadScarsInfoLen = BadScarsInfo.length;	
		  if(BadScarsInfoLen < 1){
				document.getElementById('BadScarsInfoMsg').innerHTML="Please enter details!";
				
				 document.forms["form"]["BadScarsInfo"].focus();
			 return false;
			}
			else if(BadScarsInfoLen > 1){
				document.getElementById('BadScarsInfoMsg').innerHTML="";
				
			} 
		}
	else if (form.BadScars[1].checked == true ){
		document.getElementById('BadScarsInfoMsg').innerHTML="";
	}	
		
	
	//Dyspigmentation
	
	if ((form.Dyspigmentation[0].checked == false ) && ( form.Dyspigmentation[1].checked == false ) )
	{		
	   document.getElementById('DyspigmentationMsg').innerHTML="Please select an option!";
       form.Dyspigmentation[0].focus();
	   return false;
	}
	else if ((form.Dyspigmentation[0].checked == true ) || ( form.Dyspigmentation[1].checked == true ) )
	{		
	   document.getElementById('DyspigmentationMsg').innerHTML="";
       
	}
	
	/*  Female Clients ***/
	/*  Female Clients ***/
	/*  Female Clients ***/
	
	//PregnancyRisk
	
	if ((form.PregnancyRisk[0].checked == false ) && ( form.PregnancyRisk[1].checked == false ) )
	{		
	   document.getElementById('PregnancyRiskMsg').innerHTML="Please select an option!";
       form.PregnancyRisk[0].focus();
	   return false;
	}
	else if ((form.PregnancyRisk[0].checked == true ) || ( form.PregnancyRisk[1].checked == true ) )
	{		
	   document.getElementById('PregnancyRiskMsg').innerHTML="";
       
	}
	
	//BreastFeeding
	
	if ((form.BreastFeeding[0].checked == false ) && ( form.BreastFeeding[1].checked == false ) )
	{		
	   document.getElementById('BreastFeedingMsg').innerHTML="Please select an option!";
       form.BreastFeeding[0].focus();
	   return false;
	}
	else if ((form.BreastFeeding[0].checked == true ) || ( form.BreastFeeding[1].checked == true ) )
	{		
	   document.getElementById('BreastFeedingMsg').innerHTML="";
       
	}

	
	//ContraceptionUse
	
	if ((form.ContraceptionUse[0].checked == false ) && ( form.ContraceptionUse[1].checked == false ) )
	{		
	   document.getElementById('ContraceptionUseMsg').innerHTML="Please select an option!";
       form.ContraceptionUse[0].focus();
	   return false;
	}
	else if ((form.ContraceptionUse[0].checked == true ) || ( form.ContraceptionUse[1].checked == true ) )
	{		
	   document.getElementById('ContraceptionUseMsg').innerHTML="";
       
	}
	
	//ClientSignature
	

	
	    var ClientSignature = document.forms["form"]["ClientSignature"].value;
	    
	    var fname1 = document.forms["form"]["FirstName"].value;
	    var lname1 = document.forms["form"]["LastName"].value;
	    
	    var ClientSignatureLen = ClientSignature.length;
	    
	  if(ClientSignatureLen < 1 ){
		  //alert(ClientSignature);	
			document.getElementById('ClientSignatureMsg').innerHTML="Please enter your name (First name  Last name)!";			
			document.forms["form"]["ClientSignature"].focus();
	     return false;
		}	   
	   else if(ClientSignatureLen > 1 ){
		    //var ClientSignatureVal = document.forms["form"]["ClientSignature"].value;
		   var fnamePlsLname = fname1.toLowerCase() +' '+ lname1.toLowerCase();
		  
           var ClientSignatureToLower = ClientSignature.toLowerCase();
		   //alert(fnamePlsLname);
		   if(fnamePlsLname !=ClientSignatureToLower){
			   document.getElementById('ClientSignatureMsg').innerHTML="Please enter your name (First name  Last name)!";
			   
			    return false;
		   }
		  else if(fnamePlsLname ==ClientSignatureToLower){
			  document.getElementById('ClientSignatureMsg').innerHTML="";
		   }
		   
	   }

		
		
	
    

}// validate fnction off here





// function call from fullinfo.php

//enablePhysicanCareReason
function enablePhysicanCareReason(){
	document.getElementById('PhysicanCareReason').disabled = false;
}
function disablePhysicanCareReason(){
	document.getElementById('PhysicanCareReason').value ="";
	document.getElementById('PhysicanCareReason').disabled = true;
}


function disableTxt(){
	//alert("aaa");
	document.getElementById('AccutaneLastUse').value ="";
	document.getElementById('AccutaneLastUse').disabled = true;
}
function enableTxt(){
document.getElementById('AccutaneLastUse').disabled = false;
}


//RetinoidsLastUse for text box
function disableTxtBox(){
	document.getElementById('RetinoidsLastUse').value ="";
	document.getElementById('RetinoidsLastUse').disabled = true;
	
}
function enableTxtBox(){
	document.getElementById('RetinoidsLastUse').disabled = false;
}

//PriorTattooLaserInfo for text box
function enablePriorTattooLaserInfo(){
	document.getElementById('PriorTattooLaserInfo').disabled = false;
}
function disablePriorTattooLaserInfo(){
	document.getElementById('PriorTattooLaserInfo').value ="";
	document.getElementById('PriorTattooLaserInfo').disabled = true;
}


//BadScarsInfo for text box
function enableBadScarsInfoInfo(){
	document.getElementById('BadScarsInfo').disabled = false;
}
function disableBadScarsInfoInfo(){
	document.getElementById('BadScarsInfo').value ="";
	document.getElementById('BadScarsInfo').disabled = true;
}

