function getXmlHttpRequestObject() {
 if (window.XMLHttpRequest) {
  return new XMLHttpRequest();
 } else if(window.ActiveXObject) {
  return new ActiveXObject("Microsoft.XMLHTTP");
 } else {
  alert("Your Browser Sucks!");
 }
}
 
//Our XmlHttpRequest object to get the auto suggest
var searchReq = getXmlHttpRequestObject();
 
//Called from keyup on <span class="IL_AD" id="IL_AD2">the search</span> textbox.
//Starts the AJAX request.
function searchSuggest() {
 if (searchReq.readyState == 4 || searchReq.readyState == 0) {
  var str = escape(document.getElementById('dbTxt').value);
  searchReq.open("GET", 'searchSuggest.php?search=' + str, true);
  searchReq.onreadystatechange = handleSearchSuggest;
  searchReq.send(null);
 }
}
 
//Called when the AJAX response is returned.
function handleSearchSuggest() {
 if (searchReq.readyState == 4) {
         var ss = document.getElementById('layer1');
  var str1 = document.getElementById('dbTxt');
  var curLeft=0;
  if (str1.offsetParent){
      while (str1.offsetParent){
   curLeft += str1.offsetLeft;
   str1 = str1.offsetParent;
      }
  }
  var str2 = document.getElementById('dbTxt');
  var curTop=20;
  if (str2.offsetParent){
      while (str2.offsetParent){
   curTop += str2.offsetTop;
   str2 = str2.offsetParent;
      }
  }
  var str =searchReq.responseText.split("\n");
  $("#mydiv2").hide();
  if(str.length==1)
      document.getElementById('layer1').style.visibility = "hidden";
  else
      ss.setAttribute('style','z-index:2;background:#F0F0F0;font-size:13px;color:#000000;top:'+curTop+';left:58;width:250;z-index:1;padding:2px 6px; border: 1px solid #cccccc; border-top:none; line-height:22px; overflow:auto; height:105; ');
  ss.innerHTML = '';
  for(i=0; i < str.length - 1; i++) {
   //Build our element string.  This is cleaner using the DOM, but
   //IE doesn't support dynamically added attributes.
   var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
            suggest += 'onmouseout="javascript:suggestOut(this);" ';
            suggest += 'onclick="javascript:setSearch(this.innerHTML);" ';
            suggest += 'class="small">' + $.trim(str[i]) + '</div>';
            ss.innerHTML += suggest;
 
  }
 }
}
 
//Mouse over function
function suggestOver(div_value) {
 div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
 div_value.className = 'suggest_link';
}
//Click function
function setSearch(value) {
	//value=strip_tags(value);
	//alert(value);
	
	var ph_first = value.indexOf('[');
	var ph_last = value.indexOf(']');
	var val_1 = value;
	var val_leng = parseInt(ph_last)-parseInt(ph_first);
	
	val_1 = val_1.substr(parseInt(ph_first)-2,parseInt(val_leng)+4);
	value = value.replace(val_1,'');
	
	var origvalue = value.substring(value.indexOf('<span'));
	//alert(substr);
	//alert(val_1);
	value1=origvalue.replace(/<span>/gi, "\n");
value1=value1.replace(/<p.*>/gi, "\n");
value1=value1.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 (Link->$1) ");
value1=value1.replace(/<(?:.|\s)*?>/g, "");
var origval = value1;

value=value.replace(/<span>/gi, "\n");
value=value.replace(/<p.*>/gi, "\n");
value=value.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 (Link->$1) ");
value=value.replace(/<(?:.|\s)*?>/g, "");
//var origval = value;
value=value.replace(origval, "");
//alert(value);
//value=value.replace(/[0-9]/g, "");
//alert(value);
 document.getElementById('dbTxt').value = value;
 loadXMLDoc();
 
   function loadXMLDoc()
    {
    var xmlhttp;
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
		
		 
        document.getElementById("myDiv").innerHTML=xmlhttp.responseText;

		myDivObj = document.getElementById("missingconsent");

		if (myDivObj ) {

			if(myDivObj.innerHTML == 'Missing Consent Form' ){
			$("#treatClient").attr("disabled", "disabled"); 
			$("#treatClient").addClass("treatClient"); 
			$("#treatClientHair").attr("disabled", "disabled"); 
			$("#treatClientHair").addClass("treatClient"); 

			}
		}else{
			$("#treatClient").removeClass("treatClient"); 
			$("#treatClient").prop("disabled", false);
			$("#treatClientHair").removeClass("treatClient"); 
			$("#treatClientHair").prop("disabled", false);
		}

		/*****************edited for disabling treatclient button*************/
		
		var abc = document.getElementById('techreview').value;
        if (abc == '') 
        {
			$(".tech-signature").show();
			$("#treatClient").prop("disabled", true); 
			$("#treatClientHair").prop("disabled", true); 
			$("#treatClient").addClass("treatClient"); 
			$("#treatClientHair").addClass("treatClient"); 
		} 
		else 
		{
			$(".tech-signature").hide();
			$("#treatClient").removeClass("treatClient"); 
			$("#treatClient").prop("disabled", false);
	    }
	    
	    /**********************ends*************************************/

        }
      }
    xmlhttp.open("GET","livesearch.php?name="+origval,true);
    xmlhttp.send();
    }
 
 
 
 document.getElementById('layer1').innerHTML = '';
 document.getElementById('layer1').style.visibility = "hidden";
}

