<?php
	// You need to have reference transactions enabled
	// to enable it on sandbox account, you need to ask it here: https://www.x.com/thread/38753
	// To enable reference transactions on live site you need to contact PayPal Business Services Group
	session_start();
	require_once "lib/PaypalRecurringPayments.php";
	include("../includes/dbFunctions.php");

	$subtable = "tbl_subscription_history";
	$bsnstable = "tbl_business";
	$plantable = "tbl_master_plans";
	
	$subscribe = new dbFunctions();
	$gateway = new PaypalGateway();
	
	//~ $gateway->apiUsername = "testclerisybus_api1.gmail.com";
	//~ $gateway->apiPassword = "Z3WZF5RUY4RRCZXK";
	//~ $gateway->apiSignature = "ANY.BtsshtClJCsk6sDRaFPJ8fRcA8J7LH7SxTiGMGOXVYT.omBLqbf6";
	//~ $gateway->testMode = true;
	
	$gateway->apiUsername = "info_api1.fadeawaylaser.com";
	$gateway->apiPassword = "JZWBPSZ8X4YVYAFJ";
	$gateway->apiSignature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AgBf.2Egon268UC47otPIDY4qAJC";
	$gateway->testMode = false;
	
	// Return (success) and cancel url setup
	$gateway->returnUrl = "http://".$_SERVER['HTTP_HOST']."/fadeaway/membership-subscription/index.php?action=success";
	$gateway->cancelUrl = "http://".$_SERVER['HTTP_HOST']."/fadeaway/membership-subscription/index.php?action=cancel";
	$recurring = new PaypalRecurringPayments($gateway);
	
	if(isset($_POST['planId']) || isset($_SESSION['planId'])){
		$planID = isset($_SESSION['planId']) ? $_SESSION['planId'] : base64_decode($_POST['planId']);
		$_SESSION['planId'] = $planID;
		$plancond = "where id=".$planID;
		$plandata = $subscribe->selectTableSingleRow($plantable,$plancond,$cols="*");
		
	}else{
		?>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script>
			$(function(){
				window.location.replace("../admin/dashboard.php");
				return false;
			});
		</script>
		<?php
		die;
	}
	
	switch ($_GET['action']) {
	
		case "": // Index page, here you should be redirected to Paypal

			$resultData = array();
			$descriptionData['plan_name'] = $plandata['title'];
			$descriptionData['price'] = $plandata['amount'];
			$isOk = $recurring->obtainBillingAgreement($descriptionData, "", 'USD', $resultData);
			if (!$isOk) {
				print_r($resultData);
			}
			break;
		case "success": // Paypal says everything's fine (see $gateway->returnUrl)

			$resultData = array();
			$details = $recurring->getBillingDetails($resultData);
			if (!$details) {
				echo "Something went wrong\n";
				print_r($resultData);
				return;
			}
			$billingAgreementId = $recurring->doInitialPayment($details->token, $details->payerId, $plandata['amount'], $resultData);
			if (!$billingAgreementId) {
				echo "Something went wrong\n";
				print_r($resultData);
				return;
			}
			/********** INSERT IN SUBSCRIPTION HISTORY ************/			
			$today = date("Y-m-d H:i:s");
			$expiredate = strtotime("+".$plandata["days"]." days", strtotime($today));
			$expiredate = date("Y-m-d H:i:s", $expiredate);
			
			$subcreds["BusinessID"] = $_SESSION["BusinessID"];
			$subcreds["RenewalDate"] = date("Y-m-d H:i:s");
			$subcreds["ExpireDate"] = $expiredate;
			$subcreds["PlanID"] = $planID;
			$subcreds["PaymentAmount"] = $resultData["AMT"];
			$subcreds["PaymentStatus"] = $resultData["PAYMENTSTATUS"];
			$subcreds["TimeStamp"] = $resultData["TIMESTAMP"];
			$subcreds["TransactionID"] = $resultData["TRANSACTIONID"];
			$subscribe->insert_data($subtable,$subcreds);
			
			/******** UPDATE BUSINESS TABLE ********/
			if( $plandata['title'] == "Single Location" ){
				$LocationPackage = 1;
			}else {
				$LocationPackage = 2;
			}			
			$bsnscreds["BusinessID"] = $_SESSION["BusinessID"];
			$bsnscreds["PayerID"] = $details->payerId;
			$bsnscreds["BillingAgreementID"] = $resultData["BILLINGAGREEMENTID"];
			$bsnscreds["PaypalToken"] = $resultData["TOKEN"];
			$bsnscreds["PlanType"] = "Subscribed";
			$bsnscreds["Optout"] = "No";
			$bsnscreds["LocationPackage"] = $LocationPackage;			
			$subscribe->update_spot($bsnstable,$bsnscreds);
			
			$subcondition = "where BusinessID=".$_SESSION['BusinessID']." order by ID desc";
			$subdetails = $subscribe->selectTableSingleRow($subtable,$subcondition,$cols="*");
			$expire = date("Y-m-d", strtotime($subdetails["ExpireDate"]));
			$today = date('Y-m-d');
			$period = (strtotime($expire)- strtotime($today))/24/3600;
			$_SESSION["periodtime"] = $period;		
			$_SESSION["ExpireDate"] = date("F j, Y", strtotime($expiredate));
			if($period>0) 
				$_SESSION["period"] = "true";
			/****************** Send Payment Success EMail ******************/
			include("../sendAlertEmails.php");
			$useremail = new sendAlertEmails();		
			$useremail->moveTrialtoSubscribe($_SESSION['First_Name'], $_SESSION["ExpireDate"], $_SESSION['Emp_email']);
			
			/***************************** End *****************************/
			unset($_SESSION['planId']);
			header("location:../admin/dashboard.php?sub=success");			
			break;		
		// Type ?action=test in browser to perform a subscription (reference) transaction
		case "recurring":
		// print_r($plandata); die;
			$billingAgreementCon = " WHERE BusinessID = $_SESSION[BusinessID] ";
			$BillingAgreementID = $subscribe->selectTableSingleRow("tbl_business",$billingAgreementCon,"BillingAgreementID");
			$resultData = array();
			$billingAgreementId = $BillingAgreementID['BillingAgreementID'];
			// To perform payments you need to store billing agreement ID in your database
			$isOk = $recurring->doSubscriptionPayment($billingAgreementId, $plandata['amount'], $resultData);
			if ($isOk) {
				$plandays = $plandata["days"];
				$today = date("Y-m-d H:i:s");
				$expiredate = strtotime("+".$plandays." days", strtotime($today));
				$expiredate = date("Y-m-d H:i:s", $expiredate); 
				$subdata["BusinessID"] = $_SESSION["BusinessID"];
				$subdata["RenewalDate"] = date("Y-m-d H:i:s");
				$subdata["ExpireDate"] = $expiredate;
				$subdata["PlanID"] = $planID;
				$subdata["PaymentAmount"] = $resultData["AMT"];
				$subdata["PaymentStatus"] = $resultData["PAYMENTSTATUS"];
				$subdata["TimeStamp"] = $resultData["TIMESTAMP"];
				$subdata["TransactionID"] = $resultData["TRANSACTIONID"];
				$subscribe->insert_data($subtable,$subdata);

				if( $plandata['title'] == "Single Location" ){
					$LocationPackage = 1;
				}else {
					$LocationPackage = 2;
				}			
				$bsnlocupdate["BusinessID"] = $_SESSION["BusinessID"];
				$bsnlocupdate["LocationPackage"] = $LocationPackage;			
				$subscribe->update_spot($bsnstable,$bsnlocupdate);
			

				$subcondition = "where BusinessID=".$_SESSION['BusinessID']." order by ID desc";
				$subdetails = $subscribe->selectTableSingleRow($subtable,$subcondition,$cols="*");
				$expire = date("Y-m-d", strtotime($subdetails["ExpireDate"]));
				$today = date('Y-m-d');
				$period = (strtotime($expire)- strtotime($today))/24/3600;
				$_SESSION["periodtime"] = $period;		
				$_SESSION["ExpireDate"] = date("F j, Y", strtotime($expiredate));
				if($period>0) 
					$_SESSION["period"] = "true";

				unset($_SESSION['planId']);
				header("location:../admin/dashboard.php?sub=success");
			} else
				print_r($resultData);
			break;		
		case "cancel": // User cancel subscription process (see $gateway->cancelUrl)
			?>
			<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
			<script>
				$(function(){
					window.location.replace("../admin/dashboard.php");
					return false;
				});
			</script>
			<?php 
			break;
	}
?>
