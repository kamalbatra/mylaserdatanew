<?php
	
	require_once("includes/config.php");
	require 'mail/PHPMailer-master/PHPMailerAutoload.php';	
	require_once 'MCAPI.class.php';
	
	$apikey='345e92fb68c4e6040196605098571f19-us4'; // Enter tb MailChimp API key here
	$api = new MCAPI($apikey);
	$retval = $api->templates();
	
	define("MANDRILL_HOST","smtp.mandrillapp.com");
	define("MANDRILL_SMTPAUTH","true");
	define("MANDRILL_USER","monkeydoodle@gmail.com");
	define("MANDRILL_PASS","0L611t4BpDwbq8_x-2a9Sw");
	define("MANDRILL_SECURE","tls");
	define("ADMIN_EMAIL_FROM","info@fadeawaylaser.com");
	
	class sendAlertEmails {
		public $con;
		function __construct() {
			// $this->con = mysql_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD);
			$this->con = mysqli_connect(DB_HOST,DB_USER_NAME,DB_PASSWORD,DB_NAME);
			if (!$this->con) {
				die('Could not connect: ' . mysqli_error());
			}		 
			// mysql_select_db(DB_NAME, $this->con) or die("database not found");
		}	
		
		public function sendAlerts($fname, $lname, $expiredate, $toemail) {
			$mail = new PHPMailer;
			$subject = "FadeAway Laser :: Reminder for renewal of your account";
			$mail->Host = MANDRILL_HOST;                          // Specify main and backup SMTP servers
			$mail->Port = 587;
			$mail->SMTPAuth = MANDRILL_SMTPAUTH;                // Enable SMTP authentication
			$mail->Username = MANDRILL_USER;                     // SMTP username
			$mail->Password = MANDRILL_PASS;                    // SMTP password
			$mail->SMTPSecure = MANDRILL_SECURE;                // Enable encryption, 'ssl' also accepted
			$mail->FromName = 'FadeAway';
			$mail->isHTML(true);
			$mail->From = ADMIN_EMAIL_FROM;
			$mail->addAddress($toemail);
			$mail->Subject = $subject;
			//date("Y-m-d", strtotime($subdetails["ExpireDate"]));
			$expiredate=date("F j, Y", strtotime($expiredate));
			$content = "Hello $fname,<br/>Please note that the renewal date of your Fadeaway account is $expiredate.  So, please maintain enough balance in your PayPal account.<br/><br/>--<br/>Best Regards,<br/>FadeAway Laser";
			//echo "<br/>";
			$mail->Body = $content;
			if(!$mail->Send()) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
				echo '<br/>';
			}
		}
		
		public function sendMsg($fname, $expiredate, $toemail) {
			$mail = new PHPMailer;
			$subject = "FadeAway Laser :: Successfull Subscription";
			$mail->Host = MANDRILL_HOST;                          // Specify main and backup SMTP servers
			$mail->Port = 587;
			$mail->SMTPAuth = MANDRILL_SMTPAUTH;                // Enable SMTP authentication
			$mail->Username = MANDRILL_USER;                     // SMTP username
			$mail->Password = MANDRILL_PASS;                    // SMTP password
			$mail->SMTPSecure = MANDRILL_SECURE;                // Enable encryption, 'ssl' also accepted
			$mail->FromName = 'FadeAway';
			$mail->isHTML(true);
			$mail->From = ADMIN_EMAIL_FROM;
			$mail->addAddress($toemail);
			$mail->Subject = $subject;
			//date("Y-m-d", strtotime($subdetails["ExpireDate"]));
			$expiredate=date("F j, Y", strtotime($expiredate));
			$content = "Hello $fname,<br/>You have successfully subscribed for one month. The renewal date of your plan is $expiredate.<br/><br/>--<br/>Best Regards,<br/>FadeAway Laser"; 
			//echo "<br/>";
			$mail->Body = $content;
			if(!$mail->Send()) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
				echo '<br/>';
			}
		}
		
		public function moveTrialtoSubscribe($fname, $expiredate, $toemail) {
			$mail = new PHPMailer;
			$subject = "FadeAway Laser :: Successfull Subscription";
			$mail->Host = MANDRILL_HOST;                          // Specify main and backup SMTP servers
			$mail->Port = 587;
			$mail->SMTPAuth = MANDRILL_SMTPAUTH;                // Enable SMTP authentication
			$mail->Username = MANDRILL_USER;                     // SMTP username
			$mail->Password = MANDRILL_PASS;                    // SMTP password
			$mail->SMTPSecure = MANDRILL_SECURE;                // Enable encryption, 'ssl' also accepted
			$mail->FromName = 'FadeAway';
			$mail->isHTML(true);
			$mail->From = ADMIN_EMAIL_FROM;
			$mail->addAddress($toemail);
			$mail->Subject = $subject;
			//date("Y-m-d", strtotime($subdetails["ExpireDate"]));
			$expiredate=date("F j, Y", strtotime($expiredate));
			$content = "Hello $fname,<br/>You have successfully subscribed for one month. The renewal date of your plan is $expiredate.<br/><br/>--<br/>Best Regards,<br/>FadeAway Laser"; 
			//echo "<br/>";
			$mail->Body = $content;
			if(!$mail->Send()) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
				echo '<br/>';
			}
		}
	}
?>
