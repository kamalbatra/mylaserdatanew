<?php
	require_once "membership-subscription/lib/PaypalRecurringPayments.php";
	include("includes/dbFunctions.php");
	$gateway = new PaypalGateway();
	$gateway->apiUsername = "testclerisybus_api1.gmail.com";
	$gateway->apiPassword = "Z3WZF5RUY4RRCZXK";
	$gateway->apiSignature = "ANY.BtsshtClJCsk6sDRaFPJ8fRcA8J7LH7SxTiGMGOXVYT.omBLqbf6";
	$gateway->testMode = true;
	$recurring = new PaypalRecurringPayments($gateway);		
	$alerts = new dbFunctions();
	
	include("sendAlertEmails.php");
	$useremail = new sendAlertEmails();		
	$table1 = "tbl_business";
	$table2 = "tbl_employees";
	$condition = "tbl_business.BusinessID=tbl_employees.BusinessID and tbl_employees.Admin='TRUE' and tbl_business.PlanType='Subscribed' AND tbl_business.Optout='No'";
	$bdetails = $alerts->selectTableJoin($table1,$table2,$join,$condition,$cols="*");
	foreach($bdetails as $business) {
		$subtable = "tbl_subscription_history";
		$scondition = "where BusinessID=".$business["BusinessID"]." order by ID desc";
		$subdetails = $alerts-> selectTableSingleRow($subtable,$scondition,$cols="*");
		$expiredate = $subdetails["ExpireDate"];
		$expiredate = date("Y-m-d", strtotime($expiredate));
		$today=date('Y-m-d');
		$period = (strtotime($expiredate)- strtotime($today))/24/3600;
		if($period<=3 && $period>0) {
			$useremail->sendAlerts($business["First_Name"],$business["Last_Name"],$expiredate,$business["Emp_email"]);
		} else if($period==0){
			$resultData = array();
			$billingAgreementId = $business["BillingAgreementID"];
			// To perform payments you need to store billing agreement ID in your database
			$plantable = "tbl_master_plans";
			$planID = $subdetails["PlanID"];
			$condition = "where id=".$planID;
			$plandata = $alerts->selectTableSingleRow($plantable,$condition,$cols="*");
			$isOk = $recurring->doSubscriptionPayment($billingAgreementId, $plandata['amount'], $resultData);
			if ($isOk) {
				$plandays = $plandata["days"];
				$today = date("Y-m-d H:i:s");
				$expiredate = strtotime("+".$plandays." days", strtotime($today));
				$expiredate = date("Y-m-d H:i:s", $expiredate); 
				$subdata["BusinessID"] = $business["BusinessID"];
				$subdata["RenewalDate"] = date("Y-m-d H:i:s");
				$subdata["ExpireDate"] = $expiredate;
				$subdata["PlanID"] = $planID;
				$subdata["PaymentAmount"] = $resultData["AMT"];
				$subdata["PaymentStatus"] = $resultData["PAYMENTSTATUS"];
				$subdata["TimeStamp"] = $resultData["TIMESTAMP"];
				$subdata["TransactionID"] = $resultData["TRANSACTIONID"];
				$alerts->insert_data($subtable,$subdata);				
				$useremail->sendMsg($business["First_Name"], $expiredate, $business["Emp_email"]);				
			} else {
				echo "Not OK<br/>";
				print_r($resultData);
			}
		}
	}
?>
