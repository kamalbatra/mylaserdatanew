<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
global $theme_option;

get_header(); 
?>

	<div id="inner-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">               
			        <div class="fourohfour">
			            <div class="banner-title title">                            
		                    <h1 class="white"><span class="color"><?php _e('404: ','meminz');?></span> <?php _e('Page not Found','meminz');?></h1>
		                    <p class="lead"><?php echo esc_html($theme_options['404_intro'] ); ?></p>
		                    <a href="<?php echo esc_url(home_url('/')); ?>" class="btn btn-default"><?php _e('BACK TO HOME','meminz'); ?></a>
			            </div>
			        </div>              
				</div>              
		    </div> 
		</div>              
	</div>	


<?php get_footer(); ?>