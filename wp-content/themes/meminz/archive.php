 <?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

 get_header();

 global $theme_options;?>

<!-- Start page header -->
	<div class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php the_breadcrumb(); ?>
				</div>		
			</div>	
		</div>		
	</div>
	<!-- End page header -->
	<div id="inner-page">
	<div class="container">
		<div class="row">
		
			<?php if($theme_options['blog_layout']==='left_sidebar' || get_post_meta(get_the_ID(), '_cmb_single_layout', true)==="left_sidebar"):?>
				<div class="col-md-3 sidebar-left">
					<aside>
						<?php get_sidebar( );?>
					</aside>
				</div>
			<?php endif;?>
			<?php if($theme_options['blog_layout']==='fullwidth' || get_post_meta(get_the_ID(), '_cmb_single_layout', true)==="fullwidth"):?>
			<div class="col-md-12">
			<?php else:?>
			<div class="col-md-9">
			<?php endif;?>

				<?php if(have_posts()) : 
					$postindex = 1;
					?>
					<?php while(have_posts()) : the_post(); ?>
						
						<?php get_template_part( 'content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) ); ?>
					
					<?php $postindex++; endwhile; ?>
					<?php else: ?>
					<?php get_template_part('content','none' ); ?>
				<?php endif; ?> 
				
				
				<?php meminz_pagination();?>

			</div>
			<?php if($theme_options['blog_layout']==='right_sidebar'):?>
			<div class="col-md-3">
				<aside>
					<?php get_sidebar( );?>
				</aside>
			</div>
			<?php endif;?>
		</div>
	</div>
</div>

<!-- end inner page -->
<?php get_footer(); ?>
 