<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

get_header();

global $theme_options;
?>

<!-- Start page header -->
<div class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php the_breadcrumb(); ?>
			</div>				
		</div>	
	</div>		
</div>
<!-- End page header -->
<?php if($theme_options['author_infor']==='1') :?>
<!-- Start page heading -->
<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
					<div class="media">
				        <div class="author-avatar pull-left">
					        <a href="<?php echo esc_url(get_the_author_meta('user_url' ) ); ?>"><?php echo get_avatar(get_the_author_meta('user_email'),$size='80',$default='http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=80' ); ?>  </a>
					    </div>
					    <div class="media-body author-info">
					        <div class="media-heading"><h6><a href="<?php echo esc_url(get_the_author_meta('user_url' ));?>"><?php echo get_the_author_meta('display_name');?></a></h6></div>
					        <p><?php echo get_the_author_meta('description');?></p>
					        
					    </div>
					    <ul class="pull-right list-inline author-social">
					    <?php if(get_user_meta(get_the_author_meta('ID'), '_cmb_twitterurl' ,true)!=''){ ?>
					    	<li><a title="<?php _e('Follow on Twitter','meminz');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cmb_twitterurl' ,true)); ?>"><i class="fa fa-twitter"></i></a></li>
					    <?php } ?>
					    <?php if(get_user_meta(get_the_author_meta('ID'), '_cmb_facebookurl' ,true)!=''){ ?>
					    	<li><a title="<?php _e('Like on Facebook','meminz');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cmb_facebookurl' ,true)); ?>"><i class="fa fa-facebook"></i></a></li>
					    <?php } ?>
					    <?php if(get_user_meta(get_the_author_meta('ID'), '_cmb_googleplusurl' ,true)!=''){ ?>
					    	<li><a title="<?php _e('Circle on Google Plus','meminz');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cmb_googleplusurl' ,true)) ;?>"><i class="fa fa-google-plus"></i></a></li>
					    <?php } ?>
					    <?php if(get_user_meta(get_the_author_meta('ID'), '_cmb_linkedinurl' ,true)!=''){ ?>
					    	<li><a title="<?php _e('Be Friend on Linkedin','meminz');?>" href="<?php echo esc_url(get_user_meta(get_the_author_meta('ID'), '_cmb_linkedinurl' ,true) ); ?>"><i class="fa fa-linkedin"></i></a></li>
					    <?php } ?>
					    </ul>			
					</div>
				
			</div>
		</div>
	</div>
</section>
<?php endif;?>
<!-- End page heading -->
<div id="inner-page">
	<div class="container">
		<div class="row">
			<?php if($theme_options['blog_layout']==='left_sidebar' || get_post_meta(get_the_ID(), '_cmb_single_layout', true)==="left_sidebar"):?>
				<div class="col-md-3 sidebar-left">
					<aside>
						<?php get_sidebar( );?>
					</aside>
				</div>
			<?php endif;?>
			<?php if($theme_options['blog_layout']==='fullwidth' || get_post_meta(get_the_ID(), '_cmb_single_layout', true)==="fullwidth"):?>
			<div class="col-md-12">
			<?php else:?>
			<div class="col-md-9">
			<?php endif;?>

				<?php if(have_posts()) : 
					$postindex = 1;
					?>
					<?php while(have_posts()) : the_post(); ?>
						
						<?php get_template_part( 'content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) ); ?>
					
					<?php $postindex++; endwhile; ?>
					<?php else: ?>
					<?php get_template_part('content','none' ); ?>
				<?php endif; ?> 
				
				
				<?php meminz_pagination();?>

			</div>
			<?php if($theme_options['blog_layout']==='right_sidebar'):?>
			<div class="col-md-3">
				<aside>
					<?php get_sidebar( );?>
				</aside>
			</div>
			<?php endif;?>
		</div>
	</div>
</div>
<?php get_footer(); ?>