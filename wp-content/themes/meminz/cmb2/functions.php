<?php

add_action( 'cmb2_admin_init', 'meminz_cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function meminz_cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cmb_';

        /**
     * Initiate Post metabox
     */
    $post_cmb = new_cmb2_box( array(
        'id'            => 'post_options',
        'title'         => esc_html__( 'Post Format Options', 'meminz' ),
        'object_types'  => array( 'post'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    $post_cmb->add_field( array(
        'name'       => esc_html__('oEmbed for Post Format', 'meminz' ),
        'desc'       => wp_kses(__('Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>. Or audio file for Audio post format.', 'meminz' ),array('a'=>array('href'=>array()))),
        'id'   => $prefix . 'embed_video',
        'type' => 'oembed',
    ) );

    $post_cmb->add_field( array(
        'name' => esc_html__('Post Subtitle show in header section', 'meminz' ),
        'id'   => $prefix . 'post_subtitle',
        'type' => 'textarea_small'
    ) );

    $post_cmb->add_field( array(
        'name' => esc_html__('Single Layout', 'meminz' ),
        'desc' => esc_html__('Choose display layout for this post','meminz'),
        'id'   => $prefix . 'single_layout',
        'type'    => 'select',
        'default'=>'right_sidebar',
        'options' => array(
            'fullwidth' => esc_html__( 'Fullwidth', 'meminz' ),
            'right_sidebar' => esc_html__( 'Right Sidebar', 'meminz' ),
            'left_sidebar' => esc_html__( 'Left Sidebar', 'meminz' ),
        ),
    ) );



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Initiate Portfolio metabox
     */
    $folio_cmb = new_cmb2_box( array(
        'id'            => 'porfolio_list_fields',
        'title'         => esc_html__('Portfolio Options', 'meminz' ),
        'object_types'  => array( 'portfolio'), // Post type
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'high',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__( 'Layout', 'meminz' ),
        'desc' => esc_html__( 'Select one of the prestyled layout for this protfolio', 'meminz' ),
        'id' => $prefix . 'portfolio_layout',
        'type'             => 'select',
        'default'          => 'layout1',
        'options' => array(
            'layout1' => esc_html__( 'Layout 1', 'meminz' ),
            'layout2' => esc_html__( 'Layout 2', 'meminz' ),
            'layout3' => esc_html__( 'Layout 3', 'meminz' ),
        ),
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Project Backgroud 1', 'meminz' ),
        'desc' => esc_html__('Choose image (using for layout 1, layout 2)', 'meminz' ),
        'id'   => $prefix . 'bg1',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => true, // Hide the text input for the url
            
        ),
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Project Title 1', 'meminz' ),
        'name' => esc_html__('Project title show in header section 1 (using for layout 1, layout 2)', 'meminz' ),
        'id'   => $prefix . 'project_title1',
        'type' => 'text'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Project Description 1', 'meminz' ),
        'name' => esc_html__('Project Description show in header section 1 (using for layout 1, layout 2)', 'meminz' ),
        'id'   => $prefix . 'project_description1',
        'type' => 'textarea_small'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Custom Field 1', 'meminz' ),
        'name' => esc_html__('Custom field of project 1 (using for layout 1, layout 2, layout 3)', 'meminz' ),
        'id'   => $prefix . 'custom_field1',
        'type' => 'text'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Visit 1', 'meminz' ),
        'name' => esc_html__('Insert link to project(using for layout 1, layout 2)', 'meminz' ),
        'id'   => $prefix . 'visit1',
        'type' => 'text'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Project Backgroud 2', 'meminz' ),
        'desc' => esc_html__('Choose image (using for layout 1, layout 2)', 'meminz' ),
        'id'   => $prefix . 'bg2',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => true, // Hide the text input for the url
            
        ),
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Project Title 2', 'meminz' ),
        'name' => esc_html__('Project title show in header section 1 (using for layout 1, layout 2)', 'meminz' ),
        'id'   => $prefix . 'project_title2',
        'type' => 'text'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Project Description 2', 'meminz' ),
        'name' => esc_html__('Project Description show in header section 1 (using for layout 1, layout 2)', 'meminz' ),
        'id'   => $prefix . 'project_description2',
        'type' => 'textarea_small'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Custom Field 2', 'meminz' ),
        'name' => esc_html__('Custom field of project 2 (using for layout 1)', 'meminz' ),
        'id'   => $prefix . 'custom_field2',
        'type' => 'text'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Visit 2', 'meminz' ),
        'name' => esc_html__('Insert link to project(using for layout 1)', 'meminz' ),
        'id'   => $prefix . 'visit2',
        'type' => 'text'
    ) );

    $folio_cmb->add_field( array(
        'name' => esc_html__('Project Backgroud 3', 'meminz' ),
        'desc' => esc_html__('Choose image (using for layout 3)', 'meminz' ),
        'id'   => $prefix . 'bg3',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => true, // Hide the text input for the url
            
        ),
    ) );


    /**
     * Initiate User Profile metabox
     */
    $user_cmb = new_cmb2_box( array(
        'id'         => 'user_edit',
        'title'      => esc_html__( 'User Profile Metabox', 'meminz' ),
        'object_types'  => array( 'user' ), // Tells CMB to use user_meta vs post_meta
        'context'       => 'normal',// normal, side and advanced
        'priority'      => 'core',// default, high and low - core
        'show_names'    => true, // Show field names on the left
    ) );

    

    

    $user_cmb->add_field( array(
        'name' => esc_html__( 'Facebook URL', 'meminz' ),
      
        'id'   => $prefix . 'facebookurl',
        'type' => 'text_url',
        
    ) );

    $user_cmb->add_field( array(
        'name' => esc_html__( 'Twitter URL', 'meminz' ),
        
        'id'   => $prefix . 'twitterurl',
        'type' => 'text_url',
        
    ) );
    $user_cmb->add_field( array(
        'name' => esc_html__( 'Google+ URL', 'meminz' ),
        
        'id'   => $prefix . 'googleplusurl',
        'type' => 'text_url',
        
    ) );
    $user_cmb->add_field( array(
       'name' => esc_html__( 'Linkedin URL', 'meminz' ),
        
        'id'   => $prefix . 'linkedinurl',
        'type' => 'text_url',
        
    ) );
    

}

 