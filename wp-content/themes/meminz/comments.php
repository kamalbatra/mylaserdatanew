<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

if ( post_password_required() )
    return;
?>
<?php if ( have_comments() || comments_open( ) ) : ?>
	<!-- Start comments-->
	<div class="comment-head" id="comments">
		<h4><?php _e('Leave a Comments','meminz');?></h4>
	</div>
	<div class="comment-wrapper">
		<div class="comment-main">
			<?php if(comments_open( )) : ?>
			<div class="media">
				<div class="media-body">
					<div class="row">
						<?php
							$aria_req = ( $req ? " aria-required='true'" : '' );
							$comment_args = array(
							'title_reply'=> __('&nbsp;','meminz'),
							'fields' => apply_filters( 'comment_form_default_fields', 

								array(
									  'author' => '<div class="col-md-6 mb-15">
											<label class="form-label" for="author">' . __( 'Name', 'meminz' ) . '</label> ' .
											( $req ? '<span class="not-empty">*</span>' : '' ) .
											'<input type="text" id="author" name="author" class="form-control form-block" placeholder="'.__('Enter your full name','meminz').'" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . '></div>',
										'email' =>'<div class="col-md-6 mb-15">
											<label for="email">' . __( 'Email', 'meminz' ) . '</label> ' .
											( $req ? '<span class="required">*</span>' : '' ) .
											'<input id="email" name="email" type="text" class="form-control form-block" placeholder="'.__('Enter your email address','meminz').'" value="' . esc_attr(  $commenter['comment_author_email'] ) .
											'" ' . $aria_req . ' /></div>',
									) 
								),
							'comment_field' => '<div class="col-md-12 mb-15"><textarea class="form-control input-block-level" placeholder="'.__('Message','meminz').'"  id="comment" rows="5" name="comment"  '.$aria_req.'></textarea></div>',
							'id_form'=>'form-horizontal',
							'id_submit' => 'meminz_submit',
							'label_submit' => __('ADD COMMENT','meminz'),
							'must_log_in'=> '<p class="not-empty" style="margin-left:15px;">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ,'meminz'), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
							'logged_in_as' => '<p class="not-empty" style="margin-left:15px;">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>','meminz' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
							'comment_notes_before' => '<h5 class="text-center">'.__('Your email is safe with us.','meminz').'</h5>',
							'comment_notes_after' => '',
							);
						
						?>
						<?php comment_form($comment_args); ?>
					</div>
				</div>
			</div>	

			<?php endif; ?>
			
			<?php if ( have_comments() ) : ?>
			<?php 

			$args = array(
				'walker'            => null,
				'max_depth'         => '',
				'style'             => 'div',
				'callback'          => 'meminz_theme_comment',
				'end-callback'      => null,
				'type'              => 'all',
				'reply_text'        => 'Reply',
				'page'              => '',
				'per_page'          => '',
				'avatar_size'       => 50,
				'reverse_top_level' => null,
				'reverse_children'  => '',
				'format'            => 'html5', //or xhtml if no HTML5 theme support
				'short_ping'        => false, // @since 3.6,
			    'echo'     			=> true, // boolean, default is true
			);
			?>

			<?php wp_list_comments($args);?>
			<!-- End Comment First level -->
			<?php endif; ?>
		</div>
	</div>
	<!-- End comments-->

	<!-- Comment First level -->

	<?php
	// Are there comments to navigate through?
	if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		?>
	<ul class="pager">
		<li class="previous"><?php previous_comments_link( __( '<i class="fa fa-arrow-left"></i> Previous', 'meminz' ) ); ?></li>
		<li class="next"><?php next_comments_link( __( 'Next <i class="fa fa-arrow-right"></i>', 'meminz' ) ); ?></li>
	</ul>
	<?php endif; // Check for comment navigation ?>

	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="no-comments" style="margin-left:15px;"><?php _e( 'Comments are closed.' , 'meminz' ); ?></p>
	<?php endif; ?>

<?php endif; ?>