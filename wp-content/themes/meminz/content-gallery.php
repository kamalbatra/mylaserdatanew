<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
 
global $theme_options;
?>
<article <?php post_class();?>>
	<div class="row">
		<?php if($gallery = get_post_gallery( get_the_ID(), false )){
			if(isset($gallery['ids'])) : ?>
			<div class="col-md-4">
				<div class="flexslider flex-nonav">
					<ul class="slides">
						<?php
							$gallery_ids = $gallery['ids'];
							$img_ids = explode(",",$gallery_ids);
							
							foreach( $img_ids AS $img_id ){
						?>
							<li><?php echo wp_get_attachment_image( $img_id, 'full', '', array('class'=>'img-responsive') );?></li>
						<?php } ?>
					</ul>
				</div>  
			</div>
			<?php endif; ?>
			<div class="col-md-8">
		<?php }else { ?>
			<div class="col-md-12">
		<?php } ?>
			<h4><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h4>
			<?php if($theme_options['author_checkbox']==='1' || $theme_options['date_checkbox']==='1' || $theme_options['comment_checkbox']==='1' || $theme_options['tag_checkbox']==='1'):?>
				<div class="meta-wrapper">
					<ul class="meta-post">
						<?php if($theme_options['author_checkbox']==='1') :?>
						<li><label><?php _e('Author: ','meminz');?></label> <?php the_author_posts_link( );?></li>
						<?php endif;?>  
						<?php if($theme_options['date_checkbox']==='1') :?>
						<li><label><?php _e('Date: ','meminz');?></label> <a href="<?php echo get_day_link((int)get_the_time('Y' ), (int)get_the_time('m' ), (int)get_the_time('d' )); ?>"><?php the_time(get_option('date_format'));?></a></li>
						<?php endif;?>
						<?php if($theme_options['cats_checkbox']==='1') :?>
							<li><label><?php _e('Categories: ','meminz');?></label> <?php echo get_the_category_list(', ');?></li>
						<?php endif;?>
						<?php if($theme_options['tag_checkbox']==='1') :?>
						<li><label><?php _e('Tags: ','meminz');?></label> <?php the_tags('');?></li>
						<?php endif;?>
						<?php if($theme_options['comment_checkbox']==='1') :?>
						<li><label><?php _e('Comment: ','meminz');?></label> <?php comments_popup_link(__('0', 'meminz'), __('1', 'meminz'), __('%', 'meminz')); ?></li>
						<?php endif;?>
					</ul>
				</div>
			<?php endif;?>
			<?php edit_post_link( __( 'Edit', 'meminz' ), '<span class="edit-link">', '</span>' ); ?>	
			<?php the_excerpt();?>
			<a href="<?php the_permalink(); ?>" class="btn btn-default btn-bordered"><?php _e('Read more','meminz');?></a>

			<?php
				wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'meminz' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				) );
			?>
		</div>								
	</div>
</article>
