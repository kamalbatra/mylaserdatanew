<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
 
//global $postindex;
global $theme_options;
?>
<article <?php post_class();?>>
	<div class="row">
	<?php if(has_post_thumbnail( )){ ?>
		<div class="col-md-4">
			<img src="<?php echo esc_url(meminz_thumbnail_url('full') );?>" class="img-responsive thumbnail" alt="<?php the_title( ); ?>"/>
		</div>
		<div class="col-md-8">
	<?php }else{ ?>
		<div class="col-md-12" >
	<?php } ?>
			<?php edit_post_link( __( 'Edit', 'meminz' ), '<span class="edit-link">', '</span>' ); ?>	
			<blockquote><?php the_excerpt();?></blockquote>
			<a href="<?php the_permalink(); ?>" class="btn btn-default btn-bordered"><?php _e('Read more','meminz');?></a>

			<?php
				wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'meminz' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				) );
			?>
		</div>								
	</div>
</article>
