<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

 global $theme_options ;

 ?>	

 	
	<!-- Start footer -->
	<footer>
	<?php if(is_active_sidebar('footer-1')||is_active_sidebar('footer-2')||is_active_sidebar('footer-3')||is_active_sidebar('footer-4')) :?>
		<div class="container">
			<div class="row">
				<?php 
				if(is_active_sidebar('footer-1')):?>
				<div class="col-md-4 col-sm-6">
					<?php dynamic_sidebar('footer-1');?>
				</div>
			        
			    <?php endif;?>
				<?php 
				if(is_active_sidebar('footer-2')):?>
				<div class="col-md-3 col-sm-6">
					<?php dynamic_sidebar('footer-2');?>
				</div>
			        
			    <?php endif;?>
				<?php 
				if(is_active_sidebar('footer-3')):?>
				<div class="col-md-2 col-sm-6">
					<?php dynamic_sidebar('footer-3');?>
				</div>
			        
			    <?php endif;?>
				<?php 
				if(is_active_sidebar('footer-4')):?>
				<div class="col-md-3 col-sm-6">
					<?php dynamic_sidebar('footer-4');?>
				</div>
			        
			    <?php endif;?>
			</div>
		</div>
		<hr>
	<?php endif;?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="social-network">
						<?php if(!empty($theme_options['facebook'])):?>
							<a href="<?php echo esc_url($theme_options['facebook'] ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
						<?php endif;?>
						<?php if(!empty($theme_options['twitter'])):?>
							<a href="<?php echo esc_url($theme_options['twitter'] ); ?>"  target="_blank"><i class="fa fa-twitter"></i></a>
						<?php endif;?>
						<?php if(!empty($theme_options['google-plus'])):?>
							<a href="<?php echo esc_url($theme_options['google-plus'] ); ?>"  target="_blank"><i class="fa fa-google-plus"></i></a>
						<?php endif;?>
						<?php if(!empty($theme_options['dribbble'])):?>
							<a href="<?php echo esc_url($theme_options['dribbble'] ); ?>"  target="_blank"><i class="fa fa-dribbble"></i></a>
						<?php endif;?>
						<?php if(!empty($theme_options['skype'])):?>
							<a href="<?php echo esc_url($theme_options['skype'] ); ?>"  target="_blank"><i class="fa fa-skype"></i></a>
						<?php endif;?>
						<?php if(!empty($theme_options['pinterest'])):?>
							<a href="<?php echo esc_url($theme_options['pinterest'] ); ?>"  target="_blank"><i class="fa fa-pinterest"></i></a>
						<?php endif;?>
						<?php if(!empty($theme_options['linkedin'])):?>
							<a href="<?php echo esc_url($theme_options['linkedin'] ); ?>"  target="_blank"><i class="fa fa-linkedin"></i></a>
						<?php endif;?>
					</div>
					<p><?php echo htmlspecialchars_decode($theme_options['footer-text']);?></p>
				</div>
			</div>
		</div>	
	</footer>
	<!-- End footer -->

    <?php wp_footer(); ?>
</body>
</html>