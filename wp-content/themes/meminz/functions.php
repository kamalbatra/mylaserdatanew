<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

if (!isset($theme_options) && file_exists(get_template_directory() . '/functions/admin-config.php')) {
    require_once (get_template_directory() . '/functions/admin-config.php');
}
function meminz_removeDemoModeLink() { // Be sure to rename this function to something more unique
    // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
        // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
    }
}


add_action('init', 'meminz_removeDemoModeLink');


if(!function_exists('meminz_content_width')){
    /**
     * Sets the content width in pixels, based on the theme's design and stylesheet.
     *
     * Priority 0 to make it available to lower priority callbacks.
     *
     * @global int $content_width
     *
     * @since Meminz 1.0
     */
    function meminz_content_width() {
        $GLOBALS['content_width'] = apply_filters( 'meminz_content_width', 847 );
    }
    add_action( 'after_setup_theme', 'meminz_content_width', 0 );

}

if (!function_exists('meminz_setup_theme')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @since Meminz 1.0
     */

    function meminz_setup_theme() {

        /* replace local link with this '.addcslashes( home_url('/' ) ,'/').' */
        /* echo default options string - var_dump(json_encode($bloom_options));die; */


        /*
         * Make theme available for translation.
         */
        load_theme_textdomain( 'meminz' , get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        add_image_size('meminz-imac-slider', 890, 494, true );
        add_image_size('meminz-imac-slider', 940, 511, true );


        // This theme uses wp_nav_menu() in one location.
        register_nav_menu( 'primary', __( 'Main Blog Navigation Menu', 'meminz' ) );
        register_nav_menu( 'landing-menu', __( 'Landing Page Navigation Menu', 'meminz' ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'audio',
        ) );
        
        add_theme_support('custom-header');

        add_theme_support('custom-background');
        
        // This theme uses its own gallery styles.
        
        //add_filter('use_default_gallery_style', '__return_false');

        add_editor_style(get_template_directory_uri().'/inc/assets/custom-editor-style.css');

    }
}
add_action('after_setup_theme', 'meminz_setup_theme');

/**
 * Register custom fonts.
 */
function meminz_fonts_url() {
    $fonts_url = '';
    $font_families     = array();

    
    if ( 'off' !== esc_html_x( 'on', 'Open Sans font: on or off', 'meminz' ) ) {
        $font_families[] = 'Open Sans:400,400i,700,700i';
    }


    if ( $font_families ) {
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin-ext,vietnamese' ),
        );

        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }

    return esc_url_raw( $fonts_url );

}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Inshot 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function meminz_resource_hints( $urls, $relation_type ) {
    if ( wp_style_is( 'meminz-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
        $urls[] = array(
            'href' => 'https://fonts.gstatic.com',
            'crossorigin',
        );
    }

    return $urls;
}
add_filter( 'wp_resource_hints', 'meminz_resource_hints', 10, 2 );

/* Register Sidebars */
function meminz_register_sidebars(){

    register_sidebar( array(
        'name'          => __( 'Main Sidebar', 'meminz' ),
        'id'            => 'sidebar-1',        
        'description'   => __( 'Appears in the sidebar section of the site.', 'meminz' ),        
        'before_widget' => '<div id="%1$s" class="widget %2$s">',        
        'after_widget'  => '</div>',        
        'before_title'  => '<h4 class="head">',        
        'after_title'   => '<span></span></h4>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Page Sidebar', 'meminz' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Appears in the sidebar section of the page template.', 'meminz' ),
        'before_widget' => '<div id="%1$s" class="widget cth %2$s">',        
        'after_widget'  => '</div>',        
        'before_title'  => '<h5>',        
        'after_title'   => '</h5>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Shopping Cart', 'meminz' ),
        'id'            => 'shopping_cart',
        'description'   => __( 'Appears in Footer 4.', 'meminz' ),
        'before_widget' => '<div id="%1$s" class="options_box %2$s">',        
        'after_widget'  => '</div>',        
        'before_title'  => '<h6><span>',        
        'after_title'   => '</span></h6>',
    ) );

    register_sidebar( array(

        'name'          => __( 'Footer Widget Area 1', 'meminz' ),
        'id'            => 'footer-1',
        'description'   => __( 'Appears in Footer 1.', 'meminz' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',        
        'after_widget'  => '</div>',        
        'before_title'  => '<h4><small class="white">',        
        'after_title'   => '</small></h4>',
    ) );
    register_sidebar( array(

        'name'          => __( 'Footer Widget Area 2', 'meminz' ),
        'id'            => 'footer-2',
        'description'   => __( 'Appears in Footer 2.', 'meminz' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',        
        'after_widget'  => '</div>',        
        'before_title'  => '<h4><small>',        
        'after_title'   => '</small></h4>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer Widget Area 3', 'meminz' ),
        'id'            => 'footer-3',
        'description'   => __( 'Appears in Footer 3.', 'meminz' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',        
        'after_widget'  => '</div>',        
        'before_title'  => '<h4><small>',        
        'after_title'   => '</small></h4>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Widget Area 4', 'meminz' ),
        'id'            => 'footer-4',
        'description'   => __( 'Appears in Footer 4.', 'meminz' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',        
        'after_widget'  => '</div>',        
        'before_title'  => '<h4><small>',        
        'after_title'   => '</small></h4>',
    ) );

}

add_action('widgets_init','meminz_register_sidebars' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Meminz 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function meminz_excerpt_more( $link ) {
    
    return ' &hellip; ';
}
add_filter( 'excerpt_more', 'meminz_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Meminz 1.0
 */
function meminz_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'meminz_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function meminz_pingback_header() {
    if ( is_singular() && pings_open() ) {
        printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
    }
}
add_action( 'wp_head', 'meminz_pingback_header' );


//For IE
function meminz_script_ie() {
    echo 
    '<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->';    
}
//add_action( 'wp_head', 'meminz_script_ie' );


if (!function_exists('meminz_scripts_styles')) {
    function meminz_scripts_styles() {
    	global $theme_options;
    	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ){
            wp_enqueue_script( 'comment-reply' );
        }
        wp_enqueue_script("meminz-plugins-js", get_template_directory_uri()."/js/plugins.js",array('jquery'),null,true);
        if($theme_options['show_loader']){
            wp_enqueue_script("loader-js", get_template_directory_uri()."/js/loader.js",array(),null,true);
        }
        wp_enqueue_script("meminz-cripts", get_template_directory_uri()."/js/custom.js",array(),null,true);

        $meminz_obj = array();
        $meminz_obj['disable_animation'] = $theme_options['disable_animation'];
        $meminz_obj['disable_mobile_animation'] = $theme_options['disable_mobile_animation'];
        wp_localize_script('meminz-cripts', 'meminz_obj', $meminz_obj);

        if($theme_options['user_login_type']==='ajax' || $theme_options['user_singup_type']==='ajax' ){
            //Login Ajax script 
            // localize wp-ajax, notice the path to our theme-ajax.js file
            wp_enqueue_script( 'theme-ajax', get_template_directory_uri() . '/js/theme-ajax.js', array(),null,true );
            wp_localize_script( 'theme-ajax', 'theme_ajax', array(
                'url'        => admin_url( 'admin-ajax.php' ),
                'site_url'     => home_url('/' ),
                'theme_url' => get_template_directory_uri()
            ) );
        }

        // wp_enqueue_script("demo-color-picker", get_template_directory_uri()."/js/theme-option/colorpicker.js",array(),null,true);
        // wp_enqueue_script("demo-option-panel", get_template_directory_uri()."/js/theme-option/optionspanel.js",array(),null,true);

        wp_enqueue_style( 'meminz-fonts', meminz_fonts_url(), array() , null);
    	wp_enqueue_style( 'meminz-plugins-css', get_template_directory_uri().'/css/plugins.css', array() , null);
        wp_enqueue_style( 'meminz-style', get_stylesheet_uri(), array() , null );
        wp_enqueue_style( 'custom-style', get_template_directory_uri().'/css/custom.css', array() , null);
        if($theme_options['override-preset'] === 'yes'){
            wp_enqueue_style( 'skin', get_template_directory_uri().'/skins/overridestyle.css', array() , null);
        }elseif($theme_options['color-preset']){
            wp_enqueue_style( 'skin', get_template_directory_uri().'/skins/'.$theme_options['color-preset'].'/skin.css', array() , null);
        }
        if($theme_options['custom-css'] !== ''){
            wp_add_inline_style( 'custom-style', $theme_options['custom-css'] );
        }
        // wp_enqueue_style( 'color-picker-css', get_template_directory_uri().'/css/colorpicker.css');
    }
}
add_action( 'wp_enqueue_scripts', 'meminz_scripts_styles' );


/**
 * Enqueue admin scripts and styles.
 *
 * @since Meminz 2.0
 */

if (!function_exists('meminz_enqueue_admin_scripts')) {
    function meminz_enqueue_admin_scripts() {
        wp_enqueue_style('meminz-admin', get_template_directory_uri() . '/inc/assets/admin_styles.css');
    }
}
add_action('admin_enqueue_scripts', 'meminz_enqueue_admin_scripts');




//My Bread Crumb
function the_breadcrumb() {
        echo '<ul class="breadcrumb">';
        if (!is_home()) {
                echo '<li><a href="';
                echo home_url('/' );
                echo '">';
                echo '<i class="pe-7s-home">';
                echo "</i></a></li>";
                if (is_category() || is_single()) {
                        echo '<li>';
                        the_category(' </li><li> ');
                        if (is_single()) {
                                echo "</li><li>";
                                the_title();
                                echo '</li>';
                        }
                } elseif (is_page()) {
                        echo '<li>';
                        echo the_title();
                        echo '</li>';
                }elseif (is_author()) {
                    echo '<li>';
                    echo __("Articles Posted by ",'meminz');
                    the_author();
                    echo '</li>';
                }elseif (is_search()) {
                    echo "<li>".__("Search Results for ",'meminz');
                    the_search_query();
                    echo'</li>';
                }elseif (is_tag()) {
                    echo '<li>';
                    echo __("Posts Tagged ",'meminz');
                    single_tag_title();
                    echo '</li>';
                }elseif (is_day()) {
                    echo"<li>".__('Archive for ','meminz');
                     
                     the_time(get_option('date_format'));
                     echo'</li>';
                 }elseif (is_404()) {
                    echo "<li>".__('Error 404','meminz');
                     echo '</li>';
                 }

        }else{
            echo '<li><a href="';
            echo home_url('/' );
            echo '">';
            echo '<i class="pe-7s-home">';
            echo "</i></a></li>";
            echo"<li>".__('Our Thoughts','meminz');
            echo'</li>';
        }
        echo '</ul>';
}




// //pagination
function meminz_pagination($prev = 'Prev', $next = 'Next', $pages='') {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }
    $pagination = array(
		'base' 			=> str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
		'format' 		=> '',
		'current' 		=> max( 1, get_query_var('paged') ),
		'total' 		=> $pages,
		'prev_text'     => $prev,
        'next_text'     => $next,		
        'type'			=> 'list',
		'end_size'		=> 3,
		'mid_size'		=> 3
);
    $return =  paginate_links( $pagination );
	echo '<div class="text-center">'.str_replace( "<ul class='page-numbers'>", '<ul class="pagination">', $return ).'</div>';
}


//Get thumbnail url
function meminz_thumbnail_url($size){
    global $post;
    //$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()),$size );
    if($size==''){
        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
         return $url;
    }else{
        $url = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $size);
         return $url[0];
    }
}

function meminz_post_nav() {
    global $post;
    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );
    if ( ! $next && ! $previous )
        return;
    ?>
    <ul class="pager clearfix">
      <li class="previous">
        <?php previous_post_link( '%link', _x( ' &larr; Prev', 'Previous post link', 'meminz' ) ); ?>
      </li>
      <li class="next">
        <?php next_post_link( '%link', _x( 'Next &rarr;', 'Next post link', 'meminz' ) ); ?>
      </li>
    </ul>   
<?php
}

function meminz_search_form( $form ) {
    $form = '
		<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
		<div class="search">
		<input type="text" size="16" class="search-field form-control" placeholder="'.__('Search ...','meminz').'" value="' . get_search_query() . '" name="s" id="s" />
        <input type="hidden" name="post_type" value="post">
		</div>
		</form>
	';
  return $form;
}

//Custom comment List:
function meminz_theme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP); //echo '<pre>';var_dump($comment);die;

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>
    <<?php echo esc_attr($tag ); ?> <?php comment_class( empty( $args['has_children'] ) ? 'media' : 'media parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <!-- <div id="div-comment-<?php comment_ID() ?>" class="comment-body clearfix"> -->
    <div id="div-comment-<?php comment_ID() ?>" <?php comment_class('media' );?>>
    <?php endif; ?>

        <!-- Start Comment -->
        <!-- <div class="media">  --> 
            <a class="pull-left" href="<?php echo esc_url(get_the_author_meta('user_url' ) ); ?>"><?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?></a>
            <div class="media-body">
                <h4 class="media-heading"><a href=""><?php echo get_comment_author_link($comment->comment_ID); ?></a></h4>
                <p><?php comment_text(); ?></p>
                <p>
                    <?php _e('','meminz');?>
                    <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                </p>
            </div>
        <!-- </div> -->
        <!-- End Comment -->

        <?php if ( $comment->comment_approved == '0' ) : ?>
            <em class="comment-awaiting-moderation aligncenter"><?php _e( 'Your comment is awaiting moderation.','meminz'); ?></em>
            <br />
        <?php endif; ?>
    
    
    
    <?php if ( 'div' != $args['style'] ) : ?>
    </div> 
    <?php endif; ?>
<?php
}


/**
 * Visual Composer plugin integration
 *
 * @since Meminz 1.0
 */
require_once get_template_directory() . '/inc/cth_for_vc.php';

/**
 * Theme custom style
 *
 * @since Meminz 1.0
 */
// require_once get_template_directory() . '/inc/overridestyle.php';

/**
 * Taxonomy meta box
 *
 * @since Meminz 1.0
 */
//require_once get_template_directory() . '/inc/taxonomy_metabox_fields.php';

/**
 * Custom elements for VC
 *
 * @since Meminz 1.0
 */
require_once get_template_directory() . '/vc_extend/vc_shortcodes.php';

require_once get_template_directory() . '/framework/wp_bootstrap_navwalker.php';





/** 
 * Global variables fix
 * https://forums.envato.com/t/redux-framework-global-variable-issue/36739
 * @since Meminz 1.0
 */ 
if ( !function_exists('meminz_global_var') ) {
    function meminz_global_var($opt_1 = '', $opt_2 = '', $opt_check = false){
        global $theme_options;
        if( $opt_check ) {
            if(isset($theme_options[$opt_1][$opt_2])) {
                return $theme_options[$opt_1][$opt_2];
            }
        } else {
            if(isset($theme_options[$opt_1])) {
                return $theme_options[$opt_1];
            }
        }

        return false;
        
    }
}
if(!function_exists('meminz_get_template_part')){
    /**
     * Load a template part into a template
     *
     * Makes it easy for a theme to reuse sections of code in a easy to overload way
     * for child themes.
     *
     * Includes the named template part for a theme or if a name is specified then a
     * specialised part will be included. If the theme contains no {slug}.php file
     * then no template will be included.
     *
     * The template is included using require, not require_once, so you may include the
     * same template part multiple times.
     *
     * For the $name parameter, if the file is called "{slug}-special.php" then specify
     * "special".
      * For the var parameter, simple create an array of variables you want to access in the template
     * and then access them e.g. 
     * 
     * array("var1=>"Something","var2"=>"Another One","var3"=>"heres a third";
     * 
     * becomes
     * 
     * $var1, $var2, $var3 within the template file.
     *
     * @since 3.0.0
     *
     * @param string $slug The slug name for the generic template.
     * @param string $name The name of the specialised template.
     * @param array $vars The list of variables to carry over to the template
     * @author CTHthemes 
     * @ref http://www.zmastaa.com/2015/02/06/php-2/wordpress-passing-variables-get_template_part
     * @ref http://keithdevon.com/passing-variables-to-get_template_part-in-wordpress/
     */
    function meminz_get_template_part( $slug, $name = null, $vars = null ) {

        $template = "{$slug}.php";
        $name = (string) $name;
        if ( '' !== $name && ( file_exists( get_stylesheet_directory() ."/{$slug}-{$name}.php") || file_exists( get_template_directory() ."/{$slug}-{$name}.php") ) ) {
            $template = "{$slug}-{$name}.php";
        }

        if(isset($vars)) extract($vars);
        include(locate_template($template));
    }
}

require_once get_template_directory() . '/inc/ajax.php';
/**
 * Custom meta box for page, post, portfolio...
 *
 * @since Meminz 1.0
 */
require_once get_template_directory() . '/cmb2/functions.php';



/**
 * Implement the One Click Demo Import plugin
 *
 * @since Meminz 1.0
 */
require_once get_parent_theme_file_path( '/includes/one-click-import-data.php' );



/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_parent_theme_file_path( '/framework/class-tgm-plugin-activation.php' );

add_action('tgmpa_register', 'meminz_register_required_plugins');

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function meminz_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        // This is an example of how to include a plugin from a private repo in your theme.
        array(
            'name' => esc_html__('WPBakery Page Builder','meminz'),
             // The plugin name.
            'slug' => 'js_composer',
             // The plugin slug (typically the folder name).
            'source' => 'http://assets.cththemes.net/js_composer/js_composer_outdoor_latest.zip',
             // The plugin source.
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
        ), 

        

        array(
            'name' => esc_html__('Redux Framework','meminz'),
             // The plugin name.
            'slug' => 'redux-framework',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/redux-framework/' ),
             // If set, overrides default API URL and points to an external URL.
        ), 

        array(
            'name' => esc_html__('Contact Form 7','meminz'),
             // The plugin name.
            'slug' => 'contact-form-7',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/contact-form-7/' ),
             // If set, overrides default API URL and points to an external URL.
        ), 

        array('name' => esc_html__('Easy Digital Downloads','meminz'),
             // The plugin name.
            'slug' => 'easy-digital-downloads',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/easy-digital-downloads/'),
             // If set, overrides default API URL and points to an external URL.
        ),

        array('name' => esc_html__('WP Tab Widget','meminz'),
             // The plugin name.
            'slug' => 'wp-tab-widget',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/wp-tab-widget/'),
             // If set, overrides default API URL and points to an external URL.
        ),

        array(
            'name' => esc_html__('CMB2','meminz'),
             // The plugin name.
            'slug' => 'cmb2',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/support/plugin/cmb2'),
             // If set, overrides default API URL and points to an external URL.
        ),
        array(
            'name' => esc_html__('Meminz Add-ons','meminz' ),
             // The plugin name.
            'slug' => 'meminz_add_ons',
             // The plugin slug (typically the folder name).
            'source' => 'meminz_add_ons.zip',
             // The plugin source.
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
        ), 

        
        array(
            'name' => esc_html__('Envato Market','meminz' ),
             // The plugin name.
            'slug' => 'envato-market',
             // The plugin slug (typically the folder name).
            'source' => esc_url('http://envato.github.io/wp-envato-market/dist/envato-market.zip' ),
             // The plugin source.
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('http://envato.github.io/wp-envato-market/' ),
             // If set, overrides default API URL and points to an external URL.
        ),

        array('name' => esc_html__('One Click Demo Import','meminz'),
             // The plugin name.
            'slug' => 'one-click-demo-import',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/one-click-demo-import/'),
             // If set, overrides default API URL and points to an external URL.
        ),

        

    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id'           => 'meminz',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => get_template_directory() . '/framework/plugins/',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.

        
    );

    tgmpa( $plugins, $config );
}

