<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        $theme_default_options = '{"last_tab":"1","favicon":{"url":"'.addcslashes( home_url('/' ) ,'/').'wp-content\/themes\/meminz\/img\/favicon.png","id":"","height":"","width":"","thumbnail":""},"logo":{"url":"'.addcslashes( home_url('/' ) ,'/').'wp-content\/themes\/meminz\/img\/logo.png","id":"","height":"","width":"","thumbnail":""},"logo_size_width":"124","logo_size_height":"31","logo_text":"","slogan":"","disable_animation":"0","disable_mobile_animation":"","show_loader":"1","show_cart":"yes","footer-text":"Copyright \u00a9 2016\u00a0<a class=\"copyrights\" href=\"http:\/\/themeforest.net\/user\/cththemes\">Cththemes<\/a>. All Rights Reserved.","facebook":"https:\/\/www.facebook.com\/","twitter":"https:\/\/twitter.com\/","google-plus":"https:\/\/plus.google.com","dribbble":"https:\/\/dribbble.com","skype":"https:\/\/skype.com","pinterest":"https:\/\/pinterest.com","linkedin":"https:\/\/www.linkedin.com\/","color-preset":"default","override-preset":"no","text-color":"#f55e25","background-color":"#f55e25","border-color":"#f55e25","body-font":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","font-size":"","line-height":"","color":""},"header-font":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","text-align":"","font-size":"","line-height":"","color":""},"blog_layout":"right_sidebar","blog_excerpt":"40","author_checkbox":"1","date_checkbox":"1","cats_checkbox":"0","tag_checkbox":"1","comment_checkbox":"1","author_infor":"1","user_login_type":"ajax","user_singup_type":"ajax","custom_login_link":"","custom_signup_link":"","show_logreg_md":"yes","404_intro":"Sorry, but the page you are looking for has not been found.","custom-css":"","REDUX_last_saved":1452441506,"REDUX_LAST_SAVE":1452441506,"REDUX_COMPILER":1452439697,"REDUX_LAST_COMPILER":1452439697}';
        $theme_options = json_decode($theme_default_options,true);
        return;
    }

    

    // This is your option name where all the Redux data is stored.
    $opt_name = "theme_options";

    // This line is only for altering the demo. Can be easily removed.
    //$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Meminz Options', 'meminz' ),
        'page_title'           => esc_html__( 'Meminz Options', 'meminz' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => 'AIzaSyBAycicE1b8x_pLv31OaST3vhIiCxW61kY',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => false,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'meminz' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'meminz' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'meminz' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => esc_html__('Visit us on GitHub', 'meminz' ),
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => esc_html__('Like us on Facebook', 'meminz' ),
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => esc_html__('Follow us on Twitter', 'meminz' ),
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => esc_html__('Find us on LinkedIn', 'meminz' ),
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( wp_kses(__( '<p></p>', 'meminz' ),array('p'=>array(),'strong'=>array()) ) , $v );
    } else {
        $args['intro_text'] =  wp_kses(__( '<p></p>', 'meminz' ),array('p'=>array(),'strong'=>array()) );
    }

    // Add content after the form.
    
    $args['footer_text'] = '<p>'.esc_html__('Thanks all of you who stay with us, your co-operation is our inspiration.','meminz' ).' <a href="'.esc_url('http://themeforest.net/user/cththemes/portfolio/' ).'" target="_blank">CTHthemes</a></p>';

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */


    // Set the help sidebar
    $content = wp_kses(__( '<p>This is the sidebar content, HTML is allowed.</p>', 'meminz' ),array('p'=>array('class'=>array())) );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */


    Redux::setSection( $opt_name, array(
        'title' => __('General Settings', 'meminz'),
        'id'         => 'general-settings',
        'subsection' => false,
        //'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/checkbox/" target="_blank">http://docs.reduxframework.com/core/fields/checkbox/</a>',
        'icon'       => 'el-icon-cogs',
        'fields' => array(
            array(
                'id' => 'favicon',
                'type' => 'media',
                'url' => true,
                'title' => __('Custom Favicon', 'meminz'),
                //'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => __('Upload your Favicon.', 'meminz'),
                'default' => array('url' => get_template_directory_uri().'/img/favicon.png'),
            ),
            array(
                'id' => 'logo',
                'type' => 'media',
                'url' => true,
                'title' => __('Logo', 'meminz'),
                //'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => __('Upload your logo.', 'meminz'),
                'default' => array('url' => get_template_directory_uri().'/img/logo.png'),
            ),
            array(
                'id' => 'logo_size_width',
                'type' => 'text',
                'title' => esc_html__('Logo Size Width', 'meminz'),
                'default' => '124'
            ),
            array(
                'id' => 'logo_size_height',
                'type' => 'text',
                'title' => esc_html__('Logo Size Height', 'meminz'),
                'default' => '31'
            ),
            array(
                'id' => 'logo_text',
                'type' => 'text',
                'title' => esc_html__('Logo Text', 'meminz'),
                'default' => ''
            ),
            array(
                'id' => 'slogan',
                'type' => 'text',
                'title' => esc_html__('Slogan (Sub Logo Text)', 'meminz'),
                'default' => ''
            ),
            array(
                'id' => 'disable_animation',
                'type' => 'switch',
                'title' => esc_html__('Disable Animation', 'meminz'),
                // 'subtitle' => esc_html__('Disable Animation', 'meminz'),
                //'options' => array('no' => 'No', 'yes' => 'Yes'), //Must provide key => value pairs for select options
                'default' => false
            ),
            array(
                'id' => 'disable_mobile_animation',
                'type' => 'switch',
                'title' => esc_html__('Disable Animation on Mobile', 'meminz'),
                // 'subtitle' => esc_html__('Disable Animation', 'meminz'),
                //'options' => array('no' => 'No', 'yes' => 'Yes'), //Must provide key => value pairs for select options
                'default' => false
            ),

            array(
                'id' => 'show_loader',
                'type' => 'switch',
                'title' => __('Show animation loadder', 'meminz'),
                'subtitle' => __('Show animation loader', 'meminz'),
                // 'options' => array('no' => 'No', 'yes' => 'Yes'), //Must provide key => value pairs for select options
                'default' => false
            ),
            array(
                'id' => 'show_cart',
                'type' => 'switch',
                'title' => __('Show shopping cart', 'meminz'),
                'subtitle' => __('Show shopping cart widget', 'meminz'),
                // 'options' => array('no' => 'No', 'yes' => 'Yes'), //Must provide key => value pairs for select options
                'default' => false
            ),
            // array(
            //     'id' => 'seo_des',
            //     'type' => 'textarea',
            //     'title' => __('SEO Description', 'meminz'),
            //     'subtitle' => __('', 'meminz'),
            //     'desc' => __('', 'meminz'),
            //     'default' => ''
            // ),
            // array(
            //     'id' => 'seo_key',
            //     'type' => 'textarea',
            //     'title' => __('SEO Keywords', 'meminz'),
            //     'subtitle' => __('', 'meminz'),
            //     'desc' => __('', 'meminz'),
            //     'default' => ''
            // ),
            array(
                'id' => 'footer-text',
                'type' => 'editor',
                'title' => __('Footer Text', 'meminz'),
                'subtitle' => __('Copyright Text', 'meminz'),
                'default' => 'Copyright &copy; 2016 <a class="copyrights" href="http://themeforest.net/user/cththemes">Cththemes</a>. All Rights Reserved.',
            ),
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __('Social Settings', 'meminz'),
        'id'         => 'social-settings',
        'subsection' => false,
        //'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/checkbox/" target="_blank">http://docs.reduxframework.com/core/fields/checkbox/</a>',
        'icon'       => 'el-icon-share',
        'fields' => array(
            array(
                'id' => 'facebook',
                'type' => 'text',
                'title' => __('Facebook Url', 'meminz'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'https://www.facebook.com/',
            ),
            array(
                'id' => 'twitter',
                'type' => 'text',
                'title' => __('Twitter Url', 'meminz'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'https://twitter.com/',
            ),
            array(
                'id' => 'google-plus',
                'type' => 'text',
                'title' => __('Google+ Url', 'meminz'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'https://plus.google.com',
            ),
            array(
                'id' => 'dribbble',
                'type' => 'text',
                'title' => __('Dribbble Url', 'meminz'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'https://dribbble.com',
            ),
            array(
                'id' => 'skype',
                'type' => 'text',
                'title' => __('Skype Url', 'meminz'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'https://skype.com',
            ),
            array(
                'id' => 'pinterest',
                'type' => 'text',
                'title' => __('Pinterest Url', 'meminz'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'https://pinterest.com',
            ),
            array(
                'id' => 'linkedin',
                'type' => 'text',
                'title' => __('Linkedin Url', 'meminz'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'https://www.linkedin.com/',
            ),
            // array(
            //     'id' => 'github',
            //     'type' => 'text',
            //     'title' => __('GitHub Url', 'meminz'),
            //     //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            //     'default' => 'https://github.com/',
            // ),
            // array(
            //     'id' => 'email',
            //     'type' => 'text',
            //     'title' => __('Email', 'meminz'),
            //     //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            //     'default' => 'email@yoursite.com',
            // ),
            // array(
            //     'id' => 'rss',
            //     'type' => 'text',
            //     'title' => __('RSS Url', 'meminz'),
            //     //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            //     'default' => '#'
            // ),
        ),
    ) );

    

    Redux::setSection( $opt_name, array(
        'title' => __('Styling Options', 'meminz'),
        'id'         => 'styling-settings',
        'subsection' => false,
        //'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/checkbox/" target="_blank">http://docs.reduxframework.com/core/fields/checkbox/</a>',
        'icon'       => 'el-icon-magic',
        'fields' => array(

            array(
                'id'       => 'color-preset',
                'type'     => 'image_select',
                'title'    => __( 'Theme Color', 'meminz' ),
                'subtitle' => __( 'Select your theme color', 'meminz' ),
                'desc'     => __( 'Select your theme color', 'meminz' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    'default' => array(
                        'alt' => 'Default',
                        'img' => get_template_directory_uri(). '/functions/assets/default.png'
                    ),
                    'skin2' => array(
                        'alt' => 'Skin 2',
                        'img' => get_template_directory_uri(). '/functions/assets/skin2.png'
                    ),
                    'skin3' => array(
                        'alt' => 'Skin 3',
                        'img' => get_template_directory_uri(). '/functions/assets/skin3.png'
                    ),
                    'skin4' => array(
                        'alt' => 'Skin 4',
                        'img' => get_template_directory_uri(). '/functions/assets/skin4.png'
                    ),
                    'skin5' => array(
                        'alt' => 'Skin 5',
                        'img' => get_template_directory_uri(). '/functions/assets/skin5.png'
                    ),
                    'skin6' => array(
                        'alt' => 'Skin 6',
                        'img' => get_template_directory_uri(). '/functions/assets/skin6.png'
                    ),
                    'skin7' => array(
                        'alt' => 'Skin 7',
                        'img' => get_template_directory_uri(). '/functions/assets/skin7.png'
                    ),
                    'skin8' => array(
                        'alt' => 'Skin 8',
                        'img' => get_template_directory_uri(). '/functions/assets/skin8.png'
                    ),
                    'skin9' => array(
                        'alt' => 'Skin 9',
                        'img' => get_template_directory_uri(). '/functions/assets/skin9.png'
                    ),
                    'skin10' => array(
                        'alt' => 'Skin 10',
                        'img' => get_template_directory_uri(). '/functions/assets/skin10.png'
                    ),
                    'skin11' => array(
                        'alt' => 'Skin 11',
                        'img' => get_template_directory_uri(). '/functions/assets/skin11.png'
                    ),
                    'skin12' => array(
                        'alt' => 'Skin 12',
                        'img' => get_template_directory_uri(). '/functions/assets/skin12.png'
                    ),
                    'skin13' => array(
                        'alt' => 'Skin 13',
                        'img' => get_template_directory_uri(). '/functions/assets/skin13.png'
                    ),
                    'skin14' => array(
                        'alt' => 'Skin 14',
                        'img' => get_template_directory_uri(). '/functions/assets/skin14.png'
                    ),
                    'skin15' => array(
                        'alt' => 'Skin 15',
                        'img' => get_template_directory_uri(). '/functions/assets/skin15.png'
                    ),
                    'skin16' => array(
                        'alt' => 'Skin 16',
                        'img' => get_template_directory_uri(). '/functions/assets/skin16.png'
                    ),
                    'skin17' => array(
                        'alt' => 'Skin 17',
                        'img' => get_template_directory_uri(). '/functions/assets/skin17.png'
                    ),
                    'skin18' => array(
                        'alt' => 'Skin 18',
                        'img' => get_template_directory_uri(). '/functions/assets/skin18.png'
                    ),
                    'skin19' => array(
                        'alt' => 'Skin 19',
                        'img' => get_template_directory_uri(). '/functions/assets/skin19.png'
                    ),
                    'skin20' => array(
                        'alt' => 'Skin 20',
                        'img' => get_template_directory_uri(). '/functions/assets/skin20.png'
                    ),
                    
                ),
                'default'  => 'default'
            ),
            array(
                'id' => 'override-preset',
                'type' => 'select',
                'title' => __('Use Your Own', 'meminz'),
                'subtitle' => __('Set this to <b>Yes</b> if you want to use colors and divider image background bellow.', 'meminz'),
                'options' => array(
                                    'yes' => 'Yes', 
                                    'no' => 'No'
                                ), //Must provide key => value pairs for select options
                'default' => 'no'
            ),
            array(
                'id' => 'text-color',
                'type' => 'color',
                'title' => __('Theme Text Color', 'meminz'),
                'compiler'=> array('a,
a:focus,
a:hover,
a:active,
.navbar-default .navbar-nav li a:hover,
.navbar-default .navbar-nav li a.selected,
.navbar-default .navbar-nav .active a,
.navbar-default .navbar-nav .dropdown.active a,
.navbar-default .navbar-nav .active a:hover,
.navbar-default .navbar-nav .dropdown.active a:hover,
.navbar-default .navbar-nav .active a:focus,
.navbar-default .navbar-nav .dropdown.active a:focus,
.icon-counter:hover,
.social-network a:hover,
.social-network a:focus,
.social-network a:active,
.pe-feature,
.accordion-heading a:hover i,
.wpb_accordion_header a:hover i,
.counter-number,
.pricing-head.popular .pricing-price,
.validation,
.widget_nav_menu > div > ul li a:hover,
.widget_rss > ul li a:hover,
.widget_recent_comments > ul li a:hover,
.widget_recent_entries > ul li a:hover,
.widget_meta > ul li a:hover,
.widget_archive > ul li a:hover,
.widget_pages > ul li a:hover,
.widget_categories > ul li a:hover,
.recent li h6 a:hover,
.pagination > li > a,
.pagination > li > a:hover,
.media h4.media-heading a:hover,
.wpt-tabs > li > a,
.wpt-tabs > li > a:hover,
.wpt_widget_content .tab-content li a:hover,
.meminz_main-nav > li.current-menu-parent > a,
.meminz_main-nav > .open > a.dropdown-toggle,
.meminz_main-nav > .open > a.dropdown-toggle:hover,
.meminz_main-nav > .open > a.dropdown-toggle:focus'),
                'subtitle' => __('Pick the text color for the theme (default: #f55e25).', 'meminz'),
                'default' => '#f55e25',
                'validate' => 'color',
            ),
            array(
                'id' => 'background-color',
                'type' => 'color',
                'compiler'=> array('#meminz_submit,
#meminz_submit:hover,
#meminz_submit:focus,
#meminz_submit:active,
#meminz_submit.active,
.btn-primary,
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.open > .dropdown-toggle.btn-primary,
.subscribe-button,
.navbar-default .navbar-toggle:hover .icon-bar,
.feature-box:hover .pe-feature,
.accordion-heading a i,
.accordion-heading a:hover,
.wpb_accordion_header a:hover,
.wpb_accordion_header a i,
.flex-direction-nav .flex-next:hover,
.flex-direction-nav .flex-prev:hover,
.flex-control-paging li a:hover,
.flex-control-paging li a.flex-active,
.pricing-head.popular,
.owl-theme .owl-controls .owl-buttons div.owl-prev:hover,
.owl-theme .owl-controls .owl-buttons div.owl-next:hover,
#toTopHover,
.log-tabs li a,
.log-tabs li a:hover,
.log-tabs li a:focus,
.log-tabs li a:active,
.tagcloud a:hover'),
                'title' => __('Theme Background Color', 'meminz'),
                'subtitle' => __('Pick the background color for the theme (default: #f55e25).', 'meminz'),
                'default' => '#f55e25',
                'validate' => 'color',
                'mode'=>'background-color',
            ),
            array(
                'id' => 'border-color',
                'type' => 'color',
                'compiler'=> array('#meminz_submit,
#meminz_submit:hover,
#meminz_submit:focus,
#meminz_submit:active,
#meminz_submit.active,
.btn-primary,
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.open > .dropdown-toggle.btn-primary,
.navbar-default .navbar-nav li a:hover,
.navbar-default .navbar-nav li a.selected,
.navbar-default .navbar-nav .active a,
.navbar-default .navbar-nav .dropdown.active a,
.navbar-default .navbar-nav .active a:hover,
.navbar-default .navbar-nav .dropdown.active a:hover,
.navbar-default .navbar-nav .active a:focus,
.navbar-default .navbar-nav .dropdown.active a:focus,
.subscribe-button,
.testimoni-avatar:hover,
.icon-counter:hover,
.form-control:focus,
ul.listForm li .form-control:focus,
.navbar-default .navbar-toggle:hover,
.pe-feature,
.accordion-heading a i,
.accordion-heading a:hover,
.wpb_accordion_header a i,
.wpb_accordion_header a:hover,
.tagcloud a:hover,
.meminz_main-nav > li.current-menu-parent > a,
.meminz_main-nav > .open > a.dropdown-toggle,
.meminz_main-nav > .open > a.dropdown-toggle:hover,
.meminz_main-nav > .open > a.dropdown-toggle:focus'),
                'title' => __('Theme Border Color', 'meminz'),
                'subtitle' => __('Pick the border color for the theme (default: #f55e25).', 'meminz'),
                'default' => '#f55e25',
                'validate' => 'color',
                'mode'=>'border-color',
            ),
            
            array(
                'id' => 'body-font',
                'type' => 'typography',
                'output' => array('body'),
                'title' => __('Body Font', 'meminz'),
                'subtitle' => __('Specify the body font properties.</br> Default</br>font-family: Open Sans</br>font-size: 14px</br>line-height: 24px</br>font-weight: 400</br>color: #444444', 'meminz'),
                'google' => true,
                // 'default' => array(
                //     'color' => '#444444',
                //     'font-size' => '14px',
                //     'line-height' => '24px',
                //     'font-family' => "Open Sans",
                //     'font-weight' => '400',
                // ),
            ),
            // array(
            //     'id' => 'navigation-font',
            //     'type' => 'typography',
            //     'output' => array('.navbar-default .navbar-nav li a,.navbar-default .navbar-nav li a:focus,.btn-bordered,.btn-primary'),
            //     'title' => __('Navigation Font', 'meminz'),
            //     'subtitle' => __('Specify the navigation font properties. Default</br>font-family: Open Sans</br>line-height: 18px', 'meminz'),
            //     'google' => true,
            //     'google' => true,
            //     'default' => array(
            //         'font-size' => '14px',
            //         'line-height' => '18px',
            //         'font-family' => "Open Sans",
            //     ),
            // ),
            array(
                'id' => 'header-font',
                'type' => 'typography',
                'output' => array('h1, h2, h3, h4, h5, h6'),
                'title' => __('Title-Header Font', 'meminz'),
                'subtitle' => __('Specify the title and heading font properties.</br> Default</br>font-family: Open Sans</br>font-weight: 700</br>color: #222222', 'meminz'),
                'google' => true,
                // 'default' => array(
                //     'color' => '#222222',
                //     'font-family' => "Open Sans",
                //     'font-weight' => '700',
                // ),
            ),
            

        ),
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __('Blog Settings', 'meminz'),
        'id'         => 'blog-settings',
        'subsection' => false,
        //'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/checkbox/" target="_blank">http://docs.reduxframework.com/core/fields/checkbox/</a>',
        'icon'       => 'el-icon-website',
        'fields' => array(
            // array(
            //     'id' => 'header_blog',
            //     'type' => 'media',
            //     'url' => true,
            //     'title' => __('Custom Image Header Blog', 'meminz'),
            //     //'compiler' => 'true',
            //     //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            //     'desc' => __('Upload your image header blog.', 'meminz'),
            //     'subtitle' => __('', 'meminz'),
            //     'default' => array('url' => get_template_directory_uri().'/images/bg2.jpg'),
            // ),
            array(
                    'id'       => 'blog_layout',
                    'type'     => 'image_select',
                    //'compiler' => true,
                    'title'    => __( 'Blog Layout', 'meminz' ),
                    'subtitle' => __( 'Select main content and sidebar alignment. Choose between 1 or 2 column layout.', 'meminz' ),
                    'options'  => array(
                        'fullwidth' => array(
                            'alt' => 'Fullwidth',
                            'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                        ),
                        'left_sidebar' => array(
                            'alt' => 'Left Sidebar',
                            'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                        ),
                        'right_sidebar' => array(
                            'alt' => 'Right Sidebar',
                            'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                        ),
                        // '4' => array(
                        //     'alt' => '3 Column Middle',
                        //     'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                        // ),
                        // '5' => array(
                        //     'alt' => '3 Column Left',
                        //     'img' => ReduxFramework::$_url . 'assets/img/3cl.png'
                        // ),
                        // '6' => array(
                        //     'alt' => '3 Column Right',
                        //     'img' => ReduxFramework::$_url . 'assets/img/3cr.png'
                        // )
                    ),
                    'default'  => 'right_sidebar'
                ),
            //  array(
            //     'id' => 'blog_layout',
            //     'type' => 'select',
            //     'title' => __('Blog Layout', 'meminz'),
            //     'subtitle' => __('Select Your Blog layout', 'meminz'),
            //     'desc' => '',
            //     'options' => array('1' => '1 Column', '2' => '2 Columns', '3' => '3 Columns', '4' => 'Time Line'), //Must provide key => value pairs for select options
            //     'default' => '1'
            // ),
            // array(
            //     'id' => 'blog_sidebar',
            //     'type' => 'select',
            //     'title' => __('Blog Sidebar', 'meminz'),
            //     'subtitle' => __('Select Your Blog Sidebar position', 'meminz'),
            //     'desc' => '',
            //     'options' => array('right' => 'Right', 'left' => 'Left'), //Must provide key => value pairs for select options
            //     'default' => 'right'
            // ),
            //  array(
            //     'id' => 'blog_full',
            //     'type' => 'checkbox',
            //     'title' => __('Blog Full Width', 'meminz'),
            //     'subtitle' => '',
            //     'desc' => '',
            //     'default' => '0'// 1 = on | 0 = off
            // ),
            array(
                'id' => 'blog_excerpt',
                'type' => 'text',
                'title' => __('Blog custom excerpt leng', 'meminz'),
                'subtitle' => __('Input Blog custom excerpt leng', 'meminz'),
                'default' => '40'
            ),
            array(
                'id' => 'author_checkbox',
                'type' => 'checkbox',
                'title' => __('Show author', 'meminz'),
                'default' => '1'// 1 = on | 0 = off
            ),
            array(
                'id' => 'date_checkbox',
                'type' => 'checkbox',
                'title' => __('Show date', 'meminz'),
                'default' => '1'// 1 = on | 0 = off
            ),
            array(
                'id' => 'cats_checkbox',
                'type' => 'checkbox',
                'title' => __('Show categories', 'meminz'),
                'default' => '1' // 1 = on | 0 = off
            ),
            array(
                'id' => 'tag_checkbox',
                'type' => 'checkbox',
                'title' => __('Show tag', 'meminz'),
                'default' => '1' // 1 = on | 0 = off
            ),
            array(
                'id' => 'comment_checkbox',
                'type' => 'checkbox',
                'title' => __('Show comment', 'meminz'),
                'default' => '1' // 1 = on | 0 = off
            ),
             array(
                'id' => 'author_infor',
                'type' => 'checkbox',
                'title' => __('Show author information', 'meminz'),
                'subtitle' => __('Show information author for list post', 'meminz'),
                'default' => '1'// 1 = on | 0 = off
            ),
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __('Login Settings', 'meminz'),
        'id'         => 'login-settings',
        'subsection' => false,
        //'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/checkbox/" target="_blank">http://docs.reduxframework.com/core/fields/checkbox/</a>',
        'icon'       => 'el-icon-group',
        'fields' => array(
            array(
                'id'       => 'user_login_type',
                'type'     => 'select',
                'title'    => __( 'User Login Form', 'meminz' ),
                'subtitle' => __( 'Select type of login form want to show on front.', 'meminz' ),
                'options'  => array( 'off' => 'Off', 'ajax' => 'Ajax Modal' ,'normal' => 'Noral From','custom' => 'Custom Link'),
                'default'  => 'normal',
            ),
            array(
                'id'       => 'user_singup_type',
                'type'     => 'select',
                'title'    => __( 'User Register Form', 'meminz' ),
                'subtitle' => __( 'Select type of register form want to show on front.', 'meminz' ),
                'options'  => array( 'off' => 'Off', 'ajax' => 'Ajax Modal' ,'normal' => 'Noral From','custom' => 'Custom Link'),
                'default'  => 'normal',
            ),
            array(
                'id' => 'custom_login_link',
                'type' => 'text',
                'title' => __('Custom Login Link', 'meminz'),
                'subtitle' => __('Custom Login Link', 'meminz'),
                'default' => ''
            ),
            array(
                'id' => 'custom_signup_link',
                'type' => 'text',
                'title' => __('Custom Signup Link', 'meminz'),
                'subtitle' => __('Custom Signup Link', 'meminz'),
                'default' => ''
            ),
            
            array(
                'id'       => 'show_logreg_md',
                'type'     => 'select',
                'title'    => __( 'Show Buttons on medium', 'meminz' ),
                'subtitle' => __( 'Show buttons on medium device .', 'meminz' ),
                'options'  => array( 'no' => 'No', 'yes' => 'Yes'),
                'default'  => 'yes',
            ),
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __('404 Intro text Settings', 'meminz'),
        'id'         => '404-intro-text-settings',
        'subsection' => false,
        //'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/checkbox/" target="_blank">http://docs.reduxframework.com/core/fields/checkbox/</a>',
        'icon'       => 'el-icon-file-edit',
        'fields' => array(
            array(
                'id' => '404_intro',
                'type' => 'editor',
                'title' => __('404 Page Intro ', 'meminz'),
                
                'default' => 'Sorry, but the page you are looking for has not been found.'
            ),
            // array(
            //     'id' => '404_bg',
            //     'type' => 'media',
            //     'url' => true,
            //     'title' => __('404 Background Image', 'meminz'),
            //     //'compiler' => 'true',
            //     //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            //     'desc' => __('Upload your 404 page background image', 'meminz'),
            //     'subtitle' => __('Upload your 404 page background image', 'meminz'),
            //     'default' => array('url' => get_template_directory_uri().'/images/home-slider/2.jpg'),
            // ),
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'title' => __('Custom Code', 'meminz'),
        'id'         => 'custom-code',
        'subsection' => false,
        //'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="http://docs.reduxframework.com/core/fields/checkbox/" target="_blank">http://docs.reduxframework.com/core/fields/checkbox/</a>',
        'icon'       => 'el-icon-file-new',
        'fields' => array(
            array(
                'id' => 'custom-css',
                'type' => 'ace_editor',
                'title' => __('CSS Code', 'meminz'),
                'subtitle' => __('Paste your CSS code here.', 'meminz'),
                'mode' => 'css',
                'compiler'=>array('body'),
                'theme' => 'monokai',
                

                'default' => ""
            ),
            
            
        ),
    ) );


    if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
        $section = array(
            'icon'   => 'el el-list-alt',
            'title'  => esc_html__( 'Documentation', 'meminz' ),
            'fields' => array(
                array(
                    'id'       => '17',
                    'type'     => 'raw',
                    'markdown' => true,
                    'content_path' => dirname( __FILE__ ) . '/../README.md', // FULL PATH, not relative please
                    //'content' => 'Raw content here',
                ),
            ),
        );
        Redux::setSection( $opt_name, $section );
    }
    /*
     * <--- END SECTIONS
     */

    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            /*echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            */
            $filename = get_template_directory() . '/skins/overridestyle.css';
            global $wp_filesystem;
            if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
                WP_Filesystem();
            }

            if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
            }


        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'meminz' ),
                'desc'   => wp_kses(__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'meminz' ),array('p'=>array('class'=>array())) ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }
