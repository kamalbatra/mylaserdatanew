<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

 ?>
<!DOCTYPE html>
<?php 
global $theme_options; 
?>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <!-- Favicon -->        
        <link rel="shortcut icon" href="<?php echo esc_url($theme_options['favicon']['url']);?>" type="image/x-icon"/>
        <?php    
        wp_head(); ?>
    </head>
    <body <?php body_class( );?>>  

            <?php if($theme_options['show_loader']) :?>
            <!-- Start preloading -->   
            <div id="loading" class="loading-invisible">
                <i class="pe-7s-refresh pe-spin pe-3x pe-va"></i><br />
                <p><?php _e('Please wait...','meminz');?></p>
            </div>
            <!-- End preloading -->
            <?php endif;?>

            <!-- <div id="panel"> 
                <div class="panel-inner">
                    <div class="options_toggle_holder">
                        <span class="options_toggle"><i class="pe-7s-tools"></i></span>
                    </div>  
                    <div class="options_box">       
                        <h6>Index <span>variant</span></h6>
                        <div class="layout">
                            <a href="home-alt1" class="btn btn-primary btn-block">Home Alt1</a>
                            <a href="home-alt2" class="btn btn-primary btn-block">Home Alt2</a>
                            <a href="home-alt3" class="btn btn-primary btn-block">Home Alt3</a>
                            <a href="home-alt4" class="btn btn-primary btn-block">Home Alt4</a>
                            <a href="home-alt5" class="btn btn-primary btn-block">Home Alt5</a>
                            <a href="home-alt6" class="btn btn-primary btn-block">Home Alt6</a>
                            <a href="blog" class="btn btn-primary btn-block">Blog Page</a>
                        </div>
                    </div>          
                    <div class="options_box">       
                        <h6>Unlimited <span>color</span></h6>
                        <div class="layout">
                        Use colorpicker <input maxlength="6" size="6" id="accent_color" value="f55e25" type="text">
                        </div>
                    </div>      
                </div>
                <div id="custom_styles"></div>
            </div> -->


            <?php if($theme_options['show_cart']) :?>
            <!-- cart panel -->
            <div id="cart_panel"> 
                <div class="panel-inner">
                    <div class="options_toggle_holder">
                        <span class="options_toggle"><i class="pe-7s-cart"></i></span>
                    </div>  
                    <?php
                        if(is_active_sidebar('shopping_cart')){
                            dynamic_sidebar('shopping_cart');
                        }
                        
                    ?>
                </div>
            </div>
            <!-- end of options panel -->  
            <?php endif;?>
            <!-- Start header -->
            <header>
                <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only"><?php _e('Toggle navigation','meminz');?></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo esc_url(home_url('/'));?>">
                                <?php if($theme_options['logo']['url']):?>
                                    <img src="<?php echo esc_url($theme_options['logo']['url']);?>" width="<?php echo esc_attr($theme_options['logo_size_width'] );?>" height="<?php echo esc_attr($theme_options['logo_size_height'] );?>" class="logo-img" alt="<?php bloginfo('name');?>" />
                                <?php endif;?>
                                <?php if($theme_options['logo_text']):?>
                                    <h1 class="logo_text"><?php echo esc_html($theme_options['logo_text']);?></h1>
                                <?php endif;?>
                                <?php if($theme_options['slogan']):?>
                                    <h3 class="slogan"><em><?php echo esc_html($theme_options['slogan']);?></em></h3>
                                <?php endif;?>
                            </a>
                        </div>

                        <div class="collapse navbar-collapse">
                            <?php if(is_page_template('homepage.php' )){
                        
                                $defaults1= array(
                                    'theme_location'  => 'landing-menu',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'nav navbar-nav meminz_main-nav',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                                    'walker'          => new wp_bootstrap_navwalker(),
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                );
                                
                                if ( has_nav_menu( 'landing-menu' ) ) {
                                    wp_nav_menu( $defaults1 );
                                }
                            }else{
                                $defaults1= array(
                                                'theme_location'  => 'primary',
                                                'menu'            => '',
                                                'container'       => '',
                                                'container_class' => '',
                                                'container_id'    => '',
                                                'menu_class'      => 'nav navbar-nav meminz_main-nav',
                                                'menu_id'         => '',
                                                'echo'            => true,
                                                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                                                'walker'          => new wp_bootstrap_navwalker(),
                                                'before'          => '',
                                                'after'           => '',
                                                'link_before'     => '',
                                                'link_after'      => '',
                                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                'depth'           => 0,
                                );
                                if ( has_nav_menu( 'primary' ) ) {
                                    wp_nav_menu( $defaults1 );
                                }
                            } ?>

                            <?php if($theme_options['user_login_type']!=='off' || $theme_options['user_singup_type']!=='off' ):?>
                            <?php if($theme_options['show_logreg_md'] === 'no') :?>   
                                <div class="navbar-right hidden-sm">
                            <?php else : ?>
                                <div class="navbar-right">
                            <?php endif;?>
                                <?php if($theme_options['user_login_type']==='ajax') :?>
                                    <a data-toggle="modal" data-target="#loginAjaxModal" data-activetab="#loginHref" href="#" class="btn btn-bordered"><?php _e('Sign in','meminz');?></a>
                                <?php elseif($theme_options['user_login_type'] === 'normal') :?>
                                    <a href="<?php echo wp_login_url( home_url() ); ?>" class="btn btn-bordered"><?php _e('Sign in','meminz');?></a>
                                <?php elseif($theme_options['user_login_type'] === 'custom') :?>
                                    <a href="<?php echo esc_url($theme_options['custom_login_link'] ); ?>" class="btn btn-bordered"><?php _e('Sign in','meminz');?></a>
                                <?php endif;?>

                                <?php if($theme_options['user_singup_type']==='ajax') :?>
                                    <a data-toggle="modal" data-target="#loginAjaxModal" data-activetab="#registerHref" href="#" class="btn btn-primary"><?php _e('Sign up','meminz');?></a>
                                <?php elseif($theme_options['user_singup_type']==='normal') :?>
                                    <a href="<?php echo wp_registration_url(); ?>" class="btn btn-primary"><?php _e('Sign up','meminz');?></a>
                                <?php elseif($theme_options['user_singup_type']==='custom') :?>
                                    <a href="<?php echo esc_url($theme_options['custom_signup_link'] ); ?>" class="btn btn-primary"><?php _e('Sign up','meminz');?></a>
                                 <?php endif;?>  
            
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>  
            </header>
            <!-- End header -->
        <?php if($theme_options['user_login_type']==='ajax' || $theme_options['user_singup_type']==='ajax' ):?>   
            <!-- Ajax Login Modal -->
            <div class="modal fade" id="loginAjaxModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <?php get_template_part('logreg-ajax');?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;?>
