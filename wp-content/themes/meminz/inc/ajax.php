<?php


/* Login Ajax Action */
add_action( 'wp_ajax_nopriv_user_login', 'rs_user_login_callback' );
add_action( 'wp_ajax_user_login', 'rs_user_login_callback' );
/*
 *  @desc   Process theme login
 */
function rs_user_login_callback() {
    global $wpdb;
    
    $json = array();
    
    $error = '';
    $success = '';
    $nonce = $_POST['nonce'];
    
    if ( ! wp_verify_nonce( $nonce, 'rs_user_login_action' ) )
        die ( '<p class="error">Security checked!, Cheatn huh?</p>' );

    $username = esc_sql($_POST['log']);
    $password = esc_sql($_POST['pwd']);
    $remember = esc_sql($_POST['remember']);
    $redirection_url = esc_sql($_POST['redirection_url']); 
    
    if( empty( $username ) ) {
        $json[] = __('Username field is required.','meminz');
    } else if( empty( $password ) ) {
        $json[] = __('Password field is required.','meminz');
    } else {
        
        $user_data = array();
        $user_data['user_login'] = $username;
        $user_data['user_password'] = $password;
        $user_data['remember'] = $remember; 
        $user = wp_signon( $user_data, false );
        
        if ( is_wp_error($user) ) {
            $json[] = $user->get_error_message();
        } else {
            wp_set_current_user( $user->ID, $username );
            do_action('set_current_user');
            
            $json['success'] = 1;
            $json['redirection_url'] = $redirection_url;
        }
    }
    
    
    echo json_encode( $json );
    
    // return proper result
    die();
}

add_action( 'wp_ajax_nopriv_user_registration', 'rs_user_registration_callback' );
add_action( 'wp_ajax_user_registration', 'rs_user_registration_callback' );

/*
 *  @desc   Register user
 */
function rs_user_registration_callback() {
    global $wpdb;
    
    $error = '';
    $success = '';
    $nonce = $_POST['nonce'];
    
    if ( ! wp_verify_nonce( $nonce, 'rs_user_registration_action' ) )
        die ( '<p class="error">Security checked!, Cheatn huh?</p>' );
    
    $log = $_POST['log'];
    $pwd = $_POST['pwd'];
    
    if( empty( $log ) ) {
        $error = __('Username field is required.','meminz');
    } else if( empty( $pwd ) ) {
        $error = __('Password field is required.','meminz');
    } else {
        $user_params = array (
            'user_login'    => apply_filters( 'pre_user_user_login', $log ), 
            'user_pass'     => apply_filters( 'pre_user_user_pass', $pwd ),
            'role'          => 'subscriber'
        );

        $user_id = wp_insert_user( $user_params );
        
        if( is_wp_error( $user_id ) ) {
            $error = $user_id->get_error_message();
        } else {
            do_action( 'user_register', $user_id );
            
            $success = 1;      
        } 
    }
    
    if( ! empty( $error ) )
        echo '<p class="error">'. $error .'</p>';
    
    if( ! empty( $success ) )
        echo htmlspecialchars_decode($success);
        
    // return proper result
    die();
}



// back-end image 
add_action('wp_ajax_nopriv_meminz_get_vc_attach_images', 'meminz_get_vc_attach_images_callback');
add_action('wp_ajax_meminz_get_vc_attach_images', 'meminz_get_vc_attach_images_callback');


function meminz_get_vc_attach_images_callback() {
    $images = $_POST['images'];
    $html = $images;
    if($images != '') {
        $images = explode(",", $images);
        if(count($images)){
            $html = '';
            foreach ($images as $key => $img) {
                $html .= wp_get_attachment_image( $img, 'thumbnail', '', array('class'=>'meminz-ele-attach-thumb') );
            }
        }
    }
    wp_send_json($html );
}