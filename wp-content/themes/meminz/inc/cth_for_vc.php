<?php

/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
add_action( 'vc_before_init', 'meminz_vcSetAsTheme' );
if(!function_exists('meminz_vcSetAsTheme')){
    function meminz_vcSetAsTheme() {
        vc_set_as_theme($disable_updater = true);
    }
}
if(!function_exists('meminz_echo_vc_custom_styles')){
    function meminz_echo_vc_custom_styles(){
        global $post;
        $cus_style_metas = get_post_meta( $post->ID, '_wpb_shortcodes_custom_css', true );
        if(!empty($cus_style_metas)){
            echo '<style>'.esc_attr($cus_style_metas).'</style>';
        }
    }
}

if(!function_exists('meminz_custom_css_classes_for_vc_row_and_vc_column')){
    //if(class_exists('WPBakeryVisualComposerSetup')){
    function meminz_custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
        if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
            $class_string = str_replace('vc_row', 'row', $class_string);
        }
        // if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
        //     $class_string = preg_replace('/vc_col-(xs|sm|md|lg)-(\d{1,2})/', 'col-$1-$2', $class_string);
        //     $class_string = preg_replace('/vc_col-(xs|sm|md|lg)-offset-(\d{1,2})/', 'col-$1-offset-$2', $class_string);
        // }
        return $class_string;
    }
}
// Filter to Replace default css class for vc_row shortcode and vc_column
// add_filter('vc_shortcodes_css_class', 'meminz_custom_css_classes_for_vc_row_and_vc_column', 10, 2); 

// Add new Param in Row

if(function_exists('vc_add_param')){
        vc_add_param('vc_row',array(
                                  "type" => "textfield",
                                  "heading" => __('Section id', 'meminz'),
                                  "param_name" => "ses_id",
                                  "value" => "",
                                  "description" => __("Section id", 'meminz'),   
        ));

        vc_add_param('vc_row',array(
                                  "type" => "textfield",
                                  "heading" => __('Section Class', 'meminz'),
                                  "param_name" => "ses_class",
                                  "value" => "",
                                  "description" => __("Section Class", 'meminz'),   
        ));
        vc_add_param('vc_row',array(
                                  "type" => "textfield",
                                  "heading" => __('Section Title', 'meminz'),
                                  "param_name" => "ses_title",
                                  "value" => "",
                                  "description" => __("Title of Section ", 'meminz'),   
        ));

        vc_add_param('vc_row',array(
                                  "type" => "textarea",
                                  "heading" => __('SubTitle', 'meminz'),
                                  "param_name" => "ses_subtitle",
                                  "value" => "",
                                  "description" => __("SubTitle of Section ", 'meminz'),   
        ));

        vc_add_param('vc_row',array(
                                  "type" => "textfield",
                                  "heading" => __('Next section link', 'meminz'),
                                  "param_name" => "ses_link",
                                  "value" => "",
                                  "description" => __("Next section link ex: #about - go to about us section", 'meminz'),   
        ));
        vc_add_param('vc_row',array(
                                  "type" => "dropdown",
                                  "heading" => __('Section Layout', 'meminz'),
                                  "param_name" => "layout",
                                  "value" => array(   
                                                    __('Default', 'meminz') => 'default',  
                                                    __('Home with background Section', 'meminz') => 'home_bg_sec',
                                                    __('Home Ticker Section', 'meminz') => 'home_ticker_sec',
                                                    __('Home Slider Section', 'meminz') => 'home_slider_sec',
                                                    __('Features Section', 'meminz') => 'features_sec',
                                                    __('Description Section','meminz') => 'des_sec',
                                                    __('Counter Section','meminz') => 'counter_sec',
                                                    __('Screenshot Section','meminz') => 'screenshot_sec',
                                                    __('Pricing Section','meminz') => 'pricing_sec',
                                                    __('Download Section','meminz') => 'download_sec',
                                                    __('Testimonials Section','meminz') => 'testi_sec', 
                                                    __('Client Section','meminz') => 'client_sec', 
                                                    __('Contact Section','meminz') => 'contact_sec', 
                                                    // __('Team Section','meminz') => 'team_sec',
                                                    // __('About Section', 'meminz') => 'about_sec',
                                                    // __('Services Section','meminz') => 'services_sec',
                                                    // __('Process Section','meminz') => 'process_sec',
                                                    // __('Call to action Section','meminz') => 'cta_sec',
                                                    // __('Our Clients Section','meminz') => 'clients_sec',
                                                    // __('Portfolio Section','meminz') => 'portfolio_sec',
                                                    // __('Latest News & Fun Section','meminz') => 'news_sec',
                                                    // __('Twitter Feed','meminz') => 'twitter_sec',
                                                    
                                                    
                                                                                                      
                                                                                                     
                                                    // __('Subscribe Section','meminz') => 'sub_sec',                                                  
                                                    // __('Contact Section','meminz') => 'contact_sec',
                                                    // __('Future Video Section','meminz') => 'video_sec',
                                                    
                                                    // __('Pricing Section','meminz') => 'pricing_sec',
                                                    // __('Component Section', 'meminz') => 'component_sec',
                                                    // __('General Section', 'meminz') => 'gen_sec',
                                                    // __('General Fullwidth Section', 'meminz') => 'gen_full_sec',
                                                  ),
                                  "description" => __("Select one of the pre made page sections or using default", 'meminz'),      
                                ) 
        );

        vc_add_param('vc_column',array(
                                "type" => "textfield",
                                "heading" => __('Column ID', 'meminz'),
                                "param_name" => "col_id",
                                "value" => "",
                                "description" => __("Column ID", 'meminz'),
                                ) 

        );

        vc_add_param('vc_column',array(
                                  "type" => "dropdown",
                                  "heading" => __('Use Animation', 'meminz'),
                                  "param_name" => "animation",
                                  "value" => array(   
                                                    __('No', 'meminz') => 'no',  
                                                    __('Yes', 'meminz') => 'yes',                                                                                
                                                  ),
                                  "description" => __("Use animation effect or not", 'meminz'),      
                                ) 
        );

        vc_add_param('vc_column',array(
                                  "type" => "dropdown",
                                  "heading" => __('Data effect', 'meminz'),
                                  "param_name" => "effect",
                                  "value" => array(
                                                    __('bounce','meminz')=>'bounce',
                                                    __('flash','meminz')=>'flash',
                                                    __('pulse','meminz')=>'pulse',
                                                    __('rubberBand','meminz')=>'rubberBand',
                                                    __('shake','meminz')=>'shake',
                                                    __('swing','meminz')=>'swing',
                                                    __('tada','meminz')=>'tada',
                                                    __('wobble','meminz')=>'wobble',

                                                    __('bounceIn','meminz')=>'bounceIn',
                                                    __('bounceInUp','meminz')=>'bounceInUp',
                                                    __('bounceInDown','meminz')=>'bounceInDown',
                                                    __('bounceInLeft','meminz')=>'bounceInLeft',
                                                    __('bounceInRight','meminz')=>'bounceInRight',
                                                    __('bounceOut','meminz')=>'bounceOut',
                                                    __('bounceOutUp','meminz')=>'bounceOutUp',
                                                    __('bounceOutDown','meminz')=>'bounceOutDown',
                                                    __('bounceOutLeft','meminz')=>'bounceOutLeft',
                                                    __('bounceOutRight','meminz')=>'bounceOutRight',

                                                    __('fadeIn','meminz')=>'fadeIn',
                                                    __('fadeInUp','meminz')=>'fadeInUp',
                                                    __('fadeInDown','meminz')=>'fadeInDown',
                                                    __('fadeInLeft','meminz')=>'fadeInLeft',
                                                    __('fadeInRight','meminz')=>'fadeInRight',
                                                    __('fadeInUpBig','meminz')=>'fadeInUpBig',
                                                    __('fadeInDownBig','meminz')=>'fadeInDownBig',
                                                    __('fadeInLeftBig','meminz')=>'fadeInLeftBig',
                                                    __('fadeInRightBig','meminz')=>'fadeInRightBig',

                                                    __('fadeOut','meminz')=>'fadeOut',
                                                    __('fadeOutUp','meminz')=>'fadeOutUp',
                                                    __('fadeOutDown','meminz')=>'fadeOutDown',
                                                    __('fadeOutLeft','meminz')=>'fadeOutLeft',
                                                    __('fadeOutRight','meminz')=>'fadeOutRight',
                                                    __('fadeOutUpBig','meminz')=>'fadeOutUpBig',
                                                    __('fadeOutDownBig','meminz')=>'fadeOutDownBig',
                                                    __('fadeOutLeftBig','meminz')=>'fadeOutLeftBig',
                                                    __('fadeOutRightBig','meminz')=>'fadeOutRightBig',

                                                    __('flipInX','meminz')=>'flipInX',
                                                    __('flipInY','meminz')=>'flipInY',
                                                    __('flipOutX','meminz')=>'flipOutX',
                                                    __('flipOutY','meminz')=>'flipOutY',
                                                    __('rotateIn','meminz')=>'rotateIn',
                                                    __('rotateInDownLeft','meminz')=>'rotateInDownLeft',
                                                    __('rotateInDownRight','meminz')=>'rotateInDownRight',
                                                    __('rotateInUpLeft','meminz')=>'rotateInUpLeft',
                                                    __('rotateInUpRight','meminz')=>'rotateInUpRight',

                                                    __('rotateOut','meminz')=>'rotateOut',
                                                    __('rotateOutDownLeft','meminz')=>'rotateOutDownLeft',
                                                    __('rotateOutDownRight','meminz')=>'rotateOutDownRight',
                                                    __('rotateOutUpLeft','meminz')=>'rotateOutUpLeft',
                                                    __('rotateOutUpRight','meminz')=>'rotateOutUpRight',

                                                    __('rotateOut','meminz')=>'rotateOut',
                                                    __('rotateOutDownLeft','meminz')=>'rotateOutDownLeft',
                                                    __('rotateOutDownRight','meminz')=>'rotateOutDownRight',
                                                    __('rotateOutUpLeft','meminz')=>'rotateOutUpLeft',
                                                    __('rotateOutUpRight','meminz')=>'rotateOutUpRight',

                                                    __('slideInDown','meminz')=>'slideInDown',
                                                    __('slideInLeft','meminz')=>'slideInLeft',
                                                    __('slideInRight','meminz')=>'slideInRight',
                                                    __('slideOutLeft','meminz')=>'slideOutLeft',
                                                    __('slideOutRight','meminz')=>'slideOutRight',
                                                    __('slideOutUp','meminz')=>'slideOutUp',
                                                    __('slideInUp','meminz')=>'slideInUp',
                                                    __('slideOutDown','meminz')=>'slideOutDown',

                                                    __('hinge','meminz')=>'hinge',

                                                    __('rollIn','meminz')=>'rollIn',
                                                    __('rollOut','meminz')=>'rollOut',
                                                    

                                                    __('zoomIn','meminz')=>'zoomIn',
                                                    __('zoomInUp','meminz')=>'zoomInUp',
                                                    __('zoomInDown','meminz')=>'zoomInDown',
                                                    __('zoomInLeft','meminz')=>'zoomInLeft',
                                                    __('zoomInRight','meminz')=>'zoomInRight',

                                                    __('zoomOut','meminz')=>'zoomOut',
                                                    __('zoomOutUp','meminz')=>'zoomOutUp',
                                                    __('zoomOutDown','meminz')=>'zoomOutDown',
                                                    __('zoomOutLeft','meminz')=>'zoomOutLeft',
                                                    __('zoomOutRight','meminz')=>'zoomOutRight',
                                                ),                              
                                  "description" => __("Add data effect", 'meminz'), 
                                  'dependency' => array(
                                        'element' => 'animation',
                                        'value' => array( 'yes' ),
                                        'not_empty' => false,
                                    ),     
                                ) 

        );

        vc_add_param('vc_column',array(
                              "type" => "textfield",
                                  "heading" => __('Animation Delay', 'meminz'),
                                  "param_name" => "delay",
                                  "value" => "",
                                  "description" => __("Animation delay in second like 0.4s", 'meminz'),
                                  'dependency' => array(
                                        'element' => 'animation',
                                        'value' => array( 'yes' ),
                                        'not_empty' => false,
                                    ),
                                ) 

        );

        // Add new Param in Accordion

    // vc_add_param('vc_accordion',array(

    //                               "type" => "textfield",
    //                               "heading" => __('ID', 'meminz'),
    //                               "param_name" => "id",
    //                               "value" => "parentID",
    //                               "description" => __("Id be matched with section parent id", 'meminz'),
    //                             ) 

    //     );

    // vc_add_param('vc_accordion',array(

    //                               "type" => "dropdown",
    //                               "heading" => __('Style', 'meminz'),
    //                               "param_name" => "style",
    //                               "value" => array(
    //                                         __('Style 1','meminz')=>'style1',
    //                                         __('Style 2','meminz')=>'style2',
    //                                 ),
    //                               "description" => __("Style: 1 - no border, 2: bordered", 'meminz'),
    //                             ) 

    //     );

    // vc_add_param('vc_accordion_tab',array(

    //                               "type" => "textfield",
    //                               "heading" => __('Parent ID', 'meminz'),
    //                               "param_name" => "parentid",
    //                               "value" => "parentID",
    //                               "description" => __("Enter parent id to take effect", 'meminz'),
    //                             ) 

    //     );

    // vc_add_param('vc_accordion_tab',array(

    //                               "type" => "textfield",
    //                               "heading" => __('Icon', 'meminz'),
    //                               "param_name" => "icon",
    //                               "value" => "chevron-down",
    //                               "description" => __("icon", 'meminz'),
    //                             ) 

    //     );

    }