<?php
//For category taxonomy
// Add term page
function meminz_category_add_new_meta_field() {
    
    // this will add the custom meta field to the add new term page
    wp_enqueue_media();
    wp_enqueue_script('meminz_tax_meta', get_template_directory_uri() . '/inc/assets/upload_file.js', array('jquery'), null, true);
?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[cat_header_image]"><?php
    esc_html_e('Header Background Image', 'meminz'); ?></label></th>
        <td>
            <img id="term_meta[cat_header_image][preview]" src="" alt="" style="display:none;width:200px;height:auto;">
            <input type="hidden" name="term_meta[cat_header_image][url]" id="term_meta[cat_header_image][url]" value="">
            <input type="hidden" name="term_meta[cat_header_image][id]" id="term_meta[cat_header_image][id]" value="">
            <p class="description"><a href="#" class="button button-primary upload_image_button metakey-term_meta fieldkey-cat_header_image"><?php
    esc_html_e('Upload Image', 'meminz'); ?></a>  <a href="#" class="button button-secondary remove_image_button metakey-term_meta fieldkey-cat_header_image" style="display:none;"><?php
    esc_html_e('Remove', 'meminz'); ?></a></p>
        </td>
    </tr>
<?php
}
add_action('category_add_form_fields', 'meminz_category_add_new_meta_field', 10, 2);

// Edit term page
function meminz_category_edit_meta_field($term) {
    wp_enqueue_media();
    wp_enqueue_script('meminz_tax_meta', get_template_directory_uri() . '/inc/assets/upload_file.js', array('jquery'), null, true);
    
    // put the term ID into a variable
    $t_id = $term->term_id;
    
    // retrieve the existing value(s) for this meta field. This returns an array
    $term_meta = get_option("taxonomy_category_$t_id");
    
    // echo'<pre>';var_dump($term_meta);die;
    
?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[cat_header_image]"><?php
    esc_html_e('Header Background Image', 'meminz'); ?></label></th>
        <td>
            <img id="term_meta[cat_header_image][preview]" src="<?php
    echo isset($term_meta['cat_header_image']['url']) ? esc_attr($term_meta['cat_header_image']['url']) : ''; ?>" alt="" <?php
    echo isset($term_meta['cat_header_image']['url']) ? ' style="display:block;width:200px;height=auto;"' : ' style="display:none;width:200px;height=auto;"'; ?>>
            <input type="hidden" name="term_meta[cat_header_image][url]" id="term_meta[cat_header_image][url]" value="<?php
    echo isset($term_meta['cat_header_image']['url']) ? esc_attr($term_meta['cat_header_image']['url']) : ''; ?>">
            <input type="hidden" name="term_meta[cat_header_image][id]" id="term_meta[cat_header_image][id]" value="<?php
    echo isset($term_meta['cat_header_image']['id']) ? esc_attr($term_meta['cat_header_image']['id']) : ''; ?>">
            
            <p class="description"><a href="#" class="button button-primary upload_image_button metakey-term_meta fieldkey-cat_header_image"><?php
    esc_html_e('Upload Image', 'meminz'); ?></a>  <a href="#" class="button button-secondary remove_image_button metakey-term_meta fieldkey-cat_header_image"><?php
    esc_html_e('Remove', 'meminz'); ?></a></p>
        </td>
    </tr>
<?php
}
add_action('category_edit_form_fields', 'meminz_category_edit_meta_field', 10, 2);

// Save extra taxonomy fields callback function.
function meminz_save_category_custom_meta($term_id) {
    if (isset($_POST['term_meta'])) {
        $t_id = $term_id;
        $term_meta = get_option("taxonomy_category_$t_id");
        $cat_keys = array_keys($_POST['term_meta']);
        foreach ($cat_keys as $key) {
            if (isset($_POST['term_meta'][$key])) {
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        
        // Save the option array.
        update_option("taxonomy_category_$t_id", $term_meta);
    }
}
add_action('edited_category', 'meminz_save_category_custom_meta', 10, 2);
add_action('create_category', 'meminz_save_category_custom_meta', 10, 2);

//For post_tag taxonomy
// Add term page
function meminz_post_tag_add_new_meta_field() {
    
    // this will add the custom meta field to the add new term page
    wp_enqueue_media();
    wp_enqueue_script('meminz_tax_meta', get_template_directory_uri() . '/inc/assets/upload_file.js', array('jquery'), null, true);
?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[cat_header_image]"><?php
    esc_html_e('Header Background Image', 'meminz'); ?></label></th>
        <td>
            <img id="term_meta[cat_header_image][preview]" src="" alt="" style="display:none;width:200px;height:auto;">
            <input type="hidden" name="term_meta[cat_header_image][url]" id="term_meta[cat_header_image][url]" value="">
            <input type="hidden" name="term_meta[cat_header_image][id]" id="term_meta[cat_header_image][id]" value="">
            <p class="description"><a href="#" class="button button-primary upload_image_button metakey-term_meta fieldkey-cat_header_image"><?php
    esc_html_e('Upload Image', 'meminz'); ?></a>  <a href="#" class="button button-secondary remove_image_button metakey-term_meta fieldkey-cat_header_image" style="display:none;"><?php
    esc_html_e('Remove', 'meminz'); ?></a></p>
        </td>
    </tr>
<?php
}
add_action('post_tag_add_form_fields', 'meminz_post_tag_add_new_meta_field', 10, 2);

// Edit term page
function meminz_post_tag_edit_meta_field($term) {
    wp_enqueue_media();
    wp_enqueue_script('meminz_tax_meta', get_template_directory_uri() . '/inc/assets/upload_file.js', array('jquery'), null, true);
    
    // put the term ID into a variable
    $t_id = $term->term_id;
    
    // retrieve the existing value(s) for this meta field. This returns an array
    $term_meta = get_option("taxonomy_post_tag_$t_id");
    
    // echo'<pre>';var_dump($term_meta);die;
    
?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[cat_header_image]"><?php
    esc_html_e('Header Background Image', 'meminz'); ?></label></th>
        <td>
            <img id="term_meta[cat_header_image][preview]" src="<?php
    echo isset($term_meta['cat_header_image']['url']) ? esc_attr($term_meta['cat_header_image']['url']) : ''; ?>" alt="" <?php
    echo isset($term_meta['cat_header_image']['url']) ? ' style="display:block;width:200px;height=auto;"' : ' style="display:none;width:200px;height=auto;"'; ?>>
            <input type="hidden" name="term_meta[cat_header_image][url]" id="term_meta[cat_header_image][url]" value="<?php
    echo isset($term_meta['cat_header_image']['url']) ? esc_attr($term_meta['cat_header_image']['url']) : ''; ?>">
            <input type="hidden" name="term_meta[cat_header_image][id]" id="term_meta[cat_header_image][id]" value="<?php
    echo isset($term_meta['cat_header_image']['id']) ? esc_attr($term_meta['cat_header_image']['id']) : ''; ?>">
            
            <p class="description"><a href="#" class="button button-primary upload_image_button metakey-term_meta fieldkey-cat_header_image"><?php
    esc_html_e('Upload Image', 'meminz'); ?></a>  <a href="#" class="button button-secondary remove_image_button metakey-term_meta fieldkey-cat_header_image"><?php
    esc_html_e('Remove', 'meminz'); ?></a></p>
        </td>
    </tr>
<?php
}
add_action('post_tag_edit_form_fields', 'meminz_post_tag_edit_meta_field', 10, 2);

// Save extra taxonomy fields callback function.
function meminz_save_post_tag_custom_meta($term_id) {
    if (isset($_POST['term_meta'])) {
        $t_id = $term_id;
        $term_meta = get_option("taxonomy_post_tag_$t_id");
        $cat_keys = array_keys($_POST['term_meta']);
        foreach ($cat_keys as $key) {
            if (isset($_POST['term_meta'][$key])) {
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        
        // Save the option array.
        update_option("taxonomy_post_tag_$t_id", $term_meta);
    }
}
add_action('edited_post_tag', 'meminz_save_post_tag_custom_meta', 10, 2);
add_action('create_post_tag', 'meminz_save_post_tag_custom_meta', 10, 2);
