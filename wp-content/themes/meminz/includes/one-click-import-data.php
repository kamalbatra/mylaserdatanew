<?php
//http://proteusthemes.github.io/one-click-demo-import/
//https://wordpress.org/plugins/one-click-demo-import/

function meminz_import_files() {
    return array(
        array(
            'import_file_name'             => esc_html__('Meminz theme - Full Demo Content (widgets included)','meminz' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'includes/demo_data_files/all-content.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'includes/demo_data_files/widgets.wie',
            'import_notice'                => esc_html__( 'Meminz theme - Full Demo Content (widgets included). After you import this demo, you will have to setup the front-page from Settings -> Reading screen and menu from Appearance -> Menus screen.', 'meminz' ),
        ),

    );
}
add_filter( 'pt-ocdi/import_files', 'meminz_import_files' );