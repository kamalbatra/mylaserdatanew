(function($) {
	'use strict';

		/*----------------------------------------------------*/
	/*	Animated Scroll To Anchor
	/*----------------------------------------------------*/
	/**
	 * Animated Scroll To Anchor v0.3
	 * Author: David Vogeleer
	 * http://www.individual11.com/
	 *
	 * THANKS:
	 *
	 * -> solution for setting the hash without jumping the page -> Lea Verou : http://leaverou.me/2011/05/change-url-hash-without-page-jump/
	 * -> Add stop  - Joe Mafia
	 * -> add some easing - Daniel Garcia
	 * -> added use strict, cleaned up some white space adn added conditional for anchors without hashtag -> Bret Morris, https://github.com/bretmorris
	 *
	 * TODO:
	 * -> Add hashchange support, but make it optional http://leaverou.me/2011/05/get-your-hash-the-bulletproof-way/
	 *
	 * Licensed under the MIT license.
	 * http://www.opensource.org/licenses/mit-license.php
	 * 
	 */
	 
	$.fn.scrollTo = function( options ) {

		var settings = {
			offset : -60,       //an integer allowing you to offset the position by a certain number of pixels. Can be negative or positive
			speed : 'slow',   //speed at which the scroll animates
			override : null,  //if you want to override the default way this plugin works, pass in the ID of the element you want to scroll through here
			easing : null //easing equation for the animation. Supports easing plugin as well (http://gsgd.co.uk/sandbox/jquery/easing/)
		};

		if (options) {
			if(options.override){
				//if they choose to override, make sure the hash is there
				options.override = (override('#') != -1)? options.override:'#' + options.override;
			}
			$.extend( settings, options );
		}

		return this.each(function(i, el){
			$(el).click(function(e){
				var idToLookAt;
				if ($(el).attr('href').match(/#/) !== null) {
					e.preventDefault();
					idToLookAt = (settings.override)? settings.override:$(el).attr('href');//see if the user is forcing an ID they want to use
					//if the browser supports it, we push the hash into the pushState for better linking later
					if(history.pushState){
						history.pushState(null, null, idToLookAt);
						$('html,body').stop().animate({scrollTop: $(idToLookAt).offset().top + settings.offset}, settings.speed, settings.easing);
					}else{
						//if the browser doesn't support pushState, we set the hash after the animation, which may cause issues if you use offset
						$('html,body').stop().animate({scrollTop: $(idToLookAt).offset().top + settings.offset}, settings.speed, settings.easing,function(e){
							//set the hash of the window for better linking
							window.location.hash = idToLookAt;
						});
					}
				}
			});
		});
	};
	  
	$('#GoToHome, #GoToFeatures, #GoToDesc, #GoToGallery, #GoToPricing, #GoToTestimoni, #GoToContact, .GoToto,.GoTo a' ).scrollTo({ speed: 1400 });
	
	//Bootstraping variable
	var headerWrapper		= parseInt($('.navbar').height());
	var offsetTolerance	= 60;
	
	//Detecting user's scroll
	$(window).scroll(function() {
	
		//Check scroll position
		var scrollPosition	= parseInt($(this).scrollTop());
		
		//Move trough each menu and check its position with scroll position then add selected class
		$('.navbar-nav li a').each(function() {

			var thisHref				= $(this).attr('href');
			if(/^#/.test(thisHref)&&thisHref.length > 1){
				var thisTruePosition	= parseInt($(thisHref).offset().top);
				var thisPosition 		= thisTruePosition - headerWrapper - offsetTolerance;
				
				if(scrollPosition >= thisPosition) {
					
					$('.selected').removeClass('selected');
					$('.navbar-nav li a[href="'+ thisHref +'"]').addClass('selected');
					
				}
			}
		});
		
		
		//If we're at the bottom of the page, move pointer to the last section
		var bottomPage	= parseInt($(document).height()) - parseInt($(window).height());
		
		if(scrollPosition == bottomPage || scrollPosition >= bottomPage) {
		
			$('.selected').removeClass('selected');
			$('navbar-nav li a:last').addClass('selected');
		}
	});
		



	$(".player").mb_YTPlayer();


	var a = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return a.Android() || a.BlackBerry() || a.iOS() || a.Opera() || a.Windows();
        }
    };
    var trueMobile = a.any();
    if (null == trueMobile) {
        if(meminz_obj.disable_animation != '1'){
			new WOW().init();
		}
    }
    if (trueMobile) {
    	if(meminz_obj.disable_mobile_animation != '1'){
			new WOW().init();
		}
    }

	

	//$('#nav').localScroll(800);

	//.parallax(xPosition, adjuster, inertia, outerHeight) options:
	//xPosition - Horizontal position of the element
	//adjuster - y position to start from
	//inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
	//outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
	$('.parallax').parallax("50%", 0, 0.1, true);
	$('#counter-wrapper,.counter_sec').parallax("50%", 1170, 0.1, true);
	$('#download,.download_sec').parallax("50%", 0, 0.1, true);
	$('#contact,.contact_sec').parallax("50%", 0, 0.1, true);

	var fls = jQuery(".flexslider");
    var optionsData = fls.data('options') ?  fls.data('options')  : {slideshow:true,animation:'slide',direction:'horizontal',smoothHeight:true,slideshowSpeed:7000,controlNav:true,directionNav:true};
    fls.flexslider({
        slideshow: optionsData.slideshow,
        animation: optionsData.animation,
        direction: optionsData.direction,
        smoothHeight: optionsData.smoothHeight,
        slideshowSpeed: optionsData.slideshowSpeed,

        directionNav: optionsData.directionNav,
        controlNav: optionsData.controlNav,
        
    });
	
	var imac = jQuery(".imac-device");
    var imac_optionsData = imac.data('options') ?  imac.data('options')  : {slideshow:true,animation:'slide',direction:'horizontal',smoothHeight:false,slideshowSpeed:7000,controlNav:false,directionNav:false};
    imac.flexslider({
        slideshow: imac_optionsData.slideshow,
        animation: imac_optionsData.animation,
        direction: imac_optionsData.direction,
        smoothHeight: imac_optionsData.smoothHeight,
        slideshowSpeed: imac_optionsData.slideshowSpeed,

        directionNav: imac_optionsData.directionNav,
        controlNav: imac_optionsData.controlNav,
        
    });

    jQuery('.appear').appear();
	var runOnce = true;
	jQuery(".stats").on("appear", function(data) {
		var counters = {};
		var i = 0;
		if (runOnce){
			jQuery('.counter-number').each(function(){
				//console.log(this.id);
				counters[this.id] = $(this).html();
				i++;
			});
			jQuery.each( counters, function( i, val ) {
				//console.log(i + ' - ' +val);
				jQuery({countNum: 0}).animate({countNum: val}, {
					duration: 3000,
					easing:'linear',
					step: function() {
						jQuery('#'+i).text(Math.floor(this.countNum));
					}
				});
			});
			runOnce = false;
		}
	});


	var testi_sl = jQuery(".owl-testimoni");
    //console.log(testi_sl.data('options'));
    var testi_sl_ops = testi_sl.data('options') ?  testi_sl.data('options') : {autoPlay:false,items:1,itemsCustom:''};
    if(!testi_sl_ops.itemsCustom) testi_sl_ops.itemsCustom = '320:1,768:1,992:3,1200:3';
    var itemsCustomArr = testi_sl_ops.itemsCustom.split(',');
    var itemsCustomVal = new Array();
    for (var i = 0; i < itemsCustomArr.length ; i++){
        itemsCustomVal[i] = itemsCustomArr[i].split(':');
    }
    //console.log(itemsCustomVal);
    testi_sl.owlCarousel({
        autoPlay : testi_sl_ops.autoPlay,
        stopOnHover : true,
        pagination : false,
        navigation:true,
        paginationSpeed : 1000,
        goToFirstSpeed : 2000,
        
        autoHeight : false,
        itemsCustom: itemsCustomVal,
        // itemsCustom : [
        //     [479, 1],
        //     [768, 1],
        //     [992, 3],
        //     [1200, 4]
        // ],
        items : testi_sl_ops.items,
    });



	/*
	/* totop setting */
	jQuery().UItoTop({ easingType: 'easeOutQuart' });
	/*prettyPhoto setting */
	$("a.zoom:first[data-pretty^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'pp_default',slideshow:3000, autoplay_slideshow: false});
	$("a.zoom:gt(0)[data-pretty^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
	/*
	tooltip
	=========================== */
	$('.tooltips').tooltip({
		selector: "a[data-toggle^=tooltip]"
	})	
	
	/*
	Close navbar for mobile device
	=========================== */
	$('.navbar-toggle').click(function(){
		$(".collapse").slideToggle();
		return false; 
	});
		
	$('.navbar-nav li a').click(function(){
		$(".collapse").slideToggle("normal");
	});
	
	/* Client logo hover
	=========================== */	
	$(".logo-hover").css({'opacity':'0','filter':'alpha(opacity=0)'});	
	$('.client-link').hover(function(){
				$(this).find('.logo-hover').stop().fadeTo(900, 1);
				$(this).find('.client-logo').stop().fadeTo(900, 0);
	}, function() {
				$(this).find('.logo-hover').stop().fadeTo(900, 0);
				$(this).find('.client-logo').stop().fadeTo(900, 1);
	});	
	$('#cart_panel .options_toggle').bind('click', function() {
		if($('#cart_panel').css('right') == '0px'){
			$('#cart_panel').stop(false, true).animate({right:'-230px'}, 400, 'easeOutExpo');
		}else {
			$('#cart_panel').stop(false, true).animate({right:'0px'}, 400, 'easeOutExpo');
		}	
	});

})(jQuery);

jQuery(window).load(function(){
	'use strict';
	jQuery(".scrolltop").jCarouselLite({
		vertical: true,
		hoverPause:true,
		visible: 1,
		auto:6500,
		speed:1000
	});
});