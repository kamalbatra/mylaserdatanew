(function($) {
	'use strict';

      	var fls = jQuery(".flexslider");
	    var optionsData = fls.data('options') ?  fls.data('options')  : {slideshow:true,animation:'slide',direction:'horizontal',smoothHeight:true,slideshowSpeed:7000,controlNav:true,directionNav:true};
	    fls.flexslider({
	        slideshow: optionsData.slideshow,
	        animation: optionsData.animation,
	        direction: optionsData.direction,
	        smoothHeight: optionsData.smoothHeight,
	        slideshowSpeed: optionsData.slideshowSpeed,

	        directionNav: optionsData.directionNav,
	        controlNav: optionsData.controlNav,
	        
	    });
		
		var imac = jQuery(".imac-device");
	    var imac_optionsData = imac.data('options') ?  imac.data('options')  : {slideshow:true,animation:'slide',direction:'horizontal',smoothHeight:false,slideshowSpeed:7000,controlNav:false,directionNav:false};
	    imac.flexslider({
	        slideshow: imac_optionsData.slideshow,
	        animation: imac_optionsData.animation,
	        direction: imac_optionsData.direction,
	        smoothHeight: imac_optionsData.smoothHeight,
	        slideshowSpeed: imac_optionsData.slideshowSpeed,

	        directionNav: imac_optionsData.directionNav,
	        controlNav: imac_optionsData.controlNav,
	        
	    });
})(jQuery);