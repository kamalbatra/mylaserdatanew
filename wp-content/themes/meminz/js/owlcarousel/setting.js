(function($) {
	'use strict';
    var testi_sl = jQuery(".owl-testimoni");
    //console.log(testi_sl.data('options'));
    var testi_sl_ops = testi_sl.data('options') ?  testi_sl.data('options') : {autoPlay:false,items:1,itemsCustom:''};
    if(!testi_sl_ops.itemsCustom) testi_sl_ops.itemsCustom = '479:1,768:1,992:3,1200:3';
    var itemsCustomArr = testi_sl_ops.itemsCustom.split(',');
    var itemsCustomVal = new Array();
    for (var i = 0; i < itemsCustomArr.length ; i++){
        itemsCustomVal[i] = itemsCustomArr[i].split(':');
    }
    //console.log(itemsCustomVal);
    testi_sl.owlCarousel({
        autoPlay : testi_sl_ops.autoPlay,
        stopOnHover : true,
        pagination : false,
        navigation:true,
        paginationSpeed : 1000,
        goToFirstSpeed : 2000,
        
        autoHeight : false,
        itemsCustom: itemsCustomVal,
        // itemsCustom : [
        //     [479, 1],
        //     [768, 1],
        //     [992, 3],
        //     [1200, 4]
        // ],
        items : testi_sl_ops.items,
    });
})(jQuery);