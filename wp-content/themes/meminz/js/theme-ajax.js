jQuery(document).ready(function($) {
	// for user login form
	$("form#rsUserLogin").submit(function(){
		var submit = $(".userLogin #logsubmit"),
			preloader = $(".userLogin #logpreloader"),
			message	= $(".userLogin #logmessage"),
			contents = {
				action: 		'user_login',
				nonce: 			this.rs_user_login_nonce.value,
				log:			this.log.value,
				pwd:			this.pwd.value,
				remember:		this.remember.value,
				redirection_url:	this.redirection_url.value
			};
		
		// disable button onsubmit to avoid double submision
		submit.attr("disabled", "disabled").addClass('disabled');
		
		// Display our pre-loading
		preloader.css({'display':'inline-block'});
		
		// on my previous tutorial it just simply returned HTML but this time I decided to use JSON type so we can check for data success and redirection url.
		$.post( theme_ajax.url, contents, function( data ){
			submit.removeAttr("disabled").removeClass('disabled');
			
			// hide pre-loader
			preloader.css({'display':'none'});
			
			//console.log(data);
			// check response data
			if( 1 == data.success ) {
				// redirect to home page
				window.location = data.redirection_url;
			} else {
				// display return data
				message.html( '<p class="error">' + data + '</p>' );
			}
			
		}, 'json');
		
		return false;
	});
	
	// for user registration form
	$("form#rsUserRegistration").submit(function(){
		var submit = $(".userRegistration #regsubmit"),
			preloader = $(".userRegistration #regpreloader"),
			message	= $(".userRegistration #regmessage"),
			contents = {
				action: 	'user_registration',
				nonce: 		this.rs_user_registration_nonce.value,
				log:		this.log.value,
				pwd:		this.pwd.value,
			};
		
		// disable button onsubmit to avoid double submision
		submit.attr("disabled", "disabled").addClass('disabled');
		
		// Display our pre-loading
		preloader.css({'display':'inline-block'});
		
		$.post( theme_ajax.url, contents, function( data ){
			submit.removeAttr("disabled").removeClass('disabled');
			
			// hide pre-loader
			preloader.css({'display':'none'});

			//console.log(data);
			
			// check response data
			if( '1' === data ) {
				// redirect to home page
				window.location = theme_ajax.site_url;
			} else {
				// display return data
				message.html( data );
			}
		});
		
		return false;
	});

	$('#loginAjaxModal').on('show.bs.modal', function (event) {
	  	var button = $(event.relatedTarget) // Button that triggered the modal
	  	var activetab = button.data('activetab') // Extract info from data-* attributes
	  	var modal = $(this)

	  	modal.find(activetab).tab('show');
	  
	})

});
