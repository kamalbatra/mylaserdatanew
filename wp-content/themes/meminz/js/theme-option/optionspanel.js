jQuery(document).ready(function($) {
	
	$('#panel .options_toggle').bind('click', function() {
		if($('#panel').css('left') == '0px'){
			$('#panel').stop(false, true).animate({left:'-230px'}, 400, 'easeOutExpo');
		}else {
			$('#panel').stop(false, true).animate({left:'0px'}, 400, 'easeOutExpo');
		}	
	});

	
	$('#accent_color').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onChange: function (hsb, hex, rgb) {
			$('#accent_color').val(hex);
			$('#accent_color').css('backgroundColor', '#' + hex);
			accentColorUpdate(hex);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});

	
function accentColorUpdate(hex){

	hex = '#'+hex;

	$('#custom_styles').html('<style>'+
		'a, a:focus, a:hover, a:active, .navbar-default .navbar-nav li a:hover, .navbar-default .navbar-nav li a.selected, .navbar-default .navbar-nav .active a, .navbar-default .navbar-nav .dropdown.active a, .navbar-default .navbar-nav .active a:hover, .navbar-default .navbar-nav .dropdown.active a:hover, .navbar-default .navbar-nav .active a:focus, .navbar-default .navbar-nav .dropdown.active a:focus, .icon-counter:hover, .social-network a:hover, .social-network a:focus, .social-network a:active, .pe-feature, .accordion-heading a:hover i, .counter-number, .pricing-head.popular .pricing-price, .validation, .cat li a:hover, .recent li h6 a:hover, .pagination > li > a, .pagination > li > a:hover, .media h4.media-heading a:hover,.wpb_accordion_header a:hover i,.wpt_widget_content .tab-content li a:hover,.wpt-tabs > li > a,.wpt-tabs > li > a:hover,.widget_categories > ul li a:hover,.meminz_main-nav > li.current-menu-parent > a,.meminz_main-nav > .open > a.dropdown-toggle,.meminz_main-nav > .open > a.dropdown-toggle:hover,.meminz_main-nav > .open > a.dropdown-toggle:focus { color:'+ hex +'; }' +
		'.btn-primary, .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .open > .dropdown-toggle.btn-primary, .subscribe-button, .navbar-default .navbar-toggle:hover .icon-bar, .feature-box:hover .pe-feature, .accordion-heading a i, .accordion-heading a:hover, .flex-direction-nav .flex-next:hover, .flex-direction-nav .flex-prev:hover, .flex-control-paging li a:hover, .flex-control-paging li a.flex-active, .pricing-head.popular, .owl-theme .owl-controls .owl-buttons div.owl-prev:hover, .owl-theme .owl-controls .owl-buttons div.owl-next:hover, #toTopHover, .log-tabs li a, .log-tabs li a:hover, .log-tabs li a:focus, .log-tabs li a:active, .options_toggle_holder, .tag li a:hover,.wpb_accordion_header a i { background-color:'+ hex +';}' +
		'.tagcloud a:hover,.wpb_accordion_header a:hover { background-color:'+ hex +'!important;}'+
		'.btn-primary, .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary, .navbar-default .navbar-nav li a:hover, .navbar-default .navbar-nav li a.selected, .navbar-default .navbar-nav .active a, .navbar-default .navbar-nav .dropdown.active a, .navbar-default .navbar-nav .active a:hover, .navbar-default .navbar-nav .dropdown.active a:hover, .navbar-default .navbar-nav .active a:focus, .navbar-default .navbar-nav .dropdown.active a:focus, .subscribe-button, .testimoni-avatar:hover, .icon-counter:hover, .form-control:focus, ul.listForm li .form-control:focus, .navbar-default .navbar-toggle:hover, .pe-feature, .accordion-heading a i, .accordion-heading a:hover, .tag li a:hover, .tagcloud a:hover,.wpb_accordion_header a i,.meminz_main-nav > li.current-menu-parent > a,.meminz_main-nav > .open > a.dropdown-toggle,.meminz_main-nav > .open > a.dropdown-toggle:hover,.meminz_main-nav > .open > a.dropdown-toggle:focus { border-color:'+ hex +';}'+
		//'..wpt-tabs > li > a { border-color:'+ hex +'!important;}'+
		'</style>');
}

function bodybgColorUpdate(hex){
	$('body').css('background', '#'+hex);
}
	
});