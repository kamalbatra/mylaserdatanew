<div class="login-register">
	<ul class="nav nav-tabs log-tabs" role="tablist">
		<li role="presentation"><a href="#Login" id="loginHref" role="tab" data-toggle="tab"><?php _e('Login','meminz');?></a></li>
		<li role="presentation"><a href="#register" id="registerHref" role="tab" data-toggle="tab"><?php _e('Register','meminz');?></a></li>
	</ul>
	<div class="tab-content log-tabs-containt">
		<div role="tabpanel" class="tab-pane userLogin" id="Login">
			<?php
			// check if the user already login
			if( is_user_logged_in() ) { ?>
				<?php echo sprintf(__("<p>It seems that you're already loggedin, <a href=\"%s\">logout</a> to login with different account.</p>",'meminz'), wp_logout_url( get_permalink() ));?>
			<?php } else { ?>
				<!--message wrapper-->
				<div id="logmessage" class="alert-box"></div>
				
				<form class="form-horizontal" method="post" id="rsUserLogin">
					<?php
						// this prevent automated script for unwanted spam
						if ( function_exists( 'wp_nonce_field' ) ) 
							wp_nonce_field( 'rs_user_login_action', 'rs_user_login_nonce' );
					?>
					<div class="form-group">
						<div class="col-lg-12 text-left">
							<input type="text" class="form-control form-block" name="log" id="loglog" placeholder="<?php _e('Username','meminz');?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12 text-left">
							<input type="password" class="form-control form-block" name="pwd" id="logpwd" placeholder="<?php _e('Password','meminz');?>">
						</div>
					</div>
					<div class="checkbox text-left">
					    <label><input type="checkbox" name="remember" id="remember" value="true"> <?php _e('Remember Me','meminz');?></label>
					    <br>
					    <br>
					</div>

					<div class="form-group">
						<div class="col-lg-12 text-center">
							<button type="submit" id="logsubmit" class="btn btn-primary btn-lg"><?php _e('Sign in','meminz');?></button> 
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ajax-loader.gif" id="logpreloader" alt="Preloader" />
						</div>									
					</div>
						
					

					<input type="hidden" name="redirection_url" id="redirection_url" value="<?php echo get_permalink( get_the_ID() ); ?>" />

				</form>
			<?php } ?>
		</div>
		<div role="tabpanel" class="tab-pane userRegistration" id="register">
			<?php
			// check if the user already login
			if( is_user_logged_in() ) { ?>
				<?php echo sprintf(__("<p>It seems that you're already loggedin, <a href=\"%s\">logout</a> to register new account.</p>",'meminz'), wp_logout_url( get_permalink() ));?>
			<?php } else { ?>
				<?php _e("<h2>HELLO,</h2><p>Don't have an account yet? Create one now.</p>","meminz");?>
				<!--message wrapper-->
				<div id="regmessage" class="alert-box"></div>
				
				<form class="form-horizontal" method="post" id="rsUserRegistration">
					<?php
						// to make our script safe, it's a best practice to use nonce on our form to check things out
						if ( function_exists( 'wp_nonce_field' ) ) 
							wp_nonce_field( 'rs_user_registration_action', 'rs_user_registration_nonce' );
					?>
					<div class="form-group">
						<div class="col-lg-12 text-left">
							<input type="text" autocomplete="off" class="form-control form-block" name="log" id="reglog" placeholder="<?php _e('Username','meminz');?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12 text-left">
							<input type="password" autocomplete="off" class="form-control form-block" name="pwd" id="regpwd" placeholder="<?php _e('Password','meminz');?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-12 text-center">
							<button type="submit" id="regsubmit" class="btn btn-primary btn-lg"><?php _e('Register','meminz');?></button> 
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ajax-loader.gif" id="regpreloader" alt="Preloader" />
						</div>									
					</div>


				</form>
			<?php } ?>
		</div>
	</div>
</div>