 <?php

/**
 * Template Name: Page Full Content Width
 *
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

global $wp_query;

get_header(); ?>

<!-- Start page header -->
<div class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php the_breadcrumb(); ?>
			</div>	
		</div>	
	</div>		
</div>
<!-- End page header -->
<div id="inner-page">
	<div class="container">
		<div class="row">

			<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 cth-pagecontent'); ?>>

				<?php while(have_posts()) : the_post(); ?>
					<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
					<?php the_content(); ?>
					<?php wp_link_pages(); ?>
					<?php edit_post_link(__('Edit','meminz') ); ?>
					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>
				<?php endwhile; ?>

			</div>
			
		</div>
	</div>
</div>

<?php get_footer(); ?>