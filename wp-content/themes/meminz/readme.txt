=== Meminz ===
Contributors: CTHthemes
Tags: custom-background, custom-menu, editor-style, featured-images, post-formats, sticky-post, theme-options, translation-ready
Requires at least: 4.5
Tested up to: 4.8
Stable tag: 4.8
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==
Meminz is download software landing page with modern design, designed for many kind software download and app website.

* Responsive Layout
* Custom Colors
* Custom Header
* Social Links
* Menu Description
* Post Formats
* The GNU General Public License version 3.0

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's ZIP file. Click Install Now.
3. Click Activate to use your new theme right away.
