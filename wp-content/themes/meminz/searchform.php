<?php

/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label><span class="screen-reader-text"><?php echo _x( 'Search label', 'label','meminz') ?></span></label>
		<div class="widget-item bg-widget clearfix">
			<div class="input-group">
				<input type="search" class="form-control" placeholder="<?php echo esc_attr_x( 'Search Blog', 'placeholder','meminz' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'search blog', 'title','meminz' ) ?>"/>
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary" value="<?php echo esc_attr_x( 'Search', 'submit button','meminz' ) ?>">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</div>
</form>