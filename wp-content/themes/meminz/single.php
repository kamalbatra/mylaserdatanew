<?php
/**
 * @package Meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 06-01-2014
 *
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

global $theme_options;
if(get_post_meta(get_the_ID(), '_cmb_single_layout', true)){
	$sideBar = get_post_meta(get_the_ID(), '_cmb_single_layout', true);
}else{
	$sideBar = $theme_options['blog_layout'];
}
get_header(); ?>

	<!-- Start page header -->
	<div class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php the_breadcrumb(); ?>
				</div>			
			</div>	
		</div>		
	</div>
	<!-- End page header -->
	
	<!-- Start inner page -->
	<div id="inner-page">
		<div class="container">
			<div class="row">
				<?php if($sideBar ==="left_sidebar"):?>
					<div class="col-md-3 sidebar-left">
						<aside>
							<?php get_sidebar( );?>
						</aside>
					</div>
				<?php endif;?>
				<?php if($sideBar ==="fullwidth"):?>
				<div class="col-md-12">
				<?php else:?>
				<div class="col-md-9">
				<?php endif;?>
					<?php while(have_posts()) : the_post(); ?>
						<article <?php post_class('cth-single styleSlider' );?>>
						<?php $postformat = get_post_format( );
							if($postformat === 'quote'){
								//echo '<h4>'.get_the_title( ).'</h4>';
							}else{ ?>
								<?php if(get_post_format()=='gallery'){ 
									$gallery = get_post_gallery( get_the_ID(), false );

									if(isset($gallery['ids'])) : ?>
									
										<div class="flexslider">
											<ul class="slides">
												<?php
													$gallery_ids = $gallery['ids'];
													$img_ids = explode(",",$gallery_ids);
													
													foreach( $img_ids AS $img_id ){
												?>
													<li><?php echo wp_get_attachment_image( $img_id, 'full', '', array('class'=>'img-responsive') );?></li>
												<?php } ?>
											</ul>
										</div>  
									
									<?php endif; ?>
								<?php }elseif(get_post_meta(get_the_ID(), '_cmb_embed_video', true)!=""){ ?>
								<div class="video-container">
									<?php echo wp_oembed_get(esc_url(get_post_meta(get_the_ID(), '_cmb_embed_video', true) )); ?>
								</div>
								<?php }elseif(has_post_thumbnail( )){ ?>
									<?php the_post_thumbnail('full',array('class'=>'img-responsive thumbnail')); ?>
								<?php } ?>

								<h4><?php the_title( );?></h4>
								<div class="meta-wrapper">
									<ul class="meta-post">
										<?php if($theme_options['author_checkbox']==='1') :?>
						            		<li><label><?php _e('Author: ','meminz');?></label> <?php the_author_posts_link( );?></li>
						            	<?php endif;?>
										<?php if($theme_options['date_checkbox']==='1') :?>
						            		<li><label><?php _e('Date: ','meminz');?></label> <a href="<?php echo get_day_link((int)get_the_time('Y' ), (int)get_the_time('m' ), (int)get_the_time('d' )); ?>"><?php the_date();?></a></li>
						            	<?php endif;?>
						            	<?php if($theme_options['cats_checkbox']==='1') :?>
											<li><label><?php _e('Categories: ','meminz');?></label> <?php echo get_the_category_list(', ');?></li>
										<?php endif;?>
						            	<?php if($theme_options['tag_checkbox']==='1') :?>
											<li><label><?php _e('Tags: ','meminz');?></label> <?php the_tags('');?></li>
										<?php endif;?>
										<?php if($theme_options['comment_checkbox']==='1') :?>
											<li><label><?php _e('Comment: ','meminz');?></label> <?php comments_popup_link(__('0', 'meminz'), __('1', 'meminz'), __('%', 'meminz')); ?></li>
							    		<?php endif;?>
									</ul>
								</div>
							<?php } ?>
							
							<?php the_content( );?>
						
						
							<?php
								wp_link_pages( array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'meminz' ) . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
									) );
								?>
							<?php meminz_post_nav();?>
						</article>


					<?php endwhile;?>
					<div class="clearfix"></div>
					<?php comments_template(); ?>					
				</div>
				<?php if($sideBar==="right_sidebar"):?>
				<div class="col-md-3">
					<aside>
						<?php get_sidebar( );?>
					</aside>
				</div>
				<?php endif;?>

			</div>
		</div>
	</div>
	<!-- End inner page -->

<?php get_footer(); ?>