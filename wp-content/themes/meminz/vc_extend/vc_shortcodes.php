<?php

/**
 * @package meminz - App Landing Page Wordpress Theme
 * @author Cththemes - http://themeforest.net/user/cththemes
 * @date: 25-10-2014
 *
 * @copyright  Copyright ( C ) 2014 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
 
 //video background
if(function_exists('vc_map')){
    vc_map( array(
        "name"      => __("Background Video", 'meminz'),
        "description" => __("Choose video background",'meminz'),
        "base"      => "video_bg",
        "class"     => "",
        "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
        "category"  => 'Meminz',
        "params"    => array(
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "meminz"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
        ),
        array(
            "type"      => "textfield",
            "holder"    => "div",
            "class"     => "",
            "heading"   => __("Title", 'meminz'),
            "param_name"=> "video_title",
            "value"     => "My Video",
            "description" => __("Input title of video.", 'meminz')
        ),
        array(
            "type"      => "textfield",
            "holder"    => "div",
            "class"     => "",
            "heading"   => __("Video ID", 'meminz'),
            "param_name"=> "video_id",
            "value"     => "e8DFN3m8XGQ",
            "description" => __("Input ID of video you want use background.</br>Example: http://www.youtube.com/watch?v=<b>e8DFN3m8XGQ</b>", 'meminz')
        ),
        array(
            "type"      => "dropdown",
            "class"     => "",
            "heading"   => __("Loop", 'meminz'),
            "param_name"=> "loop",
            "value"       => array(
                'Yes'   => 'true',
                'No'   => 'false',
            ),
            "description" => __("Choose for enable", 'meminz')
        ),
        array(
            "type"      => "dropdown",
            "class"     => "",
            "heading"   => __("Mute", 'meminz'),
            "param_name"=> "mute",
            "value"       => array(
                'Yes'   => 'true',
                'No'   => 'false',
            ),
            "description" => __("Choose for enable", 'meminz')
        ),
        array(
            "type"      => "textfield",
            "class"     => "",
            "heading"   => __("Opacity", 'meminz'),
            "param_name"=> "opacity",
            "value"     => "1",
            "description" => __("Choose number from 0 to 1", 'meminz')
        ),
        array(
            "type"      => "textfield",
            "class"     => "",
            "heading"   => __("Containment", 'meminz'),
            "param_name"=> "containment",
            "value"     => ".video-wrapper",
            "description" => __("The CSS selector of the DOM element where you want the video background; if not specified it takes the 'body'; if set to 'self' the player will be instanced on that element.", 'meminz')
        ),
        array(
            "type"      => "textfield",
            "class"     => "",
            "heading"   => __("Quality", 'meminz'),
            "param_name"=> "quanlity",
            "value"     => "default",
            "description" => __("'default' or 'small', 'medium', 'large', 'hd720', 'hd1080', 'highres'.", 'meminz')
        ),
    )));

    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_Video_Bg extends WPBakeryShortCode {}
    }

    // home_ticker_item

   vc_map( array(
   "name"      => __("Home Ticker", 'meminz'),
   "description" => __("Home text ticker with two button for popup and link",'meminz'),
   "base"      => "home_ticker",
   "class"     => "",
   "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
   "category"  => 'Meminz',
   "params"    => array(
      array(
          "type" => "textfield",
          "heading" => __("Extra class name", "meminz"),
          "param_name" => "el_class",
          "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
        ),
      array(
         "type"      => "textfield",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Title", 'meminz'),
         "param_name"=> "title",
         "value"     => "We are building software to help ",
         "description" => __("Input title for homepage", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Sub Title", 'meminz'),
         "param_name"=> "subtitle",
         "value"     => "people manage our business",
         "description" => __("Input sub title for homepage", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Link Video for button 1", 'meminz'),
         "param_name"=> "link_video",
         "value"     => "https://www.youtube.com/watch?v=RoAPTdvgAJg",
         "description" => __("Add video description for your website", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Text for button 1", 'meminz'),
         "param_name"=> "text_btn1",
         "value"     => "Take a video tour",
         "description" => __("Text display button 1", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Text for button 2", 'meminz'),
         "param_name"=> "text_btn2",
         "value"     => "Register for free",
         "description" => __("Text display button 2", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Link for button 2", 'meminz'),
         "param_name"=> "link_btn2",
         "value"     => "#",
      ),
      array(
          "type"      => "attach_images",
          "holder"    => "div",
          "class"     => "ajax-vc-img",
          "heading"   => __("Select image", 'meminz'),
          "param_name"=> "images1",
          "description" => __("Home slider images 1", 'meminz')
        ),
    ),
    'admin_enqueue_js' => get_template_directory_uri() . "/vc_extend/vc_js_elements.js",
    'js_view'=> 'MeminzImagesView',
 ));
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_Home_Ticker extends WPBakeryShortCode {}
    }
//home_slider
  //Register "container" content element. It will hold all your inner (child) content elements
  vc_map( array(
      "name" => __("Home Imac Slider", 'meminz'),
      "base" => "home_slider",
      "category"  => 'Meminz',
      "as_parent" => array('only' => 'home_slider_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
      "content_element" => true,
      "show_settings_on_create" => false,
      "class"=>'cth_home_slider',
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png", // Simply pass url to your icon here
      //'admin_enqueue_css' => array( get_template_directory_uri() . '/vc_extend/custom.css' ),
      "params" => array(
            // add params same as with any other content element
            array(
                "type" => "textfield",
                "heading" => __("Extra class name", "meminz"),
                "param_name" => "el_class",
                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
            ),
            array(
                "type" => "textfield",
                "heading" => __("id", "meminz"),
                "param_name" => "el_id",
                "description" => __("Slider id", 'meminz')
            ),
            array(
                "type"      => "attach_image",
                "holder"    => "div",
                "class"     => "ajax-vc-img",
                "heading"   => __("Slider holder image", 'meminz'),
                "param_name"=> "attachid",
                "description" => __("Home slider holder image", 'meminz')
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Auto Play", "meminz"),
                "param_name" => "slideshow",
                "value" => array(   
                                __('Yes', 'meminz') => 'true',  
                                __('No', 'meminz') => 'false',                                                                                
                            ),
                "description" => __("Animate slider automatically?", "meminz")
            ),
            array(
                "type" => "dropdown",
               // "class"=>"",
                "heading" => __('Animation Type', 'meminz'),
                "param_name" => "animation",
                "value" => array(   
                                __('Fade', 'meminz') => 'fade',  
                                __('Slide', 'meminz') => 'slide',                                                                                
                            ),
                "description" => __("Select your animation type", "meminz"), 
            ),
            array(
                "type" => "dropdown",
                //"class"=>"",
                "heading" => __('Direction', 'meminz'),
                "param_name" => "direction",
                "value" => array(   
                                __('Horizontal', 'meminz') => 'horizontal',  
                                __('Vertical', 'meminz') => 'vertical',                                                                                
                            ),
                "description" => __("Select the sliding direction", "meminz"), 
            ),
            array(
                "type" => "dropdown",
                "heading" => __("SmoothHeight", "meminz"),
                "param_name" => "smoothheight",
                "value" => array(   
                                __('No', 'meminz') => 'false',
                                __('Yes', 'meminz') => 'true',  
                                                                                                                
                            ),
                "description" => __("Allow height of the slider to animate smoothly in horizontal mode", "meminz")
            ),
            array(
                "type" => "textfield",
                "heading" => __("Slide Speed", "meminz"),
                "param_name" => "slideshowspeed",
                "description" => __("Set the speed of the slideshow cycling, in milliseconds", "meminz"),
                "value" => "7000"
            ),
            array(
                "type" => "dropdown",
                "heading" => __("controlNav", "meminz"),
                "param_name" => "controlnav",
                "value" => array(   
                                
                                __('Yes', 'meminz') => 'true',
                                __('No', 'meminz') => 'false',  
                                                                                                                
                            ),
                "description" => __("Create navigation for paging control of each slide? Note: Leave true for manualControls usage", "meminz")
            ),
            array(
                "type" => "dropdown",
                "heading" => __("directionNav", "meminz"),
                "param_name" => "directionnav",
                "value" => array(   
                                
                                __('Yes', 'meminz') => 'true',
                                __('No', 'meminz') => 'false',  
                                                                                                                
                            ),
                "description" => __("Boolean: Create navigation for previous/next navigation?", "meminz")
            ),
      ),
      "js_view" => 'VcColumnView',

  ) );
  vc_map( array(
      "name" => __("Slide Item", 'meminz'),
      "base" => "home_slider_item",
      "content_element" => true,
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
      "as_child" => array('only' => 'home_slider'), // Use only|except attributes to limit parent (separate multiple values with comma)
      "params" => array(
        //  add params same as with any other content element
        array(
              "type" => "textfield",
              "heading" => __("Extra class name", "meminz"),
              "param_name" => "el_class",
              "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
          ),
          array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Slide Link", 'meminz'),
         "param_name"=> "link_slider",
         "value"     => "#",
      ),
      array(
          "type"      => "attach_image",
          "holder"    => "div",
          "class"     => "ajax-vc-img",
          "heading"   => __("Slide Image", 'meminz'),
          "param_name"=> "images_slider",
          "description" => __("Slide Image", 'meminz')
        ),
        array(
            "type"      => "textarea_html",
            "holder"    => "div",
            "class"     => "",
            "heading"   => __("Slider Content", 'meminz'),
            "param_name"=> "content",
            "value"     => '',
            
        ),
          
    ),
    'admin_enqueue_js' => get_template_directory_uri() . "/vc_extend/vc_js_elements.js",
    'js_view'=> 'MeminzImagesView',
  ) );

  //Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
  if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
      class WPBakeryShortCode_Home_Slider extends WPBakeryShortCodesContainer {
      }
  }
  if ( class_exists( 'WPBakeryShortCode' ) ) {
      class WPBakeryShortCode_Home_Slider_Item extends WPBakeryShortCode {
      }
  }


// featurebox_item
   vc_map( array(
   "name"      => __("Feature Box", 'meminz'),
   "description" => __("Feature Box with icon",'meminz'),
   "base"      => "featurebox",
   "class"     => "",
   "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
   "category"  => 'Meminz',
   "params"    => array(
      array(
          "type" => "textfield",
          "heading" => __("Extra class name", "meminz"),
          "param_name" => "el_class",
          "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
        ),
      array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Icon Name", 'meminz'),
         "param_name"=> "icon",
         "value"     => "flash",
         "description" => __("Search icon : <a href='http://themes-pixeden.com/font-demos/7-stroke/' target='_blank'>PE 7 STROKE</a>", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Title", 'meminz'),
         "param_name"=> "title",
         "value"     => "features 1",
         "description" => __("Title display in featurebox.", 'meminz')
      ),
      array(
         "type"      => "textarea",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Content", 'meminz'),
         "param_name"=> "content",
         "value"     => "Praesent faucibus nisl sit amet<br>nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
         "description" => __("Content display in featurebox.", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Link", 'meminz'),
         "param_name"=> "link",
         "value"     => "#",
         "description" => __("Link address to additional info.", 'meminz')
      ),
    )));
    
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_Featurebox extends WPBakeryShortCode {}
    }

//counter_item
   vc_map( array(
   "name"      => __("Counter Item", 'meminz'),
   "base"      => "counter_num",
   "class"     => "",
   "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
   "category"  => 'Meminz',
   "params"    => array(
        array(
          "type"      => "textfield",
          "holder"    => "div",
          "class"     => "",
          "heading"   => __("Element ID", 'meminz'),
          "param_name"=> "id",
          "value"     => "",
          // "description" => __("Counter ID.", 'meminz')
        ),
        array(
          "type"      => "textfield",
          "holder"    => "div",
          "class"     => "",
          "heading"   => __("Title", 'meminz'),
          "param_name"=> "title",
          "value"     => "Downloader",
          "description" => __("Counter title", 'meminz')
        ),
        array(
          "type"      => "textfield",
          "holder"    => "div",
          "class"     => "",
          "heading"   => __("Counter Number", 'meminz'),
          "param_name"=> "number",
          "value"     => "1200",
          "description" => __("Counter number.", 'meminz')
        ),
        array(
         "type"      => "textarea",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Content", 'meminz'),
         "param_name"=> "content",
         "value"     => "",
         "description" => __("Content display in counter.", 'meminz')
      ),
        
    )));
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_Counter_Num extends WPBakeryShortCode {}
    }

//Screenshot
  //Register "container" content element. It will hold all your inner (child) content elements
  vc_map( array(
      "name" => __("Screenshot Slider", 'meminz'),
      "base" => "screenshot_slider",
      "category"  => 'Meminz',
      "as_parent" => array('only' => 'screenshot_slider_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
      "content_element" => true,
      "show_settings_on_create" => false,
      "class"=>'cth_screenshot_slider',
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png", // Simply pass url to your icon here
      //'admin_enqueue_css' => array( get_template_directory_uri() . '/vc_extend/custom.css' ),
      "params" => array(
          // add params same as with any other content element
          array(
              "type" => "textfield",
              "heading" => __("Extra class name", "meminz"),
              "param_name" => "el_class",
              "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
          ),
          array(
              "type" => "textfield",
              "heading" => __("id", "meminz"),
              "param_name" => "el_id",
              "description" => __("Slider id", 'meminz')
          ),
          array(
          "type"      => "attach_image",
          "holder"    => "div",
          "class"     => "",
          "heading"   => __("Slider holder image", 'meminz'),
          "param_name"=> "attachid",
          "description" => __("Home slider holder image", 'meminz')
        ),
          array(
                "type" => "dropdown",
                "heading" => __("Auto Play", "meminz"),
                "param_name" => "slideshow",
                "value" => array(   
                                __('Yes', 'meminz') => 'true',  
                                __('No', 'meminz') => 'false',                                                                                
                            ),
                "description" => __("Animate slider automatically?", "meminz")
            ),
            array(
                "type" => "dropdown",
               // "class"=>"",
                "heading" => __('Animation Type', 'meminz'),
                "param_name" => "animation",
                "value" => array(   
                                __('Fade', 'meminz') => 'fade',  
                                __('Slide', 'meminz') => 'slide',                                                                                
                            ),
                "description" => __("Select your animation type", "meminz"), 
            ),

            array(
                "type" => "dropdown",
                //"class"=>"",
                "heading" => __('Direction', 'meminz'),
                "param_name" => "direction",
                "value" => array(   
                                __('Horizontal', 'meminz') => 'horizontal',  
                                __('Vertical', 'meminz') => 'vertical',                                                                                
                            ),
                "description" => __("Select the sliding direction", "meminz"), 
            ),
            array(
                "type" => "dropdown",
                "heading" => __("SmoothHeight", "meminz"),
                "param_name" => "smoothheight",
                "value" => array(   
                                __('No', 'meminz') => 'false',
                                __('Yes', 'meminz') => 'true',  
                                                                                                                
                            ),
                "description" => __("Allow height of the slider to animate smoothly in horizontal mode", "meminz")
            ),
            array(
                "type" => "textfield",
                "heading" => __("Slide Speed", "meminz"),
                "param_name" => "slideshowspeed",
                "description" => __("Set the speed of the slideshow cycling, in milliseconds", "meminz"),
                "value" => "7000"
            ),
            array(
                "type" => "dropdown",
                "heading" => __("controlNav", "meminz"),
                "param_name" => "controlnav",
                "value" => array(   
                                
                                __('Yes', 'meminz') => 'true',
                                __('No', 'meminz') => 'false',  
                                                                                                                
                            ),
                "description" => __("Create navigation for paging control of each slide? Note: Leave true for manualControls usage", "meminz")
            ),
            array(
                "type" => "dropdown",
                "heading" => __("directionNav", "meminz"),
                "param_name" => "directionnav",
                "value" => array(   
                                
                                __('Yes', 'meminz') => 'true',
                                __('No', 'meminz') => 'false',  
                                                                                                                
                            ),
                "description" => __("Boolean: Create navigation for previous/next navigation?", "meminz")
            ),

      ),
      "js_view" => 'VcColumnView'
  ) );
  vc_map( array(
      "name" => __("Slide Item", 'meminz'),
      "base" => "screenshot_slider_item",
      "content_element" => true,
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
      "as_child" => array('only' => 'screenshot_slider'), // Use only|except attributes to limit parent (separate multiple values with comma)
      //"icon" => get_template_directory_uri() . "/vc_extend/swiper-icon.png",
      "params" => array(
        //  add params same as with any other content element
        array(
              "type" => "textfield",
              "heading" => __("Extra class name", "meminz"),
              "param_name" => "el_class",
              "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
          ),
          array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Slide Link", 'meminz'),
         "param_name"=> "link_slider",
         "value"     => "#",
         
      ),
      array(
          "type"      => "attach_image",
          "holder"    => "div",
          "class"     => "ajax-vc-img",
          "heading"   => __("Slide Image", 'meminz'),
          "param_name"=> "images_slider",
          "description" => __("Slide Image", 'meminz')
        ),

      array(
            "type"      => "textarea_html",
            "holder"    => "div",
            "class"     => "",
            "heading"   => __("Slider Content", 'meminz'),
            "param_name"=> "content",
            "value"     => '',
            
        ),
      
          
        ),
        'admin_enqueue_js' => get_template_directory_uri() . "/vc_extend/vc_js_elements.js",
        'js_view'=> 'MeminzImagesView',
  ) );

  //Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
  if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
      class WPBakeryShortCode_Screenshot_Slider extends WPBakeryShortCodesContainer {
      }
  }
  if ( class_exists( 'WPBakeryShortCode' ) ) {
      class WPBakeryShortCode_Screenshot_Slider_Item extends WPBakeryShortCode {
      }
  }


//pricing
   vc_map( array(
   "name"      => __("Pricing", 'meminz'),
   "description" => __("Pricing description",'meminz'),
   "base"      => "pricing",
   "class"     => "",
   "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
   "category"  => 'Meminz',
   "params"    => array(
      array(
         "type"      => "textfield",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Pricing Price", 'meminz'),
         "param_name"=> "price",
         "value"     => "$0",
         "description" => __("Pricing price for your product", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Title", 'meminz'),
         "param_name"=> "title",
         "value"     => "Free",
         "description" => __("Title for your product", 'meminz')
      ),
      array(
         "type"      => "textfield",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Sort Description", 'meminz'),
         "param_name"=> "subtitle",
         "value"     => "Free trial 30 days",
         "description" => __("Sort description for your product", 'meminz')
      ),
       array(
         "type"      => "textarea",
         "holder"    => "div",
         "class"     => "",
         "heading"  => __("Content", 'meminz'),
         "param_name"=> "content",
         "value"     => "",
         "description" => __("Content display in pricing block.", 'meminz')
      ),
       array(
         "type"      => "textfield",
         "holder"    => "div",
         "class"     => "",
         "heading"   => __("Label Button", 'meminz'),
         "param_name"=> "label",
         "value"     => "Get it now",
         "description" => __("Text display for button", 'meminz')
      ),
       array(
         "type"      => "textfield",
         "class"     => "",
         "heading"   => __("Link", 'meminz'),
         "param_name"=> "link",
         "value"     => "#",
         "description" => __("Link for button", 'meminz')
      ),
    )));
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_Pricing extends WPBakeryShortCode {}
    }
// Testimonials Slider

   // vc_map( array(
   // "name"      => __($pre_text."Testimonials Slider", 'meminz'),
   // "base"      => "testimonials_slider",
   // "class"     => "",
   // "icon" => "icon-cth",
   // "category"  => 'Content',
   // "params"    => array(
   //    array(
   //       "type"      => "textfield",
   //       "holder"    => "div",
   //       "class"     => "",
   //       "heading"   => __("Count", 'meminz'),
   //       "param_name"=> "count",
   //       "value"     => "3",
   //       "description" => __("Number of testimonials to show in a row", 'meminz')
   //    ),
   //     array(
   //       "type"      => "textfield",

   //       "class"     => "",
   //       "heading"   => __("Extra class", 'meminz'),
   //       "param_name"=> "extra_class",
   //       "value"     => "",
   //       "description" => __("Extra class.", 'meminz')
   //    ),
   //  )));

   // if ( class_exists( 'WPBakeryShortCode' ) ) {
   //      class WPBakeryShortCode_Testimonials_slider extends WPBakeryShortCode {}
   //  }

//Client
  //Register "container" content element. It will hold all your inner (child) content elements
  vc_map( array(
      "name" => __("Client", 'meminz'),
      "base" => "client",
      "category"  => 'Meminz',
      "as_parent" => array('only' => 'client_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
      "content_element" => true,
      "show_settings_on_create" => false,
      "class"=>'cth_client',
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png", // Simply pass url to your icon here
      //'admin_enqueue_css' => array( get_template_directory_uri() . '/vc_extend/custom.css' ),
      "params" => array(
          // add params same as with any other content element
          array(
              "type" => "textfield",
              "heading" => __("Extra class name", "meminz"),
              "param_name" => "el_class",
              "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
          ),
            array(
                 "type"      => "textfield",
                 "holder"    => "div",
                 "class"     => "",
                 "heading"   => __("Count", 'meminz'),
                 "param_name"=> "count",
                 "value"     => "5",
                 "description" => __("Number of client logos to show in a row", 'meminz')
              ),
      ),
      "js_view" => 'VcColumnView'
  ) );
  vc_map( array(
      "name" => __("Client Item", 'meminz'),
      "base" => "client_item",
      "content_element" => true,
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
      "as_child" => array('only' => 'client'), // Use only|except attributes to limit parent (separate multiple values with comma)
      //"icon" => get_template_directory_uri() . "/vc_extend/swiper-icon.png",
      "params" => array(
        //  add params same as with any other content element
        array(
          "type"      => "attach_image",
          "holder"    => "div",
          "class"     => "ajax-vc-img",
          "heading"   => __("Select image", 'meminz'),
          "param_name"=> "cl_image",
          "description" => __("Our client image", 'meminz')
        ),
        array(
          "type"      => "attach_image",
          "holder"    => "div",
          "class"     => "ajax-vc-img",
          "heading"   => __("Select image hover", 'meminz'),
          "param_name"=> "cl_hover",
          "description" => __("Our client image hover.", 'meminz')
        ),
        array(
             "type"      => "textfield",
             "class"     => "",
             "heading"   => __("Link", 'meminz'),
             "param_name"=> "cl_link",
             "value"     => "#",
             "description" => __("Link to client", 'meminz')
        ),
        ),
        'admin_enqueue_js' => get_template_directory_uri() . "/vc_extend/vc_js_elements.js",
        'js_view'=> 'MeminzImagesView',
  ) );

  //Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
  if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
      class WPBakeryShortCode_Client extends WPBakeryShortCodesContainer {
      }
  }
  if ( class_exists( 'WPBakeryShortCode' ) ) {
      class WPBakeryShortCode_Client_Item extends WPBakeryShortCode {
      }
  }


//Testimonials Item Slider
  //Register "container" content element. It will hold all your inner (child) content elements
  vc_map( array(
      "name" => __("Testimonials Slider", 'meminz'),
      "base" => "testimonials_slider",
      "category"  => 'Meminz',
      "as_parent" => array('only' => 'testimonials_slider_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
      "content_element" => true,
      "show_settings_on_create" => false,
      "class"=>'cth_testimonials_slider',
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png", // Simply pass url to your icon here
      //'admin_enqueue_css' => array( get_template_directory_uri() . '/vc_extend/custom.css' ),
      "params" => array(
          // add params same as with any other content element
          array(
              "type" => "textfield",
              "heading" => __("Extra class name", "meminz"),
              "param_name" => "el_class",
              "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
          ),
          array(
              "type" => "textfield",
              "heading" => __("id", "meminz"),
              "param_name" => "el_id",
              "description" => __("Slider id", 'meminz')
          ),
             // array(
             //     "type"      => "textfield",
             //     "holder"    => "div",
             //     "class"     => "",
             //     "heading"   => __("Count", 'meminz'),
             //     "param_name"=> "count",
             //     "value"     => "3",
             //     "description" => __("Number of testimonials to show in a row", 'meminz')
             //  ),

             array(
                "type" => "textfield",
                "heading" => esc_html__("Define `media queries` for columns layout", "meminz"),
                "param_name" => "itemscustom",
                "description" => esc_html__("The format is: screen-size:number-items-display,larger-screen-size:number-items-display. Ex: 479:1,768:1,992:3,1200:3", "meminz"),
                "value" => "320:1,768:1,992:3,1200:3"
            ),
            array(
                "type" => "dropdown",
                "heading" => esc_html__("Auto Play", "meminz"),
                "param_name" => "autoplay",
                // "description" => esc_html__("Boolen or number in mili-second (5000)", "meminz")
                "value" => array(   
                    esc_html__('No', 'meminz') => 'no',  
                    esc_html__('Yes', 'meminz') => 'yes',   
                ),
                "std"=>'no', 
            ),
            // array(
            //     "type" => "textfield",
            //     "heading" => esc_html__("autoplayTimeout", 'meminz'),
            //     "param_name" => "autoplaytimeout",
            //     "value"=>'4000',
            //     "description" => esc_html__("Time after display next slide (in milisecond)", 'meminz')
            // ),

            // array(
            //     "type" => "textfield",
            //     "heading" => esc_html__("autoplaySpeed", 'meminz'),
            //     "param_name" => "autoplayspeed",
            //     "value"=>'3600',
            //     "description" => esc_html__("Duration of transition between slides (in ms) or boolen", 'meminz')
            // ), 
      ),
      "js_view" => 'VcColumnView'
  ) );
  vc_map( array(
      "name" => __("Slide Item", 'meminz'),
      "base" => "testimonials_slider_item",
      "content_element" => true,
      "icon" => get_template_directory_uri() . "/vc_extend/meminz-icon.png",
      "as_child" => array('only' => 'testimonials_slider'), // Use only|except attributes to limit parent (separate multiple values with comma)
      //"icon" => get_template_directory_uri() . "/vc_extend/swiper-icon.png",
      "params" => array(
        //  add params same as with any other content element
        array(
              "type" => "textfield",
              "heading" => __("Extra class name", "meminz"),
              "param_name" => "el_class",
              "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'meminz')
          ),
        array(
          "type"      => "attach_image",
          "holder"    => "div",
          "class"     => "ajax-vc-img",
          "heading"   => __("Avatar Image", 'meminz'),
          "param_name"=> "ts_avatar",
          "description" => __("Client avatar", 'meminz')
        ),
          array(
            "type" => "textfield",
            "heading" => __( "Name", 'meminz' ),
            "param_name" => "ts_name",
            "value" => __( "Alfatih", 'meminz' ),
            "description" => __( "Client name", 'meminz' )
         ),
          array(
              "type" => "textfield",
              "heading" => __("Website", "meminz"),
              "param_name" => "ts_website",
              "value" => __( "99webpage owner", 'meminz' ),
              "description" => __("Client web", 'meminz')
          ),
          array(
             "type"      => "textfield",
             "class"     => "",
             "heading"   => __("Link", 'meminz'),
             "param_name"=> "ts_link",
             "value"     => "#",
             "description" => __("Link for client website", 'meminz')
          ),
          array(
            "type" => "textarea",
            "class" => "",
            "heading" => __( "Client comment", 'meminz' ),
            "param_name" => "content",
            "value" => __( "Sed modus munere menandri ius vero qualisque concludaturque ne vide fastidii incorrupte.", 'meminz' ),
            "description" => __( "Client comment", 'meminz' )
         ), 
      ),
      'admin_enqueue_js' => get_template_directory_uri() . "/vc_extend/vc_js_elements.js",
    'js_view'=> 'MeminzImagesView',
  ) );

  //Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
  if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
      class WPBakeryShortCode_Testimonials_Slider extends WPBakeryShortCodesContainer {
      }
  }
  if ( class_exists( 'WPBakeryShortCode' ) ) {
      class WPBakeryShortCode_Testimonials_Slider_Item extends WPBakeryShortCode {
      }
  }

}

?>