<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $id
 * @var $title
 * @var $number
 * @var $content - shortcode content
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Video_Bg
 */
$el_class = $title = $icon = $link = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div <?php if(!empty($id)) echo ' id="'.esc_attr($id).'" ';?> class="counter-ele">
<?php if(!empty($number)) :?>
	<span id="<?php echo uniqid('num-id');?>"  class="counter-number"><?php echo esc_html($number );?></span>
<?php endif;?>
<?php if(!empty($title)) :?>
	<span class="counter-text"><?php echo esc_html($title );?></span>
<?php endif;?>
	
	<?php echo wpb_js_remove_wpautop($content,true);?>
</div>