<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $title
 * @var $icon
 * @var $link
 * @var $content - shortcode content
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Video_Bg
 */
$el_class = $title = $icon = $link = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="feature-box">
<?php if(!empty($icon)) :?>
	<i class="pe-7s-<?php echo esc_attr($icon );?> pe-feature"></i>
<?php endif;?>
	<?php if(!empty($title) ) echo '<h5>'.esc_html($title ).'</h5>';?>
	<?php echo wpb_js_remove_wpautop($content,true);?>
	<?php if(!empty($link)) :?>
	<div><a href="<?php echo esc_url($link );?>" target="blank"><?php esc_html_e('Learn more','meminz');?></a></div>
	<?php endif;?>
</div>