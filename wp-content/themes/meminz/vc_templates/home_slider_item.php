<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $link_slider
 * @var $images_slider
 * Shortcode class
 * @var $this WPBakeryShortCode_Home_Slider_Item
 */
$el_class = $link_slider = $images_slider = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<li <?php echo esc_attr($el_class );?>>
<?php if(!empty($link_slider)) :?>
	<a href="<?php echo esc_url($link_slider );?>">
<?php else :?>
	<a href="javascript:void(0)">
<?php endif;?>
		<?php echo wp_get_attachment_image( $images_slider, 'meminz-imac-slider', false , array('class'=>'img-responsive') );?>
		<?php echo wpb_js_remove_wpautop($content,true);?>
	</a>
</li>