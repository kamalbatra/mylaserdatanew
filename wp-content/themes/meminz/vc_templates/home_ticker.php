<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $title
 * @var $subtitle
 * @var $link_video
 * @var $text_btn1
 * @var $text_btn2
 * @var $link_btn2
 * @var $content - shortcode content
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Video_Bg
 */

$el_class = $title = $subtitle = $link_video = 
$text_btn1 = $text_btn2 = $link_btn2 = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<h3><?php echo esc_html($title );?>
<?php if(!empty($subtitle) ) echo '<span>'.esc_html($subtitle ).'</span>';?>
</h3>
<?php if(!empty($link_video) || !empty($link_btn2) ) :?>
<p class="btn-inline">
<?php if(!empty($link_video) ) :?>
	<a href="<?php echo esc_url($link_video);?>" data-pretty="prettyPhoto" class="btn btn-bordered btn-lg zoom"><?php echo esc_html($text_btn1);?></a>
<?php endif;?>
<?php if( !empty($link_btn2) ) :?>
	<a href="<?php echo esc_url($link_btn2);?>" class="btn btn-primary btn-lg" target="_blank"><?php echo esc_html($text_btn2);?></a>
<?php endif;?>
</p>
<?php endif;?>