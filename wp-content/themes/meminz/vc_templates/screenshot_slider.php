<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $el_id
 * @var $attachid
 * @var $content - shortcode content
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Home_Slider
 */

$el_class = $el_id = $attachid = '';
$slideshow = 'true';
$animation = 'fade';
$direction = 'horizontal';
$smoothheight = 'true';
$slideshowspeed = 700;
$controlnav = 'true';
$directionnav = 'true';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$dataObj = array();

if($slideshow == 'true'){
	$dataObj['slideshow'] = true;
}else{
	$dataObj['slideshow'] = false;
}

$dataObj['animation'] = $animation;
$dataObj['direction'] = $direction;

if($smoothheight == 'true'){
	$dataObj['smoothHeight'] = true;
}else{
	$dataObj['smoothHeight'] = false;
}
$dataObj['slideshowSpeed'] = (int)$slideshowspeed;

if($controlnav == 'true'){
	$dataObj['controlNav'] = true;
}else{
	$dataObj['controlNav'] = false;
}

if($directionnav == 'true'){
	$dataObj['directionNav'] = true;
}else{
	$dataObj['directionNav'] = false;
}

?>
<div <?php if(!empty($el_id)) echo ' id="'.esc_attr($el_id).'" ';?> class="screenshot-slider">
	<div class="screenshot-wrapper">
		<div class="flexslider text-center" data-options='<?php echo json_encode($dataObj);?>'>
			<ul class="slides">
				<?php echo wpb_js_remove_wpautop($content);?>
			</ul>
		</div>
	</div>
	<?php if(!empty($attachid)) echo wp_get_attachment_image( $attachid, 'full', '' , array('class'=>'img-responsive') );?>
</div>