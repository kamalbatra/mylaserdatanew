<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $el_id
 * @var $count
 * @var $items
 * @var $autoplay
 * @var $content - shortcode content
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Home_Slider
 */

$el_class = $el_id = $count = '';
$itemscustom = '479:1,768:1,992:3,1200:3';
$autoplay = 'yes';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$dataArr = array();
if($autoplay === 'yes'){
    $dataArr['autoPlay'] = true;
}else{
    $dataArr['autoPlay'] = false;
}
// $dataObj->slidespeed = (int)$slidespeed;
// $dataArr['items'] = (int)$items;
$dataArr['itemsCustom'] = $itemscustom;

?>
<div <?php if(!empty($el_id)) echo ' id="'.esc_attr($el_id).'" ';?> class="owl-carousel owl-testimoni wow fadeInDown <?php echo esc_attr($el_class );?>" data-wow-delay="0.4s" data-options='<?php echo json_encode($dataArr);?>'>
	<?php echo wpb_js_remove_wpautop($content);?>
</div>