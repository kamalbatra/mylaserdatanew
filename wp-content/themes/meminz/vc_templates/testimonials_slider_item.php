<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $ts_avatar
 * @var $ts_name
 * @var $ts_website
 * @var $ts_link
 * Shortcode class
 * @var $this WPBakeryShortCode_Home_Slider_Item
 */
$el_class = $ts_avatar = $ts_name = $ts_website = $ts_link = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="testi-slide-wrapp <?php echo esc_attr($el_class );?>">
	<div class="testimonial">
		<blockquote>
		<?php echo wpb_js_remove_wpautop($content,true);?>
		</blockquote>
		<span class="testimoni-sparator"></span>
	</div>
	<div class="clearfix"></div>
	<div class="testimoni-author">
		<div class="author-info">
			<?php if(!empty($ts_name)) echo '<h5>'.esc_html($ts_name ).'</h5>';?>
			<?php if(!empty($ts_website)):?>
			<p>
				<?php if(!empty($ts_link)):?>
				<a href="<?php echo esc_url($ts_link );?>" target="_blank">
			<?php else :?>
				<a href="javascript:void(0)">
			<?php endif;?>
				<?php echo esc_html($ts_website );?>
				</a>
			</p>
			<?php endif;?>
		</div>
		<?php if(!empty($ts_avatar)) echo wp_get_attachment_image($ts_avatar, 'full' );?>
	</div>
</div>