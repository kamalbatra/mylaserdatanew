<?php
$output = $title = '';

extract(shortcode_atts(array(
	'title' => __("Section", "meminz")
), $atts));

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_accordion_section group', $this->settings['base'], $atts );
$output .= "\n\t\t\t" . '<div class="'.$css_class.'">';
	
	// $output .= "\n\t\t\t\t" . '<div class="accordion-heading">';										
	// 	$output .= "\n\t\t\t\t" . '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#'.sanitize_title($title).'">';
	// 		$output .= "\n\t\t\t\t" . '<i class="pe-7s-angle-down"></i>'.$title.'';
	// 	$output .= "\n\t\t\t\t" . '</a>';
	// $output .= "\n\t\t\t\t" . '</div>';

	// $output .= "\n\t\t\t\t" . '<div id="'.sanitize_title($title).'" class="accordion-body collapse">';
	// 	$output .= "\n\t\t\t\t" . '<div class="accordion-inner">';
 //        $output .= ($content=='' || $content==' ') ? __("Empty section. Edit page to add content here.", "meminz") : "\n\t\t\t\t" . wpb_js_remove_wpautop($content);
 //        $output .= "\n\t\t\t\t" . '</div>';
 //    $output .= "\n\t\t\t" . '</div> ' . $this->endBlockComment('.wpb_accordion_section') . "\n";

    $output .= "\n\t\t\t\t" . '<h3 class="wpb_accordion_header ui-accordion-header"><a href="#'.sanitize_title($title).'"><i class="pe-7s-angle-down"></i> '.$title.'</a></h3>';
    
    $output .= "\n\t\t\t\t" . '<div class="wpb_accordion_content ui-accordion-content vc_clearfix">';
        $output .= ($content=='' || $content==' ') ? __("Empty section. Edit page to add content here.", "meminz") : "\n\t\t\t\t" . wpb_js_remove_wpautop($content);
        $output .= "\n\t\t\t\t" . '</div>';
    $output .= "\n\t\t\t" . '</div> ' . $this->endBlockComment('.wpb_accordion_section') . "\n";

echo $output;