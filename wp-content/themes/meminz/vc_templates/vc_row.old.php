<?php
$output = $el_class = $bg_image = $bg_color = $bg_image_repeat = $font_color = $padding = $margin_bottom = $css = $full_width = '';
extract( shortcode_atts( array(
    'el_class' => '',
    'bg_image' => '',
    'bg_color' => '',
    'bg_image_repeat' => '',
    'font_color' => '',
    'padding' => '',
    'margin_bottom' => '',
    'full_width' => false,
    'css' => '',
    'ses_title'=>'',
    'ses_subtitle'=>'',
    'ses_link'=>'',
    'ses_icon'=>'',
    'ses_id'=>'',
    'ses_class'=>'',
    'wrap_class'=>'',
    'layout'=>'default'

), $atts));

// wp_enqueue_style( 'js_composer_front' );
//wp_enqueue_script( 'wpb_composer_front_js' );
// wp_enqueue_style('js_composer_custom_css');

$el_class = $this->getExtraClass( $el_class );

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row ' . ( $this->settings( 'base' ) === 'vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$style_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row-style '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

$style = $this->buildStyle( $bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom );

if(!empty($ses_id)){
    $ses_id = ' id="'.$ses_id.'"';
}
// if(!empty($ses_class)){
//     $ses_class = ' class="'.$ses_class.'"';
// }
if($layout === 'home_ticker_sec'){
    //home ticker
    $ses_class .= ' '.$style_class.' home-wrapper parallax image-bg';
    $output .="\n\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
        $output .="\n\t".'<div class="home-contain">';
            $output .="\n\t".'<div class="container">';
                $output .="\n\t".'<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                    $output .= "\n\t\t\t\t".wpb_js_remove_wpautop($content);    
                $output .="\n\t".'</div>';
            $output .="\n\t".'</div>';
        $output .="\n\t".'</div>';
    $output .="\n\t".'</section>'.$this->endBlockComment('row');
    $output .="\n\t".'<div class="clearfix"></div>';

}elseif($layout === 'home_bg_sec'){
    //home with backgroud video
    $ses_class .= ' '.$style_class.' home-wrapper';
    $output .="\n\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
        $output .="\n\t".'<div class="video-wrapper">';
            $output .="\n\t".'<div class="container">';
                $output .="\n\t".'<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
            $output .= "\n\t\t\t\t".wpb_js_remove_wpautop($content);    
                $output .="\n\t".'</div>';
            $output .="\n\t".'</div>';
        $output .="\n\t".'</div>';  
    $output .="\n\t".'</section>'.$this->endBlockComment('row');

}elseif($layout === 'home_slider_sc'){
    //home slider
    $ses_class .= ' '.$style_class;
    $output .="\n\t".'<section'.$ses_id.$style.' class="'.$ses_class.'" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="">';
        $output .="\n\t".'<div class="parallax-overlay-bg"></div>';             
        $output .="\n\t".'<div class="home-container text-center">';                
            $output .="\n\t".'<div class="home-title">';
                $output .="\n\t".'<div class="banner-title title">';                        
                        $output .="\n\t".'<div class="'.$css_class.'">';
                            $output .= "\n\t\t\t\t".wpb_js_remove_wpautop($content);                       
                        $output .="\n\t".'</div>';
                    $output .="\n\t".'</div>';
            $output .="\n\t".'</div>';
        if(!empty($ses_link)) {
            $output .="\n\t".'<!-- home bottom arrow -->';
            $output .="\n\t".'<div class="arrow">';
                $output .="\n\t".'<div class="arrow-block">';
                    $output .="\n\t".'<a class="arrow-up-click" href="'.$ses_link.'">';                              
                        $output .="\n\t".'<img alt="" src="'.get_stylesheet_directory_uri().'/images/arrow-down.png" />';
                    $output .="\n\t".'</a>';
                $output .="\n\t".'</div>';
            $output .="\n\t".'</div>';             
            $output .="\n\t".'<!-- End home bottom arrow -->';
        }
        $output .="\n\t".'</div> ';          
        
    $output .="\n\t".'</section>'.$this->endBlockComment('row');

}elseif($layout === 'features_sec'){
            //features
        $ses_class .= ' '.$style_class.' contain desc-wrapp gray-bg';
            $output .="\n\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
                $output .="\n\t".'<div class="container">';
                    $output .="\n\t".'<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .="\n\t".'<div class="col-md-8 col-md-offset-2">';
                            $output .="\n\t".'<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .="\n\t".'</div>';
                    $output .="\n\t".'</div>';
                    $output .="\n\t".'<div class="row wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= "\n\t\t\t\t\t\t\t\t\t".wpb_js_remove_wpautop($content);
                    $output .="\n\t".'</div>';
                $output .="\n\t".'</div>';
            $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
            $output .="\n\t\t".'<div class="clearfix"></div>'; 
}elseif($layout === 'des_sec'){
            //Description
            $ses_class .= ' '.$style_class.' contain';
            $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
                $output .="\n\t\t".'<div class="container">';
                    $output .="\n\t\t".'<div class="row">';
                                $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);                           
                    $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'</div>';
            $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
            $output .="\n\t\t".'<div class="clearfix"></div>'; 
}elseif($layout === 'screenshot_sec'){
            //Screenshot
            $ses_class .= ' '.$style_class.' contain';
            $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
                $output .="\n\t\t".'<div class="container">';
                    $output .="\n\t\t".'<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .="\n\t\t".'<div class="col-md-10 col-md-offset-1 wow fadeInUp" data-wow-delay="0.4s">';
                            $output .="\n\t\t".'<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .="\n\t\t".'</div>';
                    $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'<div id="screenshot-contain" class="wow fadeInDown" data-wow-delay="0.4s">';
                    $output .="\n\t\t".'<div class="container">';
                        $output .="\n\t\t".'<div class="row text-center">';
                            $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);  
                        $output .="\n\t\t".'</div>';
                    $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'</div>';
            $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
            $output .="\n\t\t".'<div class="clearfix"></div>';
}elseif($layout === 'pricing_sec'){
            //Pricing
            $ses_class .= ' '.$style_class.' contain gray-bg';
            $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
                $output .="\n\t\t".'<div class="container">';
                    $output .="\n\t\t".'<div class="row text-center">';
                        $output .="\n\t\t".'<div class="col-md-10 col-md-offset-1 wow fadeInUp" data-wow-delay="0.4s">';
                            $output .="\n\t\t".'<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .="\n\t\t".'</div>';
                    $output .="\n\t\t".'</div>';
                    $output .="\n\t\t".'<div class="row wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= "\n\t\t\t".wpb_js_remove_wpautop($content); 
                        //$output .="\n\t\t".'</div>';  
                    $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'</div>';
            $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
            $output .="\n\t\t".'<div class="clearfix"></div>';
}elseif($layout === 'download_sec'){
            //Download
            $ses_class .= ' '.$style_class;
            $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
                $output .="\n\t\t".'<div class="download-wrapper">';
                    $output .="\n\t\t".'<div class="container">';
                        $output .="\n\t\t".'<div class="row wow fadeInUp" data-wow-delay="0.4s">';
                            $output .="\n\t\t".'<h3>'.$ses_title.'</h3>';
                            $output .="\n\t\t".'<p>'.$ses_subtitle.'</p>';
                            $output .= "\n\t\t\t".wpb_js_remove_wpautop($content); 
                        $output .="\n\t\t".'</div>';
                    $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'</div>';
            $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
            $output .="\n\t\t".'<div class="clearfix"></div>';
}elseif($layout === 'counter_sec'){
        //counter
        $ses_class .= ' '.$style_class;
            $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
            $output .="\n\t".'<div class="counter-contain">';
                $output .="\n\t".'<div class="container">';
                    $output .="\n\t".'<div class="row text-center appear stats wow fadeInUp" data-wow-delay="0.4s">';
                        $output .="\n\t".'<div class="col-md-12">';
                            $output .="\n\t".'<h3>'.$ses_title.'</h3>';
                            $output .="\n\t".'<p>'.$ses_subtitle.'</p>';
                        $output .="\n\t".'</div>';
                    $output .="\n\t".'</div>';
                    $output .="\n\t".'<div class="row text-center appear stats wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= "\n\t\t\t\t\t\t\t\t\t".wpb_js_remove_wpautop($content);
                    $output .="\n\t".'</div>';
                $output .="\n\t".'</div>';
            $output .="\n\t".'</div>';
        $output .="\n\t\t".'</section>'.$this->endBlockComment('row');
        $output .="\n\t\t".'<div class="clearfix"></div>';  
}elseif($layout === 'testi_sec'){
    //Testimonials
    $ses_class .= ' '.$style_class.' contain';
        $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
        $output .="\n\t\t".'<div class="container">';
            $output .="\n\t\t".'<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                $output .="\n\t\t".'<div class="col-md-12">';
                    $output .="\n\t\t".'<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                $output .="\n\t\t".'</div>';
            $output .="\n\t\t".'</div>';
        $output .="\n\t\t".'</div>';    
                $output .="\n\t\t".'<div class="row row_fullwidth">';
                    $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);
                $output .="\n\t\t".'</div>';
        $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
        $output .="\n\t\t".'<div class="clearfix"></div>';
}elseif($layout === 'client_sec'){
    //Client
    $ses_class .= ' '.$style_class.' contain gray-bg';
            $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
        $output .="\n\t\t".'<div class="container">';
            $output .="\n\t\t".'<div class="row">';
                       $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);                      
            $output .="\n\t\t".'</div>';
        $output .="\n\t\t".'</div>';
        $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
        $output .="\n\t\t".'<div class="clearfix"></div>';
}elseif($layout === 'contact_sec'){
    //Contact
    $ses_class .= ' '.$style_class;
            $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
        $output .="\n\t\t".'<div class="contact-contain">';
            $output .="\n\t\t".'<div class="container">';
                $output .="\n\t\t".'<div class="row text-center">';
                    $output .="\n\t\t".'<div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s">';
                        $output .="\n\t\t".'<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                    $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'</div>';
                $output .="\n\t\t".'<div class="row">';
                    $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);  
                $output .="\n\t\t".'</div>';
            $output .="\n\t\t".'</div>';
        $output .="\n\t\t".'</div>';
    $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
        $output .="\n\t\t".'<div class="clearfix"></div>';
}else{
    $output .="\n\t\t".'<div class="'.esc_attr( $css_class ).( $full_width == 'stretch_row_content_no_spaces'? ' vc_row-no-padding' : '').'"';
    if ( ! empty( $full_width ) ) {
        $output .=' data-vc-full-width="true"';
        if ( $full_width == 'stretch_row_content' || $full_width == 'stretch_row_content_no_spaces' ) {
            $output .=' data-vc-stretch-content="true"';
        }
    }
    $output .=' '. $style.'>';

    $output .="\n\t\t". wpb_js_remove_wpautop( $content );
    $output .="\n\t\t".'</div>'. $this->endBlockComment( 'row' );
    if ( ! empty( $full_width ) ) {
        $output .="\n\t\t". '<div class="vc_row-full-width"></div>';
    }

    // $output .= '<div class="'.$css_class.'"'.$style.'>';
    // $output .= wpb_js_remove_wpautop($content);
    // $output .= '</div>'.$this->endBlockComment('row');
}


echo $output;