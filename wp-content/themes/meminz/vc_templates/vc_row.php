<?php
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $full_width
 * @var $full_height
 * @var $equal_height
 * @var $columns_placement
 * @var $content_placement
 * @var $parallax
 * @var $parallax_image
 * @var $css
 * @var $el_id
 * @var $video_bg
 * @var $video_bg_url
 * @var $video_bg_parallax
 * @var $parallax_speed_bg
 * @var $parallax_speed_video
 *
 * @var $ses_title
 * @var $ses_subtitle
 * @var $ses_link
 * @var $ses_icon
 * @var $ses_id
 * @var $ses_class
 * @var $wrap_class
 * @var $layout
 *
 * @var $content - shortcode content
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Row
 */
$el_class = $full_height = $parallax_speed_bg = $parallax_speed_video = $full_width = $equal_height = $flex_row = $columns_placement = $content_placement = $parallax = $parallax_image = $css = $el_id = $video_bg = $video_bg_url = $video_bg_parallax = $css_animation = '';
$disable_element = '';
$output = $after_output = '';
//For versopm 1.x
$style = $ses_id = $ses_class = $ses_title = $ses_subtitle = $ses_icon = $wrap_class = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


if($layout === 'home_ticker_sec'){
    //home ticker
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'home-wrapper home-ticker parallax image-bg',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
        $output .='<div class="home-contain">';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                    $output .='<div class="row meminz_row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
        $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;


}elseif($layout === 'home_bg_sec'){
    //home with backgroud video
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'home-wrapper home-video-background',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
        $output .='<div class="video-wrapper">';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                    $output .='<div class="row meminz_row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
        $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

}elseif($layout === 'features_sec'){
            //features
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'features_sec contain desc-wrapp gray-bg',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                if(!empty($ses_title) || !empty($ses_subtitle)){
                    $output .='<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .='<div class="col-md-8 col-md-offset-2">';
                            $output .='<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .='</div>';
                    $output .='</div>';
                }
                    $output .='<div class="row meminz_row wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

}elseif($layout === 'des_sec'){
            //Description
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'description_sec contain',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                    $output .='<div class="row meminz_row">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

}elseif($layout === 'screenshot_sec'){
            //Screenshot
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'screenshot_sec contain',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                if(!empty($ses_title) || !empty($ses_subtitle)){
                    $output .='<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .='<div class="col-md-10 col-md-offset-1 wow fadeInUp" data-wow-delay="0.4s">';
                            $output .='<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .='</div>';
                    $output .='</div>';
                }
                    $output .='<div class="row meminz_row wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

        //     $ses_class .= ' '.$style_class.' contain';
        //     $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
        //         $output .="\n\t\t".'<div class="container">';
        //             $output .="\n\t\t".'<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
        //                 $output .="\n\t\t".'<div class="col-md-10 col-md-offset-1 wow fadeInUp" data-wow-delay="0.4s">';
        //                     $output .="\n\t\t".'<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
        //                 $output .="\n\t\t".'</div>';
        //             $output .="\n\t\t".'</div>';
        //         $output .="\n\t\t".'</div>';
        //         $output .="\n\t\t".'<div id="screenshot-contain" class="wow fadeInDown" data-wow-delay="0.4s">';
        //             $output .="\n\t\t".'<div class="container">';
        //                 $output .="\n\t\t".'<div class="row meminz_row text-center">';
        //                     $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);  
        //                 $output .="\n\t\t".'</div>';
        //             $output .="\n\t\t".'</div>';
        //         $output .="\n\t\t".'</div>';
        //     $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
        //     $output .="\n\t\t".'<div class="clearfix"></div>';
        // echo $output;
}elseif($layout === 'pricing_sec'){
            //Pricing
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'pricing_sec contain gray-bg',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                if(!empty($ses_title) || !empty($ses_subtitle)){
                    $output .='<div class="row text-center">';
                        $output .='<div class="col-md-10 col-md-offset-1 wow fadeInUp" data-wow-delay="0.4s">';
                            $output .='<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .='</div>';
                    $output .='</div>';
                }
                    $output .='<div class="row meminz_row wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

}elseif($layout === 'download_sec'){
            //Download
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'download_sec',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
        $output .='<div class="download-wrapper">';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                
                    $output .='<div class="row meminz_row wow fadeInDown" data-wow-delay="0.4s">';
                        if(!empty($ses_title) || !empty($ses_subtitle)){
                            $output .='<h3>'.$ses_title.'</h3>';
                            $output .='<p>'.$ses_subtitle.'</p>';
                        }

                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
        $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;
}elseif($layout === 'counter_sec'){
        //counter
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'counter_sec',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
        $output .='<div class="counter-contain">';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                if(!empty($ses_title) || !empty($ses_subtitle)){
                    $output .='<div class="row text-center  appear stats wow fadeInUp">';
                        $output .='<div class="col-md-12">';
                            $output .='<h3>'.$ses_title.'</h3>';
                            $output .='<p>'.$ses_subtitle.'</p>';
                        $output .='</div>';
                    $output .='</div>';
                }
                    $output .='<div class="row meminz_row text-center appear stats wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
        $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

}elseif($layout === 'testi_sec'){
    //Testimonials
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'testimonials_sec contain',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                if(!empty($ses_title) || !empty($ses_subtitle)){
                    $output .='<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .='<div class="col-md-12">';
                            $output .='<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .='</div>';
                    $output .='</div>';
                }
                    $output .='<div class="row meminz_row wow fadeInDown" data-wow-delay="0.4s">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

    
}elseif($layout === 'client_sec'){
    //Client
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'clients_sec contain gray-bg',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                if(!empty($ses_title) || !empty($ses_subtitle)){
                    $output .='<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .='<div class="col-md-12">';
                            $output .='<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .='</div>';
                    $output .='</div>';
                }
                    $output .='<div class="row meminz_row">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

    // $ses_class .= ' '.$style_class.' contain gray-bg';
    //         $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
    //     $output .="\n\t\t".'<div class="container">';
    //         $output .="\n\t\t".'<div class="row meminz_row">';
    //                    $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);                      
    //         $output .="\n\t\t".'</div>';
    //     $output .="\n\t\t".'</div>';
    //     $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
    //     $output .="\n\t\t".'<div class="clearfix"></div>';
    //     echo $output;
}elseif($layout === 'contact_sec'){
    //Contact
    $el_class = $this->getExtraClass( $el_class );

    $css_classes = array(
        'meminz_sec',
        'contact_sec',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );
    // $css_class = preg_replace( '/\s+/', ' ', implode( ' ', array_filter( $css_classes ) ) );
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes = array();
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }elseif(!empty($ses_id)){
        $wrapper_attributes[] = 'id="' . esc_attr( $ses_id ) . '"';
    }
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<section ' . implode( ' ', $wrapper_attributes ) . '>';
        $output .='<div class="contact-contain">';
            if ( ! empty( $full_width ) ) {
                $output .='<div class="container-fluid">';
            }else{
                $output .='<div class="container">';
            }
                if(!empty($ses_title) || !empty($ses_subtitle)){
                    $output .='<div class="row text-center wow fadeInUp" data-wow-delay="0.4s">';
                        $output .='<div class="col-md-12">';
                            $output .='<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
                        $output .='</div>';
                    $output .='</div>';
                }
                    $output .='<div class="row meminz_row">';
                        $output .= wpb_js_remove_wpautop($content);    
                    $output .='</div>';
                $output .='</div>';
        $output .='</div>';
    $output .='</section>'.$this->endBlockComment('row');
    $output .='<div class="clearfix"></div>';

    echo $output;

    // $ses_class .= ' '.$style_class;
    //         $output .="\n\t\t".'<section '.$ses_id.$style.' class="'.$ses_class.'">';
    //     $output .="\n\t\t".'<div class="contact-contain">';
    //         $output .="\n\t\t".'<div class="container">';
    //             $output .="\n\t\t".'<div class="row text-center">';
    //                 $output .="\n\t\t".'<div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s">';
    //                     $output .="\n\t\t".'<h3 class="heading"><span>'.$ses_title.'</span>'.$ses_subtitle.'</h3>';
    //                 $output .="\n\t\t".'</div>';
    //             $output .="\n\t\t".'</div>';
    //             $output .="\n\t\t".'<div class="row meminz_row">';
    //                 $output .= "\n\t\t\t".wpb_js_remove_wpautop($content);  
    //             $output .="\n\t\t".'</div>';
    //         $output .="\n\t\t".'</div>';
    //     $output .="\n\t\t".'</div>';
    // $output .="\n\t\t".'</section>'.$this->endBlockComment('row'); 
    //     $output .="\n\t\t".'<div class="clearfix"></div>';
    //     echo $output;
}else{

    wp_enqueue_script( 'wpb_composer_front_js' );

    $el_class = $this->getExtraClass( $el_class ) . $this->getCSSAnimation( $css_animation );

    $css_classes = array(
        'vc_row',
        'wpb_row',
        //deprecated
        'vc_row-fluid',
        $el_class,
        vc_shortcode_custom_css_class( $css ),
    );

    if ( 'yes' === $disable_element ) {
        if ( vc_is_page_editable() ) {
            $css_classes[] = 'vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md';
        } else {
            return '';
        }
    }

    if ( vc_shortcode_custom_css_has_property( $css, array(
            'border',
            'background',
        ) ) || $video_bg || $parallax
    ) {
        $css_classes[] = 'vc_row-has-fill';
    }

    if ( ! empty( $atts['gap'] ) ) {
        $css_classes[] = 'vc_column-gap-' . $atts['gap'];
    }

    $wrapper_attributes = array();
    // build attributes for wrapper
    if ( ! empty( $el_id ) ) {
        $wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
    }
    if ( ! empty( $full_width ) ) {
        $wrapper_attributes[] = 'data-vc-full-width="true"';
        $wrapper_attributes[] = 'data-vc-full-width-init="false"';
        if ( 'stretch_row_content' === $full_width ) {
            $wrapper_attributes[] = 'data-vc-stretch-content="true"';
        } elseif ( 'stretch_row_content_no_spaces' === $full_width ) {
            $wrapper_attributes[] = 'data-vc-stretch-content="true"';
            $css_classes[] = 'vc_row-no-padding';
        }
        $after_output .= '<div class="vc_row-full-width vc_clearfix"></div>';
    }

    if ( ! empty( $full_height ) ) {
        $css_classes[] = 'vc_row-o-full-height';
        if ( ! empty( $columns_placement ) ) {
            $flex_row = true;
            $css_classes[] = 'vc_row-o-columns-' . $columns_placement;
            if ( 'stretch' === $columns_placement ) {
                $css_classes[] = 'vc_row-o-equal-height';
            }
        }
    }

    if ( ! empty( $equal_height ) ) {
        $flex_row = true;
        $css_classes[] = 'vc_row-o-equal-height';
    }

    if ( ! empty( $content_placement ) ) {
        $flex_row = true;
        $css_classes[] = 'vc_row-o-content-' . $content_placement;
    }

    if ( ! empty( $flex_row ) ) {
        $css_classes[] = 'vc_row-flex';
    }

    $has_video_bg = ( ! empty( $video_bg ) && ! empty( $video_bg_url ) && vc_extract_youtube_id( $video_bg_url ) );

    $parallax_speed = $parallax_speed_bg;
    if ( $has_video_bg ) {
        $parallax = $video_bg_parallax;
        $parallax_speed = $parallax_speed_video;
        $parallax_image = $video_bg_url;
        $css_classes[] = 'vc_video-bg-container';
        wp_enqueue_script( 'vc_youtube_iframe_api_js' );
    }

    if ( ! empty( $parallax ) ) {
        wp_enqueue_script( 'vc_jquery_skrollr_js' );
        $wrapper_attributes[] = 'data-vc-parallax="' . esc_attr( $parallax_speed ) . '"'; // parallax speed
        $css_classes[] = 'vc_general vc_parallax vc_parallax-' . $parallax;
        if ( false !== strpos( $parallax, 'fade' ) ) {
            $css_classes[] = 'js-vc_parallax-o-fade';
            $wrapper_attributes[] = 'data-vc-parallax-o-fade="on"';
        } elseif ( false !== strpos( $parallax, 'fixed' ) ) {
            $css_classes[] = 'js-vc_parallax-o-fixed';
        }
    }

    if ( ! empty( $parallax_image ) ) {
        if ( $has_video_bg ) {
            $parallax_image_src = $parallax_image;
        } else {
            $parallax_image_id = preg_replace( '/[^\d]/', '', $parallax_image );
            $parallax_image_src = wp_get_attachment_image_src( $parallax_image_id, 'full' );
            if ( ! empty( $parallax_image_src[0] ) ) {
                $parallax_image_src = $parallax_image_src[0];
            }
        }
        $wrapper_attributes[] = 'data-vc-parallax-image="' . esc_attr( $parallax_image_src ) . '"';
    }
    if ( ! $parallax && $has_video_bg ) {
        $wrapper_attributes[] = 'data-vc-video-bg="' . esc_attr( $video_bg_url ) . '"';
    }
    $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( array_unique( $css_classes ) ) ), $this->settings['base'], $atts ) );
    $wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

    $output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
    $output .= wpb_js_remove_wpautop( $content );
    $output .= '</div>';
    $output .= $after_output;

    echo $output;



}// end if $layout