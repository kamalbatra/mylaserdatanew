<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $video_id
 * @var $video_title
 * @var $show_control
 * @var $auto_play
 * @var $loop
 * @var $mute
 * @var $opacity
 * @var $containment
 * @var $quanlity
 * @var $content - shortcode content
 *
 * Shortcode class
 * @var $this WPBakeryShortCode_Video_Bg
 */
$el_class = $video_id = $video_title = $show_control = 
$auto_play = $loop = $mute = $opacity = $containment = $quanlity = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$video_datas = array();

$video_datas['videoURL'] = !empty($video_id) ? $video_id: 'e8DFN3m8XGQ';
$video_datas['containment'] = !empty($containment) ? $containment: '.video-wrapper';
$video_datas['showControls'] = false;
$video_datas['autoPlay'] = true;
if($loop == 'true') {
	$video_datas['loop'] = true;
}else{
	$video_datas['loop'] = false;
}
if($mute == 'true') {
	$video_datas['mute'] = true;
}else{
	$video_datas['mute'] = false;
}
$video_datas['startAt'] = 0;
$video_datas['addRaster'] = false;
$video_datas['opacity'] = !empty($opacity) ? floatval($opacity): 1;
$video_datas['quality'] = !empty($quanlity) ? $quanlity: 'default';
?>

<a id="bgndVideo" class="player" data-property='<?php echo json_encode($video_datas);?>'><?php echo esc_html($video_title );?></a>